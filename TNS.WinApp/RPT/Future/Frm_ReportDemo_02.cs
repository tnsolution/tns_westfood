﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using TN.Connect;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ReportDemo_02 : Form
    {
        public Frm_ReportDemo_02()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Print.Click += Btn_Print_Click;
            btn_Search.Click += Btn_Search_Click;
            DateTime ViewDate = DateTime.Now; //SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, 1, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(12).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
        }

        private void Btn_Print_Click(object sender, EventArgs e)
        {
            Crt_Data.Printing.PageSetup();
            Crt_Data.Printing.PrintPreview();
        }

        private void Frm_ReportDemo_02_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {


            //if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            //{
            //    Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
            //    return;
            //}
            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy");
            //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBox(ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
            DataTable ztb = TNS.WinApp.Frm_ReportDemo_02_DLL.Rpt_Demo.TONGLUONGNANGSUAT_THEONGAY(FromDate, ToDate);
            this.Invoke(new MethodInvoker(delegate ()
            {
                Show_Chart_Voice_Time(ztb);
            }));
        }
        private void Show_Chart_Voice_Time(DataTable ztb)
        {
            Crt_Data.Series.Clear();
            //Trục y
            //Crt_Data.ChartAreas[0].AxisY.Minimum = 0; //Giá trị nhỏ nhất cột y
            //Crt_Data.ChartAreas[0].AxisY.Maximum = 10000000000; // giá trị lớn nhất cột y
            Crt_Data.ChartAreas[0].AxisY.LabelStyle.ForeColor = Color.White; //màu chữ cột y
            Crt_Data.ChartAreas[0].AxisY.LineColor = Color.White; // Màu trục y
            Crt_Data.ChartAreas[0].AxisY.LabelStyle.Format = "n0"; // định dạng chữ cột y
            //Trục X
            Crt_Data.ChartAreas[0].AxisX.Interval = 1;
            Crt_Data.ChartAreas[0].AxisX.LineColor = Color.White;
            Crt_Data.ChartAreas[0].AxisX.LabelStyle.ForeColor = Color.White;

            //Tiêu đề báo cáo
            Crt_Data.ChartAreas[0].AxisX.Title = "BIỂU ĐỒ TỔNG LƯƠNG KHOÁN NĂNG XUẤT THEO THÁNG";
            Crt_Data.ChartAreas[0].AxisX.TitleForeColor = Color.White;
            Crt_Data.ChartAreas[0].AxisX.TitleFont = new Font("Tahoma", 9, FontStyle.Bold);


            Crt_Data.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;
            //Crt_Data.ChartAreas[0].BackColor = Color.FromArgb(227, 239, 255);

            Series zSeries_DomainTime = Crt_Data.Series.Add("Lương khoán năng xuất (VNĐ/tháng)");
            zSeries_DomainTime.Color = Color.Blue;
            zSeries_DomainTime.ChartType = SeriesChartType.Spline;
            zSeries_DomainTime.LabelForeColor = Color.White;
            zSeries_DomainTime.BorderWidth = 2;


            //zSeries_DomainTime["OrawingStyle"] =  "Cylinder";
            for (int j = 0; j < ztb.Rows.Count; j++)
            {
                zSeries_DomainTime.Points.AddXY("Tháng " + ztb.Rows[j]["DateWork"].ToString(), double.Parse(ztb.Rows[j]["Total"].ToString()));
                zSeries_DomainTime.Points[j].Label = ztb.Rows[j]["Total"].Toe0String();
                //zSeries_DomainTime.Points[j].AxisLabel = ztb.Rows[j]["LeftColumn"].ToString();
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            //dragging = true;
            //dragCursorPoint = Cursor.Position;
            //dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                //Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                //this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
            }
        }
        #endregion
    }
}
namespace TNS.WinApp.Frm_ReportDemo_02_DLL
{
    public class Rpt_Demo
    {
        public static DataTable TONGLUONGNANGSUAT_THEONGAY(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zSQL = @"

--DECLARE @FromDate datetime='2021-01-01 00:00:00'
--DECLARE @ToDate datetime='2021-05-30 00:00:00'

CREATE TABLE #temp(
DateWork datetime,
[MoneyDate] money
)
--Nhóm chính
INSERT INTO #temp
SELECT OrderDate,SUM([Money] + MoneyPrivate +Money_Borrow)  AS [MoneyDate]
FROM [dbo].[FTR_Order_Money] 
WHERE  OrderDate BETWEEN @FROMDATE AND @TODATE 
AND RecordStatus <>99 
GROUP BY OrderDate

--Chia lại
INSERT INTO #temp 
SELECT OrderDate,Sum(MoneyPersonal) 
FROM [dbo].[FTR_Order_Adjusted]
WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate 
AND Share=0 AND RecordStatus <>99 
GROUP BY OrderDate

--Giờ dư
INSERT INTO #temp 
SELECT DateImport,[Money] from [dbo].[Temp_Import_Detail]
WHERE  DateImport BETWEEN @FROMDATE AND @TODATE  
AND RecordStatus <>99 

CREATE TABLE #temp2(
DateWork INT,
Total money
)
--Sum tổng lương 1 ngày
INSERT INTO #temp2 
SELECT MONTH(DateWork), SUM([MoneyDate]) FROM #temp 
GROUP BY MONTH(DateWork)


SELECT DateWork,Total  FROM #temp2 
ORDER BY DateWork

DROP TABLE #temp
DROP TABLE #temp2
";
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}

