﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.SLR;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
using TNS.CORE;

namespace TNS.WinApp
{
    public partial class Frm_BaoCaoTongHopThuNhap : Form
    {
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private int _Radio = 0;
        public Frm_BaoCaoTongHopThuNhap()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            btn_Search.Click += Btn_Search_Click;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            btn_Export.Click += Btn_Export_Click;
            rdo_BP.CheckedChanged += Rdo_BP_CheckedChanged;
            rdo_KH.CheckedChanged += Rdo_KH_CheckedChanged;
            rdo_TN.CheckedChanged += Rdo_TN_CheckedChanged;

        }

       

        private void Rdo_KH_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_KH.Checked == true)
            {
                cbo_Department.Enabled = false;
                cbo_Team.Enabled = false;
            }
        }
        private void Rdo_BP_CheckedChanged(object sender, EventArgs e)
        {
            if(rdo_BP.Checked==true)
            {
                cbo_Department.Enabled = true;
                cbo_Team.Enabled = false;
            }
        }
        private void Rdo_TN_CheckedChanged(object sender, EventArgs e)
        {
            cbo_Department.Enabled = true;
            cbo_Team.Enabled = true;
        }
        private void Frm_BaoCaoTongHopThuNhapcs_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            DateTime DateView = SessionUser.Date_Work;
            dte_FromDate.Value = new DateTime(DateView.Year, 1, 1);
            dte_ToDate.Value = new DateTime(DateView.Year, DateView.Month, 1);
            if (Role.AccessBranch == "2,4")
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "--Chọn tất cả--");
            else
            {
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "");
                Cbo_Branch_SelectedIndexChanged(null, null);
            }
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            cbo_Department.Enabled = false;
            cbo_Team.Enabled = false;
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            if (rdo_KH.Checked)
                _Radio = 0;
            if (rdo_BP.Checked)
                _Radio = 1;
            if (rdo_TN.Checked)
                _Radio = 2; if (dte_FromDate.Value.Year != dte_ToDate.Value.Year)
            {
                Utils.TNMessageBoxOK( "Bạn chỉ được chọn trong 1 năm", 1);
                return;
            }
            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }

        private void DisplayData()
        {
           
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month,1, 0, 0, 0);

            DateTime DateView = dte_ToDate.Value;
            DateTime zFromDate = new DateTime(DateView.Year, DateView.Month, 1, 0, 0, 0);
            DateTime ToDate = zFromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            
            DataTable zTable = new DataTable();
            if (_Radio==0)
                 zTable = Report.TONGTHUNHAP_KHOI(FromDate, ToDate, _BranchKey);
            if (_Radio == 1)
            {
                //if(rdo_Default.Checked==true)
                zTable = Report.TONGTHUNHAP_BOPHAN_MACDINH(FromDate, ToDate, _BranchKey, _DepartmentKey);
                //else
                //    zTable = Report.TONGTHUNHAP_BOPHAN_QUANLY(FromDate, ToDate, _BranchKey, _DepartmentKey);
            }
            if (_Radio == 2)
                zTable = Report.TONGTHUNHAP_TONHOM(FromDate, ToDate, _BranchKey, _DepartmentKey,_TeamKey);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "Amount" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Sô tiền" }, 0);

                string[] temp = zTablePivot.Columns[1].ColumnName.Split('|');
                string DateFlag = temp[0];
                string KeyFlag = temp[1];
                int m = 0;
                for (int i = 1; i < zTablePivot.Columns.Count; i++)
                {
                    temp = zTablePivot.Columns[i].ColumnName.Split('|');
                    if (temp.Length == 2)// nếu là TONGCONG thì add vào cột tổng nhỏ
                    {
                        zTablePivot.Columns.Add(DateFlag + "|Tổng", typeof(double)).SetOrdinal(i);
                        break;
                    }
                    string Date = temp[0];
                    string TeamKey = temp[1];
                    string TeamName = temp[2];


                    if (DateFlag != "0" && DateFlag != Date)
                    {
                        //add cột tổng
                        zTablePivot.Columns.Add(DateFlag + "|Tổng", typeof(double)).SetOrdinal(i);
                        DateFlag = Date;
                    }

                    if (KeyFlag != TeamKey)
                    {
                        KeyFlag = TeamKey;
                    }

                }

                //tính toán cột tổng mỗi tháng
                foreach (DataRow r in zTablePivot.Rows)
                {
                    temp = zTablePivot.Columns[1].ColumnName.Split('|');
                    double TongCotThang = 0;
                    DateFlag = temp[0];

                    for (int k = 1; k < zTablePivot.Columns.Count; k++)
                    {
                        temp = zTablePivot.Columns[k].ColumnName.Split('|');
                        string Date = temp[0];

                        if (DateFlag != Date)
                        {
                            r[k - 1] = TongCotThang.ToString();

                            TongCotThang = 0;
                            DateFlag = temp[0];
                        }
                        double zSoTien = 0;
                        if (double.TryParse(r[k].ToString(), out zSoTien))
                        {

                        }
                        TongCotThang += zSoTien;
                    }
                }
                //Tính lại Trung bình thu nhập tổng nhỏ mỗi tháng, và tổng nhỏ của tổng cộng
                for (int i = 1; i < zTablePivot.Columns.Count - 1; i++)
                {
                    DataColumn Col = zTablePivot.Columns[i];
                    string[] strCaption = Col.ColumnName.Split('|');
                    double ThuNhap = 0;
                    double SoLD = 0;
                    if (strCaption[1] == "Tổng" || strCaption[0]=="99/9999")
                    {
                        for(int k=0;k< zTablePivot.Rows.Count-1; k++)
                        {
                            //Nếu là trung bình phải chia Thu nhập:  TB tổng =Tổng thu nhập tháng / Tổng lao động thangs
                            string[] P = zTablePivot.Rows[k][0].ToString().Split('.');
                            double zSoTien = 0;
                            if (double.TryParse(zTablePivot.Rows[k][i].ToString(), out zSoTien))
                            {

                            }
                            if (P[0] == "08")
                            {
                                ThuNhap = zSoTien;
                            }
                            if (P[0] == "09")
                            {
                                SoLD = zSoTien;
                            }
                            if (P[0] == "12")
                            {
                                if (SoLD != 0)
                                    zTablePivot.Rows[k][i] = (ThuNhap / SoLD);
                                else
                                {
                                    zTablePivot.Rows[k][i] = 0;
                                }

                            }
                           
                        }
                    }
                }
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        private void AddGroup(DataTable zTablePivot, string Loai, DataRow[] Array, int Index)
        {
            DataTable zGroup = Array.CopyToDataTable();
            DataRow dr = zTablePivot.NewRow();
            dr[0] = Loai;
            dr[1] = Loai;

            for (int c = 2; c < zGroup.Columns.Count; c++)
            {
                float Tong = 0;
                foreach (DataRow r in zGroup.Rows)
                {
                    float ztemp = 0;
                    if (float.TryParse(r[c].ToString(), out ztemp))
                    {

                    }
                    Tong += ztemp;
                }
                dr[c] = Tong;
            }

            zTablePivot.Rows.InsertAt(dr, Index);
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = TableView.Columns.Count;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "NỘI DUNG";

            //Row Header
            int ColStart = 2;
            for (int i = 1; i < TableView.Columns.Count - 1; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    if(strCaption[0]=="99/9999")
                    {
                        GVData.Rows[0][ColStart] = "TỔNG CỘNG";
                        GVData.Rows[1][ColStart] = strCaption[2];
                        GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Purple;
                        GVData.Cols[ColStart].Width = 120;
                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[2];
                        GVData.Cols[ColStart].Width = 120;
                    }

                }
                else
                {

                    if (strCaption[0] == "99/9999")
                    {
                        GVData.Rows[0][ColStart] = "TỔNG CỘNG";
                        GVData.Rows[1][ColStart] = strCaption[1];
                        GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Red;
                        GVData.Cols[ColStart].Width = 120;
                    }
                    else
                    {
                        if (strCaption[1] == "Tổng")
                        {
                            GVData.Rows[0][ColStart] = strCaption[0];
                            GVData.Rows[1][ColStart] = strCaption[1];
                            GVData.Cols[ColStart].Width = 120;
                            GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                            GVData.Cols[ColStart].StyleNew.ForeColor = Color.Blue;
                        }
                        else
                        {
                            GVData.Rows[0][ColStart] = strCaption[0];
                            GVData.Rows[1][ColStart] = strCaption[1];
                            GVData.Cols[ColStart].Width = 120;
                        }
                    }
                }

                ColStart++;
            }


            //--------------------
            //double TotalLK = 0;
            //double TotalTG = 0;
            //double TotalTP = 0;

            //Fill Data
            ColStart = 1;// add data vào cột thứ 1 trở đi
            int RowStart = 2;//add dòng từ số thứ tự 4 trở đi
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count-1; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows[RowStart][0] = zNo;
                string[] s = rData[0].ToString().Trim().Split('.');
                GVData.Rows[RowStart][1] = s[1];
                ColStart = 2;
                for (int k = 1; k < rData.ItemArray.Length - 1; k++)
                {
                    GVData.Rows[RowStart][ColStart] = rData[k].Toe0String();// tên nội dung
                    ColStart++;
                }
                zNo++;
                RowStart++;
            }

            ////Style

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            //GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;
            //Trộn dòng 2
            GVData.Rows[2].AllowMerging = true;
            //Trộn dòng cuối
            GVData.Rows[GVData.Rows.Count - 1].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 300;

            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 2;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 2; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            ////GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
            // tô màu cột tổng
            //GVData.Rows[TotalRow - 1].StyleNew.BackColor = Color.LightGreen;

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo cáo thu nhập.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        Access_Role_Info Role;
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }

        }
        #endregion
    }
}
