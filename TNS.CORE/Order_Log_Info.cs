﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.CORE
{
    public class Order_Log_Info
    {
        #region [FiledName]
        private int _AutoKey = 0;
        private string _OrderKey = "";
        private string _OrderID = "";
        private DateTime _OrderDate;
        private int _Status = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [Properties] 
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }

        public string OrderID
        {
            get
            {
                return _OrderID;
            }

            set
            {
                _OrderID = value;
            }
        }

        public int Status
        {
            get
            {
                return _Status;
            }

            set
            {
                _Status = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        public DateTime OrderDate
        {
            get
            {
                return _OrderDate;
            }

            set
            {
                _OrderDate = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Order_Log_Info()
        {
        }
        public Order_Log_Info(string OrderKey)
        {
            string zSQL = "SELECT * FROM FTR_Order WHERE OrderKey = @OrderKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _OrderKey = zReader["OrderKey"].ToString();
                    _OrderID = zReader["OrderID"].ToString().Trim();
                    if (_OrderDate == DateTime.MinValue)
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                    if (zReader["Status"] != DBNull.Value)
                        _Status = int.Parse(zReader["Status"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FTR_Order_Log(" +
                          "OrderKey,OrderID,OrderDate,Status," +
                          "CreatedOn,CreatedBy,CreatedName)" +
                          "VALUES (" +
                          "@OrderKey,@OrderID,@OrderDate,@Status," +
                          "GETDATE(),@CreatedBy,@CreatedName)";
            zSQL += "SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE FTR_Order_Log SET " +
                          "OrderKey = @OrderKey," +
                          "OrderID = @OrderID," +
                          "OrderDate = @OrderDate," +
                          "Status = @Status," +
                          "ModifiedOn = GETDATE()," +
                          "ModifiedBy = @ModifiedBy," +
                          "ModifiedName = @ModifiedName " +
                          "WHERE AutoKey = @AutoKey";
            zSQL += "SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@Status", SqlDbType.Int).Value = _Status;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FTR_Order_Log WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_OrderKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion



    }
}
