﻿namespace TNS.WinApp
{
    partial class Frm_Report_4_V2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Report_4_V2));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Print = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_TeamID_Search = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.GVData = new System.Windows.Forms.DataGridView();
            this.Panel_Done = new System.Windows.Forms.Panel();
            this.dte_RpDate = new TN_Tools.TNDateTime();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_RpGiamDoc = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpHCNS = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpKeToan = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpNguoiLap = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_RpTitle = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonHeader6 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnClose_Panel_Message = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_Done = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_TeamID_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.Panel_Done.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1366, 42);
            this.HeaderControl.TabIndex = 198;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Bảng thông tin tổng hợp lương [năng suất]";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btn_Print);
            this.panel1.Controls.Add(this.dte_ToDate);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.dte_FromDate);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.cbo_TeamID_Search);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1366, 80);
            this.panel1.TabIndex = 206;
            // 
            // btn_Print
            // 
            this.btn_Print.Location = new System.Drawing.Point(644, 11);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Print.Size = new System.Drawing.Size(111, 40);
            this.btn_Print.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Print.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Print.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Print.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Print.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Print.TabIndex = 319;
            this.btn_Print.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Print.Values.Image")));
            this.btn_Print.Values.Text = "Xuất In";
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(49, 30);
            this.dte_ToDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(99, 25);
            this.dte_ToDate.TabIndex = 215;
            this.dte_ToDate.Value = new System.DateTime(((long)(0)));
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(8, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 15);
            this.label2.TabIndex = 214;
            this.label2.Text = "Đến";
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(49, 5);
            this.dte_FromDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(99, 25);
            this.dte_FromDate.TabIndex = 213;
            this.dte_FromDate.Value = new System.DateTime(((long)(0)));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(16, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 15);
            this.label3.TabIndex = 212;
            this.label3.Text = "Từ";
            // 
            // cbo_TeamID_Search
            // 
            this.cbo_TeamID_Search.DropDownWidth = 119;
            this.cbo_TeamID_Search.Location = new System.Drawing.Point(153, 20);
            this.cbo_TeamID_Search.Name = "cbo_TeamID_Search";
            this.cbo_TeamID_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_TeamID_Search.Size = new System.Drawing.Size(273, 22);
            this.cbo_TeamID_Search.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_TeamID_Search.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_TeamID_Search.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_TeamID_Search.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_TeamID_Search.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_TeamID_Search.StateCommon.Item.Border.Rounding = 4;
            this.cbo_TeamID_Search.StateCommon.Item.Border.Width = 1;
            this.cbo_TeamID_Search.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_TeamID_Search.TabIndex = 207;
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(538, 10);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(100, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 205;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(432, 10);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(100, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 201;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Xem";
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 122);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(1366, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 208;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Danh sách";
            // 
            // GVData
            // 
            this.GVData.BackgroundColor = System.Drawing.Color.White;
            this.GVData.ColumnHeadersHeight = 25;
            this.GVData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.Location = new System.Drawing.Point(0, 152);
            this.GVData.Name = "GVData";
            this.GVData.ReadOnly = true;
            this.GVData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData.Size = new System.Drawing.Size(1366, 448);
            this.GVData.TabIndex = 209;
            // 
            // Panel_Done
            // 
            this.Panel_Done.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Panel_Done.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Done.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Done.Controls.Add(this.dte_RpDate);
            this.Panel_Done.Controls.Add(this.label10);
            this.Panel_Done.Controls.Add(this.label1);
            this.Panel_Done.Controls.Add(this.label4);
            this.Panel_Done.Controls.Add(this.label6);
            this.Panel_Done.Controls.Add(this.txt_RpGiamDoc);
            this.Panel_Done.Controls.Add(this.txt_RpHCNS);
            this.Panel_Done.Controls.Add(this.txt_RpKeToan);
            this.Panel_Done.Controls.Add(this.txt_RpNguoiLap);
            this.Panel_Done.Controls.Add(this.label8);
            this.Panel_Done.Controls.Add(this.label16);
            this.Panel_Done.Controls.Add(this.txt_RpTitle);
            this.Panel_Done.Controls.Add(this.kryptonHeader6);
            this.Panel_Done.Controls.Add(this.btn_Done);
            this.Panel_Done.Location = new System.Drawing.Point(367, 204);
            this.Panel_Done.Name = "Panel_Done";
            this.Panel_Done.Size = new System.Drawing.Size(492, 325);
            this.Panel_Done.TabIndex = 292;
            // 
            // dte_RpDate
            // 
            this.dte_RpDate.CustomFormat = "dd/MM/yyyy";
            this.dte_RpDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_RpDate.Location = new System.Drawing.Point(183, 35);
            this.dte_RpDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_RpDate.Name = "dte_RpDate";
            this.dte_RpDate.Size = new System.Drawing.Size(121, 26);
            this.dte_RpDate.TabIndex = 316;
            this.dte_RpDate.Value = new System.DateTime(((long)(0)));
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(51, 250);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 15);
            this.label10.TabIndex = 269;
            this.label10.Text = "Tổng giám đốc";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(78, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 15);
            this.label1.TabIndex = 269;
            this.label1.Text = "BP HCNS";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(47, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 15);
            this.label4.TabIndex = 269;
            this.label4.Text = "Kế toán trưởng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(78, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 15);
            this.label6.TabIndex = 269;
            this.label6.Text = "Người lập";
            // 
            // txt_RpGiamDoc
            // 
            this.txt_RpGiamDoc.Location = new System.Drawing.Point(183, 241);
            this.txt_RpGiamDoc.Name = "txt_RpGiamDoc";
            this.txt_RpGiamDoc.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpGiamDoc.Size = new System.Drawing.Size(282, 26);
            this.txt_RpGiamDoc.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpGiamDoc.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpGiamDoc.StateCommon.Border.Rounding = 4;
            this.txt_RpGiamDoc.StateCommon.Border.Width = 1;
            this.txt_RpGiamDoc.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpGiamDoc.TabIndex = 289;
            // 
            // txt_RpHCNS
            // 
            this.txt_RpHCNS.Location = new System.Drawing.Point(183, 209);
            this.txt_RpHCNS.Name = "txt_RpHCNS";
            this.txt_RpHCNS.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpHCNS.Size = new System.Drawing.Size(282, 26);
            this.txt_RpHCNS.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpHCNS.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpHCNS.StateCommon.Border.Rounding = 4;
            this.txt_RpHCNS.StateCommon.Border.Width = 1;
            this.txt_RpHCNS.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpHCNS.TabIndex = 289;
            // 
            // txt_RpKeToan
            // 
            this.txt_RpKeToan.Location = new System.Drawing.Point(183, 178);
            this.txt_RpKeToan.Name = "txt_RpKeToan";
            this.txt_RpKeToan.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpKeToan.Size = new System.Drawing.Size(282, 26);
            this.txt_RpKeToan.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpKeToan.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpKeToan.StateCommon.Border.Rounding = 4;
            this.txt_RpKeToan.StateCommon.Border.Width = 1;
            this.txt_RpKeToan.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpKeToan.TabIndex = 289;
            // 
            // txt_RpNguoiLap
            // 
            this.txt_RpNguoiLap.Location = new System.Drawing.Point(183, 147);
            this.txt_RpNguoiLap.Name = "txt_RpNguoiLap";
            this.txt_RpNguoiLap.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpNguoiLap.Size = new System.Drawing.Size(282, 26);
            this.txt_RpNguoiLap.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpNguoiLap.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpNguoiLap.StateCommon.Border.Rounding = 4;
            this.txt_RpNguoiLap.StateCommon.Border.Width = 1;
            this.txt_RpNguoiLap.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpNguoiLap.TabIndex = 289;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(94, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 15);
            this.label8.TabIndex = 269;
            this.label8.Text = "Ngày in";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(97, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 15);
            this.label16.TabIndex = 269;
            this.label16.Text = "Tiêu đề";
            // 
            // txt_RpTitle
            // 
            this.txt_RpTitle.Location = new System.Drawing.Point(183, 67);
            this.txt_RpTitle.Multiline = true;
            this.txt_RpTitle.Name = "txt_RpTitle";
            this.txt_RpTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpTitle.Size = new System.Drawing.Size(282, 76);
            this.txt_RpTitle.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpTitle.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpTitle.StateCommon.Border.Rounding = 4;
            this.txt_RpTitle.StateCommon.Border.Width = 1;
            this.txt_RpTitle.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpTitle.TabIndex = 268;
            // 
            // kryptonHeader6
            // 
            this.kryptonHeader6.AutoSize = false;
            this.kryptonHeader6.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose_Panel_Message});
            this.kryptonHeader6.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader6.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader6.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader6.Name = "kryptonHeader6";
            this.kryptonHeader6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader6.Size = new System.Drawing.Size(490, 30);
            this.kryptonHeader6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.kryptonHeader6.TabIndex = 206;
            this.kryptonHeader6.Values.Description = "";
            this.kryptonHeader6.Values.Heading = "!.Thông tin xuất báo cáo";
            this.kryptonHeader6.Values.Image = null;
            // 
            // btnClose_Panel_Message
            // 
            this.btnClose_Panel_Message.Checked = ComponentFactory.Krypton.Toolkit.ButtonCheckState.Checked;
            this.btnClose_Panel_Message.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.True;
            this.btnClose_Panel_Message.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose_Panel_Message.UniqueName = "BEA2AE54CDCE44F263AB208C74E6907A";
            // 
            // btn_Done
            // 
            this.btn_Done.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Done.Location = new System.Drawing.Point(203, 274);
            this.btn_Done.Name = "btn_Done";
            this.btn_Done.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Done.Size = new System.Drawing.Size(119, 40);
            this.btn_Done.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Done.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.TabIndex = 12;
            this.btn_Done.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Done.Values.Image")));
            this.btn_Done.Values.Text = "OK";
            // 
            // Frm_Report_4_V2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 600);
            this.Controls.Add(this.Panel_Done);
            this.Controls.Add(this.GVData);
            this.Controls.Add(this.kryptonHeader2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Report_4_V2";
            this.Text = "Bảng thông tin tổng hợp lương [năng suất]";
            this.Load += new System.EventHandler(this.Frm_Report_4_V2_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_TeamID_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.Panel_Done.ResumeLayout(false);
            this.Panel_Done.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private SYS.TNDateTimePicker dte_ToDate;
        private System.Windows.Forms.Label label2;
        private SYS.TNDateTimePicker dte_FromDate;
        private System.Windows.Forms.Label label3;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_TeamID_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.DataGridView GVData;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Print;
        private System.Windows.Forms.Panel Panel_Done;
        private TN_Tools.TNDateTime dte_RpDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpGiamDoc;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpHCNS;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpKeToan;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpNguoiLap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader6;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose_Panel_Message;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Done;
    }
}