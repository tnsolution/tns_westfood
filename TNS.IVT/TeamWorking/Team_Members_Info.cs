﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.IVT
{
    public class Team_Members_Info
    {
        private int _TeamKey = 0;
        private string _Message = "";
        private string _TeamWorkingKey = "";
        private string _EmployeeKey = "";

        public int TeamKey { get => _TeamKey; set => _TeamKey = value; }
        public string Message { get => _Message; set => _Message = value; }
        public string TeamWorkingKey { get => _TeamWorkingKey; set => _TeamWorkingKey = value; }
        public string EmployeeKey { get => _EmployeeKey; set => _EmployeeKey = value; }

            
        public void Create()
        {
           
            //---------- String SQL Access Database ---------------
            string zSQL = "[dbo].[Team_Member_INSERT]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.NVarChar).Value = _TeamKey;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public string Delete(string TeamWorkingKey, string EmployeeKey)
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FTR_Team_Member WHERE TeamWorkingKey = @TeamWorkingKey AND EmployeeKey = @EmployeeKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TeamWorkingKey);
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Delete(string TeamWorkingKey)
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FTR_Team_Member WHERE TeamWorkingKey = @TeamWorkingKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TeamWorkingKey);
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }


        public string Create_Employee()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO FTR_Team_Member ("
        + " TeamWorkingKey ,EmployeeKey) "
         + " VALUES ( "
         + "@TeamWorkingKey ,@EmployeeKey) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TeamWorkingKey);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
