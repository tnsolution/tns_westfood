﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Team_Members_Default_Data
    {
        public static DataTable List(int TeamKey, string CardID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT COUNT(*) FROM [dbo].[HRM_Team_Member_Default] A
LEFT JOIN[dbo].[HRM_Employee] B ON A.EmployeeKey = B.EmployeeKey
WHERE A.TeamKey = @TeamKey AND B.CardID = @CardID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = CardID;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
