﻿using C1.Win.C1FlexGrid;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_BaoCao_Mau2 : Form
    {
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Parent = "";

        public Frm_BaoCao_Mau2()
        {
            InitializeComponent();
            Load += Frm_BaoCao_Mau2_Load;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Print.Click += Btn_Print_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
            btn_Done.Click += Btn_Done_Click;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
        }

        private void Frm_BaoCao_Mau2_Load(object sender, EventArgs e)
        {

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = 4 AND DepartmentKey!=26  AND RecordStatus< 99", "---- Chọn tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Parent, "SELECT ProductKey, ProductName FROM [dbo].[IVT_Product] WHERE RecordStatus <> 99 AND Parent ='0' ORDER BY Rank ", "--Chọn tất cả--");
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;

            // Khởi tạo thông tin báo cáo
            LoadInfo_Report();
            Panel_Done.Visible = false;
        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cboTeam, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cboTeam.SelectedValue.ToInt() != 0)
            {
                string[] s = cboTeam.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Phân_bổ_tổng_hợp" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cboTeam.SelectedValue.ToInt();
            _Parent = cbo_Parent.SelectedValue.ToString();

            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > BP:" + cbo_Department.Text + " > Nhóm:" + cboTeam.Text + " > Nhóm SP:" + cbo_Parent.Text + " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);


            txt_TitleGrid.Text = "Thông tin chi tiết sản phẩm theo công đoạn, nhóm, ngày thường, chủ nhật, lễ, tổng.";
            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }

        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);

            DataTable zTable = Report.ReportMau2(FromDate, ToDate, _DepartmentKey, _TeamKey, _Parent, txt_Search.Text.Trim());

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "THUONG", "CN", "LE", "TONG" }, "SẢN PHẨM", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Ngày thường", "Chủ nhật", "Ngày lễ", "Tổng" }, 0);

                zTablePivot.Columns.Add("LoaiSP").SetOrdinal(0);

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string loai = zTablePivot.Rows[i][1].ToString().Split('|')[0];
                    zTablePivot.Rows[i][0] = loai;
                }

                string Loai = zTablePivot.Rows[0][0].ToString();
                DataRow[] Array = zTablePivot.Select("LoaiSP='" + Loai + "'");
                if (Array.Length > 0)
                {
                    AddGroup(zTablePivot, Loai, Array, 0);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string temploai = zTablePivot.Rows[i][0].ToString();

                    if (temploai != Loai && temploai.ToUpper() != "TỔNG CỘNG")
                    {
                        Loai = temploai;
                        Array = zTablePivot.Select("LoaiSP='" + Loai + "'");
                        if (Array.Length > 0)
                        {
                            AddGroup(zTablePivot, Loai, Array, i);
                        }
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
        }

        private void AddGroup(DataTable zTablePivot, string Loai, DataRow[] Array, int Index)
        {
            DataTable zGroup = Array.CopyToDataTable();
            DataRow dr = zTablePivot.NewRow();
            dr[0] = Loai;
            dr[1] = Loai;

            for (int c = 2; c < zGroup.Columns.Count; c++)
            {
                float Tong = 0;
                foreach (DataRow r in zGroup.Rows)
                {
                    float ztemp = 0;
                    if (float.TryParse(r[c].ToString(), out ztemp))
                    {

                    }
                    Tong += ztemp;
                }
                dr[c] = Tong;
            }

            zTablePivot.Rows.InsertAt(dr, Index);
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 4;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "Stt";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "Sản phẩm";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "Mã";

            zCellRange = GVData.GetCellRange(0, 3, 1, 3);
            zCellRange.Data = "Đơn vị";

            zCellRange = GVData.GetCellRange(0, 4, 1, 4);
            zCellRange.Data = "Nhóm sản phẩm";

            zCellRange = GVData.GetCellRange(0, 5, 1, 5);
            zCellRange.Data = "Loại sản phẩm";

            //Row Header
            int ColStart = 6;
            for (int i = 2; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    if (strCaption[2].Trim() == "Tổng")
                    {
                        GVData.Rows[0][ColStart] = strCaption[1];
                        GVData.Rows[1][ColStart] = strCaption[2];
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Blue;
                        GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = strCaption[1];
                        GVData.Rows[1][ColStart] = strCaption[2];
                    }
                }
                else
                {
                    if (strCaption[1].Trim() == "Tổng")
                    {
                        GVData.Rows[0][ColStart] = strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[1];
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Red;
                        GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                    }
                    else
                    {
                        GVData.Rows[0][ColStart] = strCaption[0];
                        GVData.Rows[1][ColStart] = strCaption[1];
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Purple;
                    }
                }
                ColStart++;
            }

            ColStart = 1;
            int RowStart = 2;
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {

                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[1].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][0] = zNo;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                    GVData.Rows[rIndex + RowStart][3] = strTemp[3];
                    GVData.Rows[rIndex + RowStart][4] = strTemp[4];
                    GVData.Rows[rIndex + RowStart][5] = strTemp[5];
                    zNo++;
                }
                else
                {
                    zNo = 1;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart].StyleNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                }

                for (int cIndex = 2; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex + 3] = rData[cIndex].Toe0String();
                }
            }


            ////Style
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 450;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 115;
            GVData.Cols[3].AllowMerging = true;
            GVData.Cols[3].Width = 60;
            GVData.Cols[4].AllowMerging = true;
            GVData.Cols[4].Width = 120;
            GVData.Cols[5].AllowMerging = true;
            GVData.Cols[5].Width = 120;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            GVData.Cols[ToTalCol - 4].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 6;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[4].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[5].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;
            GVData.Cols[4].StyleNew.BackColor = Color.Empty;
            GVData.Cols[5].StyleNew.BackColor = Color.Empty;

            for (int i = 6; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 4].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }

        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #region[In]
        private void LoadInfo_Report()
        {
            //Ngày lập
            dte_RpDate.Value = SessionUser.Date_Work;
            //Tên báo cáo
            txt_RpTitle.Text = "Báo cáo phân bổ tổng hợp";
            //Người lập
            txt_RpNguoiLap.Text = SessionUser.UserLogin.EmployeeName;
            //Kế toán trưởng
            txt_RpKeToan.Text = "Trần Tấn Long Thạch";
            //HCNS
            txt_RpHCNS.Text = "Lê Văn Hòa";
            //Tổng giám đốc
            txt_RpGiamDoc.Text = "Nguyễn Vũ Lộc";
        }
        private void Btn_Print_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                Panel_Done.Visible = true;
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu", 1);
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo cáo phân bổ tổng hợp.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    XuatExcelMau(newFile);
                    Process.Start(Path);
                }

            }
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }

        private void XuatExcelMau(FileInfo newFile)
        {
            string TieuDe = txt_RpTitle.Text;
            string Font = "Time New Roman";
            string LapBang = txt_RpNguoiLap.Text;
            string KeToan = txt_RpKeToan.Text;
            string NhanSu = txt_RpHCNS.Text;
            string GiamDoc = txt_RpGiamDoc.Text;

            var fileMau = new FileInfo(Application.StartupPath + @"\FileTemplate\MauBaoCao_So1.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(fileMau))
            {
                ExcelWorksheet workSheet = xlPackage.Workbook.Worksheets.FirstOrDefault();

                //tham số chỉ định tùy chọn
                int ColumnGV = GVData.Cols.Count;  //tổng cột cần hiển thị của Gidview
                int RowGV = GVData.Rows.Count; //số dòng dữ liệu Gidview
                int RowExcel = 6;  //dòng bắt đầu cần ghi dữ liệu trong excel         

                #region 2. DATA DỮ LIỆU                
                for (int i = 1; i <= RowGV; i++)
                {
                    int rowGV = i - 1;
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;

                        object val = GVData.Rows[rowGV][colGV];
                        if (val != null)
                        {
                            workSheet.Cells[RowExcel + i, c].Value = val.ToString();

                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Value = string.Empty;
                        }
                        if (rowGV > 1 && GVData.Rows[rowGV][0] == null)
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Font.Color.SetColor(Color.Navy);
                            workSheet.Cells[RowExcel + i, c].Style.Font.Bold = true;
                        }
                        if (c == 2 || c == 3 || c == 4 || c == 5 || c == 6)
                        { // cot ten nhan vien
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Numberformat.Format = "0.00";
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                }
                #endregion
                #region 3. ĐỊNH DẠNG  DATA EXCEL
                //DÒNG THỨ 7 TRONG EXCEL ĐẾN DÒNG 11 TRONG EXCEL LÀ TIÊU ĐỀ BẢNG
                for (int r = 7; r <= 8; r++)
                {
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;
                        workSheet.Cells[r, c].Style.Font.Name = Font;
                        workSheet.Cells[r, c].Style.Font.Size = 12;
                        workSheet.Cells[r, c].Style.Font.Bold = true;
                        workSheet.Cells[r, c].Style.Font.Color.SetColor(Color.Navy);
                        workSheet.Cells[r, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[r, c].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[r, c].Style.WrapText = true;
                        //workSheet.Column(c).Width = 15;
                        workSheet.Column(c).AutoFit();

                    }
                }

                //DÒNG TỔNG FOR TỪNG CỘT
                for (int c = 1; c <= ColumnGV; c++)
                {
                    int colGV = c - 1;

                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Name = Font;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Size = 12;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Bold = true;
                    workSheet.Cells[RowExcel + RowGV, c].Style.Font.Color.SetColor(Color.Navy);
                    workSheet.Cells[RowExcel + RowGV, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    workSheet.Cells[RowExcel + RowGV, c].Style.WrapText = true;

                    if (c == 2 || c == 3 || c == 4 || c == 5)
                    {
                        workSheet.Cells[RowExcel + RowGV, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                    }
                }

                //CỘT TỔNG
                workSheet.Column(ColumnGV - 3).Style.Font.Bold = true;
                workSheet.Column(ColumnGV - 2).Style.Font.Bold = true;
                workSheet.Column(ColumnGV - 1).Style.Font.Bold = true;
                workSheet.Column(ColumnGV).Style.Font.Bold = true;

                #endregion

                #region[4.Trộn ngang > dọc tự động tiêu đề column báo cáo]       
                MergeAutoExcel(workSheet, 7, 8, 1, ColumnGV);
                workSheet.Row(7).Height = 65; //Độ cao tiêu đề
                workSheet.Row(8).Height = 100; // Độ cao tiêu đề

                //Trộn tuy chình
                //workSheet.Cells[8, ColumnGV, 10, ColumnGV].Merge = true;
                //workSheet.Cells[8, ColumnGV - 1, 10, ColumnGV - 1].Merge = true;
                //workSheet.Cells[8, ColumnGV - 2, 10, ColumnGV - 2].Merge = true;
                #endregion

                #region 1. TÊN BÁO CÁO EXCEL
                //Thêm các dường line excel
                workSheet.Cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                // Dòng Tên báo cáo
                workSheet.Cells[4, 7].Value = TieuDe.ToUpper();
                workSheet.Cells[4, 7].Style.Font.Size = 16;
                workSheet.Cells[4, 7].Style.Font.Name = Font;
                workSheet.Cells[4, 7].Style.Font.Bold = true;
                workSheet.Cells[4, 7].Style.Font.Color.SetColor(Color.Navy);
                workSheet.Cells[4, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                // Xóa các đường line Từ dòng 1 đến dòng tiêu đề
                workSheet.Cells["1:6"].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:5"].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[6.Định dạng màu,độ rộng các cột fit]



                workSheet.Row(7).Height = 45;
                workSheet.Row(8).Height = 65;

                workSheet.Column(1).Width = 7;
                workSheet.Column(1).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(2).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(3).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(4).Style.Font.Color.SetColor(Color.Navy);
                workSheet.Column(5).Style.Font.Color.SetColor(Color.Navy);

                workSheet.View.FreezePanes(10, 5);
                #endregion

                #region[7.Khung chữ ký]
                // Ngày ký
                workSheet.Cells[RowExcel + RowGV + 2, 13].Value = "Ngày " + dte_RpDate.Value.Day + " tháng " + dte_RpDate.Value.Month + " năm " + dte_RpDate.Value.Year;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 2, 13].Style.Font.Color.SetColor(Color.Navy);

                //Tiêu đề Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 3, 2].Value = "Lập bảng";
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Color.SetColor(Color.Black);
                // Tên Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 9, 2].Value = LapBang;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 3, 5].Value = "Kế toán trưởng";
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Color.SetColor(Color.Black);

                //Tên kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 9, 5].Value = KeToan;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề BP HCNS
                workSheet.Cells[RowExcel + RowGV + 3, 9].Value = "BP HCNS";
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 9].Style.Font.Color.SetColor(Color.Black);
                //Tên Trưởng phòng HCNS
                workSheet.Cells[RowExcel + RowGV + 9, 9].Value = NhanSu;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 9].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 3, 13].Value = "Tổng giám đốc";
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 13].Style.Font.Color.SetColor(Color.Black);
                //Tên tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 9, 13].Value = GiamDoc;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 13].Style.Font.Color.SetColor(Color.Black);

                //Xóa đường line các dòng ký tên
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 10)].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[8.Tiện ích Cài đặt in]
                workSheet.PrinterSettings.PaperSize = ePaperSize.A3;//Khổ giấy
                workSheet.PrinterSettings.PrintArea = workSheet.Cells[1, 1, RowExcel + RowGV + 9, ColumnGV];//Khu vực in
                workSheet.PrinterSettings.PageOrder = ePageOrder.OverThenDown;//Kiểu in chữ Z
                workSheet.PrinterSettings.RepeatRows = workSheet.Cells["7:10"];// Tiêu đề lặp lại khi qua trang khác
                //workSheet.PrinterSettings.RepeatColumns = workSheet.Cells["A:B"];// Column lặp lại khi qua trang khác
                workSheet.PrinterSettings.Orientation = eOrientation.Landscape;//Trang giấy nằm ngang
                workSheet.PrinterSettings.FitToPage = false;//In nhiu trang giấy
                workSheet.View.PageBreakView = true;
                workSheet.PrinterSettings.FitToHeight = 0;//Tự động cắt trang
                #endregion

                xlPackage.SaveAs(newFile);
            }
        }
        //trộn dòng cố định
        private void MergeDongExcel(ExcelWorksheet workSheet, int currentRow, int fromCol, int ToCol)
        {
            try
            {
                workSheet.Cells[currentRow, fromCol, currentRow, ToCol].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[currentRow, fromCol + 1, currentRow, ToCol].Merge = true;
            }
        }
        //trộn cột cố định
        private void MergeCotExcel(ExcelWorksheet workSheet, int currentColumn, int fromRow, int toRow)
        {
            try
            {
                workSheet.Cells[fromRow, currentColumn, toRow, currentColumn].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[fromRow + 1, currentColumn, toRow, currentColumn].Merge = true;
            }
        }
        //cấp 3 trộn kết hợp
        private void MergeAutoExcel(ExcelWorksheet workSheet, int fromRow, int toRow, int fromCol, int toCol)
        {
            string Val = workSheet.Cells[fromRow, fromCol].Value.ToString();
            int TronCot = 0;

            #region [Trộn cột :(dòng trên dòng dưới) ]

            for (int r = fromRow; r < toRow; r++)
            {
                for (int c = fromCol + 1; c < toCol; c++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();

                    if (Val == Dta)
                    {
                        TronCot++;
                    }
                    else
                    {
                        if (TronCot >= 1)
                        {
                            try
                            {
                                workSheet.Cells[r, c - TronCot - 1, r, c - 1].Merge = true;
                                TronCot = 0;
                            }
                            catch (Exception)
                            {

                            }
                        }
                        Val = Dta;
                    }
                }

                //trộn cuối cùng 
                if (Val == workSheet.Cells[r, toCol, r, toCol].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (TronCot >= 1)
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol - 1].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }
            #endregion

            #region [Trộn dòng :(cột trái cột phải)]

            for (int c = fromCol; c <= toCol; c++)
            {
                int TronDong = 0;
                Val = workSheet.Cells[fromRow, c].Value.ToString();
                for (int r = fromRow + 1; r <= toRow; r++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();
                    if (Val == Dta)
                    {
                        TronDong++;
                    }
                    else
                    {
                        Val = Dta;
                    }
                }

                if (TronDong > 0)
                {
                    try
                    {
                        workSheet.Cells[fromRow, c, fromRow + TronDong, c].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                if (c == toCol && Val == workSheet.Cells[fromRow, c].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[toRow - TronDong, toCol, toRow, toCol].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
            #endregion
        }
        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
