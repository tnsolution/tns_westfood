﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.HRM
{
    public class Contract_Fee_Info
    {
        private int _AutoKey = 0;
        private string _ContractKey = "";
        private int _CategoryKey = 0;
        private double _Price = 0;
        private float _Quantity = 0;
        private double _Amount = 0;
        private float _PercentFee = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private int _Rank = 0;
        private string _Ext = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";

        #region-----------
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }
        public string ContractKey
        {
            get
            {
                return _ContractKey;
            }

            set
            {
                _ContractKey = value;
            }
        }
        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }
        public double Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }
        public float Quantity
        {
            get
            {
                return _Quantity;
            }

            set
            {
                _Quantity = value;
            }
        }
        public double Amount
        {
            get
            {
                return _Amount;
            }

            set
            {
                _Amount = value;
            }
        }
        public float PercentFee
        {
            get
            {
                return _PercentFee;
            }

            set
            {
                _PercentFee = value;
            }
        }
        public DateTime FromDate
        {
            get
            {
                return _FromDate;
            }

            set
            {
                _FromDate = value;
            }
        }
        public DateTime ToDate
        {
            get
            {
                return _ToDate;
            }

            set
            {
                _ToDate = value;
            }
        }
        public int Rank
        {
            get
            {
                return _Rank;
            }

            set
            {
                _Rank = value;
            }
        }
        public string Ext
        {
            get
            {
                return _Ext;
            }

            set
            {
                _Ext = value;
            }
        }
        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }
        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }
        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }
        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }
        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }
        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }
        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }
        #endregion

        public Contract_Fee_Info()
        {

        }

        public Contract_Fee_Info(DataRow InRow)
        {
            if (InRow["AutoKey"] != DBNull.Value)
                _AutoKey = int.Parse(InRow["AutoKey"].ToString());
            _ContractKey = InRow["ContractKey"].ToString();
            if (InRow["CategoryKey"] != DBNull.Value)
                _CategoryKey = int.Parse(InRow["CategoryKey"].ToString());
            if (InRow["Price"] != DBNull.Value)
                _Price = double.Parse(InRow["Price"].ToString());
            if (InRow["Quantity"] != DBNull.Value)
                _Quantity = float.Parse(InRow["Quantity"].ToString());
            if (InRow["Amount"] != DBNull.Value)
                _Amount = double.Parse(InRow["Amount"].ToString());
            if (InRow["PercentFee"] != DBNull.Value)
                _PercentFee = float.Parse(InRow["PercentFee"].ToString());
            if (InRow["FromDate"] != DBNull.Value)
                _FromDate = (DateTime)InRow["FromDate"];
            if (InRow["ToDate"] != DBNull.Value)
                _ToDate = (DateTime)InRow["ToDate"];
            if (InRow["Rank"] != DBNull.Value)
                _Rank = int.Parse(InRow["Rank"].ToString());
            _Ext = InRow["Ext"].ToString();
            if (InRow["RecordStatus"] != DBNull.Value)
                _RecordStatus = int.Parse(InRow["RecordStatus"].ToString());
            CreatedBy = InRow["CreatedBy"].ToString();
            CreatedName = InRow["CreatedName"].ToString();
            if (InRow["CreatedOn"] != DBNull.Value)
                _CreatedOn = (DateTime)InRow["CreatedOn"];
            if (InRow["ModifiedOn"] != DBNull.Value)
                _ModifiedOn = (DateTime)InRow["ModifiedOn"];
            _ModifiedBy = InRow["ModifiedBy"].ToString();
            _ModifiedName = InRow["ModifiedName"].ToString();
        }

        public Contract_Fee_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Contract_Fee WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _ContractKey = zReader["ContractKey"].ToString();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["Quantity"] != DBNull.Value)
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["PercentFee"] != DBNull.Value)
                        _PercentFee = float.Parse(zReader["PercentFee"].ToString());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["ToDate"] != DBNull.Value)
                        _ToDate = (DateTime)zReader["ToDate"];
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    _Ext = zReader["Ext"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    CreatedBy = zReader["CreatedBy"].ToString();
                    CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public string Create()
        {
            string zSQL = "INSERT INTO HRM_Contract_Fee ("
                + " ContractKey, CategoryKey, Price, Quantity, Amount, PercentFee, FromDate, ToDate, Rank, Ext, RecordStatus)"
                + " VALUES ("
                + " @ContractKey, @CategoryKey, @Price, @Quantity, @Amount, @PercentFee, @FromDate, @ToDate, @Rank, @Ext, @RecordStatus)";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("ContractKey", SqlDbType.NVarChar).Value = _ContractKey;
                zCommand.Parameters.Add("CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("PercentFee", SqlDbType.Float).Value = _PercentFee;
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("Ext", SqlDbType.NVarChar).Value = _Ext;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.NVarChar).Value = _RecordStatus;
                //zCommand.Parameters.Add("CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                //zCommand.Parameters.Add("CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                //zCommand.Parameters.Add("CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                //zCommand.Parameters.Add("ModifiedOn", SqlDbType.DateTime).Value = _ModifiedOn;
                //zCommand.Parameters.Add("ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                //zCommand.Parameters.Add("ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                Message = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = " UPDATE HRM_Contract_Fee SET "
                        + " ContractKey = @ContractKey, "
                        + " CategoryKey = @CategoryKey, "
                        + " Price = @Price, "
                        + " Quantity = @Quantity, "
                        + " Amount = @Amount, "
                        + " PercentFee = @PercentFee,"
                        + " FromDate = @FromDate, "
                        + " ToDate = @ToDate, "
                        + " Rank = @Rank, "
                        + " Ext = @Ext, "
                        + " RecordStatus = @RecordStatus "
                        //+ " CreatedBy = @CreatedBy "
                        //+ " CreatedName = @CreatedName "
                        //+ " CreatedOn = @CreatedOn "
                        //+ " ModifiedOn = @ModifiedOn "
                        //+ " ModifiedBy = @ModifiedBy "
                        //+ " ModifiedName = @ModifiedName "
                        + "WHERE AutoKey = @AutoKey ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_AutoKey > 0)
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                else
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = DBNull.Value;
                zCommand.Parameters.Add("ContractKey", SqlDbType.NVarChar).Value = _ContractKey;
                zCommand.Parameters.Add("CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("PercentFee", SqlDbType.Float).Value = _PercentFee;
                if (_FromDate == DateTime.MinValue)
                    zCommand.Parameters.Add("FromDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("FromDate", SqlDbType.DateTime).Value = _FromDate;
                if (_ToDate == DateTime.MinValue)
                    zCommand.Parameters.Add("ToDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("ToDate", SqlDbType.DateTime).Value = _ToDate;
                zCommand.Parameters.Add("Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("Ext", SqlDbType.NVarChar).Value = _Ext;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.NVarChar).Value = _RecordStatus;
                //zCommand.Parameters.Add("CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                //zCommand.Parameters.Add("CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                //zCommand.Parameters.Add("CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                //zCommand.Parameters.Add("ModifiedOn", SqlDbType.DateTime).Value = _ModifiedOn;
                //zCommand.Parameters.Add("ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                //zCommand.Parameters.Add("ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                Message = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete()
        {
            string zResult = "";
            string zSQL = "DELETE FROM HRM_Contract_Fee  WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete(int CategoryKey)
        {
            string zResult = "";
            string zSQL = "DELETE FROM HRM_Contract_Fee  WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
    }
}
