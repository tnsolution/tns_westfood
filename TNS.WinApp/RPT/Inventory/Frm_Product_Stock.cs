﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Product_Stock : Form
    {
        private DateTime _FromDate;
        private DateTime _ToDate;
        private int _Warehouse = 0;
        private string _ProductID ="";
        public Frm_Product_Stock()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            LoadDataToToolbox.KryptonComboBox(cbo_Warehouse_Search, " SELECT WarehouseKey,WarehouseName AS WarehouseName FROM dbo.IVT_Warehouse WHERE Slug=1 AND RecordStatus<> 99 ORDER BY Rank", "");
        }

        private void Frm_Product_Stock_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
       
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            _Warehouse = cbo_Warehouse_Search.SelectedValue.ToInt();
            _ProductID = txt_Search.Text.Trim();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }

        #region[ListView]

        private void DisplayData()
        {

            DataTable _InTable = Stock_Product.StockProductV2(_FromDate, _ToDate, _Warehouse,_ProductID);
            if (_InTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {

                    InitGV_Layout(_InTable);
                }));
            }

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách nhập xuất tồn.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 8;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã hàng";
            GVData.Rows[0][2] = "Tên hàng";
            GVData.Rows[0][3] = "Đơn vị";
            GVData.Rows[0][4] = "Tồn đầu kì";
            GVData.Rows[0][5] = "Nhập trong kì";
            GVData.Rows[0][6] = "Xuất trong kì";
            GVData.Rows[0][7] = "Tồn cuối kì";

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[0];
                GVData.Rows[rIndex + 1][2] = rData[1];
                GVData.Rows[rIndex + 1][3] = rData[2];
                GVData.Rows[rIndex + 1][4] = rData[3].Toe2String();
                GVData.Rows[rIndex + 1][5] = rData[4].Toe2String();
                GVData.Rows[rIndex + 1][6] = rData[5].Toe2String();
                GVData.Rows[rIndex + 1][7] = rData[6].Toe2String();
                if (double.Parse(rData[3].ToString()) < 0)
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Red;
                if (double.Parse(rData[4].ToString()) < 0)
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Red;
                if (double.Parse(rData[5].ToString()) < 0)
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Red;
                if (double.Parse(rData[6].ToString()) < 0)
                    GVData.Rows[rIndex + 1].StyleNew.ForeColor = Color.Red;

            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 150;
            GVData.Cols[2].Width = 450;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 150;
            GVData.Cols[5].Width = 150;
            GVData.Cols[6].Width = 150;
            GVData.Cols[7].Width = 150;

            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[6].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[7].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
               // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
