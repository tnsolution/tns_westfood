﻿namespace TNS.WinApp
{
    partial class Frm_Employee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Employee));
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.LVData = new System.Windows.Forms.ListView();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cbo_Branch_Search = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbo_Deparment_Search = new System.Windows.Forms.ComboBox();
            this.cbo_Status_Search = new System.Windows.Forms.ComboBox();
            this.cbo_Position_Search = new System.Windows.Forms.ComboBox();
            this.cbo_Team_Search = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.txt_EmployeeName_Search = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lbl_team = new System.Windows.Forms.Label();
            this.txt_IssueID_Search = new System.Windows.Forms.TextBox();
            this.txt_EmployeeID_Search = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.txt_Description = new System.Windows.Forms.TextBox();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btn_Import = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Deny = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Add = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.GVData = new System.Windows.Forms.DataGridView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btn_CheckCardID = new System.Windows.Forms.Button();
            this.dte_LeavingTryDate = new TNS.SYS.TNDateTimePicker();
            this.dte_LeavingDate = new TNS.SYS.TNDateTimePicker();
            this.dte_StartingDate = new TNS.SYS.TNDateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.lbl_LeavingDate = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_AccoutCode = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.txt_AccountCode2 = new System.Windows.Forms.TextBox();
            this.txt_CardID = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txt_ReportyTo = new System.Windows.Forms.TextBox();
            this.lbl_Teams = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbo_WorkingStatus = new System.Windows.Forms.ComboBox();
            this.lbl_Department = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdo_ScoreYes = new System.Windows.Forms.RadioButton();
            this.rdo_ScoreNo = new System.Windows.Forms.RadioButton();
            this.cbo_Department = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdo_Yes = new System.Windows.Forms.RadioButton();
            this.rdo_No = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.cbo_Position = new System.Windows.Forms.ComboBox();
            this.cbo_Team = new System.Windows.Forms.ComboBox();
            this.cbo_Branch = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Modified = new System.Windows.Forms.Label();
            this.lbl_Created = new System.Windows.Forms.Label();
            this.dte_BirthDay = new TNS.SYS.TNDateTimePicker();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.dte_IssueDate = new TNS.SYS.TNDateTimePicker();
            this.txt_FullName = new System.Windows.Forms.TextBox();
            this.btn_CheckIssuedID = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_AddressBank = new System.Windows.Forms.TextBox();
            this.txt_BankCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_CompanyEmail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_CompanyPhone = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txt_IssueID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txt_IssuePlace = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_HomeTown = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdo_Male = new System.Windows.Forms.RadioButton();
            this.rdo_Female = new System.Windows.Forms.RadioButton();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.LV_Contract = new System.Windows.Forms.ListView();
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_IP_Leaving = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Left.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.LVData);
            this.Panel_Left.Controls.Add(this.kryptonHeader5);
            this.Panel_Left.Controls.Add(this.panel4);
            this.Panel_Left.Controls.Add(this.label6);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 42);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(660, 726);
            this.Panel_Left.TabIndex = 1;
            // 
            // LVData
            // 
            this.LVData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVData.FullRowSelect = true;
            this.LVData.GridLines = true;
            this.LVData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LVData.HideSelection = false;
            this.LVData.Location = new System.Drawing.Point(0, 170);
            this.LVData.Name = "LVData";
            this.LVData.Size = new System.Drawing.Size(658, 556);
            this.LVData.TabIndex = 0;
            this.LVData.UseCompatibleStateImageBehavior = false;
            this.LVData.View = System.Windows.Forms.View.Details;
            this.LVData.ItemActivate += new System.EventHandler(this.LV_Employee_ItemActivate);
            this.LVData.Click += new System.EventHandler(this.LV_Employee_Click);
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 140);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(658, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 203;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Danh sách nhân sự";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.btn_Search);
            this.panel4.Controls.Add(this.cbo_Branch_Search);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.cbo_Deparment_Search);
            this.panel4.Controls.Add(this.cbo_Status_Search);
            this.panel4.Controls.Add(this.cbo_Position_Search);
            this.panel4.Controls.Add(this.cbo_Team_Search);
            this.panel4.Controls.Add(this.label12);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.txt_EmployeeName_Search);
            this.panel4.Controls.Add(this.label27);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.label26);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.lbl_team);
            this.panel4.Controls.Add(this.txt_IssueID_Search);
            this.panel4.Controls.Add(this.txt_EmployeeID_Search);
            this.panel4.Controls.Add(this.comboBox2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(658, 140);
            this.panel4.TabIndex = 152;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(555, 47);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(96, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 150;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // cbo_Branch_Search
            // 
            this.cbo_Branch_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Branch_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Branch_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Branch_Search.FormattingEnabled = true;
            this.cbo_Branch_Search.Location = new System.Drawing.Point(63, 12);
            this.cbo_Branch_Search.Name = "cbo_Branch_Search";
            this.cbo_Branch_Search.Size = new System.Drawing.Size(208, 23);
            this.cbo_Branch_Search.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label20.Location = new System.Drawing.Point(275, 75);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 15);
            this.label20.TabIndex = 147;
            this.label20.Text = "Nhân Viên";
            // 
            // cbo_Deparment_Search
            // 
            this.cbo_Deparment_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Deparment_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Deparment_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Deparment_Search.FormattingEnabled = true;
            this.cbo_Deparment_Search.Location = new System.Drawing.Point(63, 41);
            this.cbo_Deparment_Search.Name = "cbo_Deparment_Search";
            this.cbo_Deparment_Search.Size = new System.Drawing.Size(208, 23);
            this.cbo_Deparment_Search.TabIndex = 2;
            // 
            // cbo_Status_Search
            // 
            this.cbo_Status_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Status_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Status_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Status_Search.FormattingEnabled = true;
            this.cbo_Status_Search.Location = new System.Drawing.Point(341, 98);
            this.cbo_Status_Search.Name = "cbo_Status_Search";
            this.cbo_Status_Search.Size = new System.Drawing.Size(208, 23);
            this.cbo_Status_Search.TabIndex = 2;
            // 
            // cbo_Position_Search
            // 
            this.cbo_Position_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Position_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Position_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Position_Search.FormattingEnabled = true;
            this.cbo_Position_Search.Location = new System.Drawing.Point(63, 98);
            this.cbo_Position_Search.Name = "cbo_Position_Search";
            this.cbo_Position_Search.Size = new System.Drawing.Size(208, 23);
            this.cbo_Position_Search.TabIndex = 2;
            // 
            // cbo_Team_Search
            // 
            this.cbo_Team_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Team_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Team_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Team_Search.FormattingEnabled = true;
            this.cbo_Team_Search.Location = new System.Drawing.Point(63, 69);
            this.cbo_Team_Search.Name = "cbo_Team_Search";
            this.cbo_Team_Search.Size = new System.Drawing.Size(208, 23);
            this.cbo_Team_Search.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label12.Location = new System.Drawing.Point(296, 18);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(43, 15);
            this.label12.TabIndex = 147;
            this.label12.Text = "CMND";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label42.Location = new System.Drawing.Point(29, 16);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(32, 15);
            this.label42.TabIndex = 149;
            this.label42.Text = "Khối";
            // 
            // txt_EmployeeName_Search
            // 
            this.txt_EmployeeName_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_EmployeeName_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_EmployeeName_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_EmployeeName_Search.Location = new System.Drawing.Point(341, 69);
            this.txt_EmployeeName_Search.Name = "txt_EmployeeName_Search";
            this.txt_EmployeeName_Search.Size = new System.Drawing.Size(208, 21);
            this.txt_EmployeeName_Search.TabIndex = 0;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label27.Location = new System.Drawing.Point(279, 102);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 15);
            this.label27.TabIndex = 149;
            this.label27.Text = "Tình trạng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label4.Location = new System.Drawing.Point(3, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 149;
            this.label4.Text = "Bộ phận";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label26.Location = new System.Drawing.Point(3, 102);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(52, 15);
            this.label26.TabIndex = 149;
            this.label26.Text = "Chức vụ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(311, 44);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 15);
            this.label15.TabIndex = 147;
            this.label15.Text = "Thẻ";
            // 
            // lbl_team
            // 
            this.lbl_team.AutoSize = true;
            this.lbl_team.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_team.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_team.Location = new System.Drawing.Point(3, 73);
            this.lbl_team.Name = "lbl_team";
            this.lbl_team.Size = new System.Drawing.Size(58, 15);
            this.lbl_team.TabIndex = 149;
            this.lbl_team.Text = "Tổ Nhóm";
            // 
            // txt_IssueID_Search
            // 
            this.txt_IssueID_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_IssueID_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_IssueID_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssueID_Search.Location = new System.Drawing.Point(341, 15);
            this.txt_IssueID_Search.Name = "txt_IssueID_Search";
            this.txt_IssueID_Search.Size = new System.Drawing.Size(208, 21);
            this.txt_IssueID_Search.TabIndex = 0;
            // 
            // txt_EmployeeID_Search
            // 
            this.txt_EmployeeID_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_EmployeeID_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_EmployeeID_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_EmployeeID_Search.Location = new System.Drawing.Point(341, 41);
            this.txt_EmployeeID_Search.Name = "txt_EmployeeID_Search";
            this.txt_EmployeeID_Search.Size = new System.Drawing.Size(208, 21);
            this.txt_EmployeeID_Search.TabIndex = 0;
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.comboBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.comboBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(1368, 232);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(234, 23);
            this.comboBox2.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.DodgerBlue;
            this.label6.Dock = System.Windows.Forms.DockStyle.Right;
            this.label6.Location = new System.Drawing.Point(658, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(2, 726);
            this.label6.TabIndex = 150;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage1.Controls.Add(this.panel10);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(698, 699);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Chi tiết";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel10.Controls.Add(this.txt_Description);
            this.panel10.Controls.Add(this.kryptonHeader4);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 550);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(698, 89);
            this.panel10.TabIndex = 206;
            // 
            // txt_Description
            // 
            this.txt_Description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Description.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Description.Location = new System.Drawing.Point(0, 30);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.Size = new System.Drawing.Size(696, 57);
            this.txt_Description.TabIndex = 203;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(696, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 202;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "Ghi chú";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel8.Controls.Add(this.btn_IP_Leaving);
            this.panel8.Controls.Add(this.btn_Import);
            this.panel8.Controls.Add(this.btn_New);
            this.panel8.Controls.Add(this.btn_Deny);
            this.panel8.Controls.Add(this.btn_Add);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel8.Location = new System.Drawing.Point(0, 639);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(698, 60);
            this.panel8.TabIndex = 205;
            // 
            // btn_Import
            // 
            this.btn_Import.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Import.Location = new System.Drawing.Point(177, 9);
            this.btn_Import.Name = "btn_Import";
            this.btn_Import.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Import.Size = new System.Drawing.Size(132, 40);
            this.btn_Import.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Import.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import.TabIndex = 10;
            this.btn_Import.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Import.Values.Image")));
            this.btn_Import.Values.Text = "Nhập Excel Mới";
            // 
            // btn_New
            // 
            this.btn_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_New.Location = new System.Drawing.Point(315, 9);
            this.btn_New.Name = "btn_New";
            this.btn_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_New.Size = new System.Drawing.Size(120, 40);
            this.btn_New.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.TabIndex = 9;
            this.btn_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_New.Values.Text = "Làm mới";
            // 
            // btn_Deny
            // 
            this.btn_Deny.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Deny.Location = new System.Drawing.Point(567, 9);
            this.btn_Deny.Name = "btn_Deny";
            this.btn_Deny.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Deny.Size = new System.Drawing.Size(120, 40);
            this.btn_Deny.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Deny.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Deny.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Deny.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Deny.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Deny.TabIndex = 7;
            this.btn_Deny.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Deny.Values.Image")));
            this.btn_Deny.Values.Text = "Xóa thông tin";
            // 
            // btn_Add
            // 
            this.btn_Add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Add.Location = new System.Drawing.Point(441, 9);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Add.Size = new System.Drawing.Size(120, 40);
            this.btn_Add.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Add.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Add.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Add.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Add.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Add.TabIndex = 8;
            this.btn_Add.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Add.Values.Image")));
            this.btn_Add.Values.Text = "Cập nhật";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.GVData);
            this.panel2.Controls.Add(this.kryptonHeader1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 457);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(698, 93);
            this.panel2.TabIndex = 147;
            this.panel2.Visible = false;
            // 
            // GVData
            // 
            this.GVData.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GVData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.Location = new System.Drawing.Point(0, 30);
            this.GVData.Name = "GVData";
            this.GVData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData.Size = new System.Drawing.Size(698, 63);
            this.GVData.TabIndex = 0;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(698, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 203;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Địa chỉ";
            // 
            // panel3
            // 
            this.panel3.AutoScroll = true;
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.panel5);
            this.panel3.Controls.Add(this.panel1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(698, 457);
            this.panel3.TabIndex = 204;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.btn_CheckCardID);
            this.panel5.Controls.Add(this.dte_LeavingTryDate);
            this.panel5.Controls.Add(this.dte_LeavingDate);
            this.panel5.Controls.Add(this.dte_StartingDate);
            this.panel5.Controls.Add(this.label18);
            this.panel5.Controls.Add(this.kryptonHeader3);
            this.panel5.Controls.Add(this.lbl_LeavingDate);
            this.panel5.Controls.Add(this.label28);
            this.panel5.Controls.Add(this.label16);
            this.panel5.Controls.Add(this.txt_AccoutCode);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.label10);
            this.panel5.Controls.Add(this.label49);
            this.panel5.Controls.Add(this.txt_AccountCode2);
            this.panel5.Controls.Add(this.txt_CardID);
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.txt_ReportyTo);
            this.panel5.Controls.Add(this.lbl_Teams);
            this.panel5.Controls.Add(this.label25);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.cbo_WorkingStatus);
            this.panel5.Controls.Add(this.lbl_Department);
            this.panel5.Controls.Add(this.groupBox2);
            this.panel5.Controls.Add(this.cbo_Department);
            this.panel5.Controls.Add(this.groupBox1);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.cbo_Position);
            this.panel5.Controls.Add(this.cbo_Team);
            this.panel5.Controls.Add(this.cbo_Branch);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(347, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(359, 438);
            this.panel5.TabIndex = 204;
            // 
            // btn_CheckCardID
            // 
            this.btn_CheckCardID.Location = new System.Drawing.Point(286, 35);
            this.btn_CheckCardID.Name = "btn_CheckCardID";
            this.btn_CheckCardID.Size = new System.Drawing.Size(46, 23);
            this.btn_CheckCardID.TabIndex = 1;
            this.btn_CheckCardID.Text = "...";
            this.btn_CheckCardID.UseVisualStyleBackColor = true;
            // 
            // dte_LeavingTryDate
            // 
            this.dte_LeavingTryDate.CustomFormat = "dd/MM/yyyy";
            this.dte_LeavingTryDate.Location = new System.Drawing.Point(210, 260);
            this.dte_LeavingTryDate.Name = "dte_LeavingTryDate";
            this.dte_LeavingTryDate.Size = new System.Drawing.Size(119, 28);
            this.dte_LeavingTryDate.TabIndex = 7;
            this.dte_LeavingTryDate.Value = new System.DateTime(((long)(0)));
            // 
            // dte_LeavingDate
            // 
            this.dte_LeavingDate.CustomFormat = "dd/MM/yyyy";
            this.dte_LeavingDate.Location = new System.Drawing.Point(210, 288);
            this.dte_LeavingDate.Name = "dte_LeavingDate";
            this.dte_LeavingDate.Size = new System.Drawing.Size(119, 26);
            this.dte_LeavingDate.TabIndex = 7;
            this.dte_LeavingDate.Value = new System.DateTime(((long)(0)));
            // 
            // dte_StartingDate
            // 
            this.dte_StartingDate.CustomFormat = "dd/MM/yyyy";
            this.dte_StartingDate.Location = new System.Drawing.Point(212, 234);
            this.dte_StartingDate.Name = "dte_StartingDate";
            this.dte_StartingDate.Size = new System.Drawing.Size(119, 24);
            this.dte_StartingDate.TabIndex = 7;
            this.dte_StartingDate.Value = new System.DateTime(((long)(0)));
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(80, 265);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 15);
            this.label18.TabIndex = 133;
            this.label18.Text = "Ngày cuối thử việc";
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(357, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 202;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Công tác";
            // 
            // lbl_LeavingDate
            // 
            this.lbl_LeavingDate.AutoSize = true;
            this.lbl_LeavingDate.BackColor = System.Drawing.Color.Transparent;
            this.lbl_LeavingDate.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_LeavingDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_LeavingDate.Location = new System.Drawing.Point(98, 293);
            this.lbl_LeavingDate.Name = "lbl_LeavingDate";
            this.lbl_LeavingDate.Size = new System.Drawing.Size(86, 15);
            this.lbl_LeavingDate.TabIndex = 133;
            this.lbl_LeavingDate.Text = "Ngày nghỉ việc";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label28.Location = new System.Drawing.Point(10, 410);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(90, 15);
            this.label28.TabIndex = 139;
            this.label28.Text = "Loại tính lương";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(-1, 379);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 15);
            this.label16.TabIndex = 139;
            this.label16.Text = "Tăng ca và dư giờ";
            // 
            // txt_AccoutCode
            // 
            this.txt_AccoutCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_AccoutCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_AccoutCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_AccoutCode.Location = new System.Drawing.Point(212, 317);
            this.txt_AccoutCode.Name = "txt_AccoutCode";
            this.txt_AccoutCode.Size = new System.Drawing.Size(118, 21);
            this.txt_AccoutCode.TabIndex = 10;
            this.txt_AccoutCode.Text = "0";
            this.txt_AccoutCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(98, 236);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(87, 15);
            this.label17.TabIndex = 133;
            this.label17.Text = "Ngày bắt đầu *";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label10.Location = new System.Drawing.Point(64, 344);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(114, 15);
            this.label10.TabIndex = 139;
            this.label10.Text = "Tài khoản kế toán 2";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label49.Location = new System.Drawing.Point(4, 66);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(84, 15);
            this.label49.TabIndex = 139;
            this.label49.Text = "Người quản lý";
            // 
            // txt_AccountCode2
            // 
            this.txt_AccountCode2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_AccountCode2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_AccountCode2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_AccountCode2.Location = new System.Drawing.Point(211, 344);
            this.txt_AccountCode2.Name = "txt_AccountCode2";
            this.txt_AccountCode2.Size = new System.Drawing.Size(119, 21);
            this.txt_AccountCode2.TabIndex = 2;
            this.txt_AccountCode2.Text = "0";
            this.txt_AccountCode2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_CardID
            // 
            this.txt_CardID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_CardID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_CardID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_CardID.Location = new System.Drawing.Point(98, 36);
            this.txt_CardID.Name = "txt_CardID";
            this.txt_CardID.Size = new System.Drawing.Size(182, 21);
            this.txt_CardID.TabIndex = 0;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label22.Location = new System.Drawing.Point(64, 320);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(114, 15);
            this.label22.TabIndex = 145;
            this.label22.Text = "Tài khoản kế toán 1";
            // 
            // txt_ReportyTo
            // 
            this.txt_ReportyTo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_ReportyTo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_ReportyTo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ReportyTo.Location = new System.Drawing.Point(98, 63);
            this.txt_ReportyTo.Name = "txt_ReportyTo";
            this.txt_ReportyTo.Size = new System.Drawing.Size(234, 21);
            this.txt_ReportyTo.TabIndex = 2;
            // 
            // lbl_Teams
            // 
            this.lbl_Teams.AutoSize = true;
            this.lbl_Teams.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Teams.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Teams.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Teams.Location = new System.Drawing.Point(35, 152);
            this.lbl_Teams.Name = "lbl_Teams";
            this.lbl_Teams.Size = new System.Drawing.Size(56, 15);
            this.lbl_Teams.TabIndex = 133;
            this.lbl_Teams.Text = "Tổ nhóm";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label25.Location = new System.Drawing.Point(5, 39);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 15);
            this.label25.TabIndex = 137;
            this.label25.Text = "Mã Nhân Viên *";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(57, 93);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 15);
            this.label9.TabIndex = 133;
            this.label9.Text = "Khối";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(39, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 15);
            this.label3.TabIndex = 133;
            this.label3.Text = "Chức vụ";
            // 
            // cbo_WorkingStatus
            // 
            this.cbo_WorkingStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_WorkingStatus.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_WorkingStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_WorkingStatus.FormattingEnabled = true;
            this.cbo_WorkingStatus.Location = new System.Drawing.Point(98, 206);
            this.cbo_WorkingStatus.Name = "cbo_WorkingStatus";
            this.cbo_WorkingStatus.Size = new System.Drawing.Size(234, 23);
            this.cbo_WorkingStatus.TabIndex = 6;
            // 
            // lbl_Department
            // 
            this.lbl_Department.AutoSize = true;
            this.lbl_Department.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Department.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Department.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl_Department.Location = new System.Drawing.Point(38, 122);
            this.lbl_Department.Name = "lbl_Department";
            this.lbl_Department.Size = new System.Drawing.Size(53, 15);
            this.lbl_Department.TabIndex = 133;
            this.lbl_Department.Text = "Bộ phận";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdo_ScoreYes);
            this.groupBox2.Controls.Add(this.rdo_ScoreNo);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.groupBox2.Location = new System.Drawing.Point(110, 398);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(220, 30);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // rdo_ScoreYes
            // 
            this.rdo_ScoreYes.AutoSize = true;
            this.rdo_ScoreYes.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_ScoreYes.Location = new System.Drawing.Point(6, 8);
            this.rdo_ScoreYes.Name = "rdo_ScoreYes";
            this.rdo_ScoreYes.Size = new System.Drawing.Size(70, 19);
            this.rdo_ScoreYes.TabIndex = 0;
            this.rdo_ScoreYes.Text = "Bù công";
            this.rdo_ScoreYes.UseVisualStyleBackColor = true;
            // 
            // rdo_ScoreNo
            // 
            this.rdo_ScoreNo.AutoSize = true;
            this.rdo_ScoreNo.Checked = true;
            this.rdo_ScoreNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_ScoreNo.Location = new System.Drawing.Point(99, 8);
            this.rdo_ScoreNo.Name = "rdo_ScoreNo";
            this.rdo_ScoreNo.Size = new System.Drawing.Size(116, 19);
            this.rdo_ScoreNo.TabIndex = 1;
            this.rdo_ScoreNo.TabStop = true;
            this.rdo_ScoreNo.Text = "Thời gian thực tế";
            this.rdo_ScoreNo.UseVisualStyleBackColor = true;
            // 
            // cbo_Department
            // 
            this.cbo_Department.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Department.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Department.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Department.FormattingEnabled = true;
            this.cbo_Department.Location = new System.Drawing.Point(98, 119);
            this.cbo_Department.Name = "cbo_Department";
            this.cbo_Department.Size = new System.Drawing.Size(234, 23);
            this.cbo_Department.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdo_Yes);
            this.groupBox1.Controls.Add(this.rdo_No);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.groupBox1.Location = new System.Drawing.Point(110, 367);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 30);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // rdo_Yes
            // 
            this.rdo_Yes.AutoSize = true;
            this.rdo_Yes.Checked = true;
            this.rdo_Yes.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_Yes.Location = new System.Drawing.Point(6, 8);
            this.rdo_Yes.Name = "rdo_Yes";
            this.rdo_Yes.Size = new System.Drawing.Size(41, 19);
            this.rdo_Yes.TabIndex = 0;
            this.rdo_Yes.TabStop = true;
            this.rdo_Yes.Text = "Có";
            this.rdo_Yes.UseVisualStyleBackColor = true;
            // 
            // rdo_No
            // 
            this.rdo_No.AutoSize = true;
            this.rdo_No.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_No.Location = new System.Drawing.Point(96, 8);
            this.rdo_No.Name = "rdo_No";
            this.rdo_No.Size = new System.Drawing.Size(61, 19);
            this.rdo_No.TabIndex = 1;
            this.rdo_No.Text = "Không";
            this.rdo_No.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label11.Location = new System.Drawing.Point(29, 210);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 15);
            this.label11.TabIndex = 133;
            this.label11.Text = "Tình trạng";
            // 
            // cbo_Position
            // 
            this.cbo_Position.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Position.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Position.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Position.FormattingEnabled = true;
            this.cbo_Position.Location = new System.Drawing.Point(98, 177);
            this.cbo_Position.Name = "cbo_Position";
            this.cbo_Position.Size = new System.Drawing.Size(234, 23);
            this.cbo_Position.TabIndex = 3;
            // 
            // cbo_Team
            // 
            this.cbo_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Team.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Team.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Team.FormattingEnabled = true;
            this.cbo_Team.Location = new System.Drawing.Point(98, 148);
            this.cbo_Team.Name = "cbo_Team";
            this.cbo_Team.Size = new System.Drawing.Size(234, 23);
            this.cbo_Team.TabIndex = 5;
            // 
            // cbo_Branch
            // 
            this.cbo_Branch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.cbo_Branch.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Branch.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Branch.FormattingEnabled = true;
            this.cbo_Branch.Location = new System.Drawing.Point(98, 90);
            this.cbo_Branch.Name = "cbo_Branch";
            this.cbo_Branch.Size = new System.Drawing.Size(234, 23);
            this.cbo_Branch.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.lbl_Modified);
            this.panel1.Controls.Add(this.lbl_Created);
            this.panel1.Controls.Add(this.dte_BirthDay);
            this.panel1.Controls.Add(this.kryptonHeader2);
            this.panel1.Controls.Add(this.dte_IssueDate);
            this.panel1.Controls.Add(this.txt_FullName);
            this.panel1.Controls.Add(this.btn_CheckIssuedID);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txt_AddressBank);
            this.panel1.Controls.Add(this.txt_BankCode);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txt_CompanyEmail);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.txt_CompanyPhone);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.txt_IssueID);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.txt_IssuePlace);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txt_HomeTown);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(347, 438);
            this.panel1.TabIndex = 203;
            // 
            // lbl_Modified
            // 
            this.lbl_Modified.AutoSize = true;
            this.lbl_Modified.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Modified.Location = new System.Drawing.Point(2, 419);
            this.lbl_Modified.Name = "lbl_Modified";
            this.lbl_Modified.Size = new System.Drawing.Size(68, 14);
            this.lbl_Modified.TabIndex = 283;
            this.lbl_Modified.Text = "Chỉnh sửa: ";
            // 
            // lbl_Created
            // 
            this.lbl_Created.AutoSize = true;
            this.lbl_Created.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Created.Location = new System.Drawing.Point(13, 397);
            this.lbl_Created.Name = "lbl_Created";
            this.lbl_Created.Size = new System.Drawing.Size(56, 14);
            this.lbl_Created.TabIndex = 284;
            this.lbl_Created.Text = "Tạo bởi: ";
            // 
            // dte_BirthDay
            // 
            this.dte_BirthDay.CustomFormat = "dd/MM/yyyy";
            this.dte_BirthDay.Location = new System.Drawing.Point(93, 89);
            this.dte_BirthDay.Name = "dte_BirthDay";
            this.dte_BirthDay.Size = new System.Drawing.Size(148, 25);
            this.dte_BirthDay.TabIndex = 2;
            this.dte_BirthDay.Value = new System.DateTime(((long)(0)));
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(345, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 202;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Cá nhân";
            // 
            // dte_IssueDate
            // 
            this.dte_IssueDate.CustomFormat = "dd/MM/yyyy";
            this.dte_IssueDate.Location = new System.Drawing.Point(93, 200);
            this.dte_IssueDate.Name = "dte_IssueDate";
            this.dte_IssueDate.Size = new System.Drawing.Size(148, 22);
            this.dte_IssueDate.TabIndex = 7;
            this.dte_IssueDate.Value = new System.DateTime(((long)(0)));
            // 
            // txt_FullName
            // 
            this.txt_FullName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_FullName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_FullName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_FullName.Location = new System.Drawing.Point(93, 36);
            this.txt_FullName.Name = "txt_FullName";
            this.txt_FullName.Size = new System.Drawing.Size(237, 21);
            this.txt_FullName.TabIndex = 0;
            // 
            // btn_CheckIssuedID
            // 
            this.btn_CheckIssuedID.Location = new System.Drawing.Point(300, 173);
            this.btn_CheckIssuedID.Name = "btn_CheckIssuedID";
            this.btn_CheckIssuedID.Size = new System.Drawing.Size(31, 23);
            this.btn_CheckIssuedID.TabIndex = 6;
            this.btn_CheckIssuedID.Text = "...";
            this.btn_CheckIssuedID.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(25, 203);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 15);
            this.label8.TabIndex = 133;
            this.label8.Text = "Ngày cấp *";
            // 
            // txt_AddressBank
            // 
            this.txt_AddressBank.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_AddressBank.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_AddressBank.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_AddressBank.Location = new System.Drawing.Point(93, 339);
            this.txt_AddressBank.Multiline = true;
            this.txt_AddressBank.Name = "txt_AddressBank";
            this.txt_AddressBank.Size = new System.Drawing.Size(237, 43);
            this.txt_AddressBank.TabIndex = 10;
            // 
            // txt_BankCode
            // 
            this.txt_BankCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_BankCode.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_BankCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_BankCode.Location = new System.Drawing.Point(93, 311);
            this.txt_BankCode.Name = "txt_BankCode";
            this.txt_BankCode.Size = new System.Drawing.Size(237, 21);
            this.txt_BankCode.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(40, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 15);
            this.label2.TabIndex = 131;
            this.label2.Text = "CMND *";
            // 
            // txt_CompanyEmail
            // 
            this.txt_CompanyEmail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_CompanyEmail.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_CompanyEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_CompanyEmail.Location = new System.Drawing.Point(93, 284);
            this.txt_CompanyEmail.Name = "txt_CompanyEmail";
            this.txt_CompanyEmail.Size = new System.Drawing.Size(237, 21);
            this.txt_CompanyEmail.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(33, 231);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(58, 15);
            this.label13.TabIndex = 131;
            this.label13.Text = "Nơi cấp *";
            // 
            // txt_CompanyPhone
            // 
            this.txt_CompanyPhone.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_CompanyPhone.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_CompanyPhone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_CompanyPhone.Location = new System.Drawing.Point(93, 256);
            this.txt_CompanyPhone.Name = "txt_CompanyPhone";
            this.txt_CompanyPhone.Size = new System.Drawing.Size(237, 21);
            this.txt_CompanyPhone.TabIndex = 8;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label21.Location = new System.Drawing.Point(15, 342);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(68, 15);
            this.label21.TabIndex = 145;
            this.label21.Text = "Ngân hàng";
            // 
            // txt_IssueID
            // 
            this.txt_IssueID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_IssueID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_IssueID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssueID.Location = new System.Drawing.Point(93, 173);
            this.txt_IssueID.Name = "txt_IssueID";
            this.txt_IssueID.Size = new System.Drawing.Size(200, 21);
            this.txt_IssueID.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(52, 314);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 15);
            this.label1.TabIndex = 145;
            this.label1.Text = "ATM";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label14.Location = new System.Drawing.Point(34, 125);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 15);
            this.label14.TabIndex = 131;
            this.label14.Text = "Địa chỉ *";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label23.Location = new System.Drawing.Point(3, 259);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 15);
            this.label23.TabIndex = 146;
            this.label23.Text = "Số điện thoại";
            // 
            // txt_IssuePlace
            // 
            this.txt_IssuePlace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_IssuePlace.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_IssuePlace.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_IssuePlace.Location = new System.Drawing.Point(93, 228);
            this.txt_IssuePlace.Name = "txt_IssuePlace";
            this.txt_IssuePlace.Size = new System.Drawing.Size(237, 21);
            this.txt_IssuePlace.TabIndex = 3;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label24.Location = new System.Drawing.Point(42, 287);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(42, 15);
            this.label24.TabIndex = 145;
            this.label24.Text = "Email ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label7.Location = new System.Drawing.Point(30, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 15);
            this.label7.TabIndex = 133;
            this.label7.Text = "Giới tính *";
            // 
            // txt_HomeTown
            // 
            this.txt_HomeTown.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_HomeTown.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_HomeTown.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_HomeTown.Location = new System.Drawing.Point(93, 119);
            this.txt_HomeTown.Multiline = true;
            this.txt_HomeTown.Name = "txt_HomeTown";
            this.txt_HomeTown.Size = new System.Drawing.Size(237, 48);
            this.txt_HomeTown.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(40, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 15);
            this.label5.TabIndex = 127;
            this.label5.Text = "Họ tên *";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label19.Location = new System.Drawing.Point(21, 92);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 15);
            this.label19.TabIndex = 138;
            this.label19.Text = "Ngày sinh *";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.rdo_Male);
            this.groupBox4.Controls.Add(this.rdo_Female);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.groupBox4.Location = new System.Drawing.Point(93, 54);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(148, 30);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            // 
            // rdo_Male
            // 
            this.rdo_Male.AutoSize = true;
            this.rdo_Male.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_Male.Location = new System.Drawing.Point(6, 8);
            this.rdo_Male.Name = "rdo_Male";
            this.rdo_Male.Size = new System.Drawing.Size(52, 19);
            this.rdo_Male.TabIndex = 0;
            this.rdo_Male.TabStop = true;
            this.rdo_Male.Text = "Nam";
            this.rdo_Male.UseVisualStyleBackColor = true;
            // 
            // rdo_Female
            // 
            this.rdo_Female.AutoSize = true;
            this.rdo_Female.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic);
            this.rdo_Female.Location = new System.Drawing.Point(93, 8);
            this.rdo_Female.Name = "rdo_Female";
            this.rdo_Female.Size = new System.Drawing.Size(42, 19);
            this.rdo_Female.TabIndex = 1;
            this.rdo_Female.TabStop = true;
            this.rdo_Female.Text = "Nữ";
            this.rdo_Female.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(660, 42);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(706, 726);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.tabPage3.Controls.Add(this.panel9);
            this.tabPage3.Location = new System.Drawing.Point(4, 23);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(698, 699);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Hợp đồng";
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel6);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(692, 693);
            this.panel9.TabIndex = 163;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.LV_Contract);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(692, 693);
            this.panel6.TabIndex = 161;
            // 
            // LV_Contract
            // 
            this.LV_Contract.BackColor = System.Drawing.SystemColors.Info;
            this.LV_Contract.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Contract.GridLines = true;
            this.LV_Contract.HideSelection = false;
            this.LV_Contract.Location = new System.Drawing.Point(0, 0);
            this.LV_Contract.Name = "LV_Contract";
            this.LV_Contract.Size = new System.Drawing.Size(692, 693);
            this.LV_Contract.TabIndex = 160;
            this.LV_Contract.UseCompatibleStateImageBehavior = false;
            this.LV_Contract.View = System.Windows.Forms.View.Details;
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1366, 42);
            this.HeaderControl.TabIndex = 3;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Thông tin nhân viên";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            this.HeaderControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.HeaderControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.HeaderControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btn_IP_Leaving
            // 
            this.btn_IP_Leaving.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_IP_Leaving.Location = new System.Drawing.Point(36, 9);
            this.btn_IP_Leaving.Name = "btn_IP_Leaving";
            this.btn_IP_Leaving.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_IP_Leaving.Size = new System.Drawing.Size(135, 40);
            this.btn_IP_Leaving.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_IP_Leaving.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_IP_Leaving.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_IP_Leaving.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_IP_Leaving.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_IP_Leaving.TabIndex = 10;
            this.btn_IP_Leaving.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton1.Values.Image")));
            this.btn_IP_Leaving.Values.Text = "Nhập Nghỉ việc";
            // 
            // Frm_Employee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Employee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "THÔN TIN NHÂN VIÊN";
            this.Load += new System.EventHandler(this.Frm_Employees_Load);
            this.Panel_Left.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_EmployeeName_Search;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txt_CompanyEmail;
        private System.Windows.Forms.TextBox txt_CompanyPhone;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txt_HomeTown;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rdo_Male;
        private System.Windows.Forms.RadioButton rdo_Female;
        private System.Windows.Forms.TextBox txt_FullName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_IssuePlace;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txt_IssueID;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txt_CardID;
        private System.Windows.Forms.ComboBox cbo_Department;
        private System.Windows.Forms.ComboBox cbo_Position;
        private System.Windows.Forms.ComboBox cbo_Branch;
        private System.Windows.Forms.ComboBox cbo_Team;
        private System.Windows.Forms.Label lbl_Department;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbl_Teams;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox txt_ReportyTo;
        private System.Windows.Forms.ComboBox cbo_Team_Search;
        private System.Windows.Forms.Label lbl_team;
        private System.Windows.Forms.ComboBox cbo_Branch_Search;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.DataGridView GVData;
        private System.Windows.Forms.TextBox txt_BankCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ListView LV_Contract;
        private System.Windows.Forms.ListView LVData;
        private System.Windows.Forms.Button btn_CheckCardID;
        private System.Windows.Forms.Button btn_CheckIssuedID;
        private TNS.SYS.TNDateTimePicker dte_IssueDate;
        private TNS.SYS.TNDateTimePicker dte_BirthDay;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cbo_WorkingStatus;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txt_EmployeeID_Search;
        private System.Windows.Forms.TextBox txt_IssueID_Search;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Deny;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Add;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_New;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Import;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox txt_Description;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private SYS.TNDateTimePicker dte_LeavingDate;
        private SYS.TNDateTimePicker dte_StartingDate;
        private System.Windows.Forms.Label lbl_LeavingDate;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txt_AccoutCode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_AccountCode2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txt_AddressBank;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdo_Yes;
        private System.Windows.Forms.RadioButton rdo_No;
        private System.Windows.Forms.ComboBox cbo_Deparment_Search;
        private System.Windows.Forms.ComboBox cbo_Status_Search;
        private System.Windows.Forms.ComboBox cbo_Position_Search;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdo_ScoreYes;
        private System.Windows.Forms.RadioButton rdo_ScoreNo;
        private System.Windows.Forms.Label lbl_Modified;
        private System.Windows.Forms.Label lbl_Created;
        private SYS.TNDateTimePicker dte_LeavingTryDate;
        private System.Windows.Forms.Label label18;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_IP_Leaving;
    }
}