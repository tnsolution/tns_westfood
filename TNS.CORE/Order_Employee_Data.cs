﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Order_Employee_Data
    {
        public static DataTable List(string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT A.*,B.* FROM FTR_Order_Employee A " +
                "LEFT JOIN SYS_Personal B ON A.EmployeeKey = B.ParentKey " +
                "WHERE OrderKey = @OrderKey AND A.RecordStatus < 99" +
                "ORDER BY LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey",SqlDbType.NVarChar).Value  = OrderKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Sum_Product(string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT ROUND(SUM(Basket),1) AS Basket,ROUND(SUM(Kilogram),1) AS Kilogram,ROUND(SUM(Time),1) AS Time " +
                "FROM [dbo].[FTR_Order_Employee] WHERE OrderKey = @OrderKey AND RecordStatus < 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static int Find_Employee(string OrderKey, string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM FTR_Order_Employee WHERE OrderKey = @OrderKey AND EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
    }
}
