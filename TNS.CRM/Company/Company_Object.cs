﻿using System.Collections.Generic;
using System.Data;
using TNS.LOC;

namespace TNS.CRM
{
    public class Company_Object : Company_Info
    {

        private Address_Info _Address;
        public Address_Info Address
        {
            get
            {
                return _Address;
            }

            set
            {
                _Address = value;
            }
        }

        private Contact_Person_Object _Contact;
        public Contact_Person_Object Contact
        {
            get
            {
                return _Contact;
            }

            set
            {
                _Contact = value;
            }
        }
        private List<Address_Info> _List_Address;
        public List<Address_Info> List_Address
        {
            get
            {
                return _List_Address;
            }

            set
            {
                _List_Address = value;
            }
        }

        private List<Contact_Person_Object> _List_Contact;
        public List<Contact_Person_Object> List_Contact
        {
            get
            {
                return _List_Contact;
            }

            set
            {
                _List_Contact = value;
            }
        }

        public Company_Object() { }

        public Company_Object(string ParentKey) : base(ParentKey)
        {
            DataTable zTable = Address_Data.List_Address(base.Key);
            List_Address = new List<Address_Info>();
            foreach (DataRow nRow in zTable.Rows)
            {
                Address_Info zAddress = new Address_Info(nRow);
                List_Address.Add(zAddress);
            }

            zTable = Contact_Data.List(base.Key);
            List_Contact = new List<Contact_Person_Object>();
            foreach (DataRow nRow in zTable.Rows)
            {
                Contact_Person_Object zContact = new Contact_Person_Object(nRow);
                List_Contact.Add(zContact);
            }
        }

        public void Save_Object()
        {
            string zResult = "";
            base.Save();
            if (List_Address != null)
            {
                for (int i = 0; i < List_Address.Count; i++)
                {
                    Address = (Address_Info)List_Address[i];
                    Address.ParentKey = base.Key;
                    Address.ParentTable = "SYS_Person";
                    Address.ModifiedBy = base.ModifiedBy;
                    Address.ModifiedName = base.ModifiedName;
                    switch (Address.RecordStatus)
                    {
                        case 99:
                            Address.DeleteDetail();
                            break;
                        case 1:
                            Address.Create();
                            break;
                        case 2:
                            Address.Update();
                            break;
                    }
                }
                base.Message += zResult;
            }
           
            if (List_Contact != null)
            {
                for (int i = 0; i < List_Contact.Count; i++)
                {
                    _Contact = (Contact_Person_Object)List_Contact[i];
                    _Contact.CompanyKey = base.Key;                   
                    _Contact.ModifiedBy = base.ModifiedBy;
                    _Contact.ModifiedName = base.ModifiedName;
                    switch (Contact.RecordStatus)
                    {
                        case 99:
                            Contact.Delete_Detail_Object();
                            break;

                        case 1:
                            Contact.Save_Object();
                            break;
                        case 2:
                            string a=_Contact.Phone;
                            Contact.Update_Object();
                            break;
                    }
                }
                base.Message += zResult;
            }
            
        }
        public void Delete_Object()
        {
            base.Delete();
            Address_Info zInfo = new Address_Info();
            zInfo.ModifiedBy = base.ModifiedBy;
            zInfo.ModifiedName = base.ModifiedName;
            zInfo.ParentKey = base.Key;
            zInfo.Delete();

            Contact_Person_Object zContact = new Contact_Person_Object();
            zContact.ModifiedName = base.ModifiedName;
            zContact.ModifiedName = base.ModifiedName;
            zContact.CompanyKey = base.Key;
            zContact.Delete();
        }
    }
}
