﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.CORE
{
    public class Order_Employee_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _OrderDate;
        private string _EmployeeKey = "";
        private string _FullName = "";
        private string _EmployeeID = "";
        private string _OrderKey = "";
        private float _Basket;
        private float _Kilogram;
        private float _Time;
        private int _Borrow = 0;
        private int _Share = 0;
        private int _Private = 0;
        private double _Money = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public float Basket
        {
            get { return _Basket; }
            set { _Basket = value; }
        }
        public float Kilogram
        {
            get { return _Kilogram; }
            set { _Kilogram = value; }
        }
        public float Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
        public int Borrow
        {
            get { return _Borrow; }
            set { _Borrow = value; }
        }
        public int Share
        {
            get { return _Share; }
            set { _Share = value; }
        }
        public int Private
        {
            get { return _Private; }
            set { _Private = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string EmployeeID
        {
            get
            {
                return _EmployeeID;
            }

            set
            {
                _EmployeeID = value;
            }
        }

        public string FullName
        {
            get
            {
                return _FullName;
            }

            set
            {
                _FullName = value;
            }
        }

        public int Key
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public double Money
        {
            get
            {
                return _Money;
            }

            set
            {
                _Money = value;
            }
        }

        public DateTime OrderDate
        {
            get
            {
                return _OrderDate;
            }

            set
            {
                _OrderDate = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Order_Employee_Info()
        {
        }
        public Order_Employee_Info(string OrderKey)
        {
            string zSQL = "SELECT * FROM FTR_Order_Employee WHERE OrderKey = @OrderKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["CardID"].ToString().Trim();
                    _OrderKey = zReader["OrderKey"].ToString().Trim();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["Basket"] != DBNull.Value)
                        _Basket = float.Parse(zReader["Basket"].ToString());
                    if (zReader["Kilogram"] != DBNull.Value)
                        _Kilogram = float.Parse(zReader["Kilogram"].ToString());
                    if (zReader["Time"] != DBNull.Value)
                        _Time = float.Parse(zReader["Time"].ToString());
                    if (zReader["Borrow"] != DBNull.Value)
                        _Borrow = int.Parse(zReader["Borrow"].ToString());
                    if (zReader["Share"] != DBNull.Value)
                        _Share = int.Parse(zReader["Share"].ToString());
                    if (zReader["Private"] != DBNull.Value)
                        _Private = int.Parse(zReader["Private"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Order_Employee_Info(string EmployeeKey,string OrderKey)
        {
            string zSQL = "SELECT * FROM FTR_Order_Employee WHERE EmployeeKey = @EmployeeKey AND OrderKey = @OrderKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["CardID"].ToString().Trim();
                    _OrderKey = zReader["OrderKey"].ToString().Trim();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["Basket"] != DBNull.Value)
                        _Basket = float.Parse(zReader["Basket"].ToString());
                    if (zReader["Kilogram"] != DBNull.Value)
                        _Kilogram = float.Parse(zReader["Kilogram"].ToString());
                    if (zReader["Time"] != DBNull.Value)
                        _Time = float.Parse(zReader["Time"].ToString());
                    if (zReader["Borrow"] != DBNull.Value)
                        _Borrow = int.Parse(zReader["Borrow"].ToString());
                    if (zReader["Share"] != DBNull.Value)
                        _Share = int.Parse(zReader["Share"].ToString());
                    if (zReader["Private"] != DBNull.Value)
                        _Private = int.Parse(zReader["Private"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Order_Employee_Info(DataRow nRow)
        {
            if (nRow["AuToKey"] != DBNull.Value)
                _AutoKey = int.Parse(nRow["AuToKey"].ToString());
            _EmployeeKey = nRow["EmployeeKey"].ToString().Trim();
            _FullName = nRow["FullName"].ToString().Trim();
            _EmployeeID = nRow["CardID"].ToString().Trim();
            _OrderKey = nRow["OrderKey"].ToString().Trim();
            if (nRow["OrderDate"] != DBNull.Value)
                _OrderDate = (DateTime)nRow["OrderDate"];
            if (nRow["Basket"] != DBNull.Value)
                _Basket = float.Parse(nRow["Basket"].ToString());
            if (nRow["Kilogram"] != DBNull.Value)
                _Kilogram = float.Parse(nRow["Kilogram"].ToString());
            if (nRow["Time"] != DBNull.Value)
                _Time = float.Parse(nRow["Time"].ToString());
            if (nRow["Borrow"] != DBNull.Value)
                _Borrow = int.Parse(nRow["Borrow"].ToString());
            if (nRow["Share"] != DBNull.Value)
                _Share = int.Parse(nRow["Share"].ToString());
            if (nRow["Private"] != DBNull.Value)
                _Private = int.Parse(nRow["Private"].ToString());
            if (nRow["RecordStatus"] != DBNull.Value)
                _RecordStatus = int.Parse(nRow["RecordStatus"].ToString());
            if (nRow["CreatedOn"] != DBNull.Value)
                _CreatedOn = (DateTime)nRow["CreatedOn"];
            _CreatedBy = nRow["CreatedBy"].ToString().Trim();
            _CreatedName = nRow["CreatedName"].ToString().Trim();
            if (nRow["ModifiedOn"] != DBNull.Value)
                _ModifiedOn = (DateTime)nRow["ModifiedOn"];
            _ModifiedBy = nRow["ModifiedBy"].ToString().Trim();
            _ModifiedName = nRow["ModifiedName"].ToString().Trim();
        }

        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_Employee_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey.Trim();
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = _Basket;
                zCommand.Parameters.Add("@Kilogram", SqlDbType.Float).Value = _Kilogram;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                zCommand.Parameters.Add("@Borrow", SqlDbType.Int).Value = _Borrow;
                zCommand.Parameters.Add("@Share", SqlDbType.Int).Value = _Share;
                zCommand.Parameters.Add("@Private", SqlDbType.Int).Value = _Private;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "FTR_Order_Employee_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AuToKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey.Trim();
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = _Basket;
                zCommand.Parameters.Add("@Kilogram", SqlDbType.Float).Value = _Kilogram;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                zCommand.Parameters.Add("@Borrow", SqlDbType.Int).Value = _Borrow;
                zCommand.Parameters.Add("@Share", SqlDbType.Int).Value = _Share;
                zCommand.Parameters.Add("@Private", SqlDbType.Int).Value = _Private;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_Employee_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //Bổ sung thêm function tính thời gian phan bổ 19-05-2020 //ThoiGian_TinhToanLai = dbo.CHIA_THOI_GIAN_CONGVIEC(EmployeeKey, OrderKey, [Time]) 
        public string UpdateMoney()
        {
            string zSQL = @"UPDATE FTR_Order_Employee 
SET Money = @Money, 
ThoiGian_TinhToanLai = dbo.CHIA_THOI_GIAN_CONGVIEC(EmployeeKey, OrderKey, [Time]) -----bo sung
WHERE EmployeeKey = @EmployeeKey AND OrderKey = @OrderKey AND RecordStatus < 99";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey.Trim();
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete_All()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_Employee_DELETE_ALL";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
