﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TNS.CORE
{
    public class Extend_Object:Order_Extend_Info
    {
        private List<Order_Extend_Employee_Info> _List_Employee;
        public List<Order_Extend_Employee_Info> List_Employee
        {
            get
            {
                return _List_Employee;
            }

            set
            {
                _List_Employee = value;
            }
        }

        public Extend_Object() : base()
        {
            _List_Employee = new List<Order_Extend_Employee_Info>();
        }
        //public Extend_Object(string ExtendKey) : base(ExtendKey)
        //{
        //    //--Order
        //    DataTable zTable = Order_Rate_Data.List(OrderKey);
        //    _List_Rate = new List<Order_Rate_Info>();
        //    foreach (DataRow nRow in zTable.Rows)
        //    {
        //        Order_Rate_Info zOrder = new Order_Rate_Info(nRow);
        //        _List_Rate.Add(zOrder);
        //    }
        //    //--Order_Employee
        //    zTable = Order_Employee_Data.List(OrderKey);
        //    _List_Employee = new List<Order_Employee_Info>();
        //    foreach (DataRow nRow in zTable.Rows)
        //    {
        //        Order_Employee_Info Order_Employee = new Order_Employee_Info(nRow);
        //        _List_Employee.Add(Order_Employee);
        //    }
        //}
    }
}
