﻿namespace TNS.WinApp
{
    partial class Frm_Production_Money
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Production_Money));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.Panel_Right_2 = new System.Windows.Forms.Panel();
            this.GVData2 = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Count_Month = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonHeader10 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonHeader9 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label4 = new System.Windows.Forms.Label();
            this.Panel_Left_2 = new System.Windows.Forms.Panel();
            this.Panel_Team_2 = new System.Windows.Forms.Panel();
            this.LVData2 = new System.Windows.Forms.ListView();
            this.kryptonHeader8 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.kryptonHeader7 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_Money_Month = new System.Windows.Forms.TextBox();
            this.Panel_Search_2 = new System.Windows.Forms.Panel();
            this.dte_Month = new TNS.SYS.TNDateTimePicker();
            this.kryptonHeader6 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dte_Teams = new TNS.SYS.TNDateTimePicker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.GVData1 = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Count_Date = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save_Date = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label6 = new System.Windows.Forms.Label();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_ToTalDifGeneral = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_ToTalTimeDifGeneral = new System.Windows.Forms.TextBox();
            this.txt_MedTimeDifGeneral = new System.Windows.Forms.TextBox();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label47 = new System.Windows.Forms.Label();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.Panel_Detail = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label41 = new System.Windows.Forms.Label();
            this.txt_ToTal_Money = new System.Windows.Forms.TextBox();
            this.txt_ToTal_Time = new System.Windows.Forms.TextBox();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.LVData1 = new System.Windows.Forms.ListView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.tabPage2.SuspendLayout();
            this.Panel_Right_2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData2)).BeginInit();
            this.panel4.SuspendLayout();
            this.Panel_Left_2.SuspendLayout();
            this.Panel_Team_2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.Panel_Search_2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.Panel_Right.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData1)).BeginInit();
            this.panel2.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            this.Panel_Detail.SuspendLayout();
            this.Panel_Team.SuspendLayout();
            this.Panel_Search.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage2.Controls.Add(this.Panel_Right_2);
            this.tabPage2.Controls.Add(this.Panel_Left_2);
            this.tabPage2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1258, 700);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "THÁNG";
            // 
            // Panel_Right_2
            // 
            this.Panel_Right_2.Controls.Add(this.GVData2);
            this.Panel_Right_2.Controls.Add(this.panel4);
            this.Panel_Right_2.Controls.Add(this.kryptonHeader9);
            this.Panel_Right_2.Controls.Add(this.label4);
            this.Panel_Right_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right_2.Location = new System.Drawing.Point(303, 3);
            this.Panel_Right_2.Name = "Panel_Right_2";
            this.Panel_Right_2.Size = new System.Drawing.Size(952, 694);
            this.Panel_Right_2.TabIndex = 203;
            // 
            // GVData2
            // 
            this.GVData2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData2.Location = new System.Drawing.Point(2, 30);
            this.GVData2.Name = "GVData2";
            this.GVData2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData2.Size = new System.Drawing.Size(950, 546);
            this.GVData2.TabIndex = 208;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.btn_Count_Month);
            this.panel4.Controls.Add(this.kryptonHeader10);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(2, 576);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(950, 118);
            this.panel4.TabIndex = 207;
            // 
            // btn_Count_Month
            // 
            this.btn_Count_Month.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count_Month.Location = new System.Drawing.Point(816, 68);
            this.btn_Count_Month.Name = "btn_Count_Month";
            this.btn_Count_Month.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Count_Month.Size = new System.Drawing.Size(120, 40);
            this.btn_Count_Month.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count_Month.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count_Month.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count_Month.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Count_Month.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count_Month.TabIndex = 205;
            this.btn_Count_Month.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Count_Month.Values.Image")));
            this.btn_Count_Month.Values.Text = "Tính toán";
            // 
            // kryptonHeader10
            // 
            this.kryptonHeader10.AutoSize = false;
            this.kryptonHeader10.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader10.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader10.Name = "kryptonHeader10";
            this.kryptonHeader10.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader10.Size = new System.Drawing.Size(950, 30);
            this.kryptonHeader10.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader10.TabIndex = 200;
            this.kryptonHeader10.Values.Description = "";
            this.kryptonHeader10.Values.Heading = "";
            // 
            // kryptonHeader9
            // 
            this.kryptonHeader9.AutoSize = false;
            this.kryptonHeader9.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader9.Location = new System.Drawing.Point(2, 0);
            this.kryptonHeader9.Name = "kryptonHeader9";
            this.kryptonHeader9.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader9.Size = new System.Drawing.Size(950, 30);
            this.kryptonHeader9.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader9.TabIndex = 200;
            this.kryptonHeader9.Values.Description = "";
            this.kryptonHeader9.Values.Heading = "Thông tin công nhân";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.DodgerBlue;
            this.label4.Dock = System.Windows.Forms.DockStyle.Left;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(2, 694);
            this.label4.TabIndex = 209;
            // 
            // Panel_Left_2
            // 
            this.Panel_Left_2.Controls.Add(this.Panel_Team_2);
            this.Panel_Left_2.Controls.Add(this.panel3);
            this.Panel_Left_2.Controls.Add(this.Panel_Search_2);
            this.Panel_Left_2.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left_2.Location = new System.Drawing.Point(3, 3);
            this.Panel_Left_2.Name = "Panel_Left_2";
            this.Panel_Left_2.Size = new System.Drawing.Size(300, 694);
            this.Panel_Left_2.TabIndex = 139;
            // 
            // Panel_Team_2
            // 
            this.Panel_Team_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team_2.Controls.Add(this.LVData2);
            this.Panel_Team_2.Controls.Add(this.kryptonHeader8);
            this.Panel_Team_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Team_2.Location = new System.Drawing.Point(0, 66);
            this.Panel_Team_2.Name = "Panel_Team_2";
            this.Panel_Team_2.Size = new System.Drawing.Size(300, 510);
            this.Panel_Team_2.TabIndex = 207;
            // 
            // LVData2
            // 
            this.LVData2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVData2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVData2.FullRowSelect = true;
            this.LVData2.GridLines = true;
            this.LVData2.HideSelection = false;
            this.LVData2.Location = new System.Drawing.Point(0, 30);
            this.LVData2.Name = "LVData2";
            this.LVData2.Size = new System.Drawing.Size(300, 480);
            this.LVData2.TabIndex = 201;
            this.LVData2.UseCompatibleStateImageBehavior = false;
            this.LVData2.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader8
            // 
            this.kryptonHeader8.AutoSize = false;
            this.kryptonHeader8.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader8.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader8.Name = "kryptonHeader8";
            this.kryptonHeader8.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader8.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader8.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader8.TabIndex = 200;
            this.kryptonHeader8.Values.Description = "";
            this.kryptonHeader8.Values.Heading = "Chọn nhóm";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.kryptonHeader7);
            this.panel3.Controls.Add(this.txt_Money_Month);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 576);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(300, 118);
            this.panel3.TabIndex = 206;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(50, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 15);
            this.label1.TabIndex = 144;
            this.label1.Text = "Tổng Số Tiền";
            // 
            // kryptonHeader7
            // 
            this.kryptonHeader7.AutoSize = false;
            this.kryptonHeader7.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader7.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader7.Name = "kryptonHeader7";
            this.kryptonHeader7.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader7.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader7.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader7.TabIndex = 200;
            this.kryptonHeader7.Values.Description = "";
            this.kryptonHeader7.Values.Heading = "Chi tiết thông tin";
            // 
            // txt_Money_Month
            // 
            this.txt_Money_Month.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_Money_Month.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Money_Month.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Money_Month.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Money_Month.Location = new System.Drawing.Point(132, 38);
            this.txt_Money_Month.Name = "txt_Money_Month";
            this.txt_Money_Month.Size = new System.Drawing.Size(149, 21);
            this.txt_Money_Month.TabIndex = 153;
            // 
            // Panel_Search_2
            // 
            this.Panel_Search_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search_2.Controls.Add(this.dte_Month);
            this.Panel_Search_2.Controls.Add(this.kryptonHeader6);
            this.Panel_Search_2.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search_2.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search_2.Name = "Panel_Search_2";
            this.Panel_Search_2.Size = new System.Drawing.Size(300, 66);
            this.Panel_Search_2.TabIndex = 204;
            // 
            // dte_Month
            // 
            this.dte_Month.CustomFormat = "dd/MM/yyyy";
            this.dte_Month.Location = new System.Drawing.Point(132, 34);
            this.dte_Month.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_Month.Name = "dte_Month";
            this.dte_Month.Size = new System.Drawing.Size(149, 28);
            this.dte_Month.TabIndex = 157;
            this.dte_Month.Value = new System.DateTime(((long)(0)));
            // 
            // kryptonHeader6
            // 
            this.kryptonHeader6.AutoSize = false;
            this.kryptonHeader6.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader6.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader6.Name = "kryptonHeader6";
            this.kryptonHeader6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader6.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader6.TabIndex = 200;
            this.kryptonHeader6.Values.Description = "";
            this.kryptonHeader6.Values.Heading = "Chọn tháng";
            // 
            // dte_Teams
            // 
            this.dte_Teams.CustomFormat = "dd/MM/yyyy";
            this.dte_Teams.Location = new System.Drawing.Point(132, 34);
            this.dte_Teams.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_Teams.Name = "dte_Teams";
            this.dte_Teams.Size = new System.Drawing.Size(149, 28);
            this.dte_Teams.TabIndex = 0;
            this.dte_Teams.Value = new System.DateTime(((long)(0)));
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 42);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1266, 726);
            this.tabControl1.TabIndex = 139;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage1.Controls.Add(this.Panel_Right);
            this.tabPage1.Controls.Add(this.label47);
            this.tabPage1.Controls.Add(this.Panel_Left);
            this.tabPage1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1258, 700);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "NGÀY";
            // 
            // Panel_Right
            // 
            this.Panel_Right.Controls.Add(this.GVData1);
            this.Panel_Right.Controls.Add(this.panel2);
            this.Panel_Right.Controls.Add(this.kryptonHeader4);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(305, 3);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(950, 694);
            this.Panel_Right.TabIndex = 138;
            // 
            // GVData1
            // 
            this.GVData1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData1.Location = new System.Drawing.Point(0, 30);
            this.GVData1.Name = "GVData1";
            this.GVData1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData1.Size = new System.Drawing.Size(950, 546);
            this.GVData1.TabIndex = 202;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.btn_Count_Date);
            this.panel2.Controls.Add(this.btn_Save_Date);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.kryptonHeader5);
            this.panel2.Controls.Add(this.txt_ToTalDifGeneral);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.txt_ToTalTimeDifGeneral);
            this.panel2.Controls.Add(this.txt_MedTimeDifGeneral);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 576);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(950, 118);
            this.panel2.TabIndex = 206;
            // 
            // btn_Count_Date
            // 
            this.btn_Count_Date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count_Date.Location = new System.Drawing.Point(818, 68);
            this.btn_Count_Date.Name = "btn_Count_Date";
            this.btn_Count_Date.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Count_Date.Size = new System.Drawing.Size(120, 40);
            this.btn_Count_Date.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count_Date.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count_Date.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count_Date.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Count_Date.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count_Date.TabIndex = 204;
            this.btn_Count_Date.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Count_Date.Values.Image")));
            this.btn_Count_Date.Values.Text = "Tính toán";
            // 
            // btn_Save_Date
            // 
            this.btn_Save_Date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save_Date.Location = new System.Drawing.Point(818, 68);
            this.btn_Save_Date.Name = "btn_Save_Date";
            this.btn_Save_Date.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save_Date.Size = new System.Drawing.Size(120, 40);
            this.btn_Save_Date.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save_Date.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save_Date.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save_Date.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save_Date.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save_Date.TabIndex = 205;
            this.btn_Save_Date.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save_Date.Values.Image")));
            this.btn_Save_Date.Values.Text = "Cập nhật";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(15, 87);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(181, 18);
            this.label6.TabIndex = 151;
            this.label6.Text = "Tổng Tiền Làm Việc Khác";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(950, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 200;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Thông tin chung của nhóm";
            // 
            // txt_ToTalDifGeneral
            // 
            this.txt_ToTalDifGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_ToTalDifGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_ToTalDifGeneral.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_ToTalDifGeneral.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToTalDifGeneral.Location = new System.Drawing.Point(198, 87);
            this.txt_ToTalDifGeneral.Name = "txt_ToTalDifGeneral";
            this.txt_ToTalDifGeneral.ReadOnly = true;
            this.txt_ToTalDifGeneral.Size = new System.Drawing.Size(123, 21);
            this.txt_ToTalDifGeneral.TabIndex = 150;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(15, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 18);
            this.label2.TabIndex = 147;
            this.label2.Text = "Tổng  TG Việc Khác (Ăn Chung)";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(15, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 18);
            this.label5.TabIndex = 149;
            this.label5.Text = "TG Trung Bình Làm Việc Khác";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_ToTalTimeDifGeneral
            // 
            this.txt_ToTalTimeDifGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_ToTalTimeDifGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_ToTalTimeDifGeneral.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_ToTalTimeDifGeneral.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToTalTimeDifGeneral.Location = new System.Drawing.Point(198, 36);
            this.txt_ToTalTimeDifGeneral.Name = "txt_ToTalTimeDifGeneral";
            this.txt_ToTalTimeDifGeneral.ReadOnly = true;
            this.txt_ToTalTimeDifGeneral.Size = new System.Drawing.Size(123, 21);
            this.txt_ToTalTimeDifGeneral.TabIndex = 146;
            // 
            // txt_MedTimeDifGeneral
            // 
            this.txt_MedTimeDifGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_MedTimeDifGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_MedTimeDifGeneral.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_MedTimeDifGeneral.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_MedTimeDifGeneral.Location = new System.Drawing.Point(198, 62);
            this.txt_MedTimeDifGeneral.Name = "txt_MedTimeDifGeneral";
            this.txt_MedTimeDifGeneral.ReadOnly = true;
            this.txt_MedTimeDifGeneral.Size = new System.Drawing.Size(123, 21);
            this.txt_MedTimeDifGeneral.TabIndex = 148;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(950, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 201;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "Thông tin công nhân";
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.DodgerBlue;
            this.label47.Dock = System.Windows.Forms.DockStyle.Left;
            this.label47.Location = new System.Drawing.Point(303, 3);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(2, 694);
            this.label47.TabIndex = 205;
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.Panel_Detail);
            this.Panel_Left.Controls.Add(this.Panel_Team);
            this.Panel_Left.Controls.Add(this.Panel_Search);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(3, 3);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(300, 694);
            this.Panel_Left.TabIndex = 204;
            // 
            // Panel_Detail
            // 
            this.Panel_Detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Detail.Controls.Add(this.label39);
            this.Panel_Detail.Controls.Add(this.kryptonHeader3);
            this.Panel_Detail.Controls.Add(this.label41);
            this.Panel_Detail.Controls.Add(this.txt_ToTal_Money);
            this.Panel_Detail.Controls.Add(this.txt_ToTal_Time);
            this.Panel_Detail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel_Detail.Location = new System.Drawing.Point(0, 576);
            this.Panel_Detail.Name = "Panel_Detail";
            this.Panel_Detail.Size = new System.Drawing.Size(300, 118);
            this.Panel_Detail.TabIndex = 205;
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label39.Location = new System.Drawing.Point(50, 41);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 15);
            this.label39.TabIndex = 144;
            this.label39.Text = "Tổng Số Tiền";
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 200;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Chi tiết thông tin";
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label41.Location = new System.Drawing.Point(20, 68);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(110, 15);
            this.label41.TabIndex = 145;
            this.label41.Text = "Tổng Số Thời Gian";
            // 
            // txt_ToTal_Money
            // 
            this.txt_ToTal_Money.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_ToTal_Money.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_ToTal_Money.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_ToTal_Money.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToTal_Money.Location = new System.Drawing.Point(132, 38);
            this.txt_ToTal_Money.Name = "txt_ToTal_Money";
            this.txt_ToTal_Money.ReadOnly = true;
            this.txt_ToTal_Money.Size = new System.Drawing.Size(149, 21);
            this.txt_ToTal_Money.TabIndex = 0;
            // 
            // txt_ToTal_Time
            // 
            this.txt_ToTal_Time.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_ToTal_Time.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_ToTal_Time.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_ToTal_Time.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToTal_Time.Location = new System.Drawing.Point(132, 65);
            this.txt_ToTal_Time.Name = "txt_ToTal_Time";
            this.txt_ToTal_Time.ReadOnly = true;
            this.txt_ToTal_Time.Size = new System.Drawing.Size(149, 21);
            this.txt_ToTal_Time.TabIndex = 1;
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.LVData1);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Team.Location = new System.Drawing.Point(0, 66);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(300, 628);
            this.Panel_Team.TabIndex = 204;
            // 
            // LVData1
            // 
            this.LVData1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVData1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVData1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVData1.FullRowSelect = true;
            this.LVData1.GridLines = true;
            this.LVData1.HideSelection = false;
            this.LVData1.Location = new System.Drawing.Point(0, 30);
            this.LVData1.Name = "LVData1";
            this.LVData1.Size = new System.Drawing.Size(300, 598);
            this.LVData1.TabIndex = 201;
            this.LVData1.UseCompatibleStateImageBehavior = false;
            this.LVData1.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 200;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Chọn nhóm";
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.Controls.Add(this.dte_Teams);
            this.Panel_Search.Controls.Add(this.kryptonHeader2);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(300, 66);
            this.Panel_Search.TabIndex = 203;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 200;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Chọn ngày";
            // 
            // txtTitle
            // 
            this.txtTitle.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtTitle.Size = new System.Drawing.Size(1266, 42);
            this.txtTitle.TabIndex = 203;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Tổng hợp lương năng xuất";
            this.txtTitle.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            this.txtTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.txtTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.txtTitle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Frm_Production_Money
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1266, 768);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.txtTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Production_Money";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TỔNG HỢP LƯƠNG NĂNG SUẤT";
            this.Load += new System.EventHandler(this.Frm_Money_Productivity_Load);
            this.tabPage2.ResumeLayout(false);
            this.Panel_Right_2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData2)).EndInit();
            this.panel4.ResumeLayout(false);
            this.Panel_Left_2.ResumeLayout(false);
            this.Panel_Team_2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.Panel_Search_2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.Panel_Right.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Detail.ResumeLayout(false);
            this.Panel_Detail.PerformLayout();
            this.Panel_Team.ResumeLayout(false);
            this.Panel_Search.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel Panel_Left_2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Money_Month;
        private TNS.SYS.TNDateTimePicker dte_Month;
        private System.Windows.Forms.Timer timer1;
        private TNS.SYS.TNDateTimePicker dte_Teams;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_ToTalDifGeneral;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_MedTimeDifGeneral;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_ToTalTimeDifGeneral;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txt_ToTal_Time;
        private System.Windows.Forms.TextBox txt_ToTal_Money;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Panel Panel_Detail;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private System.Windows.Forms.Panel Panel_Team;
        private System.Windows.Forms.ListView LVData1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel Panel_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.DataGridView GVData1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private System.Windows.Forms.Panel panel2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Count_Date;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save_Date;
        private System.Windows.Forms.Panel Panel_Team_2;
        private System.Windows.Forms.ListView LVData2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader8;
        private System.Windows.Forms.Panel panel3;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader7;
        private System.Windows.Forms.Panel Panel_Search_2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader6;
        private System.Windows.Forms.Panel Panel_Right_2;
        private System.Windows.Forms.DataGridView GVData2;
        private System.Windows.Forms.Panel panel4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Count_Month;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader10;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label47;
    }
}