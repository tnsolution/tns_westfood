﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Order_Rate : Form
    {
        private int _RateKey = 0;
        private Category_Info _Category;
        public Frm_Setup_Order_Rate()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;
        }

        

        private void Frm_Setup_Order_Rate_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            Load_Data();
        }
        private void Txt_Rate_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Rate.Text.Trim(), out zAmount))
            {
                txt_Rate.Text = zAmount.Toe2String();
                txt_Rate.SelectionStart = txt_Rate.Text.Length;
            }
            else
            {
                Utils.TNMessageBoxOK("Lỗi!.Định dạng số.",1);
            }
        }
        private void Load_Data()
        {
            _Category = new Category_Info(_RateKey);
            txt_RateID.Text = _Category.RateID;
            txt_Rate_Name.Text = _Category.RateName;
            txt_Rate.Text = _Category.Rate.ToString();
            txt_Rank.Text = _Category.Rank.ToString("n0");
            txt_Description.Text = _Category.Description.Trim();
            lbl_Created.Text = "Tạo bởi:[" + _Category.CreatedName + "][" + _Category.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + _Category.ModifiedName + "][" + _Category.ModifiedOn + "]";

        }
        private void Save()
        {
            _Category = new Category_Info(_RateKey);
            //if (_RateKey == 0)
            //{
            //    _Category.RateID = Category_Data.Auto();
            //}
            _Category.RateID = txt_RateID.Text.Trim().ToUpper();
            _Category.RateName = txt_Rate_Name.Text.Trim();

            float zRate = 0;
            if(float.TryParse(txt_Rate.Text.Trim(),out zRate))
            {

            }
            _Category.Rate = zRate;
            _Category.Rank = txt_Rank.Text.Trim().ToInt();
            _Category.Description = txt_Description.Text.Trim();
            _Category.CreatedBy = SessionUser.UserLogin.Key;
            _Category.CreatedName = SessionUser.UserLogin.EmployeeName;
            _Category.ModifiedBy = SessionUser.UserLogin.Key;
            _Category.ModifiedName = SessionUser.UserLogin.EmployeeName;
            _Category.Save();
        }
        #region[ListView]
        private void DisplayData()
        {

            DataTable ztb = Order_Rate_Data.ListSearch(txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][6].ToString();
                if (zKey != null || zKey != "")
                {
                    _RateKey = int.Parse(zKey);
                    Load_Data();
                }
                else
                {
                    _RateKey = 0;
                    Load_Data();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách chức vụ.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã";
            GVData.Rows[0][2] = "Tên hệ số";
            GVData.Rows[0][3] = "Hệ số";
            GVData.Rows[0][4] = "Sắp xếp";
            GVData.Rows[0][5] = "Ghi chú";
            GVData.Rows[0][6] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3].Toe2String();
                GVData.Rows[rIndex + 1][4] = rData[4].Toe0String();
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[0];

            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 80;
            GVData.Cols[4].Width = 80;
            GVData.Cols[6].Visible = false;

            GVData.Cols[3].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[4].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion
        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_RateID.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã hệ số!",1);
                return;
            }
            if (txt_Rate_Name.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập tên hệ số !",1);
                return;
            }
            float zRate;
            if (!float.TryParse(txt_Rate.Text, out zRate))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số hệ số!",1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số sắp xếp!", 1);
                return;
            }
            if (Category_Data.CountID(txt_RateID.Text.Trim().ToUpper()) > 0 && _RateKey == 0)
            {
                Utils.TNMessageBoxOK("Mã hệ số đã tồn tại !.", 1);
                return;
            }
            else
            {
                Save();
                if (_Category.Message.Length == 0)
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công", 3);
                    _RateKey = 0;
                    Load_Data();
                    DisplayData();
                }
                else
                {
                    Utils.TNMessageBoxOK(_Category.Message, 4);
                }
            }

        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            _Category = new Category_Info(_RateKey);
            _Category.ModifiedBy = SessionUser.UserLogin.Key;
            _Category.ModifiedName = SessionUser.UserLogin.EmployeeName;
            _Category.Delete();
            if (_Category.Message.Length == 0)
            {
                Utils.TNMessageBoxOK("Cập nhật thành công",3);
                DisplayData();
            }
            else
            {
                Utils.TNMessageBoxOK(_Category.Message, 4);
            }
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _RateKey = 0;
            _Category = new Category_Info(_RateKey);
            Load_Data();
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_RateID.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
