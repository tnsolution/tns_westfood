﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_LuongKhoanTinhChuyenCan_ChiTiet : Form
    {
        int _TeamKey = 0;
        string _TeamName = "";
        string _InEmployeeID = "";
        string _TeamKeySalary = "";
        string _OptionText = "Tất cả";
        public Frm_LuongKhoanTinhChuyenCan_ChiTiet()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            rdo_Dif.CheckedChanged += Rdo_Dif_CheckedChanged;
            cbo_TeamMain.SelectedIndexChanged += Cbo_TeamMain_SelectedIndexChanged;

        }

        private void Frm_LuongKhoanTinhChuyenCan_ChiTiet_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY RANK";

            LoadDataToToolbox.KryptonComboBox(cbo_TeamMain, zSQL, "");// nhóm chính

            Cbo_TeamMain_SelectedIndexChanged(null, null);

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            cboTeam.Enabled = false;

            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();
        }
        private void Cbo_TeamMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            string zSQL = " SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team " +
                          " WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26 AND TeamKey !=" + cbo_TeamMain.SelectedValue.ToInt() +
                          " ORDER BY RANK";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "--Tất cả--");
        }
        private void Rdo_Dif_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Dif.Checked)
            {
                cboTeam.Enabled = true;
            }
            else
            {
                cboTeam.Enabled = false;
            }
        }

        private void Check_Radio()
        {
            _OptionText = "Tất cả";
            if (rdo_All.Checked)
            {
                _TeamKeySalary = "";
                _OptionText = "Tất cả";
            }

            if (rdo_Team.Checked)
            {
                _TeamKeySalary = "'" + cbo_TeamMain.SelectedValue.ToInt() + "'";
                _OptionText = "Tại tổ";
            }

            if (rdo_Dif.Checked)
            {
                _TeamKeySalary = "";
                // Lấy danh sách nhóm trừ tổ chính nếu chọn tất cả
                if (cboTeam.SelectedValue.ToInt() == 0)
                {

                    for (int i = 1; i < cboTeam.Items.Count; i++)
                    {

                        _TeamKeySalary += "'" + ((TN_Item)cboTeam.Items[i]).Value + "',";

                    }
                    _TeamKeySalary = _TeamKeySalary.Remove(_TeamKeySalary.Length - 1, 1);
                    _OptionText = "Tất cả Tổ khác";
                }
                else
                {
                    _TeamKeySalary += "'" + cboTeam.SelectedValue.ToInt() + "'";
                    _OptionText = "Tổ khác:" + cboTeam.Text; ;
                }

            }

        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();
            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cboTeam.Text + " > Từ khóa:" + txt_Search.Text + "> Tùy chọn:" + _OptionText;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            if (txt_Search.Text.Trim().Length > 0)
            {
                _InEmployeeID = "'" + txt_Search.Text.Trim() + "'";
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            else
            {
                if (cbo_TeamMain.SelectedValue.ToString() != "0")
                {

                    _TeamKey = cbo_TeamMain.SelectedValue.ToInt();
                    Check_Radio();
                    // Lấy danh sách nhân viên của nhóm chính
                    DataTable zEmployee = Report.ListEmployeeOfTeam(_TeamKey, dte_FromDate.Value, dte_ToDate.Value);
                    _InEmployeeID = "";
                    if (zEmployee.Rows.Count > 0)
                    {
                        for (int i = 0; i < zEmployee.Rows.Count; i++)
                        {

                            _InEmployeeID += "'" + zEmployee.Rows[i]["EmployeeID"] + "',";

                        }
                        _InEmployeeID = _InEmployeeID.Remove(_InEmployeeID.Length - 1, 1);

                        using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                    }
                    else
                    {
                        Utils.TNMessageBoxOK("Không tìm thấy nhân viên!", 1);
                    }
                }
                else
                {
                    Utils.TNMessageBoxOK("Vui lòng chọn 1 nhóm!", 1);
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string zTeamName = "";
            if (cbo_TeamMain.SelectedValue.ToInt() != 0)
            {
                string[] s = cbo_TeamMain.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Khoán_Sản_Xuất_Công_Nhân_Tinh_Chuyen_Can" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVEmployee.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void DisplayData()
        {
            DataTable zTable = Report.KhoanTinhChuyenCan_ChiTiet(dte_FromDate.Value, dte_ToDate.Value, _InEmployeeID, _TeamKeySalary);
            if (zTable.Rows.Count > 0)
            {
                for (int i = 0; i < zTable.Rows.Count; i++)
                {

                    //Nếu nhân sự không có làm việc hôm đó thì xóa luôn , nếu không sẽ phát sinh colum rỗng
                    float zgiodu = 0;
                    if (float.TryParse(zTable.Rows[i][2].ToString(), out zgiodu))
                    {

                    }
                    float zluonkhoan = 0;
                    if (float.TryParse(zTable.Rows[i][3].ToString(), out zluonkhoan))
                    {

                    }
                    if (zluonkhoan == 0 && zgiodu == 0)
                    {
                        zTable.Rows.Remove(zTable.Rows[i]);
                        i--;
                    }
                }


                DataTable BangGioDu = new DataTable();

                DataRow[] rGioDu = zTable.Select("[TIENDU] > 0");
                if (rGioDu.Length > 0)
                {
                    BangGioDu = rGioDu.CopyToDataTable();

                    for (int i = 0; i < rGioDu.Length; i++)
                    {
                        rGioDu[i].Delete();
                    }
                }
                zTable.AcceptChanges();

                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate("WORK", "ORDER",
                     new string[] { "LK"}, "CÔNG NHÂN", "TỔNG CỘNG", "TỔNG CỘNG",
                     new string[] { "LƯƠNG KHOÁN" }, 0, false);


                string[] temp = zTablePivot.Columns[1].ColumnName.Split('|');
                int TeamKeyFlag = temp[0].ToInt();
                string TeamNameFlag = temp[1];


                for (int i = 1; i < zTablePivot.Columns.Count; i++)
                {

                    temp = zTablePivot.Columns[i].ColumnName.Split('|');
                    if (temp.Length >= 7)
                    {
                        int TeamKey = temp[0].ToInt();
                        string TeamName = temp[1];

                        if (TeamKeyFlag != 0 && TeamKeyFlag != TeamKey)
                        {
                            //add 4 cột tổng
                            zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|LƯƠNG KHOÁN").SetOrdinal(i);
                            //zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|SỐ LƯỢNG").SetOrdinal(i);

                            TeamKeyFlag = TeamKey;
                            TeamNameFlag = TeamName;
                        }
                    }
                }

                int c = zTablePivot.Columns.Count;

                zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|LƯƠNG KHOÁN").SetOrdinal(c - 1);
                //zTablePivot.Columns.Add(TeamNameFlag + "|Tổng|SỐ LƯỢNG").SetOrdinal(c-2);

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitLayoutGV(zTablePivot);
                    FillDataGV(zTablePivot);
                    FillDataGV_GioDu(BangGioDu);
                    SumTotalCounmGV();
                    // GVEmployee.Sort(SortFlags.Ascending, 2);
                }));
            }
        }
        void InitLayoutGV(DataTable zTable)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();

            //for (int i = 0; i < zTable.Columns.Count - 1; i++)
            //{
            //    DataColumn Col = zTable.Columns[i];
            //    string[] strCaption = Col.ColumnName.Split('|');
            //    if (strCaption.Length > 3 && strCaption[6].Trim() == "LƯƠNG KHOÁN") //bỏ cột lương khoán
            //    {
            //        zTable.Columns.Remove(Col.ColumnName);
            //    }

            //}
            ////bỏ cột tổng cộng số lượng vì không ý nghĩa cac sp là khác nhạu
            //zTable.Columns.Remove(zTable.Columns[zTable.Columns.Count - 1]);


            GVEmployee.Cols.Add(zTable.Columns.Count + 3);
            GVEmployee.Rows.Add(5);

            CellRange zRange = GVEmployee.GetCellRange(0, 0, 4, 0);
            zRange.Data = "STT";

            zRange = GVEmployee.GetCellRange(0, 1, 4, 1);
            zRange.Data = "HỌ VÀ TÊN";

            zRange = GVEmployee.GetCellRange(0, 2, 4, 2);
            zRange.Data = "MÃ THẺ";

            zRange = GVEmployee.GetCellRange(0, 3, 4, 3);
            zRange.Data = "GIÃN CA";

            for (int i = 1; i < zTable.Columns.Count; i++)
            {
                DataColumn Col = zTable.Columns[i];

                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length >= 7)
                {
                    GVEmployee.Rows[0][i + 3] = strCaption[1];  //NHÓM CÔNG NHÂN
                    GVEmployee.Rows[1][i + 3] = strCaption[2];  //Sản phẩm
                    GVEmployee.Rows[2][i + 3] = strCaption[4];  //Công việc
                    GVEmployee.Rows[3][i + 3] = strCaption[5].Toe0String();  //Đơn giá
                    GVEmployee.Rows[4][i + 3] = strCaption[6];  //"LK", "TG", "SLTP", "NS"
                }
                else
                {
                    if (strCaption.Length == 3)
                    {
                        GVEmployee.Rows[0][i + 3] = strCaption[0];  //NHÓM CÔNG NHÂN
                        GVEmployee.Rows[3][i + 3] = strCaption[1];  //Tổng
                        GVEmployee.Rows[4][i + 3] = strCaption[2];   //"LK", "TG", "SLTP", "NS"

                        //zRange = GVEmployee.GetCellRange(0, i + 3, 3, i + 3);
                        //zRange.Data = strCaption[1];
                        //GVEmployee.MergedRanges.Add(zRange);
                    }
                    else
                    {
                        GVEmployee.Rows[3][i + 3] = strCaption[0];
                        GVEmployee.Rows[4][i + 3] = strCaption[1];
                        //zRange = GVEmployee.GetCellRange(0, i + 3, 3, i + 3);
                        //zRange.Data = strCaption[0];
                    }
                }
                GVEmployee.Cols[i].Width = 110;
            }

            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;

            GVEmployee.Rows[0].AllowMerging = true;
            GVEmployee.Rows[1].AllowMerging = true;
            GVEmployee.Rows[2].AllowMerging = true;
            GVEmployee.Rows[3].AllowMerging = true;
            //GVEmployee.Rows[4].AllowMerging = true;

            GVEmployee.Cols[0].Width = 40;
            GVEmployee.Cols[0].AllowMerging = true;

            GVEmployee.Cols[1].Width = 180;
            GVEmployee.Cols[1].AllowMerging = true;

            GVEmployee.Cols[2].AllowMerging = true;
            GVEmployee.Cols[2].Width = 100;

            GVEmployee.Cols[3].AllowMerging = true;
            GVEmployee.Cols[3].Width = 100;


            //4 cột tổng cuối 
            //TỔNG CỘT
            GVEmployee.Cols[GVEmployee.Cols.Count - 1].AllowMerging = true;
            CellRange zCellRange;
            zCellRange = GVEmployee.GetCellRange(0, GVEmployee.Cols.Count - 1, 4, GVEmployee.Cols.Count - 1);
            zCellRange.Data = "TỔNG CỘNG";

            GVEmployee.Cols[GVEmployee.Cols.Count - 1].StyleNew.ForeColor = Color.Red;
            //GVEmployee.Cols[GVEmployee.Cols.Count - 2].StyleNew.ForeColor = Color.Red;
            GVEmployee.Cols[GVEmployee.Cols.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVEmployee.Cols[GVEmployee.Cols.Count - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //----------------------

            GVEmployee.Rows.Fixed = 5;
            GVEmployee.Cols.Frozen = 3;

            GVEmployee.Cols[0].StyleNew.BackColor = Color.Empty;
            GVEmployee.Cols[1].StyleNew.BackColor = Color.Empty;
            GVEmployee.Cols[2].StyleNew.BackColor = Color.Empty;

            GVEmployee.Rows[0].StyleNew.WordWrap = true;
            GVEmployee.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Rows[0].Height = 30;

            GVEmployee.Rows[1].StyleNew.WordWrap = true;
            GVEmployee.Rows[1].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Rows[1].Height = 70;

            GVEmployee.Rows[2].StyleNew.WordWrap = true;
            GVEmployee.Rows[2].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Rows[2].Height = 70;

            GVEmployee.Rows[3].StyleNew.WordWrap = true;
            GVEmployee.Rows[3].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[4].StyleNew.WordWrap = true;
            GVEmployee.Rows[4].TextAlign = TextAlignEnum.CenterCenter;

            for (int i = 3; i < GVEmployee.Cols.Count; i++)
            {
                GVEmployee.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        void FillDataGV(DataTable zTable)
        {
            //for (int i = 0; i < zTable.Columns.Count; i++)
            //{
            //    DataColumn Col = zTable.Columns[i];
            //    string[] strCaption = Col.ColumnName.Split('|');
            //    if (strCaption.Length > 3 && strCaption[6].Trim() == "LƯƠNG KHOÁN") //bỏ cột lương khoán
            //    {
            //        zTable.Columns.Remove(Col.ColumnName);
            //    }

            //}
            //bỏ cột tổng cộng số lượng vì không ý nghĩa cac sp là khác nhạu
            //zTable.Columns.Remove(zTable.Columns[zTable.Columns.Count - 2]);


            int TeamKeyFlag = _TeamKey;

            double TotalLK = 0;
            double TotalTP = 0;

            // Thêm 1 cột để sắp xếp
            zTable.Columns.Add("Rank");
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                DataRow rPivot = zTable.Rows[i];
                string rColumn0 = rPivot[0].ToString();
                string[] temp = rColumn0.Split('|');
                string EmployeeID = temp[1];
                zTable.Rows[i]["Rank"] = ConvertIDRank(EmployeeID);

                ////Nếu nhân sự không có làm việc hôm đó thì xóa luôn , nếu không sẽ phát sinh colum rỗng
                //string zluonkhoan = rPivot[zTable.Columns.Count - 5].ToString();
                //if (zluonkhoan.ToFloat() == 0)
                //{
                //    zTable.Rows.Remove(rPivot);
                //    i--;
                //}
            }
            DataView dv = zTable.DefaultView;
            dv.Sort = " Rank ASC";
            zTable = dv.ToTable();
            zTable.Columns.Remove("Rank");
            //--sắp xếp xong xóa cột sắp xếp đi

            int Row = 5;
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                GVEmployee.Rows.Add();
                GVEmployee.Rows[Row][0] = i + 1;

                int colum_LK = 4;
                #region [--Fill data bình thường--]
                for (int j = 0; j < zTable.Columns.Count; j++)
                {
                    DataRow rPivot = zTable.Rows[i];

                    string rColumn0 = rPivot[j].ToString();

                    if (rColumn0.Contains("|"))
                    {
                        string[] temp = rColumn0.Split('|');
                        string EmployeeName = temp[0];
                        string EmployeeID = temp[1];
                        string OverMoney = "0";

                        //foreach (DataRow rgiodu in BangGioDu.Rows)
                        //{
                        //    string[] str = rgiodu[0].ToString().Split('|');
                        //    if (str[1] == EmployeeID)
                        //    {
                        //        OverMoney = FormatMoney(rgiodu["TIENDU"]);
                        //        break;
                        //    }
                        //}

                        GVEmployee.Rows[Row][j + 1] = temp[0];
                        GVEmployee.Rows[Row][j + 2] = temp[1];
                        GVEmployee.Rows[Row][j + 3] = OverMoney.Toe0String();
                    }
                    else
                    {
                        //if (j + 3 == colum_LK)  // nếu là cột lương khoán thì làm tròn 0 số thập phân
                        //{
                        //    GVEmployee.Rows[Row][j + 3] = zTable.Rows[i][j].Toe0String();
                        //    colum_LK += 4;
                        //}
                        //else
                        GVEmployee.Rows[Row][j + 3] = zTable.Rows[i][j].Toe0String(); //--FormatMoney(zTable.Rows[i][j].ToString());
                    }
                }
                #endregion

                #region [--Tinh tổng từng loại cột--]
                for (int j = 1; j < zTable.Columns.Count; j++)
                {
                    string[] temp = zTable.Columns[j].ColumnName.Split('|');
                    if (temp.Length >= 7)
                    {
                        TeamKeyFlag = temp[0].ToInt();
                        double LK = 0;
                        //double TP = 0;
                        if (double.TryParse(zTable.Rows[i][j].ToString(), out LK))
                        {

                        }
                        //if (double.TryParse(zTable.Rows[i][j].ToString(), out TP))
                        //{

                        //}
                        TotalLK += LK;
                        //TotalTP += TP;
                        //TotalLK += zTable.Rows[i][j].ToDouble();
                        //TotalTG += zTable.Rows[i][j + 1].ToDouble();
                        //TotalSR += zTable.Rows[i][j + 2].ToDouble();
                        //TotalTP += zTable.Rows[i][j + 3].ToDouble();
                    }
                    else
                    {
                        int TeamKey = temp[0].ToInt();

                        if (TeamKeyFlag != 0 && TeamKeyFlag != TeamKey)
                        {
                            //add 4 cột tổng
                            GVEmployee.Rows[Row][j + 3] = TotalLK.Toe0String(); //FormatMoney(TotalLK);
                            //GVEmployee.Rows[Row][j + 3] = TotalTP.Toe2String(); //FormatMoney(TotalTP);
                            //color dòng tổng con
                            GVEmployee.Cols[j + 3].StyleNew.ForeColor = Color.Blue;
                            //GVEmployee.Cols[j + 3 + 1].StyleNew.ForeColor = Color.Blue;

                            GVEmployee.Cols[j + 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                            //GVEmployee.Cols[j + 3 + 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

                            TeamKeyFlag = TeamKey;

                            TotalLK = 0;

                            //TotalTP = 0;
                        }

                    }
                }
                #endregion

                //double m1 = UnFormatMoney(GVEmployee.Rows[Row][3]).ToDouble();

                //double m2 = UnFormatMoney(GVEmployee.Rows[Row][GVEmployee.Cols.Count - 4]).ToDouble();
                //double m3 = m1 + m2; // tổng giãn ca + tổng lương khoán
                //GVEmployee.Rows[Row][GVEmployee.Cols.Count - 4] = FormatMoney(m3);

                Row++;
            }
        }
        void FillDataGV_GioDu(DataTable BangGioDu)
        {
            if (GVEmployee.Rows.Count > 5)
            {
                for (int i = 5; i < GVEmployee.Rows.Count; i++)
                {
                    string ID = GVEmployee.Rows[i][2].ToString();
                    foreach (DataRow rgiodu in BangGioDu.Rows)
                    {
                        string[] temp = rgiodu[0].ToString().Split('|');

                        if (ID == temp[1]) //temp[1]:EmployeeID
                        {
                            GVEmployee.Rows[i][3] = rgiodu["TIENDU"].Toe0String();
                            BangGioDu.Rows.Remove(rgiodu);
                            break;
                        }
                    }

                }
            }
            else
            {
                foreach (DataRow rgiodu in BangGioDu.Rows)
                {
                    string[] temp = rgiodu[0].ToString().Split('|');
                    string EmployeeName = temp[0];
                    string EmployeeID = temp[1];
                    string OverMoney = rgiodu["TIENDU"].Toe0String();

                    GVEmployee.Rows.Add();
                    int r = GVEmployee.Rows.Count;

                    GVEmployee.Rows[r - 1][1] = EmployeeName;
                    GVEmployee.Rows[r - 1][2] = EmployeeID;
                    GVEmployee.Rows[r - 1][3] = OverMoney.Toe0String();
                }
            }
            if (GVEmployee.Rows.Count > 5)
            {
                if (rdo_GopGioDu.Checked == true) // nếu gộp giờ dư thì cộng vào
                {
                    for (int i = 5; i < GVEmployee.Rows.Count; i++)
                    {
                        double m1 = 0;
                        double m2 = 0;
                        if (double.TryParse(GVEmployee.Rows[i][3].ToString(), out m1))
                        {

                        }
                        if (GVEmployee.Rows[i][GVEmployee.Cols.Count - 1] != null && double.TryParse(GVEmployee.Rows[i][GVEmployee.Cols.Count - 1].ToString(), out m2))
                        {

                        }
                        double m3 = m1 + m2; // tổng giãn ca + tổng lương khoán
                        GVEmployee.Rows[i][GVEmployee.Cols.Count - 1] = m3.Toe0String();

                        //double m1 = UnFormatMoney(GVEmployee.Rows[i][3]).ToDouble();

                        //double m2 = UnFormatMoney(GVEmployee.Rows[i][GVEmployee.Cols.Count - 4]).ToDouble();
                        //double m3 = m1 + m2; // tổng giãn ca + tổng lương khoán
                        //GVEmployee.Rows[i][GVEmployee.Cols.Count - 4] = FormatMoney(m3);
                    }
                }
            }
        }
        void SumTotalCounmGV()
        {
            //Tổng theo cột
            GVEmployee.Rows.Add();
            GVEmployee.Rows[GVEmployee.Rows.Count - 1][1] = "TỔNG CỘNG";
            //int zColumn_LK = 4;
            for (int i = 3; i < GVEmployee.Cols.Count; i++)
            {
                double zTotal = 0;
                for (int j = 5; j < GVEmployee.Rows.Count; j++)
                {
                    double zTemp = 0;
                    if (GVEmployee.Rows[j][i] != null)
                    {
                        if (double.TryParse(GVEmployee.Rows[j][i].ToString(), out zTemp))
                        {

                        }
                        zTotal += zTemp;
                    }

                }
                GVEmployee.Rows[GVEmployee.Rows.Count - 1][i] = zTotal.Toe0String();
            }
            //Dòng tổng
            GVEmployee.Rows[GVEmployee.Rows.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }

        // in thì lấy report 23
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
