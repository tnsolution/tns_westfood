﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Production_Adjusted : Form
    {
        DateTime _FromDate = new DateTime();
        DateTime _ToDate = new DateTime();

        private string _OrderKey = "";
        private string _OrderID = "";
        private string _TeamName = "";
        private int _Status = 0;
        private int _TeamKey = 0;
        private int _StageKey = 0;
        private int _Dangsua = 0;

        //2 bảng tạm
        private DataTable _TableRemark = new DataTable(); //Tính nhanh
        private DataTable _TableGeneral = new DataTable();//Sữa lại dữ liệu
        //2 bản chính
        private float _TotalTime = 0; //Tổng thời gian
        private float _ToTalKg = 0; //Tổng số kí
        private double _ToTalMoney = 0; //Tổng số tiền
        private float _TBKg = 0;   //Số kí trung bình
        private double _TBMoney = 0;  //Số tiền trung bình

        private Order_Money_Info zOrder;
        private int _Count = 0;
        private int _RowDataDefault = 0; //Tổng số dòng vừa load dữ liệu

        public Frm_Production_Adjusted()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DoubleBuffered(GVOrder, true);
            Utils.DoubleBuffered(GVData1, true);
            Utils.DoubleBuffered(GVData2, true);
            Utils.DoubleBuffered(GVSum1, true);
            Utils.DoubleBuffered(GVSum2, true);

            Utils.DrawGVStyle(ref GVTeam);
            Utils.DrawGVStyle(ref GVOrder);
            Utils.DrawGVStyle(ref GVSum1);
            Utils.DrawGVStyle(ref GVSum2);

            Utils.DrawGVStyle(ref GVData1);
            Utils.DrawGVStyle(ref GVData2);

            InitLayout_GVTeam(GVTeam);
            InitLayout_GVSum(GVSum1);
            InitLayout_GVSum(GVSum2);
            InitLayout_GVData(GVData1);
            InitLayout_GVData(GVData2);

            GVData1.CellEndEdit += GVData1_CellEndEdit;
            GVData1.KeyDown += GVData1_KeyDown;
            GVTeam.SelectionChanged += GVTeam_Click;
            GVOrder.SelectionChanged += GVOrder_Click;

            dte_Date.Validated += (o, s) =>
            {
                _FromDate = new DateTime(dte_Date.Value.Year, dte_Date.Value.Month, dte_Date.Value.Day, 0, 0, 0);
                _ToDate = new DateTime(dte_Date.Value.Year, dte_Date.Value.Month, dte_Date.Value.Day, 23, 59, 59);
            };

            dte_Date.Value = SessionUser.Date_Work;
            _FromDate = new DateTime(dte_Date.Value.Year, dte_Date.Value.Month, dte_Date.Value.Day, 0, 0, 0);
            _ToDate = new DateTime(dte_Date.Value.Year, dte_Date.Value.Month, dte_Date.Value.Day, 23, 59, 59);
        }

        private void Frm_Production_Adjusted_Load(object sender, EventArgs e)
        {
            btn_Save.Visible = false;
            btn_EditEnd.Visible = false;

            InitData_GVTeam(GVTeam, Team_Data.List_Team(false));

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region[LAYOUT SỬ DỤNG CHUNG]
        //Layout Gidview  List nhóm
        private void InitLayout_GVTeam(DataGridView GV)
        {
            GV.Columns.Add("No", "#");
            GV.Columns.Add("TeamID", "Mã nhóm");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 30;
            GV.Columns["TeamID"].Width = 70;

            GV.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["TeamName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVTeam(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["TeamName"].Tag = r["TeamKey"].ToString();
                    GvRow.Cells["TeamName"].Value = r["TeamName"].ToString();
                    GvRow.Cells["TeamID"].Value = r["TeamID"].ToString().Trim();
                }
                GV.ClearSelection();
            }
        }
        //Layout Gidview  List Đơn hang
        private void InitLayout_GVOrder(DataGridView GV)
        {
            GV.Columns.Clear();
            // Setup Column 
            GV.Columns.Add("No", "#");
            GV.Columns.Add("Info1", "Đơn hàng");
            GV.Columns.Add("Info2", "Công việc");

            GV.Columns.Add("OrderID", "Mã đơn hàng");
            GV.Columns.Add("Status", "Tình trạng");
            GV.Columns.Add("StageName", "Công đoạn");
            GV.Columns.Add("Price", "Đơn giá");

            GV.Columns["No"].Width = 30;

            GV.Columns["Info1"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["Info1"].Width = 130;

            GV.Columns["Info2"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["Info2"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            GV.Columns["OrderID"].Width = 0;
            GV.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["OrderID"].Visible = false;

            GV.Columns["Status"].Width = 0;
            GV.Columns["Status"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Status"].Visible = false;

            GV.Columns["StageName"].Width = 0;
            GV.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["StageName"].Visible = false;

            GV.Columns["Price"].Width = 0;
            GV.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Price"].Visible = false;
        }
        private void InitData_GVOrder(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["OrderID"].Tag = r["OrderKey"].ToString();
                    GvRow.Cells["OrderID"].Value = r["OrderID"].ToString();
                    GvRow.Cells["StageName"].Tag = r["StageKey"].ToString();
                    GvRow.Cells["StageName"].Value = r["StageName"].ToString();
                    GvRow.Cells["Price"].Value = r["Price"].ToString();
                    GvRow.Cells["Status"].Value = r["Status"].ToString();

                    string Status = "";

                    if (r["Status"].ToString() == "1")
                    {
                        Status = "Đã chia lại xong";
                    }
                    else if (r["Status"].ToString() == "")
                    {
                        Status = "Chưa chia lại";
                    }
                    else
                    {
                        Status = "Cần kiểm tra lại";
                    }

                    string OrderInfo = r["OrderID"].ToString() + Environment.NewLine + Status;
                    string Description = "Công việc: " + r["StageName"].ToString() + Environment.NewLine + "Đơn giá: " + r["Price"].Ton1String();

                    GvRow.Cells["Info1"].Value = OrderInfo;
                    GvRow.Cells["Info2"].Value = Description;
                }
                GV.ClearSelection();
            }
        }
        //Layout Gidview List Tổng
        private void InitLayout_GVSum(DataGridView GV)
        {
            GV.Columns.Add("Total", "TỔNG");
            GV.Columns.Add("Time", "Thời gian");
            GV.Columns.Add("KgProduct", "Kg");
            GV.Columns.Add("Kg", "Kg đem về");
            GV.Columns.Add("Money", "Tiền đem về");
            GV.Columns.Add("MoneyProduct", "Tiền chia lại");

            GV.Columns["Total"].Width = 280;
            GV.Columns["Time"].Width = 150;
            GV.Columns["KgProduct"].Width = 150;
            GV.Columns["Kg"].Width = 150;
            GV.Columns["Money"].Width = 150;
            GV.Columns["MoneyProduct"].Width = 150;

            GV.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KgProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Kg"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["MoneyProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        }
        private void InitData_GVSum(DataGridView GV, DataGridView GVSum)
        {
            GVSum.Rows.Clear();
            float totalmoneyproduct = 0;
            float totalkg = 0;
            float totaltime = 0;
            float totalkgProduct = 0;
            float totalmoney = 0;

            int no = GVSum.Rows.Count - 1;
            GVSum.Rows[no].Cells["Total"].Value = "";

            for (int k = 0; k < GV.Rows.Count - 1; k++)
            {
                totaltime += GV.Rows[k].Cells["Time"].Value.ToFloat();
                totalkgProduct += GV.Rows[k].Cells["KgProduct"].Value.ToFloat();
                totalkg += GV.Rows[k].Cells["Kg"].Value.ToFloat();
                totalmoney += GV.Rows[k].Cells["Money"].Value.ToFloat();
                totalmoneyproduct += GV.Rows[k].Cells["MoneyProduct"].Value.ToFloat();
            }
            GVSum.Rows.Add();
            GVSum.Rows[no].Cells["Time"].Value = totaltime.Ton1String();
            GVSum.Rows[no].Cells["KgProduct"].Value = totalkgProduct.Ton1String();
            GVSum.Rows[no].Cells["Kg"].Value = totalkg.Ton1String();
            GVSum.Rows[no].Cells["Money"].Value = totalmoney.Ton1String();
            GVSum.Rows[no].Cells["MoneyProduct"].Value = totalmoneyproduct.Ton1String();
            GVSum.Rows[no].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV.ClearSelection();
        }
        //Layout Gidview List Danh sách công nhân
        private void InitLayout_GVData(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeID", "Mã NV");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("Time", "Thời Gian");
            GV.Columns.Add("KgProduct", "Số Kg Chia Lại");
            GV.Columns.Add("Kg", "Số Kg Đem Về");
            GV.Columns.Add("Money", "Số Tiền Đem Về");
            GV.Columns.Add("MoneyProduct", "Số Tiền Chia Lại");
            GV.Columns.Add("Message", "Thông Báo");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EmployeeID"].Width = 70;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["EmployeeName"].Width = 170;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["Time"].Width = 150;
            GV.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["KgProduct"].Width = 150;
            GV.Columns["KgProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Kg"].Width = 150;
            GV.Columns["Kg"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Money"].Width = 150;
            GV.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["MoneyProduct"].Width = 150;
            GV.Columns["MoneyProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Message"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

        }
        #endregion

        #region [Datagidview Progess]
        private void GVTeam_Click(object sender, EventArgs e)
        {
            if (_Dangsua == 0)
            {
                if (GVTeam.SelectedRows.Count > 0 &&
                    GVTeam.CurrentRow.Cells["TeamName"].Tag != null)
                {
                    _TeamKey = int.Parse(GVTeam.CurrentRow.Cells["TeamName"].Tag.ToString());
                    _TeamName = GVTeam.CurrentRow.Cells["TeamName"].Value.ToString();

                    DataTable zTableOrder = HoTroSanXuat.DonHangThucHienChiaLai(_TeamKey, _FromDate, _ToDate);
                    InitLayout_GVOrder(GVOrder);
                    InitData_GVOrder(GVOrder, zTableOrder);

                    //Clear các bảng con khi chọn lại
                    _StageKey = 0;
                    _OrderKey = "";
                    GVData1.Rows.Clear();
                    GVData2.Rows.Clear();
                    GVSum1.Rows.Clear();
                    GVSum2.Rows.Clear();

                    txt_title.Text = _TeamName;
                }
            }
            else
            {
                MessageBox.Show("Bạn chưa kết thúc thao tác cập nhật nhanh!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void GVOrder_Click(object sender, EventArgs e)
        {
            if (GVOrder.SelectedRows.Count > 0 && GVOrder.CurrentRow.Cells["OrderID"].Tag != null)
            {
                _Status = GVOrder.CurrentRow.Cells["Status"].Value.ToInt();
                _OrderKey = GVOrder.CurrentRow.Cells["OrderID"].Tag.ToString();
                _OrderID = GVOrder.CurrentRow.Cells["OrderID"].Value.ToString();
                _StageKey = int.Parse(GVOrder.CurrentRow.Cells["StageName"].Tag.ToString());


                switch (_Status)
                {
                    case 1:
                        if (_Dangsua == 0)
                        {
                            GVLoadData2();
                            GVData1.Rows.Clear();
                            btn_EditBegin.Visible = true;
                            btn_EditEnd.Visible = true;
                            btn_Count.Visible = false;
                            btn_Save.Visible = false;
                            btn_Apply.Visible = false;
                            tabControl1.SelectedTab = tabPage2;
                        }
                        else
                        {
                            MessageBox.Show("Bạn chưa kết thúc thao tác cập nhật nhanh!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;

                    case 0:
                        GVLoadData1();
                        GVData2.Rows.Clear();
                        btn_EditBegin.Visible = false;
                        btn_EditEnd.Visible = false;
                        btn_Save.Visible = false;
                        btn_Apply.Visible = true;
                        btn_Count.Visible = true;
                        tabControl1.SelectedTab = tabPage1;
                        break;

                    default:
                        btn_Save.Visible = false;
                        btn_Count.Visible = true;
                        break;
                }

                _TableGeneral = new DataTable();

                txt_title.Text = _TeamName + " > " + _OrderID;
            }


        }
        private void GVLoadData1()
        {
            this.Cursor = Cursors.WaitCursor;
            _ToTalMoney = 0;
            _ToTalKg = 0;
            _TotalTime = 0;

            if (_TableGeneral != null && _TableGeneral.Rows.Count > 0)
            // TH: Tải dữ liệu đã lưu sửa lại
            {
                Load_GVData1_Edit(GVData1, _TableGeneral);
                btn_Count.Visible = true;
            }
            else if (_TableRemark != null && _TableRemark.Rows.Count > 0)
            {
                DataRow[] temp = _TableRemark.Select("OrderKey='" + _OrderKey + "'");
                if (temp.Count() > 0)
                    Load_GVData1_Express(GVData1, temp.CopyToDataTable());//TH: Tính nhanh
                else
                {
                    GVData1.Rows.Clear();
                    GVSum1.Rows.Clear();
                    MessageBox.Show("Vui lòng chọn đơn hàng đã đánh dấu cập nhật nhanh!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            else
            //TH1:Tải DS nguồn để tính mới hoặc Load DS từ nhìu nguôn để tính lại từ đầu với dữ liệu đã lưu
            {
                DataTable zTable = HoTroSanXuat.CongNhanChuaChiLai(_OrderKey, _TeamKey, _FromDate, _ToDate); //Caculater_Productivity_Data.ListEmployeeGeneral(_OrderKey, _TeamKey, dte_Date.Value, 0);
                Load_GVData1_Refresh(GVData1, zTable);
            }
            InitData_GVSum(GVData1, GVSum1);
            this.Cursor = Cursors.Default;
        }
        private void GVLoadData2()
        {
            this.Cursor = Cursors.WaitCursor;
            int i = 0;
            GVData2.Rows.Clear();
            DataTable zTable = HoTroSanXuat.CongNhanChiLaiXong(_OrderKey, _FromDate, _ToDate); //Caculater_Productivity_Data.ListEmployeeGeneral(_OrderKey, _TeamKey, dte_Date.Value, 1);
            foreach (DataRow nRow in zTable.Rows)
            {
                if (_Status == 1)
                {
                    GVData2.Rows.Add();
                    DataGridViewRow nRowView = GVData2.Rows[i];
                    nRowView.Cells["No"].Value = (i + 1).ToString();
                    nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                    nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                    nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                    if (nRow["Time"].ToString().Trim() != "")
                    {
                        float zTime = nRow["Time"].ToFloat();
                        _TotalTime += zTime;
                        nRowView.Cells["Time"].Value = zTime.ToString("n1");
                    }
                    else
                    {
                        nRowView.Cells["Time"].Value = 0;
                    }
                    if (nRow["KgEat"].ToString() != "")
                    {
                        float zSoLuongthanhPham = float.Parse(nRow["KgEat"].ToString().Trim());
                        _ToTalKg += zSoLuongthanhPham;
                        nRowView.Cells["KgProduct"].Value = zSoLuongthanhPham.ToString("n1");
                    }
                    else
                    {
                        nRowView.Cells["KgProduct"].Value = 0;
                    }
                    if (nRow["MoneyEat"].ToString() != "")
                    {
                        double zMoney = double.Parse(nRow["MoneyEat"].ToString().Trim());
                        _ToTalMoney += zMoney;
                        nRowView.Cells["MoneyProduct"].Value = zMoney.ToString("n0");
                    }
                    else
                    {
                        nRowView.Cells["MoneyProduct"].Value = "0";
                    }
                    i++;
                }
            }
            InitData_GVSum(GVData2, GVSum2);
            this.Cursor = Cursors.Default;
        }
        private void GVData1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            int i = 0;
            Order_Employee_Info zOrder_Employee;
            if (GVData1.Rows[e.RowIndex].Tag == null)
            {
                zOrder_Employee = new Order_Employee_Info();
                GVData1.Rows[e.RowIndex].Tag = zOrder_Employee;
            }
            else
            {
                zOrder_Employee = (Order_Employee_Info)GVData1.Rows[e.RowIndex].Tag;
            }
            if (GVData1.CurrentCell.ColumnIndex == 1)
            {
                if (GVData1.CurrentCell.Value != null)
                {
                    string EmployeeID = GVData1.CurrentCell.Value.ToString();
                    Employee_Info zEm = new Employee_Info();
                    zEm.GetEmployee(EmployeeID);
                    if (zOrder_Employee.Key == 0)
                    {
                        if (zEm.Key == "")
                        {
                            MessageBox.Show("Không có tên nhân viên này", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            GVData1.CurrentRow.Cells[1].Value = "";
                            GVData1.CurrentRow.Cells[2].Value = "";
                            GVData1.CurrentRow.Cells[6].Value = "";
                        }
                        else
                        {
                            int nRowCount = Employee_Data.Count_CardID(EmployeeID, _TeamKey);
                            if (nRowCount > 0)
                            {
                                for (int y = 0; y < GVData1.Rows.Count - 2; y++)
                                {
                                    if (EmployeeID == GVData1.Rows[y].Cells["EmployeeID"].Value.ToString())
                                    {
                                        i++;
                                    }
                                }
                                if (i > 0)
                                {
                                    MessageBox.Show("Đã có trong danh sách", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    GVData1.CurrentRow.Cells[1].Value = "";
                                    GVData1.CurrentRow.Cells[2].Value = "";
                                    GVData1.CurrentRow.Cells[6].Value = "";
                                }
                                if (i == 0)
                                {
                                    nRowCount = Employee_Data.Count(_TeamKey, EmployeeID, _OrderKey);
                                    if (nRowCount > 0)
                                    {
                                        MessageBox.Show("Nhân viên này đã có rồi!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    else
                                    {
                                        nRowCount = Employee_Data.Count_CardID(EmployeeID, _TeamKey);
                                        if (nRowCount > 0)
                                        {
                                            ShowProductInGridView(e.RowIndex);
                                            GVData1.CurrentRow.Cells[0].Tag = zEm.Key;
                                            GVData1.CurrentRow.Cells[2].Value = zEm.FullName;
                                        }
                                        else
                                        {
                                            ShowProductInGridView(e.RowIndex);
                                            GVData1.CurrentRow.Cells[0].Tag = zEm.Key;
                                            GVData1.CurrentRow.Cells[2].Value = zEm.FullName;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ShowProductInGridView(e.RowIndex);
                                GVData1.CurrentRow.Cells[0].Tag = zEm.Key;
                                GVData1.CurrentRow.Cells[2].Value = zEm.FullName;
                            }
                        }
                        zOrder_Employee.RecordStatus = 1;
                    }
                }
            }
        }
        private void GVData1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GVData1.Rows.Count; i++)
                    {
                        if (GVData1.Rows[i].Selected)
                        {
                            if (GVData1.Rows[i].Cells["EmployeeID"].Tag != null)
                            {
                                GVData1.Rows[i].DefaultCellStyle.BackColor = Color.Red; ;
                                GVData1.Rows[i].Cells["Time"].Tag = 99;
                                GVData1.Rows[i].ReadOnly = true;
                                GVData1.Rows[i].Cells["Message"].Value = "Đã xóa";
                            }
                        }
                    }
                }
            }
        }
        private void ShowProductInGridView(int RowIndex)
        {
            DataGridViewRow zRowView = GVData1.Rows[RowIndex];
            zRowView.Cells["No"].Value = (RowIndex + 1).ToString();
        }
        #endregion

        #region [Progess Load GVData1]
        //TH1:Tải DS nguồn để tính mới hoặc Load DS từ nhìu nguôn để tính lại từ đầu với dữ liệu đã lưu
        private void Load_GVData1_Refresh(DataGridView GV, DataTable Table)
        {
            int i = 0;
            GV.Rows.Clear();
            _RowDataDefault = Table.Rows.Count;
            foreach (DataRow nRow in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                if (nRow["Time"].ToString().Trim() != "")
                {
                    float zTime = nRow["Time"].ToFloat();
                    _TotalTime += zTime;
                    nRowView.Cells["Time"].Value = zTime.ToString("n1");
                }
                else
                {
                    nRowView.Cells["Time"].Value = 0;
                }
                nRowView.Cells["Time"].Tag = 1;
                if (nRow["KgEat"].ToString() != "")
                {
                    float zSoLuongthanhPham = float.Parse(nRow["KgEat"].ToString().Trim());
                    _ToTalKg += zSoLuongthanhPham;
                    nRowView.Cells["Kg"].Value = zSoLuongthanhPham.ToString("n1");
                }
                else
                {
                    nRowView.Cells["Kg"].Value = 0;
                }
                if (nRow["MoneyEat"].ToString() != "")
                {
                    double zMoney = double.Parse(nRow["MoneyEat"].ToString().Trim());
                    _ToTalMoney += zMoney;
                    nRowView.Cells["Money"].Value = zMoney.ToString("n0");
                }
                else
                {
                    nRowView.Cells["Money"].Value = "0";
                }
                i++;
            }
            GV.ClearSelection();
        }
        //TH3: Tải dữ liệu đã lưu sửa lại
        private void Load_GVData1_Edit(DataGridView GV, DataTable Table)
        {
            int k = 0;
            GV.Rows.Clear();
            _RowDataDefault = Table.Rows.Count;
            foreach (DataRow nRow in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[k];
                nRowView.Cells["No"].Value = (k + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                if (nRow["Time"] != null && nRow["Time"].ToString().Trim() != "")
                {
                    float zTime = nRow["Time"].ToFloat();
                    _TotalTime += zTime;
                    nRowView.Cells["Time"].Value = zTime.ToString("n1");
                }
                else
                {
                    nRowView.Cells["Time"].Value = 0;
                }
                nRowView.Cells["Time"].Tag = nRow["Edit"].ToString().Trim();
                if (nRow["KgEat"] != null && nRow["KgEat"].ToString() != "")
                {
                    float zSoLuongthanhPham = float.Parse(nRow["KgEat"].ToString().Trim());
                    _ToTalKg += zSoLuongthanhPham;
                    nRowView.Cells["KgProduct"].Value = zSoLuongthanhPham.ToString("n1");
                }
                else
                {
                    nRowView.Cells["KgProduct"].Value = 0;
                }
                if (nRow["Kg_Eat"] != null && nRow["Kg_Eat"].ToString() != "")
                {
                    float zSoKgDemve = float.Parse(nRow["Kg_Eat"].ToString().Trim());
                    nRowView.Cells["Kg"].Value = zSoKgDemve.ToString("n1");
                }
                else
                {
                    nRowView.Cells["Kg"].Value = 0;
                }
                if (nRow["MoneyEat"] != null && nRow["MoneyEat"].ToString() != "")
                {
                    double zMoney = double.Parse(nRow["MoneyEat"].ToString().Trim());
                    _ToTalMoney += zMoney;
                    nRowView.Cells["MoneyProduct"].Value = zMoney.Ton1String();
                }
                else
                {
                    nRowView.Cells["MoneyProduct"].Value = "0";
                }
                if (nRow["Money_Eat"] != null && nRow["Money_Eat"].ToString() != "")
                {
                    double zMoney = double.Parse(nRow["Money_Eat"].ToString().Trim());
                    nRowView.Cells["Money"].Value = zMoney.Ton1String();
                }
                else
                {
                    nRowView.Cells["Money"].Value = "0";
                }
                k++;
            }
            GV.ClearSelection();
        }
        //TH2:Tải dữ liệu Tính nhanh
        private void Load_GVData1_Express(DataGridView GV, DataTable Table)
        {
            int i = 0;
            GV.Rows.Clear();
            _RowDataDefault = Table.Rows.Count;
            DataTable BangTam = HoTroSanXuat.CongNhanChuaChiLai(_OrderKey, _TeamKey, _FromDate, _ToDate); //Caculater_Productivity_Data.ListEmployeeGeneral(_OrderKey, _TeamKey, dte_Date.Value, 0);
            foreach (DataRow nRow in Table.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[i];
                string zEmployeeKey = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                if (nRow["Time"].ToString().Trim() != "")
                {
                    float zTime = nRow["Time"].ToFloat();
                    _TotalTime += zTime;
                    nRowView.Cells["Time"].Value = zTime.ToString("n1");
                }
                else
                {
                    nRowView.Cells["Time"].Value = 0;
                }
                nRowView.Cells["Time"].Tag = nRow["Edit"].ToString().Trim();
                if (nRowView.Cells["Time"].Tag.ToString() == "99")
                {
                    GV.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    GV.Rows[i].ReadOnly = true;
                }
                DataRow[] nRowBangTam = BangTam.Select("EmployeeKey='" + zEmployeeKey + "'");
                if (nRowBangTam.Count() > 0)
                {
                    if (nRowBangTam[0]["KgEat"].ToString() != "")
                    {
                        float zSoLuongthanhPham = float.Parse(nRowBangTam[0]["KgEat"].ToString().Trim());
                        _ToTalKg += zSoLuongthanhPham;
                        nRowView.Cells["Kg"].Value = zSoLuongthanhPham.ToString("n1");
                    }
                    else
                    {
                        nRowView.Cells["Kg"].Value = 0;
                    }
                    if (nRowBangTam[0]["MoneyEat"].ToString() != "")
                    {
                        double zMoney = double.Parse(nRowBangTam[0]["MoneyEat"].ToString().Trim());
                        _ToTalMoney += zMoney;
                        nRowView.Cells["Money"].Value = zMoney.Ton1String();
                    }
                    else
                    {
                        nRowView.Cells["Money"].Value = "0";
                    }
                }
                else
                {
                    if (nRow["KgEat"].ToString() != "")
                    {
                        float zSoLuongthanhPham = float.Parse(nRow["KgEat"].ToString().Trim());
                        _ToTalKg += zSoLuongthanhPham;
                        nRowView.Cells["Kg"].Value = zSoLuongthanhPham.ToString("n1");
                    }
                    else
                    {
                        nRowView.Cells["Kg"].Value = 0;
                    }
                    if (nRow["MoneyEat"].ToString() != "")
                    {
                        double zMoney = double.Parse(nRow["MoneyEat"].ToString().Trim());
                        _ToTalMoney += zMoney;
                        nRowView.Cells["Money"].Value = zMoney.Ton1String();
                    }
                    else
                    {
                        nRowView.Cells["Money"].Value = "0";
                    }
                }
                i++;
            }
            GV.ClearSelection();
        }
        #endregion

        #region [Process Event]
        private void btn_Count_Click(object sender, EventArgs e)
        {
            if (_OrderKey.Length > 0)
            {
                Caculator_Order();
                InitData_GVSum(GVData1, GVSum1);
            }
            else
            {
                MessageBox.Show("Vui lòng chọn thông tin đơn hàng trước khi tính !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            txt_title.Text = "Thông tin công nhân";
            string zResult = Save_Order();
            if (zResult == "")
            {
                MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                _Status = 1;
                GVLoadData2();
                GVData1.Rows.Clear();
                GVSum1.Rows.Clear();
                tabControl1.SelectedTab = tabPage2;
                btn_Apply.Visible = false;
                btn_Save.Visible = false;
                this.Cursor = Cursors.Default;
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Cursor = Cursors.Default;
            }

        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            if (_OrderKey.Length > 0)
            {
                Frm_Search_Employee frm = new Frm_Search_Employee();
                frm.OrderKey = _OrderKey;
                frm.ShowDialog();
                if (frm.EmployeeKey.Length > 0)
                {
                    int n = 0;
                    int tongdong = GVData1.Rows.Count - 1;
                    GVData1.Rows.Add();
                    GVData1.Rows[tongdong].Cells["No"].Value = (tongdong + 1).ToString();
                    GVData1.Rows[tongdong].Cells["EmployeeID"].Tag = frm.EmployeeKey.ToString();
                    GVData1.Rows[tongdong].Cells["EmployeeID"].Value = frm.EmployeeID.ToString();
                    GVData1.Rows[tongdong].Cells["EmployeeName"].Value = frm.EmployeeName.ToString();
                    GVData1.Rows[tongdong].Cells["Time"].Value = frm.Time.ToString();
                    GVData1.Rows[tongdong].Cells["Kg"].Value = frm.KgProduct.ToString();
                    GVData1.Rows[tongdong].Cells["Money"].Value = frm.Money.ToString("n0");
                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn đơn hàng muốn chuyến nhóm");
            }
        }
        private void btn_Apply_Click(object sender, EventArgs e)
        {
            if (btn_Apply.Tag.ToInt() == 1)
            {
                DataTable In_Table = new DataTable();
                In_Table = HoTroSanXuat.DonHangThucHienChiaLai(_TeamKey, _FromDate, _ToDate); //Order_Data.List_General(_TeamKey, dte_Date.Value);

                int n = 0;
                for (int i = 0; i < In_Table.Rows.Count; i++)
                {
                    if (In_Table.Rows[i]["Status"].ToString().Trim() == "")
                    {
                        n++;
                    }
                }
                if (n > 0 && _Status == 0)
                {
                    btn_Apply.Text = "Đóng cập nhật";
                    Frm_Production_Adjusted_Remark frm = new Frm_Production_Adjusted_Remark();
                    frm.TeamKey = _TeamKey;
                    frm.OrderKey = _OrderKey;
                    frm.Date = dte_Date.Value;
                    frm.Status = _Status;
                    frm.ShowDialog();
                    _TableRemark = frm._TableRemark;
                    if (_TableRemark != null && _TableRemark.Rows.Count > 0)
                    {
                        btn_Apply.Tag = 0;
                        tabControl1.SelectedTab = tabPage1;
                        GVLoadData1();
                        _Dangsua = 1;
                    }
                    else
                    {
                        btn_Apply.Text = "Thêm nhanh";
                        btn_Apply.Tag = 1;
                        _Dangsua = 0;
                        _TableRemark = null;
                    }

                }
                else
                {
                    if (_Status == 1)
                    {
                        MessageBox.Show("Vui lòng chọn đơn hàng chưa tính!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Không tìm thấy đơn hàng chưa tính!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                }

            }
            else
            {
                DialogResult dlr = MessageBox.Show("Vui lòng lưu tất cả dữ liệu trước khi thực hiện thao tác này", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dlr == DialogResult.Yes)
                {
                    btn_Apply.Text = "Thêm nhanh";
                    _TableRemark = new DataTable();
                    btn_Apply.Tag = 1;
                    _Dangsua = 0;
                    GVLoadData1();

                }
            }

        }
        private void btn_EditBegin_Click(object sender, EventArgs e)
        {
            txt_title.Text = "Tính lại đơn hàng từ ban đầu !.";
            GVLoadData1();
            btn_Apply.Visible = false;
            btn_Count.Visible = true;
            btn_EditEnd.Visible = false;
            GVData2.Rows.Clear();
            GVSum2.Rows.Clear();
            tabControl1.SelectedTab = tabPage1;
        }
        private void btn_EditEnd_Click(object sender, EventArgs e)
        {
            txt_title.Text = "Sửa lại đơn hàng đã tính trước đó !.";

            //Tạo bảng nhớ tạm để so sánh dữ liệu
            _TableGeneral = HoTroSanXuat.CongNhanChuaChiLai(_OrderKey, _TeamKey, _FromDate, _ToDate);//Caculater_Productivity_Data.ListEmployeeGeneral(_OrderKey, _TeamKey, dte_Date.Value, 1);
            _TableGeneral.Columns.Add("Edit");
            for (int j = 0; j < _TableGeneral.Rows.Count; j++)
            {
                _TableGeneral.Rows[j]["Edit"] = 1;
            }
            GVLoadData1();
            GVData2.Rows.Clear();
            GVSum2.Rows.Clear();
            btn_Apply.Visible = false;
            btn_EditEnd.Visible = false;
            btn_EditBegin.Visible = false;
            btn_Count.Visible = true;
            tabControl1.SelectedTab = tabPage1;
            _TableGeneral = new DataTable();
        }
        #endregion

        #region [Progess Caculator + Save Order]
        private void Caculator_Order()
        {
            //Tính tổng 
            _TotalTime = 0;
            _ToTalKg = 0;
            _ToTalMoney = 0;
            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                if (GVData1.Rows[i].Cells["Time"].Tag.ToString() != "99")
                {
                    _TotalTime += float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                    _ToTalKg += float.Parse(GVData1.Rows[i].Cells["Kg"].Value.ToString());
                    _ToTalMoney += double.Parse(GVData1.Rows[i].Cells["Money"].Value.ToString());
                }
            }
            if (_TotalTime == 0)
            {
                MessageBox.Show("Tổng thời gian bằng 0.Lỗi");
                btn_Save.Visible = false;
                btn_Count.Visible = true;
            }
            else
            {
                _TBKg = _ToTalKg / _TotalTime;
                _TBMoney = _ToTalMoney / _TotalTime;
                //Tính cá nhân
                for (int i = 0; i < GVData1.Rows.Count - 1; i++)
                {
                    if (GVData1.Rows[i].Cells["Time"].Tag.ToString() != "99")
                    {
                        float zTime = float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                        GVData1.Rows[i].Cells["KgProduct"].Value = (_TBKg * zTime).ToString("n1");
                        GVData1.Rows[i].Cells["MoneyProduct"].Value = (_TBMoney * zTime).Ton1String();
                    }
                    else
                    {
                        GVData1.Rows[i].Cells["Time"].Value = 0;
                        GVData1.Rows[i].Cells["Kg"].Value = 0;
                        GVData1.Rows[i].Cells["KgProduct"].Value = 0;
                        GVData1.Rows[i].Cells["MoneyProduct"].Value = 0;
                    }
                }
                btn_Save.Visible = true;
                btn_Count.Visible = false;
            }
        }
        private string Save_Order()
        {
            string zMessage = "";
            for (int j = 0; j < GVData1.Rows.Count - 1; j++)
            {
                if (GVData1.Rows[j].Cells["EmployeeID"].Value.ToString() != "")
                {
                    if (GVData1.Rows[j].Cells["Time"].Tag.ToString() != "99")
                    {
                        if (j >= _RowDataDefault)
                            GVData1.Rows[j].Cells["Time"].Tag = 0; //tạo mới
                        else
                            GVData1.Rows[j].Cells["Time"].Tag = 1;//chỉnh sửa
                    }
                }
            }
            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                zOrder = new Order_Money_Info();
                zOrder.OrderKey = _OrderKey;
                zOrder.OrderDate = dte_Date.Value;
                zOrder.TeamKey = _TeamKey;
                zOrder.StageKey = _StageKey;
                zOrder.EmployeeKey = GVData1.Rows[i].Cells["EmployeeID"].Tag.ToString();
                zOrder.EmployeeID = GVData1.Rows[i].Cells["EmployeeID"].Value.ToString();
                zOrder.EmployeeName = GVData1.Rows[i].Cells["EmployeeName"].Value.ToString();
                zOrder.TimeEat = float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                zOrder.KgEat = float.Parse(GVData1.Rows[i].Cells["KgProduct"].Value.ToString());
                if (GVData1.Rows[i].Cells["MoneyProduct"].Value != null
                    || GVData1.Rows[i].Cells["MoneyProduct"].Value.ToString() != "")
                {
                    zOrder.MoneyEat = float.Parse(GVData1.Rows[i].Cells["MoneyProduct"].Value.ToString());
                }
                else
                {
                    zOrder.MoneyEat = 0;
                }
                zOrder.Category_Eat = 1;

                if (GVData1.Rows[i].Cells["Time"].Tag.ToString() == "99")
                {
                    zOrder.ModifiedBy = SessionUser.UserLogin.Key;
                    zOrder.ModifiedName = SessionUser.Personal_Info.FullName;
                    zOrder.Delete_General();//Update lại  dữ liệu = 0 nếu dòng đó đã chọn xóa
                }
                else
                {
                    //Lưu những gì vào bản chính
                    _Count = Order_Money_Data.Find_Employee(_OrderKey, zOrder.EmployeeKey);
                    if (_Count == 0)
                    {
                        zOrder.CreatedBy = SessionUser.UserLogin.Key;
                        zOrder.CreatedName = SessionUser.Personal_Info.FullName;
                        zOrder.Create_General();
                    }
                    else
                    {
                        zOrder.ModifiedBy = SessionUser.UserLogin.Key;
                        zOrder.ModifiedName = SessionUser.Personal_Info.FullName;
                        zOrder.Update_General();
                    }
                }
                if (zOrder.Message.Substring(0, 2) != "11" && zOrder.Message.Substring(0, 2) != "20")
                {
                    zMessage += zOrder.Message;
                }
            }
            if (zMessage == "")
            {
                Order_Log_Info zOrder_Log = new Order_Log_Info();
                zOrder_Log.OrderKey = _OrderKey;
                zOrder_Log.OrderID = _OrderID;
                zOrder_Log.OrderDate = dte_Date.Value;
                zOrder_Log.Status = 1;
                _Count = Order_Log_Data.Find_Log(_OrderKey);
                if (_Count == 0)
                {
                    zOrder_Log.CreatedBy = SessionUser.UserLogin.Key;
                    zOrder_Log.CreatedName = SessionUser.Personal_Info.FullName;
                    zOrder_Log.Create();
                }
                else
                {
                    zOrder_Log.ModifiedBy = SessionUser.UserLogin.Key;
                    zOrder_Log.ModifiedName = SessionUser.Personal_Info.FullName;
                    zOrder_Log.Update();
                }

            }
            return zMessage;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion        
    }
}