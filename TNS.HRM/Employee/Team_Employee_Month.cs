﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;
using TNS.Misc;
using TNS.SYS;

namespace TNS.HRM
{
    public class Team_Employee_Month
    {
        public static DataTable Search(int Branch, int Department, int TeamKey, string Name, string EmployeeID,DateTime DateWrite)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeKey,EmployeeID,EmployeeName,TeamID,
CASE 
WHEN Gender=0 THEN N'Nữ'
WHEN Gender=1 THEN N'Nam'
END AS Gender
,IssueID,BankCode,
CASE 
WHEN OverTime=0 THEN N'Không'
WHEN OverTime=1 THEN N'Có'
END AS OverTime
FROM [dbo].[HRM_Team_Employee_Month] 
WHERE (Month(DateWrite) =Month(@DateInput) AND YEAR(DateWrite) =YEAR(@DateInput) )
AND RecordStatus <> 99
";
            if (Branch > 0)
            {
                zSQL += " AND BranchKey = @BranchKey";
            }
            if (Department > 0)
            {
                zSQL += " AND DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                zSQL += " AND TeamKey = @TeamKey";
            }
            if (Name.Length > 0)
            {
                zSQL += " AND EmployeeName LIKE @Name";
            }
            if (EmployeeID.Length > 0)
            {
                zSQL += " AND EmployeeID LIKE @EmployeeID";
            }
           
            zSQL += " ORDER BY LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = "%" + EmployeeID + "%";

                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Branch;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@DateInput", SqlDbType.DateTime).Value = DateWrite;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string Refresh(DateTime DateWrite)
        {
            string zResult = "";
            string zSQL = @"HRM_Team_Employee_Month_INSERT";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.CommandTimeout = 350;
                zCommand.Parameters.Add("@DateInput", SqlDbType.DateTime).Value = DateWrite;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = SessionUser.UserLogin.Key;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = SessionUser.UserLogin.EmployeeName;

                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = "08" + ex.ToString();
            }
            return zResult;
        }
        public static int OptionTeamKey(string EmployeeKey,DateTime DateWrite)
        {
            int zResult = 0;
            string zSQL = @"SELECT [dbo].[Fn_GetOptionChangeTeamKey](@EmployeeKey,@DateInput)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DateInput", SqlDbType.DateTime).Value = DateWrite;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zMesage = "08" + ex.ToString();
            }
            return zResult;
        }


    }
    
}
