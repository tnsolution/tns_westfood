﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Order_Money_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM FTR_Order_Money WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static string Delete_Employee(string OrderKey, string EmployeeKey)
        {
           
            string zSQL = @"
DELETE FROM FTR_Order_Money WHERE OrderKey = @OrderKey AND EmployeeKey = @EmployeeKey
DELETE FROM FTR_Order_Money_Tam WHERE OrderKey = @OrderKey AND EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                string Result = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return string.Empty;
        }

        public static int Find_Employee(string OrderKey, string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM FTR_Order_Money WHERE OrderKey = @OrderKey AND EmployeeKey = @EmployeeKey ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        public static int Find_Employee_Tam(string OrderKey, string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM FTR_Order_Money_Tam WHERE OrderKey = @OrderKey AND EmployeeKey = @EmployeeKey ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        public static DataTable List_Team(int zTeamKey, DateTime FromDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"RPR_SalaryTeam";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = zTeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Team_Month(int zTeamKey, DateTime FromDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();

            string zSQL = @"
	SELECT A.EmployeeKey,A.EmployeeName,A.EmployeeID  , 
	[dbo].[FTR_Sum_Money](A.EmployeeKey,@FromDate,@ToDate) AS Money ,
	[dbo].[FTR_Sum_Money_Dif](A.EmployeeKey,@FromDate,@ToDate) AS MoneyDif ,
	[dbo].[FTR_Sum_Money_Dif_Holiday](A.EmployeeKey,@FromDate,@ToDate) AS  MoneyDifHoliday	 ,
	[dbo].[FTR_Sum_Money_Dif_Sunday](A.EmployeeKey,@FromDate,@ToDate)  AS MoneyDifSunday,
	[dbo].[FTR_Sum_Money_Holidays](A.EmployeeKey,@FromDate,@ToDate) AS MoneyHoliday ,
	[dbo].[FTR_Sum_Money_Sunday](A.EmployeeKey,@FromDate,@ToDate) AS MoneySunday ,
	[dbo].[FTR_Sum_MoneyTimeKeep](A.EmployeeKey,@FromDate,@ToDate) AS MoneyTimeKeep 
	FROM [dbo].[FTR_Money_Date] A 
	WHERE TeamKey = @TeamKey
	GROUP BY  A.EmployeeKey,A.EmployeeName,A.EmployeeID
	ORDER BY LEN(EmployeeID), EmployeeID
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = zTeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable GetEmployee(string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT TeamKey FROM HRM_Employee WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = new Guid(EmployeeKey);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int Find_OrderID(string OrderKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM FTR_Order_Money WHERE OrderKey = @OrderKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
    }
}
