﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_EmployeeChangeTeam : Form
    {
        int _Key = 0;
        public Frm_EmployeeChangeTeam()
        {
            InitializeComponent();
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GVData_Click;
            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            dte_Date.Value = SessionUser.Date_Work;
            InitGV_Layout(GVData);
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND TeamKey <> 98 AND BranchKey=4
                            ORDER BY RANK";
            LoadDataToToolbox.KryptonComboBox(cbo_Team, zSQL, "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_TeamNew, zSQL, "--Chọn--");
        }

        private void Frm_EmployeeChangeTeam_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {

            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                    cbo_Team.SelectedValue = 0;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                    cbo_Team.SelectedValue = zinfo.TeamKey;
                }
            }
        }
        #region[Progess]
        private void LoadInfo()
        {
            EmployeeChangeTeam_Info zInfo = new EmployeeChangeTeam_Info(_Key);
            txt_EmployeeID.Tag = zInfo.EmployeeKey;
            if (zInfo.Key != 0)
            {
                txt_EmployeeID.ReadOnly = true;
            }
            if (zInfo.DateWrite != DateTime.MinValue)
            {
                dte_Date.Value = zInfo.DateWrite;
            }

            cbo_Team.SelectedValue = zInfo.TeamKey;
            txt_EmployeeName.Text = zInfo.EmployeeName;
            txt_EmployeeID.Text = zInfo.EmployeeID;
            cbo_TeamNew.SelectedValue = zInfo.TeamNewKey;
            txt_Description.Text = zInfo.Description;

        }
        private void Load_Refresh()
        {
            _Key = 0;
            dte_Date.Value = SessionUser.Date_Work;
            txt_EmployeeID.Tag = null;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_Description.Text = "";
            cbo_Team.SelectedValue = 0;
            cbo_TeamNew.SelectedValue = 0;
            txt_EmployeeID.ReadOnly = false;
            dte_Date.Focus();
        }
        private void LoadData()
        {
            DataTable ztb = EmployeeChangeTeam_Data.List_EmployeeChange(dte_FromDate.Value, dte_ToDate.Value);
            if (ztb.Rows.Count > 0)
                InitGV_Data(GVData, ztb);
            else
                GVData.Rows.Clear();
        }
        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Tag == null || txt_EmployeeID.Text.Trim() == "")
            {
                MessageBox.Show("Chưa chọn nhân viên!");
            }
            else if (cbo_Team.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chưa chọn nhóm tổ gốc!");
            }
            else if (cbo_TeamNew.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chưa chọn tổ chuyển đến");
            }
            else
            {
                EmployeeChangeTeam_Info zInfo = new EmployeeChangeTeam_Info(_Key);
                zInfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                if (dte_Date.Value != DateTime.MinValue)
                {
                    zInfo.DateWrite = dte_Date.Value;
                }

                Team_Info zTeam;

                zInfo.TeamKey = cbo_Team.SelectedValue.ToInt();
                zTeam = new Team_Info(zInfo.TeamKey);
                zInfo.TeamID = zTeam.TeamID;
                zInfo.TeamName = zTeam.TeamName;
                zInfo.EmployeeName = txt_EmployeeName.Text;
                zInfo.EmployeeID = txt_EmployeeID.Text;
                zInfo.TeamNewKey = cbo_TeamNew.SelectedValue.ToInt();
                zTeam = new Team_Info(zInfo.TeamNewKey);
                zInfo.TeamIDNew = zTeam.TeamID;
                zInfo.TeamNameNew = zTeam.TeamName;
                zInfo.Description = txt_Description.Text;

                zInfo.CreatedBy = SessionUser.UserLogin.Key;
                zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zInfo.Save();
                if (zInfo.Message.Length == 0)
                {
                    UpdateTeam(true);// update bảng nhân sự
                    RefreshTeam();
                    MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _Key = zInfo.Key;
                    LoadInfo();
                    LoadData();

                }
                else
                {
                    MessageBox.Show("Vui lòng liên hệ IT", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


        }
        private void UpdateTeam(bool Type)
        {
            Team_Info zTeam;
            Employee_Info zEmployee = new Employee_Info(txt_EmployeeID.Tag.ToString());
            if (Type == true)
            {
                zTeam = new Team_Info(cbo_TeamNew.SelectedValue.ToInt());
                zEmployee.TeamKey = zTeam.Key;
                zEmployee.DepartmentKey = zTeam.DepartmentKey;
                zEmployee.BranchKey = zTeam.BranchKey;
            }
            else
            {
                zTeam = new Team_Info(cbo_Team.SelectedValue.ToInt());
                zEmployee.TeamKey = zTeam.Key;
                zEmployee.DepartmentKey = zTeam.DepartmentKey;
                zEmployee.BranchKey = zTeam.BranchKey;
            }
            zEmployee.Save();
        }
        private void RefreshTeam()
        {
            string zMessage = Team_Employee_Month.Refresh(dte_FromDate.Value);
        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key != 0)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    EmployeeChangeTeam_Info zInfo = new EmployeeChangeTeam_Info(_Key);
                    zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zInfo.Delete();
                    if (zInfo.Message.Length == 0)
                    {
                        UpdateTeam(false);
                        RefreshTeam();
                        MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Load_Refresh();
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng liên hệ IT", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
            else
            {
                MessageBox.Show("Vui lòng chọn phiếu !.", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            Load_Refresh();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        #endregion

        private void GVData_Click(object sender, EventArgs e)
        {
            if (GVData.CurrentRow.Tag != null)
            {
                _Key = GVData.CurrentRow.Tag.ToInt();
                LoadInfo();
            }
        }
        private void txt_Amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        void InitGV_Layout(DataGridView GV)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("DateWrite", "Ngày chuyển");
            GV.Columns.Add("EmployeeName", "Họ và tên");
            GV.Columns.Add("EmployeeID", "Số thẻ");
            GV.Columns.Add("TeamID", "Tổ/nhóm gốc");
            GV.Columns.Add("TeamIDNew", "Tổ/nhóm chuyển đến");
            GV.Columns.Add("Description", "Nội dung");

            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["DateWrite"].Width = 80;
            GV.Columns["DateWrite"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EmployeeName"].Width = 150;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["EmployeeID"].Width = 80;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["TeamID"].Width = 100;
            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["TeamIDNew"].Width = 100;
            GV.Columns["TeamIDNew"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Description"].Width = 250;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 45;
            #endregion
        }
        void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GVData.Rows.Clear();
            foreach (DataRow nRow in zTable.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (no + 1).ToString();
                if (nRow["DateWrite"].ToString() != "")
                {
                    DateTime zDateWrite = DateTime.Parse(nRow["DateWrite"].ToString());
                    nRowView.Cells["DateWrite"].Value = zDateWrite.ToString("dd/MM/yyyy");
                }
                else
                {
                    nRowView.Cells["DateWrite"].Value = "";
                }
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
                nRowView.Cells["TeamIDNew"].Value = nRow["TeamIDNew"].ToString().Trim();
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                no++;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
