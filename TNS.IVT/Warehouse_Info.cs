﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Warehouse_Info
    {
        private int _WarehouseKey = 0;
        private string _WarehouseID = "";
        private string _WarehouseName = "";
        private string _Description = "";
        private int _BranchKey = 0;
        private int _CategoryKey = 0;
        private string _Message = "";
        private int _Rank = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private DateTime? _CreatedOn = null;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime? _ModifiedOn = null;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";

        #region[Properties]
        public int WarehouseKey { get => _WarehouseKey; set => _WarehouseKey = value; }
        public string WarehouseID { get => _WarehouseID; set => _WarehouseID = value; }
        public string WarehouseName { get => _WarehouseName; set => _WarehouseName = value; }
        public string Description { get => _Description; set => _Description = value; }
        public int BranchKey { get => _BranchKey; set => _BranchKey = value; }
        public int CategoryKey { get => _CategoryKey; set => _CategoryKey = value; }
        public string Message { get => _Message; set => _Message = value; }

        public int Rank
        {
            get
            {
                return _Rank;
            }

            set
            {
                _Rank = value;
            }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public DateTime? CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime? ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }
        #endregion
        public Warehouse_Info()
        {

        }
        public Warehouse_Info(string WarehouseID)
        {
            string zSQL = "SELECT * FROM IVT_Warehouse WHERE WarehouseID = @WarehouseID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseID", SqlDbType.NVarChar).Value = WarehouseID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _WarehouseKey = int.Parse(zReader["WarehouseKey"].ToString());
                    _WarehouseID = zReader["WarehouseID"].ToString();
                    _WarehouseName = zReader["WarehouseName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        public void GetWareHouseName(string WareHouseName)
        {
            string zSQL = "SELECT * FROM IVT_Warehouse WHERE WarehouseName = @WarehouseName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseName", SqlDbType.NVarChar).Value = WareHouseName;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _WarehouseKey = int.Parse(zReader["WarehouseKey"].ToString());
                    _WarehouseID = zReader["WarehouseID"].ToString();
                    _WarehouseName = zReader["WarehouseName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        public Warehouse_Info(int WarehouseKey)
        {
            string zSQL = "SELECT * FROM IVT_Warehouse WHERE WarehouseKey = @WarehouseKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _Slug = int.Parse(zReader["Slug"].ToString());
                    _WarehouseKey = int.Parse(zReader["WarehouseKey"].ToString());
                    _WarehouseID = zReader["WarehouseID"].ToString();
                    _WarehouseName = zReader["WarehouseName"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
    }
}
