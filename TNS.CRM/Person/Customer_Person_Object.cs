﻿using TNS.SYS;

namespace TNS.CRM
{
    public class Customer_Person_Object:Customer_Info
    {
        private Personal_Object _PersoObject;
        public Personal_Object PersoObject
        {
            get
            {
                return _PersoObject;
            }

            set
            {
                _PersoObject = value;
            }
        }
        public Customer_Person_Object():base()
        {

        }

        public Customer_Person_Object(string CustomerKey) : base(CustomerKey)
        {
            PersoObject = new Personal_Object(CustomerKey);
        }

       

        public void SaveObject()
        {
            base.Save();
            PersoObject.ParentKey = base.Key;
            PersoObject.ParentTable = "CRM_Customer";
            PersoObject.Save_Object();
        }

        public void DeleteObject()
        {
            base.Delete();
            PersoObject.Delete_Object();
        }
    }
}
