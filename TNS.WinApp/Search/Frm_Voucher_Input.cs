﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Voucher_Input : Form
    {
        private bool IsPostBack;
        private string _FormatQuantity = "#,###,##0.00";
        private IFormatProvider _FormatProviderQuantity = new CultureInfo("en-US", true);

        private int _WarehouseKey = 0;
        private string _WarehouseName = "";
        private string _ProductID = "";
        private string _OrderKey = "";
        private string _OrderID = "";
        public string WarehouseName
        {
            get { return _WarehouseName; }
            set { _WarehouseName = value; }
        }
        public int WarehouseKey
        {
            get { return _WarehouseKey; }
            set { _WarehouseKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
        }

        public string OrderKey
        {
            get => _OrderKey;
            set => _OrderKey = value;
        }
        public string OrderID
        {
            get => _OrderID;
            set => _OrderID = value;
        }

        public Frm_Voucher_Input()
        {
            InitializeComponent();

            //-----List View
            LVData.ItemActivate += LV_Product_ItemActivate;
            btn_Search.Click += btn_Search_Click;
        }

        private void Frm_Voucher_Input_Load(object sender, EventArgs e)
        {
            Picker_From.Value = SessionUser.Date_Work;
            Picker_To.Value = SessionUser.Date_Work;
            IsPostBack = false;
            lbl_FormTitle.Text = "Sản phẩm tồn [" + _WarehouseName + "]";
            LV_Product_All_SetupLayout(LVData);
            LoadDataWareHouse();
            Utils.DrawLVStyle(ref LVData);
            Utils.SizeLastColumn_LV(LVData);
            IsPostBack = true;
        }
        private void LoadDataWareHouse()
        {
            string zSQL = "SELECT WarehouseKey,WarehouseName FROM IVT_Warehouse ";
            if (_WarehouseKey > 0)
                zSQL += " WHERE WarehouseKey = " + _WarehouseKey;

            zSQL += " ORDER BY RANK";
            LoadDataToToolbox.KryptonComboBox(cbo_Warehouse, zSQL, "--Chọn--");
        }

        #region -----ListView Product----- 
        private void LV_Product_All_SetupLayout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Sản Phẩm";
            colHead.Width = 115;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên sản phẩm";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Lương Nhập";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Lượng Xuất";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Số Lượng Tồn";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số CT";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }
        private void LV_Product_All_LoadData(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["OrderID"].ToString();

                lvi.ForeColor = Color.FromArgb(64, 64, 64);
                lvi.Font = new Font("Arial", 9);
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["UnitName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                float InputQuatity = float.Parse(nRow["InputQuatity"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = InputQuatity.ToString();
                lvi.SubItems.Add(lvsi);

                float OutputQuatity = float.Parse(nRow["OutputQuantity"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = OutputQuatity.ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = (InputQuatity - OutputQuatity).ToString();
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            lbl_Total_Record.Text = n.Ton0String();
            this.Cursor = Cursors.Default;

        }
        private void LV_Product_ItemActivate(object sender, EventArgs e)
        {
            _ProductID = "";
            _OrderID = "";
            if (LVData.SelectedItems.Count == 0)
            {
                MessageBox.Show("Vui lòng chọn đơn hàng!");
            }
            else
            {
                _ProductID = LVData.SelectedItems[0].SubItems[1].Text;
                _OrderID = LVData.SelectedItems[0].SubItems[7].Text;
                this.Close();
            }

        }
        #endregion

        #region -----Event Handler-----
        private void btn_Search_Click(object sender, EventArgs e)
        {
            DataTable zTable = new DataTable();
            if (txt_OrderID.Text.Length == 0 && txt_ProductName.Text.Length == 0)
            {
                zTable = Product_Data.Products_HaveIn_Warehouse_Search(txt_OrderID.Text, (int)cbo_Warehouse.SelectedValue, Picker_From.Value, Picker_To.Value);
            }
            if (txt_ProductName.Text.Length > 0)
            {
                zTable = Product_Data.Products_HaveIn_Warehouse_Search_Product(txt_ProductName.Text, (int)cbo_Warehouse.SelectedValue, Picker_From.Value, Picker_To.Value);
            }
            if (txt_Input.Text.Length > 0)
            {
                zTable = Product_Data.Products_HaveIn_Warehouse_Search_Input(float.Parse(txt_Input.Text), (int)cbo_Warehouse.SelectedValue, Picker_From.Value, Picker_To.Value);
            }
            LV_Product_All_LoadData(zTable);

        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
