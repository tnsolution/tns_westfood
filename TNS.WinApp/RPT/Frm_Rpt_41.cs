﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Rpt_41 : Form
    {
        string _ProductGroup = "0";
        public Frm_Rpt_41()
        {
            InitializeComponent();
            this.Load += Frm_Rpt_41_Load;
            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void Frm_Rpt_41_Load(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Click += GVData_SelChange;

            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LoadDataToToolbox.KryptonComboBox(cbo_ProductID, @"
SELECT GroupID, GroupID + ' - ' + Description 
FROM IVT_Product_Stages_Group
WHERE RecordStatus <> 99
ORDER BY GroupID", "--Tất cả--");

        }

        private void GVData_SelChange(object sender, EventArgs e)
        {
            if (GVData.RowSel > 0)
            {
                int Key = GVData.Rows[GVData.RowSel][10].ToInt();

                DataTable zTable = Report.Report_41_Detail(dte_FromDate.Value, dte_ToDate.Value, Key);
                if (zTable.Rows.Count > 0)
                {
                    InitDataDetail(zTable);
                }
            }
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;

            _ProductGroup = cbo_ProductID.SelectedValue.ToString();
            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }

        private void DisplayData()
        {
            DataTable zTable = Report.Report_41_Master(dte_FromDate.Value, dte_ToDate.Value, _ProductGroup);

            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataMaster(zTable);
                }));
            }
        }

        void InitDataMaster(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "SẢN PHẨM";
            GVData.Rows[0][2] = "CÔNG ĐOẠN";
            GVData.Rows[0][3] = "TỔNG CÔNG NHÂN";
            GVData.Rows[0][4] = "TỔNG THỜI GIAN";
            GVData.Rows[0][5] = "TỔNG SỐ RỔ";
            GVData.Rows[0][6] = "TỔNG SỐ LƯỢNG NL";
            GVData.Rows[0][7] = "TỔNG SỐ LƯỢNG TP";          
            GVData.Rows[0][8] = "NS Nguyên liệu (KG/Giờ)";
            GVData.Rows[0][9] = "NS Thành phẩm (KG/Giờ)";
            GVData.Rows[0][10] = ""; // ẩn cột này

            double TotalCol3 = 0;
            double TotalCol4 = 0;
            double TotalCol5 = 0;
            double TotalCol6 = 0;
            double TotalCol7 = 0;
            double TotalCol8 = 0;
            double TotalCol9 = 0;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = FormatMoney(rData[4]);
                GVData.Rows[rIndex + 1][5] = FormatMoney(rData[5]);
                GVData.Rows[rIndex + 1][6] = FormatMoney(rData[6]);
                GVData.Rows[rIndex + 1][7] = FormatMoney(rData[7]);
                GVData.Rows[rIndex + 1][8] = FormatMoney(rData[8]);
                GVData.Rows[rIndex + 1][9] = FormatMoney(rData[9]);
                GVData.Rows[rIndex + 1][10] = rData[0];

                TotalCol3 += rData[3].ToDouble();
                TotalCol4 += rData[4].ToDouble();
                TotalCol5 += rData[5].ToDouble();
                TotalCol6 += rData[6].ToDouble();
                TotalCol7 += rData[7].ToDouble();
                TotalCol8 += rData[8].ToDouble();
                TotalCol9 += rData[9].ToDouble();
            }

            GVData.Rows[TotalRow - 1][1] = "TỔNG";
            GVData.Rows[TotalRow - 1][2] = "";
            GVData.Rows[TotalRow - 1][3] = TotalCol3.ToString();
            GVData.Rows[TotalRow - 1][4] = FormatMoney(TotalCol4);
            GVData.Rows[TotalRow - 1][5] = FormatMoney(TotalCol5);
            GVData.Rows[TotalRow - 1][6] = FormatMoney(TotalCol6);
            GVData.Rows[TotalRow - 1][7] = FormatMoney(TotalCol7);
            GVData.Rows[TotalRow - 1][8] = FormatMoney(TotalCol8);
            GVData.Rows[TotalRow - 1][9] = FormatMoney(TotalCol9);
            

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 50;
            GVData.Cols[1].Width = 100;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[2].Width = 450;
            GVData.Cols[10].Visible = false;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Fixed = 2;

            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        void InitDataDetail(DataTable TableView)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVEmployee.Cols.Add(ToTalCol);
            GVEmployee.Rows.Add(TotalRow);

            //Row Header
            GVEmployee.Rows[0][0] = "STT";
            GVEmployee.Rows[0][1] = "HỌ TÊN";
            GVEmployee.Rows[0][2] = "MÃ SỐ";
            GVEmployee.Rows[0][3] = "TỔNG THỜI GIAN";
            GVEmployee.Rows[0][4] = "TỔNG SỐ RỔ";
            GVEmployee.Rows[0][5] = "TỔNG SỐ LƯỢNG NL";
            GVEmployee.Rows[0][6] = "TỔNG SỐ LƯỢNG TP";
            GVEmployee.Rows[0][7] = "NS Nguyên liệu (KG/Giờ)";
            GVEmployee.Rows[0][8] = "NS Thành phẩm (KG/Giờ)";

            double TotalCol3 = 0;
            double TotalCol4 = 0;
            double TotalCol5 = 0;
            double TotalCol6 = 0;
            double TotalCol7 = 0;
            double TotalCol8 = 0;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVEmployee.Rows[rIndex + 1][0] = rIndex + 1;
                GVEmployee.Rows[rIndex + 1][1] = rData[0];
                GVEmployee.Rows[rIndex + 1][2] = rData[1];
                GVEmployee.Rows[rIndex + 1][3] = FormatMoney(rData[2]);
                GVEmployee.Rows[rIndex + 1][4] = FormatMoney(rData[3]);
                GVEmployee.Rows[rIndex + 1][5] = FormatMoney(rData[4]);
                GVEmployee.Rows[rIndex + 1][6] = FormatMoney(rData[5]);
                GVEmployee.Rows[rIndex + 1][7] = FormatMoney(rData[6]);
                GVEmployee.Rows[rIndex + 1][8] = FormatMoney(rData[7]);

                TotalCol3 += rData[2].ToDouble();
                TotalCol4 += rData[3].ToDouble();
                TotalCol5 += rData[4].ToDouble();
                TotalCol6 += rData[5].ToDouble();
                TotalCol7 += rData[6].ToDouble();
                TotalCol8 += rData[7].ToDouble();
            }

            GVEmployee.Rows[TotalRow - 1][1] = "TỔNG";
            GVEmployee.Rows[TotalRow - 1][2] = "";
            GVEmployee.Rows[TotalRow - 1][3] = FormatMoney(TotalCol3);
            GVEmployee.Rows[TotalRow - 1][4] = FormatMoney(TotalCol4);
            GVEmployee.Rows[TotalRow - 1][5] = FormatMoney(TotalCol5);
            GVEmployee.Rows[TotalRow - 1][6] = FormatMoney(TotalCol6);
            GVEmployee.Rows[TotalRow - 1][7] = FormatMoney(TotalCol7);
            GVEmployee.Rows[TotalRow - 1][8] = FormatMoney(TotalCol8);

            //Style         
            GVEmployee.ExtendLastCol = false;
            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Styles.Normal.WordWrap = true;

            GVEmployee.Rows[0].Height = 40;

            GVEmployee.Cols[0].Width = 50;
            GVEmployee.Cols[1].Width = 250;

            GVEmployee.Rows.Fixed = 1;
            GVEmployee.Cols.Fixed = 1;

            GVEmployee.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVEmployee.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVEmployee.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 2; i < GVEmployee.Cols.Count; i++)
            {
                GVEmployee.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}
