﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNS.CORE
{
    //thông tin dùng tính toán trên 1 đơn hàng
    public class Order_Model
    {
        public string OrderKey { get; set; } = "";
        public string OrderID { get; set; } = "";
        public DateTime OrderDate { get; set; } = new DateTime();
        public float Quantity { get; set; } = 0; // số lượng thực hiện
        public float TotalKg { get; set; } = 0; // tổng số kg của đơn hàng
        public float TotalBasket { get; set; } = 0;// tổng số xọt 
        public float TotalTime { get; set; } = 0;   //tổng thời gian làm
        public float RateOrder { get; set; } = 0;   //tổng hệ số phụ trợ 
        public float RateDay { get; set; } = 0;    //he số ngày làm việc
        public float Price { get; set; } = 0;   //đơn giá làm việc
        public float PercentFinish { get; set; } = 0; //tỉ lệ hoàn thành đơn hàng
        public float GioNam { get; set; } = 0;// tổng giờ của nam
        public float GioNu { get; set; } = 0;//tổng giờ của nữ
        public float ToTalMoney { get; set; } = 0;// tổng tiền chính thức của đơn hàng
        public float KgTB { get; set; } = 0;//So nguyen lieu  trung binh
        public float BasketTB { get; set; } = 0;//So ro  trung binh
        public float TimeTB { get; set; } = 0;//So gio  trung binh
        public float MoneyTB { get; set; } = 0;//So tien  trung binh
    }
}
