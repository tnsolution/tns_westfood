﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.Misc;
using TNS.HRM;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_EmployeeTeamOfficeDetail : Form
    {
        private int _Key = 0;
        public int Key
        {
            get
            {
                return _Key;
            }

            set
            {
                _Key = value;
            }
        }
        public Frm_EmployeeTeamOfficeDetail()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;

            btnMini.Click += btnMini_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            btn_Copy.Click += Btn_Copy_Click;

        }

        

        private void Frm_Salary_Default_Detail_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND BranchKey = 2  AND RecordStatus < 99 ORDER BY Rank", "---- Chọn ----");
            if (Key==0)
            SetDefault();
            else
            LoadData();
        }

        private void SetDefault()
        {
            Key = 0;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_EmployeeID.Tag = null;
            txt_Description.Text = "";
            cbo_Team.SelectedValue = 0;
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = DateTime.MinValue ;
            btn_Del.Enabled = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            Txt_EmployeeID_Leave(null,null);
        }
        private void LoadData()
        {
            Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            cbo_Team.SelectedValue = zinfo.TeamKeyView ;
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            if (zinfo.ToDate == DateTime.MinValue)
            {
                dte_ToDate.Value = DateTime.MinValue;
                btn_Copy.Enabled = false;
            }
            else
            {
                dte_ToDate.Value = zinfo.ToDate;
                btn_Copy.Enabled = true;
            }
            btn_Del.Enabled = true;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã nhân viên!");
                return;
            }
            else
            {
                Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim(). ToString();
                zinfo.TeamKeyView = int.Parse(cbo_Team.SelectedValue.ToString());
                zinfo.Description = txt_Description.Text.Trim();
                DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
                DateTime zToDate = DateTime.MinValue;
                if (dte_ToDate.Value != DateTime.MinValue)
                {
                    zToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
                }
                zinfo.FromDate = zFromDate;
                zinfo.ToDate = zToDate;
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "11" || zinfo.Message == "20")
                {
                    MessageBox.Show("Cập nhật thành công!");
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show(zinfo.Message);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        MessageBox.Show("Đã xóa !");
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            Employee_TeamOfficeView_Info zinfo = new Employee_TeamOfficeView_Info(Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            cbo_Team.SelectedValue = zinfo.TeamKeyView;
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            if (zinfo.ToDate == null)
            {
                dte_ToDate.Value = DateTime.MinValue;
            }
            else
            {
                dte_ToDate.Value = zinfo.ToDate;
            }
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            _Key = 0;

            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            MessageBox.Show("Sao chép thành công!Vui lòng chỉnh sửa thông tin.");
        }

        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
