﻿namespace TNS.WinApp
{
    partial class Frm_Production_Auto_Money
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Production_Auto_Money));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.PicLoading = new System.Windows.Forms.PictureBox();
            this.btn_Run = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GVTeam = new System.Windows.Forms.DataGridView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.timerTeam = new System.Windows.Forms.Timer(this.components);
            this.timerEmployee = new System.Windows.Forms.Timer(this.components);
            this.Panel_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).BeginInit();
            this.Panel_Left.SuspendLayout();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1000, 42);
            this.HeaderControl.TabIndex = 186;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Tính lương năng xuất";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Search.Controls.Add(this.PicLoading);
            this.Panel_Search.Controls.Add(this.btn_Run);
            this.Panel_Search.Controls.Add(this.label2);
            this.Panel_Search.Controls.Add(this.label3);
            this.Panel_Search.Controls.Add(this.dte_FromDate);
            this.Panel_Search.Controls.Add(this.dte_ToDate);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 42);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(1000, 66);
            this.Panel_Search.TabIndex = 202;
            // 
            // PicLoading
            // 
            this.PicLoading.BackColor = System.Drawing.Color.Transparent;
            this.PicLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PicLoading.Dock = System.Windows.Forms.DockStyle.Right;
            this.PicLoading.Image = ((System.Drawing.Image)(resources.GetObject("PicLoading.Image")));
            this.PicLoading.Location = new System.Drawing.Point(930, 0);
            this.PicLoading.Name = "PicLoading";
            this.PicLoading.Size = new System.Drawing.Size(68, 64);
            this.PicLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.PicLoading.TabIndex = 206;
            this.PicLoading.TabStop = false;
            // 
            // btn_Run
            // 
            this.btn_Run.Location = new System.Drawing.Point(209, 11);
            this.btn_Run.Name = "btn_Run";
            this.btn_Run.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Run.Size = new System.Drawing.Size(102, 40);
            this.btn_Run.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Run.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Run.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Run.TabIndex = 205;
            this.btn_Run.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Run.Values.Image")));
            this.btn_Run.Values.Text = "Tính toán";
            this.btn_Run.Click += new System.EventHandler(this.btn_Run_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(24, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 108;
            this.label2.Text = "Từ Ngày";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(16, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 127;
            this.label3.Text = "Đến Ngày";
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(79, 9);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(124, 20);
            this.dte_FromDate.TabIndex = 0;
            this.dte_FromDate.Value = new System.DateTime(((long)(0)));
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(79, 35);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(124, 20);
            this.dte_ToDate.TabIndex = 1;
            this.dte_ToDate.Value = new System.DateTime(((long)(0)));
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.Panel_Team);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Left.Location = new System.Drawing.Point(0, 108);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(1000, 492);
            this.Panel_Left.TabIndex = 206;
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GVTeam);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Team.Location = new System.Drawing.Point(0, 0);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(1000, 492);
            this.Panel_Team.TabIndex = 204;
            // 
            // GVTeam
            // 
            this.GVTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVTeam.ColumnHeadersHeight = 25;
            this.GVTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVTeam.Location = new System.Drawing.Point(0, 30);
            this.GVTeam.Name = "GVTeam";
            this.GVTeam.ReadOnly = true;
            this.GVTeam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVTeam.Size = new System.Drawing.Size(1000, 462);
            this.GVTeam.TabIndex = 203;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(1000, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 200;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "1 - Tổ/ nhóm";
            // 
            // timerTeam
            // 
            this.timerTeam.Enabled = true;
            this.timerTeam.Interval = 1;
            this.timerTeam.Tick += new System.EventHandler(this.timerTeam_Tick);
            // 
            // timerEmployee
            // 
            this.timerEmployee.Enabled = true;
            this.timerEmployee.Interval = 1;
            this.timerEmployee.Tick += new System.EventHandler(this.timerEmployee_Tick);
            // 
            // Frm_Production_Auto_Money
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 600);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.Panel_Search);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Production_Auto_Money";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Production_Money_AutoRun";
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PicLoading)).EndInit();
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Search;
        private System.Windows.Forms.PictureBox PicLoading;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Run;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private SYS.TNDateTimePicker dte_FromDate;
        private SYS.TNDateTimePicker dte_ToDate;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Panel Panel_Team;
        private System.Windows.Forms.DataGridView GVTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Timer timerTeam;
        private System.Windows.Forms.Timer timerEmployee;
    }
}