﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using TNS.Misc;
using TNS.SYS;
using TNS_Report;

using OfficeOpenXml;
using System.Diagnostics;
using System.IO;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.WinApp
{
    public partial class Frm_ReportDemo_01 : Form
    {
        public Frm_ReportDemo_01()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            DateTime ViewDate = DateTime.Now; //SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
        }

        private void Frm_ReportDemo_01_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {


            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy");
            //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBox(ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
            DataTable ztb = TNS.WinApp.Frm_ReportDemo_01_DLL.Rpt_Demo.TRUNGBINHNGAY(FromDate, ToDate);
            this.Invoke(new MethodInvoker(delegate ()
            {
                Show_Chart_Voice_Time(ztb);
            }));
        }
        private void Show_Chart_Voice_Time(DataTable ztb)
        {
            Crt_Data.Series.Clear();
            //Trục y
            //Crt_Data.ChartAreas[0].AxisY.Minimum = 0; //Giá trị nhỏ nhất cột y
            //Crt_Data.ChartAreas[0].AxisY.Maximum = 10000000000; // giá trị lớn nhất cột y
            Crt_Data.ChartAreas[0].AxisY.LabelStyle.ForeColor = Color.White; //màu chữ cột y
            Crt_Data.ChartAreas[0].AxisY.LineColor = Color.White; // Màu trục y
            Crt_Data.ChartAreas[0].AxisY.LabelStyle.Format = "n0"; // định dạng chữ cột y
            Crt_Data.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Regular); // định dạng chữ cột y
            //Trục X
            Crt_Data.ChartAreas[0].AxisX.Interval = 1;
            Crt_Data.ChartAreas[0].AxisX.LineColor = Color.White;
            Crt_Data.ChartAreas[0].AxisX.LabelStyle.ForeColor = Color.White;
            Crt_Data.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Tahoma", 8, FontStyle.Regular); // định dạng chữ cột x

            //Tiêu đề báo cáo
            Crt_Data.ChartAreas[0].AxisX.Title = "BIỂU ĐỒ TRUNG BÌNH LƯƠNG TỔ NHÓM";
            Crt_Data.ChartAreas[0].AxisX.TitleForeColor = Color.White;
            Crt_Data.ChartAreas[0].AxisX.TitleFont = new Font("Tahoma", 10, FontStyle.Regular);


            Crt_Data.ChartAreas[0].AxisY.MajorGrid.LineColor = Color.White;

            Series zSeries_DomainTime = Crt_Data.Series.Add("Trung bình lương (VNĐ/Giờ)");
            zSeries_DomainTime.Color = Color.DarkGreen;
            zSeries_DomainTime.ChartType = SeriesChartType.Bar;
            zSeries_DomainTime.LabelForeColor = Color.White;

            for (int j = 0; j < ztb.Rows.Count; j++)
            {
                zSeries_DomainTime.Points.AddXY(ztb.Rows[j]["LeftColumn"].ToString(), double.Parse(ztb.Rows[j]["MediumDate"].ToString()));
                zSeries_DomainTime.Points[j].Label = ztb.Rows[j]["MediumDate"].Toe0String();
                //zSeries_DomainTime.Points[j].AxisLabel = ztb.Rows[j]["LeftColumn"].ToString();
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            //dragging = true;
            //dragCursorPoint = Cursor.Position;
            //dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                //Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                //this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
            }
        }
        #endregion
    }
}

namespace TNS.WinApp.Frm_ReportDemo_01_DLL
{
    public class Rpt_Demo
    {
        public static DataTable TRUNGBINHNGAY(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            string zSQL = @"

--declare @FromDate datetime ='2021-01-01 00:00:00'
--declare @ToDate datetime ='2021-01-30 23:59:59'

CREATE TABLE temp(
Ngay nvarchar(4),
Thang nvarchar(4),
Nam nvarchar(4),
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
[MoneyDate] money,
[HourDate] money,
[Medium] money
)
        --Tiền Nhóm chính
        INSERT INTO Temp 
        SELECT DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
        SUM([Money] + MoneyPrivate +Money_Borrow) ,0,0
        FROM [dbo].[FTR_Order_Money]  
        WHERE  OrderDate BETWEEN @FROMDATE AND @TODATE  AND ([Money]>0 OR MoneyPrivate > 0 OR Money_Borrow  >0)
        AND RecordStatus <>99   
        GROUP BY DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),TeamKey,EmployeeKey,EmployeeID,EmployeeName

        --Tiền Chia lại
        INSERT INTO Temp 
        SELECT DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
        Sum(MoneyPersonal) ,0,0
        FROM [dbo].[FTR_Order_Adjusted]
        WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate AND MoneyPersonal >0
        AND Share=0 AND RecordStatus <>99
        GROUP BY DAY(OrderDate),MONTH(OrderDate),YEAR(OrderDate),TeamKey,EmployeeKey,EmployeeID,EmployeeName

        --Tiền Giờ dư
        INSERT INTO Temp 
        SELECT DAY(DateImport),MONTH(DateImport),YEAR(DateImport),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),
        EmployeeKey,EmployeeID,EmployeeName,
        [Money] ,0,0
        FROM [dbo].[Temp_Import_Detail]
        WHERE  DateImport BETWEEN @FROMDATE AND @TODATE AND [Money] > 0
        AND RecordStatus <>99 

--Lấy giờ dư
--Lưu ý: nếu là cn thì số giờ * hệ sô cn, nếu là lễ thì số giờ * hệ số lễ
        INSERT INTO Temp 
        SELECT DAY(DateImport),MONTH(DateImport),YEAR(DateImport),
        [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),
        EmployeeKey,EmployeeID,EmployeeName,
        0,
        CASE 
	        WHEN DATENAME(weekday,DateImport) ='SUNDAY'
		        THEN ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1)*[dbo].[LayHeSoChuNhat](DateImport)
	        WHEN[dbo].[LayHeSoNgayLeDonHang](DateImport) >0 
	        THEN ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1) * [dbo].[LayHeSoNgayLeDonHang](DateImport)
	        ELSE ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1)
        END HourDate,
        0
        FROM [dbo].[Temp_Import_Detail]
        WHERE RecordStatus <> 99 AND ROUND(((CAST (LEFT(TotalTime ,2) AS MONEY) *60 + CAST (Right(TotalTime ,2) AS MONEY)) /60),1) >0
        AND DateImport BETWEEN @FROMDATE AND @TODATE  

 SELECT TeamName AS LeftColumn,
ROUND(MediumDate,0) AS MediumDate 
  FROM(
	        
			SELECT 
			A.TeamKey AS TeamKey,
			B.TeamID,B.TeamName,
			CASE WHEN SUM(HourDate) >0 THEN (SUM(MoneyDate)/SUM(HourDate) )
			ELSE 0
			END  MediumDate,
			D.Rank AS BranchRank,C.Rank AS DepartmentRank,B.Rank AS TeamRank
			FROM Temp A
			LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	        LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	        LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
            GROUP BY  A.TeamKey,
			B.TeamID,B.TeamName,D.Rank,C.Rank,B.Rank
         ) X --WHERE MediumDate > 0
          ORDER BY  MediumDate ---BranchRank,DepartmentRank,TeamRank

DROP TABLE Temp

";
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
