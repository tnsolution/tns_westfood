﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Connect;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ComboItem : Form
    {
        private string _ParentProduct = "";
        public string Key = "";
        private Combo_Object _ComboObj;
        public Frm_ComboItem()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVProduct, true);
            Utils.DoubleBuffered(GVStage, true);
            Utils.DoubleBuffered(GVDetail, true);
            Utils.DoubleBuffered(GVView, true);

            Utils.DrawGVStyle(ref GVProduct);
            Utils.DrawGVStyle(ref GVStage);
            Utils.DrawGVStyle(ref GVDetail);
            Utils.DrawGVStyle(ref GVView);
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            this.FormClosing += Frm_Main_FormClosing;

            GVProduct.CellContentClick += GVProduct_CellContentClick;
            GVStage.CellContentClick += GVStage_CellContentClick;
            GVDetail.CellContentClick += GVDetail_CellContentClick;
            GVDetail.CellEndEdit += GVDetail_CellEndEdit;


            btn_Search_Stage.Click += Btn_Search_Click;
            btn_New.Click += Btn_New_Click;
            btn_Save.Click += Btn_Save_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_SearchProduct.Click += Btn_SearchProduct_Click;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }


        private void Frm_ComboItem_Load(object sender, EventArgs e)
        {
            LoadCombobox();
            DesignLayout_GVProduct(GVProduct);
            DesignLayout_GVStage(GVStage);
            DesignLayout_GVDetail(GVDetail);
            DesignLayout_GVView(GVView);
            if (Key == "")
            {
                ClearForm();
            }
            else
            {
                LoadData();
            }
        }

        void LoadCombobox()
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Product, @"
SELECT ProductKey,ProductName FROM [dbo].[IVT_Product]WHERE RecordStatus <> 99 AND Parent ='0' ORDER BY ProductName", "");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, @"
SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY RANK", "--Tất cả--");
            LoadDataToToolbox.KryptonComboBox(cbo_Unit, @"
SELECT UnitKey,Unitname FROM [dbo].[IVT_Product_Unit] WHERE RecordStatus <> 99 ORDER BY Rank", "");
        }

        private void Btn_SearchProduct_Click(object sender, EventArgs e)
        {
            GVProduct.Rows.Clear();
            if (cbo_Product.SelectedValue.ToString() != "0")
            {

                DataTable ztb = Combo_Data.List_Product_Of_Material(cbo_Product.SelectedValue.ToString(), txt_ProductSearch.Text.Trim());
                InitGV_Product(GVProduct, ztb);
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DataTable ztb = Combo_Data.ListStage(txt_Stage.Text.Trim(), cbo_Team.SelectedValue.ToInt());
            InitGV_Stage(GVStage, ztb);
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            ClearForm();
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {

            string zCheck = CheckBeforSave();
            if (zCheck == "")
            {
                string zResult = SaveCombo();
                if (zResult == string.Empty)
                {
                    MessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadData();
                }
                else
                {
                    MessageBox.Show(zResult, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(zCheck, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (Key == "")
            {
                MessageBox.Show("Không tìm thấy đơn hàng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DialogResult dlr = MessageBox.Show("Bạn có muốn xóa bảng đơn giá này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    _ComboObj = new Combo_Object(Key);
                    _ComboObj.DeleteObject();
                    if (_ComboObj.Message == "")
                    {
                        MessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ClearForm();
                    }
                    else
                    {
                        MessageBox.Show(_ComboObj.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void LoadData()
        {
            _ComboObj = new Combo_Object(Key);
            txt_ComboID.Text = _ComboObj.ComboID;
            txt_ComboName.Text = _ComboObj.ComboName;
            txt_ProductID.Tag = _ComboObj.ProductKey;
            txt_ProductID.Text = _ComboObj.ProductID;
            txt_ProductName.Text = _ComboObj.ProductName;
            cbo_Unit.SelectedValue = _ComboObj.UnitKey;
            txt_Description.Text = _ComboObj.Description;
            dte_FromDate.Value = _ComboObj.FromDate;
            dte_ToDate.Value = _ComboObj.ToDate;
            _ParentProduct = _ComboObj.Parent;
            InitGV_Detail_V2(GVDetail);
            InitGV_Detail_V3(GVView);
        }
        private void ClearForm()
        {
            _ComboObj = new Combo_Object();
            _ListDetail = new List<ComboItem_Info>();
            Key = "";
            _ParentProduct = "";
            GVDetail.Rows.Clear();
            GVView.Rows.Clear();

            txt_ComboID.Clear();
            txt_ComboName.Clear();
            txt_ProductID.Tag = "";
            txt_ProductID.Text = "";
            txt_ProductName.Text = "";
            txt_Total.Text = "0";
            cbo_Unit.SelectedIndex = 0;
            txt_Description.Clear();
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = DateTime.MinValue;
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            if (txt_ComboID.Text.ToString() == "")
            {
                zResult += "Vui lòng điền mã bảng giá! \n";
            }

            if (txt_ComboName.Text.ToString() == "")
            {
                zResult += "Vui lòng điền tiêu đề! \n";
            }

            if (txt_ProductID.Tag.ToString() == "")
            {
                zResult += "Vui lòng chọn thành phẩm! \n";
            }
            if (dte_FromDate.Value == DateTime.MinValue)
            {
                zResult += "Vui lòng chọn ngày bắt đầu! \n";
            }

            if (Key == "")
            {
                bool Count = Combo_Data.CheckComboID(txt_ComboID.Text.ToString());
                if (!Count)
                {
                    zResult += "Mã bảng giá đã sử dụng! \n";
                }
            }
            return zResult;
        }
        #region
        //private string Save()
        //{
        //    #region [Combo_FED_Info]
        //    Combo_Info zInfo;
        //    if (Key == "")
        //    {
        //        zInfo = new Combo_Info();
        //    }
        //    else
        //    {
        //        zInfo = new Combo_Info(Key);
        //    }
        //    zInfo.ComboID = txt_ComboID.Text.Trim().ToUpper();
        //    zInfo.ComboName = txt_ComboName.Text.Trim();
        //    zInfo.ProductKey = txt_ProductID.Tag.ToString();
        //    zInfo.ProductID = txt_ProductID.Text.Trim();
        //    zInfo.ProductName = txt_ProductName.Text.Trim();
        //    zInfo.UnitKey = cbo_Unit.SelectedValue.ToInt();
        //    zInfo.UnitName = cbo_Unit.Text;
        //    zInfo.Description = txt_Description.Text.Trim();
        //    zInfo.ComboCost = txt_Total.Text.ToFloat();

        //    if (dte_FromDate.Value != DateTime.MinValue)
        //    {
        //        zInfo.FromDate = dte_FromDate.Value;
        //    }
        //    if (dte_ToDate.Value != DateTime.MinValue)
        //    {
        //        zInfo.ToDate = dte_ToDate.Value;
        //    }
        //    if (txt_Total.Text != string.Empty)
        //    {
        //        double zComboCost = double.Parse(txt_Total.Text);
        //        zInfo.ComboCost = zComboCost;
        //    }
        //    zInfo.CreatedBy = SessionUser.UserLogin.Key;
        //    zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
        //    zInfo.ModifiedBy = SessionUser.UserLogin.Key;
        //    zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
        //    zInfo.Save();
        //    #endregion
        //    string zResult = "";
        //    if (zInfo.Message == string.Empty)
        //    {
        //        #region [ComboItem_Info]
        //        Key = zInfo.Key;
        //        zInfo.EmtyDetail();
        //        zResult += zInfo.Message;
        //        if (_ListDetail.Count > 0)
        //        {
        //            foreach (ComboItem_Info zItem in _ListDetail)
        //            {
        //                zItem.ComboKey = zInfo.Key;
        //                zItem.CreatedBy = SessionUser.UserLogin.Key;
        //                zItem.CreatedName = SessionUser.UserLogin.EmployeeName;
        //                zItem.Create();
        //                zResult += zItem.Message;
        //            }
        //        }
        //        #endregion
        //    }
        //    else
        //    {
        //        zResult = zInfo.Message;
        //    }
        //    return zResult;
        //}
        #endregion
        private string SaveCombo()
        {
            if (Key == "")
                _ComboObj = new Combo_Object();
            else
                _ComboObj = new Combo_Object(Key);
            _ComboObj.ComboID = txt_ComboID.Text.Trim().ToUpper();
            _ComboObj.ComboName = txt_ComboName.Text.Trim();
            _ComboObj.ProductKey = txt_ProductID.Tag.ToString();
            _ComboObj.ProductID = txt_ProductID.Text.Trim();
            _ComboObj.ProductName = txt_ProductName.Text.Trim();
            _ComboObj.UnitKey = cbo_Unit.SelectedValue.ToInt();
            _ComboObj.UnitName = cbo_Unit.Text;
            _ComboObj.Description = txt_Description.Text.Trim();
            float zComboCost = 0;
            if (float.TryParse(txt_Total.Text, out zComboCost))
            {

            }
            _ComboObj.ComboCost = zComboCost;
            _ComboObj.Parent = _ParentProduct;

            if (dte_FromDate.Value != DateTime.MinValue)
            {
                _ComboObj.FromDate = dte_FromDate.Value;
            }
            if (dte_ToDate.Value != DateTime.MinValue)
            {
                _ComboObj.ToDate = dte_ToDate.Value;
            }
            
            _ComboObj.CreatedBy = SessionUser.UserLogin.Key;
            _ComboObj.CreatedName = SessionUser.UserLogin.EmployeeName;
            _ComboObj.ModifiedBy = SessionUser.UserLogin.Key;
            _ComboObj.ModifiedName = SessionUser.UserLogin.EmployeeName;

            _ListDetail = new List<ComboItem_Info>();
            ComboItem_Info zItem;
            for (int i = 0; i < GVDetail.Rows.Count; i++)
            {
                if (GVDetail.Rows[i].Tag != null && GVDetail.Rows[i].Cells["ItemID"].Value != null)
                {
                    zItem = (ComboItem_Info)GVDetail.Rows[i].Tag;
                    _ListDetail.Add(zItem);
                    _ComboObj.List_Item = _ListDetail;
                }
            }
            _ComboObj.Save_Object();
            Key = _ComboObj.Key;
            return _ComboObj.Message;

        }
        private void SumComboItem()
        {
            float zTotal = 0;
            for (int i = 0; i < GVDetail.Rows.Count; i++)
            {
                if (GVDetail.Rows[i].Tag != null
                    && GVDetail.Rows[i].Cells["ItemID"].Value != null
                    && bool.Parse(GVDetail.Rows[i].Cells["Active"].Value.ToString()) == true
                    && GVDetail.Rows[i].Visible == true)

                {
                    float zTemp = 0;
                    if (float.TryParse(GVDetail.Rows[i].Cells["SubTotalPrice"].Value.ToString(), out zTemp))
                    {

                    }
                    zTotal += zTemp;
                }
            }
            txt_Total.Text = zTotal.ToString("n0");
        }



        #region[GV Product]
        private void DesignLayout_GVProduct(DataGridView GV)
        {
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ProductID", "Mã");
            GV.Columns.Add("ProductName", "Tên thành phẩm");
            GV.Columns.Add("UnitName", "Đơn vị");
            GV.Columns.Add("Parent", "");

            Image imgMinus = new Bitmap(TNS.WinApp.Properties.Resources.plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = "+";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns["No"].Width = 30;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["ProductID"].Width = 100;
            GV.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ProductName"].Width = 240;
            GV.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ProductName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["UnitName"].Width = 60;
            GV.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Parent"].Width = 60;
            GV.Columns["Parent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["Parent"].Visible = false;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
        }
        void InitGV_Product(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GV.Rows.Clear();
            foreach (DataRow nRow in zTable.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Cells["No"].Value = (no + 1).ToString();
                nRowView.Cells["No"].Tag = nRow["ProductKey"].ToString().Trim();
                nRowView.Cells["ProductID"].Value = nRow["ProductID"].ToString().Trim();
                nRowView.Cells["ProductName"].Value = nRow["ProductName"].ToString().Trim();
                nRowView.Cells["UnitName"].Tag = nRow["BasicUnit"].ToString().Trim();
                nRowView.Cells["UnitName"].Value = nRow["UnitName"].ToString().Trim();
                nRowView.Cells["Parent"].Value = nRow["Parent"].ToString().Trim();
                no++;
            }
        }
        private void GVProduct_CellContentClick(object sender, EventArgs e)
        {
            if (GVProduct.CurrentCell.ColumnIndex == 5)
            {
                if (Key == "")
                {
                    DataGridViewRow zGVRow = GVProduct.CurrentRow;
                    txt_ProductID.Tag = zGVRow.Cells["No"].Tag.ToString();
                    txt_ProductID.Text = zGVRow.Cells["ProductID"].Value.ToString();
                    txt_ProductName.Text = zGVRow.Cells["ProductName"].Value.ToString();
                    cbo_Unit.SelectedValue = zGVRow.Cells["UnitName"].Tag.ToInt();
                    _ParentProduct = zGVRow.Cells["Parent"].Value.ToString().Trim();
                }
                else
                {
                    DialogResult dlr = MessageBox.Show("Bảng giá đã có thành phẩm.Bạn có muốn đổi thành phẩm ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dlr == DialogResult.Yes)
                    {
                        DataGridViewRow zGVRow = GVProduct.CurrentRow;

                        txt_ProductID.Tag = zGVRow.Cells["No"].Tag.ToString();
                        txt_ProductID.Text = zGVRow.Cells["ProductID"].Value.ToString();
                        txt_ProductName.Text = zGVRow.Cells["ProductName"].Value.ToString();
                        cbo_Unit.SelectedValue = zGVRow.Cells["UnitName"].Tag.ToInt();
                        _ParentProduct = zGVRow.Cells["Parent"].Value.ToString().Trim();
                    }
                }
            }
        }
        #endregion

        #region[GV Stage]
        private void DesignLayout_GVStage(DataGridView GV)
        {
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("StageID", "Mã");
            GV.Columns.Add("StageName", "Tên công việc");
            GV.Columns.Add("UnitName", "Đơn vị");
            GV.Columns.Add("Price", "Đơn giá");


            Image imgMinus = new Bitmap(TNS.WinApp.Properties.Resources.plus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = "+";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol);

            GV.Columns.Add("GroupID", "");


            GV.Columns["No"].Width = 30;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StageID"].Width = 80;
            GV.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["StageName"].Width = 200;
            GV.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["UnitName"].Width = 50;
            GV.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Price"].Width = 70;
            GV.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["GroupID"].Width = 0;
            GV.Columns["GroupID"].Visible = false;

            GV.ReadOnly = true;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            //GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
        }
        void InitGV_Stage(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GV.Rows.Clear();
            foreach (DataRow nRow in zTable.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Cells["No"].Value = (no + 1).ToString();
                nRowView.Cells["StageID"].Tag = nRow["StageKey"].ToString().Trim();
                nRowView.Cells["StageID"].Value = nRow["StagesID"].ToString().Trim();
                nRowView.Cells["StageName"].Value = nRow["StageName"].ToString().Trim();
                nRowView.Cells["UnitName"].Tag = nRow["BasicUnit"].ToString().Trim();
                nRowView.Cells["UnitName"].Value = nRow["UnitName"].ToString().Trim();
                float zPrice = float.Parse(nRow["Price"].ToString());
                nRowView.Cells["Price"].Value = zPrice.ToString("n1");
                nRowView.Cells["GroupID"].Tag = nRow["GroupKey"].ToString().Trim();
                nRowView.Cells["GroupID"].Value = nRow["GroupID"].ToString().Trim();
                no++;
            }
        }
        List<ComboItem_Info> _ListDetail = new List<ComboItem_Info>();
        private void GVStage_CellContentClick(object sender, EventArgs e)
        {//nếu đang xem tổng phải load về danh sách thêm  công việc
            tab1.SelectedTab = PageAction;
            if (GVStage.CurrentCell.ColumnIndex == 5)
            {
                DataGridViewRow zGVRow = GVStage.CurrentRow;
                ComboItem_Info zComboItem = new ComboItem_Info();
                zComboItem.Key = 0;
                zComboItem.ItemKey = zGVRow.Cells["StageID"].Tag.ToString();
                zComboItem.ItemID = zGVRow.Cells["StageID"].Value.ToString();
                zComboItem.ItemName = zGVRow.Cells["StageName"].Value.ToString();

                if (zGVRow.Cells["Price"].Value != DBNull.Value)
                {
                    float zPrice = float.Parse(zGVRow.Cells["Price"].Value.ToString());
                    zComboItem.Price = zPrice;
                }
                zComboItem.Active = true;
                zComboItem.UnitKey = zGVRow.Cells["UnitName"].Tag.ToInt();
                zComboItem.UnitName = zGVRow.Cells["UnitName"].Value.ToString();
                zComboItem.GroupStage = zGVRow.Cells["GroupID"].Tag.ToInt();
                zComboItem.GroupName = zGVRow.Cells["GroupID"].Value.ToString();
                zComboItem.ConvertRate = 0;
                zComboItem.RecoveryRate = 0;
                zComboItem.LonRate = 0;
                zComboItem.KgRate = 0;
                zComboItem.SubTotalPrice = zComboItem.Price;
                zComboItem.RecordStatus = 1; //tạo mới

                AddItem(zComboItem);
                //InitGV_Detail(GVDetail);
            }
        }
        private void AddItem(ComboItem_Info AddInItem)
        {
            int RowIndex = GVDetail.Rows.Count;
            GVDetail.Rows.Add();
            DataGridViewRow Row = GVDetail.Rows[RowIndex];
            Row.Tag = AddInItem;
            //Row.Cells["No"].Value = (RowIndex + 1);
            Row.Cells["ItemID"].Value = AddInItem.ItemID;
            Row.Cells["ItemName"].Value = AddInItem.ItemName;
            Row.Cells["UnitName"].Value = AddInItem.UnitName;
            Row.Cells["StagePrice"].Value = AddInItem.Price.ToString("n1");
            Row.Cells["ConvertRate"].Value = AddInItem.ConvertRate.ToString("n2");
            Row.Cells["RecoveryRate"].Value = AddInItem.RecoveryRate.ToString("n1");
            Row.Cells["LonRate"].Value = AddInItem.LonRate.ToString("n2");
            Row.Cells["KgRate"].Value = AddInItem.KgRate.ToString("n1");
            Row.Cells["SubTotalPrice"].Value = AddInItem.SubTotalPrice.ToString("n1");
            Row.Cells["Rank"].Value = AddInItem.Rank.ToString("n0");
            if (AddInItem.Active == true)
            {
                Row.Cells["Active"].Value = true;
            }
            else
                Row.Cells["Active"].Value = false;
            Row.Cells["Description"].Value = "";
            SumComboItem();
        }
        private void ShowEditItem(ComboItem_Info EditItem, int RowIndex)
        {
            DataGridViewRow Row = GVDetail.Rows[RowIndex];
            Row.Tag = EditItem;
            //Row.Cells["No"].Value = (RowIndex + 1);
            Row.Cells["ItemID"].Value = EditItem.ItemID;
            Row.Cells["ItemName"].Value = EditItem.ItemName;
            Row.Cells["UnitName"].Value = EditItem.UnitName;
            Row.Cells["StagePrice"].Value = EditItem.Price.ToString("n1");
            Row.Cells["ConvertRate"].Value = EditItem.ConvertRate.ToString("n2");
            Row.Cells["LonRate"].Value = EditItem.LonRate.ToString("n2");
            Row.Cells["KgRate"].Value = EditItem.KgRate.ToString("n1");
            Row.Cells["RecoveryRate"].Value = EditItem.RecoveryRate.ToString("n1");
            Row.Cells["SubTotalPrice"].Value = EditItem.SubTotalPrice.ToString("n1");
            Row.Cells["Rank"].Value = EditItem.Rank.ToString("n0");
            if (EditItem.Active == true)
            {
                Row.Cells["Active"].Value = true;
            }
            else
                Row.Cells["Active"].Value = false;
            Row.Cells["Description"].Value = EditItem.Description.ToString();

        }
        private void DeleteItem(int RowIndex)
        {
            if (GVDetail.Rows[RowIndex].Tag != null)
            {
                ComboItem_Info zItem = (ComboItem_Info)GVDetail.Rows[RowIndex].Tag;
                GVDetail.Rows[RowIndex].Visible = false;
                zItem.RecordStatus = 99;
            }
            else
            {
                GVDetail.Rows[RowIndex].Visible = false;
            }
        }
        private void Caculator(ComboItem_Info zItem)
        {
            float zPrice = 0;
            float zConvertRate = 0;
            float zRecoveryRate = 0;
            float zLonRate = 0;
            float zKgRate = 0;

            zPrice = zItem.Price;

            if (zItem.ConvertRate == 0)
                zConvertRate = 1;
            else
                zConvertRate = zItem.ConvertRate;

            if (zItem.RecoveryRate == 0)
                zRecoveryRate = 1;
            else
                zRecoveryRate = zItem.RecoveryRate / 100;

            if (zItem.LonRate == 0)
                zLonRate = 1;
            else
                zLonRate = zItem.LonRate;

            if (zItem.KgRate == 0)
                zKgRate = 1;
            else
                zKgRate = zItem.KgRate / 100;

            zItem.SubTotalPrice = zPrice * zLonRate * zKgRate * zConvertRate / zRecoveryRate;
        }
        #endregion


        #region[GV Detail]
        private void DesignLayout_GVDetail(DataGridView GV)
        {
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ItemID", "Mã");
            GV.Columns.Add("ItemName", "Tên");
            GV.Columns.Add("UnitName", "Đơn vị");
            GV.Columns.Add("StagePrice", "Đơn giá");
            GV.Columns.Add("RecoveryRate", "Tỉ lệ thu hồi (%)");
            GV.Columns.Add("KgRate", "Tỉ lệ thành phẩm (%)");
            GV.Columns.Add("LonRate", "Tỉ lệ quy đổi theo quy cách");
            GV.Columns.Add("ConvertRate", " Tỉ lệ khác");
            GV.Columns.Add("SubTotalPrice", "Đơn giá đã quy đổi");
            DataGridViewCheckBoxColumn zColumn;
            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Active";
            zColumn.HeaderText = "Sử dụng";
            GV.Columns.Add(zColumn);

            GV.Columns.Add("Rank", "Sắp xếp");

            GV.Columns.Add("Description", "Ghi chú");

            Image imgMinus = new Bitmap(TNS.WinApp.Properties.Resources.minus);
            DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            GVImageCol.HeaderText = "Xóa";
            GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            GVImageCol.Image = imgMinus;
            GVImageCol.Width = 32;
            GV.Columns.Add(GVImageCol); //

            GV.Columns["No"].Width = 30;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;

            GV.Columns["ItemID"].Width = 80;
            GV.Columns["ItemID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ItemID"].Frozen = true;

            GV.Columns["ItemName"].Width = 220;
            GV.Columns["ItemName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ItemName"].Frozen = true;

            GV.Columns["UnitName"].Width = 50;
            GV.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["StagePrice"].Width = 70;
            GV.Columns["StagePrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["ConvertRate"].Width = 50;
            GV.Columns["ConvertRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["LonRate"].Width = 60;
            GV.Columns["LonRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["KgRate"].Width = 50;
            GV.Columns["KgRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["RecoveryRate"].Width = 50;
            GV.Columns["RecoveryRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["SubTotalPrice"].Width = 80;
            GV.Columns["SubTotalPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Active"].Width = 40;
            GV.Columns["Active"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["Rank"].Width = 40;
            GV.Columns["Rank"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Description"].Width = 140;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.ColumnHeadersHeight = 70;
            GV.ReadOnly = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            //GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
        }

        #region
        //Load tất cả 
        //private void InitGV_Detail(DataGridView GV)
        //{
        //    GV.Rows.Clear();
        //    for (int i = 0; i < _ComboObj.List_Item.Count; i++)
        //    {
        //        GV.Rows.Add();
        //        ComboItem_Info zExistItem = _ComboObj.List_Item[i];
        //        DataGridViewRow zGvRow = GV.Rows[i];
        //        zGvRow.Tag = zExistItem;
        //        zGvRow.Cells["No"].Value = (i + 1).ToString();
        //        //zGvRow.Cells["No"].Tag = zExistItem.Key;
        //        zGvRow.Cells["ItemID"].Value = zExistItem.ItemID.Trim();
        //        zGvRow.Cells["ItemName"].Value = zExistItem.ItemName.ToString().Trim();
        //        zGvRow.Cells["UnitName"].Tag = zExistItem.UnitKey.ToString().Trim();
        //        zGvRow.Cells["UnitName"].Value = zExistItem.UnitName.ToString().Trim();
        //        zGvRow.Cells["StagePrice"].Value = zExistItem.Price.ToString("n1");
        //        zGvRow.Cells["ConvertRate"].Value = zExistItem.ConvertRate.ToString("n2");
        //        zGvRow.Cells["LonRate"].Value = zExistItem.LonRate.ToString("n1");
        //        zGvRow.Cells["KgRate"].Value = zExistItem.KgRate.ToString("n1");
        //        zGvRow.Cells["RecoveryRate"].Value = zExistItem.RecoveryRate.ToString("n1");
        //        zGvRow.Cells["SubTotalPrice"].Value = zExistItem.SubTotalPrice.ToString("n1");
        //        zGvRow.Cells["Rank"].Value = zExistItem.Rank.ToString("n0");
        //        if (zExistItem.Active == true)
        //        {
        //            zGvRow.Cells["Active"].Value = true;
        //        }
        //        else
        //            zGvRow.Cells["Active"].Value = false;
        //        zGvRow.Cells["Description"].Value = zExistItem.Description.ToString();
        //    }
        //    SumComboItem();
        //}
        #endregion

        private void InitGV_Detail_V2(DataGridView GV)
        {
            GV.Rows.Clear();
            if (_ComboObj.List_Item.Count > 0)
            {
                int zGroupKey = _ComboObj.List_Item[0].GroupStage.ToInt();
                string zGroupName = _ComboObj.List_Item[0].GroupName.ToString();
                float zKgRate = _ComboObj.List_Item[0].KgRate;
                float zLonRate = _ComboObj.List_Item[0].LonRate;

                double total = 0;//_ComboObj.List_Item.Where(item => item.GroupStage == zGroupKey).Sum(item => item.SubTotalPrice);
                int Index = 0;
                int TotoRow = _ComboObj.List_Item.Count;
                for (int i = 0; i <= TotoRow; i++)

                {
                    GV.Rows.Add();
                    DataGridViewRow zGvRow = GV.Rows[i];
                    if (i == TotoRow)
                    {
                        zGvRow.Tag = null;
                        zGvRow.Cells["No"].Value = (i + 1).ToString();
                        //zGvRow.Cells["No"].Tag = zExistItem.Key;
                        zGvRow.Cells["ItemID"].Value = "TỔNG";
                        zGvRow.Cells["ItemName"].Value = zGroupName;
                        zGvRow.Cells["UnitName"].Tag = "";
                        zGvRow.Cells["UnitName"].Value = cbo_Unit.Text;
                        zGvRow.Cells["StagePrice"].Value = "";
                        zGvRow.Cells["ConvertRate"].Value = "";
                        zGvRow.Cells["LonRate"].Value = zLonRate.ToString("n2");
                        zGvRow.Cells["KgRate"].Value = zKgRate.ToString("n2");
                        zGvRow.Cells["RecoveryRate"].Value = "";
                        zGvRow.Cells["SubTotalPrice"].Value = total.ToString("n0");
                        zGvRow.Cells["Rank"].Value = "";
                        zGvRow.Cells["Active"].Value = false;
                        zGvRow.Cells["Description"].Value = "";
                        total = 0;//_ComboObj.List_Item.Where(item => item.GroupStage == zGroupKey).Sum(item => item.SubTotalPrice);
                        zGvRow.DefaultCellStyle.BackColor = Color.LemonChiffon;
                        zGvRow.DefaultCellStyle.ForeColor = Color.Maroon;
                        zGvRow.DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);
                        zGvRow.ReadOnly = true;

                    }
                    else
                    {
                        ComboItem_Info zExistItem = _ComboObj.List_Item[Index];
                        if (zGroupKey == zExistItem.GroupStage.ToInt())
                        {


                            zGvRow.Tag = zExistItem;
                            zGvRow.Cells["No"].Value = (i + 1).ToString();
                            //zGvRow.Cells["No"].Tag = zExistItem.Key;
                            zGvRow.Cells["ItemID"].Value = zExistItem.ItemID.Trim();
                            zGvRow.Cells["ItemName"].Value = zExistItem.ItemName.ToString().Trim();
                            zGvRow.Cells["UnitName"].Tag = zExistItem.UnitKey.ToString().Trim();
                            zGvRow.Cells["UnitName"].Value = zExistItem.UnitName.ToString().Trim();
                            zGvRow.Cells["StagePrice"].Value = zExistItem.Price.ToString("n1");
                            zGvRow.Cells["ConvertRate"].Value = zExistItem.ConvertRate.ToString("n2");
                            zGvRow.Cells["LonRate"].Value = zExistItem.LonRate.ToString("n2");
                            zGvRow.Cells["KgRate"].Value = zExistItem.KgRate.ToString("n1");
                            zGvRow.Cells["RecoveryRate"].Value = zExistItem.RecoveryRate.ToString("n1");
                            zGvRow.Cells["SubTotalPrice"].Value = zExistItem.SubTotalPrice.ToString("n1");
                            zGvRow.Cells["Rank"].Value = zExistItem.Rank.ToString("n0");
                            if (zExistItem.Active == true)
                            {
                                zGvRow.Cells["Active"].Value = true;
                                total += zExistItem.SubTotalPrice;
                            }
                            else
                                zGvRow.Cells["Active"].Value = false;
                            zGvRow.Cells["Description"].Value = zExistItem.Description.ToString();

                            Index++;
                        }
                        else
                        {
                            zGvRow.Tag = null;
                            zGvRow.Cells["No"].Value = (i + 1).ToString();
                            //zGvRow.Cells["No"].Tag = zExistItem.Key;
                            zGvRow.Cells["ItemID"].Value = "TỔNG";
                            zGvRow.Cells["ItemName"].Value = zGroupName;
                            zGvRow.Cells["UnitName"].Tag = "";
                            zGvRow.Cells["UnitName"].Value = cbo_Unit.Text;
                            zGvRow.Cells["StagePrice"].Value = "";
                            zGvRow.Cells["ConvertRate"].Value = "";
                            zGvRow.Cells["LonRate"].Value = zLonRate.ToString("n2");
                            zGvRow.Cells["KgRate"].Value = zKgRate.ToString("n2");
                            zGvRow.Cells["RecoveryRate"].Value = "";
                            zGvRow.Cells["SubTotalPrice"].Value = total.ToString("n0");
                            zGvRow.Cells["Rank"].Value = "";
                            zGvRow.Cells["Active"].Value = false;
                            zGvRow.Cells["Description"].Value = "";
                            zGvRow.DefaultCellStyle.BackColor = Color.LemonChiffon;
                            zGvRow.DefaultCellStyle.ForeColor = Color.Maroon;
                            zGvRow.DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);
                            zGvRow.ReadOnly = true;
                            // khởi tạo lại dòng đầu tiên
                            zGroupKey = zExistItem.GroupStage.ToInt();
                            zGroupName = zExistItem.GroupName.ToString();
                            zKgRate = zExistItem.KgRate;
                            zLonRate = zExistItem.LonRate;
                            total = 0;//_ComboObj.List_Item.Where(item => item.GroupStage == zGroupKey).Sum(item => item.SubTotalPrice);

                            TotoRow++;
                        }
                    }
                }
                SumComboItem();
            }
        }

        private void GVDetail_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (GVDetail.Rows[e.RowIndex].Tag != null)
            {
                if (e.ColumnIndex == 10)
                {
                    DataGridViewRow zRowEdit = GVDetail.Rows[e.RowIndex];
                    ComboItem_Info zItem;
                    if (GVDetail.Rows[e.RowIndex].Tag == null)
                    {
                        zItem = new ComboItem_Info();
                        GVDetail.Rows[e.RowIndex].Tag = zItem;
                    }
                    else
                    {
                        zItem = (ComboItem_Info)GVDetail.Rows[e.RowIndex].Tag;
                    }
                    if (bool.Parse(zRowEdit.Cells[e.ColumnIndex].Value.ToString()) == false)
                    {
                        zItem.Active = true;
                    }
                    else
                    {
                        zItem.Active = false;
                    }
                    ShowEditItem(zItem, e.RowIndex);
                }
                if (e.ColumnIndex == 13)
                {
                    DataGridViewRow zGvRow = GVDetail.CurrentRow;
                    int Index = GVDetail.CurrentRow.Index;
                    DeleteItem(Index);
                }
                GVDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);
                SumComboItem();
            }
            else
            {
                if (e.ColumnIndex == 13)
                {
                    DataGridViewRow zGvRow = GVDetail.CurrentRow;
                    int Index = GVDetail.CurrentRow.Index;
                    DeleteItem(Index);
                }
                GVDetail.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }
        private void GVDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVDetail.Rows[e.RowIndex];
            ComboItem_Info zItem;
            if (GVDetail.Rows[e.RowIndex].Tag == null)
            {
                zItem = new ComboItem_Info();
                GVDetail.Rows[e.RowIndex].Tag = zItem;
            }
            else
            {
                zItem = (ComboItem_Info)GVDetail.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            switch (e.ColumnIndex)
            {
                case 4: // giá
                    if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "")
                    {
                        zItem.Price = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        float zPrice = 0;
                        if(float.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString(),out zPrice))
                        {

                        }

                        if (zPrice != zItem.Price)
                        {
                            zItem.Price = zPrice;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 5: // tỉ lệ thu hồi
                    if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "")
                    {
                        zItem.RecoveryRate = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        float zRecoveryRate = 0;
                        if (float.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString(), out zRecoveryRate))
                        {

                        }
                        if (zRecoveryRate != zItem.RecoveryRate)
                        {
                            zItem.RecoveryRate = zRecoveryRate;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 6: // tỉ lệ thành phẩm
                    if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "")
                    {
                        zItem.KgRate = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        float zKgRate = 0;
                        if (float.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString(), out zKgRate))
                        {

                        }
                        if (zKgRate != zItem.KgRate)
                        {
                            zItem.KgRate = zKgRate;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 7: // tỉ lệ lon
                    if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "")
                    {
                        zItem.LonRate = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        float zLonRate = 0;
                        if (float.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString(), out zLonRate))
                        {

                        }
                        if (zLonRate != zItem.LonRate)
                        {
                            zItem.LonRate = zLonRate;
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 8: // tỉ lệ quy đổi
                    if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "")
                    {
                        zItem.ConvertRate = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        float zConvertRate = 0;
                        if (float.TryParse(zRowEdit.Cells[e.ColumnIndex].Value.ToString(), out zConvertRate))
                        {

                        }
                        if (zConvertRate != zItem.ConvertRate)
                        {
                            zItem.ConvertRate = zConvertRate;
                            zIsChangeValued = true;
                        }
                    }
                    break;

                case 11: // sắp xếp
                    if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "")
                    {
                        zItem.Rank = 0;
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zItem.Rank.ToString())
                        {
                            zItem.Rank = zRowEdit.Cells[e.ColumnIndex].Value.ToInt();
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 12: // ghi chú
                    if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() == "")
                    {
                        zItem.Description = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zItem.Description.ToString())
                        {
                            zItem.Description = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            zIsChangeValued = true;
                        }
                    }
                    break;
            }
            if (zItem.Key != 0)
            {
                zItem.RecordStatus = 2; //đã tạo có sửa
            }
            else
            {
                zItem.RecordStatus = 1;// tạo mới và sửa
            }
            if (zIsChangeValued)
            {
                Caculator(zItem);
                ShowEditItem(zItem, e.RowIndex);
                SumComboItem();
            }

        }
        #endregion

        #region[GV View]
        private void DesignLayout_GVView(DataGridView GV)
        {
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ItemID", "Mã");
            GV.Columns.Add("ItemName", "Tên");
            GV.Columns.Add("UnitName", "Đơn vị");
            GV.Columns.Add("StagePrice", "Đơn giá");
            GV.Columns.Add("RecoveryRate", "Tỉ lệ thu hồi (%)");
            GV.Columns.Add("KgRate", "Tỉ lệ thành phẩm (%)");
            GV.Columns.Add("LonRate", "Tỉ lệ quy đổi theo quy cách");
            GV.Columns.Add("ConvertRate", " Tỉ lệ khác");
            GV.Columns.Add("SubTotalPrice", "Đơn giá đã quy đổi");
            DataGridViewCheckBoxColumn zColumn;
            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Active";
            zColumn.HeaderText = "Sử dụng";
            GV.Columns.Add(zColumn);

            GV.Columns.Add("Rank", "Sắp xếp");

            GV.Columns.Add("Description", "Ghi chú");

            //Image imgMinus = new Bitmap(TNS.WinApp.Properties.Resources.minus);
            //DataGridViewImageColumn GVImageCol = new DataGridViewImageColumn();
            //GVImageCol.HeaderText = "Xóa";
            //GVImageCol.ImageLayout = DataGridViewImageCellLayout.Zoom;
            //GVImageCol.Image = imgMinus;
            //GVImageCol.Width = 32;
            //GV.Columns.Add(GVImageCol); //

            GV.Columns["No"].Width = 30;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;

            GV.Columns["ItemID"].Width = 80;
            GV.Columns["ItemID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ItemID"].Frozen = true;

            GV.Columns["ItemName"].Width = 220;
            GV.Columns["ItemName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["ItemName"].Frozen = true;

            GV.Columns["UnitName"].Width = 50;
            GV.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["StagePrice"].Width = 70;
            GV.Columns["StagePrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["ConvertRate"].Width = 50;
            GV.Columns["ConvertRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["LonRate"].Width = 60;
            GV.Columns["LonRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["KgRate"].Width = 50;
            GV.Columns["KgRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["RecoveryRate"].Width = 50;
            GV.Columns["RecoveryRate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["SubTotalPrice"].Width = 80;
            GV.Columns["SubTotalPrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Active"].Width = 40;
            GV.Columns["Active"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["Rank"].Width = 40;
            GV.Columns["Rank"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Description"].Width = 140;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.ColumnHeadersHeight = 70;
            GV.ReadOnly = false;
            GV.AllowUserToAddRows = false;
            GV.AllowUserToDeleteRows = false;
            //GV.SelectionMode = DataGridViewSelectionMode.CellSelect;
            GV.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
        }
        private void InitGV_Detail_V3(DataGridView GV)
        {

            GV.Rows.Clear();
            if (_ComboObj.List_Item.Count > 0)
            {
                double zComboCost = 0;
                int zGroupKey = _ComboObj.List_Item[0].GroupStage.ToInt();
                string zGroupName = _ComboObj.List_Item[0].GroupName.ToString();
                float zKgRate = _ComboObj.List_Item[0].KgRate;
                float zLonRate = _ComboObj.List_Item[0].LonRate;

                double total = 0;//_ComboObj.List_Item.Where(item => item.GroupStage == zGroupKey).Sum(item => item.SubTotalPrice);
                int Index = 0;
                int TotoRow = _ComboObj.List_Item.Count;
                for (int i = 0; i <= TotoRow; i++)

                {

                    if (i == TotoRow)
                    {
                        GV.Rows.Add();
                        DataGridViewRow zGvRow = GV.Rows[Index];
                        zGvRow.Tag = null;
                        zGvRow.Cells["No"].Value = (Index + 1).ToString();
                        //zGvRow.Cells["No"].Tag = zExistItem.Key;
                        zGvRow.Cells["ItemID"].Value = "TỔNG";
                        zGvRow.Cells["ItemName"].Value = zGroupName;
                        zGvRow.Cells["UnitName"].Tag = "";
                        zGvRow.Cells["UnitName"].Value = cbo_Unit.Text;
                        zGvRow.Cells["StagePrice"].Value = "";
                        zGvRow.Cells["ConvertRate"].Value = "";
                        zGvRow.Cells["LonRate"].Value = zLonRate.ToString("n2");
                        zGvRow.Cells["KgRate"].Value = zKgRate.ToString("n2");
                        zGvRow.Cells["RecoveryRate"].Value = "";
                        zGvRow.Cells["SubTotalPrice"].Value = total.ToString("n0");
                        zComboCost += total;
                        zGvRow.Cells["Rank"].Value = "";
                        zGvRow.Cells["Active"].Value = false;
                        zGvRow.Cells["Description"].Value = "";
                        total = 0;//_ComboObj.List_Item.Where(item => item.GroupStage == zGroupKey).Sum(item => item.SubTotalPrice);
                        zGvRow.DefaultCellStyle.BackColor = Color.LemonChiffon;
                        zGvRow.DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);
                        zGvRow.ReadOnly = true;

                    }
                    else
                    {
                        ComboItem_Info zExistItem = _ComboObj.List_Item[i];
                        if (zGroupKey == zExistItem.GroupStage.ToInt())
                        {
                            if (zExistItem.Active == true)
                                total += zExistItem.SubTotalPrice;

                        }
                        else
                        {
                            GV.Rows.Add();
                            DataGridViewRow zGvRow = GV.Rows[Index];
                            zGvRow.Tag = null;
                            zGvRow.Cells["No"].Value = (Index + 1).ToString();
                            //zGvRow.Cells["No"].Tag = zExistItem.Key;
                            zGvRow.Cells["ItemID"].Value = "TỔNG";
                            zGvRow.Cells["ItemName"].Value = zGroupName;
                            zGvRow.Cells["UnitName"].Tag = "";
                            zGvRow.Cells["UnitName"].Value = cbo_Unit.Text;
                            zGvRow.Cells["StagePrice"].Value = "";
                            zGvRow.Cells["ConvertRate"].Value = "";
                            zGvRow.Cells["LonRate"].Value = zLonRate.ToString("n2");
                            zGvRow.Cells["KgRate"].Value = zKgRate.ToString("n2");
                            zGvRow.Cells["RecoveryRate"].Value = "";
                            zGvRow.Cells["SubTotalPrice"].Value = total.ToString("n0");
                            zComboCost += total;
                            zGvRow.Cells["Rank"].Value = "";
                            zGvRow.Cells["Active"].Value = false;
                            zGvRow.Cells["Description"].Value = "";

                            zGvRow.DefaultCellStyle.BackColor = Color.LemonChiffon;
                            zGvRow.DefaultCellStyle.Font = new Font("Arial", 8, FontStyle.Bold);
                            zGvRow.ReadOnly = true;
                            zGroupKey = zExistItem.GroupStage.ToInt();
                            zGroupName = zExistItem.GroupName.ToString();
                            zKgRate = zExistItem.KgRate;
                            zLonRate = zExistItem.LonRate;
                            total = 0;//_ComboObj.List_Item.Where(item => item.GroupStage == zGroupKey).Sum(item => item.SubTotalPrice);
                            if (zExistItem.Active == true)
                                total += zExistItem.SubTotalPrice;

                            Index++;
                        }
                    }
                }
                //txt_Total.Text = zComboCost.ToString("n0");
            }
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;
        private void Frm_Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn đóng trang này !.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

    }
}
