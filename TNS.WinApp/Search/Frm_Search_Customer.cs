﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CRM;
using TNS.Misc;

namespace TNS.WinApp
{
    public partial class Frm_Search_Customer : Form
    {
        public Frm_Search_Customer()
        {
            InitializeComponent();
            GV_Customer_Layout();
        }
        DataTable ztb = new DataTable();
        public string CustomerKey = "";
        public string CustomerID = "";
        public string CustomerName = "";
        private void Frm_Search_Customer_Load(object sender, EventArgs e)
        {
            ztb.Columns.Add("Value");
            ztb.Columns.Add("Name");
            ztb.Rows.Add("1", "Khách hàng cá nhân");
            ztb.Rows.Add("2", "Khách hàng doanh nghiệp");
            cbo_Slug_Customer.DataSource = ztb;
            cbo_Slug_Customer.DisplayMember = "Name";
            cbo_Slug_Customer.ValueMember = "Value";

            Utils.DoubleBuffered(GV_Customer, true);
            Utils.DrawGVStyle(ref GV_Customer);
        }
        private void txt_Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                GV_Customer.Focus();
                if (GV_Customer.Rows.Count > 0)
                    GV_Customer.CurrentRow.Selected = true;
            }
            else
            {
                if (e.KeyCode != Keys.Enter)
                {
                    if (txt_Search.Text.Length > 0)

                    {
                        GV_Customer.Visible = true;
                        GV_Customer_LoadData();
                    }
                    else
                        GV_Customer.Visible = false;
                }
            }
        }

        #region[ListView]

        private void GV_Customer_Layout()
        {
            // Setup Column 
            GV_Customer.Columns.Add("No", "STT");
            GV_Customer.Columns.Add("CustomerID", "Mã khách hàng");
            GV_Customer.Columns.Add("CustomerName", "Khách hàng");
            GV_Customer.Columns.Add("TaxCode", "Mã số thuế");
            GV_Customer.Columns.Add("Address", "Địa chỉ");

            GV_Customer.Columns["No"].Width = 40;
            GV_Customer.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Customer.Columns["No"].ReadOnly = true;

            GV_Customer.Columns["CustomerID"].Width = 110;
            GV_Customer.Columns["CustomerID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Customer.Columns["CustomerName"].Width = 250;
            GV_Customer.Columns["CustomerName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Customer.Columns["CustomerName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Customer.Columns["TaxCode"].Width = 110;
            GV_Customer.Columns["TaxCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Customer.Columns["Address"].Width = 275;
            GV_Customer.Columns["Address"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Customer.Columns["Address"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;



            GV_Customer.BackgroundColor = Color.White;
            GV_Customer.GridColor = Color.FromArgb(227, 239, 255);
            GV_Customer.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Customer.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Customer.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_Customer.ReadOnly = true;

            GV_Customer.AllowUserToResizeRows = false;
            GV_Customer.AllowUserToResizeColumns = true;

            GV_Customer.RowHeadersVisible = false;
            GV_Customer.AllowUserToAddRows = true;
            GV_Customer.AllowUserToDeleteRows = false;
            GV_Customer.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV_Customer.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Customer.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_Customer.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Customer.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            for (int i = 1; i <= 8; i++)
            {
                GV_Customer.Rows.Add();
            }

        }
        private void GV_Customer_LoadData()
        {

            GV_Customer.Rows.Clear();
            DataTable In_Table = new DataTable();
            int _Slug = int.Parse(cbo_Slug_Customer.SelectedValue.ToString());
            In_Table = Customer_Data.Search(txt_Search.Text, _Slug);
            int i = 0;
            foreach (DataRow nRow in In_Table.Rows)
            {
                GV_Customer.Rows.Add();
                DataGridViewRow nRowView = GV_Customer.Rows[i];
                nRowView.Tag = nRow["CustomerKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["CustomerID"].Value = nRow["CustomerID"].ToString().Trim();
                nRowView.Cells["CustomerName"].Value = nRow["CustomerName"].ToString().Trim();
                nRowView.Cells["TaxCode"].Value = nRow["TaxCode"].ToString().Trim();
                nRowView.Cells["Address"].Value = nRow["Address"].ToString().Trim();
                i++;
            }

        }

        #endregion


        private void GV_Customer_DoubleClick(object sender, EventArgs e)
        {
            if (GV_Customer.CurrentRow.Tag != null)
            {
                CustomerKey = GV_Customer.CurrentRow.Tag.ToString();
                CustomerID = GV_Customer.CurrentRow.Cells["CustomerID"].Value.ToString();
                CustomerName = GV_Customer.CurrentRow.Cells["CustomerName"].Value.ToString();
                this.Close();
            }

        }

        private void GV_Customer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_Search.Tag = CustomerKey = GV_Customer.CurrentRow.Tag.ToString();
            }
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
