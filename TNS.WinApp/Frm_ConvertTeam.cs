﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TN.Library.HumanResources;
using TN.Library.Miscellaneous;
using TN.Library.Productivity;

namespace TN_WinApp
{
    public partial class Frm_ConvertTeam : Form
    {
        private string _OrderKey = "";
        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }
        public Frm_ConvertTeam()
        {
            InitializeComponent();
        }

        private void Frm_ConvertTeam_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Name, "SELECT EmployeeKey,EmployeeName FROM FTR_Order_Money WHERE OrderKey = '" + OrderKey + "' AND Money_Eat > 0 AND RecordStatus < 99", "---- Choose Teams ----");
        }
        private void Cbo_Name_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbo_Name.SelectedIndex > 0 )
            {
                LoadDataToToolbox.ComboBoxData(cbo_TeamDefault, "SELECT A.TeamKey,B.TeamName FROM FTR_Order_Money A " +
                    "LEFT JOIN SYS_Team B ON A.TeamKey = B.TeamKey WHERE EmployeeKey = '" + cbo_Name.SelectedValue + "'" +
                    " AND OrderKey = '"+ OrderKey + "'" +
                    " AND A.RecordStatus < 99", "---- Choose Teams ----");
            }
        }
        private void Cbo_TeamDefault_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_TeamDefault.SelectedIndex > 0)
            {
                LoadDataToToolbox.ComboBoxData(cbo_TeamConvert, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != '" + cbo_TeamDefault.SelectedValue + "' AND RecordStatus < 99", "---- Choose Teams ----");
            }
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            Employee_Info zInfo = new Employee_Info();
            zInfo.Key = cbo_Name.SelectedValue.ToString();
            zInfo.TeamKey = cbo_TeamConvert.SelectedValue.ToInt();
            zInfo.UpdateTeam();
        }
    }
}
