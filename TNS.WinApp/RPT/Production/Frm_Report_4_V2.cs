﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_4_V2 : Form
    {
        int _ViewTeam = 0;
        public Frm_Report_4_V2()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            btn_Search.Click += btn_Search_Click;
            btn_Export.Click += btn_Export_Click;

            btn_Print.Click += Btn_Print_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
            btn_Done.Click += Btn_Done_Click;

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);


            InitLayout_GV(GVData);


        }

        private void Frm_Report_4_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY RANK";
            LoadDataToToolbox.KryptonComboBox(cbo_TeamID_Search, zSQL, "---Chọn tất cả---");

            // Khởi tạo thông tin báo cáo
            LoadInfo_Report();
            Panel_Done.Visible = false;
        }
        private void InitLayout_GV(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("CardID", "Mã NV");

            GV.Columns.Add("NgoaiGio", "Ngoài giờ");
            GV.Columns.Add("KhoanSP", "Khoán SP (Tại tổ)");                                    //Lương Ngày Thường
            GV.Columns.Add("LamCN", "Làm thêm CN (Tại tổ)");                          //Lương Ngày Chủ Nhật
            GV.Columns.Add("LamLe", "Làm thêm lễ (Tại tổ)");                                                //Lương Ngày Lễ

            GV.Columns.Add("KhoanSPKhac", "Khoán SP (Tổ khác)");               //Lương Khác Ngày Thường
            GV.Columns.Add("KhoanSPCN", "Khoán SP CN (Tổ khác)");            //Lương Khác Ngày Chủ Nhật
            GV.Columns.Add("KhoanSPLe", "Khoán SP lễ (Tổ khác)");       //Lương Việc Ngày Ngày Lễ
            GV.Columns.Add("Tong", "Tổng cộng");

            GV.Columns["No"].Width = 30;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 300;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["EmployeeName"].ReadOnly = true;

            GV.Columns["CardID"].Width = 120;
            GV.Columns["CardID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["CardID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["CardID"].ReadOnly = true;
            GV.Columns["CardID"].Frozen = true;

            GV.Columns["NgoaiGio"].Width = 110;
            GV.Columns["NgoaiGio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["NgoaiGio"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSP"].Width = 110;
            GV.Columns["KhoanSP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSP"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["LamCN"].Width = 110;
            GV.Columns["LamCN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["LamCN"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["LamLe"].Width = 110;
            GV.Columns["LamLe"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["LamLe"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSPKhac"].Width = 110;
            GV.Columns["KhoanSPKhac"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSPKhac"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSPLe"].Width = 110;
            GV.Columns["KhoanSPLe"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSPLe"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSPCN"].Width = 110;
            GV.Columns["KhoanSPCN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSPCN"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["Tong"].Width = 110;
            GV.Columns["Tong"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Tong"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 52;
        }

        private void HeaderRow(DataGridViewRow RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView.Cells["No"].Value = "";
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = TeamName;
            RowView.Cells["CardID"].Value = "Số công nhân " + Table.Select("TeamKey=" + TeamKey).Length;

            RowView.Cells["NgoaiGio"].Value = Table.Compute("SUM([NgoaiGio])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSP"].Value = Table.Compute("SUM([KhoanSP])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["LamCN"].Value = Table.Compute("SUM([LamCN])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["LamLe"].Value = Table.Compute("SUM([LamLe])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSPKhac"].Value = Table.Compute("SUM([KhoanSPKhac])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSPLe"].Value = Table.Compute("SUM([KhoanSPLe])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSPCN"].Value = Table.Compute("SUM([KhoanSPCN])", "TeamKey=" + TeamKey).Toe0String();

            RowView.Cells["Tong"].Value = (double.Parse(Table.Compute("SUM([NgoaiGio])", "TeamKey=" + TeamKey).ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSP])", "TeamKey=" + TeamKey).ToString())
                                                + double.Parse(Table.Compute("SUM([LamCN])", "TeamKey=" + TeamKey).ToString())
                                                + double.Parse(Table.Compute("SUM([LamLe])", "TeamKey=" + TeamKey).ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSPKhac])", "TeamKey=" + TeamKey).ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSPLe])", "TeamKey=" + TeamKey).ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSPCN])", "TeamKey=" + TeamKey).ToString())
                                                ).Toe0String();

            RowView.DefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        private void DetailRow(DataGridViewRow RowView, DataRow rDetail, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = rDetail["EmployeeKey"].ToString().Trim();
            RowView.Cells["EmployeeName"].Value = rDetail["EmployeeName"].ToString().Trim();
            RowView.Cells["CardID"].Value = rDetail["EmployeeID"].ToString().Trim();

            RowView.Cells["NgoaiGio"].Value = rDetail["NgoaiGio"].Toe0String();
            RowView.Cells["KhoanSP"].Value = rDetail["KhoanSP"].Toe0String();
            RowView.Cells["LamCN"].Value = rDetail["LamCN"].Toe0String();
            RowView.Cells["LamLe"].Value = rDetail["LamLe"].Toe0String();
            RowView.Cells["KhoanSPKhac"].Value = rDetail["KhoanSPKhac"].Toe0String();
            RowView.Cells["KhoanSPCN"].Value = rDetail["KhoanSPCN"].Toe0String();
            RowView.Cells["KhoanSPLe"].Value = rDetail["KhoanSPLe"].Toe0String();

            double zTotalRow = 0;
            for (int i = 5; i < rDetail.ItemArray.Length - 4; i++) // 3 dòng sắp xếp không tính
            {
                double zTemp = 0;
                if (double.TryParse(rDetail[i].ToString(), out zTemp))
                {

                }
                zTotalRow += zTemp;
            }

            RowView.Cells["Tong"].Value = zTotalRow.Toe0String();
        }
        private void TotalRow(DataGridViewRow RowView, DataTable Table, int No)
        {
            RowView.Cells["No"].Value = "";
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = "Tổng cộng";
            RowView.Cells["CardID"].Value = "";

            RowView.Cells["NgoaiGio"].Value = Table.Compute("SUM([NgoaiGio])", "").Toe0String();
            RowView.Cells["KhoanSP"].Value = Table.Compute("SUM([KhoanSP])", "").Toe0String();
            RowView.Cells["LamCN"].Value = Table.Compute("SUM([LamCN])", "").Toe0String();
            RowView.Cells["LamLe"].Value = Table.Compute("SUM([LamLe])", "").Toe0String();
            RowView.Cells["KhoanSPKhac"].Value = Table.Compute("SUM([KhoanSPKhac])", "").Toe0String();
            RowView.Cells["KhoanSPLe"].Value = Table.Compute("SUM([KhoanSPLe])", "").Toe0String();
            RowView.Cells["KhoanSPCN"].Value = Table.Compute("SUM([KhoanSPCN])", "").Toe0String();

            RowView.Cells["Tong"].Value = (double.Parse(Table.Compute("SUM([NgoaiGio])", "").ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSP])", "").ToString())
                                                + double.Parse(Table.Compute("SUM([LamCN])", "").ToString())
                                                + double.Parse(Table.Compute("SUM([LamLe])", "").ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSPKhac])", "").ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSPLe])", "").ToString())
                                                + double.Parse(Table.Compute("SUM([KhoanSPCN])", "").ToString())
                                                ).Toe0String();
            RowView.DefaultCellStyle.Font = new Font("Tahoma", 10, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LightSkyBlue;
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {

            GVData.Rows.Clear();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            if (cbo_TeamID_Search.SelectedIndex >= 0)
                _ViewTeam = cbo_TeamID_Search.SelectedValue.ToInt();

            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cbo_TeamID_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }
        private int _STT = 1;
        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);

            DataTable zTable = HoTroSanXuat.TongHopLuongNangSuat_V2(_ViewTeam, FromDate, ToDate);

            if (zTable.Rows.Count > 0)
            {
                //Sắp xếp
                zTable.Columns.Add("EmployeeIDRank");
                for (int i = 0; i < zTable.Rows.Count; i++)
                {
                    zTable.Rows[i]["EmployeeIDRank"] = ConvertIDRank(zTable.Rows[i][2].ToString());
                }
                DataView dv = zTable.DefaultView;
                dv.Sort = "BranchRank ASC,DepartmentRank ASC, TeamRank ASC , EmployeeIDRank ASC";
                zTable = dv.ToTable();
                zTable.Columns.Remove("EmployeeIDRank");
                //--sắp xếp xong xóa cột sắp xếp đi

                this.Invoke(new MethodInvoker(delegate ()
                {
                    DataGridViewRow zGvRow;
                    int NoGroup = 1;
                    int RowTam = NoGroup;
                    _STT = 1;
                    #region [GROUP]
                    int TeamKey = zTable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = zTable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = zTable.Select("TeamKey='" + TeamKey + "'");
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[0];
                        zGvRow.Tag = TeamKey;
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    #endregion

                    for (int i = 0; i < zTable.Rows.Count; i++)
                    {
                        DataRow r = zTable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = zTable.Select("TeamKey='" + TeamKey + "'");
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }

                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, zTable, RowTam + 1);
                }));
            }
        }

        private void btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cbo_TeamID_Search.SelectedValue.ToInt() != 0)
            {
                string[] s = cbo_TeamID_Search.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }


            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Bảng_Thông_Tin_Tổng_Hợp_Lương_Khoán_Năng_Suất" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    if (GVData.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        foreach (DataGridViewColumn col in GVData.Columns)
                        {
                            dt.Columns.Add(col.HeaderText);
                        }

                        foreach (DataGridViewRow row in GVData.Rows)
                        {
                            DataRow dRow = dt.NewRow();
                            foreach (DataGridViewCell cell in row.Cells)
                            {
                                dRow[cell.ColumnIndex] = cell.Value;
                            }
                            dt.Rows.Add(dRow);
                        }
                        string Message = ExportTableToExcel(dt, Path);
                        if (Message != "OK")
                        {
                            Utils.TNMessageBox(Message, 4);
                        }
                        else
                        {
                            string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                            Process.Start(Path);
                        }
                    }
                    else
                    {
                        Utils.TNMessageBoxOK("Không tìm thấy dữ liệu!", 1);
                    }
                }

            }
        }

        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("LKNS");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    //worksheet.Cells.AutoFilter = true;  //thêm lọc khi xuất excel
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 15;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 15;
                    worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        for (int j = 4; j <= zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }
        #region[In]
        private void LoadInfo_Report()
        {
            //Ngày lập
            dte_RpDate.Value = SessionUser.Date_Work;
            //Tên báo cáo
            txt_RpTitle.Text = "BÁO CÁO TỔNG HỢP LƯƠNG KHOÁN";
            //Người lập
            txt_RpNguoiLap.Text = SessionUser.UserLogin.EmployeeName;
            //Kế toán trưởng
            txt_RpKeToan.Text = "Trần Tấn Long Thạch";
            //HCNS
            txt_RpHCNS.Text = "Lê Văn Hòa";
            //Tổng giám đốc
            txt_RpGiamDoc.Text = "Nguyễn Vũ Lộc";
        }
        private void Btn_Print_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                txt_RpTitle.Text = "BÁO CÁO TỔNG HỢP LƯƠNG KHOÁN THÁNG " + dte_RpDate.Value.Month + " NĂM " + dte_RpDate.Value.Year;
                Panel_Done.Visible = true;
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu", 1);
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = txt_RpTitle.Text+".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    XuatExcelMau(newFile);
                    Process.Start(Path);
                }

            }
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }
        private void XuatExcelMau(FileInfo newFile)
        {
            string TieuDe = txt_RpTitle.Text;
            string Font = "Time New Roman";
            string LapBang = txt_RpNguoiLap.Text;
            string KeToan = txt_RpKeToan.Text;
            string NhanSu = txt_RpHCNS.Text;
            string GiamDoc = txt_RpGiamDoc.Text;

            var fileMau = new FileInfo(Application.StartupPath + @"\FileTemplate\MauBaoCao_So1.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(fileMau))
            {
                ExcelWorksheet workSheet = xlPackage.Workbook.Worksheets.FirstOrDefault();

                //tham số chỉ định tùy chọn
                int ColumnGV = GVData.ColumnCount;  //tổng cột cần hiển thị của Gidview
                int RowGV = GVData.Rows.Count; //số dòng dữ liệu Gidview
                int RowExcel = 6;  //dòng bắt đầu cần ghi dữ liệu trong excel               

                #region 2. DATA DỮ LIỆU   

                //Dong headder
                workSheet.Cells[RowExcel + 1, 1].Value = "STT";
                workSheet.Cells[RowExcel + 1, 2].Value = "Tên nhân viên";
                workSheet.Cells[RowExcel + 1, 3].Value = "Mã nhân viên";
                workSheet.Cells[RowExcel + 1, 4].Value = "Ngoài giờ";
                workSheet.Cells[RowExcel + 1, 5].Value = "Khoán SP (Tại tổ)";
                workSheet.Cells[RowExcel + 1, 6].Value = "Làm thêm CN (Tại tổ)";
                workSheet.Cells[RowExcel + 1, 7].Value = "Làm thêm lễ (Tại tổ)";
                workSheet.Cells[RowExcel + 1, 8].Value = "Khoán SP (Tổ khác)";
                workSheet.Cells[RowExcel + 1, 9].Value = "Khoán SP CN (Tổ khác)";
                workSheet.Cells[RowExcel + 1, 10].Value = "Khoán SP lễ (Tổ khác)";
                workSheet.Cells[RowExcel + 1, 11].Value = "Tổng cộng";


                for (int i = 2; i <= RowGV; i++)
                {
                    int rowGV = i - 1;
                    DataGridViewRow row = GVData.Rows[rowGV];
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;
                        object val = row.Cells[colGV].Value;

                        if (val != null)
                        {
                            workSheet.Cells[RowExcel + i, c].Value = val.ToString();
                            //Dòng tổng thì in đậm
                            if (GVData.Rows[rowGV].Cells[0].Value.ToString() == "")
                            {
                                workSheet.Cells[RowExcel + i, c].Style.Font.Color.SetColor(Color.Navy);
                                workSheet.Cells[RowExcel + i, c].Style.Font.Bold = true;
                            }
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Value = string.Empty;
                        }
                        if (c == 2 || c == 3)
                        { // cot ten nhan vien
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Numberformat.Format = "0.00";
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                    }


                }

                #endregion

                #region 3. ĐỊNH DẠNG  DATA EXCEL
                //DÒNG THỨ 7 TRONG EXCEL ĐẾN DÒNG 8 TRONG EXCEL LÀ TIÊU ĐỀ BẢNG
                for (int r = 7; r < 8; r++)
                {
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;
                        workSheet.Cells[r, c].Style.Font.Name = Font;
                        workSheet.Cells[r, c].Style.Font.Size = 12;
                        workSheet.Cells[r, c].Style.Font.Bold = true;
                        workSheet.Cells[r, c].Style.Font.Color.SetColor(Color.Navy);
                        workSheet.Cells[r, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[r, c].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[r, c].Style.WrapText = true;
                        //workSheet.Column(c).Width = 15;
                        workSheet.Column(c).AutoFit();

                    }
                }
                workSheet.Column(ColumnGV).Style.Font.Bold = true;

                #endregion

                #region[4.Trộn ngang > dọc tự động tiêu đề column báo cáo]       
                //MergeAutoExcel(workSheet, 7, 8, 1, ColumnGV);
                #endregion

                #region 1. TÊN BÁO CÁO EXCEL
                //Thêm các dường line excel
                workSheet.Cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                // Dòng Tên báo cáo
                workSheet.Cells["A4:K4"].Merge = true;
                workSheet.Cells["A4"].Value = TieuDe.ToUpper();
                workSheet.Cells["A4"].Style.Font.Size = 16;
                workSheet.Cells["A4"].Style.Font.Name = Font;
                workSheet.Cells["A4"].Style.Font.Bold = true;
                workSheet.Cells["A4"].Style.Font.Color.SetColor(Color.Navy);
                workSheet.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells["A4"].Style.WrapText = true;
                workSheet.Row(4).Height = 40;
                workSheet.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                // Xóa các đường line Từ dòng 1 đến dòng tiêu đề
                workSheet.Cells["1:6"].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:5"].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[6.Khung chữ ký]
                // Ngày ký
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV-1].Value = "Ngày " + dte_RpDate.Value.Day + " tháng " + dte_RpDate.Value.Month + " năm " + dte_RpDate.Value.Year;
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV-1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV-1].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 2, ColumnGV-1].Style.Font.Color.SetColor(Color.Navy);

                //Tiêu đề Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 3, 2].Value = "Lập bảng";
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Color.SetColor(Color.Black);
                // Tên Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 9, 2].Value = LapBang;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 3, 3].Value = "Kế toán trưởng";
                workSheet.Cells[RowExcel + RowGV + 3, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 3].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 3].Style.Font.Color.SetColor(Color.Black);
                //Tên kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 9, 3].Value = KeToan;
                workSheet.Cells[RowExcel + RowGV + 9, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 3].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 3].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề BP HCNS
                workSheet.Cells[RowExcel + RowGV + 3, 6].Value = "BP HCNS";
                workSheet.Cells[RowExcel + RowGV + 3, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 6].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 6].Style.Font.Color.SetColor(Color.Black);
                //Tên Trưởng phòng HCNS
                workSheet.Cells[RowExcel + RowGV + 9, 6].Value = NhanSu;
                workSheet.Cells[RowExcel + RowGV + 9, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 6].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 6].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 1].Value = "Tổng giám đốc";
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 1].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, ColumnGV - 1].Style.Font.Color.SetColor(Color.Black);
                //Tên tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 1].Value = GiamDoc;
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 1].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, ColumnGV - 1].Style.Font.Color.SetColor(Color.Black);

                //Xóa đường line các dòng ký tên
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Bottom.Style = ExcelBorderStyle.None;

                #endregion

                #region[5.Định dạng màu,độ rộng các cột fit]
                workSheet.Column(1).Width = 7;
                workSheet.Row(7).Height = 80; //Độ cao tiêu đề
                workSheet.Column(1).Width = 7;
                workSheet.Column(2).Width = 30;
                workSheet.Column(3).Width = 18;
                #endregion

                #region[7.Tiện ích Cài đặt in]
                workSheet.PrinterSettings.PaperSize = ePaperSize.A4; //Khổ giấy
                workSheet.PrinterSettings.PrintArea = workSheet.Cells[1, 1, RowExcel + RowGV + 9, ColumnGV];//Khu vực in
                workSheet.PrinterSettings.PageOrder = ePageOrder.OverThenDown;//Kiểu in chữ Z
                workSheet.PrinterSettings.RepeatRows = workSheet.Cells["7:8"];// Tiêu đề lặp lại khi qua trang khác
                //workSheet.PrinterSettings.RepeatColumns = workSheet.Cells["A:A"];// Colunm lặp lại khi qua trang khác
                workSheet.PrinterSettings.Orientation = eOrientation.Portrait;//Trang giấy đứng
                workSheet.View.PageBreakView = true;//Đường phân cách trang in
                workSheet.PrinterSettings.FitToPage = true;//In độ cao rộng vào 1 trang duy nhất
                workSheet.PrinterSettings.FitToWidth = 1;//Chiều rộng cột đúng 1 trang
                workSheet.PrinterSettings.FitToHeight = 0;//Tự động cắt trang
                #endregion

                xlPackage.SaveAs(newFile);
            }
        }
        //cấp 1 trộn dòng cố định
        private void MergeDongExcel(ExcelWorksheet workSheet, int currentRow, int fromCol, int ToCol)
        {
            try
            {
                workSheet.Cells[currentRow, fromCol, currentRow, ToCol].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[currentRow, fromCol + 1, currentRow, ToCol].Merge = true;
            }
        }
        //cấp 2 trộn cột cố định
        private void MergeCotExcel(ExcelWorksheet workSheet, int currentColumn, int fromRow, int toRow)
        {
            try
            {
                workSheet.Cells[fromRow, currentColumn, toRow, currentColumn].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[fromRow + 1, currentColumn, toRow, currentColumn].Merge = true;
            }
        }
        //cấp 3 trộn kết hợp
        private void MergeAutoExcel(ExcelWorksheet workSheet, int fromRow, int toRow, int fromCol, int toCol)
        {
            string Val = workSheet.Cells[fromRow, fromCol].Value.ToString();
            int TronCot = 0;

            #region [Trộn cột :(dòng trên dòng dưới) ]

            for (int r = fromRow; r < toRow; r++)
            {
                for (int c = fromCol + 1; c < toCol; c++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();

                    if (Val == Dta)
                    {
                        TronCot++;
                    }
                    else
                    {
                        if (TronCot >= 1)
                        {
                            try
                            {
                                workSheet.Cells[r, c - TronCot - 1, r, c - 1].Merge = true;
                                TronCot = 0;
                            }
                            catch (Exception)
                            {

                            }
                        }
                        Val = Dta;
                    }
                }

                //trộn cuối cùng 
                if (Val == workSheet.Cells[r, toCol, r, toCol].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (TronCot >= 1)
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol - 1].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }
            #endregion

            #region [Trộn dòng :(cột trái cột phải)]

            for (int c = fromCol; c <= toCol; c++)
            {
                int TronDong = 0;
                Val = workSheet.Cells[fromRow, c].Value.ToString();
                for (int r = fromRow + 1; r <= toRow; r++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();
                    if (Val == Dta)
                    {
                        TronDong++;
                    }
                    else
                    {
                        Val = Dta;
                    }
                }

                if (TronDong > 0)
                {
                    try
                    {
                        workSheet.Cells[fromRow, c, fromRow + TronDong, c].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                if (c == toCol && Val == workSheet.Cells[fromRow, c].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[toRow - TronDong, toCol, toRow, toCol].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
            #endregion
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
