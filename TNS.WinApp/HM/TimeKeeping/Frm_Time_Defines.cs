﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.HumanResources;
using TN.Library.Miscellaneous;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Time_Defines : Form
    {
        #region[Private]
        private int _CodeKey = 0;
        #endregion
        public Frm_Time_Defines()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GV_List.Click += GV_List_Click;
            GV_List.KeyDown += GV_List_KeyDown;
            GV_List_Layout();
        }

        private void GV_List_KeyDown(object sender, KeyEventArgs e)
        {
            string zMessage = "";
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GV_List.Rows.Count; i++)
                    {
                        if (GV_List.Rows[i].Selected)
                        {
                            if (GV_List.Rows[i].Tag != null)
                            {
                                Time_Defines_Info zinfo = new Time_Defines_Info();
                                zinfo.Key = int.Parse(GV_List.Rows[i].Tag.ToString());
                                zinfo.Delete();
                                zMessage += TN_Message.Show(zinfo.Message);
                            }
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        MessageBox.Show("Đã xóa", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GV_List_LoadData();
                        _CodeKey = 0;
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
       
        DataTable ztbCategory = new DataTable();
        DataTable ztbSlug = new DataTable();
        private void Frm_Time_Defines_Load(object sender, EventArgs e)
        {
            LoadTableCategory();
            LoadTableSlug();
            LoadDataToToolbox.ComboBoxData(cbo_Category, ztbCategory, false, 0, 1);
            LoadDataToToolbox.ComboBoxData(cbo_Slug, ztbSlug, false, 0, 1);
            GV_List_LoadData();
            LoadData();
        }

        #region[Progess]

        void LoadTableCategory()
        {
            ztbCategory = new DataTable();
            ztbCategory.Columns.Add("Value");
            ztbCategory.Columns.Add("Name");
            ztbCategory.Rows.Add("0", "--Chọn--");
            ztbCategory.Rows.Add("1", "Văn phòng");
            ztbCategory.Rows.Add("2", "Công nhân");
            ztbCategory.Rows.Add("3", "Hỗ trợ");

        }
        void LoadTableSlug()
        {
            ztbSlug = new DataTable();
            ztbSlug.Columns.Add("Value");
            ztbSlug.Columns.Add("Name");
            ztbSlug.Rows.Add("0", "--Chọn--");
            ztbSlug.Rows.Add("1", "Được tính chuyên cần");
            ztbSlug.Rows.Add("2", "Không được tính chuyên cần");

        }
        private void LoadData()
        {

            Time_Defines_Info zinfo = new Time_Defines_Info(_CodeKey);
            txt_PermitName.Text = zinfo.CodeName;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            cbo_Category.SelectedValue = zinfo.CategoryKey.ToString();
            cbo_Slug.SelectedValue = zinfo.Slug.ToString();
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_PermitName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Time_Defines_Data.CountID(txt_PermitName.Text.Trim().ToUpper()) > 0 && _CodeKey == 0)
            {
                MessageBox.Show("Mã chấm công đã tồn tại !.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (cbo_Category.SelectedIndex == 0)
            {
                MessageBox.Show("Chưa chọn loại áp dụng!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
             if(cbo_Slug.SelectedIndex == 0)
            {
                MessageBox.Show("Chưa chọn loại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                string zMessage = "";
                Time_Defines_Info zinfo = new Time_Defines_Info(_CodeKey);
                zinfo.CodeName = txt_PermitName.Text.Trim().ToUpper();
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.CategoryKey = int.Parse(cbo_Category.SelectedValue.ToString());
                zinfo.Slug = int.Parse(cbo_Slug.SelectedValue.ToString());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                zMessage = TN_Message.Show(zinfo.Message);
                if (zMessage =="")
                {
                    MessageBox.Show("Cập nhật thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GV_List_LoadData();
                    _CodeKey = zinfo.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show(zMessage, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (_CodeKey == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Time_Defines_Info zinfo = new Time_Defines_Info(_CodeKey);
                    zinfo.Delete();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage.Length == 0)
                    {
                        MessageBox.Show("Đã xóa", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GV_List_LoadData();
                        _CodeKey = 0;
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _CodeKey = 0;
            LoadData();
            txt_PermitName.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            GV_List_LoadData();
        }
        #endregion

        #region[ListView]

        private void GV_List_Layout()
        {
            // Setup Column 
            GV_List.Columns.Add("No", "STT");
            GV_List.Columns.Add("CodeName", "Mã");
            GV_List.Columns.Add("Description", "Nội dung");

            GV_List.Columns["No"].Width = 40;
            GV_List.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_List.Columns["No"].ReadOnly = true;

            GV_List.Columns["CodeName"].Width = 68;
            GV_List.Columns["CodeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_List.Columns["Description"].Width = 195;
            GV_List.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_List.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;




            GV_List.BackgroundColor = Color.White;
            GV_List.GridColor = Color.FromArgb(227, 239, 255);
            GV_List.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_List.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_List.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_List.ReadOnly = true;

            GV_List.AllowUserToResizeRows = false;
            GV_List.AllowUserToResizeColumns = true;

            GV_List.RowHeadersVisible = false;
            GV_List.AllowUserToAddRows = true;
            GV_List.AllowUserToDeleteRows = false;
            GV_List.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV_List.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_List.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_List.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_List.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            for (int i = 1; i <= 8; i++)
            {
                GV_List.Rows.Add();
            }

        }
        private void GV_List_LoadData()
        {

            GV_List.Rows.Clear();
            DataTable In_Table = new DataTable();
            In_Table = Time_Defines_Data.Search(txt_Search.Text);
            int i = 0;
            foreach (DataRow nRow in In_Table.Rows)
            {
                GV_List.Rows.Add();
                DataGridViewRow nRowView = GV_List.Rows[i];
                nRowView.Tag = nRow["CodeKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["CodeName"].Value = nRow["CodeName"].ToString().Trim();
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                i++;
            }

        }

        #endregion

        private void GV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GV_List.Rows.Count; i++)
            {
                if (GV_List.CurrentRow.Selected == true)
                {
                    GV_List.Rows[i].DefaultCellStyle.BackColor = SystemColors.Window; // normal item

                }
                else
                {
                    GV_List.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue; // highlighted item
                }
            }

            if (GV_List.CurrentRow.Tag != null)
            {
                _CodeKey = int.Parse(GV_List.CurrentRow.Tag.ToString());
                LoadData();
            }
            else
            {
                _CodeKey = 0;
                LoadData();
            }

        }
        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {
           
                if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
                {

                    e.Handled = false;

                }
                else
                {
                    e.Handled = true;
                }
           
        }
    }
}
