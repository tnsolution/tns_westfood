﻿namespace TNS.WinApp
{
    partial class Frm_SearchProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LV_Search = new System.Windows.Forms.ListView();
            this.txt_Product = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LV_Search
            // 
            this.LV_Search.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Search.FullRowSelect = true;
            this.LV_Search.GridLines = true;
            this.LV_Search.HideSelection = false;
            this.LV_Search.Location = new System.Drawing.Point(0, 19);
            this.LV_Search.Name = "LV_Search";
            this.LV_Search.Size = new System.Drawing.Size(502, 171);
            this.LV_Search.TabIndex = 180;
            this.LV_Search.UseCompatibleStateImageBehavior = false;
            this.LV_Search.View = System.Windows.Forms.View.Details;
            this.LV_Search.ItemActivate += new System.EventHandler(this.LV_Search_ItemActivate);
            // 
            // txt_Product
            // 
            this.txt_Product.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_Product.Dock = System.Windows.Forms.DockStyle.Top;
            this.txt_Product.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Product.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Product.Location = new System.Drawing.Point(0, 0);
            this.txt_Product.Multiline = true;
            this.txt_Product.Name = "txt_Product";
            this.txt_Product.Size = new System.Drawing.Size(502, 19);
            this.txt_Product.TabIndex = 179;
            // 
            // Frm_SearchProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 190);
            this.Controls.Add(this.LV_Search);
            this.Controls.Add(this.txt_Product);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(400, 400);
            this.Name = "Frm_SearchProduct";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm kiếm sản phẩm";
            this.Load += new System.EventHandler(this.Frm_SearchProduct_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView LV_Search;
        private System.Windows.Forms.TextBox txt_Product;
    }
}