﻿namespace TNS.WinApp.CORE.Import
{
    partial class Frm_Import_Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Import_Order));
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GVTeam = new System.Windows.Forms.DataGridView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Order = new System.Windows.Forms.Panel();
            this.GVOrder = new System.Windows.Forms.DataGridView();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.Panel_Employee = new System.Windows.Forms.Panel();
            this.GVEmployee = new System.Windows.Forms.DataGridView();
            this.txt_title = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnHideInfo = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnShowInfo = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.Panel_Order_Rate = new System.Windows.Forms.Panel();
            this.GVRate = new System.Windows.Forms.DataGridView();
            this.kryptonHeader9 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.txt_Quantity = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Price = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_Time = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_Basket = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_Kilogram = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).BeginInit();
            this.Panel_Order.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).BeginInit();
            this.Panel_Employee.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVEmployee)).BeginInit();
            this.Panel_Right.SuspendLayout();
            this.Panel_Order_Rate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVRate)).BeginInit();
            this.Panel_Info.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTitle
            // 
            this.txtTitle.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtTitle.Size = new System.Drawing.Size(1266, 42);
            this.txtTitle.TabIndex = 221;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Nhập Excel đơn hàng, công việc";
            this.txtTitle.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 42);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Panel_Order);
            this.splitContainer1.Panel1.Controls.Add(this.Panel_Team);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.Panel_Employee);
            this.splitContainer1.Panel2.Controls.Add(this.splitter1);
            this.splitContainer1.Panel2.Controls.Add(this.Panel_Right);
            this.splitContainer1.Size = new System.Drawing.Size(1266, 726);
            this.splitContainer1.SplitterDistance = 271;
            this.splitContainer1.TabIndex = 222;
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GVTeam);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Team.Location = new System.Drawing.Point(0, 0);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(271, 220);
            this.Panel_Team.TabIndex = 205;
            // 
            // GVTeam
            // 
            this.GVTeam.AllowUserToAddRows = false;
            this.GVTeam.AllowUserToDeleteRows = false;
            this.GVTeam.AllowUserToResizeColumns = false;
            this.GVTeam.AllowUserToResizeRows = false;
            this.GVTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVTeam.ColumnHeadersHeight = 25;
            this.GVTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVTeam.Location = new System.Drawing.Point(0, 30);
            this.GVTeam.Name = "GVTeam";
            this.GVTeam.ReadOnly = true;
            this.GVTeam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVTeam.Size = new System.Drawing.Size(271, 190);
            this.GVTeam.TabIndex = 203;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(271, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 200;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "1-Đơn hàng gốc";
            // 
            // Panel_Order
            // 
            this.Panel_Order.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Order.Controls.Add(this.GVOrder);
            this.Panel_Order.Controls.Add(this.kryptonHeader2);
            this.Panel_Order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order.Location = new System.Drawing.Point(0, 220);
            this.Panel_Order.Name = "Panel_Order";
            this.Panel_Order.Size = new System.Drawing.Size(271, 506);
            this.Panel_Order.TabIndex = 215;
            // 
            // GVOrder
            // 
            this.GVOrder.AllowUserToAddRows = false;
            this.GVOrder.AllowUserToDeleteRows = false;
            this.GVOrder.AllowUserToResizeColumns = false;
            this.GVOrder.AllowUserToResizeRows = false;
            this.GVOrder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVOrder.ColumnHeadersHeight = 25;
            this.GVOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVOrder.Location = new System.Drawing.Point(0, 30);
            this.GVOrder.Name = "GVOrder";
            this.GVOrder.ReadOnly = true;
            this.GVOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVOrder.Size = new System.Drawing.Size(271, 476);
            this.GVOrder.TabIndex = 203;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(271, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 200;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "2-Đơn hàng phần mềm";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(777, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 726);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // Panel_Employee
            // 
            this.Panel_Employee.Controls.Add(this.GVEmployee);
            this.Panel_Employee.Controls.Add(this.txt_title);
            this.Panel_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Employee.Location = new System.Drawing.Point(0, 0);
            this.Panel_Employee.Name = "Panel_Employee";
            this.Panel_Employee.Size = new System.Drawing.Size(777, 726);
            this.Panel_Employee.TabIndex = 212;
            // 
            // GVEmployee
            // 
            this.GVEmployee.AllowUserToAddRows = false;
            this.GVEmployee.AllowUserToDeleteRows = false;
            this.GVEmployee.AllowUserToResizeColumns = false;
            this.GVEmployee.AllowUserToResizeRows = false;
            this.GVEmployee.ColumnHeadersHeight = 25;
            this.GVEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVEmployee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVEmployee.Location = new System.Drawing.Point(0, 30);
            this.GVEmployee.Name = "GVEmployee";
            this.GVEmployee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVEmployee.Size = new System.Drawing.Size(777, 696);
            this.GVEmployee.TabIndex = 201;
            // 
            // txt_title
            // 
            this.txt_title.AutoSize = false;
            this.txt_title.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnHideInfo,
            this.btnShowInfo});
            this.txt_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.txt_title.Location = new System.Drawing.Point(0, 0);
            this.txt_title.Name = "txt_title";
            this.txt_title.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_title.Size = new System.Drawing.Size(777, 30);
            this.txt_title.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_title.TabIndex = 203;
            this.txt_title.Values.Description = "";
            this.txt_title.Values.Heading = "....";
            // 
            // btnHideInfo
            // 
            this.btnHideInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnHideInfo.Image")));
            this.btnHideInfo.UniqueName = "0010BF7FD3B9491A00975C9A919D6B8A";
            // 
            // btnShowInfo
            // 
            this.btnShowInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnShowInfo.Image")));
            this.btnShowInfo.UniqueName = "8D0A4B861A87443FB6855495AB7AD05A";
            // 
            // Panel_Right
            // 
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Right.Controls.Add(this.Panel_Order_Rate);
            this.Panel_Right.Controls.Add(this.Panel_Info);
            this.Panel_Right.Controls.Add(this.kryptonHeader5);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Right;
            this.Panel_Right.Location = new System.Drawing.Point(781, 0);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(210, 726);
            this.Panel_Right.TabIndex = 217;
            // 
            // Panel_Order_Rate
            // 
            this.Panel_Order_Rate.Controls.Add(this.GVRate);
            this.Panel_Order_Rate.Controls.Add(this.kryptonHeader9);
            this.Panel_Order_Rate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order_Rate.Location = new System.Drawing.Point(0, 181);
            this.Panel_Order_Rate.Name = "Panel_Order_Rate";
            this.Panel_Order_Rate.Size = new System.Drawing.Size(210, 545);
            this.Panel_Order_Rate.TabIndex = 211;
            // 
            // GVRate
            // 
            this.GVRate.AllowUserToAddRows = false;
            this.GVRate.AllowUserToDeleteRows = false;
            this.GVRate.AllowUserToResizeColumns = false;
            this.GVRate.AllowUserToResizeRows = false;
            this.GVRate.ColumnHeadersHeight = 25;
            this.GVRate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVRate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVRate.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GVRate.Location = new System.Drawing.Point(0, 30);
            this.GVRate.Name = "GVRate";
            this.GVRate.ReadOnly = true;
            this.GVRate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVRate.Size = new System.Drawing.Size(210, 515);
            this.GVRate.TabIndex = 201;
            // 
            // kryptonHeader9
            // 
            this.kryptonHeader9.AutoSize = false;
            this.kryptonHeader9.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader9.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader9.Name = "kryptonHeader9";
            this.kryptonHeader9.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader9.Size = new System.Drawing.Size(210, 30);
            this.kryptonHeader9.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader9.TabIndex = 202;
            this.kryptonHeader9.Values.Description = "";
            this.kryptonHeader9.Values.Heading = "Hệ số đơn hàng";
            // 
            // Panel_Info
            // 
            this.Panel_Info.Controls.Add(this.txt_Quantity);
            this.Panel_Info.Controls.Add(this.txt_Price);
            this.Panel_Info.Controls.Add(this.label15);
            this.Panel_Info.Controls.Add(this.txt_Time);
            this.Panel_Info.Controls.Add(this.label13);
            this.Panel_Info.Controls.Add(this.txt_Basket);
            this.Panel_Info.Controls.Add(this.label16);
            this.Panel_Info.Controls.Add(this.txt_Kilogram);
            this.Panel_Info.Controls.Add(this.label5);
            this.Panel_Info.Controls.Add(this.label18);
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Info.Location = new System.Drawing.Point(0, 30);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(210, 151);
            this.Panel_Info.TabIndex = 217;
            // 
            // txt_Quantity
            // 
            this.txt_Quantity.Location = new System.Drawing.Point(97, 7);
            this.txt_Quantity.Name = "txt_Quantity";
            this.txt_Quantity.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Quantity.Size = new System.Drawing.Size(96, 26);
            this.txt_Quantity.StateCommon.Border.ColorAngle = 1F;
            this.txt_Quantity.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Quantity.StateCommon.Border.Rounding = 4;
            this.txt_Quantity.StateCommon.Border.Width = 1;
            this.txt_Quantity.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Quantity.TabIndex = 212;
            this.txt_Quantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Price
            // 
            this.txt_Price.Location = new System.Drawing.Point(97, 118);
            this.txt_Price.Name = "txt_Price";
            this.txt_Price.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Price.Size = new System.Drawing.Size(96, 26);
            this.txt_Price.StateCommon.Border.ColorAngle = 1F;
            this.txt_Price.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Price.StateCommon.Border.Rounding = 4;
            this.txt_Price.StateCommon.Border.Width = 1;
            this.txt_Price.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Price.TabIndex = 216;
            this.txt_Price.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label15.Location = new System.Drawing.Point(9, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(88, 15);
            this.label15.TabIndex = 201;
            this.label15.Text = "Kg nguyên liệu";
            // 
            // txt_Time
            // 
            this.txt_Time.Location = new System.Drawing.Point(97, 90);
            this.txt_Time.Name = "txt_Time";
            this.txt_Time.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Time.Size = new System.Drawing.Size(96, 26);
            this.txt_Time.StateCommon.Border.ColorAngle = 1F;
            this.txt_Time.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Time.StateCommon.Border.Rounding = 4;
            this.txt_Time.StateCommon.Border.Width = 1;
            this.txt_Time.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Time.TabIndex = 215;
            this.txt_Time.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label13.Location = new System.Drawing.Point(35, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 15);
            this.label13.TabIndex = 203;
            this.label13.Text = "Số Lượng";
            // 
            // txt_Basket
            // 
            this.txt_Basket.Location = new System.Drawing.Point(97, 63);
            this.txt_Basket.Name = "txt_Basket";
            this.txt_Basket.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Basket.Size = new System.Drawing.Size(96, 26);
            this.txt_Basket.StateCommon.Border.ColorAngle = 1F;
            this.txt_Basket.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Basket.StateCommon.Border.Rounding = 4;
            this.txt_Basket.StateCommon.Border.Width = 1;
            this.txt_Basket.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Basket.TabIndex = 214;
            this.txt_Basket.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label16.Location = new System.Drawing.Point(56, 69);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 15);
            this.label16.TabIndex = 205;
            this.label16.Text = "Số Rổ";
            // 
            // txt_Kilogram
            // 
            this.txt_Kilogram.Location = new System.Drawing.Point(97, 35);
            this.txt_Kilogram.Name = "txt_Kilogram";
            this.txt_Kilogram.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Kilogram.Size = new System.Drawing.Size(96, 26);
            this.txt_Kilogram.StateCommon.Border.ColorAngle = 1F;
            this.txt_Kilogram.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Kilogram.StateCommon.Border.Rounding = 4;
            this.txt_Kilogram.StateCommon.Border.Width = 1;
            this.txt_Kilogram.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Kilogram.TabIndex = 213;
            this.txt_Kilogram.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(19, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 15);
            this.label5.TabIndex = 208;
            this.label5.Text = "Tổng TG làm";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label18.Location = new System.Drawing.Point(44, 123);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 15);
            this.label18.TabIndex = 210;
            this.label18.Text = "Đơn Giá";
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(210, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 200;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Thông tin đơn hàng";
            // 
            // Frm_Import_Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1266, 768);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.txtTitle);
            this.Font = new System.Drawing.Font("Tahoma", 9F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Import_Order";
            this.Text = "Frm_Import_Order";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).EndInit();
            this.Panel_Order.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).EndInit();
            this.Panel_Employee.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVEmployee)).EndInit();
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Order_Rate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVRate)).EndInit();
            this.Panel_Info.ResumeLayout(false);
            this.Panel_Info.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel Panel_Team;
        private System.Windows.Forms.DataGridView GVTeam;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel Panel_Order;
        private System.Windows.Forms.DataGridView GVOrder;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel Panel_Employee;
        private System.Windows.Forms.DataGridView GVEmployee;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txt_title;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnHideInfo;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnShowInfo;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.Panel Panel_Order_Rate;
        private System.Windows.Forms.DataGridView GVRate;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader9;
        private System.Windows.Forms.Panel Panel_Info;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Quantity;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Price;
        private System.Windows.Forms.Label label15;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Time;
        private System.Windows.Forms.Label label13;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Basket;
        private System.Windows.Forms.Label label16;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Kilogram;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label18;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
    }
}