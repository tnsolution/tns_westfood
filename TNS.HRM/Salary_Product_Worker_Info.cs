﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.HRM
{
    public class Salary_Product_Worker_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private DateTime _SalaryMonth;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private string _ATM = "";
        private double _SalaryBasic = 0;
        private double _SalaryOvertime = 0;
        private double _SalaryProduct = 0;
        private double _SalarySunday = 0;
        private double _SalaryHoliday = 0;
        private double _SalaryDif = 0;
        private double _SalaryDifSunday = 0;
        private double _SalaryDifHoliday = 0;
        private double _MoneyTimeKeep = 0;
        private double _SalaryTimeKeep = 0;
        private double _SalarySupport = 0;
        private float _NumberOffHoliday;
        private double _SalaryOffHoliday = 0;
        private double _SalaryPament = 0;
        private double _SupportNew = 0;
        private double _ToTal = 0;
        private double _SupportMonth = 0;
        private double _SalaryChil = 0;
        private float _NumberRice;
        private double _SalaryRice = 0;
        private int _TotalDate = 0;
        private double _AddvancePayment = 0;
        private double _Receipt = 0;
        private double _BHXH = 0;
        private double _BHYT = 0;
        private double _BHTN = 0;
        private double _CD = 0;
        private double _SalaryTexas = 0;
        private double _Texas = 0;
        private double _TLV = 0;
        private float _DayOff;
        private double _SalaryDayOff = 0;
        private double _RealLeader = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime SalaryMonth
        {
            get { return _SalaryMonth; }
            set { _SalaryMonth = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string ATM
        {
            get { return _ATM; }
            set { _ATM = value; }
        }
        public double SalaryBasic
        {
            get { return _SalaryBasic; }
            set { _SalaryBasic = value; }
        }
        public double SalaryOvertime
        {
            get { return _SalaryOvertime; }
            set { _SalaryOvertime = value; }
        }
        public double SalaryProduct
        {
            get { return _SalaryProduct; }
            set { _SalaryProduct = value; }
        }
        public double SalarySunday
        {
            get { return _SalarySunday; }
            set { _SalarySunday = value; }
        }
        public double SalaryHoliday
        {
            get { return _SalaryHoliday; }
            set { _SalaryHoliday = value; }
        }
        public double SalaryDif
        {
            get { return _SalaryDif; }
            set { _SalaryDif = value; }
        }
        public double SalaryDifSunday
        {
            get { return _SalaryDifSunday; }
            set { _SalaryDifSunday = value; }
        }
        public double SalaryDifHoliday
        {
            get { return _SalaryDifHoliday; }
            set { _SalaryDifHoliday = value; }
        }
        public double MoneyTimeKeep
        {
            get { return _MoneyTimeKeep; }
            set { _MoneyTimeKeep = value; }
        }
        public double SalaryTimeKeep
        {
            get { return _SalaryTimeKeep; }
            set { _SalaryTimeKeep = value; }
        }
        public double SalarySupport
        {
            get { return _SalarySupport; }
            set { _SalarySupport = value; }
        }
        public float NumberOffHoliday
        {
            get { return _NumberOffHoliday; }
            set { _NumberOffHoliday = value; }
        }
        public double SalaryOffHoliday
        {
            get { return _SalaryOffHoliday; }
            set { _SalaryOffHoliday = value; }
        }
        public double SalaryPament
        {
            get { return _SalaryPament; }
            set { _SalaryPament = value; }
        }
        public double SupportNew
        {
            get { return _SupportNew; }
            set { _SupportNew = value; }
        }
        public double ToTal
        {
            get { return _ToTal; }
            set { _ToTal = value; }
        }
        public double SupportMonth
        {
            get { return _SupportMonth; }
            set { _SupportMonth = value; }
        }
        public double SalaryChil
        {
            get { return _SalaryChil; }
            set { _SalaryChil = value; }
        }
        public float NumberRice
        {
            get { return _NumberRice; }
            set { _NumberRice = value; }
        }
        public double SalaryRice
        {
            get { return _SalaryRice; }
            set { _SalaryRice = value; }
        }
        public int TotalDate
        {
            get { return _TotalDate; }
            set { _TotalDate = value; }
        }
        public double AddvancePayment
        {
            get { return _AddvancePayment; }
            set { _AddvancePayment = value; }
        }
        public double Receipt
        {
            get { return _Receipt; }
            set { _Receipt = value; }
        }
        public double BHXH
        {
            get { return _BHXH; }
            set { _BHXH = value; }
        }
        public double BHYT
        {
            get { return _BHYT; }
            set { _BHYT = value; }
        }
        public double BHTN
        {
            get { return _BHTN; }
            set { _BHTN = value; }
        }
        public double CD
        {
            get { return _CD; }
            set { _CD = value; }
        }
        public double SalaryTexas
        {
            get { return _SalaryTexas; }
            set { _SalaryTexas = value; }
        }
        public double Texas
        {
            get { return _Texas; }
            set { _Texas = value; }
        }
        public double TLV
        {
            get { return _TLV; }
            set { _TLV = value; }
        }
        public float DayOff
        {
            get { return _DayOff; }
            set { _DayOff = value; }
        }
        public double SalaryDayOff
        {
            get { return _SalaryDayOff; }
            set { _SalaryDayOff = value; }
        }
        public double RealLeader
        {
            get { return _RealLeader; }
            set { _RealLeader = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Salary_Product_Worker_Info()
        {
        }
        public Salary_Product_Worker_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SLR_Finish WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["SalaryMonth"] != DBNull.Value)
                        _SalaryMonth = (DateTime)zReader["SalaryMonth"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _ATM = zReader["ATM"].ToString().Trim();
                    if (zReader["SalaryBasic"] != DBNull.Value)
                        _SalaryBasic = double.Parse(zReader["SalaryBasic"].ToString());
                    if (zReader["SalaryOvertime"] != DBNull.Value)
                        _SalaryOvertime = double.Parse(zReader["SalaryOvertime"].ToString());
                    if (zReader["SalaryProduct"] != DBNull.Value)
                        _SalaryProduct = double.Parse(zReader["SalaryProduct"].ToString());
                    if (zReader["SalarySunday"] != DBNull.Value)
                        _SalarySunday = double.Parse(zReader["SalarySunday"].ToString());
                    if (zReader["SalaryHoliday"] != DBNull.Value)
                        _SalaryHoliday = double.Parse(zReader["SalaryHoliday"].ToString());
                    if (zReader["SalaryDif"] != DBNull.Value)
                        _SalaryDif = double.Parse(zReader["SalaryDif"].ToString());
                    if (zReader["SalaryDifSunday"] != DBNull.Value)
                        _SalaryDifSunday = double.Parse(zReader["SalaryDifSunday"].ToString());
                    if (zReader["SalaryDifHoliday"] != DBNull.Value)
                        _SalaryDifHoliday = double.Parse(zReader["SalaryDifHoliday"].ToString());
                    if (zReader["MoneyTimeKeep"] != DBNull.Value)
                        _MoneyTimeKeep = double.Parse(zReader["MoneyTimeKeep"].ToString());
                    if (zReader["SalaryTimeKeep"] != DBNull.Value)
                        _SalaryTimeKeep = double.Parse(zReader["SalaryTimeKeep"].ToString());
                    if (zReader["SalarySupport"] != DBNull.Value)
                        _SalarySupport = double.Parse(zReader["SalarySupport"].ToString());
                    if (zReader["NumberOffHoliday"] != DBNull.Value)
                        _NumberOffHoliday = float.Parse(zReader["NumberOffHoliday"].ToString());
                    if (zReader["SalaryOffHoliday"] != DBNull.Value)
                        _SalaryOffHoliday = double.Parse(zReader["SalaryOffHoliday"].ToString());
                    if (zReader["SalaryPament"] != DBNull.Value)
                        _SalaryPament = double.Parse(zReader["SalaryPament"].ToString());
                    if (zReader["SupportNew"] != DBNull.Value)
                        _SupportNew = double.Parse(zReader["SupportNew"].ToString());
                    if (zReader["ToTal"] != DBNull.Value)
                        _ToTal = double.Parse(zReader["ToTal"].ToString());
                    if (zReader["SupportMonth"] != DBNull.Value)
                        _SupportMonth = double.Parse(zReader["SupportMonth"].ToString());
                    if (zReader["SalaryChil"] != DBNull.Value)
                        _SalaryChil = double.Parse(zReader["SalaryChil"].ToString());
                    if (zReader["NumberRice"] != DBNull.Value)
                        _NumberRice = float.Parse(zReader["NumberRice"].ToString());
                    if (zReader["SalaryRice"] != DBNull.Value)
                        _SalaryRice = double.Parse(zReader["SalaryRice"].ToString());
                    if (zReader["TotalDate"] != DBNull.Value)
                        _TotalDate = int.Parse(zReader["TotalDate"].ToString());
                    if (zReader["AddvancePayment"] != DBNull.Value)
                        _AddvancePayment = double.Parse(zReader["AddvancePayment"].ToString());
                    if (zReader["Receipt"] != DBNull.Value)
                        _Receipt = double.Parse(zReader["Receipt"].ToString());
                    if (zReader["BHXH"] != DBNull.Value)
                        _BHXH = double.Parse(zReader["BHXH"].ToString());
                    if (zReader["BHYT"] != DBNull.Value)
                        _BHYT = double.Parse(zReader["BHYT"].ToString());
                    if (zReader["BHTN"] != DBNull.Value)
                        _BHTN = double.Parse(zReader["BHTN"].ToString());
                    if (zReader["CD"] != DBNull.Value)
                        _CD = double.Parse(zReader["CD"].ToString());
                    if (zReader["SalaryTexas"] != DBNull.Value)
                        _SalaryTexas = double.Parse(zReader["SalaryTexas"].ToString());
                    if (zReader["Texas"] != DBNull.Value)
                        _Texas = double.Parse(zReader["Texas"].ToString());
                    if (zReader["TLV"] != DBNull.Value)
                        _TLV = double.Parse(zReader["TLV"].ToString());
                    if (zReader["DayOff"] != DBNull.Value)
                        _DayOff = float.Parse(zReader["DayOff"].ToString());
                    if (zReader["SalaryDayOff"] != DBNull.Value)
                        _SalaryDayOff = double.Parse(zReader["SalaryDayOff"].ToString());
                    if (zReader["RealLeader"] != DBNull.Value)
                        _RealLeader = double.Parse(zReader["RealLeader"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SLR_SalaryProduct_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_SalaryMonth == DateTime.MinValue)
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = _SalaryMonth;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@SalaryProduct", SqlDbType.Money).Value = _SalaryProduct;
                zCommand.Parameters.Add("@SalarySunday", SqlDbType.Money).Value = _SalarySunday;
                zCommand.Parameters.Add("@SalaryHoliday", SqlDbType.Money).Value = _SalaryHoliday;
                zCommand.Parameters.Add("@SalaryDif", SqlDbType.Money).Value = _SalaryDif;
                zCommand.Parameters.Add("@SalaryDifSunday", SqlDbType.Money).Value = _SalaryDifSunday;
                zCommand.Parameters.Add("@SalaryDifHoliday", SqlDbType.Money).Value = _SalaryDifHoliday;
                zCommand.Parameters.Add("@MoneyTimeKeep", SqlDbType.Money).Value = _MoneyTimeKeep;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            DateTime zFromDate = new DateTime(_SalaryMonth.Year, _SalaryMonth.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            string zSQL = "SLR_SalaryProduct_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_SalaryMonth == DateTime.MinValue)
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = _SalaryMonth;
                zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = zToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@SalaryProduct", SqlDbType.Money).Value = _SalaryProduct;
                zCommand.Parameters.Add("@SalarySunday", SqlDbType.Money).Value = _SalarySunday;
                zCommand.Parameters.Add("@SalaryHoliday", SqlDbType.Money).Value = _SalaryHoliday;
                zCommand.Parameters.Add("@SalaryDif", SqlDbType.Money).Value = _SalaryDif;
                zCommand.Parameters.Add("@SalaryDifSunday", SqlDbType.Money).Value = _SalaryDifSunday;
                zCommand.Parameters.Add("@SalaryDifHoliday", SqlDbType.Money).Value = _SalaryDifHoliday;
                zCommand.Parameters.Add("@MoneyTimeKeep", SqlDbType.Money).Value = _MoneyTimeKeep;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Create_TimeWorking()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SLR_SalaryTimeWorking_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_SalaryMonth == DateTime.MinValue)
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = _SalaryMonth;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@SalaryOvertime", SqlDbType.Money).Value = _SalaryOvertime;
                zCommand.Parameters.Add("@NumberRice", SqlDbType.Float).Value = _NumberRice;
                zCommand.Parameters.Add("@SalaryRice", SqlDbType.Money).Value = _SalaryRice;
                zCommand.Parameters.Add("@TotalDate", SqlDbType.Int).Value = _TotalDate;
                zCommand.Parameters.Add("@DayOff", SqlDbType.Float).Value = _DayOff;
                zCommand.Parameters.Add("@SalaryDayOff", SqlDbType.Money).Value = _SalaryDayOff;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update_TimeWorking()
        {
            DateTime zFromDate = new DateTime(_SalaryMonth.Year, _SalaryMonth.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            string zSQL = "SLR_SalaryTimeWorking_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_SalaryMonth == DateTime.MinValue)
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = _SalaryMonth;
                zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = zToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@SalaryOvertime", SqlDbType.Money).Value = _SalaryOvertime;
                zCommand.Parameters.Add("@NumberRice", SqlDbType.Float).Value = _NumberRice;
                zCommand.Parameters.Add("@SalaryRice", SqlDbType.Money).Value = _SalaryRice;
                zCommand.Parameters.Add("@TotalDate", SqlDbType.Int).Value = _TotalDate;
                zCommand.Parameters.Add("@DayOff", SqlDbType.Float).Value = _DayOff;
                zCommand.Parameters.Add("@SalaryDayOff", SqlDbType.Money).Value = _SalaryDayOff;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Create_Holiday()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SLR_SalaryHoliday_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_SalaryMonth == DateTime.MinValue)
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = _SalaryMonth;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@NumberOffHoliday", SqlDbType.Float).Value = _NumberOffHoliday;
                zCommand.Parameters.Add("@SalaryOffHoliday", SqlDbType.Money).Value = _SalaryOffHoliday;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update_Holiday()
        {
            DateTime zFromDate = new DateTime(_SalaryMonth.Year, _SalaryMonth.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            string zSQL = "SLR_SalaryHoliday_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_SalaryMonth == DateTime.MinValue)
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@SalaryMonth", SqlDbType.Date).Value = _SalaryMonth;
                zCommand.Parameters.Add("@FromDate", SqlDbType.Date).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.Date).Value = zToDate;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@NumberOffHoliday", SqlDbType.Float).Value = _NumberOffHoliday;
                zCommand.Parameters.Add("@SalaryOffHoliday", SqlDbType.Money).Value = _SalaryOffHoliday;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SLR_Finish_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
