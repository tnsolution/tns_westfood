﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Team_Employee_Month : Form
    {
        public Frm_Team_Employee_Month()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            btn_Search.Click += Btn_Search_Click; ;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            btn_New.Click += Btn_New_Click;
        }

       

        private void Frm_Team_Employee_Month_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            dte_FromDate.Value = SessionUser.Date_Work;
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Chọn----");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Chọn----");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Chọn ----");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Chọn tất cả ----");
            InitData_LV();
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            InitData_LV();
        }
        private void InitData_LV()
        {
            this.Cursor = Cursors.WaitCursor;
            int zBranch = 0;
            int zDepartment = 0;
            int zTeam = 0;
            string zEmployeeID = "";
            string zName = "";
            if (cbo_Branch.SelectedValue != null)
            {
                zBranch = cbo_Branch.SelectedValue.ToInt();
            }
            else
            {
                zBranch = 0;
            }
            if (cbo_Department.SelectedValue != null)
            {
                zDepartment = cbo_Department.SelectedValue.ToInt();
            }
            else
            {
                zDepartment = 0;
            }
            if (cbo_Team.SelectedValue != null)
            {
                zTeam = int.Parse(cbo_Team.SelectedValue.ToString());
            }
            else
            {
                zTeam = 0;
            }
            if (txt_Name.Text.Trim() != null)
            {
                zName = txt_Name.Text.Trim();
            }
            if (txt_ID.Text.Trim() != string.Empty)
            {
                zEmployeeID = txt_ID.Text.Trim();
            }

            DataTable In_Table = Team_Employee_Month.Search(zBranch, zDepartment, zTeam, zName,
                zEmployeeID, dte_FromDate.Value);
            // Thêm 1 cột để sắp xếp
            In_Table.Columns.Add("Rank");
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow row = In_Table.Rows[i];
                string ID = row[1].ToString();
                In_Table.Rows[i]["Rank"] = ConvertIDRank(ID);
            }
            DataView dv = In_Table.DefaultView;
            dv.Sort = " TeamID ASC, Rank ASC";
            In_Table = dv.ToTable();
            In_Table.Columns.Remove("Rank");
            //--sắp xếp xong xóa cột sắp xếp đi
            InitData(In_Table);
            lbl_TitleMonth.Text = "Thông tin nhân sự tháng " + dte_FromDate.Value.ToString("MM/yyyy");

            this.Cursor = Cursors.Default;

        }
        void InitData(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "Mã thẻ";
            GVData.Rows[0][2] = "Họ và tên";
            GVData.Rows[0][3] = "Tổ nhóm";
            GVData.Rows[0][4] = "Giới tính";
            GVData.Rows[0][5] = "CMND";
            GVData.Rows[0][6] = "ATM";
            GVData.Rows[0][7] = "Ngoài giờ(Gián tiếp)";

            GVData.Rows[0][8] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[0];
            }

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 120;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 120;
            GVData.Cols[4].Width = 120;
            GVData.Cols[5].Width = 120;
            GVData.Cols[6].Width = 120;
            GVData.Cols[7].Width = 120;
            GVData.Cols[8].Visible = false;

        }

        private void Btn_New_Click(object sender, EventArgs e)
        {


            DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            string zMessage = Team_Employee_Month.Refresh(zToDate);
            if (zMessage == "11")
                MessageBox.Show("Khởi tạo nhóm tháng " + dte_FromDate.Value.ToString("MM/yyyy") + " thành công!", "Thông báo");
            else
                MessageBox.Show(zMessage, "Thông báo");
            InitData_LV();
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            string zResult = "";
            string temp = ID.Replace("A", "");
            string s = "";
            //Thiếu bao nhieu số kí tự thì thêm vào bấy nhiêu cho đủ chuỗi VD: 11--->0011, A11--->1000011
            for (int i = 0; i < 4 - temp.Trim().Length; i++)
            {
                s += "0";
            }
            if (ID.Substring(0, 1) == "A")
            {
                zResult = "100" + s + temp;
            }
            else
            {
                zResult = s + temp;
            }

            return zResult;
        }
    }
}
