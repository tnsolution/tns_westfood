﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TN.Library.System
{
    public class Department_Info
    {
        #region [ Field Name ]
        private int _DepartmentKey = 0;
        private string _DepartmentName = "";
        private string _DepartmentID = "";
        private int _BranchKey = 0;
        private int _Slug = 0;
        private int _Rank = 0;
        private int _RecordStatus = 0;
        private string _Description = "";
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_DepartmentKey > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public string DepartmentName
        {
            get { return _DepartmentName; }
            set { _DepartmentName = value; }
        }
        public string DepartmentID
        {
            get { return _DepartmentID; }
            set { _DepartmentID = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int BranchKey
        {
            get
            {
                return _BranchKey;
            }

            set
            {
                _BranchKey = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Department_Info()
        {
        }
        public Department_Info(int DepartmentKey)
        {
            string zSQL = "SELECT * FROM SYS_Department WHERE DepartmentKey = @DepartmentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentName = zReader["DepartmentName"].ToString().Trim();
                    _DepartmentID = zReader["DepartmentID"].ToString().Trim();
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString()); if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public void Get_Department_ID(string DepartmentID)
        {
            string zSQL = "SELECT * FROM SYS_Department WHERE DepartmentID = @DepartmentID and RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = DepartmentID.Trim();
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    _DepartmentName = zReader["DepartmentName"].ToString().Trim();
                    _DepartmentID = zReader["DepartmentID"].ToString().Trim();
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString()); if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Department_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = _DepartmentID.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _DepartmentKey = int.Parse(_Message.Substring(2));
                else
                    _DepartmentKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "SYS_Department_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@DepartmentName", SqlDbType.NVarChar).Value = _DepartmentName.Trim();
                zCommand.Parameters.Add("@DepartmentID", SqlDbType.NVarChar).Value = _DepartmentID.Trim();
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Department_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_DepartmentKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
