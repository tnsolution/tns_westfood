﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TNS.WinApp
{
    public partial class Frm_SalaryFinish : Form
    {
        private DataTable _Table;
        public Frm_SalaryFinish()
        {
            InitializeComponent();
            GV_Employee_Layout();
        }

        private void Frm_SalaryFinish_Load(object sender, EventArgs e)
        {

        }

        #region [Design Layout DataGridView]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("WorkingDate", "Ngày Đi Làm");
            GV_Employee.Columns.Add("OverTime", "Tăng ca");
            GV_Employee.Columns.Add("DayOff", "Ngày nghỉ");
            GV_Employee.Columns.Add("SalaryDayOff", "Lương Ngày Nghỉ");
            GV_Employee.Columns.Add("SalaryOneDay", "Lương 1 Ngày");
            GV_Employee.Columns.Add("SalaryBasic", "Lương Căn bản");
            GV_Employee.Columns.Add("SalaryOverTime", "Lương tăng ca");
            GV_Employee.Columns.Add("SalaryContract", "Lương Hợp Đồng");
            GV_Employee.Columns.Add("SalaryProduct", "Lương Sản Xuất");
            GV_Employee.Columns.Add("SalarySupport", "Tổng Tiền Khác");
            GV_Employee.Columns.Add("Rewards", "Khen thưởng");
            GV_Employee.Columns.Add("Deductions", "Các khoản trừ");
            GV_Employee.Columns.Add("DeductionsFamily", "Các khoản trừ gia cảnh");
            GV_Employee.Columns.Add("Advance", "Các khoản khấu trừ");
            GV_Employee.Columns.Add("Discripline", "Kỷ luật");
            GV_Employee.Columns.Add("Different", "Các khoản khác");
            GV_Employee.Columns.Add("ToTal", "Tổng Cộng");
            GV_Employee.Columns.Add("PersonalTax", "Thuế TNCN");
            GV_Employee.Columns.Add("RealLeaders", "Thực Lãnh");

            GV_Employee.Columns["No"].Width = 60;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;

            GV_Employee.Columns["EmployeeName"].Width = 170;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Employee.Columns["EmployeeName"].ReadOnly = true;

            GV_Employee.Columns["EmployeeID"].Width = 70;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Employee.Columns["EmployeeID"].ReadOnly = true;
            GV_Employee.Columns["EmployeeID"].Frozen = true;


            GV_Employee.Columns["WorkingDate"].Width = 95;
            GV_Employee.Columns["WorkingDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["WorkingDate"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Employee.Columns["OverTime"].Width = 95;
            GV_Employee.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["OverTime"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Employee.Columns["DayOff"].Width = 95;
            GV_Employee.Columns["DayOff"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["DayOff"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Employee.Columns["SalaryDayOff"].Width = 150;
            GV_Employee.Columns["SalaryDayOff"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryOneDay"].Width = 130;
            GV_Employee.Columns["SalaryOneDay"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryBasic"].Width = 130;
            GV_Employee.Columns["SalaryBasic"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryOverTime"].Width = 130;
            GV_Employee.Columns["SalaryOverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalaryContract"].Width = 130;
            GV_Employee.Columns["SalaryContract"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["SalaryContract"].ReadOnly = true;

            GV_Employee.Columns["SalaryProduct"].Width = 130;
            GV_Employee.Columns["SalaryProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["SalarySupport"].Width = 130;
            GV_Employee.Columns["SalarySupport"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Rewards"].Width = 95;
            GV_Employee.Columns["Rewards"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Deductions"].Width = 95;
            GV_Employee.Columns["Deductions"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["DeductionsFamily"].Width = 95;
            GV_Employee.Columns["DeductionsFamily"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Advance"].Width = 95;
            GV_Employee.Columns["Advance"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Discripline"].Width = 95;
            GV_Employee.Columns["Discripline"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Different"].Width = 95;
            GV_Employee.Columns["Different"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["ToTal"].Width = 95;
            GV_Employee.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["PersonalTax"].Width = 95;
            GV_Employee.Columns["PersonalTax"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["RealLeaders"].Width = 95;
            GV_Employee.Columns["RealLeaders"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            // setup style view

            GV_Employee.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV_Employee.BackgroundColor = Color.White;
            GV_Employee.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Employee.DefaultCellStyle.Font = new Font("Arial", 9);
            GV_Employee.ColumnHeadersHeight = 40;

            GV_Employee.AllowUserToResizeRows = false;
            GV_Employee.AllowUserToResizeColumns = true;

            GV_Employee.RowHeadersVisible = false;

            //// setup Height Header
            // GV_Employees.ColumnHeadersHeight = GV_Employees.ColumnHeadersHeight * 3 / 2;
            GV_Employee.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            foreach (DataGridViewColumn column in GV_Employee.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            for (int i = 1; i <= 8; i++)
            {
                GV_Employee.Rows.Add();
            }
        }
        //private void GV_Team_Month_LoadData()
        //{
        //    GV_Employee.Rows.Clear();
        //    _Table = Order_Money_Data.List_Team_Month(_TeamKey, dte_Teams.Value);

        //    int i = 0;
        //    foreach (DataRow nRow in _Table.Rows)
        //    {
        //        GV_Employee_Month.Rows.Add();
        //        DataGridViewRow nRowView = GV_Employee_Month.Rows[i];
        //        nRowView.Cells["No"].Value = (i + 1).ToString();
        //        nRowView.Cells["EmployeeName"].Tag = nRow["EmployeeKey"].ToString().Trim();
        //        nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
        //        nRowView.Cells["CardID"].Value = nRow["EmployeeID"].ToString().Trim();
        //        float zMoney = float.Parse(nRow["Money"].ToString());
        //        nRowView.Cells["Money"].Value = zMoney.ToString("#,###,###");
        //        float zMoneyholiday = float.Parse(nRow["MoneyHoliday"].ToString());
        //        nRowView.Cells["Money_Holiday"].Value = zMoneyholiday.ToString("#,###,###");
        //        float zMoneSyunday = float.Parse(nRow["MoneySunday"].ToString().Trim());
        //        nRowView.Cells["MOney_Sunday"].Value = zMoneSyunday.ToString("#,###,###");
        //        float zMoneyDif = float.Parse(nRow["MoneyDif"].ToString());
        //        nRowView.Cells["Money_Dif"].Value = zMoneyDif.ToString("#,###,###");
        //        float zMoneyDifHoliday = float.Parse(nRow["MoneyDifSunday"].ToString());
        //        nRowView.Cells["Money_Dif_Sunday"].Value = zMoneyDifHoliday.ToString("#,###,###");
        //        float zMoneyDifSunday = float.Parse(nRow["MoneyDifHoliday"].ToString());
        //        nRowView.Cells["Money_Dif_Holiday"].Value = zMoneyDifSunday.ToString("#,###,###");
        //        i++;
        //    }

        //}
        #endregion
    }
}
