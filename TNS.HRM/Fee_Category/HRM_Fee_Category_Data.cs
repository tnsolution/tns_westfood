﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM

{
    public class HRM_Fee_Category_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT Autokey,ID,Name,Description,[Rank] FROM HRM_Fee_Category WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name,int Slug)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT Autokey,ID,Name,Description,[Rank]  FROM HRM_Fee_Category WHERE ID LIKE @Name  AND   RecordStatus <> 99 AND Slug =@Slug ORDER BY [Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
       
        public static int CountID(string Name,int Slug)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(ID) FROM HRM_Fee_Category WHERE ID = @Name and  RecordStatus <> 99 AND Slug= @Slug ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = Slug;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
