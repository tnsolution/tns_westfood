﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.SYS
{
    public class Department_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,B.BranchName AS BranchName FROM SYS_Department A 
LEFT JOIN SYS_Branch B ON B.BranchKey = A.BranchKey
WHERE A.RecordStatus != 99 ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Combobox()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus != 99 ORDER BY [Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.DepartmentKey,A.DepartmentID,DepartmentName,B.BranchName AS BranchName,A.Rank,A.Description FROM SYS_Department A
LEFT JOIN SYS_Branch B ON B.BranchKey = A.BranchKey
WHERE (A.DepartmentName LIKE @Name OR A.DepartmentID LIKE @Name) AND   A.RecordStatus <> 99 ORDER BY B.Rank ASC, A.Rank ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int CountID(string Name)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(DepartmentKey) FROM SYS_Department WHERE DepartmentID = @Name AND   RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
