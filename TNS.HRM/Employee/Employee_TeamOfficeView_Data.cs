﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Employee_TeamOfficeView_Data
    {
        public static DataTable List(int Department, int TeamKey, string Search)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey, A.EmployeeID, A.EmployeeName,B.TeamKey,B.TeamID, B.TeamName, A.Description FROM HRM_Employee_TeamOfficeView A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyView
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey              
WHERE A.RecordStatus != 99  AND D.BranchKey=2
";
            if (Department > 0)
            {
                zSQL += " AND C.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKeyView = @TeamKey";
            }
            if (Search.Length > 0)
            {
                zSQL += " AND (A.EmployeeName LIKE @Name OR A.EmployeeID LIKE @Name )";
            }

            zSQL += " ORDER BY  D.RANK,C.RANK,B.RANK,LEN(A.EmployeeID), A.EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List02(int Department, int TeamKey, string Search, int Type)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey, A.EmployeeID, A.EmployeeName,C.DepartmentID,B.TeamID, B.TeamName,A.FromDate,A.ToDate, A.Description FROM HRM_Employee_TeamOfficeView A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeyView
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey              
WHERE A.RecordStatus != 99  AND D.BranchKey=2
";
            if (Type == 1)
                zSQL += " AND A.ToDate is null ";
            if (Type == 2)
                zSQL += " AND A.ToDate is not null ";
            if (Department > 0)
            {
                zSQL += " AND C.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKeyView = @TeamKey";
            }
            if (Search.Length > 0)
            {
                zSQL += " AND (A.EmployeeName LIKE @Name OR A.EmployeeID LIKE @Name )";
            }

            zSQL += " ORDER BY  D.RANK,C.RANK,B.RANK,LEN(A.EmployeeID), A.EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
