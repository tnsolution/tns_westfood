﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.HRM
{
    public class Employee_Info
    {
        #region [ Field Name ]
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _ReportTo = "";
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _FullName = "";
        private int _PositionKey = 0;
        private string _Description = "";
        private int _Slug = 0;
        private string _CompanyPhone = "";
        private string _CompanyEmail = "";
        private string _Organization = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private float _SalaryBasic = 0;
        private string _AccountCode = "";
        private string _AccountCode2 = "";
        private int _OverTime = 0;
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private int _WorkingStatus = 0;
        private DateTime _StartingDate;
        private DateTime _LeavingDate;
        private int _ScoreStock;
        private DateTime _LeavingTryDate;

        #endregion

        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_EmployeeKey.Trim().Length > 0) return true; else return false;
            }
        }

        public string Key
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string ReportTo
        {
            get { return _ReportTo; }
            set { _ReportTo = value; }
        }
        public int BranchKey
        {
            get { return _BranchKey; }
            set { _BranchKey = value; }
        }
        public int DepartmentKey
        {
            get { return _DepartmentKey; }
            set { _DepartmentKey = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public int PositionKey
        {
            get { return _PositionKey; }
            set { _PositionKey = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string CompanyPhone
        {
            get { return _CompanyPhone; }
            set { _CompanyPhone = value; }
        }
        public string CompanyEmail
        {
            get { return _CompanyEmail; }
            set { _CompanyEmail = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }

        public string FullName
        {
            get
            {
                return _FullName;
            }

            set
            {
                _FullName = value;
            }
        }

        public float SalaryBasic
        {
            get
            {
                return _SalaryBasic;
            }

            set
            {
                _SalaryBasic = value;
            }
        }

        public string AccountCode
        {
            get
            {
                return _AccountCode;
            }

            set
            {
                _AccountCode = value;
            }
        }

        public int OverTime
        {
            get
            {
                return _OverTime;
            }

            set
            {
                _OverTime = value;
            }
        }

        public int WorkingStatus
        {
            get
            {
                return _WorkingStatus;
            }

            set
            {
                _WorkingStatus = value;
            }
        }

        public DateTime StartingDate
        {
            get
            {
                return _StartingDate;
            }

            set
            {
                _StartingDate = value;
            }
        }

        public DateTime LeavingDate
        {
            get
            {
                return _LeavingDate;
            }

            set
            {
                _LeavingDate = value;
            }
        }

        public string AccountCode2
        {
            get
            {
                return _AccountCode2;
            }

            set
            {
                _AccountCode2 = value;
            }
        }

        public int ScoreStock
        {
            get
            {
                return _ScoreStock;
            }

            set
            {
                _ScoreStock = value;
            }
        }

        public DateTime LeavingTryDate
        {
            get
            {
                return _LeavingTryDate;
            }

            set
            {
                _LeavingTryDate = value;
            }
        }



        #endregion

        #region [ Constructor Get Information ]
        public Employee_Info()
        {
        }
        public Employee_Info(string EmployeeKey)
        {
            string zSQL = @"SELECT *, [dbo].[Fn_GetFullName](EmployeeKey) AS FullName FROM HRM_Employee 
                             WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _FullName = zReader["FullName"].ToString().Trim();
                    _ReportTo = zReader["ReportTo"].ToString();
                    if (zReader["BranchKey"] != DBNull.Value)
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    if (zReader["DepartmentKey"] != DBNull.Value)
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["PositionKey"] != DBNull.Value)
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _CompanyPhone = zReader["CompanyPhone"].ToString().Trim();
                    _CompanyEmail = zReader["CompanyEmail"].ToString().Trim();
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();

                    if (zReader["SalaryBasic"] != DBNull.Value)
                        _SalaryBasic = float.Parse(zReader["SalaryBasic"].ToString().Trim());
                    _AccountCode = zReader["AccountCode"].ToString().Trim();
                    _AccountCode2 = zReader["AccountCode2"].ToString().Trim();
                    if (zReader["OverTime"] != DBNull.Value)
                        _OverTime = int.Parse(zReader["OverTime"].ToString());
                    if (zReader["ScoreStock"] != DBNull.Value)
                        _ScoreStock = int.Parse(zReader["ScoreStock"].ToString());
                    if (zReader["WorkingStatus"] != DBNull.Value)
                        _WorkingStatus = int.Parse(zReader["WorkingStatus"].ToString());
                    if (zReader["StartingDate"] != DBNull.Value)
                        _StartingDate = (DateTime)zReader["StartingDate"];
                    if (zReader["LeavingDate"] != DBNull.Value)
                        _LeavingDate= (DateTime)zReader["LeavingDate"];
                    if (zReader["LeavingTryDate"] != DBNull.Value)
                        _LeavingTryDate = (DateTime)zReader["LeavingTryDate"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public void GetEmployee(string CardID)
        {
            string zSQL = @"
SELECT * FROM dbo.SYS_Personal A
LEFT JOIN dbo.HRM_Employee B ON B.EmployeeKey =A.ParentKey WHERE A.CardID =@CardID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = CardID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    _FullName = zReader["FullName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    if (zReader["DepartmentKey"] != DBNull.Value)
                    {
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    }

                    if (zReader["TeamKey"] != DBNull.Value)
                    {
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    }

                    if (zReader["BranchKey"] != DBNull.Value)
                    {
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    _Description = zReader["Description"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();

                    if (zReader["SalaryBasic"] != DBNull.Value)
                        _SalaryBasic = float.Parse(zReader["SalaryBasic"].ToString().Trim());
                    _AccountCode = zReader["AccountCode"].ToString().Trim();
                    _AccountCode2 = zReader["AccountCode2"].ToString().Trim();
                    if (zReader["OverTime"] != DBNull.Value)
                        _OverTime = int.Parse(zReader["OverTime"].ToString());
                    if (zReader["ScoreStock"] != DBNull.Value)
                        _ScoreStock = int.Parse(zReader["ScoreStock"].ToString());
                    if (zReader["WorkingStatus"] != DBNull.Value)
                        _WorkingStatus = int.Parse(zReader["WorkingStatus"].ToString());
                    if (zReader["StartingDate"] != DBNull.Value)
                        _StartingDate = (DateTime)zReader["StartingDate"];
                    if (zReader["LeavingDate"] != DBNull.Value)
                        _LeavingDate = (DateTime)zReader["LeavingDate"];
                    if (zReader["LeavingTryDate"] != DBNull.Value)
                        _LeavingTryDate = (DateTime)zReader["LeavingTryDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public void GetEmployeeID(string EmployeeID)
        {
            string zSQL = @"
SELECT *, [dbo].[Fn_GetFullName](EmployeeKey) AS FullName FROM HRM_Employee 
                             WHERE EmployeeID = @EmployeeID and RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    _FullName = zReader["FullName"].ToString();
                    if (zReader["PositionKey"] != DBNull.Value)
                    {
                        _PositionKey = int.Parse(zReader["PositionKey"].ToString());
                    }

                    if (zReader["DepartmentKey"] != DBNull.Value)
                    {
                        _DepartmentKey = int.Parse(zReader["DepartmentKey"].ToString());
                    }

                    if (zReader["TeamKey"] != DBNull.Value)
                    {
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    }

                    if (zReader["BranchKey"] != DBNull.Value)
                    {
                        _BranchKey = int.Parse(zReader["BranchKey"].ToString());
                    }

                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    _Description = zReader["Description"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();

                    if (zReader["SalaryBasic"] != DBNull.Value)
                        _SalaryBasic = float.Parse(zReader["SalaryBasic"].ToString().Trim());
                    _AccountCode = zReader["AccountCode"].ToString().Trim();
                    _AccountCode2 = zReader["AccountCode2"].ToString().Trim();
                    if (zReader["OverTime"] != DBNull.Value)
                        _OverTime = int.Parse(zReader["OverTime"].ToString());
                    if (zReader["ScoreStock"] != DBNull.Value)
                        _ScoreStock = int.Parse(zReader["ScoreStock"].ToString());
                    if (zReader["WorkingStatus"] != DBNull.Value)
                        _WorkingStatus = int.Parse(zReader["WorkingStatus"].ToString());
                    if (zReader["StartingDate"] != DBNull.Value)
                        _StartingDate = (DateTime)zReader["StartingDate"];
                    if (zReader["LeavingDate"] != DBNull.Value)
                        _LeavingDate = (DateTime)zReader["LeavingDate"];
                    if (zReader["LeavingTryDate"] != DBNull.Value)
                        _LeavingTryDate = (DateTime)zReader["LeavingTryDate"];
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            //string _Name = LastName + " " + FirstName;
            string zSQL = "HRM_Employee_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@ReportTo", SqlDbType.NVarChar).Value = _ReportTo;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = _CompanyPhone.Trim();
                zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = _CompanyEmail.Trim();
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;

                zCommand.Parameters.Add("@SalaryBasic", SqlDbType.Money).Value = _SalaryBasic;
                zCommand.Parameters.Add("@AccountCode", SqlDbType.NVarChar).Value = _AccountCode.Trim();
                zCommand.Parameters.Add("@AccountCode2", SqlDbType.NVarChar).Value = _AccountCode2.Trim();
                zCommand.Parameters.Add("@OverTime", SqlDbType.Int).Value = _OverTime;
                zCommand.Parameters.Add("@ScoreStock", SqlDbType.Int).Value = _ScoreStock;

                zCommand.Parameters.Add("@WorkingStatus", SqlDbType.Int).Value = _WorkingStatus;
                if(_StartingDate==DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = _StartingDate;
                }
                if (_LeavingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = _LeavingDate;
                }
                if (_LeavingTryDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LeavingTryDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LeavingTryDate", SqlDbType.DateTime).Value = _LeavingTryDate;
                }

                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _EmployeeKey = _Message.Substring(2, 36);
                else
                    _EmployeeKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "HRM_Employee_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@ReportTo", SqlDbType.NVarChar).Value = _ReportTo;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = _BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = _DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = _PositionKey;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@CompanyPhone", SqlDbType.NVarChar).Value = _CompanyPhone.Trim();
                zCommand.Parameters.Add("@CompanyEmail", SqlDbType.NVarChar).Value = _CompanyEmail.Trim();
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;

                zCommand.Parameters.Add("@SalaryBasic", SqlDbType.Money).Value = _SalaryBasic;
                zCommand.Parameters.Add("@AccountCode", SqlDbType.NVarChar).Value = _AccountCode.Trim();
                zCommand.Parameters.Add("@AccountCode2", SqlDbType.NVarChar).Value = _AccountCode2.Trim();
                zCommand.Parameters.Add("@OverTime", SqlDbType.Int).Value = _OverTime;
                zCommand.Parameters.Add("@ScoreStock", SqlDbType.Int).Value = _ScoreStock;

                if (_StartingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@StartingDate", SqlDbType.DateTime).Value = _StartingDate;
                }
                if (_LeavingDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = _LeavingDate;
                }
                zCommand.Parameters.Add("@WorkingStatus", SqlDbType.Int).Value = _WorkingStatus;
                if (_LeavingTryDate == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@LeavingTryDate", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@LeavingTryDate", SqlDbType.DateTime).Value = _LeavingTryDate;
                }
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "HRM_Employee_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult = "";
            if (_EmployeeKey.Length == 0)
            {
                zResult = Create();
            }
            else if (_EmployeeKey.Length > 0)
            {
                zResult = Update();
            }
            return zResult;
        }

        public string UpdateTeam()
        {
            string zSQL = "UPDATE HRM_Employee SET TeamKey = @TeamKey WHERE EmployeeKey= @EmployeeKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
