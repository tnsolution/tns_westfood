﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{

    public partial class Frm_Rpt42_V2 : Form
    {
        EmployeeModel Item = new EmployeeModel();

        public Frm_Rpt42_V2()
        {
            InitializeComponent();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            txt_ID.KeyUp += Txt_ID_KeyUp;
            txt_ID.KeyDown += Txt_ID_KeyDown;
            txt_ID.Leave += Txt_ID_Leave;
            btn_Export.Click += Btn_Export_Click;
            btn_ClearList.Click += btn_ClearList_Click;

            Utils.DrawLVStyle(ref LVData);
            Utils.DrawLVStyle(ref LVEmployee);
            InitLayout_LV(LVData);
            InitLayout_LV(LVEmployee);
            Utils.SizeLastColumn_LV(LVEmployee);
            Utils.SizeLastColumn_LV(LVData);

            LVData.ItemSelectionChanged += LVData_ItemSelectionChanged;
            LVData.ItemActivate += LVData_ItemActivate;
            LVEmployee.Click += (o, e) => { LVData.Visible = false; };
            btn_Hide.Click += Btn_Hide_Click;
            btn_Show.Click += Btn_Show_Click;
        }



        private void Frm_Rpt42_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LoadDataToToolbox.KryptonComboBox(cbo_Team, @"
SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
ORDER BY Rank", "--Tất cả--");
            splitContainer1.SplitterDistance = 300;
        }
        private void Btn_Show_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 300;
            btn_Hide.Visible = true;
            btn_Show.Visible = false;
        }
        private void Btn_Hide_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 120;
            btn_Hide.Visible = false;
            btn_Show.Visible = true;
        }
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số thẻ";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nhân Viên";
            colHead.Width = 160;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData_LVSearch()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            int Team = 0;
            string EmployeeID = "";
            DataTable In_Table = new DataTable();

            if (cbo_Team.SelectedValue != null)
            {
                Team = int.Parse(cbo_Team.SelectedValue.ToString());
            }
            else
            {
                Team = 0;
            }

            if (txt_ID.Text.Trim() != string.Empty)
            {
                EmployeeID = txt_ID.Text.Trim();
            }

            In_Table = Employee_Data.Search(0, 0, Team, string.Empty, EmployeeID, string.Empty, 0, 0);
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["EmployeeKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CardID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FullName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);



                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        private void AddEmployeeToView(ListView LV, EmployeeModel Item)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            lvi = new ListViewItem();
            lvi.Text = "";
            lvi.Tag = Item.EmployeeKey;
            lvi.ForeColor = Color.Navy;
            lvi.BackColor = Color.White;
            lvi.ImageIndex = 0;

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = Item.CardID;
            lvi.SubItems.Add(lvsi);

            lvsi = new ListViewItem.ListViewSubItem();
            lvsi.Text = Item.FullName;
            lvi.SubItems.Add(lvsi);
            lvi.Checked = true;
            LV.Items.Add(lvi);
            this.Cursor = Cursors.Default;
        }

        private void LVData_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            txt_ID.Tag = LVData.Items[e.ItemIndex].Tag.ToString();
            txt_ID.Text = LVData.Items[e.ItemIndex].SubItems[1].Text.Trim();
            if (LVData.SelectedItems.Count > 0)
            {
                Item = new EmployeeModel();
                Item.EmployeeKey = LVData.SelectedItems[0].Tag.ToString();
                Item.FullName = LVData.SelectedItems[0].SubItems[2].Text;
                Item.CardID = LVData.SelectedItems[0].SubItems[1].Text;
            }
        }
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            txt_ID.Tag = LVData.SelectedItems[0].Tag;
            LVData.Visible = false;
            txt_ID.Focus();

            Item = new EmployeeModel();
            Item.EmployeeKey = LVData.SelectedItems[0].Tag.ToString();
            Item.FullName = LVData.SelectedItems[0].SubItems[2].Text;
            Item.CardID = LVData.SelectedItems[0].SubItems[1].Text;

            if (Item.CardID != string.Empty)
            {
                int Check = 0;
                for (int i = 0; i < LVEmployee.Items.Count; i++)
                {
                    if (Item.CardID == LVEmployee.Items[0].SubItems[1].Text)
                    {
                        Check++;
                    }
                }
                if (Check == 0)
                {
                    AddEmployeeToView(LVEmployee, Item);

                    Item = new EmployeeModel();
                    txt_ID.Clear();
                }
                else
                {
                    Utils.TNMessageBoxOK("Mã nhân viên đã có trong danh sách!",1);
                }
            }
        }

        private void Txt_ID_Leave(object sender, EventArgs e)
        {
            if (!LVData.Focused)
            {
                LVData.Visible = false;
            }
        }
        private void Txt_ID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_ID.Tag = LVData.SelectedItems[0].Tag;
                LVData.Visible = false;
            }
        }
        private void Txt_ID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LVData.Focus();
                if (LVData.Items.Count > 0)
                {
                    LVData.Items[0].Selected = true;
                }
            }
            else
            {
                if (e.KeyCode != Keys.Enter)
                {
                    if (txt_ID.Text.Length > 0)
                    {
                        LVData.Visible = true;
                        InitData_LVSearch();
                    }
                    else
                    {
                        LVData.Visible = false;
                    }
                }
            }
        }

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            if (Item.CardID != string.Empty)
            {
                AddEmployeeToView(LVEmployee, Item);

                Item = new EmployeeModel();
                txt_ID.Clear();
            }
        }

        private void DisplayData()
        {
            DataTable zTable = Report.Report_42_V4(dte_FromDate.Value, dte_ToDate.Value, _InEmployeeID);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);

                DataTable zTablePivot = zPvt.Generate("WORK", "ORDER",
                     new string[] { "LK", "TGBua", "SR", "TP", "NS" }, "NGÀY CÔNG", "TỔNG", "TỔNG",
                     new string[] { "LƯƠNG KHOÁN", "THỜI GIAN", "SỐ RỔ", "THÀNH PHẨM", "NĂNG SUẤT (KG/GIỜ)" }, 0, false);

                //ĐIỀU CHỈNH BẢNG ĐÃ PIVOT
                DataTable zTableView = new DataTable();
                zTableView.Columns.Add("OrderDate", typeof(DateTime));
                zTableView.Columns.Add("EmployeeName", typeof(string));
                zTableView.Columns.Add("EmployeeID", typeof(string));

                for (int i = 1; i < zTablePivot.Columns.Count; i++)
                {
                    string ColumnName = zTablePivot.Columns[i].ColumnName;
                    zTableView.Columns.Add(ColumnName);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    DataRow rPivot = zTablePivot.Rows[i];
                    DataRow zRowView = zTableView.NewRow();

                    string rColumn0 = rPivot[0].ToString();
                    if (rColumn0.Contains("|"))
                    {
                        string[] temp = rColumn0.Split('|');

                        zRowView["OrderDate"] = temp[0];// Convert.ToDateTime(temp[0]).ToString("dd/MM/yyyy");
                        zRowView["EmployeeName"] = temp[1];
                        zRowView["EmployeeID"] = temp[2];
                    }
                    else
                    {
                        zRowView["EmployeeName"] = rColumn0;
                    }

                    for (int j = 1; j < zTablePivot.Columns.Count; j++)
                    {
                        zRowView[j + 2] = rPivot[j].Toe2String();
                    }

                    zTableView.Rows.Add(zRowView);
                }

                DataView dv = zTableView.DefaultView;
                dv.Sort = "EmployeeID, OrderDate";
                DataTable zSortTable = dv.ToTable();

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitLayoutGV(zSortTable);
                    FillDataGV(zSortTable);
                }));
            }
        }

        void InitLayoutGV(DataTable zTable)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();

            GVEmployee.Cols.Add(zTable.Columns.Count);
            GVEmployee.Rows.Add(4);

            CellRange zRange = GVEmployee.GetCellRange(0, 0, 3, 0);
            zRange.Data = "NGÀY";

            zRange = GVEmployee.GetCellRange(0, 1, 3, 1);
            zRange.Data = "HỌ TÊN";

            zRange = GVEmployee.GetCellRange(0, 2, 3, 2);
            zRange.Data = "MÃ THẺ";

            for (int i = 3; i < zTable.Columns.Count; i++)
            {
                DataColumn Col = zTable.Columns[i];

                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length >= 4)
                {
                    GVEmployee.Rows[0][i] = strCaption[0];  //Sản phẩm
                    GVEmployee.Rows[1][i] = strCaption[1];  //Công việc
                    GVEmployee.Rows[2][i] = strCaption[2].Toe0String();  //Đơn giá
                    GVEmployee.Rows[3][i] = strCaption[3];  //"LK", "TG", "SLTP", "NS"
                }
                else
                {
                    GVEmployee.Rows[0][i] = strCaption[0];  //Tổng
                    GVEmployee.Rows[1][i] = strCaption[1];   //"LK", "TG", "SLTP", "NS"
                }
                GVEmployee.Cols[i].Width = 110;
            }

            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;

            GVEmployee.Rows[0].AllowMerging = true;
            GVEmployee.Rows[1].AllowMerging = true;
            GVEmployee.Rows[2].AllowMerging = true;
            GVEmployee.Rows[3].AllowMerging = true;

            GVEmployee.Cols[0].Width = 75;
            GVEmployee.Cols[0].AllowMerging = true;

            GVEmployee.Cols[1].AllowMerging = true;
            GVEmployee.Cols[1].Width = 150;

            GVEmployee.Cols[2].AllowMerging = true;
            GVEmployee.Cols[2].Width = 50;

            GVEmployee.Rows.Fixed = 4;
            GVEmployee.Cols.Fixed = 3;

            GVEmployee.Rows[0].StyleNew.WordWrap = true;
            GVEmployee.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[1].StyleNew.WordWrap = true;
            GVEmployee.Rows[1].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[2].StyleNew.WordWrap = true;
            GVEmployee.Rows[2].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[3].StyleNew.WordWrap = true;
            GVEmployee.Rows[3].TextAlign = TextAlignEnum.CenterCenter;
        }
        void FillDataGV(DataTable zTable)
        {
            int Row = 4;
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                GVEmployee.Rows.Add();
                for (int j = 0; j < zTable.Columns.Count; j++)
                {
                    GVEmployee.Rows[Row][j] = zTable.Rows[i][j].ToString();
                }

                Row++;
            }
        }

        string _InEmployeeID = "";
        private void btn_Search_Click(object sender, EventArgs e)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();
            LVData.Visible = false;
            _InEmployeeID = "";
            if (LVEmployee.Items.Count > 0)
            {
                for (int i = 0; i < LVEmployee.Items.Count; i++)
                {
                    if (LVEmployee.Items[i].Checked)
                    {
                        _InEmployeeID += "'" + LVEmployee.Items[i].SubItems[1].Text + "',";
                    }
                }
                _InEmployeeID = _InEmployeeID.Remove(_InEmployeeID.Length - 1, 1);

                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy nhân viên!",1);
            }
        }
        private void btn_ClearList_Click(object sender, EventArgs e)
        {
            LVEmployee.Items.Clear();
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "ChiTietNangSuat_LuongKhoanTheoCongNhan.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVEmployee.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
               // this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }

    public class EmployeeModel
    {
        public string EmployeeKey { get; set; } = "";
        public string FullName { get; set; } = "";
        public string CardID { get; set; } = "";
    }
}