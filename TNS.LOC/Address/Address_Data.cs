﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.LOC
{
    public class Address_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *, WardKey AS [Key] FROM LOC_Address WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Address(string Parent)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,Address,WardName,DistrictName,ProvinceName,CountryName,CategoryKey 
FROM LOC_Address A
LEFT JOIN[dbo].[LOC_District] B ON A.DistrictKey = B.DistrictKey
LEFT JOIN[dbo].[LOC_Province] C ON A.ProvinceKey = C.ProvinceKey
LEFT JOIN[dbo].[LOC_Ward] D ON A.WardKey = D.WardKey
LEFT JOIN[dbo].[LOC_Country] E ON A.CountryKey = E.CountryKey
WHERE A.ParentKey = @Parent AND A.RecordStatus< 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

       
        public static string Delete_Address(string Parent)
        {
            string _Message = "";
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM LOC_Address WHERE ParentKey = @Parent ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
