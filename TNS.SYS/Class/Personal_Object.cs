﻿using System.Collections.Generic;
using System.Data;
using TNS.LOC;


namespace TNS.SYS
{
    public class Personal_Object : Personal_Info
    {
        private Address_Info _Address;
        public Address_Info Address
        {
            get
            {
                return _Address;
            }

            set
            {
                _Address = value;
            }
        }

        private List<Address_Info> _List_Address;
        public List<Address_Info> List_Address
        {
            get
            {
                return _List_Address;
            }

            set
            {
                _List_Address = value;
            }
        }

     
        public Personal_Object() : base()
        {

        }

        public Personal_Object(string ParentKey) : base(ParentKey)
        {
            DataTable zTable = Address_Data.List_Address(base.Key);
            List_Address = new List<Address_Info>();
            foreach (DataRow nRow in zTable.Rows)
            {
                Address_Info zAddress = new Address_Info(nRow);
                List_Address.Add(zAddress);
            }
        }

        public void Save_Object()
        {
            string zResult = "";
            base.Save();
            if(List_Address != null)
            {
                for (int i = 0; i < List_Address.Count; i++)
                {
                    Address = (Address_Info)List_Address[i];
                    Address.ParentKey = base.Key;
                    Address.ParentTable = "SYS_Person";
                    Address.ModifiedBy = base.ModifiedBy;
                    Address.ModifiedName = base.ModifiedName;
                    switch (Address.RecordStatus)
                    {
                        case 99:
                            Address.DeleteDetail();
                            break;
                        case 1:
                            Address.Create();
                            break;
                        case 2:
                            Address.Update();
                            break;
                    }
                }
                base.Message += zResult;
            }
           
        }
        public void Delete_Object()
        {
            string zResult = "";
            base.Delete();
            Address = new Address_Info();
            Address.Delete(base.Key);
            if (Address.Message.Length > 0)
            {
                zResult += Address.Message;
            }
        }
    }
}
