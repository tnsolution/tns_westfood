﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_5 : Form
    {
        const int _HeaderHeight = 80;
        int _TeamKey = 0;
        int _TotalCol = 0;

        string[] TitleMerge;
        string[] TitleColumn;
        public Frm_Report_5()
        {
            InitializeComponent();
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += btn_Search_Click;
            btn_Export.Click += btn_Export_Click;
        }

        private void Frm_Report_5_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVEmployee, true);

            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"
SELECT A.TeamKey, A.TeamID +'-'+ A.TeamName AS TeamName  
FROM SYS_Team A
LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
WHERE A.RecordStatus <> 99 
AND   A.BranchKey = 4 
AND   A.DepartmentKey != 26
AND   A.TeamKey !=98 
ORDER BY C.RANK,B.RANK,A.RANK";
            LoadDataToToolbox.KryptonComboBox(cbo_Team, zSQL, "");


            GVEmployee.ColumnHeadersHeight = _HeaderHeight;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void InitGV_Paint(DataGridView GV, DataTable Table)
        {
            /*
             * Dùng tạo column header name cho 2 dòng column trộn và không trộn
             */

            int nRow0 = 0;
            int nRow1 = 3;

            _TotalCol = GV.ColumnCount - 1;

            TitleMerge = new string[_TotalCol - 3];         //column merge
            TitleColumn = new string[GV.ColumnCount];   //column bình thường

            TitleColumn[0] = "";
            TitleColumn[1] = "Tên";
            TitleColumn[2] = "Mã";
            TitleColumn[_TotalCol] = "Tổng";

            string ColumnName = "";
            for (int i = 2; i < Table.Columns.Count; i++)
            {
                string ColName = Table.Columns[i].ColumnName;
                ColName = ColName.Substring(0, ColName.Length - 3);
                if (ColumnName != ColName)
                {
                    ColumnName = ColName;
                    TitleMerge[nRow0] = ColumnName;
                    nRow0++;

                    TitleColumn[nRow1] = "Số lượng";
                    TitleColumn[nRow1 + 1] = "Lương khoán";
                    nRow1 += 2;
                }
            }

            GV.Scroll += GridView_Scroll;
            GV.Paint += GridView_Paint;
            GV.Click += GridView_Click;

        }
        private void InitGV_Layout(DataGridView GV, DataTable zTable)
        {
            GV.Columns.Clear();
            GV.Rows.Clear();

            if (zTable.Rows.Count > 0)
            {
                GV.Columns.Add("No", "#");
                GV.Columns.Add("ProductName", "");
                GV.Columns.Add("ProductID", "");
                for (int j = 2; j < zTable.Columns.Count; j++)
                {
                    string Name = zTable.Columns[j].ColumnName;
                    GV.Columns.Add(Name, "");
                    GV.Columns[Name].Width = 100;
                    GV.Columns[Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GV.Columns[Name].ReadOnly = true;
                }
                GV.Columns.Add("ToTal", "");

                GV.Columns["No"].Width = 40;
                GV.Columns["No"].Frozen = true;
                GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                GV.Columns["No"].ReadOnly = true;

                GV.Columns["ProductName"].Width = 180;
                GV.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                GV.Columns["ProductName"].ReadOnly = true;

                GV.Columns["ProductID"].Width = 80;
                GV.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                GV.Columns["ProductID"].ReadOnly = true;
                GV.Columns["ProductID"].Frozen = true;

                GV.Columns["ToTal"].Width = 120;
                GV.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                GV.Columns["ToTal"].DefaultCellStyle.ForeColor = Color.Navy;
                GV.Columns["ToTal"].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
                GV.Columns["ToTal"].ReadOnly = true;
            }
        }
        private void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            GV.Rows.Clear();
            DataGridViewRow nRowView;
            int rows = 0;
            if (zTable.Rows.Count > 0)
            {
                double[] Total = new double[zTable.Columns.Count];
                double _TOTAL = 0;

                foreach (DataRow nRow in zTable.Rows)
                {
                    double zTotalRow = 0;
                    GV.Rows.Add();
                    nRowView = GV.Rows[rows];
                    nRowView.Cells["No"].Value = (rows + 1).ToString();
                    nRowView.Cells["ProductName"].Value = nRow["ProductName"].ToString().Trim();
                    nRowView.Cells["ProductID"].Value = nRow["ProductID"].ToString().Trim();

                    for (int j = 2; j < zTable.Columns.Count; j++)
                    {
                        string Name = zTable.Columns[j].ColumnName;
                        double zValueCell = 0;
                        if (nRow[Name].ToString().Trim().Length == 0)
                        {
                            nRowView.Cells[Name].Value = "";
                        }
                        else
                        {
                            double zTemp = 0;
                            if (double.TryParse(nRow[Name].ToString(), out zTemp))
                            {

                            }
                            zValueCell = Math.Round(zTemp, 0);
                            nRowView.Cells[Name].Value = zValueCell.Toe0String();
                            if (j % 2 != 0)
                            {
                                zTotalRow += zValueCell;
                            }
                        }
                        Total[j] += zValueCell;
                    }
                    nRowView.Cells["ToTal"].Value = zTotalRow.Toe0String();
                    _TOTAL += zTotalRow;
                    rows++;
                }

                GV.Rows.Add();
                nRowView = GV.Rows[rows];
                nRowView.Cells["ProductName"].Value = "TỔNG LƯƠNG KHOÁN";
                nRowView.DefaultCellStyle.ForeColor = Color.Navy;
                nRowView.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

                int col = 3;
                for (int j = 2; j < Total.Length; j++)
                {
                    nRowView.Cells[col].Value = Total[j].Toe0String();
                    nRowView.Cells["ToTal"].Value = _TOTAL.Toe0String();
                    col++;
                }
                GV.Visible = true;
            }
            else
            {
                GV.Visible = false;
            }
        }



        

       
        private void DisplayData()
        {
            DataTable zTable = Report.No5(dte_FromDate.Value, dte_ToDate.Value, _TeamKey);
            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(GVEmployee, zTable);
                    InitGV_Data(GVEmployee, zTable);
                    InitGV_Paint(GVEmployee, zTable);
                }));
            }
           

        }

        private void GridView_Click(object sender, EventArgs e)
        {
            GVEmployee.Refresh();
        }
        private void GridView_Paint(object sender, PaintEventArgs e)
        {
            Image image = imageList1.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);
            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Tahoma", 9, FontStyle.Bold);
            Pen nLinePen = new Pen(nColor, 1);
            drawing = new Bitmap(GVEmployee.Width, GVEmployee.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;

            for (int i = 0; i <= _TotalCol; i++)
            {
                Rectangle r1 = GVEmployee.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;

                if (i > 2 && i < _TotalCol)
                {
                    // Chia
                    r1.Height = _HeaderHeight / 3 - 2;
                    r1.Width = r1.Width - 2;
                    r1.Y = _HeaderHeight - r1.Height;

                    g.DrawImage(image, r1);
                }
                else
                {
                    // Trộn
                    r1.Width = r1.Width - 2;
                    r1.Height = r1.Height - 2;

                    g.DrawImage(image, r1);
                }
                //e.Graphics.FillRectangle(new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.BackColor), r1);
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);
                //e.Graphics.DrawString(TitleColumn[i],
                //  new Font("Tahoma", 9, FontStyle.Bold),
                //   new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.ForeColor),
                //   r1,
                //   format);
            }

            int k = 0;
            for (int j = 3; j < _TotalCol; j += 2)
            {
                Rectangle r1 = GVEmployee.GetCellDisplayRectangle(j, -1, true);
                int w2 = GVEmployee.GetCellDisplayRectangle(j + 1, -1, true).Width;
                r1.X += 1;
                r1.Y += 1;
                r1.Width = r1.Width + w2 - 2;
                r1.Height = r1.Height - _HeaderHeight / 3 - 2;
                g.DrawImage(image, r1);

                //e.Graphics.FillRectangle(new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.BackColor), r1);
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                g.DrawString(TitleMerge[k], nFont, nBrush, r1, format);

                //e.Graphics.DrawString(TitleMerge[k],
                //   new Font("Tahoma", 9, FontStyle.Bold),
                //    new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.ForeColor),
                //    r1,
                //    format);

                k++;
            }

            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }
        private void GridView_Scroll(object sender, ScrollEventArgs e)
        {
            Rectangle rtHeader = GVEmployee.DisplayRectangle;
            rtHeader.Height = _HeaderHeight;
            GVEmployee.Invalidate(rtHeader);
            if (e.Type == ScrollEventType.EndScroll)
            {
                GVEmployee.Refresh();
            }
        }
        private void GridView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            Rectangle rtHeader = GVEmployee.DisplayRectangle;
            rtHeader.Height = GVEmployee.ColumnHeadersHeight / 2;
            GVEmployee.Invalidate(rtHeader);
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK( "Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            _TeamKey = cbo_Team.SelectedValue.ToInt();

            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cbo_Team.Text;
             Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cbo_Team.SelectedValue.ToInt() != 0)
            {
                string[] s = cbo_Team.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Bảng_Thông_Tin_Sản_Phẩm_Theo_Công_Đoạn_Số_Lượng_Và_Lương_Khoán" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    if (GVEmployee.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        foreach (DataGridViewColumn col in GVEmployee.Columns)
                        {
                            dt.Columns.Add(col.HeaderText);

                        }
                        DataRow dRow = dt.NewRow();
                        dt.Rows.Add(dRow);
                        foreach (DataGridViewRow row in GVEmployee.Rows)
                        {
                            dRow = dt.NewRow();
                            foreach (DataGridViewCell cell in row.Cells)
                            {
                                dRow[cell.ColumnIndex] = cell.Value;
                            }
                            dt.Rows.Add(dRow);
                        }
                        ExportTableToExcel(dt, Path);
                        Process.Start(Path);
                    }
                    else
                    {
                        Utils.TNMessageBox( "Không tìm thấy dữ liệu!", 4);
                    }
                }

            }
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    newFile.Delete();
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(3, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 60;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 15;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 15;
                    worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        for (int j = 4; j <= zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    //Merge header
                    //DataTable zTablStage = Salary_Report_Productivity.StageName(dte_FromDate.Value, dte_ToDate.Value, _TeamKey); //Danh sách tên công đoạn
                    worksheet.Cells[1, 1].Value = "STT";
                    worksheet.Cells[1, 2].Value = "Tên thành phẩm";
                    worksheet.Cells[1, 3].Value = " Mã ";
                    worksheet.Cells[1, 1, 2, 1].Merge = true;
                    worksheet.Cells[1, 2, 2, 2].Merge = true;
                    worksheet.Cells[1, 3, 2, 3].Merge = true;
                    int k = 0;
                    for (int i = 4; i < TitleColumn.Length; i = i + 2)
                    {
                        worksheet.Cells[1, i, 1, i + 1].Merge = true;
                        worksheet.Cells[1, i, 1, i + 1].Value = TitleMerge[k]; //zTablStage.Rows[k]["StageName"].ToString();
                        worksheet.Cells[2, i].Value = "Số lượng";
                        worksheet.Cells[2, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[2, i + 1].Value = "Lương khoán";
                        worksheet.Cells[2, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        k++;
                    }

                    //for (int i = 4; i < zTable.Columns.Count; i = i + 2)
                    //{
                    //    worksheet.Cells[1, i, 1, i + 1].Merge = true;
                    //    worksheet.Cells[1, i, 1, i + 1].Value = zTablStage.Rows[k]["StageName"].ToString();
                    //    worksheet.Cells[2, i].Value = "Số lượng";
                    //    worksheet.Cells[2, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    worksheet.Cells[2, i + 1].Value = "Lương khoán";
                    //    worksheet.Cells[2, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    k++;
                    //}
                    worksheet.Cells[1, zTable.Columns.Count].Value = " Tổng cộng ";
                    worksheet.Cells[1, zTable.Columns.Count, 2, zTable.Columns.Count].Merge = true;                   //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
