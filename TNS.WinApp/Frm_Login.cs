﻿using System;
using System.Net;
using System.Windows.Forms;
using TNS.LOG;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Login : Form
    {
        public Frm_Login()
        {
            InitializeComponent();
            //txt_User.Text = "tns";
            //txt_Password.Text = "123456";
            btn_Login.Click += Btn_Login_Click;
            //btn_Cancel.Click += Btn_Cancel_Click;
            for(int i=Application.OpenForms.Count-1;i>=0;i--)
            {
                if(Application.OpenForms[i].Name!="Frm_Main")
                {
                    Application.OpenForms[i].Close();
                }
            }
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Btn_Login_Click(object sender, EventArgs e)
        {
            string[] zResult = User_Data.CheckUser(txt_User.Text, txt_Password.Text);

            if (zResult[0] == "ERR")
            {
                switch (zResult[1])
                {
                    case "CheckUser_Error01":
                        lbl_Message.Text = "Vui lòng kiểm tra lại tên Đăng Nhập";
                        break;
                    case "CheckUser_Error02":
                        lbl_Message.Text = "Tài khoản này chưa Active";
                        break;
                    case "CheckUser_Error03":
                        lbl_Message.Text = "Tài khoản của bạn đã bị khóa";
                        break;
                    case "CheckUser_Error04":
                        lbl_Message.Text = "Tài khoản này hết hạn";
                        break;
                    case "CheckUser_Error05":
                        lbl_Message.Text = "Vui lòng kiểm tra Mật Khẩu";
                        break;
                    case "CheckUser_Error06":
                        lbl_Message.Text = "Tài khoản đang được sử dụng ở máy khác";
                        break;
                }
                lbl_Message.Visible = true;
                Log_Login(2,"Tài khoản:"+txt_User.Text+"-"+ lbl_Message.Text);
            }
            else
            {
                string zUserKey = zResult[1];
                SessionUser zUserLogin = new SessionUser(txt_User.Text, zUserKey);
                Log_Login(1, "Đăng nhập thành công");
                this.Close();
            }
        }
        public void Log_Login(int TypeLog, string Description)
        {
            string zUserKey = "";
            if (TypeLog == 2)
            {
                zUserKey = "";
            }
            else
            {
                User_Info nUserLogin = new User_Info();
                nUserLogin.GetEmployee(SessionUser.UserLogin.Key);
                zUserKey = nUserLogin.Key;
            }

            string zComputerName = "";
            if (Environment.MachineName == null)
            {
                zComputerName = "Đối tượng bị null";
            }
            else
            {
                zComputerName = Environment.MachineName;
            }

            string zIpAddress = "";
            if (Dns.GetHostAddresses(Environment.MachineName) == null)
            {
                zIpAddress = "IP bị null";
            }
            else
            {
                if (Dns.GetHostAddresses(Environment.MachineName).Length == 1)
                    zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[0].ToString(); //có thể bị lấy nhằm IPV6
                else
                    zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[1].ToString();
            }
            string zWinVersion = "";
            //if (Environment.OSVersion.VersionString == null)
            //    zWinVersion = "Win version bị null";
            //else
            //    zWinVersion = Environment.OSVersion.VersionString.ToString();

            //string subKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion";
            //Microsoft.Win32.RegistryKey key;
            string zWinName = "";
            //if (Microsoft.Win32.Registry.LocalMachine != null)
            //{
            //    key = Microsoft.Win32.Registry.LocalMachine;
            //    Microsoft.Win32.RegistryKey skey = key.OpenSubKey(subKey);
            //    zWinName = skey.GetValue("ProductName").ToString();
            //}
            //else
            //{
            //    zWinName = "Winname bị null";
            //}

            Login_Info zInfo = new Login_Info();
            zInfo.UserKey = zUserKey;
            zInfo.DateLogin = DateTime.Now;
            zInfo.ComputerLogin = zComputerName;
            zInfo.IPAddress = zIpAddress;
            zInfo.WindownsName = zWinName;
            zInfo.WindownsVersion = zWinVersion;
            //1: thành công
            //2:thất bại
            //3:đăng xuất
            zInfo.TypeLog = TypeLog; 
            zInfo.Description = Description;
            zInfo.Create();
        }
        //public void Log_Login(bool Status, string Description)
        //{

        //    User_Info nUserLogin = new User_Info(txt_User.Text.Trim());


        //    string zComputerName = Environment.MachineName;
        //    string zIpAddress = "";
        //    //string zIpAddress = Dns.GetHostAddresses(Environment.MachineName)[1].ToString(); //có thể bị lấy nhằm IPV6
        //    string zWinVersion = Environment.OSVersion.VersionString.ToString();
        //    string zmyHost = System.Net.Dns.GetHostName();
        //    //int zCursor = System.Net.Dns.GetHostEntry(zmyHost).AddressList.Length - 1;
        //    //zIpAddress = System.Net.Dns.GetHostEntry(zmyHost).AddressList[zCursor].ToString();

        //    //Loại IP V6
        //    for (int i = 0; i <= System.Net.Dns.GetHostEntry(zmyHost).AddressList.Length - 1; i++)
        //    {
        //        //if (System.Net.Dns.GetHostEntry(zmyHost).AddressList[i].IsIPv6LinkLocal == false)
        //        //{
        //            zIpAddress = zIpAddress + ";" + System.Net.Dns.GetHostEntry(zmyHost).AddressList[i].ToString();
        //       // }
        //    }

        //    string subKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion";
        //    Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine;
        //    Microsoft.Win32.RegistryKey skey = key.OpenSubKey(subKey);
        //    string zWinName = skey.GetValue("ProductName").ToString();

        //    Login_Info zInfo = new Login_Info();
        //    zInfo.UserKey = nUserLogin.Key;
        //    zInfo.DateLogin = DateTime.Now;
        //    zInfo.ComputerLogin = zComputerName;
        //    zInfo.IPAddress = zIpAddress;
        //    zInfo.WindownsName = zWinName;
        //    zInfo.WindownsVersion = zWinVersion;
        //    if (Status == true)
        //        zInfo.FailedPassword = 0; //dăng nhập thành công
        //    else
        //        zInfo.FailedPassword = 1;
        //    zInfo.Description = Description;
        //    zInfo.Create();
        //}


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
