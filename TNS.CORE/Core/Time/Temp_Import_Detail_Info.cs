﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Temp_Import_Detail_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _SheetKey = 0;
        private int _ExcelKey = 0;
        private DateTime _DateImport;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private string _BeginTime = "00:00";
        private string _EndTime = "00:00";
        private string _OffTime = "00:00";
        private string _DifferentTime = "00:00";
        private string _TotalTime = "00:00";
        private string _OverTime = "00:00";
        private DateTime _OverTimeBegin;
        private DateTime _OverTimeEnd;
        private double _Money = 0;
        private double _Amount = 0;
        private int _Slug = 0;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private float _Time_TeamMain = 0; 

        private float _NBeginTime = 0;
        private float _NEndTime = 0;
        private float _NOffTime = 0;
        private float _NDifferentTime = 0;
        private float _NTotalTime = 0;
        private float _NOverTime = 0;
        
        private float _Rate = 0; //hệ số cn hoặc lễ
        private string _Class = "TH"; //TH: ngày thường, LE:ngày lễ, CN :Chủ nhật
        #endregion
        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public DateTime DateImport
        {
            get { return _DateImport; }
            set { _DateImport = value; }
        }
        public int SheetKey
        {
            get { return _SheetKey; }
            set { _SheetKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamID
        {
            get { return _TeamID; }
            set { _TeamID = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public string BeginTime
        {
            get { return _BeginTime; }
            set { _BeginTime = value; }
        }
        public string EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }
        public string OffTime
        {
            get { return _OffTime; }
            set { _OffTime = value; }
        }
        public string DifferentTime
        {
            get { return _DifferentTime; }
            set { _DifferentTime = value; }
        }
        public string TotalTime
        {
            get { return _TotalTime; }
            set { _TotalTime = value; }
        }
        public string OverTime
        {
            get { return _OverTime; }
            set { _OverTime = value; }
        }
        public DateTime OverTimeBegin
        {
            get { return _OverTimeBegin; }
            set { _OverTimeBegin = value; }
        }
        public DateTime OverTimeEnd
        {
            get { return _OverTimeEnd; }
            set { _OverTimeEnd = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int ExcelKey
        {
            get
            {
                return _ExcelKey;
            }

            set
            {
                _ExcelKey = value;
            }
        }

        public double Money
        {
            get
            {
                return _Money;
            }

            set
            {
                _Money = value;
            }
        }

        public double Amount
        {
            get
            {
                return _Amount;
            }

            set
            {
                _Amount = value;
            }
        }

        public float NBeginTime
        {
            get
            {
                return _NBeginTime;
            }

            set
            {
                _NBeginTime = value;
            }
        }

        public float NEndTime
        {
            get
            {
                return _NEndTime;
            }

            set
            {
                _NEndTime = value;
            }
        }

        public float NOffTime
        {
            get
            {
                return _NOffTime;
            }

            set
            {
                _NOffTime = value;
            }
        }

        public float NDifferentTime
        {
            get
            {
                return _NDifferentTime;
            }

            set
            {
                _NDifferentTime = value;
            }
        }

        public float NTotalTime
        {
            get
            {
                return _NTotalTime;
            }

            set
            {
                _NTotalTime = value;
            }
        }

        public float NOverTime
        {
            get
            {
                return _NOverTime;
            }

            set
            {
                _NOverTime = value;
            }
        }

        public float Rate
        {
            get
            {
                return _Rate;
            }

            set
            {
                _Rate = value;
            }
        }

        public string Class
        {
            get
            {
                return _Class;
            }

            set
            {
                _Class = value;
            }
        }

        public float Time_TeamMain
        {
            get
            {
                return _Time_TeamMain;
            }

            set
            {
                _Time_TeamMain = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Temp_Import_Detail_Info()
        {
        }
        public Temp_Import_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM Temp_Import_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    if (zReader["SheetKey"] != DBNull.Value)
                        _SheetKey = int.Parse(zReader["SheetKey"].ToString());
                    if (zReader["DateImport"] != DBNull.Value)
                        _DateImport = (DateTime)zReader["DateImport"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _BeginTime = zReader["BeginTime"].ToString();
                    _EndTime = zReader["EndTime"].ToString();
                    _OffTime = zReader["OffTime"].ToString();
                    _DifferentTime = zReader["DifferentTime"].ToString();
                    _TotalTime = zReader["TotalTime"].ToString();
                    _OverTime = zReader["OverTime"].ToString();
                    if (zReader["OverTimeBegin"] != DBNull.Value)
                        _OverTimeBegin = (DateTime)zReader["OverTimeBegin"];
                    if (zReader["OverTimeEnd"] != DBNull.Value)
                        _OverTimeEnd = (DateTime)zReader["OverTimeEnd"];

                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());

                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();

                    if (zReader["NBeginTime"] != DBNull.Value)
                        _NBeginTime = float.Parse(zReader["NBeginTime"].ToString());
                    if (zReader["NEndTime"] != DBNull.Value)
                        _NEndTime = float.Parse(zReader["NEndTime"].ToString());
                    if (zReader["NOffTime"] != DBNull.Value)
                        _NOffTime = float.Parse(zReader["NOffTime"].ToString());
                    if (zReader["NDifferentTime"] != DBNull.Value)
                        _NDifferentTime = float.Parse(zReader["NDifferentTime"].ToString());
                    if (zReader["NTotalTime"] != DBNull.Value)
                        _NTotalTime = float.Parse(zReader["NTotalTime"].ToString());
                    if (zReader["NOverTime"] != DBNull.Value)
                        _NOverTime = float.Parse(zReader["NOverTime"].ToString());
                    if (zReader["Rate"] != DBNull.Value)
                        _Rate = float.Parse(zReader["Rate"].ToString());
                    if (zReader["Time_TeamMain"] != DBNull.Value)
                        _Time_TeamMain = float.Parse(zReader["Time_TeamMain"].ToString());
                    _Class = zReader["Class"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Temp_Import_Detail_Info(DataRow Row)
        {
            try
            {
                if (Row["AutoKey"] != DBNull.Value)
                    _AutoKey = int.Parse(Row["AutoKey"].ToString());
                if (Row["ExcelKey"] != DBNull.Value)
                    _ExcelKey = int.Parse(Row["ExcelKey"].ToString());
                if (Row["SheetKey"] != DBNull.Value)
                    _SheetKey = int.Parse(Row["SheetKey"].ToString());
                if (Row["DateImport"] != DBNull.Value)
                    _DateImport = (DateTime)Row["DateImport"];
                _EmployeeKey = Row["EmployeeKey"].ToString();
                _EmployeeID = Row["EmployeeID"].ToString().Trim();
                _EmployeeName = Row["EmployeeName"].ToString().Trim();
                if (Row["TeamKey"] != DBNull.Value)
                    _TeamKey = int.Parse(Row["TeamKey"].ToString());
                _TeamName = Row["TeamName"].ToString().Trim();
                _TeamID = Row["TeamID"].ToString().Trim();
                _BeginTime = Row["BeginTime"].ToString();
                _EndTime = Row["EndTime"].ToString();
                _OffTime = Row["OffTime"].ToString();
                _DifferentTime = Row["DifferentTime"].ToString();
                _TotalTime = Row["TotalTime"].ToString();
                _OverTime = Row["OverTime"].ToString();
                if (Row["OverTimeBegin"] != DBNull.Value)
                    _OverTimeBegin = (DateTime)Row["OverTimeBegin"];
                if (Row["OverTimeEnd"] != DBNull.Value)
                    _OverTimeEnd = (DateTime)Row["OverTimeEnd"];
                if (Row["Money"] != DBNull.Value)
                    _Money = double.Parse(Row["Money"].ToString());
                if (Row["Amount"] != DBNull.Value)
                    _Amount = double.Parse(Row["Amount"].ToString());
                if (Row["Slug"] != DBNull.Value)
                    _Slug = int.Parse(Row["Slug"].ToString());
                _Description = Row["Description"].ToString().Trim();
                if (Row["RecordStatus"] != DBNull.Value)
                    _RecordStatus = int.Parse(Row["RecordStatus"].ToString());
                if (Row["CreatedOn"] != DBNull.Value)
                    _CreatedOn = (DateTime)Row["CreatedOn"];
                _CreatedBy = Row["CreatedBy"].ToString().Trim();
                _CreatedName = Row["CreatedName"].ToString().Trim();
                if (Row["ModifiedOn"] != DBNull.Value)
                    _ModifiedOn = (DateTime)Row["ModifiedOn"];
                _ModifiedBy = Row["ModifiedBy"].ToString().Trim();
                _ModifiedName = Row["ModifiedName"].ToString().Trim();

                if (Row["NBeginTime"] != DBNull.Value)
                    _NBeginTime = float.Parse(Row["NBeginTime"].ToString());
                if (Row["NEndTime"] != DBNull.Value)
                    _NEndTime = float.Parse(Row["NEndTime"].ToString());
                if (Row["NOffTime"] != DBNull.Value)
                    _NOffTime = float.Parse(Row["NOffTime"].ToString());
                if (Row["NDifferentTime"] != DBNull.Value)
                    _NDifferentTime = float.Parse(Row["NDifferentTime"].ToString());
                if (Row["NTotalTime"] != DBNull.Value)
                    _NTotalTime = float.Parse(Row["NTotalTime"].ToString());
                if (Row["NOverTime"] != DBNull.Value)
                    _NOverTime = float.Parse(Row["NOverTime"].ToString());
                if (Row["Rate"] != DBNull.Value)
                    _Rate = float.Parse(Row["Rate"].ToString());
                _Class = Row["Class"].ToString().Trim();
                if (Row["Time_TeamMain"] != DBNull.Value)
                    _Time_TeamMain = float.Parse(Row["Time_TeamMain"].ToString());
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
        }
        public Temp_Import_Detail_Info(string EmployeeID, DateTime DateTime)
        {
            DateTime zFromDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 23, 0, 0);

            string zSQL = "SELECT * FROM Temp_Import_Detail  WHERE EmployeeID = @Name AND DateImport BETWEEN @FromDate AND @ToDate AND RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = EmployeeID.Trim();
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    if (zReader["SheetKey"] != DBNull.Value)
                        _SheetKey = int.Parse(zReader["SheetKey"].ToString());
                    if (zReader["DateImport"] != DBNull.Value)
                        _DateImport = (DateTime)zReader["DateImport"];
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _BeginTime = zReader["BeginTime"].ToString();
                    _EndTime = zReader["EndTime"].ToString();
                    _OffTime = zReader["OffTime"].ToString();
                    _DifferentTime = zReader["DifferentTime"].ToString();
                    _TotalTime = zReader["TotalTime"].ToString();
                    _OverTime = zReader["OverTime"].ToString();
                    if (zReader["OverTimeBegin"] != DBNull.Value)
                        _OverTimeBegin = (DateTime)zReader["OverTimeBegin"];
                    if (zReader["OverTimeEnd"] != DBNull.Value)
                        _OverTimeEnd = (DateTime)zReader["OverTimeEnd"];

                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());

                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();

                    if (zReader["NBeginTime"] != DBNull.Value)
                        _NBeginTime = float.Parse(zReader["NBeginTime"].ToString());
                    if (zReader["NEndTime"] != DBNull.Value)
                        _NEndTime = float.Parse(zReader["NEndTime"].ToString());
                    if (zReader["NOffTime"] != DBNull.Value)
                        _NOffTime = float.Parse(zReader["NOffTime"].ToString());
                    if (zReader["NDifferentTime"] != DBNull.Value)
                        _NDifferentTime = float.Parse(zReader["NDifferentTime"].ToString());
                    if (zReader["NTotalTime"] != DBNull.Value)
                        _NTotalTime = float.Parse(zReader["NTotalTime"].ToString());
                    if (zReader["NOverTime"] != DBNull.Value)
                        _NOverTime = float.Parse(zReader["NOverTime"].ToString());
                    if (zReader["Rate"] != DBNull.Value)
                        _Rate = float.Parse(zReader["Rate"].ToString());
                    _Class = zReader["Class"].ToString().Trim();
                    if (zReader["Time_TeamMain"] != DBNull.Value)
                        _Time_TeamMain = float.Parse(zReader["Time_TeamMain"].ToString());
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO Temp_Import_Detail (
SheetKey ,ExcelKey ,DateImport,EmployeeKey ,EmployeeID ,EmployeeName ,
TeamKey,TeamName,TeamID,BeginTime ,EndTime ,OffTime ,DifferentTime ,
TotalTime ,OverTime ,NBeginTime ,NEndTime ,NOffTime ,NDifferentTime ,
NTotalTime ,NOverTime , Time_TeamMain,
OverTimeBegin ,OverTimeEnd ,Slug ,Description ,Money,Amount,Rate,Class,
RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) 
VALUES ( 
@SheetKey,@ExcelKey ,@DateImport,@EmployeeKey ,@EmployeeID ,@EmployeeName ,
@TeamKey,@TeamName,@TeamID,@BeginTime ,@EndTime ,@OffTime ,@DifferentTime ,
@TotalTime ,@OverTime ,@NBeginTime ,@NEndTime ,@NOffTime ,@NDifferentTime ,
@NTotalTime ,@NOverTime , @Time_TeamMain,
@OverTimeBegin ,@OverTimeEnd ,@Slug ,@Description ,@Money,@Amount ,@Rate, @Class,
@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT '11' + CAST(AutoKey AS nvarchar) FROM Temp_Import_Detail WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@SheetKey", SqlDbType.Int).Value = _SheetKey;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                if (_DateImport == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = _DateImport;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.NVarChar).Value = _BeginTime;
                zCommand.Parameters.Add("@EndTime", SqlDbType.NVarChar).Value = _EndTime;
                zCommand.Parameters.Add("@OffTime", SqlDbType.NVarChar).Value = _OffTime;
                zCommand.Parameters.Add("@DifferentTime", SqlDbType.NVarChar).Value = _DifferentTime;
                zCommand.Parameters.Add("@TotalTime", SqlDbType.NVarChar).Value = _TotalTime;
                zCommand.Parameters.Add("@OverTime", SqlDbType.NVarChar).Value = _OverTime;

                zCommand.Parameters.Add("@NBeginTime", SqlDbType.Float).Value = _NBeginTime;
                zCommand.Parameters.Add("@NEndTime", SqlDbType.Float).Value = _NEndTime;
                zCommand.Parameters.Add("@NOffTime", SqlDbType.Float).Value = _NOffTime;
                zCommand.Parameters.Add("@NDifferentTime", SqlDbType.Float).Value = _NDifferentTime;
                zCommand.Parameters.Add("@NTotalTime", SqlDbType.Float).Value = _NTotalTime;
                zCommand.Parameters.Add("@NOverTime", SqlDbType.Float).Value = _NOverTime;

                if (_OverTimeBegin == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = _OverTimeBegin;
                if (_OverTimeEnd == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = _OverTimeEnd;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Amount", SqlDbType.Decimal).Value = _Amount;
                zCommand.Parameters.Add("@Money", SqlDbType.Decimal).Value = _Money;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@Rate", SqlDbType.Float).Value = _Rate;
                zCommand.Parameters.Add("@Time_TeamMain", SqlDbType.Float).Value = _Time_TeamMain;
                zCommand.Parameters.Add("@Class", SqlDbType.Char).Value = _Class;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                if (zResult.Substring(0, 2) == "11")
                {
                    _AutoKey = int.Parse(zResult.Substring(2));
                    _Message = zResult.Substring(0, 2);
                }

                else
                    _AutoKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE Temp_Import_Detail SET "
                        + " SheetKey = @SheetKey,"
                          + " ExcelKey = @ExcelKey,"
                        + " DateImport = @DateImport,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeID = @EmployeeID,"
                        + " EmployeeName = @EmployeeName,"
                        + " TeamKey = @TeamKey,"
                        + " TeamName = @TeamName,"
                        + " TeamID = @TeamID,"
                        + " BeginTime = @BeginTime,"
                        + " EndTime = @EndTime,"
                        + " OffTime = @OffTime,"
                        + " DifferentTime = @DifferentTime,"
                        + " TotalTime = @TotalTime,"
                        + " OverTime = @OverTime,"
                        + " NBeginTime = @NBeginTime,"
                        + " NEndTime = @NEndTime,"
                        + " NOffTime = @NOffTime,"
                        + " NDifferentTime = @NDifferentTime,"
                        + " NTotalTime = @NTotalTime,"
                        + " NOverTime = @NOverTime,"
                        + " OverTimeBegin = @OverTimeBegin,"
                        + " OverTimeEnd = @OverTimeEnd,"
                        + " Slug = @Slug,"
                        + " Rate = @Rate,"
                        + " Amount = @Amount,"
                        + " Money = @Money,"
                        + " Time_TeamMain = @Time_TeamMain,"
                        + " Description = @Description,"
                        + " Class = @Class,"
                        + " RecordStatus = 1,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@SheetKey", SqlDbType.Int).Value = _SheetKey;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                if (_DateImport == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateImport", SqlDbType.DateTime).Value = _DateImport;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.NVarChar).Value = _BeginTime;
                zCommand.Parameters.Add("@EndTime", SqlDbType.NVarChar).Value = _EndTime;
                zCommand.Parameters.Add("@OffTime", SqlDbType.NVarChar).Value = _OffTime;
                zCommand.Parameters.Add("@DifferentTime", SqlDbType.NVarChar).Value = _DifferentTime;
                zCommand.Parameters.Add("@TotalTime", SqlDbType.NVarChar).Value = _TotalTime;
                zCommand.Parameters.Add("@OverTime", SqlDbType.NVarChar).Value = _OverTime;

                zCommand.Parameters.Add("@NBeginTime", SqlDbType.Float).Value = _NBeginTime;
                zCommand.Parameters.Add("@NEndTime", SqlDbType.Float).Value = _NEndTime;
                zCommand.Parameters.Add("@NOffTime", SqlDbType.Float).Value = _NOffTime;
                zCommand.Parameters.Add("@NDifferentTime", SqlDbType.Float).Value = _NDifferentTime;
                zCommand.Parameters.Add("@NTotalTime", SqlDbType.Float).Value = _NTotalTime;
                zCommand.Parameters.Add("@NOverTime", SqlDbType.Float).Value = _NOverTime;

                if (_OverTimeBegin == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = _OverTimeBegin;
                if (_OverTimeEnd == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = _OverTimeEnd;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Amount", SqlDbType.Decimal).Value = _Amount;
                zCommand.Parameters.Add("@Money", SqlDbType.Decimal).Value = _Money;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@Rate", SqlDbType.Float).Value = _Rate;
                zCommand.Parameters.Add("@Time_TeamMain", SqlDbType.Float).Value = _Time_TeamMain;
                zCommand.Parameters.Add("@Class", SqlDbType.Char).Value = _Class;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE Temp_Import_Detail SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
