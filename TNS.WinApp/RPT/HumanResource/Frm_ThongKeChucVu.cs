﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
using TNS_Report;

namespace TNS.WinApp
{
    public partial class Frm_ThongKeChucVu : Form
    {
        private int _BranchKey = 0;
        private int _TeamKey= 0;
        private int _DepartmentKey = 0;
        private int _PositionKey = 0;
        private DateTime _FromDate;
        public Frm_ThongKeChucVu()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

             btn_Export.Click += Btn_Export_Click;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;

            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Tất cả ----");
            LoadDataToToolbox.KryptonComboBox(cbo_Position, "SELECT PositionKey,PositionName FROM SYS_Position WHERE   RecordStatus < 99 ORDER BY Rank", "---- Chọn tất cả----");
        }

        private void Frm_ThongKeChucVu_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime DateView = SessionUser.Date_Work;
            dte_FromDate.Value = DateTime.Now;

        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            _BranchKey = cbo_Branch.SelectedValue.ToInt();

            if (cbo_Department.SelectedIndex > 0)
                _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            else
                _DepartmentKey = 0;
            if (cbo_Team.SelectedIndex > 0)
                _TeamKey = cbo_Team.SelectedValue.ToInt();
            else
                _TeamKey = 0;
            if (cbo_Position.SelectedIndex > 0)
                _PositionKey = cbo_Position.SelectedValue.ToInt();
            else
                _PositionKey = 0;
            _FromDate = dte_FromDate.Value;

            string Status = "Tìm DL form " + HeaderControl.Text + " > tháng: " + dte_FromDate.Value.ToString("MM/yyyy");
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }
        #region[Xem DislayData]
        private void DisplayData()
        {


            DataTable zTable = new DataTable();

            zTable = Report_HumanResource.PHANBO_CHUCVU(dte_FromDate.Value,_BranchKey,_DepartmentKey,_TeamKey,_PositionKey);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "Amount" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Số lượng" }, 0);

                zTablePivot.Columns.Add("PositionRank", typeof(int));
                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] s = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["PositionRank"] = s[2];

                }
                zTablePivot.Rows[zTablePivot.Rows.Count - 1]["PositionRank"] = 9999;
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                //DataRow row = zTablePivot.Rows[zTablePivot.Rows.Count - 1];
                //zTablePivot.Rows.Remove(row);
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " PositionRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("PositionRank");


                string[] temp = zTablePivot.Columns[1].ColumnName.Split('|');
                string DateFlag = temp[0];
                string KeyFlag = temp[1];
                int m = 0;
                for (int i = 1; i < zTablePivot.Columns.Count; i++)
                {
                    temp = zTablePivot.Columns[i].ColumnName.Split('|');
                    if (temp.Length == 2)// nếu là TONGCONG thì add vào cột tổng nhỏ
                    {
                        zTablePivot.Columns.Add(DateFlag + "|Tổng", typeof(double)).SetOrdinal(i);
                        break;
                    }
                    string Date = temp[0];
                    string TeamKey = temp[1];
                    string TeamName = temp[2];


                    if (DateFlag != "0" && DateFlag != Date)
                    {
                        //add cột tổng
                        zTablePivot.Columns.Add(DateFlag + "|Tổng", typeof(double)).SetOrdinal(i);
                        DateFlag = Date;
                    }

                    if (KeyFlag != TeamKey)
                    {
                        KeyFlag = TeamKey;
                    }

                }

                //tính toán cột tổng mỗi tháng
                foreach (DataRow r in zTablePivot.Rows)
                {
                    temp = zTablePivot.Columns[1].ColumnName.Split('|');
                    double TongCotThang = 0;
                    DateFlag = temp[0];

                    for (int k = 1; k < zTablePivot.Columns.Count; k++)
                    {
                        temp = zTablePivot.Columns[k].ColumnName.Split('|');
                        string Date = temp[0];

                        if (DateFlag != Date)
                        {
                            r[k - 1] = TongCotThang.ToString();

                            TongCotThang = 0;
                            DateFlag = temp[0];
                        }
                        double zSoTien = 0;
                        if (double.TryParse(r[k].ToString(), out zSoTien))
                        {

                        }
                        TongCotThang += zSoTien;
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
                GVData.Clear();
            }
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "MÃ";


            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "CHỨC VỤ";



            //Row Header
            int ColStart = 3;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                    GVData.Cols[ColStart].Width = 60;

                }
                else
                {

                    if (strCaption[0].Trim() == "TỔNG CỘNG")
                    {
                        GVData.Rows[0][ColStart] = "TỔNG CỘNG";
                        GVData.Rows[1][ColStart] = "-";

                        GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                        GVData.Cols[ColStart].StyleNew.ForeColor = Color.Red;
                        GVData.Cols[ColStart].Width = 60;
                    }
                    else
                    {
                        if (strCaption[1] == "Tổng")
                        {
                            GVData.Rows[0][ColStart] = strCaption[0];
                            GVData.Rows[1][ColStart] = strCaption[1];
                            GVData.Cols[ColStart].Width = 60;
                            GVData.Cols[ColStart].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
                            //GVData.Cols[ColStart].StyleNew.BackColor = Color.Pink;
                            GVData.Cols[ColStart].StyleNew.ForeColor = Color.Blue;
                        }
                        //else
                        //{
                        //    GVData.Rows[0][ColStart] = strCaption[0];
                        //    GVData.Rows[1][ColStart] = strCaption[1];
                        //    GVData.Cols[ColStart].Width = 120;
                        //}
                    }
                }

                ColStart++;
            }
            //--------------------
            //double TotalLK = 0;
            //double TotalTG = 0;
            //double TotalTP = 0;

            ////Fill Data
            ColStart = 2;// add data vào cột thứ 1 trở đi
            int RowStart = 2;//add dòng từ số thứ tự 4 trở đi
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows[RowStart][0] = zNo;

                ColStart = 2;
                for (int k = 0; k < rData.ItemArray.Length ; k++)
                {
                    if (k == 0)
                    {
                        string[] s = rData[0].ToString().Trim().Split('|');
                        if(s[0]=="TỔNG CỘNG")
                        {
                            GVData.Rows[RowStart][ColStart - 1] = s[0];
                            GVData.Rows[RowStart][ColStart] = "";
                        }
                        else
                        {
                            GVData.Rows[RowStart][ColStart - 1] = s[0];
                            GVData.Rows[RowStart][ColStart] = s[1];
                        }

                    }
                    else
                    {
                        GVData.Rows[RowStart][ColStart] = rData[k].Toe0String();// tên nội dung
                    }
                    ColStart++;
                }

                zNo++;
                RowStart++;
            }

            ////Style

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            //Trộn dòng 1
            //GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 150;
            //Trộn dòng 2
            //GVData.Rows[2].AllowMerging = true;
            //Trộn dòng cuối
            GVData.Rows[GVData.Rows.Count - 1].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 300;
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Rows[GVData.Rows.Count-1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

        }
        #endregion
        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo cáo chức danh" + dte_FromDate.Value.ToString("MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
