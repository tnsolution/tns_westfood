﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class WorkingHistory_Data
    {
        public static DataTable List(string Search)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT HistoryKey,EmployeeID,EmployeeName,BranchName,
DepartmentName,TeamName,PositionName,
StartingDate,LeavingDate,[Description] 
FROM HRM_WorkingHistory 
WHERE RecordStatus != 99 ";
            if (Search.Length > 0)
            {
                zSQL += "AND EmployeeID = @Search";
            }

            zSQL += " ORDER BY LEN(EmployeeID),EmployeeID, StartingDate DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = Search;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string Working_CapNhatNgayHetHan_NghiViec(string EmployeeKey, DateTime DateTime, string ModifiedBy, string ModifiedName)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE [dbo].[HRM_WorkingHistory] SET LeavingDate = @LeavingDate , ModifiedOn =GETDATE(),ModifiedBy =@ModifiedBy, ModifiedName=@ModifiedName
WHERE EmployeeKey= @EmployeeKey AND LeavingDate IS NULL AND RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DateTime;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {
                zResult = Ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
