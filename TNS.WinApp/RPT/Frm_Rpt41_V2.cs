﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.Misc;
using TNS.CORE;
using TNS.SYS;
using System.IO;
using System.Diagnostics;
using TNS.LOG;

namespace TNS.WinApp
{
    public partial class Frm_Rpt41_V2 : Form
    {
        string _ProductGroup = "0";
        float _SoGio = 0;
        float _SoRo = 0;
        float _NguyenLieu = 0;
        float _ThanhPham = 0;
        int _StageKey = 0;
        public Frm_Rpt41_V2()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += btn_Search_Click;
            btn_ExportMaster.Click += Btn_ExportMaster_Click;
            btn_ExportDetail.Click += Btn_ExportDetail_Click;
        }

        private void Btn_ExportDetail_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "ThongKeNangSuatCongDoan_SanPham_CongNhan.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVEmployee.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
           
        }

        private void Btn_ExportMaster_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "ThongKeNangSuatCongDoan_SanPham_CongDoan.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }

        private void Frm_Rpt_41_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.DoubleClick += GVData_SelChange;

            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LoadDataToToolbox.KryptonComboBox(cbo_ProductID, @"
SELECT GroupID,GroupName 
FROM IVT_Product_Stages_Group
WHERE RecordStatus <> 99
ORDER BY GroupID", "--Tất cả--");
        }
        private void GVData_SelChange(object sender, EventArgs e)
        {
            if (GVData.RowSel > 0)
            {
                GVEmployee.Rows.Count = 0;
                GVEmployee.Cols.Count = 0;

                _StageKey = GVData.Rows[GVData.RowSel][11].ToInt();
                 _SoGio = 0;
                 _SoRo = 0;
                 _NguyenLieu = 0;
                 _ThanhPham = 0;
                if (float.TryParse(GVData.Rows[GVData.RowSel][5].ToString(), out _SoGio))
                {

                }
                if (float.TryParse(GVData.Rows[GVData.RowSel][6].ToString(), out _SoRo))
                {

                }
                if (float.TryParse(GVData.Rows[GVData.RowSel][7].ToString(), out _NguyenLieu))
                {

                }
                if (float.TryParse(GVData.Rows[GVData.RowSel][8].ToString(), out _ThanhPham))
                {

                }
                using (Frm_Loading frm = new Frm_Loading(DisplayDetail)) { frm.ShowDialog(this); }

            }
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;

            _ProductGroup = cbo_ProductID.SelectedValue.ToString();
            //if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            //{
                //Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                //return;
            //}
            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }
        private void DisplayData()
        {
            DataTable zTable = Report.Report_41_Master_V4(dte_FromDate.Value, dte_ToDate.Value, _ProductGroup);

            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataMaster(zTable);
                }));
            }
        }
        private void DisplayDetail()
        {
            DataTable zTable = Report.Report_41_Detail_V3(dte_FromDate.Value, dte_ToDate.Value, _StageKey, _SoGio, _SoRo, _NguyenLieu, _ThanhPham);
            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataDetail(zTable);
                }));
            }
        }

        void InitDataMaster(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "SẢN PHẨM";
            GVData.Rows[0][2] = "CÔNG ĐOẠN";
            GVData.Rows[0][3] = "MÃ CÔNG ĐOẠN";
            GVData.Rows[0][4] = "TỔNG CÔNG NHÂN";
            GVData.Rows[0][5] = "TỔNG THỜI GIAN";
            GVData.Rows[0][6] = "TỔNG SỐ RỔ";
            GVData.Rows[0][7] = "TỔNG SỐ LƯỢNG NL";
            GVData.Rows[0][8] = "TỔNG SỐ LƯỢNG TP";
            GVData.Rows[0][9] = "NS Nguyên liệu (KG/Giờ)";
            GVData.Rows[0][10] = "NS Thành phẩm (KG/Giờ)";
            GVData.Rows[0][11] = ""; // ẩn cột này

            double TotalCol4 = 0;
            double TotalCol5 = 0;
            double TotalCol6 = 0;
            double TotalCol7 = 0;
            double TotalCol8 = 0;
            double TotalCol9 = 0;
            double TotalCol10 = 0;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5].Ton2String();
                GVData.Rows[rIndex + 1][6] = rData[6].Ton2String();
                GVData.Rows[rIndex + 1][7] = rData[7].Ton2String();
                GVData.Rows[rIndex + 1][8] = rData[8].Ton2String();
                GVData.Rows[rIndex + 1][9] = rData[9].Ton2String();
                GVData.Rows[rIndex + 1][10] = rData[10].Ton2String();
                GVData.Rows[rIndex + 1][11] = rData[0];

                double Col4 = 0;
                double Col5 = 0;
                double Col6 = 0;
                double Col7 = 0;
                double Col8 = 0;
                double Col9 = 0;
                double Col10 = 0;

                if(double.TryParse(rData[4].ToString(),out Col4))
                {

                }
                if (double.TryParse(rData[5].ToString(), out Col5))
                {

                }
                if (double.TryParse(rData[6].ToString(), out Col6))
                {

                }
                if (double.TryParse(rData[7].ToString(), out Col7))
                {

                }
                if (double.TryParse(rData[8].ToString(), out Col8))
                {

                }
                if (double.TryParse(rData[9].ToString(), out Col9))
                {

                }
                if (double.TryParse(rData[10].ToString(), out Col10))
                {

                }

                TotalCol4 += Col4;
                TotalCol5 += Col5;
                TotalCol6 += Col6;
                TotalCol7 += Col7;
                TotalCol8 += Col8;
                TotalCol9 += Col9;
                TotalCol10 += Col10;
            }

            GVData.Rows[TotalRow - 1][1] = "TỔNG";
            GVData.Rows[TotalRow - 1][2] = "";
            GVData.Rows[TotalRow - 1][3] = "";
            GVData.Rows[TotalRow - 1][4] = TotalCol4.ToString();
            GVData.Rows[TotalRow - 1][5] = TotalCol5.Ton2String();
            GVData.Rows[TotalRow - 1][6] = TotalCol6.Ton2String();
            GVData.Rows[TotalRow - 1][7] = TotalCol7.Ton2String();
            GVData.Rows[TotalRow - 1][8] = TotalCol8.Ton2String();
            GVData.Rows[TotalRow - 1][9] = TotalCol9.Ton2String();
            GVData.Rows[TotalRow - 1][10] = TotalCol10.Ton2String();
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 100;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[2].Width = 300;
            GVData.Cols[3].Width = 80;
            GVData.Cols[11].Visible = false;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Fixed = 3;

            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 4; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        void InitDataDetail(DataTable TableView)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count;

            GVEmployee.Cols.Add(ToTalCol);
            GVEmployee.Rows.Add(TotalRow);

            //Row Header
            GVEmployee.Rows[0][0] = "STT";
            GVEmployee.Rows[0][1] = "HỌ TÊN";
            GVEmployee.Rows[0][2] = "MÃ SỐ";
            GVEmployee.Rows[0][3] = "MÃ NHÓM";
            GVEmployee.Rows[0][4] = "TỔNG THỜI GIAN";
            GVEmployee.Rows[0][5] = "TỔNG SỐ RỔ";
            GVEmployee.Rows[0][6] = "TỔNG SỐ LƯỢNG NL";
            GVEmployee.Rows[0][7] = "TỔNG SỐ LƯỢNG TP";
            GVEmployee.Rows[0][8] = "NS Nguyên liệu (KG/Giờ)";
            GVEmployee.Rows[0][9] = "NS Thành phẩm (KG/Giờ)";

            double TotalCol4 = 0;
            double TotalCol5 = 0;
            double TotalCol6 = 0;
            double TotalCol7 = 0;
            double TotalCol8 = 0;
            double TotalCol9 = 0;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVEmployee.Rows[rIndex + 1][0] = rIndex + 1;
                GVEmployee.Rows[rIndex + 1][1] = rData[0];
                GVEmployee.Rows[rIndex + 1][2] = rData[1];
                GVEmployee.Rows[rIndex + 1][3] = rData[2];
                GVEmployee.Rows[rIndex + 1][4] = rData[3].Ton2String();
                GVEmployee.Rows[rIndex + 1][5] = rData[4].Ton2String();
                GVEmployee.Rows[rIndex + 1][6] = rData[5].Ton2String();
                GVEmployee.Rows[rIndex + 1][7] = rData[6].Ton2String();
                GVEmployee.Rows[rIndex + 1][8] = rData[7].Ton2String();
                GVEmployee.Rows[rIndex + 1][9] = rData[8].Ton2String();

                double Col4 = 0;
                double Col5 = 0;
                double Col6 = 0;
                double Col7 = 0;
                double Col8 = 0;
                double Col9 = 0;

                if (double.TryParse(rData[3].ToString(), out Col4))
                {

                }
                if (double.TryParse(rData[4].ToString(), out Col5))
                {

                }
                if (double.TryParse(rData[5].ToString(), out Col6))
                {

                }
                if (double.TryParse(rData[6].ToString(), out Col7))
                {

                }
                if (double.TryParse(rData[7].ToString(), out Col8))
                {

                }
                if (double.TryParse(rData[8].ToString(), out Col9))
                {

                }


                TotalCol4 += Col4;
                TotalCol5 += Col5;
                TotalCol6 += Col6;
                TotalCol7 += Col7;
                TotalCol8 += Col8;
                TotalCol9 += Col9;
                if (rData[9].ToInt() == 1)
                {
                    GVEmployee.Rows[rIndex + 1].StyleNew.BackColor = Color.LemonChiffon;
                }
            }

            GVEmployee.Rows[TotalRow - 1][1] = "TỔNG";
            GVEmployee.Rows[TotalRow - 1][2] = "";
            GVEmployee.Rows[TotalRow - 1][3] = "";
            GVEmployee.Rows[TotalRow - 1][4] = TotalCol4.Ton2String();
            GVEmployee.Rows[TotalRow - 1][5] = TotalCol5.Ton2String();
            GVEmployee.Rows[TotalRow - 1][6] = TotalCol6.Ton2String();
            GVEmployee.Rows[TotalRow - 1][7] = TotalCol7.Ton2String();
            GVEmployee.Rows[TotalRow - 1][8] = TotalCol8.Ton2String();
            GVEmployee.Rows[TotalRow - 1][9] = TotalCol9.Ton2String();
            GVEmployee.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            //Style         
            GVEmployee.ExtendLastCol = false;
            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Styles.Normal.WordWrap = true;

            GVEmployee.Rows[0].Height = 40;

            GVEmployee.Cols[0].Width = 40;
            GVEmployee.Cols[1].Width = 150;

            GVEmployee.Rows.Fixed = 1;
            GVEmployee.Cols.Fixed = 3;

            GVEmployee.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVEmployee.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVEmployee.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 2; i < GVEmployee.Cols.Count; i++)
            {
                GVEmployee.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        string UnFormatMoney(object input)
        {
            try
            {
                if (input.ToString().Length > 0)
                {
                    string zTemp = input.ToString().Replace(".", "");
                    zTemp = zTemp.Replace(",", ".");
                    double zResult = double.Parse(zTemp);
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("en-US"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_ExportDetail.Enabled = false;
                btn_ExportMaster.Enabled = false;
            }
        }
        #endregion
    }
}
