﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Combo_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT ComboKey,ComboID,ComboName,ProductID,ProductName,UnitName,ComboCost,FromDate,ToDate,Description FROM [dbo].[IVT_Combo]
WHERE RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                //zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Product_Of_Material(string Parent,string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,dbo.Fn_GetUnitName(BasicUnit) AS UnitName,B.CategoryName AS CategoryName FROM IVT_Product  A
LEFT JOIN dbo.IVT_Product_Category B ON B.CategoryKey = A.CategoryKey
WHERE A.Parent = @Parent AND A.RecordStatus < 99  ";
if(Name.Trim().Length>0)
            {
                zSQL += " AND(ProductID LIKE @Name OR ProductName LIKE @Name)";
            }
zSQL+=@" ORDER BY LEN(A.ProductID), A.ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%"+Name+"%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListStage(string Name, int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *, dbo.LayNhomCongViec(StageKey) AS GroupID FROM IVT_Product_Stages WHERE RecordStatus <> 99 ";
            if (Name != string.Empty)
                zSQL += " AND StageName LIKE N'%" + Name + "%' ";
            if (TeamKey != 0)
                zSQL += " AND TeamKey = @TeamKey";
            zSQL += " ORDER BY StagesID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static bool CheckProductKey(string ProductKey)
        {
            bool zResult = false;  //  Khong co
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT Count(*) AS Amount FROM dbo.IVT_Combo WHERE ProductKey = @ProductKey AND RecordStatus != 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = ProductKey;
                //zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                //zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string Result = ex.ToString();
            }
            if (zTable.Rows.Count > 0)
            {
                int zAmount = 0;
                DataRow zRow = zTable.Rows[0];
                zAmount = int.Parse(zRow["Amount"].ToString());
                if (zAmount == 0)
                {
                    zResult = true;
                }
                else
                {
                    zResult = false;
                }
            }
            return zResult;
        }
        public static DataTable ListItem(string ComboKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM [dbo].[IVT_ComboItem] WHERE RecordStatus <> 99 AND ComboKey = @ComboKey ORDER BY [Rank]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ComboKey", SqlDbType.NVarChar).Value = ComboKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static bool CheckComboID(string ComboID)
        {
            bool zResult = false;  //  Khong co
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT Count(*) AS Amount FROM dbo.IVT_Combo WHERE ComboID = @ComboID AND RecordStatus != 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ComboID", SqlDbType.NVarChar).Value = ComboID;
                //zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                //zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string Result = ex.ToString();
            }
            if (zTable.Rows.Count > 0)
            {
                int zAmount = 0;
                DataRow zRow = zTable.Rows[0];
                zAmount = int.Parse(zRow["Amount"].ToString());
                if (zAmount == 0)
                {
                    zResult = true;
                }
                else
                {
                    zResult = false;
                }
            }
            return zResult;
        }
    }
}
