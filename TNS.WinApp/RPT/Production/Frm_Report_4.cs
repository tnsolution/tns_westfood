﻿using OfficeOpenXml;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_4 : Form
    {
        int _ViewTeam = 0;
        public Frm_Report_4()
        {
            InitializeComponent();
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);


            InitLayout_GV(GVData);

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY RANK";
            LoadDataToToolbox.KryptonComboBox(cbo_TeamID_Search, zSQL, "---Chọn tất cả---");

            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
        }

        private void InitLayout_GV(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("CardID", "Mã NV");

            GV.Columns.Add("NgoaiGio", "Ngoài giờ");
            GV.Columns.Add("KhoanSP", "Khoán SP (Tại tổ)");                                    //Lương Ngày Thường
            GV.Columns.Add("LamCN", "Làm thêm CN (Tại tổ)");                          //Lương Ngày Chủ Nhật
            GV.Columns.Add("LamLe", "Làm thêm lễ (Tại tổ)");                                                //Lương Ngày Lễ

            GV.Columns.Add("KhoanSPKhac", "Khoán SP (Tổ khác) ");               //Lương Khác Ngày Thường
            GV.Columns.Add("KhoanSPCN", "Khoán SP CN (Tổ khác)");            //Lương Khác Ngày Chủ Nhật
            GV.Columns.Add("KhoanSPLe", "Khoán SP lễ (Tổ khác)");       //Lương Việc Ngày Ngày Lễ
            GV.Columns.Add("Tong", "Tổng");

            GV.Columns["No"].Width = 60;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 250;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["EmployeeName"].ReadOnly = true;

            GV.Columns["CardID"].Width = 140;
            GV.Columns["CardID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["CardID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["CardID"].ReadOnly = true;
            GV.Columns["CardID"].Frozen = true;

            GV.Columns["NgoaiGio"].Width = 100;
            GV.Columns["NgoaiGio"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["NgoaiGio"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSP"].Width = 100;
            GV.Columns["KhoanSP"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSP"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["LamCN"].Width = 100;
            GV.Columns["LamCN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["LamCN"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["LamLe"].Width = 100;
            GV.Columns["LamLe"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["LamLe"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSPKhac"].Width = 100;
            GV.Columns["KhoanSPKhac"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSPKhac"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSPLe"].Width = 100;
            GV.Columns["KhoanSPLe"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSPLe"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["KhoanSPCN"].Width = 100;
            GV.Columns["KhoanSPCN"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KhoanSPCN"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["Tong"].Width = 100;
            GV.Columns["Tong"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Tong"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 52;
        }

        private void HeaderRow(DataGridViewRow RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView.Cells["No"].Value = (No + 1).ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = TeamName;
            RowView.Cells["CardID"].Value = "Số công nhân " + Table.Select("TeamKey=" + TeamKey).Length;

            RowView.Cells["NgoaiGio"].Value = Table.Compute("SUM([NgoaiGio])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSP"].Value = Table.Compute("SUM([KhoanSP])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["LamCN"].Value = Table.Compute("SUM([LamCN])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["LamLe"].Value = Table.Compute("SUM([LamLe])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSPKhac"].Value = Table.Compute("SUM([KhoanSPKhac])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSPLe"].Value = Table.Compute("SUM([KhoanSPLe])", "TeamKey=" + TeamKey).Toe0String();
            RowView.Cells["KhoanSPCN"].Value = Table.Compute("SUM([KhoanSPCN])", "TeamKey=" + TeamKey).Toe0String();

            RowView.DefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        private void DetailRow(DataGridViewRow RowView, DataRow rDetail, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = rDetail["EmployeeKey"].ToString().Trim();
            RowView.Cells["EmployeeName"].Value = rDetail["EmployeeName"].ToString().Trim();
            RowView.Cells["CardID"].Value = rDetail["EmployeeID"].ToString().Trim();

            RowView.Cells["NgoaiGio"].Value = rDetail["NgoaiGio"].Toe0String();
            RowView.Cells["KhoanSP"].Value = rDetail["KhoanSP"].Toe0String();
            RowView.Cells["LamCN"].Value = rDetail["LamCN"].Toe0String();
            RowView.Cells["LamLe"].Value = rDetail["LamLe"].Toe0String();
            RowView.Cells["KhoanSPKhac"].Value = rDetail["KhoanSPKhac"].Toe0String();
            RowView.Cells["KhoanSPCN"].Value = rDetail["KhoanSPCN"].Toe0String();
            RowView.Cells["KhoanSPLe"].Value = rDetail["KhoanSPLe"].Toe0String();

            double zTotalRow = 0;
            for (int i = 4; i < rDetail.ItemArray.Length-1; i++)
            {
                double zTemp = 0;
                if(double.TryParse(rDetail[i].ToString(),out zTemp))
                {

                }
                zTotalRow += zTemp;
            }

            RowView.Cells["Tong"].Value = zTotalRow.Toe0String();
        }
        private void TotalRow(DataGridViewRow RowView, DataTable Table, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = "Tổng ";
            RowView.Cells["CardID"].Value = "";

            RowView.Cells["NgoaiGio"].Value = Table.Compute("SUM([NgoaiGio])", "").Toe0String();
            RowView.Cells["KhoanSP"].Value = Table.Compute("SUM([KhoanSP])", "").Toe0String();
            RowView.Cells["LamCN"].Value = Table.Compute("SUM([LamCN])", "").Toe0String();
            RowView.Cells["LamLe"].Value = Table.Compute("SUM([LamLe])", "").Toe0String();
            RowView.Cells["KhoanSPKhac"].Value = Table.Compute("SUM([KhoanSPKhac])", "").Toe0String();
            RowView.Cells["KhoanSPLe"].Value = Table.Compute("SUM([KhoanSPLe])", "").Toe0String();
            RowView.Cells["KhoanSPCN"].Value = Table.Compute("SUM([KhoanSPCN])", "").Toe0String();

            RowView.DefaultCellStyle.Font = new Font("Tahoma", 10, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LightSkyBlue;
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            
            GVData.Rows.Clear();
            if (cbo_TeamID_Search.SelectedIndex >= 0)
                _ViewTeam = cbo_TeamID_Search.SelectedValue.ToInt();

            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cbo_TeamID_Search.Text;
             Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private int _STT = 1;
        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);

            DataTable zTable = HoTroSanXuat.TongHopLuongNangSuat_V2(_ViewTeam, FromDate, ToDate);

            if (zTable.Rows.Count > 0)
            {
                // Thêm 1 cột để sắp xếp
                //zTable.Columns.Add("RankID");
                //for (int i = 0; i < zTable.Rows.Count; i++)
                //{
                //    DataRow row = zTable.Rows[i];
                //    string EmployeeID = row[3].ToString();
                //    zTable.Rows[i]["RankID"] = ConvertIDRank(EmployeeID);
                //}
                //DataView dv = zTable.DefaultView;
                //dv.Sort = "Rank ASC , RankID ASC";
                //zTable = dv.ToTable();
                //zTable.Columns.Remove("RankID");
                //--sắp xếp xong xóa cột sắp xếp đi

                this.Invoke(new MethodInvoker(delegate ()
                {
                    DataGridViewRow zGvRow;
                    int NoGroup = 1;
                    #region [GROUP]
                    int TeamKey = zTable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = zTable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = zTable.Select("TeamKey='" + TeamKey+"'");
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[0];
                        zGvRow.Tag = TeamKey;
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    #endregion

                    for (int i = 0; i < zTable.Rows.Count; i++)
                    {
                        DataRow r = zTable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = zTable.Select("TeamKey='" + TeamKey+"'");
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, i + NoGroup);
                    }

                    zGvRow = GVData.Rows[GVData.Rows.Count - 1];
                    TotalRow(zGvRow, zTable, GVData.Rows.Count - 1);
                }));
            }
        }

        private void btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cbo_TeamID_Search.SelectedValue.ToInt() != 0)
            {
                string []s= cbo_TeamID_Search.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
                

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Bảng_Thông_Tin_Tổng_Hợp_Lương_Khoán_Năng_Suất" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    if (GVData.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        foreach (DataGridViewColumn col in GVData.Columns)
                        {
                            dt.Columns.Add(col.HeaderText);
                        }

                        foreach (DataGridViewRow row in GVData.Rows)
                        {
                            DataRow dRow = dt.NewRow();
                            foreach (DataGridViewCell cell in row.Cells)
                            {
                                dRow[cell.ColumnIndex] = cell.Value;
                            }
                            dt.Rows.Add(dRow);
                        }
                        string Message = ExportTableToExcel(dt, Path);
                        if (Message != "OK")
                        {
                            Utils.TNMessageBox("Thông báo", Message, 4);
                        }
                        else
                        {
                            string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                             Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                            Process.Start(Path);
                        }
                    }
                    else
                    {
                        Utils.TNMessageBox("Thông báo", "Không tìm thấy dữ liệu!", 4);
                    }
                }

            }
        }

        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("LKNS");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    //worksheet.Cells.AutoFilter = true;  //thêm lọc khi xuất excel
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 15;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 15;
                    worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        for (int j = 4; j <= zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            string zResult = "";
            string temp = ID.Replace("A", "");
            string s = "";
            //Thiếu bao nhieu số kí tự thì thêm vào bấy nhiêu cho đủ chuỗi VD: 11--->0011, A11--->1000011
            for (int i = 0; i < 4 - temp.Trim().Length; i++)
            {
                s += "0";
            }
            if (ID.Substring(0, 1) == "A")
            {
                zResult = "100" + s + temp;
            }
            else
            {
                zResult = s + temp;
            }

            return zResult;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}