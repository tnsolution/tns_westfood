﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Production_Record : Form
    {
        private Order_Object _Order_Obj;
        private string _Message_Check = "";
        private string _OrderKey = "";
        private int _WorkStatus = 0;
        private DataTable zTable;
        private DateTime _OrderDate;
        private string _RoleID = "";
        private float _SumQuantity = 0;
        public Frm_Production_Record()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Employee, true);
            Utils.DoubleBuffered(GV_Sum, true);
            Utils.DrawGVStyle(ref GV_Employee);
            Utils.DrawGVStyle(ref GV_Sum);
            Utils.DrawLVStyle(ref LVData);

            //-----dte_OrderDate
            dte_FromDate.CustomFormat = "dd/MM/yyyy";
            dte_ToDate.CustomFormat = "dd/MM/yyyy";
            dte_ToDate.Validated += Dte_ToDate_Validated;
            cbo_Compare.SelectedIndexChanged += Cbo_Compare_SelectedIndexChanged;
            //----- DataGridView
            GV_Employee_Layout();
            GV_Sum_Layout();

            //----- ListView
            InitLV_Layout(LVData);
            LVData.Click += LVData_Click;
            LVData.ItemActivate += LVData_ItemActivate;
            //----- Button
            btn_Open_Order.Click += Btn_Open_Order_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Copy.Click += Btn_Copy_Click;
            btn_Import.Click += Btn_Import_Click;
            //----- ListView Product
            Layout_Product(LV_Product);
            txt_Product.KeyUp += Txt_Product_KeyUp;
            LV_Product.ItemSelectionChanged += LV_Product_ItemSelectionChanged;
            LV_Product.ItemActivate += LV_Product_ItemActivate;
            txt_Product.Leave += Txt_Product_Leave;
            LV_Product.KeyDown += LV_Product_KeyDown;
            //----- ListViewStage
            Layout_Stage(LV_Stage);
            txt_Stage.KeyUp += Txt_Stage_KeyUp;
            LV_Stage.ItemSelectionChanged += LV_Stage_ItemSelectionChanged;
            LV_Stage.ItemActivate += LV_Stage_ItemActivate;
            txt_Stage.Leave += Txt_Stage_Leave;
            LV_Stage.KeyDown += LV_Stage_KeyDown;
            //----- Up-Down
            btn_Up.Click += Btn_Up_Click;
            btn_Down.Click += Btn_Down_Click;
            btn_Up.Visible = false;
            btn_Down.Visible = true;
            Utils.SizeLastColumn_LV(LVData);
            Utils.SizeLastColumn_LV(LV_Product);
            Utils.SizeLastColumn_LV(LV_Stage);
        }

        private void Dte_ToDate_Validated(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Compare, Order_Data.List_Compare(dte_FromDate.Value, dte_ToDate.Value), true, 0, 1);
        }

        private void Cbo_Compare_SelectedIndexChanged(object sender, EventArgs e)
        {
            Search_With_Comparee();
        }

        private void Btn_Down_Click(object sender, EventArgs e)
        {
            Panel_GV.Visible = false;
            Panel_LV.Dock = DockStyle.Fill;
            btn_Up.Visible = true;
            btn_Down.Visible = false;

        }
        private void Btn_Up_Click(object sender, EventArgs e)
        {
            Panel_GV.Visible = true;
            Panel_LV.Dock = DockStyle.Fill;
            btn_Up.Visible = false;
            btn_Down.Visible = true;
        }
        private void Frm_Record_Productivity_Load(object sender, EventArgs e)
        {
            DataRow zDelete = User_Data.GetUserDel(SessionUser.UserLogin.Key, _RoleID).Rows[0];
            if (Convert.ToInt32(zDelete[0]) > 0)
            {
                btn_Del.Visible = true;
            }
            else
            {
                btn_Del.Visible = false;
            }
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
            Load_ComboBox();
            LV_Product.Hide();
            LV_Stage.Hide();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            DataTable zTable = Order_Data.List(0, dte_FromDate.Value, dte_ToDate.Value, string.Empty, 0, 0);
            Load_ListView(zTable);
            LoadData();
        }
        private void Load_ComboBox()
        {
            //DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            //DateTime zToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
            LoadDataToToolbox.ComboBoxData(cbo_Team, "SELECT TeamKey, TeamName, TeamID FROM SYS_Team WHERE RecordStatus < 99 ORDER BY TeamName", "---Tất cả---");
            LoadDataToToolbox.ComboBoxData(cbo_Category, "SELECT AutoKey,RateName FROM FTR_Category WHERE RecordStatus < 99", "---Chọn Hệ Số---");
            LoadDataToToolbox.ComboBoxData(cbo_Compare, Order_Data.List_Compare(dte_FromDate.Value, dte_ToDate.Value), true, 0, 1);
        }

        #region ----- Panel Left -----

        #region ----- Desgin Layout ------
        private void InitLV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày Thực Hiện";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn Hàng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình Trạng";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Sản Phẩm";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Công Đoạn";
            colHead.Width = 300;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn Giá";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "SL Thực Tế";
            colHead.Width = 90;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Hệ Số";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ghi Chú";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void Search_With_Comparee()
        {
            _SumQuantity = 0;
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();
            In_Table = Order_Data.List_Compare_Productivity(dte_FromDate.Value, dte_ToDate.Value, cbo_Compare.SelectedValue.ToString());

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["OrderKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                DateTime zOrderDate = DateTime.Parse(nRow["OrderDate"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zOrderDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                if (nRow["WorkStatus"].ToString().Trim() == "0")
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvi.ForeColor = Color.Navy;
                    lvsi.Text = "Đang thực hiện";
                    lvi.SubItems.Add(lvsi);
                }

                if (nRow["WorkStatus"].ToString().Trim() == "1")
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvi.ForeColor = Color.Gray;
                    lvsi.Text = "Đã thực hiện";
                    lvi.SubItems.Add(lvsi);
                }

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["StageName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                if (nRow["Price"].ToString() != "")
                {
                    double zPrice = double.Parse(nRow["Price"].ToString().Trim());
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = Math.Round(zPrice, 1).ToString();
                    lvi.SubItems.Add(lvsi);
                }
                float zQuantityReality = float.Parse(nRow["QuantityReality"].ToString());
                _SumQuantity += zQuantityReality;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zQuantityReality.ToString();
                lvi.SubItems.Add(lvsi);

                float zRate = 0;
                if (nRow["Rate"].ToString() == null)
                {
                    zRate = 0;
                }
                else
                {
                    zRate = float.Parse(nRow["Rate"].ToString());
                }
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRate.ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Note"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["WorkStatus"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            RowSum(LVData);//dong tổng
            this.Cursor = Cursors.Default;
        }
        private void RowSum(ListView LV)
        {
            ;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            //Dòng tổng
            lvi = new ListViewItem();
            lvi.Text = "";
            lvi.Tag = null;
            lvi.BackColor = Color.White;
            lvi.ForeColor = Color.Navy;
            lvi.Font = new Font("Tahoma", 10, FontStyle.Bold);

            for (int i = 0; i < 10; i++)
            {
                if (i == 1)
                {

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "TỔNG";
                    lvi.SubItems.Add(lvsi);
                }
                if (i == 5)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = _SumQuantity.Ton0String();
                    lvi.SubItems.Add(lvsi);
                }
                else
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "";
                    lvi.SubItems.Add(lvsi);
                }
            }
            LV.Items.Add(lvi);
        }
        private void Load_ListView(DataTable In_Table)
        {
            _SumQuantity = 0;
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["OrderKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                DateTime zOrderDate = DateTime.Parse(nRow["OrderDate"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zOrderDate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                if (nRow["WorkStatus"].ToString().Trim() == "0")
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvi.ForeColor = Color.Navy;
                    lvsi.Text = "Đang thực hiện";
                    lvi.SubItems.Add(lvsi);
                }

                if (nRow["WorkStatus"].ToString().Trim() == "1")
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvi.ForeColor = Color.Gray;
                    lvsi.Text = "Đã thực hiện";
                    lvi.SubItems.Add(lvsi);
                }

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["StageName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                if (nRow["Price"].ToString() != "")
                {
                    double zPrice = double.Parse(nRow["Price"].ToString().Trim());
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = Math.Round(zPrice, 1).ToString();
                    lvi.SubItems.Add(lvsi);
                }
                float zQuantityReality = float.Parse(nRow["QuantityReality"].ToString());
                _SumQuantity += zQuantityReality;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zQuantityReality.ToString();
                lvi.SubItems.Add(lvsi);

                float zRate = 0;
                if (nRow["Rate"].ToString() == null)
                {
                    zRate = 0;
                }
                else
                {
                    zRate = float.Parse(nRow["Rate"].ToString());
                }
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zRate.ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Note"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["WorkStatus"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            RowSum(LVData);//dong tổng
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region ----- Desgin Layout Product -----
        private void Layout_Product(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Sản Phẩm";
            colHead.Width = -1;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LoadDataProduct()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Product;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Product_Data.SearchProduct(txt_Product.Text);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["ProductKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();

                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }
            LV_Product.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            LV_Product.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            this.Cursor = Cursors.Default;
        }
        private void Txt_Product_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LV_Product.Focus();
                if (LV_Product.Items.Count > 0)
                    LV_Product.Items[0].Selected = true;
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txt_Product.Text.Length > 0)
                    {
                        LV_Product.Visible = true;
                        LoadDataProduct();
                    }
                    else
                        LV_Product.Visible = false;
                }
            }
        }
        private void LV_Product_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            txt_Product.Tag = LV_Product.Items[e.ItemIndex].Tag.ToString();
            txt_Product.Text = LV_Product.Items[e.ItemIndex].SubItems[1].Text.Trim();
        }
        private void LV_Product_ItemActivate(object sender, EventArgs e)
        {
            txt_Product.Tag = LV_Product.SelectedItems[0].Tag;
            LV_Product.Visible = false;
            txt_Product.Focus();
        }
        private void Txt_Product_Leave(object sender, EventArgs e)
        {
            if (!LV_Product.Focused)
                LV_Product.Visible = false;
        }
        private void LV_Product_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_Product.Tag = LV_Product.SelectedItems[0].Tag;
                LV_Product.Visible = false;
            }
        }
        #endregion

        #region ----- Design Layout Stage -----
        private void Layout_Stage(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Công Đoạn";
            colHead.Width = 220;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LoadData_Stage()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Stage;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Product_Stages_Data.List(txt_Stage.Text);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["StageKey"].ToString();
                lvi.BackColor = Color.White;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["StageName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }
            LV_Stage.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
            LV_Stage.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            this.Cursor = Cursors.Default;
        }
        private void LV_Stage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_Stage.Tag = LV_Stage.SelectedItems[0].Tag;
                LV_Stage.Visible = false;
            }
        }
        private void Txt_Stage_Leave(object sender, EventArgs e)
        {
            if (!LV_Stage.Focused)
                LV_Stage.Visible = false;
        }
        private void LV_Stage_ItemActivate(object sender, EventArgs e)
        {
            txt_Stage.Tag = LV_Stage.SelectedItems[0].Tag;
            LV_Stage.Visible = false;
            txt_Stage.Focus();
        }
        private void LV_Stage_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            txt_Stage.Tag = LV_Stage.Items[e.ItemIndex].Tag.ToString();
            txt_Stage.Text = LV_Stage.Items[e.ItemIndex].SubItems[1].Text.Trim();
        }
        private void Txt_Stage_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LV_Stage.Focus();
                if (LV_Stage.Items.Count > 0)
                    LV_Stage.Items[0].Selected = true;
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txt_Stage.Text.Length > 0)
                    {
                        LV_Stage.Visible = true;
                        LoadData_Stage();
                    }
                    else
                        LV_Stage.Visible = false;
                }
            }
        }
        #endregion

        #region ----- Process Data -----
        private void LoadData()
        {
            _Order_Obj = new Order_Object(_OrderKey);
            Load_GridView_Employee();
        }
        #endregion

        #region ----- Process Event -----
        private void LVData_Click(object sender, EventArgs e)
        {
            //for (int i = 0; i < LVData.Items.Count; i++)
            //{
            //    if (LVData.Items[i].Selected == true)
            //    {
            //        LVData.Items[i].BackColor = Color.LightBlue; // highlighted item
            //    }
            //    else
            //    {
            //        LVData.Items[i].BackColor = SystemColors.Window; // normal item
            //    }
            //}
            if (LVData.SelectedItems[0].Tag != null)
            {
                _OrderKey = LVData.SelectedItems[0].Tag.ToString();
                _WorkStatus = int.Parse(LVData.SelectedItems[0].SubItems[10].Text);
                _OrderDate = DateTime.Parse(LVData.SelectedItems[0].SubItems[1].Text);
                LoadData();
                GV_Sum_LoadData();
            }
        }
        private string Check_Befor_Order()
        {
            _Message_Check = "";
            if (dte_FromDate.Value == DateTime.MinValue)
                _Message_Check = "Vui lòng kiểm tra ngày tháng";
            if (cbo_Team.SelectedValue == null)
                _Message_Check = "Vui lòng chọn nhóm";
            return _Message_Check;
        }
        private void Btn_Open_Order_Click(object sender, EventArgs e)
        {
            if (Check_Befor_Order().Length > 0)
            {
                MessageBox.Show(_Message_Check, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _OrderKey = "";
                _WorkStatus = 0;
                Frm_Order frm = new Frm_Order();
                frm.OrderKey = _OrderKey;
                frm.OrderDate = dte_FromDate.Value;
                frm.TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());
                frm.RoleID = _RoleID;
                frm.Show();
                //if (frm.IsDisposed == true)
                //{
                //    Load_ListView();
                //}
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            _OrderKey = "";
            _Order_Obj = new Order_Object();
            string zProductKey = "";
            int zStageKey = 0;
            if (txt_Product.Tag == null || txt_Product.Text.Length == 0)
                zProductKey = "";
            else
                zProductKey = txt_Product.Tag.ToString();

            if (txt_Stage.Tag == null || txt_Stage.Text.Length == 0)
                zStageKey = 0;
            else
                zStageKey = int.Parse(txt_Stage.Tag.ToString());

            DataTable zTable = Order_Data.List(int.Parse(cbo_Team.SelectedValue.ToString()), dte_FromDate.Value, dte_ToDate.Value, zProductKey, zStageKey, int.Parse(cbo_Category.SelectedValue.ToString()));
            Load_ListView(zTable);
            LoadData();
        }
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            if (LVData.SelectedItems[0].Tag != null)
            {
                Frm_Order frm = new Frm_Order();
                frm.OrderKey = _OrderKey;
                frm.WorkStatus = _WorkStatus;
                frm.OrderDate = _OrderDate;
                frm.TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());
                frm.RoleID = _RoleID;
                frm.ShowDialog();
                //Load_ListView();
            }

        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zOrderID = "";
            for (int i = 0; i < LVData.Items.Count; i++)
            {
                if (LVData.Items[i].Selected)
                {
                    zOrderID += LVData.Items[i].SubItems[2].Text + ",";
                }
            }
            string zMessage = "";
            DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin phiếu " + zOrderID.Substring(0, zOrderID.Length - 1) + "  này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                int index = 0;
                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    if (LVData.Items[i].Selected)
                    {
                        _OrderKey = LVData.Items[i].Tag.ToString();
                        _Order_Obj = new Order_Object();
                        _Order_Obj.Key = _OrderKey;
                        _Order_Obj.Delete_Object();
                        Order_Money_Info zInfo = new Order_Money_Info();
                        zInfo.OrderKey = _OrderKey;
                        zInfo.Delete_Order();
                        zMessage += TN_Message.Show(_Order_Obj.Message);
                        index = i;
                    }
                }
                if (zMessage.Length == 0)
                {
                    MessageBox.Show("Xóa Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LVData.Items[index].Remove();
                    //Load_ListView();
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void LVData_KeyDown(object sender, KeyEventArgs e)
        {
            string zOrderID = "";
            for (int i = 0; i < LVData.Items.Count; i++)
            {
                if (LVData.Items[i].Selected)
                {
                    zOrderID += LVData.Items[i].SubItems[2].Text + ",";
                }
            }
            if (e.KeyData == Keys.Delete)
            {
                string zMessage = "";
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin phiếu " + zOrderID.Substring(0, zOrderID.Length - 1) + "  này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    int index = 0;
                    for (int i = 0; i < LVData.Items.Count; i++)
                    {
                        if (LVData.Items[i].Selected)
                        {
                            _OrderKey = LVData.Items[i].Tag.ToString();
                            _Order_Obj = new Order_Object();
                            _Order_Obj.Key = _OrderKey;
                            _Order_Obj.Delete_Object();
                            Order_Money_Info zInfo = new Order_Money_Info();
                            zInfo.OrderKey = _OrderKey;
                            zInfo.Delete_Order();
                            zMessage += TN_Message.Show(_Order_Obj.Message); 
                            index = i;
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        MessageBox.Show("Xóa Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LVData.Items[index].Remove();
                        //Load_ListView();
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        #endregion

        #endregion

        #region ----- Panel Right -----

        #region ----- Design Layout -----        
        private void GV_Employee_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;

            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("Name", "Tên Nhân Viên");
            GV_Employee.Columns.Add("Basket", "Số Rổ");
            GV_Employee.Columns.Add("Kg", "Số Lượng");
            GV_Employee.Columns.Add("Time", "Thời Gian");
            GV_Employee.Columns.Add("Borrow", "Mượn Người");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Check_Private";
            zColumn.HeaderText = "Ăn Riêng";
            GV_Employee.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Check";
            zColumn.HeaderText = "Ăn Chung";
            GV_Employee.Columns.Add(zColumn);

            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Employee.Columns["EmployeeID"].Width = 70;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].ReadOnly = true;

            GV_Employee.Columns["Name"].Width = 250;
            GV_Employee.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["Name"].ReadOnly = true;
            GV_Employee.Columns["Name"].Frozen = true;

            GV_Employee.Columns["Basket"].Width = 120;
            GV_Employee.Columns["Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Basket"].ReadOnly = true;

            GV_Employee.Columns["Kg"].Width = 120;
            GV_Employee.Columns["Kg"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Kg"].ReadOnly = true;

            GV_Employee.Columns["Time"].Width = 120;
            GV_Employee.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee.Columns["Time"].ReadOnly = true;

            GV_Employee.Columns["Borrow"].Width = 120;
            GV_Employee.Columns["Borrow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["Borrow"].ReadOnly = true;
            GV_Employee.Columns["Borrow"].ReadOnly = true;

            GV_Employee.Columns["Check_Private"].Width = 120;
            GV_Employee.Columns["Check_Private"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["Check_Private"].ReadOnly = true;

            GV_Employee.Columns["Check"].Width = 120;
            GV_Employee.Columns["Check"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["Check"].ReadOnly = true;
        }
        private void Load_GridView_Employee()
        {

            GV_Employee.Rows.Clear();
            for (int i = 0; i < _Order_Obj.List_Employee.Count; i++)
            {
                GV_Employee.Rows.Add();
                Order_Employee_Info zOrder_Employee = (Order_Employee_Info)_Order_Obj.List_Employee[i];
                GV_Employee.Rows[i].Tag = zOrder_Employee;
                GV_Employee.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Employee.Rows[i].Cells["No"].Tag = zOrder_Employee.EmployeeKey;
                GV_Employee.Rows[i].Cells["EmployeeID"].Value = zOrder_Employee.EmployeeID;
                GV_Employee.Rows[i].Cells["Name"].Value = zOrder_Employee.FullName;
                GV_Employee.Rows[i].Cells["Basket"].Value = zOrder_Employee.Basket;
                GV_Employee.Rows[i].Cells["Kg"].Value = zOrder_Employee.Kilogram;
                GV_Employee.Rows[i].Cells["Time"].Value = zOrder_Employee.Time;
                if (zOrder_Employee.Borrow == 1)
                {
                    GV_Employee.Rows[i].Cells["Borrow"].Value = "X";
                }
                if (zOrder_Employee.Share == 1)
                {
                    GV_Employee.Rows[i].Cells["Check"].Value = true;
                }
                if (zOrder_Employee.Private == 1)
                {
                    GV_Employee.Rows[i].Cells["Check_Private"].Value = true;
                }
            }
        }
        private void GV_Sum_Layout()
        {
            // Setup Column 
            GV_Sum.Columns.Add("SUM", "TỔNG");
            GV_Sum.Columns.Add("Sum_Basket", "TỔNG SỐ RỔ");
            GV_Sum.Columns.Add("Sum_Kg", "TỔNG SỐ KG");
            GV_Sum.Columns.Add("Sum_Time", "TỔNG THỜI GIAN");

            GV_Sum.Columns["SUM"].Width = 360;
            GV_Sum.Columns["SUM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Sum.Columns["SUM"].ReadOnly = true;

            GV_Sum.Columns["Sum_Basket"].Width = 120;
            GV_Sum.Columns["Sum_Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Sum.Columns["Sum_Basket"].ReadOnly = true;

            GV_Sum.Columns["Sum_Kg"].Width = 120;
            GV_Sum.Columns["Sum_Kg"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Sum.Columns["Sum_Kg"].ReadOnly = true;

            GV_Sum.Columns["Sum_Time"].Width = 120;
            GV_Sum.Columns["Sum_Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Sum.Columns["Sum_Time"].ReadOnly = true;
        }
        private void GV_Sum_LoadData()
        {
            GV_Sum.Rows.Clear();
            zTable = Order_Employee_Data.Sum_Product(_OrderKey);
            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GV_Sum.Rows.Add();
                DataGridViewRow nRowView = GV_Sum.Rows[i];
                nRowView.Cells["Sum_Basket"].Value = nRow["Basket"].ToString();
                nRowView.Cells["Sum_Kg"].Value = nRow["Kilogram"].ToString();
                nRowView.Cells["Sum_Time"].Value = nRow["Time"].ToString();
                i++;
            }
        }
        #endregion

        #region [Process Data]
        //---- Order
        private DateTime _OrderDate_Copy;
        private int _TeamKey_Copy;
        private string _ProductKey_Copy;
        private string _OrderIDFollow_Copy;
        private string _ProductID_Copy;
        private string _ProductName_Copy;
        private float _QuantityDocument_Copy;
        private float _QuantityReality_Copy;
        private float _QuantityLoss_Copy;
        private int _UnitKey_Copy;
        private string _UnitName_Copy;
        private int _Category_Stage;
        private float _PercentFinishCopy = 0;
        //---- Order_Rate
        private int _Category;
        //---- Order_Employee
        private string _EmployeeKey;
        private string _CardID;
        private float _Basket;
        private float _Kilogram;
        private float _Time;
        private int _Private;
        private int _Share;
        private int _Borrow;
        private Order_Info _Order;
        private Order_Rate_Info _Order_Rate;
        private Order_Employee_Info _Order_Employee;
        private DataTable _Table;
        private string _Message_Order = "";
        private string _Message_Rate = "";
        private string _Message_GV = "";

        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }

        private void LoadData_Copy()
        {
            _Order = new Order_Info(_OrderKey);
            _OrderDate_Copy = _Order.OrderDate;
            _ProductKey_Copy = _Order.ProductKey;
            _TeamKey_Copy = _Order.TeamKey;
            _Category_Stage = _Order.Category_Stage;
            _OrderIDFollow_Copy = _Order.OrderIDFollow;
            _ProductID_Copy = _Order.ProductID;
            _ProductName_Copy = _Order.ProductName;
            _QuantityDocument_Copy = _Order.QuantityDocument;
            _QuantityReality_Copy = _Order.QuantityReality;
            _QuantityLoss_Copy = _Order.QuantityReality;
            _UnitKey_Copy = _Order.UnitKey;
            _UnitName_Copy = _Order.UnitName;
            _PercentFinishCopy = _Order.PercentFinish;
            _Order_Rate = new Order_Rate_Info(_OrderKey);
            _Category = _Order_Rate.CategoryKey;
        }
        private void Save()
        {
            _Order = new Order_Info();
            _Order.OrderDate = _OrderDate_Copy;
            _Order.TeamKey = _TeamKey_Copy;
            _Order.OrderIDFollow = _OrderIDFollow_Copy;
            _Order.Category_Stage = _Category_Stage;
            _Order.ProductKey = _ProductKey_Copy;
            _Order.ProductID = _ProductID_Copy;
            _Order.ProductName = _ProductName_Copy;
            _Order.QuantityDocument = _QuantityDocument_Copy;
            _Order.QuantityReality = _QuantityReality_Copy;
            _Order.QuantityLoss = _QuantityLoss_Copy;
            _Order.UnitKey = _UnitKey_Copy;
            _Order.UnitName = _UnitName_Copy;
            _Order.PercentFinish = _PercentFinishCopy;
            _Order.Create();
            _Message_Order = _Order.Message.Substring(0, 2);
            _Order_Rate = new Order_Rate_Info();
            _Order_Rate.OrderKey = _Order.Key;
            _Order_Rate.CategoryKey = _Category;
            _Order_Rate.Create();
            _Message_Rate = _Order_Rate.Message.Substring(0, 2);
        }
        private void Load_GV()
        {
            string zMessage = "";
            _Table = Order_Employee_Data.List(_OrderKey);
            int i = 0;
            foreach (DataRow nRow in _Table.Rows)
            {

                _EmployeeKey = nRow["EmployeeKey"].ToString();
                _CardID = nRow["CardID"].ToString().Trim();
                _Basket = float.Parse(nRow["Basket"].ToString().Trim());
                _Kilogram = float.Parse(nRow["Kilogram"].ToString().Trim());
                _Time = float.Parse(nRow["Time"].ToString().Trim());
                _Share = int.Parse(nRow["Share"].ToString().Trim());
                _Private = int.Parse(nRow["Private"].ToString().Trim());
                _Borrow = int.Parse(nRow["Borrow"].ToString().Trim());

                _Order_Employee = new Order_Employee_Info();
                _Order_Employee.OrderKey = _Order.Key;
                _Order_Employee.EmployeeKey = _EmployeeKey;
                _Order_Employee.EmployeeID = _CardID;
                _Order_Employee.Kilogram = _Kilogram;
                _Order_Employee.Time = _Time;
                _Order_Employee.Basket = _Basket;
                _Order_Employee.Borrow = _Borrow;
                _Order_Employee.Share = _Share;
                _Order_Employee.Private = _Private;
                _Order_Employee.Create();
                zMessage += _Order_Employee.Message.Substring(0, 2);
                i++;
            }
            _Message_GV = zMessage.Substring(0, 2);
        }
        #endregion

        #region [Process Event]
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            if (_OrderKey.Length == 0)
            {
                MessageBox.Show("Vui lòng chọn đơn hàng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dlr = MessageBox.Show("Bạn muốn copy đơn hàng này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    LoadData_Copy();
                    Save();
                    Load_GV();

                    string zProductKey = "";
                    int zStageKey = 0;
                    if (txt_Product.Tag == null || txt_Product.Text.Length == 0)
                        zProductKey = "";
                    else
                        zProductKey = txt_Product.Tag.ToString();

                    if (txt_Stage.Tag == null || txt_Stage.Text.Length == 0)
                        zStageKey = 0;
                    else
                        zStageKey = int.Parse(txt_Stage.Tag.ToString());
                    DataTable zTable = Order_Data.List(int.Parse(cbo_Team.SelectedValue.ToString()), dte_FromDate.Value, dte_ToDate.Value, zProductKey, zStageKey, int.Parse(cbo_Category.SelectedValue.ToString()));
                    Load_ListView(zTable);
                    if (_Message_GV == "11" && _Message_Order == "11" && _Message_GV == "11")
                    {
                        MessageBox.Show("Copy thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_Record frm = new Frm_Import_Record();
            frm.Show();
        }
        #endregion

        #endregion
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
