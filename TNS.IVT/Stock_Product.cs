﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Stock_Product
    {
        public static DataTable StockProduct(DateTime FromDate, DateTime ToDate, int WarehouseKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);


            DataTable zTable = new DataTable();
            string zSQL = @"SELECT  A.ProductID AS ProductID, A.ProductName AS   ProductName, 
 B.Unitname AS Unitname,
dbo.IVT_Get_BeginQuantityReality(A.ProductKey, @ToDate,@WarehouseKey) AS BeginStock,
dbo.IVT_Get_InQuantityReality(A.ProductKey, @FromDate, @ToDate,@WarehouseKey) AS InStock,
dbo.IVT_Get_OutQuantityReality(A.ProductKey, @FromDate, @ToDate,@WarehouseKey) AS OutStock,
dbo.IVT_Get_StockQuantityReality(A.ProductKey, @FromDate, @ToDate,@WarehouseKey) AS EndStock
FROM IVT_Product A 
LEFT JOIN IVT_Product_Unit B ON A.BasicUnit = B.UnitKey
WHERE A.RecordStatus <> 99 AND A.Parent!='0'
 ORDER BY ProductID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable StockProductV2(DateTime FromDate, DateTime ToDate, int WarehouseKey, string ProductID)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zFilter = "";
            if (ProductID.Trim().Length > 0)
            {
                zFilter = " AND A.ProductID =@ProductID ";
            }

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @WarehouseKey int =1;

--Chuyển thời gian tìm kím lùi 1 ngày
declare @temp NVARCHAR(50)=  LEFT(CONVERT(VARCHAR,@FromDate,120),10)+' 23:59:59'
declare @ToDateTruoc datetime = DATEADD(day, -1, CONVERT(datetime,@temp,120))
CREATE TABLE #TONKHO
(
ProductKey NVARCHAR(50),
ProductID NVARCHAR(250),
ProductName NVARCHAR(250),
UnitName  NVARCHAR(50),
BeginStock FLOAT,
InStock FLOAT,
OutStock FLOAT,
)
INSERT INTO #TONKHO
SELECT CAST(A.ProductKey AS NVARCHAR(50)) AS ProductKey, A.ProductID AS ProductID, A.ProductName AS   ProductName, 
 B.Unitname AS Unitname,
 ---Đầu kì
ISNULL(dbo.IVT_Get_InQuantityReality(A.ProductKey, '2010-01-01 00:00:00', @ToDateTruoc,@WarehouseKey),0) -
ISNULL(dbo.IVT_Get_OutQuantityReality(A.ProductKey,'2010-01-01 00:00:00', @ToDateTruoc,@WarehouseKey),0) 
AS BeginStock,
--Nhập trong kì
ISNULL(dbo.IVT_Get_InQuantityReality(A.ProductKey, @FromDate, @ToDate,@WarehouseKey),0) AS InStock,
--Xuất trong kì
ISNULL(dbo.IVT_Get_OutQuantityReality(A.ProductKey,@FromDate, @ToDate,@WarehouseKey),0) AS OutStock
FROM IVT_Product A 
LEFT JOIN IVT_Product_Unit B ON A.BasicUnit = B.UnitKey
WHERE A.RecordStatus <> 99 AND A.Parent!='0' @Paramater
ORDER BY A.Rank, ProductID

SELECT ProductID,ProductName,UnitName,BeginStock,InStock,OutStock,(BeginStock+InStock-OutStock) AS EndStock FROM #TONKHO 
WHERE ( BeginStock > 0 OR InStock > 0 OR OutStock >0)

DROP TABLE #TONKHO";

            zSQL = zSQL.Replace("@Paramater", zFilter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable BANGKENHAPKHO(DateTime FromDate, DateTime ToDate, int WarehouseKey, string ProductID,string Parent,int Type)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zFilter = "";
            if (WarehouseKey > 0)
            {
                zFilter += " AND B.WarehouseKey =@WarehouseKey ";
            }
            if (ProductID.Trim().Length > 0)
            {
                zFilter += " AND C.ProductID =@ProductID ";
            }
            if (Parent.Length>2)
            {
                zFilter += " AND C.Parent = @Parent ";
            }
            if (Type > 0)
            {
                zFilter += " AND A.ProductType =@Type ";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @WarehouseKey int =1;

SELECT B.OrderDate,B.OrderID,[dbo].[Fn_TenLoaiKhoNoi_KhacHang](B.WarehouseLocal),
CASE  WHEN B.WarehouseLocal>0 THEN [dbo].[Fn_WarehouseName](B.WarehouseLocal)
ELSE [dbo].[Fn_GetCustomerName](B.CustomerKey)--Nếu là tư khách hàng phải hiển thị khách hàng 
END,
B.OrderDescription,
[dbo].[Fn_LayTenSanPhamTuBangProduct](C.Parent),
C.ProductID,C.ProductName,[dbo].[Fn_GetNameProductType](A.ProductType),A.UnitName,
A.QuantityDocument,A.QuantityReality,(A.QuantityDocument-A.QuantityReality),A.OrderIDFollow,A.NoteStatus FROM [dbo].[IVT_Stock_Input_Products] A
LEFT JOIN [dbo].[IVT_Stock_Input] B ON B.OrderKey=A.OrderKey
LEFT JOIN IVT_Product C ON CAST(C.ProductKey AS NVARCHAR(50))=A.ProductKey
WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99
AND B.OrderDate BETWEEN @FromDate AND @ToDate @Paramater 
ORDER BY B.OrderDate
";

            zSQL = zSQL.Replace("@Paramater", zFilter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable BANGKEXUATKHO(DateTime FromDate, DateTime ToDate, int WarehouseKey, string ProductID, string Parent, int Type)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zFilter = "";
            if (WarehouseKey > 0)
            {
                zFilter += " AND B.WarehouseKey =@WarehouseKey ";
            }
            if (ProductID.Trim().Length > 0)
            {
                zFilter += " AND C.ProductID =@ProductID ";
            }
            if (Parent.Length > 2)
            {
                zFilter += " AND C.Parent = @Parent ";
            }
            if (Type > 0)
            {
                zFilter += " AND A.ProductType =@Type ";
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @WarehouseKey int =1;

SELECT B.OrderDate,B.OrderID,[dbo].[Fn_TenLoaiKhoNoi_KhacHang](B.WarehouseLocal),
CASE  WHEN B.WarehouseLocal>0 THEN [dbo].[Fn_WarehouseName](B.WarehouseLocal)
ELSE [dbo].[Fn_GetCustomerName](B.CustomerKey)--Nếu là tư khách hàng phải hiển thị khách hàng 
END,
B.OrderDescription,
[dbo].[Fn_LayTenSanPhamTuBangProduct](C.Parent),
C.ProductID,C.ProductName,[dbo].[Fn_GetNameProductType](A.ProductType),A.UnitName,
A.QuantityDocument,A.QuantityReality,(A.QuantityDocument-A.QuantityReality),A.OrderIDFollow,A.NoteStatus FROM [dbo].[IVT_Stock_Output_Products] A
LEFT JOIN [dbo].[IVT_Stock_Output] B ON B.OrderKey=A.OrderKey
LEFT JOIN IVT_Product C ON CAST(C.ProductKey AS NVARCHAR(50))=A.ProductKey
WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99
AND B.OrderDate BETWEEN @FromDate AND @ToDate @Paramater 
ORDER BY  B.OrderDate ";

            zSQL = zSQL.Replace("@Paramater", zFilter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        #region[Báo cáo thẻ kho]
        public static DataTable THEKHO(DateTime FromDate, DateTime ToDate, int WarehouseKey, string ProductID)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
           WITH #K AS (
	SELECT B.OrderDate,
	B.OrderID,
    N'Nhập kho' AS PHIEU,
	[dbo].[Fn_WarehouseName](B.WarehouseLocal) AS  NHAPXUAT,
	[dbo].[Fn_TenLoaiKhoNoi_KhacHang](B.WarehouseLocal) AS LOAI,
	B.OrderDescription,
	0.0 AS DAUKI,
	ISNULL(ROUND(A.QuantityReality,1),0) AS NHAP,0.0 AS XUAT,
	0.0 AS CUOIKI
	FROM [dbo].[IVT_Stock_Input_Products] A
	LEFT JOIN [dbo].[IVT_Stock_Input] B ON B.OrderKey=A.OrderKey
	LEFT JOIN IVT_Product C ON CAST(C.ProductKey AS NVARCHAR(50))=A.ProductKey
	WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99
	AND B.OrderDate BETWEEN @FromDate AND @ToDate AND C.ProductID= @ProductID AND B.WarehouseKey =@WarehouseKey 
UNION ALL
	SELECT B.OrderDate,
	B.OrderID,
	N'Xuất kho' AS PHIEU,
	[dbo].[Fn_WarehouseName](B.WarehouseLocal) AS  NHAPXUAT,
	[dbo].[Fn_TenLoaiKhoNoi_KhacHang](B.WarehouseLocal) AS LOAI,
	B.OrderDescription,
	0.0 AS DAUKI,
	0.0 AS NHAP,ISNULL(ROUND(A.QuantityReality,1),0) AS XUAT,
	0.0 AS CUOIKI
	FROM [dbo].[IVT_Stock_Output_Products] A
	LEFT JOIN [dbo].[IVT_Stock_Output] B ON B.OrderKey=A.OrderKey
	LEFT JOIN IVT_Product C ON CAST(C.ProductKey AS NVARCHAR(50))=A.ProductKey
	WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99
	AND B.OrderDate BETWEEN @FromDate AND @ToDate AND C.ProductID=@ProductID AND B.WarehouseKey =@WarehouseKey 
)
SELECT * FROM #K
ORDER BY OrderDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();

                double zTonDau = Stock_Product.TONDAU_THEKHO(zFromDate, zToDate, WarehouseKey, ProductID);
                if (zTable.Rows.Count > 0)
                {
                    //Add dòng đầu kì
                    DataRow r;
                    r = zTable.NewRow();
                    r["OrderDescription"] = "TỒN ĐẦU KÌ";
                    r["DAUKI"] = 0;
                    r["NHAP"] = 0;
                    r["XUAT"] = 0;
                    r["CUOIKI"] = zTonDau;
                    zTable.Rows.InsertAt(r,0);
                    double zTongNhap = 0;
                    double zTongXuat = 0;
                    double zTam = 0;
                    zTam += zTonDau;
                    for (int i = 1; i < zTable.Rows.Count; i++)
                    {
                        r = zTable.Rows[i];
                        r["DAUKI"] = zTam;

                        zTongNhap += double.Parse(r["NHAP"].ToString());
                        zTongXuat+= double.Parse(r["XUAT"].ToString());
                        zTam += double.Parse(r["NHAP"].ToString()) - double.Parse(r["XUAT"].ToString());
                        r["CUOIKI"] = zTam;
                    }
                    //add dòng tổng
                    r = zTable.NewRow();
                    r["OrderDescription"] = "TỔNG CỘNG";
                    r["DAUKI"] = zTonDau;
                    r["NHAP"] = zTongNhap;
                    r["XUAT"] = zTongXuat;
                    r["CUOIKI"] = zTam;
                    zTable.Rows.InsertAt(r, zTable.Rows.Count);
                }

            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static double TONDAU_THEKHO(DateTime FromDate, DateTime ToDate, int WarehouseKey, string ProductID)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string zFilter = "";
            if (ProductID.Trim().Length > 0)
            {
                zFilter = " AND A.ProductID =@ProductID ";
            }

            double zResult = 0;
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'
--declare @WarehouseKey int =1;

--Chuyển thời gian tìm kím lùi 1 ngày
declare @temp NVARCHAR(50)=  LEFT(CONVERT(VARCHAR,@FromDate,120),10)+' 23:59:59'
declare @ToDateTruoc datetime = DATEADD(day, -1, CONVERT(datetime,@temp,120))
CREATE TABLE #TONKHO
(
ProductKey NVARCHAR(50),
ProductID NVARCHAR(250),
ProductName NVARCHAR(250),
UnitName  NVARCHAR(50),
BeginStock FLOAT
)
INSERT INTO #TONKHO
SELECT CAST(A.ProductKey AS NVARCHAR(50)) AS ProductKey, A.ProductID AS ProductID, A.ProductName AS   ProductName, 
 B.Unitname AS Unitname,
 ---Đầu kì
ISNULL(dbo.IVT_Get_InQuantityReality(A.ProductKey, '2010-01-01 00:00:00', @ToDateTruoc,@WarehouseKey),0) -
ISNULL(dbo.IVT_Get_OutQuantityReality(A.ProductKey,'2010-01-01 00:00:00', @ToDateTruoc,@WarehouseKey),0) 
AS BeginStock
FROM IVT_Product A 
LEFT JOIN IVT_Product_Unit B ON A.BasicUnit = B.UnitKey
WHERE A.RecordStatus <> 99 AND A.Parent!='0' @Paramater
ORDER BY A.Rank, ProductID

SELECT ISNULL((ROUND(BeginStock,1)),0) FROM #TONKHO 

DROP TABLE #TONKHO";

            zSQL = zSQL.Replace("@Paramater", zFilter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = ProductID;
                zCommand.CommandTimeout = 350;
                if (double.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zResult;
        }

        #endregion
    }
}
