﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Employee_Exe
    {
        public Order_Money_Date_Info Info { get; set; }
        public int AutoKey { get; set; } = 0;
        public string Message { get; set; } = string.Empty;
        public string RoleID { get; set; } = string.Empty;

        public string CreatedBy { get; set; } = string.Empty;
        public string CreatedName { get; set; } = string.Empty;
        public string ModifiedBy { get; set; } = string.Empty;
        public string ModifiedName { get; set; } = string.Empty;
        public int RecordStatus { get; set; } = 0;
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"  INSERT INTO FTR_Money_Date
          (
		    TeamKey,
			EmployeeKey, 
			EmployeeID, 
			EmployeeName, 
			Date, 
			Money, 
			Money_Holidays, 
			Money_Sundays, 
			Money_Dif, 
			Money_Dif_Sunday, 
			Money_Dif_Holiday,
			Total,
			RecordStatus, 
			CreatedOn, 
			CreatedBy, 
			CreatedName, 
			ModifiedOn, 
			ModifiedBy, 
			ModifiedName
          ) VALUES (
		     @TeamKey,
			 @EmployeeKey, 
			 @EmployeeID, 
			 @EmployeeName, 
			 @Date, 
			 @Money, 
			 @Money_Holidays, 
			 @Money_Sundays, 
			 @Money_Dif, 
			 @Money_Dif_Sunday, 
			 @Money_Dif_Holiday,
			 @ToTal,
			 0, 
			 GETDATE(),
			 @CreatedBy, 
			 @CreatedName, 
			 GETDATE(),
			 @ModifiedBy, 
			 @ModifiedName 
          ) SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Info.EmployeeKey.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = Info.TeamKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Info.EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Info.EmployeeName.Trim();
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Info.Date;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = Info.Money;
                zCommand.Parameters.Add("@Money_Holidays", SqlDbType.Money).Value = Info.Money_Holidays;
                zCommand.Parameters.Add("@Money_Sundays", SqlDbType.Money).Value = Info.Money_Sundays;
                zCommand.Parameters.Add("@Money_Dif", SqlDbType.Money).Value = Info.Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday", SqlDbType.Money).Value = Info.Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday", SqlDbType.Money).Value = Info.Money_Dif_Holiday;
                zCommand.Parameters.Add("@ToTal", SqlDbType.Money).Value = Info.Total;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Info.ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Info.ModifiedName.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Info.CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Info.CreatedName.Trim();
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @"
UPDATE FTR_Money_Date SET 
TeamKey = @TeamKey,
EmployeeKey = @EmployeeKey,
EmployeeID = @EmployeeID,
EmployeeName = @EmployeeName,
Date = @Date,
Money = @Money,
Money_Holidays = @Money_Holidays,
Money_Sundays = @Money_Sundays,
Money_Dif = @Money_Dif,
Money_Dif_Sunday = @Money_Dif_Sunday,
Money_Dif_Holiday = @Money_Dif_Holiday,
Total = @ToTal,
ModifiedOn = GETDATE(),
ModifiedBy = @ModifiedBy,
ModifiedName = @ModifiedName
WHERE 
EmployeeKey = @EmployeeKey AND 
YEAR([Date]) = YEAR(@Date) AND 
MONTH([Date]) = MONTH(@Date) AND 
DAY([Date]) = DAY(@Date) 
SELECT 20";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Info.EmployeeKey.Trim();
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = Info.TeamKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = Info.EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = Info.EmployeeName.Trim();
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Info.Date;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = Info.Money;
                zCommand.Parameters.Add("@Money_Holidays", SqlDbType.Money).Value = Info.Money_Holidays;
                zCommand.Parameters.Add("@Money_Sundays", SqlDbType.Money).Value = Info.Money_Sundays;
                zCommand.Parameters.Add("@Money_Dif", SqlDbType.Money).Value = Info.Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday", SqlDbType.Money).Value = Info.Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday", SqlDbType.Money).Value = Info.Money_Dif_Holiday;
                zCommand.Parameters.Add("@ToTal", SqlDbType.Money).Value = Info.Total;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Info.ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Info.ModifiedName.Trim();
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public void Save()
        {
            int IsExits = EmployeeExits();
            if (IsExits > 0)
                Update();
            else
                Create();
        }
        public int EmployeeExits()
        {
            int result = 0;
            string zSQL = @"
SELECT COUNT(EmployeeKey) 
FROM FTR_Money_Date 
WHERE EmployeeKey = @EmployeeKey AND 
YEAR([Date]) = YEAR(@Date) AND 
MONTH([Date]) = MONTH(@Date) AND 
DAY([Date]) = DAY(@Date)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Info.Date;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = Info.EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return result;
        }
    }
}
