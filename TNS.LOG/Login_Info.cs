﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.LOG
{
    public class Login_Info
    {

        #region [ Field Name ]
        private string _RowGuid = "";
        private string _UserKey = "";
        private DateTime _DateLogin;
        private string _ComputerLogin = "";
        private string _BrowserType = "";
        private string _BrowserName = "";
        private string _BrowserVersion = "";
        private string _HostName = "";
        private string _IPAddress = "";
        private string _IPOneAddress = "";
        private string _WindownsName = "";
        private string _WindownsVersion = "";
        private int _TypeLog = 0;
        private string _UserAgent = "";
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_RowGuid.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _RowGuid; }
            set { _RowGuid = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public DateTime DateLogin
        {
            get { return _DateLogin; }
            set { _DateLogin = value; }
        }
        public string ComputerLogin
        {
            get { return _ComputerLogin; }
            set { _ComputerLogin = value; }
        }
        public string BrowserType
        {
            get { return _BrowserType; }
            set { _BrowserType = value; }
        }
        public string BrowserName
        {
            get { return _BrowserName; }
            set { _BrowserName = value; }
        }
        public string BrowserVersion
        {
            get { return _BrowserVersion; }
            set { _BrowserVersion = value; }
        }
        public string HostName
        {
            get { return _HostName; }
            set { _HostName = value; }
        }
        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        public string IPOneAddress
        {
            get { return _IPOneAddress; }
            set { _IPOneAddress = value; }
        }
        public string WindownsName
        {
            get { return _WindownsName; }
            set { _WindownsName = value; }
        }
        public string WindownsVersion
        {
            get { return _WindownsVersion; }
            set { _WindownsVersion = value; }
        }
        public string UserAgent
        {
            get { return _UserAgent; }
            set { _UserAgent = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int TypeLog
        {
            get
            {
                return _TypeLog;
            }

            set
            {
                _TypeLog = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Login_Info()
        {
        }
        public Login_Info(string RowGuid)
        {
            string zSQL = "SELECT * FROM LOG_Login WHERE RowGuid = @RowGuid";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = Guid.Parse(RowGuid);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _RowGuid = zReader["RowGuid"].ToString();
                    _UserKey = zReader["UserKey"].ToString();
                    if (zReader["DateLogin"] != DBNull.Value)
                        _DateLogin = (DateTime)zReader["DateLogin"];
                    _ComputerLogin = zReader["ComputerLogin"].ToString().Trim();
                    _BrowserType = zReader["BrowserType"].ToString().Trim();
                    _BrowserName = zReader["BrowserName"].ToString().Trim();
                    _BrowserVersion = zReader["BrowserVersion"].ToString().Trim();
                    _HostName = zReader["HostName"].ToString().Trim();
                    _IPAddress = zReader["IPAddress"].ToString().Trim();
                    _IPOneAddress = zReader["IPOneAddress"].ToString().Trim();
                    _WindownsName = zReader["WindownsName"].ToString().Trim();
                    _WindownsVersion = zReader["WindownsVersion"].ToString().Trim();
                    if (zReader["FailedPassword"] != DBNull.Value)
                        _TypeLog = int.Parse(zReader["TypeLog"].ToString());
                    _UserAgent = zReader["UserAgent"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO LOG_Login ("
        + " UserKey ,DateLogin ,ComputerLogin ,BrowserType ,BrowserName ,BrowserVersion ,HostName ,IPAddress ,IPOneAddress ,WindownsName ,WindownsVersion ,TypeLog ,UserAgent ,Description ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@UserKey ,@DateLogin ,@ComputerLogin ,@BrowserType ,@BrowserName ,@BrowserVersion ,@HostName ,@IPAddress ,@IPOneAddress ,@WindownsName ,@WindownsVersion ,@TypeLog ,@UserAgent ,@Description ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_UserKey.Length > 0)
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                else
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_DateLogin == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateLogin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateLogin", SqlDbType.DateTime).Value = _DateLogin;
                zCommand.Parameters.Add("@ComputerLogin", SqlDbType.NVarChar).Value = _ComputerLogin.Trim();
                zCommand.Parameters.Add("@BrowserType", SqlDbType.NVarChar).Value = _BrowserType.Trim();
                zCommand.Parameters.Add("@BrowserName", SqlDbType.NVarChar).Value = _BrowserName.Trim();
                zCommand.Parameters.Add("@BrowserVersion", SqlDbType.NVarChar).Value = _BrowserVersion.Trim();
                zCommand.Parameters.Add("@HostName", SqlDbType.NVarChar).Value = _HostName.Trim();
                zCommand.Parameters.Add("@IPAddress", SqlDbType.NVarChar).Value = _IPAddress.Trim();
                zCommand.Parameters.Add("@IPOneAddress", SqlDbType.NVarChar).Value = _IPOneAddress.Trim();
                zCommand.Parameters.Add("@WindownsName", SqlDbType.NVarChar).Value = _WindownsName.Trim();
                zCommand.Parameters.Add("@WindownsVersion", SqlDbType.NVarChar).Value = _WindownsVersion.Trim();
                zCommand.Parameters.Add("@TypeLog", SqlDbType.Int).Value = _TypeLog;
                zCommand.Parameters.Add("@UserAgent", SqlDbType.NVarChar).Value = _UserAgent.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE LOG_Login SET "
                        + " UserKey = @UserKey,"
                        + " DateLogin = @DateLogin,"
                        + " ComputerLogin = @ComputerLogin,"
                        + " BrowserType = @BrowserType,"
                        + " BrowserName = @BrowserName,"
                        + " BrowserVersion = @BrowserVersion,"
                        + " HostName = @HostName,"
                        + " IPAddress = @IPAddress,"
                        + " IPOneAddress = @IPOneAddress,"
                        + " WindownsName = @WindownsName,"
                        + " WindownsVersion = @WindownsVersion,"
                        + " TypeLog = @TypeLog,"
                        + " UserAgent = @UserAgent,"
                        + " Description = @Description,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE RowGuid = @RowGuid";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_RowGuid.Length == 36)
                    zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_RowGuid);
                else
                    zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_UserKey.Length == 36)
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                else
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_DateLogin == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateLogin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateLogin", SqlDbType.DateTime).Value = _DateLogin;
                zCommand.Parameters.Add("@ComputerLogin", SqlDbType.NVarChar).Value = _ComputerLogin.Trim();
                zCommand.Parameters.Add("@BrowserType", SqlDbType.NVarChar).Value = _BrowserType.Trim();
                zCommand.Parameters.Add("@BrowserName", SqlDbType.NVarChar).Value = _BrowserName.Trim();
                zCommand.Parameters.Add("@BrowserVersion", SqlDbType.NVarChar).Value = _BrowserVersion.Trim();
                zCommand.Parameters.Add("@HostName", SqlDbType.NVarChar).Value = _HostName.Trim();
                zCommand.Parameters.Add("@IPAddress", SqlDbType.NVarChar).Value = _IPAddress.Trim();
                zCommand.Parameters.Add("@IPOneAddress", SqlDbType.NVarChar).Value = _IPOneAddress.Trim();
                zCommand.Parameters.Add("@WindownsName", SqlDbType.NVarChar).Value = _WindownsName.Trim();
                zCommand.Parameters.Add("@WindownsVersion", SqlDbType.NVarChar).Value = _WindownsVersion.Trim();
                zCommand.Parameters.Add("@TypeLog", SqlDbType.Int).Value = _TypeLog;
                zCommand.Parameters.Add("@UserAgent", SqlDbType.NVarChar).Value = _UserAgent.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOG_Login SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE RowGuid = @RowGuid";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_RowGuid);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_RowGuid.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
