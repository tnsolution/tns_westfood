﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TN.SLR
{
    public class Salary_Data
    {
        public static DataTable ThongTinPhieuLuong(DateTime DateWrite, string EmployeeKey)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            //---------- String SQL Access Database ---------------
            string zSQL = @"
--declare @EmployeeKey NVARCHAR(50)='408C353A-26CB-495D-85C4-87F0DAAD4DFC'
--declare @FromDate Datetime ='2020-06-01 00:00:00'
--declare @ToDate Datetime ='2020-06-30 23:59:59'

DECLARE @TeamKey FLOAT =0
DECLARE @VP int =0;
DECLARE @HTSX int =0;
DECLARE @SX int =0;
-------------------------Xét khối -------
Select @TeamKey = [dbo].[Fn_TeamKeyWorkingHistory](@EmployeeKey,@FromDate,@ToDate)

SELECT @SX =COUNT(*)  FROM SYS_Team 
WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26  AND TeamKey =@TeamKey

SELECT @HTSX  =COUNT(*) FROM SYS_Team 
WHERE RecordStatus<> 99 AND BranchKey = 4 AND DepartmentKey = 26  AND TeamKey =@TeamKey

SELECT @VP =COUNT(*) FROM SYS_Team WHERE RecordStatus <> 99 AND BranchKey=2 AND TeamKey =@TeamKey

IF(@SX > 0)
		SELECT 
		EmployeeKey,EmployeeID,EmployeeName,ATM,
		TeamName,PositionName,CONCAT(MONTH(DateWrite),'/',YEAR(DateWrite)) AS DateWrite
		FROM 
		[dbo].[SLR_ReportWorker_Close]
		WHERE EmployeeKey= @EmployeeKey
		AND DateWrite BETWEEN  @FromDate AND @ToDate 
		AND Publish_Close=1
		AND RecordStatus <> 99
		Group by EmployeeKey,EmployeeID,EmployeeName,ATM,
		TeamName,PositionName,DateWrite
IF(@HTSX > 0)
		SELECT 
		EmployeeKey,EmployeeID,EmployeeName,ATM,
		TeamName,PositionName,CONCAT(MONTH(DateWrite),'/',YEAR(DateWrite)) AS DateWrite
		FROM 
		[dbo].[SLR_ReportSupport_Close]
		WHERE EmployeeKey= @EmployeeKey
		AND DateWrite BETWEEN  @FromDate AND @ToDate 
		AND Publish_Close=1
		AND RecordStatus <> 99
		Group by EmployeeKey,EmployeeID,EmployeeName,ATM,
		TeamName,PositionName,DateWrite
IF(@VP > 0)
		SELECT 
		EmployeeKey,EmployeeID,EmployeeName,ATM,
		TeamName,PositionName,CONCAT(MONTH(DateWrite),'/',YEAR(DateWrite)) AS DateWrite
		FROM 
		[dbo].[SLR_ReportOffice_Close]
		WHERE EmployeeKey= @EmployeeKey
		AND DateWrite BETWEEN  @FromDate AND @ToDate 
		AND Publish_Close=1
		AND RecordStatus <> 99
		Group by EmployeeKey,EmployeeID,EmployeeName,ATM,
		TeamName,PositionName,DateWrite";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ChitietPhieuLuong(DateTime DateWrite,string EmployeeKey)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            //---------- String SQL Access Database ---------------
            string zSQL = @"
--declare @EmployeeKey NVARCHAR(50)='408C353A-26CB-495D-85C4-87F0DAAD4DFC'
--declare @FromDate Datetime ='2020-06-01 00:00:00'
--declare @ToDate Datetime ='2020-06-30 23:59:59'

DECLARE @TeamKey FLOAT =0
DECLARE @VP int =0;
DECLARE @HTSX int =0;
DECLARE @SX int =0;
-------------------------Xét khối -------
Select @TeamKey = [dbo].[Fn_TeamKeyWorkingHistory](@EmployeeKey,@FromDate,@ToDate)

SELECT @SX =COUNT(*)  FROM SYS_Team 
WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26  AND TeamKey =@TeamKey

SELECT @HTSX  =COUNT(*) FROM SYS_Team 
WHERE RecordStatus<> 99 AND BranchKey = 4 AND DepartmentKey = 26  AND TeamKey =@TeamKey

SELECT @VP =COUNT(*) FROM SYS_Team WHERE RecordStatus <> 99 AND BranchKey=2 AND TeamKey =@TeamKey

IF(@SX > 0)
        SELECT 
        CASE WHEN TypeName !='' THEN TypeName+'-'+ CodeName
	         ELSE CodeName
        END AS CodeName, ROUND(Amount,1) AS Amount FROM 
        [dbo].[SLR_ReportWorker_Close]
        WHERE EmployeeKey= @EmployeeKey
        AND DateWrite BETWEEN  @FromDate AND @ToDate 
        AND Publish_Close=1
        AND RecordStatus <> 99
        ORDER BY Rank
IF(@HTSX > 0)
        SELECT 
        CASE WHEN TypeName !='' THEN TypeName+'-'+ CodeName
	         ELSE CodeName
        END AS CodeName, ROUND(Amount,1) AS Amount FROM 
        [dbo].[SLR_ReportSupport_Close]
        WHERE EmployeeKey= @EmployeeKey
        AND DateWrite BETWEEN  @FromDate AND @ToDate 
        AND Publish_Close=1
        AND RecordStatus <> 99
        ORDER BY Rank
IF(@VP > 0)
        SELECT 
        CASE WHEN TypeName !='' THEN TypeName+'-'+ CodeName
	         ELSE CodeName
        END AS CodeName, ROUND(Amount,1) AS Amount FROM 
        [dbo].[SLR_ReportOffice_Close]
        WHERE EmployeeKey= @EmployeeKey
        AND DateWrite BETWEEN  @FromDate AND @ToDate 
        AND Publish_Close=1
        AND RecordStatus <> 99
        ORDER BY Rank


";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
