﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TN.Toolbox;
using TNS.CORE;
using TNS.HRM;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_OrderView : Form
    {
        Color ColorWarning = Color.FromArgb(199, 21, 112);
        Color ColorNormal = Color.FromArgb(255, 255, 225);
        int _TeamKey_Search = 0;
        bool _IsNew = false;
        string _OrderKey = "";
        DateTime _OrderDate ;
        Order_Object _Order;
        public Frm_OrderView()
        {
            InitializeComponent();

            LV_Product.ItemSelectionChanged += LV_Product_ItemSelectionChanged;
            LV_Product.ItemActivate += LV_Product_ItemActivate;
            LV_Product.KeyDown += LV_Product_KeyDown;

            cbo_TeamID_Search.SelectedIndexChanged += cbo_TeamID_Search_SelectedIndexChanged;

            GVOrder.SelectionChanged += GVOrder_SelectionChanged;

            txt_OrderID.Leave += txt_OrderID_Leave;
            txt_OrderIDFollow.Leave += txt_OrderIDFollow_Leave;
            txt_WorkID.Leave += txt_WorkID_Leave;
            txt_ProductID.Leave += txt_ProductID_Leave;
            txt_Search_ProductID.KeyUp += txt_Search_ProductID_KeyUp;
            txt_Search_ProductID.Leave += txt_Search_ProductID_Leave;
            txt_Search_WorkID.Leave += txt_Search_WorkID_Leave;

            btn_SearchInput_Document.Click += btn_SearchInput_Document_Click;
            btn_SearchInput_Work.Click += btn_SearchInput_Work_Click;

            txt_QuantityDocument.Leave += (o, e) => { QuantityCalculator(); };
            txt_QuantityLose.Leave += (o, e) => { QuantityCalculator(); };
            txt_QuantityReality.Leave += (o, e) => { QuantityCalculator(); };

            txt_QuantityDocument.KeyPress += txt_Number_KeyPress;
            txt_QuantityLose.KeyPress += txt_Number_KeyPress;
            txt_QuantityReality.KeyPress += txt_Number_KeyPress;

            btn_Export.Click += Btn_Export_Click;
            btn_Export1.Click += Btn_Export1_Click;
            btn_Export2.Click += Btn_Export2_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVOrder, true);
            Utils.DoubleBuffered(GVRate, true);

            Utils.DrawGVStyle(ref GVRate);
            Utils.DrawGVStyle(ref GVOrder);
        }


        private void Frm_Import_Order_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
           

            btn_Show.Visible = true;
            btn_Hide.Visible = false;
            Panel_Info.Visible = false;

            splitContainer1.SplitterDistance = Screen.PrimaryScreen.Bounds.Width;
            splitContainer1.Panel1MinSize = 200;
            splitContainer1.Panel2MinSize = 0;

            Layout_Product();
            SetupLayoutGVOrder();
            SetupLayoutGVRate();
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cbo_TeamID_Search, zSQL, "---Chọn---");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, zSQL, "---Chọn---");

            dte_OrderDate.Value = SessionUser.Date_Work;
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;


            ArrayList ListItems = new ArrayList();
            TN_Item li = new TN_Item();
            li.Value = -1;
            li.Name = "--Chọn--";
            ListItems.Add(li);
            li = new TN_Item();
            li.Value = 0;
            li.Name = "Đang thực hiện";
            ListItems.Add(li);
            li = new TN_Item();
            li.Value = 1;
            li.Name = "Đã thực hiện";
            ListItems.Add(li);

            cbo_Status.DataSource = ListItems;
            cbo_Status.DisplayMember = "Name";
            cbo_Status.ValueMember = "Value";

           // DataTable zTable = Order_Data.List_Search(dte_FromDate.Value, dte_ToDate.Value, "", "", "", "", 0);
            //LoadGVOrder(zTable);
        }

        #region[Layout and Event GV]
        private void Layout_Product()
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 0;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV_Product.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã sản phẩm";
            colHead.Width = 127;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Product.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên sản phẩm";
            colHead.Width = -1;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV_Product.Columns.Add(colHead);
        }
        private void SetupLayoutGVOrder()
        {
            // Setup Column 
            GVOrder.Columns.Add("No", "STT");
            GVOrder.Columns.Add("OrderID_Compare", "Mã Tham Chiếu");
            GVOrder.Columns.Add("OrderID", "Mã Đơn Hàng");
            GVOrder.Columns.Add("OrderDate", "Ngày");
            GVOrder.Columns.Add("TeamID", "Mã Nhóm");
            GVOrder.Columns.Add("TeamName", "Tên Nhóm");
            GVOrder.Columns.Add("ProductID", "Mã Sản Phẩm");
            GVOrder.Columns.Add("ProductName", "Tên Sản Phẩm");
            GVOrder.Columns.Add("StageID", "Mã Công Đoạn");
            GVOrder.Columns.Add("StageName", "Tên Công Đoạn");
            GVOrder.Columns.Add("StagePrice", "Đơn Giá");
            GVOrder.Columns.Add("OrderIDFollow", "Số Chứng Từ");
            GVOrder.Columns.Add("QuantityDocument", "SL Chứng Từ");
            GVOrder.Columns.Add("QuantityReality", "SL Thực Tế");
            GVOrder.Columns.Add("QuantityLose", "SL Hao Hụt");
            GVOrder.Columns.Add("Unit", "Đơn Vị");
            GVOrder.Columns.Add("WorkStatus", "Tình Trạng");
            GVOrder.Columns.Add("Percent", "Tỷ lệ HT");
            GVOrder.Columns.Add("Note", "Ghi Chú");
            GVOrder.Columns.Add("Message", "Thông Báo");

            GVOrder.Columns["No"].Width = 40;
            GVOrder.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["OrderID_Compare"].Width = 150;
            GVOrder.Columns["OrderID_Compare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderID"].Width = 170;
            GVOrder.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderDate"].Width = 100;
            GVOrder.Columns["OrderDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["TeamID"].Width = 100;
            GVOrder.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["TeamName"].Width = 250;
            GVOrder.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["ProductID"].Width = 120;
            GVOrder.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["ProductName"].Width = 200;
            GVOrder.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["StageID"].Width = 120;
            GVOrder.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["StageName"].Width = 300;
            GVOrder.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["StagePrice"].Width = 80;
            GVOrder.Columns["StagePrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["OrderIDFollow"].Width = 120;
            GVOrder.Columns["OrderIDFollow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["QuantityDocument"].Width = 120;
            GVOrder.Columns["QuantityDocument"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["QuantityReality"].Width = 120;
            GVOrder.Columns["QuantityReality"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["QuantityLose"].Width = 120;
            GVOrder.Columns["QuantityLose"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["Unit"].Width = 90;
            GVOrder.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["WorkStatus"].Width = 120;
            GVOrder.Columns["WorkStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["Percent"].Width = 90;
            GVOrder.Columns["Percent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVOrder.Columns["Note"].Width = 200;
            GVOrder.Columns["Note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["Message"].Width = 300;
            GVOrder.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.ReadOnly = true;
        }
        private void SetupLayoutGVRate()
        {
            GVRate.Columns.Add("RateID", "Mã");
            GVRate.Columns.Add("RateName", "Hệ số");
            GVRate.Columns.Add("Rate", "Giá trị");

            GVRate.Columns["RateID"].Width = 90;
            GVRate.Columns["RateID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVRate.Columns["RateName"].Width = 250;
            GVRate.Columns["RateName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVRate.Columns["RateName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GVRate.Columns["RateName"].ReadOnly = true;

            GVRate.Columns["Rate"].Width = 50;
            GVRate.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVRate.Columns["Rate"].ReadOnly = true;
        }
        private void GVOrder_SelectionChanged(object sender, EventArgs e)
        {
            if (GVOrder.SelectedRows.Count > 0 &&
                GVOrder.CurrentRow.Tag != null)
            {
                _IsNew = false;
                GVRate.Rows.Clear();
                _OrderKey = GVOrder.CurrentRow.Tag.ToString();
                LoadOrderInfo(_OrderKey, false);

                Cursor = Cursors.WaitCursor;
                using (Frm_Loading frm = new Frm_Loading(Display)) { frm.ShowDialog(this); }
                Cursor = Cursors.Default;
                

                btn_Show_Click(null,null);
            }
            else
            {
            }
        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        #endregion

        private void Display()
        {
            DataTable zTableEmployee = HoTroSanXuat.CacNhanVienThucHien_OrderView(_OrderKey);
            DataTable zTableView = Order_Adjusted_Data.List_NhanSuChiaLaiLan1_Cua1DonHang(_OrderKey,_OrderDate);
            DataTable zTableView2 = Order_Adjusted_Data.List_NhanSuChiaLaiLan2_Cua1DonHang(_OrderKey, _OrderDate);
            this.Invoke(new MethodInvoker(delegate ()
            {

                InitGV_Layout_GVData();
                InitData_GVData(zTableEmployee);
                // chia lại lan 1

                InitGV_Layout_GVAdjustView1();
                InitData_GVAdjustView1(zTableView);
                // chia lại lan 2

                InitGV_Layout_GVAdjustView2();
                InitData_GVAdjustView2(zTableView2);
            }));
           
          
        }


        #region [Function]        
        private void LoadLVProduct()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Product;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable zTable = Product_Data.SearchProduct(txt_Search_ProductID.Text.Trim());
            if (zTable.Rows.Count > 0)
            {
                int n = zTable.Rows.Count;
                for (int i = 0; i < n; i++)
                {
                    lvi = new ListViewItem();
                    lvi.Text = (i + 1).ToString();
                    DataRow nRow = zTable.Rows[i];
                    lvi.Tag = nRow["ProductKey"].ToString();
                    lvi.BackColor = Color.White;

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["ProductID"].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = nRow["ProductName"].ToString().Trim();

                    lvi.SubItems.Add(lvsi);
                    LV.Items.Add(lvi);
                }

                Utils.SizeLastColumn_LV(LV_Product);
            }
            else
            {
                LV_Product.Visible = false;
            }
            this.Cursor = Cursors.Default;
        }
        private void QuantityCalculator()
        {
            decimal zQuantityReality = 0;
            decimal zQuantityDocument = 0;
            decimal zQuantityLoss = 0;

            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityReality = decimal.Parse(txt_QuantityReality.Text);
            }
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityDocument = decimal.Parse(txt_QuantityDocument.Text);
            }
            if (txt_QuantityDocument.Text.Length > 0)
            {
                zQuantityLoss = zQuantityDocument - zQuantityReality;
            }

            txt_QuantityReality.Text = zQuantityReality.ToString("n1");
            txt_QuantityLose.Text = zQuantityLoss.ToString("n1");
        }
        void LoadGVOrder(DataTable InTable)
        {
            int Index = 0;
            GVOrder.Rows.Clear();
            foreach (DataRow Row in InTable.Rows)
            {
                GVOrder.Rows.Add();
                GVOrder.Rows[Index].Tag = Row["OrderKey"].ToString();
                GVOrder.Rows[Index].Cells["No"].Value = (Index + 1).ToString();
                GVOrder.Rows[Index].Cells["OrderID_Compare"].Value = Row["OrderID_Compare"].ToString();
                GVOrder.Rows[Index].Cells["OrderID"].Value = Row["OrderID"].ToString();
                DateTime zOrderDate = DateTime.Parse(Row["OrderDate"].ToString());
                GVOrder.Rows[Index].Cells["OrderDate"].Value = zOrderDate.ToString("dd/MM/yyyy");

                GVOrder.Rows[Index].Cells["TeamID"].Tag = Row["TeamKey"].ToString();
                GVOrder.Rows[Index].Cells["TeamID"].Value = Row["TeamID"].ToString();
                GVOrder.Rows[Index].Cells["TeamName"].Value = Row["TeamName"].ToString();

                GVOrder.Rows[Index].Cells["ProductID"].Tag = Row["ProductKey"].ToString();
                GVOrder.Rows[Index].Cells["ProductID"].Value = Row["ProductID"].ToString();
                GVOrder.Rows[Index].Cells["ProductName"].Value = Row["ProductName"].ToString();

                GVOrder.Rows[Index].Cells["StageID"].Tag = Row["StageKey"].ToString();
                GVOrder.Rows[Index].Cells["StageID"].Value = Row["StagesID"].ToString();
                GVOrder.Rows[Index].Cells["StageName"].Value = Row["StageName"].ToString();
                GVOrder.Rows[Index].Cells["StagePrice"].Value = Row["StagePrice"].Ton2String();

                GVOrder.Rows[Index].Cells["OrderIDFollow"].Value = Row["OrderIDFollow"].ToString();
                GVOrder.Rows[Index].Cells["QuantityDocument"].Value = Row["QuantityDocument"].Ton2String();
                GVOrder.Rows[Index].Cells["QuantityReality"].Value = Row["QuantityReality"].Ton2String();
                GVOrder.Rows[Index].Cells["QuantityLose"].Value = Row["QuantityLoss"].Ton2String();

                GVOrder.Rows[Index].Cells["Unit"].Value = Row["UnitName"].ToString();
                if (Row["WorkStatus"].ToInt() == 1)
                    GVOrder.Rows[Index].Cells["WorkStatus"].Value = "ĐÃ THỰC HIỆN";
                else
                    GVOrder.Rows[Index].Cells["WorkStatus"].Value = "";

                GVOrder.Rows[Index].Cells["Percent"].Value = Row["PercentFinish"].Ton2String();

                GVOrder.Rows[Index].Cells["Note"].Value = Row["Note"].ToString();
                GVOrder.Rows[Index].Cells["Message"].Value = "";
                Index++;
            }
            GVOrder.ClearSelection();

        }
        void LoadOrderInfo(string OrderKey, bool Copy)
        {
            _Order = new Order_Object(OrderKey);
            _OrderDate = _Order.OrderDate;

            cbo_Status.SelectedValue = _Order.WorkStatus;
            dte_OrderDate.Value = _Order.OrderDate;
            txt_OrderID.Tag = OrderKey;
            txt_OrderID.Text = _Order.OrderID;
            Team_Info zTeam = new Team_Info(_Order.TeamKey);
            cbo_Team.SelectedValue = zTeam.Key;
            txt_ProductID.Tag = _Order.ProductKey;
            txt_ProductID.Text = _Order.ProductID;
            txt_ProductName.Text = _Order.ProductName;
            Product_Stages_Info zStage = new Product_Stages_Info(_Order.Category_Stage);
            txt_WorkID.Tag = _Order.Category_Stage;
            txt_WorkID.Text = zStage.StagesID;
            txt_WorkName.Text = zStage.StageName;
            txt_Price.Text = zStage.Price.Ton2String();
            txt_Unit.Tag = _Order.UnitKey;
            txt_Unit.Text = _Order.UnitName;
            txt_Note.Text = _Order.Note;
            txt_OrderIDFollow.Text = _Order.OrderIDFollow;
            txt_OrderIDCompare.Text = _Order.OrderID_Compare;
            txt_QuantityDocument.Text = _Order.QuantityDocument.Ton2String();
            txt_QuantityReality.Text = _Order.QuantityReality.Ton2String();
            txt_QuantityLose.Text = _Order.QuantityLoss.Ton2String();
            txt_Percent.Text = _Order.PercentFinish.ToString();

            txt_OrderIDFollow.StateCommon.Back.Color1 = ColorNormal;
            txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
            txt_ProductID.StateCommon.Back.Color1 = ColorNormal;
            txt_OrderID.StateCommon.Back.Color1 = ColorNormal;
            dte_OrderDate.BackColor = ColorNormal;
            cbo_Team.StateCommon.DropBack.Color1 = ColorNormal;
            cbo_Status.StateCommon.DropBack.Color1 = ColorNormal;
            txt_Unit.StateCommon.Back.Color1 = ColorNormal;

            if (_Order.List_Rate.Count > 0)
            {
                for (int i = 0; i < _Order.List_Rate.Count; i++)
                {
                    Order_Rate_Info zRate = _Order.List_Rate[i];
                    LoadGVRate(zRate, i);
                }
            }
        }
        void LoadGVRate(Order_Rate_Info zOrderRate, int index)
        {
            GVRate.Rows.Add();
            GVRate.Rows[index].Tag = zOrderRate;
            GVRate.Rows[index].Cells["RateID"].Tag = zOrderRate.CategoryKey;
            Category_Info zRate = new Category_Info(zOrderRate.CategoryKey);
            GVRate.Rows[index].Cells["RateID"].Value = zRate.RateID;
            GVRate.Rows[index].Cells["RateName"].Value = zOrderRate.RateName; ;
            GVRate.Rows[index].Cells["Rate"].Value = zOrderRate.Rate;
        }
        //Layout Gidview đơn hàng năng suất
        void InitGV_Layout_GVData()
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 13;

            GVEmployee.Cols.Add(ToTalCol);
            GVEmployee.Rows.Add(1);

            //Row Header
            GVEmployee.Rows[0][0] = "Stt";
            GVEmployee.Rows[0][1] = "Mã NV";
            GVEmployee.Rows[0][2] = "Tên Nhân Viên";
            GVEmployee.Rows[0][3] = "Nhóm";
            GVEmployee.Rows[0][4] = "TG P.bổ";
            GVEmployee.Rows[0][5] = "SL Ng.Liệu";
            GVEmployee.Rows[0][6] = "Số Rổ";
            GVEmployee.Rows[0][7] = "Thời Gian";
            GVEmployee.Rows[0][8] = "SL Th.Phẩm";
            GVEmployee.Rows[0][9] = "Tiền";

            GVEmployee.Rows[0][10] = "Mượn Người";
            GVEmployee.Rows[0][11] = "Ăn Riêng";
            GVEmployee.Rows[0][12] = "Ăn Chung";

            //Style         
            GVEmployee.AllowFreezing = AllowFreezingEnum.Both;
            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVEmployee.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVEmployee.Rows.Fixed = 1;
            GVEmployee.Cols.Frozen = 4;

            GVEmployee.Cols[0].StyleNew.BackColor = Color.Empty;
            GVEmployee.Cols[1].StyleNew.BackColor = Color.Empty;
            GVEmployee.Cols[2].StyleNew.BackColor = Color.Empty;
            GVEmployee.Cols[3].StyleNew.BackColor = Color.Empty;

            GVEmployee.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[0].Height = 40;

            GVEmployee.Cols[0].Width = 30;
            GVEmployee.Cols[1].Width = 55;
            GVEmployee.Cols[2].Width = 135;
            GVEmployee.Cols[3].Width = 60;
            GVEmployee.Cols[4].Width = 90;
            GVEmployee.Cols[5].Width = 90;
            GVEmployee.Cols[6].Width = 90;
            GVEmployee.Cols[7].Width = 90;
            GVEmployee.Cols[8].Width = 100;
            GVEmployee.Cols[9].Width = 100;
            GVEmployee.Cols[10].Width = 55;
            GVEmployee.Cols[11].Width = 55;
            GVEmployee.Cols[12].Width = 55;

            for (int i = 4; i < 10; i++)
            {
                GVEmployee.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            GVEmployee.Cols[10].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Cols[11].TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Cols[12].TextAlign = TextAlignEnum.CenterCenter;
        }
        private void InitData_GVData(DataTable TableView)
        {
            float Toatal4 = 0;
            float Toatal5 = 0;
            float Toatal6 = 0;
            float Toatal7 = 0;
            float Toatal8 = 0;
            float Toatal9 = 0;
            int zRow = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                GVEmployee.Rows.Add();
                DataRow rData = TableView.Rows[rIndex];

                GVEmployee.Rows[rIndex + 1][0] = rIndex + 1;
                GVEmployee.Rows[rIndex + 1][1] = rData[1];
                GVEmployee.Rows[rIndex + 1][2] = rData[2];
                GVEmployee.Rows[rIndex + 1][3] = rData[3];
                GVEmployee.Rows[rIndex + 1][4] = rData[4].Ton2String();
                GVEmployee.Rows[rIndex + 1][5] = rData[5].Ton2String(); 
                GVEmployee.Rows[rIndex + 1][6] = rData[6].Ton2String();
                GVEmployee.Rows[rIndex + 1][7] = rData[7].Ton2String();
                GVEmployee.Rows[rIndex + 1][8] = rData[8].Ton2String();
                GVEmployee.Rows[rIndex + 1][9] = rData[9].Ton2String();
                if (rData[10].ToInt() == 1)
                {
                    GVEmployee.Rows[rIndex + 1][10] = "X";
                    GVEmployee.Rows[rIndex + 1].StyleNew.BackColor = Color.LightGreen;
                }
                else
                {
                    GVEmployee.Rows[rIndex + 1][10] = "";
                }
                if (rData[11].ToInt() == 1)
                {
                    GVEmployee.Rows[rIndex + 1][11] = "X";
                }
                else
                {
                    GVEmployee.Rows[rIndex + 1][11] = "";
                }
                if (rData[12].ToInt() == 1)
                {
                    GVEmployee.Rows[rIndex + 1][12] = "X";
                }
                else
                {
                    GVEmployee.Rows[rIndex + 1][12] = "";
                }
                Toatal4 += float.Parse(rData[4].ToString()) ;
                Toatal5 += float.Parse(rData[5].ToString());
                Toatal6 += float.Parse(rData[6].ToString());
                Toatal7 += float.Parse(rData[7].ToString());
                Toatal8 += float.Parse(rData[8].ToString());
                Toatal9 += float.Parse(rData[9].ToString());
                zRow++;
            }
            GVEmployee.Rows.Add();
            GVEmployee.Rows[zRow][4] = Toatal4.Ton2String();
            GVEmployee.Rows[zRow][5] = Toatal5.Ton2String();
            GVEmployee.Rows[zRow][6] = Toatal6.Ton2String();
            GVEmployee.Rows[zRow][7] = Toatal7.Ton2String();
            GVEmployee.Rows[zRow][8] = Toatal8.Ton2String();
            GVEmployee.Rows[zRow][9] = Toatal9.Ton2String();
            GVEmployee.Rows[zRow].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        ///
        // đơn hàng chia lại lần 1
        void InitGV_Layout_GVAdjustView1()
        {
            GV_AdjustView1.Rows.Count = 0;
            GV_AdjustView1.Cols.Count = 0;
            GV_AdjustView1.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 16;

            GV_AdjustView1.Cols.Add(ToTalCol);
            GV_AdjustView1.Rows.Add(1);

            //Row Header
            GV_AdjustView1.Rows[0][0] = "Stt";
            GV_AdjustView1.Rows[0][1] = "Mã NV";
            GV_AdjustView1.Rows[0][2] = "Tên Nhân Viên";
            GV_AdjustView1.Rows[0][3] = "Nhóm";
            GV_AdjustView1.Rows[0][4] = "Giờ nhập";
            GV_AdjustView1.Rows[0][5] = "Mượn";
            GV_AdjustView1.Rows[0][6] = "Ăn Riêng";
            GV_AdjustView1.Rows[0][7] = "Ăn Chung";
            GV_AdjustView1.Rows[0][8] = "TG đem về";
            GV_AdjustView1.Rows[0][9] = "TG chia lại";
            GV_AdjustView1.Rows[0][10] = "Rổ đem về";
            GV_AdjustView1.Rows[0][11] = "Rổ chia lại";
            GV_AdjustView1.Rows[0][12] = "Số kí đem về";
            GV_AdjustView1.Rows[0][13] = "Số kí chia lại";
            GV_AdjustView1.Rows[0][14] = "Tiền đem về";
            GV_AdjustView1.Rows[0][15] = "Tiền chia lại";

               

            //Style         
            GV_AdjustView1.AllowFreezing = AllowFreezingEnum.Both;
            GV_AdjustView1.AllowResizing = AllowResizingEnum.Both;
            GV_AdjustView1.AllowMerging = AllowMergingEnum.FixedOnly;
            GV_AdjustView1.SelectionMode = SelectionModeEnum.Row;
            GV_AdjustView1.VisualStyle = VisualStyle.Office2010Blue;
            GV_AdjustView1.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GV_AdjustView1.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GV_AdjustView1.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GV_AdjustView1.Rows.Fixed = 1;
            GV_AdjustView1.Cols.Frozen = 4;

            GV_AdjustView1.Cols[0].StyleNew.BackColor = Color.Empty;
            GV_AdjustView1.Cols[1].StyleNew.BackColor = Color.Empty;
            GV_AdjustView1.Cols[2].StyleNew.BackColor = Color.Empty;
            GV_AdjustView1.Cols[3].StyleNew.BackColor = Color.Empty;

            GV_AdjustView1.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GV_AdjustView1.Rows[0].Height = 40;

            GV_AdjustView1.Cols[0].Width = 30;
            GV_AdjustView1.Cols[1].Width = 55;
            GV_AdjustView1.Cols[2].Width = 135;
            GV_AdjustView1.Cols[3].Width = 60;
            GV_AdjustView1.Cols[4].Width = 60;
            GV_AdjustView1.Cols[5].Width = 45;
            GV_AdjustView1.Cols[6].Width = 45;
            GV_AdjustView1.Cols[7].Width = 45;
            GV_AdjustView1.Cols[8].Width = 70;
            GV_AdjustView1.Cols[9].Width = 70;
            GV_AdjustView1.Cols[10].Width = 70;
            GV_AdjustView1.Cols[11].Width = 70;
            GV_AdjustView1.Cols[12].Width = 95;
            GV_AdjustView1.Cols[13].Width = 95;
            GV_AdjustView1.Cols[14].Width = 95;
            GV_AdjustView1.Cols[15].Width = 85;


            GV_AdjustView1.Cols[4].TextAlign = TextAlignEnum.CenterCenter;
            GV_AdjustView1.Cols[5].TextAlign = TextAlignEnum.CenterCenter;
            GV_AdjustView1.Cols[6].TextAlign = TextAlignEnum.CenterCenter;
            GV_AdjustView1.Cols[7].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 8; i < 16; i++)
            {
                GV_AdjustView1.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

        }
        private void InitData_GVAdjustView1(DataTable TableView)
        {
            float Toatal4 = 0;
            float Toatal8 = 0;
            float Toatal9 = 0;
            float Toatal10 = 0;
            float Toatal11 = 0;
            float Toatal12 = 0;
            float Toatal13 = 0;
            float Toatal14 = 0;
            float Toatal15 = 0;
            int zRow = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                GV_AdjustView1.Rows.Add();
                DataRow rData = TableView.Rows[rIndex];

                GV_AdjustView1.Rows[rIndex + 1][0] = rIndex + 1;
                GV_AdjustView1.Rows[rIndex + 1][1] = rData[1];
                GV_AdjustView1.Rows[rIndex + 1][2] = rData[2];
                GV_AdjustView1.Rows[rIndex + 1][3] = rData[3];
                GV_AdjustView1.Rows[rIndex + 1][4] = rData[4].Ton1String(); ;
                if (rData[5].ToInt() == 1)
                {
                    GV_AdjustView1.Rows[rIndex + 1][5] = "X";
                    GV_AdjustView1.Rows[rIndex + 1].StyleNew.BackColor = Color.LightGreen;
                }
                else
                {
                    GV_AdjustView1.Rows[rIndex + 1][5] = "";
                }
                if (rData[6].ToInt() == 1)
                {
                    GV_AdjustView1.Rows[rIndex + 1][6] = "X";
                }
                else
                {
                    GV_AdjustView1.Rows[rIndex + 1][6] = "";
                }
                if (rData[7].ToInt() == 1)
                {
                    GV_AdjustView1.Rows[rIndex + 1][7] = "X";
                }
                else
                {
                    GV_AdjustView1.Rows[rIndex + 1][7] = "";
                }

                GV_AdjustView1.Rows[rIndex + 1][8] = rData[8].Ton2String();
                GV_AdjustView1.Rows[rIndex + 1][9] = rData[9].Ton2String();
                GV_AdjustView1.Rows[rIndex + 1][10] = rData[10].Ton2String();
                GV_AdjustView1.Rows[rIndex + 1][11] = rData[11].Ton2String();
                GV_AdjustView1.Rows[rIndex + 1][12] = rData[12].Ton2String();
                GV_AdjustView1.Rows[rIndex + 1][13] = rData[13].Ton2String();
                GV_AdjustView1.Rows[rIndex + 1][14] = rData[14].Ton2String();
                GV_AdjustView1.Rows[rIndex + 1][15] = rData[15].Ton2String();

                Toatal4 += float.Parse(rData[4].ToString());
                Toatal8 += float.Parse(rData[8].ToString());
                Toatal9 += float.Parse(rData[9].ToString());
                Toatal10 += float.Parse(rData[10].ToString());
                Toatal11 += float.Parse(rData[11].ToString());
                Toatal12 += float.Parse(rData[12].ToString());
                Toatal13 += float.Parse(rData[13].ToString());
                Toatal14 += float.Parse(rData[14].ToString());
                Toatal15 += float.Parse(rData[15].ToString());
                zRow++;
            }
            GV_AdjustView1.Rows.Add();
            GV_AdjustView1.Rows[zRow][4] = Toatal4.Ton2String();
            GV_AdjustView1.Rows[zRow][8] = Toatal8.Ton2String();
            GV_AdjustView1.Rows[zRow][9] = Toatal9.Ton2String();
            GV_AdjustView1.Rows[zRow][10] = Toatal10.Ton2String();
            GV_AdjustView1.Rows[zRow][11] = Toatal11.Ton2String();
            GV_AdjustView1.Rows[zRow][12] = Toatal12.Ton2String();
            GV_AdjustView1.Rows[zRow][13] = Toatal13.Ton2String();
            GV_AdjustView1.Rows[zRow][14] = Toatal14.Ton2String();
            GV_AdjustView1.Rows[zRow][15] = Toatal15.Ton2String();

            GV_AdjustView1.Rows[zRow].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        //don hàng chia lại lần 2
        void InitGV_Layout_GVAdjustView2()
        {
            GV_AdjustView2.Rows.Count = 0;
            GV_AdjustView2.Cols.Count = 0;
            GV_AdjustView2.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 16;

            GV_AdjustView2.Cols.Add(ToTalCol);
            GV_AdjustView2.Rows.Add(1);

            //Row Header
            GV_AdjustView2.Rows[0][0] = "Stt";
            GV_AdjustView2.Rows[0][1] = "Mã NV";
            GV_AdjustView2.Rows[0][2] = "Tên Nhân Viên";
            GV_AdjustView2.Rows[0][3] = "Nhóm";
            GV_AdjustView2.Rows[0][4] = "Giờ nhập";
            GV_AdjustView2.Rows[0][5] = "Mượn";
            GV_AdjustView2.Rows[0][6] = "Ăn Riêng";
            GV_AdjustView2.Rows[0][7] = "Ăn Chung";
            GV_AdjustView2.Rows[0][8] = "TG đem về";
            GV_AdjustView2.Rows[0][9] = "TG chia lại";
            GV_AdjustView2.Rows[0][10] = "Rổ đem về";
            GV_AdjustView2.Rows[0][11] = "Rổ chia lại";
            GV_AdjustView2.Rows[0][12] = "Số kí đem về";
            GV_AdjustView2.Rows[0][13] = "Số kí chia lại";
            GV_AdjustView2.Rows[0][14] = "Tiền đem về";
            GV_AdjustView2.Rows[0][15] = "Tiền chia lại";



            //Style         
            GV_AdjustView2.AllowFreezing = AllowFreezingEnum.Both;
            GV_AdjustView2.AllowResizing = AllowResizingEnum.Both;
            GV_AdjustView2.AllowMerging = AllowMergingEnum.FixedOnly;
            GV_AdjustView2.SelectionMode = SelectionModeEnum.Row;
            GV_AdjustView2.VisualStyle = VisualStyle.Office2010Blue;
            GV_AdjustView2.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GV_AdjustView2.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GV_AdjustView2.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GV_AdjustView2.Rows.Fixed = 1;
            GV_AdjustView2.Cols.Frozen = 4;

            GV_AdjustView2.Cols[0].StyleNew.BackColor = Color.Empty;
            GV_AdjustView2.Cols[1].StyleNew.BackColor = Color.Empty;
            GV_AdjustView2.Cols[2].StyleNew.BackColor = Color.Empty;
            GV_AdjustView2.Cols[3].StyleNew.BackColor = Color.Empty;

            GV_AdjustView2.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GV_AdjustView2.Rows[0].Height = 40;

            GV_AdjustView2.Cols[0].Width = 30;
            GV_AdjustView2.Cols[1].Width = 55;
            GV_AdjustView2.Cols[2].Width = 135;
            GV_AdjustView2.Cols[3].Width = 60;
            GV_AdjustView2.Cols[4].Width = 60;
            GV_AdjustView2.Cols[5].Width = 45;
            GV_AdjustView2.Cols[6].Width = 45;
            GV_AdjustView2.Cols[7].Width = 45;
            GV_AdjustView2.Cols[8].Width = 70;
            GV_AdjustView2.Cols[9].Width = 70;
            GV_AdjustView2.Cols[10].Width = 70;
            GV_AdjustView2.Cols[11].Width = 70;
            GV_AdjustView2.Cols[12].Width = 95;
            GV_AdjustView2.Cols[13].Width = 95;
            GV_AdjustView2.Cols[14].Width = 95;
            GV_AdjustView2.Cols[15].Width = 85;


            GV_AdjustView2.Cols[4].TextAlign = TextAlignEnum.CenterCenter;
            GV_AdjustView2.Cols[5].TextAlign = TextAlignEnum.CenterCenter;
            GV_AdjustView2.Cols[6].TextAlign = TextAlignEnum.CenterCenter;
            GV_AdjustView2.Cols[7].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 8; i < 16; i++)
            {
                GV_AdjustView2.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

        }
        private void InitData_GVAdjustView2(DataTable TableView)
        {
            float Toatal4 = 0;
            float Toatal8 = 0;
            float Toatal9 = 0;
            float Toatal10 = 0;
            float Toatal11 = 0;
            float Toatal12 = 0;
            float Toatal13 = 0;
            float Toatal14 = 0;
            float Toatal15 = 0;
            int zRow = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                GV_AdjustView2.Rows.Add();
                DataRow rData = TableView.Rows[rIndex];

                GV_AdjustView2.Rows[rIndex + 1][0] = rIndex + 1;
                GV_AdjustView2.Rows[rIndex + 1][1] = rData[1];
                GV_AdjustView2.Rows[rIndex + 1][2] = rData[2];
                GV_AdjustView2.Rows[rIndex + 1][3] = rData[3];
                GV_AdjustView2.Rows[rIndex + 1][4] = rData[4].Ton1String(); ;
                if (rData[5].ToInt() == 1)
                {
                    GV_AdjustView2.Rows[rIndex + 1][5] = "X";
                    GV_AdjustView2.Rows[rIndex + 1].StyleNew.BackColor = Color.LightGreen;
                }
                else
                {
                    GV_AdjustView2.Rows[rIndex + 1][5] = "";
                }
                if (rData[6].ToInt() == 1)
                {
                    GV_AdjustView2.Rows[rIndex + 1][6] = "X";
                }
                else
                {
                    GV_AdjustView2.Rows[rIndex + 1][6] = "";
                }
                if (rData[7].ToInt() == 1)
                {
                    GV_AdjustView2.Rows[rIndex + 1][7] = "X";
                }
                else
                {
                    GV_AdjustView2.Rows[rIndex + 1][7] = "";
                }

                GV_AdjustView2.Rows[rIndex + 1][8] = rData[8].Ton2String();
                GV_AdjustView2.Rows[rIndex + 1][9] = rData[9].Ton2String();
                GV_AdjustView2.Rows[rIndex + 1][10] = rData[10].Ton2String();
                GV_AdjustView2.Rows[rIndex + 1][11] = rData[11].Ton2String();
                GV_AdjustView2.Rows[rIndex + 1][12] = rData[12].Ton2String();
                GV_AdjustView2.Rows[rIndex + 1][13] = rData[13].Ton2String();
                GV_AdjustView2.Rows[rIndex + 1][14] = rData[14].Ton2String();
                GV_AdjustView2.Rows[rIndex + 1][15] = rData[15].Ton2String();

                Toatal4 += float.Parse(rData[4].ToString());
                Toatal8 += float.Parse(rData[8].ToString());
                Toatal9 += float.Parse(rData[9].ToString());
                Toatal10 += float.Parse(rData[10].ToString());
                Toatal11 += float.Parse(rData[11].ToString());
                Toatal12 += float.Parse(rData[12].ToString());
                Toatal13 += float.Parse(rData[13].ToString());
                Toatal14 += float.Parse(rData[14].ToString());
                Toatal15 += float.Parse(rData[15].ToString());
                zRow++;
            }
            GV_AdjustView2.Rows.Add();
            GV_AdjustView2.Rows[zRow][4] = Toatal4.Ton2String();
            GV_AdjustView2.Rows[zRow][8] = Toatal8.Ton2String();
            GV_AdjustView2.Rows[zRow][9] = Toatal9.Ton2String();
            GV_AdjustView2.Rows[zRow][10] = Toatal10.Ton2String();
            GV_AdjustView2.Rows[zRow][11] = Toatal11.Ton2String();
            GV_AdjustView2.Rows[zRow][12] = Toatal12.Ton2String();
            GV_AdjustView2.Rows[zRow][13] = Toatal13.Ton2String();
            GV_AdjustView2.Rows[zRow][14] = Toatal14.Ton2String();
            GV_AdjustView2.Rows[zRow][15] = Toatal15.Ton2String();

            GV_AdjustView2.Rows[zRow].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion
        private void btn_Show_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 200;
            btn_Hide.Visible = true;
            btn_Show.Visible = false;

            Panel_Info.Visible = true;
        }

        private void btn_Hide_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = Screen.PrimaryScreen.Bounds.Width;
            btn_Hide.Visible = false;
            btn_Show.Visible = true;

            Panel_Info.Visible = false;
        }
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region[Event]
        private void txt_OrderIDFollow_Leave(object sender, EventArgs e)
        {
            if (txt_OrderIDFollow.Text.Trim().Length > 0)
            {
                if (txt_OrderIDFollow.Text.Trim().ToUpper() == "CVTG")
                {
                    txt_OrderIDFollow.StateCommon.Back.Color1 = ColorNormal;
                }
                else
                {
                    int Count = Order_Data.CountOrderIDFollow(txt_OrderIDFollow.Text.Trim());
                    if (Count <= 0)
                    {
                        txt_OrderIDFollow.StateCommon.Back.Color1 = Color.FromArgb(199, 21, 112);
                    }
                    else
                    {
                        txt_OrderIDFollow.StateCommon.Back.Color1 = Color.FromArgb(255, 255, 255);
                    }
                }
            }
            else
            {
                txt_OrderIDFollow.StateCommon.Back.Color1 = Color.FromArgb(255, 255, 255);
            }
        }
        private void txt_Search_ProductID_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LV_Product.Focus();
                if (LV_Product.Items.Count > 0)
                    LV_Product.Items[0].Selected = true;
            }
            else
            {
                if (e.KeyCode == Keys.Enter)
                {
                    if (txt_Search_ProductID.Text.Length > 0)
                    {
                        LV_Product.Focus();
                        LV_Product.Visible = true;
                        LV_Product.BringToFront();
                        LoadLVProduct();
                    }
                    else
                        LV_Product.Visible = false;
                }
            }
        }
        private void LV_Product_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            txt_Search_ProductID.Tag = LV_Product.Items[e.ItemIndex].Tag.ToString();
            txt_Search_ProductID.Text = LV_Product.Items[e.ItemIndex].SubItems[1].Text.Trim();
            txt_Search_ProductName.Text = LV_Product.Items[e.ItemIndex].SubItems[2].Text.Trim();
        }
        private void LV_Product_ItemActivate(object sender, EventArgs e)
        {
            txt_Search_ProductID.Tag = LV_Product.SelectedItems[0].Tag;
            txt_Search_ProductID.Text = LV_Product.SelectedItems[0].SubItems[1].Text.Trim();
            txt_Search_ProductName.Text = LV_Product.SelectedItems[0].SubItems[2].Text.Trim();
            txt_Search_ProductName.Focus();
            LV_Product.Visible = false;
        }
        private void LV_Product_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_Search_ProductID.Tag = LV_Product.SelectedItems[0].Tag;
                txt_Search_ProductID.Text = LV_Product.SelectedItems[0].SubItems[1].Text.Trim();
                txt_Search_ProductName.Text = LV_Product.SelectedItems[0].SubItems[2].Text.Trim();
                txt_Search_ProductName.Focus();
                LV_Product.Visible = false;
            }
        }
        private void txt_Search_ProductID_Leave(object sender, EventArgs e)
        {
            if (!LV_Product.Focused)
                LV_Product.Visible = false;

            if (txt_Search_ProductID.Text.Trim().Length == 0)
            {
                txt_Search_ProductID.Tag = null;
                txt_Search_ProductName.Clear();
            }
            else
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(txt_Search_ProductID.Text.Trim());
                txt_Search_ProductName.Text = zProduct.ProductName;
            }
        }
        private void txt_Search_WorkID_Leave(object sender, EventArgs e)
        {
            if (txt_Search_WorkID.Text.Trim().Length == 0)
            {
                txt_Search_WorkName.Clear();
            }
        }
        private void cbo_TeamID_Search_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_TeamID_Search.SelectedIndex >= 0)
            {
                _TeamKey_Search = cbo_TeamID_Search.SelectedValue.ToInt();
            }
        }
        
        private void txt_OrderID_Leave(object sender, EventArgs e)
        {
            if (txt_OrderID.Text.Trim().Length == 0)
            {
                txt_OrderID.StateCommon.Back.Color1 = ColorWarning;
            }
            else
            {
                if (txtHeader.Tag != null && _IsNew == true)
                {
                    string OldID = txtHeader.Tag.ToString();
                    if (txt_OrderID.Text.ToUpper().Trim() == OldID.ToUpper().Trim())
                    {
                        txt_OrderID.StateCommon.Back.Color1 = ColorWarning;
                        MessageBox.Show("Mã đơn hàng không được trùng !.");
                    }
                }
            }
        }
        private void txt_WorkID_Leave(object sender, EventArgs e)
        {
            if (txt_WorkID.Text.Trim().Length == 0)
            {
                txt_WorkName.Clear();
                txt_Price.Clear();
            }
            else
            {
                Product_Stages_Info zStage = new Product_Stages_Info();
                zStage.GetStagesID_Info(txt_WorkID.Text.Trim());
                txt_WorkGroup.Text = zStage.GroupID;
                if (zStage.Key == 0)
                {
                    txt_WorkID.StateCommon.Back.Color1 = ColorWarning;
                    txt_WorkID.Tag = null;
                }
                else
                {
                    txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
                }
            }
        }
        private void txt_ProductID_Leave(object sender, EventArgs e)
        {

        }
        private void txt_Number_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void btn_Search_Work_Click(object sender, EventArgs e)
        {
            Frm_Setup_Work Frm = new Frm_Setup_Work();
            Frm.TeamKey = int.Parse(cbo_TeamID_Search.SelectedValue.ToString());
            Frm.SearchStyle = true;
            Frm.ShowDialog();

            Product_Stages_Info zInfo = Frm.Product_Stages;
            if (zInfo != null)
            {
                txt_Search_WorkID.Tag = zInfo.Key;
                txt_Search_WorkID.Text = zInfo.StagesID;
                txt_Search_WorkName.Text = zInfo.StageName;
            }
        }
        private void btn_Search_Product_Click(object sender, EventArgs e)
        {

        }
        private void btn_OpenSearch_Click(object sender, EventArgs e)
        {
            if (!Panel_Search.Visible)
            {
                btn_OpenSearch.Text = "Ẩn -";
                Panel_Search.Visible = true;
            }
            else
            {
                btn_OpenSearch.Text = "Mở -";
                Panel_Search.Visible = false;
            }
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            using (Frm_Loading frm = new Frm_Loading(LoadData)) { frm.ShowDialog(this); }
            Cursor = Cursors.Default;
        }

        private void btn_SearchInput_Work_Click(object sender, EventArgs e)
        {
            Frm_Setup_Work Frm = new Frm_Setup_Work();
            Frm.TeamKey = int.Parse(cbo_Team.SelectedValue.ToString());
            Frm.SearchStyle = true;
            Frm.ShowDialog();

            Product_Stages_Info zInfo = Frm.Product_Stages;
            if (zInfo != null)
            {
                txt_WorkID.Tag = zInfo.Key;
                txt_WorkID.Text = zInfo.StagesID;
                txt_WorkName.Text = zInfo.StageName;
                txt_Price.Text = zInfo.Price.Ton2String();

                txt_WorkID.StateCommon.Back.Color1 = ColorNormal;
            }
        }
        private void btn_SearchInput_Document_Click(object sender, EventArgs e)
        {
            Frm_Search_Order frm = new Frm_Search_Order();
            frm.ShowDialog();
            if (frm.ProductKey.Length > 0)
            {
                txt_ProductID.Tag = frm.ProductKey;
                txt_OrderIDFollow.Text = frm.OrderID;
                txt_ProductName.Text = frm.ProductName;
                txt_ProductID.Text = frm.ProductID;
                txt_QuantityDocument.Text = frm.Quantity_Document.ToString();
                txt_Unit.Tag = frm.UnitKey;
                txt_Unit.Text = frm.UnitName;

                txt_ProductID.StateCommon.Back.Color1 = ColorNormal;
            }
        }
       
        #endregion
        private void LoadData()
        {
            DateTime FromDate = dte_FromDate.Value;
            DateTime ToDate = dte_ToDate.Value;
            string ProductID = txt_Search_ProductID.Text.Trim();
            string WorkID = txt_Search_WorkID.Text.Trim();
            string OrderID = txt_Search_IDOrder.Text.Trim();
            string OrderIDOriginal = txt_Search_IDOrder_Original.Text.Trim();

            DataTable zTable = Order_Data.List_Search(FromDate, ToDate, ProductID, WorkID, OrderIDOriginal, OrderID, _TeamKey_Search);
            this.Invoke(new MethodInvoker(delegate ()
            {
                LoadGVOrder(zTable);
            }));
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Năng suất đơn hàng " + txt_OrderID.Text + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVEmployee.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void Btn_Export1_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Năng suất chia lại lần 1 đơn hàng " + txt_OrderID.Text + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GV_AdjustView1.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void Btn_Export2_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Năng suất chia lại lần 2 đơn hàng " + txt_OrderID.Text + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GV_AdjustView1.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }


    }
}