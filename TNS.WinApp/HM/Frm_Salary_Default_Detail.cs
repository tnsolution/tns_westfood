﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Salary_Default_Detail : Form
    {
        private int _Key = 0;
        public int Key
        {
            get
            {
                return _Key;
            }

            set
            {
                _Key = value;
            }
        }
        public Frm_Salary_Default_Detail()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Copy.Click += Btn_Copy_Click;

            btnMini.Click += btnMini_Click;
            btnClose.Click += btnClose_Click;
            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            txt_Amount.Leave += Txt_Number_Leave;
            this.MouseDown += Frm_Main_MouseDown;
            this.MouseMove += Frm_Main_MouseMove;
            this.MouseUp += Frm_Main_MouseUp;
        }

        

        private void Frm_Salary_Default_Detail_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Salary, "SELECT AutoKey,ID+'-'+Name FROM HRM_Fee_Category WHERE RecordStatus <> 99 AND Slug=0  ORDER BY [Rank]", "--Chọn--");
            if (Key==0)
            SetDefault();
            else
            LoadData();
        }
        private void Txt_Number_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Amount.Text.Trim(), out zAmount))
            {
                txt_Amount.Text = zAmount.Toe1String();
                txt_Amount.SelectionStart = txt_Amount.Text.Length;
            }
            else
            {
                MessageBox.Show("Lỗi!.Định dạng số.");
            }
        }
        private void SetDefault()
        {
            Key = 0;
            txt_EmployeeID.Tag = null;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            txt_Amount.Text = "0";
            txt_Description.Text = "";
            cbo_Salary.SelectedValue = "0";
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value =  DateTime.MinValue;
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_EmployeeID.Focus();
        }
        private void LoadData()
        {
            Employee_SalaryDefault_Info zinfo = new Employee_SalaryDefault_Info(Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            float zValue = float.Parse(zinfo.Value.ToString());
            txt_Amount.Text = zValue.Ton0String();
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            if (zinfo.ToDate == null)
            {
                dte_ToDate.Value = DateTime.MinValue;
                btn_Copy.Enabled = false;
            }
            else
            {
                dte_ToDate.Value = zinfo.ToDate;
                btn_Copy.Enabled = true;
            }
            cbo_Salary.SelectedValue = zinfo.CategoryKey;
            btn_Del.Enabled = true;

            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã nhân viên!");
                return;
            }
            if (cbo_Salary.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chọn danh mục lương!");
                return;
            }
            float Value = 0;
            if(!float.TryParse(txt_Amount.Text.Trim(),out Value))
            {
                MessageBox.Show("Giá trị/ số tiền không đúng!");
                return;
            }
            else
            {
                Employee_SalaryDefault_Info zinfo = new Employee_SalaryDefault_Info(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim(). ToString();
                zinfo.Value = float.Parse(txt_Amount.Text.Trim());
                zinfo.Description = txt_Description.Text;

                DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
                DateTime zToDate = DateTime.MinValue;
                if (dte_ToDate.Value != DateTime.MinValue)
                {
                    zToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
                }
                zinfo.FromDate = zFromDate;
                zinfo.ToDate = zToDate;

                zinfo.CategoryKey = cbo_Salary.SelectedValue.ToInt();
                string[] s = cbo_Salary.Text.Split('-');
                zinfo.CategoryID = s[0];
                zinfo.CategoryName =s[1];
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "11" || zinfo.Message == "20")
                {
                    MessageBox.Show("Cập nhật thành công!");
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show(zinfo.Message);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Employee_SalaryDefault_Info zinfo = new Employee_SalaryDefault_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        MessageBox.Show("Đã xóa !");
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            Employee_SalaryDefault_Info zinfo = new Employee_SalaryDefault_Info(Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            float zValue = float.Parse(zinfo.Value.ToString());
            txt_Amount.Text = zValue.Ton0String();
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.FromDate;
            if (zinfo.ToDate == null)
                dte_ToDate.Value = DateTime.MinValue;
            else
                dte_ToDate.Value = zinfo.ToDate;
            cbo_Salary.SelectedValue = zinfo.CategoryKey;
            _Key = 0;

            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            MessageBox.Show("Sao chép thành công!Vui lòng chỉnh sửa thông tin.");
        }

        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
