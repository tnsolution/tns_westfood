﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.LOG
{
    public class Application_Data
    {
        public static DataTable List(DateTime FromDate, DateTime ToDate, string UserKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT CreatedOn ,ActionName ,ActionType,RowGuid,Form FROM [dbo].[LOG_Application] 
WHERE CreatedOn BETWEEN @FromDate AND @ToDate
AND UserKey =@UserKey
ORDER BY CreatedOn DESC
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string Save_LogApplication(string UserKey,string EmployeeKey, string ActionName, int Type)
        {

            string zSQL = @"INSERT INTO LOG_Application (
UserKey,EmployeeKey ,DateLog ,ActionName,ActionType ,RecordStatus ,CreatedOn ,ModifiedOn  ) 
VALUES (
@UserKey,@EmployeeKey ,GETDATE() ,@ActionName ,@ActionType,0 ,GETDATE(),GETDATE() )";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(UserKey);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = ActionName;
                zCommand.Parameters.Add("@ActionType", SqlDbType.Int).Value = Type;
                string zResult = "";
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return string.Empty;
        }
        public static DataTable List_LogForm(DateTime DateWrite, int Form)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT CreatedOn ,CreatedName,ActionName FROM [dbo].[LOG_Application] 
WHERE DateLog BETWEEN @FromDate AND @ToDate
AND Form IS NOT NULL AND Form = @Form  
ORDER BY CreatedOn DESC
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Form", SqlDbType.Int).Value = Form;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListLogAll(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * FROM (
	(SELECT CreatedOn ,[dbo].[Fn_GetUserName](UserKey) AS UserName,ActionName ,ActionType,CAST(RowGuid AS NVARCHAR(50)) AS RowGuid,Form FROM [dbo].[LOG_Application] 
	WHERE CreatedOn BETWEEN @FromDate AND @ToDate)
	UNION
	(SELECT DateLogin AS CreateOn,[dbo].[Fn_GetUserName](UserKey) AS UserName,Description,0,'' ,''
	FROM [dbo].[LOG_Login] 
	WHERE DateLogin BETWEEN @FromDate AND @ToDate)
	)X
	ORDER BY X.CreatedOn DESC 
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
