﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Order_Adjusted_Info
    {
        private int _AutoKey;
        private int _TeamKey = 0;
        private string _TeamID = "";
        private string _TeamName = "";
        private string _OrderKey = "";
        private string _OrderIDCompare = "";
        private string _OrderID = "";
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private string _EmployeeID = "";
        private DateTime _OrderDate;
        private int _StageKey = 0;
        private string _StageID = "";
        private string _StageName = "";
        private float _Price = 0;
        private float _Time = 0;//Số giờ người dùng nhập
        private float _TimeEat = 0;//Số giờ đem về
        private float _Kg = 0;
        private float _Basket = 0;
        private float _Money = 0;
        private int _Borrow = 0;
        private int _Private = 0;
        private int _Share = 0;
        private float _KgProduct = 0;
        private float _TimeProduct = 0;
        private float _BasketProduct = 0;
        private int _CategoryKey = 0;
        private string _CategoryName = "";
        private float _MoneyPersonal = 0;
        private float _MoneyShare = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _Message = "";

        #region[Properties]
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public int TeamKey
        {
            get
            {
                return _TeamKey;
            }

            set
            {
                _TeamKey = value;
            }
        }

        public string TeamID
        {
            get
            {
                return _TeamID;
            }

            set
            {
                _TeamID = value;
            }
        }

        public string TeamName
        {
            get
            {
                return _TeamName;
            }

            set
            {
                _TeamName = value;
            }
        }

        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }

        public string OrderIDCompare
        {
            get
            {
                return _OrderIDCompare;
            }

            set
            {
                _OrderIDCompare = value;
            }
        }

        public string OrderID
        {
            get
            {
                return _OrderID;
            }

            set
            {
                _OrderID = value;
            }
        }

        public string EmployeeKey
        {
            get
            {
                return _EmployeeKey;
            }

            set
            {
                _EmployeeKey = value;
            }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public string EmployeeID
        {
            get
            {
                return _EmployeeID;
            }

            set
            {
                _EmployeeID = value;
            }
        }

        public DateTime OrderDate
        {
            get
            {
                return _OrderDate;
            }

            set
            {
                _OrderDate = value;
            }
        }

        public int StageKey
        {
            get
            {
                return _StageKey;
            }

            set
            {
                _StageKey = value;
            }
        }

        public string StageID
        {
            get
            {
                return _StageID;
            }

            set
            {
                _StageID = value;
            }
        }

        public string StageName
        {
            get
            {
                return _StageName;
            }

            set
            {
                _StageName = value;
            }
        }

        public float Time
        {
            get
            {
                return _Time;
            }

            set
            {
                _Time = value;
            }
        }

        public float Kg
        {
            get
            {
                return _Kg;
            }

            set
            {
                _Kg = value;
            }
        }

        public float Money
        {
            get
            {
                return _Money;
            }

            set
            {
                _Money = value;
            }
        }

        public int Borrow
        {
            get
            {
                return _Borrow;
            }

            set
            {
                _Borrow = value;
            }
        }

        public int Private
        {
            get
            {
                return _Private;
            }

            set
            {
                _Private = value;
            }
        }

        public int Share
        {
            get
            {
                return _Share;
            }

            set
            {
                _Share = value;
            }
        }

        public float KgProduct
        {
            get
            {
                return _KgProduct;
            }

            set
            {
                _KgProduct = value;
            }
        }

        public int CategoryKey
        {
            get
            {
                return _CategoryKey;
            }

            set
            {
                _CategoryKey = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return _CategoryName;
            }

            set
            {
                _CategoryName = value;
            }
        }

        public float MoneyPersonal
        {
            get
            {
                return _MoneyPersonal;
            }

            set
            {
                _MoneyPersonal = value;
            }
        }

        public float MoneyShare
        {
            get
            {
                return _MoneyShare;
            }

            set
            {
                _MoneyShare = value;
            }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        public float Price
        {
            get
            {
                return _Price;
            }

            set
            {
                _Price = value;
            }
        }

        public float TimeEat
        {
            get
            {
                return _TimeEat;
            }

            set
            {
                _TimeEat = value;
            }
        }

        public float Basket
        {
            get
            {
                return _Basket;
            }

            set
            {
                _Basket = value;
            }
        }

        public float TimeProduct
        {
            get
            {
                return _TimeProduct;
            }

            set
            {
                _TimeProduct = value;
            }
        }

        public float BasketProduct
        {
            get
            {
                return _BasketProduct;
            }

            set
            {
                _BasketProduct = value;
            }
        }
        #endregion

        public Order_Adjusted_Info()
        {
        }
        public Order_Adjusted_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FTR_Order_Adjusted WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _OrderKey = zReader["OrderKey"].ToString().Trim();
                    _OrderIDCompare = zReader["OrderIDCompare"].ToString().Trim();
                    _OrderID = zReader["OrderID"].ToString().Trim();
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    if (zReader["OrderDate"] != DBNull.Value)
                        _OrderDate = (DateTime)zReader["OrderDate"];
                    if (zReader["StageKey"] != DBNull.Value)
                        _StageKey = int.Parse(zReader["StageKey"].ToString());
                    _StageID = zReader["StageID"].ToString().Trim();
                    _StageName = zReader["StageName"].ToString().Trim();
                    if (zReader["Price"] != DBNull.Value)
                        _Price = int.Parse(zReader["Price"].ToString());
                    if (zReader["Time"] != DBNull.Value)
                        _Time = float.Parse(zReader["Time"].ToString());
                    if (zReader["TimeEat"] != DBNull.Value)
                        _TimeEat = float.Parse(zReader["TimeEat"].ToString());
                    if (zReader["Kg"] != DBNull.Value)
                        _Kg = float.Parse(zReader["Kg"].ToString());
                    if (zReader["Basket"] != DBNull.Value)
                        _Basket = float.Parse(zReader["Basket"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = float.Parse(zReader["Money"].ToString());
                    if (zReader["Borrow"] != DBNull.Value)
                        _Borrow = int.Parse(zReader["Borrow"].ToString());
                    if (zReader["Private"] != DBNull.Value)
                        _Private = int.Parse(zReader["Private"].ToString());
                    if (zReader["Share"] != DBNull.Value)
                        _Share = int.Parse(zReader["Share"].ToString());
                    if (zReader["KgProduct"] != DBNull.Value)
                        _KgProduct = float.Parse(zReader["KgProduct"].ToString());
                    if (zReader["TimeProduct"] != DBNull.Value)
                        _TimeProduct = float.Parse(zReader["TimeProduct"].ToString());
                    if (zReader["BasketProduct"] != DBNull.Value)
                        _BasketProduct = float.Parse(zReader["BasketProduct"].ToString());
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryName = zReader["CategoryName"].ToString();
                    if (zReader["MoneyPersonal"] != DBNull.Value)
                        _MoneyPersonal = float.Parse(zReader["MoneyPersonal"].ToString());
                    if (zReader["MoneyShare"] != DBNull.Value)
                        _MoneyShare = float.Parse(zReader["MoneyShare"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08 " + Err.ToString();
            }
            finally { zConnect.Close(); }
        }

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO FTR_Order_Adjusted (
            TeamKey,TeamID,TeamName,OrderKey,OrderIDCompare,OrderID,
            EmployeeKey,EmployeeName,EmployeeID,OrderDate,StageKey,StageID,StageName,
            Price,Time,TimeEat,Kg,Basket,Money,Borrow,Private,Share,KgProduct,TimeProduct,BasketProduct,
            CategoryKey,CategoryName,MoneyPersonal,MoneyShare,RecordStatus,
            CreatedOn  ,CreatedBy ,CreatedName,ModifiedOn ,ModifiedBy ,ModifiedName ) 
             VALUES ( 
            @TeamKey,@TeamID,@TeamName,@OrderKey,@OrderIDCompare,@OrderID,
            @EmployeeKey,@EmployeeName,@EmployeeID,@OrderDate,@StageKey,@StageID,@StageName,
            @Price,@Time,@TimeEat,@Kg,@Basket,@Money,@Borrow,@Private,@Share,@KgProduct,@TimeProduct,@BasketProduct,
            @CategoryKey,@CategoryName,@MoneyPersonal,@MoneyShare,0,
            GETDATE() ,@CreatedBy ,@CreatedName,GETDATE() ,@ModifiedBy ,@ModifiedName )
            SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@OrderIDCompare", SqlDbType.NVarChar).Value = _OrderIDCompare;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                if (_OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@StageID", SqlDbType.NVarChar).Value = _StageID;
                zCommand.Parameters.Add("@StageName", SqlDbType.NVarChar).Value = _StageName;
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = _Price;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                zCommand.Parameters.Add("@TimeEat", SqlDbType.Float).Value = _TimeEat;
                zCommand.Parameters.Add("@Kg", SqlDbType.Float).Value = _Kg;
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = _Basket;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Borrow", SqlDbType.Int).Value = _Borrow;
                zCommand.Parameters.Add("@Private", SqlDbType.Int).Value = _Private;
                zCommand.Parameters.Add("@Share", SqlDbType.Int).Value = _Share;
                zCommand.Parameters.Add("@KgProduct", SqlDbType.Float).Value = _KgProduct;
                zCommand.Parameters.Add("@TimeProduct", SqlDbType.Float).Value = _TimeProduct;
                zCommand.Parameters.Add("@BasketProduct", SqlDbType.Float).Value = _BasketProduct;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@MoneyPersonal", SqlDbType.Float).Value = _MoneyPersonal;
                zCommand.Parameters.Add("@MoneyShare", SqlDbType.Float).Value = _MoneyShare;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_CreatedOn == DateTime.MinValue)
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedOn == DateTime.MinValue)
                    zCommand.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = _ModifiedOn;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();

            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @"UPDATE FTR_Order_Adjusted SET 
                        AutoKey =@AutoKey,
                        TeamKey =@TeamKey,
                        TeamID =@TeamID,
                        TeamName =@TeamName,
                        OrderKey =@OrderKey,
                        OrderIDCompare =@OrderIDCompare,
                        OrderID =@OrderID,
                        EmployeeKey = @EmployeeKey,
                        EmployeeName = @EmployeeName,
                        EmployeeID = @EmployeeID,
                        OrderDate = @OrderDate,
                        StageKey = @StageKey,
                        StageID = @StageID,
                        StageName = @StageName,
                        Price=@Price,
                        Time = @Time,
                        TimeEat = @TimeEat,
                        Kg = @Kg,
                        Basket = @Basket,
                        Money = @Money,
                        Borrow = @Borrow,
                        Private = @Private,
                        Share = @Share,
                        KgProduct = @KgProduct,
                        TimeProduct = @TimeProduct,
                        BasketProduct = @BasketProduct,
                        CategoryKey = @CategoryKey,
                        CategoryName = @CategoryName,
                        MoneyPersonal = @MoneyPersonal,
                        MoneyShare, = @MoneyShare,
                        RecordStatus = 1,
                        ModifiedOn = GetDate(),
                        ModifiedBy = @ModifiedBy,
                        ModifiedName = @ModifiedName
                        WHERE AutoKey = @AutoKey
                        SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {

                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@OrderIDCompare", SqlDbType.NVarChar).Value = _OrderIDCompare;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = _OrderID;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                if (_OrderDate == null)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = _OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@StageID", SqlDbType.NVarChar).Value = _StageID;
                zCommand.Parameters.Add("@StageName", SqlDbType.NVarChar).Value = _StageName;
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = _Price;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = _Time;
                zCommand.Parameters.Add("@TimeEat", SqlDbType.Float).Value = _TimeEat;
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = _Basket;
                zCommand.Parameters.Add("@Kg", SqlDbType.Float).Value = _Kg;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Borrow", SqlDbType.Int).Value = _Borrow;
                zCommand.Parameters.Add("@Private", SqlDbType.Int).Value = _Private;
                zCommand.Parameters.Add("@Share", SqlDbType.Int).Value = _Share;
                zCommand.Parameters.Add("@KgProduct", SqlDbType.Float).Value = _KgProduct;
                zCommand.Parameters.Add("@TimeProduct", SqlDbType.Float).Value = _TimeProduct;
                zCommand.Parameters.Add("@BasketProduct", SqlDbType.Float).Value = _BasketProduct;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName;
                zCommand.Parameters.Add("@MoneyPersonal", SqlDbType.Float).Value = _MoneyPersonal;
                zCommand.Parameters.Add("@MoneyShare", SqlDbType.Float).Value = _MoneyShare;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                if (_CreatedOn == null)
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CreatedOn", SqlDbType.DateTime).Value = _CreatedOn;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                if (_ModifiedOn == null)
                    zCommand.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@ModifiedOn", SqlDbType.DateTime).Value = _ModifiedOn;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE FTR_Order_Adjusted SET RecordStatus = 99  WHERE AutoKey = @AutoKey 
                         SELECT 30 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteTeam_OrderShare1(int TeamKey, string OrderKey)
        {

            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"DELETE FROM FTR_Order_Adjusted WHERE TeamKey = @TeamKey AND OrderKey =@OrderKey AND CategoryKey = 1  ";
            zSQL += DeleteTeamOfShare1(TeamKey, OrderKey);
            zSQL += " SELECT 30";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        string DeleteTeamOfShare1(int TeamKey, string OrderKey)
        {
            string zSql = "";

            DataTable zTable = new DataTable();
            zTable = Order_Adjusted_Data.List_TeamOfShare1(TeamKey, OrderKey);
            foreach (DataRow r in zTable.Rows)
            {
                zSql += " DELETE FROM FTR_Order_Adjusted WHERE  TeamKey = " + r["TeamKey"].ToInt() + " AND OrderKey = '" + OrderKey + "' AND  CategoryKey = 2 ";
            }
            return zSql;
        }
        public string DeleteTeam_OrderShare2(int TeamKey, string OrderKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM FTR_Order_Adjusted WHERE TeamKey = @TeamKey AND OrderKey =@OrderKey AND CategoryKey=2  SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteAll(string OrderKey) // bao gồm tất cả các nhóm khác có liên quan đến đon hàng này
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE FTR_Order_Adjusted SET RecordStatus = 99  WHERE OrderKey = @OrderKey 
                         SELECT 30 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08 " + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
