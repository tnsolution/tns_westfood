﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class HoTroSanXuat
    {
        #region [CORE] Data dùng tính lương từng đơn hàng sản xuất
        public static DataTable DanhSachToNhom()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSachToNhom(int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT *   FROM SYS_Team  WHERE RecordStatus <> 99 AND TeamKey = "+ TeamKey +" AND BranchKey=4 AND DepartmentKey != 26 ORDER BY Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// lay hết các công việc trong khoản thời gian
        /// </summary>
        /// <param name="TeamKey"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static DataTable CongViecNhom(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT
A.Category_Stage AS StageKey, 
dbo.LayMaCongViec(A.Category_Stage) AS StageID,
dbo.LayTenCongViec(A.Category_Stage) AS StageName,
dbo.LayGiaCongViec(A.Category_Stage) AS StagePrice
FROM FTR_Order A
WHERE 
A.RecordStatus <> 99 AND 
A.TeamKey = @TeamKey AND 
A.WorkStatus = 1 AND A.OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY Category_Stage
ORDER BY StageName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// Danh sách đơn hàng làm cùng 1 công việc
        /// </summary>
        /// <param name="TeamKey"></param>
        /// <param name="WorkKey"></param>
        /// <returns></returns>
        public static DataTable DonHangThucHien(int TeamKey, int WorkKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.OrderKey, A.OrderID, OrderID_Compare,
A.OrderDate, A.WorkStatus,
ROUND(A.QuantityReality,2) AS Quantity,
dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
dbo.FTR_Order_Employee_Kilogram(A.OrderKey) AS TotalKg,
dbo.FTR_Order_Employee_Basket(A.OrderKey) AS TotalBasket,
dbo.FTR_Order_Employee_Time(A.OrderKey) AS TotalTime,
dbo.FTR_Order_SumRate(A.OrderKey) AS RateOrder,
dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
dbo.LayGiaCongViec(A.Category_Stage) AS Price    --nên bỏ function nay xem chay nhanh hon không
FROM FTR_Order A
WHERE 
A.RecordStatus <> 99 
AND A.TeamKey = @TeamKey 
AND A.Category_Stage = @StageKey
AND A.OrderDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = WorkKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable DonHangThucHien(int TeamKey, int WorkKey, string OrderID, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.OrderKey, A.OrderID, OrderID_Compare,
A.OrderDate, A.WorkStatus,
ROUND(A.QuantityReality,1) AS Quantity,
dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
dbo.FTR_Order_Employee_Kilogram(A.OrderKey) AS TotalKg,
dbo.FTR_Order_Employee_Basket(A.OrderKey) AS TotalBasket,
dbo.FTR_Order_Employee_Time(A.OrderKey) AS TotalTime,
dbo.FTR_Order_SumRate(A.OrderKey) AS RateOrder,
dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
dbo.LayGiaCongViec(A.Category_Stage) AS Price    --nên bỏ function nay xem chay nhanh hon không
FROM FTR_Order A
WHERE 
A.RecordStatus <> 99 
AND A.TeamKey = @TeamKey 
AND A.Category_Stage = @StageKey
AND A.OrderID = @OrderID
AND A.OrderDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = WorkKey;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable DonHangThucHien(string OrderID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.OrderKey, A.OrderID, A.OrderID_Compare,A.TeamKey,A.Category_Stage,
A.OrderDate, A.WorkStatus,
ROUND(A.QuantityReality,2) AS Quantity,
dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
dbo.FTR_Order_Employee_Kilogram(A.OrderKey) AS TotalKg,
dbo.FTR_Order_Employee_Basket(A.OrderKey) AS TotalBasket,
dbo.FTR_Order_Employee_Time(A.OrderKey) AS TotalTime,
dbo.FTR_Order_SumRate(A.OrderKey) AS RateOrder,
dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
dbo.LayGiaCongViec(A.Category_Stage) AS Price    --nên bỏ function nay xem chay nhanh hon không
FROM FTR_Order A
WHERE 
A.RecordStatus <> 99 
AND A.OrderID = @OrderID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);               
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;              
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable CacNhanVienThucHien(string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.EmployeeKey,
dbo.Fn_GetEmployeeID(A.EmployeeKey) EmployeeID,
dbo.Fn_GetFullName(A.EmployeeKey) EmployeeName,
dbo.NamHoacNu(A.EmployeeKey) Gender,
dbo.FTR_SoLuongThanhPham(A.EmployeeKey,A.OrderKey) AS KgProduct,
ROUND(A.Basket, 2) AS Basket,
ROUND(A.Kilogram, 2) AS Kilogram, 
ROUND(A.Time, 2) AS Time,
[dbo].[FTR_SoTienThanhPham](A.EmployeeKey,A.OrderKey) AS Money,
A.Borrow,
A.Private,
A.Share
FROM FTR_Order_Employee A
WHERE A.RecordStatus <> 99 AND
A.OrderKey = @OrderKey 
ORDER BY LEN(EmployeeID), EmployeeID
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #region [Close ]

        //--------------------------------  Chia lại --------------------------------//
        /// <summary>
        /// Các đơn hàng có thông tin cần phải dùng chia lại cho công nhân
        /// </summary>
        /// <param name="TeamKey"></param>
        /// <param name="FromDate"></param>
        /// <returns></returns>
        //        public static DataTable DonHangThucHienChiaLai(int TeamKey, DateTime FromDate, DateTime ToDate)
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"
        //SELECT
        //B.OrderKey, B.OrderID, B.OrderDate, B.Category_Stage AS StageKey,
        //dbo.LayGiaCongViec(B.Category_Stage) AS Price,
        //dbo.LayTenCongViec(B.Category_Stage) AS StageName,
        //dbo.LayTinhTrangChiLai(B.OrderKey) AS [Status]
        //FROM  FTR_Order_Money A
        //LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
        //WHERE A.RecordStatus <> 99 
        //AND A.TeamKey = @TeamKey
        //AND A.Money_Eat > 0
        //AND B.RecordStatus <> 99
        //AND B.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY B.OrderKey, B.OrderID,  B.OrderDate, B.Category_Stage
        //ORDER BY B.OrderDate, B.OrderID";

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        /// <summary>
        /// lấy số tiền của các công nhân trong đơn hàng đã thực hiện chia lại xong
        /// </summary>
        /// <param name="OrderKey"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static DataTable CongNhanChiLaiXong(string OrderKey, DateTime FromDate, DateTime ToDate)
        {
            string zSQL = "";

            DataTable zTable = new DataTable();
            zSQL = @"
SELECT EmployeeKey, EmployeeID, EmployeeName,
TimeEat AS Time, KgEat, MoneyEat, Kg_Eat, Money_Eat
FROM FTR_Order_Money
WHERE OrderKey = @OrderKey 
AND KgEat IS NOT NULL
ORDER BY LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        /// <summary>
        /// Lay các công nhân có làm việc trong đơn hàng cần phải chia lại
        /// </summary>
        /// <param name="OrderKey"></param>
        /// <param name="TeamKey"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static DataTable CongNhanChuaChiLai(string OrderKey, int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            string zSQL = "";
            DataTable zTable = new DataTable();

            zSQL = @"
SELECT dbo.Fn_GetFullName(A.EmployeeKey) AS EmployeeName,
A.EmployeeKey, A.EmployeeID ,
dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS Time,
dbo.LaySoLuongAnChung(A.EmployeeKey, @OrderKey) AS KgEat,
dbo.LaySoTienAnChung(A.EmployeeKey, @OrderKey) AS MoneyEat
FROM HRM_Employee A
WHERE A.RecordStatus < 99
AND A.TeamKey = @TeamKey 
ORDER BY LEN(A.EmployeeID), A.EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #endregion

        #endregion

        #region [CORE] Data dùng tính lương công nhân]
        //Chưa thấy sử dụng
        public static DataTable TinhTienDonHangCongNhan(int zTeamKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.EmployeeKey, 
A.EmployeeName, 
A.OrderDate,
dbo.Fn_GetEmployeeID(A.EmployeeKey) AS EmployeeID,
dbo.Lay15Ngay(A.EmployeeKey) AS Slug,
dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
--dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeam,
dbo.SumTimeTeamPrivate(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeamPrivate,
dbo.SumTimeTeamBorrowPrivate(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeamBorrowPrivate,
dbo.SumTimeTeamBorrowShare(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeamBorrowShare,
dbo.SumTimeTeamDifBorrowShare(@FromDate, @ToDate, A.EmployeeKey) AS TimeTeamDifBorrowShare,
dbo.TongSoTienDuocChiaLai(A.EmployeeKey, @FromDate, @ToDate) AS MoneyBorrowShare,                   -- MoneyBorrowShare là số tiền chia lai
ROUND(SUM(A.Money), 2) AS MoneyPersonal,
ROUND(SUM(A.MoneyPrivate), 2) AS MoneyPrivate,
ROUND(SUM(A.Money_Borrow), 2) AS MoneyBorrowPrivate,
ROUND(SUM(A.Money_Dif_Private), 2) AS MoneyTimePrivate,
ROUND(SUM(A.Money_Dif_General), 2) AS MoneyTimeShare
FROM FTR_Order_Money A 	
WHERE 
A.RecordStatus != 99 AND
dbo.Fn_LayTeamKey_TheoThang(A.EmployeeKey,@ToDate) = @TeamKey AND 
OrderDate BETWEEN @FromDate AND @ToDate	
GROUP BY EmployeeKey, EmployeeName, EmployeeID, OrderDate
ORDER BY LEN(EmployeeID), EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = zTeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #region[Close-14/08/2020]
//        public static DataTable TinhTienDonHangCongNhan(int zTeamKey, DateTime FromDate, DateTime ToDate)
//        {
//            DataTable zTable = new DataTable();
//            string zSQL = @"
//SELECT 
//A.EmployeeKey, 
//A.EmployeeName, 
//A.OrderDate,
//dbo.Fn_GetEmployeeID(A.EmployeeKey) AS EmployeeID,
//dbo.Lay15Ngay(A.EmployeeKey) AS Slug,
//dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
//--dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
//dbo.SumTimeTeam(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeam,
//dbo.SumTimeTeamPrivate(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeamPrivate,
//dbo.SumTimeTeamBorrowPrivate(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeamBorrowPrivate,
//dbo.SumTimeTeamBorrowShare(@FromDate, @ToDate, @TeamKey, A.EmployeeKey) AS TimeTeamBorrowShare,
//dbo.SumTimeTeamDifBorrowShare(@FromDate, @ToDate, A.EmployeeKey) AS TimeTeamDifBorrowShare,
//dbo.TongSoTienDuocChiaLai(A.EmployeeKey, @FromDate, @ToDate) AS MoneyBorrowShare,                   -- MoneyBorrowShare là số tiền chia lai
//ROUND(SUM(A.Money), 2) AS MoneyPersonal,
//ROUND(SUM(A.MoneyPrivate), 2) AS MoneyPrivate,
//ROUND(SUM(A.Money_Borrow), 2) AS MoneyBorrowPrivate,
//ROUND(SUM(A.Money_Dif_Private), 2) AS MoneyTimePrivate,
//ROUND(SUM(A.Money_Dif_General), 2) AS MoneyTimeShare
//FROM FTR_Order_Money A 	
//WHERE 
//A.RecordStatus != 99 AND
//dbo.LayTeamKey(A.EmployeeKey) = @TeamKey AND 
//OrderDate BETWEEN @FromDate AND @ToDate	
//GROUP BY EmployeeKey, EmployeeName, EmployeeID, OrderDate
//ORDER BY LEN(EmployeeID), EmployeeID";

//            string zConnectionString = ConnectDataBase.ConnectionString;
//            try
//            {
//                SqlConnection zConnect = new SqlConnection(zConnectionString);
//                zConnect.Open();
//                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
//                zCommand.CommandType = CommandType.Text;
//                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = zTeamKey;
//                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
//                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
//                zCommand.CommandTimeout = 350;
//                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
//                zAdapter.Fill(zTable);
//                zCommand.Dispose();
//                zConnect.Close();
//            }
//            catch (Exception ex)
//            {
//                string zstrMessage = ex.ToString();
//            }
//            return zTable;
//        }
        #endregion
        #endregion


        #region [CORE REPORT]
        /// <summary>
        /// TỔNG HỢP LƯƠNG PHÂN KHÚC NĂNG SUẤT SẢN XUẤT
        /// </summary>
        /// <param name="zTeamKey"></param>
        /// <param name="FromDate"></param>
        /// <returns></returns>
        //Chưa thấy sd
        public static DataTable TongHopLuongNangSuat(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();

            string zSQL = @"
SELECT 
A.TeamKey,
A.EmployeeKey, 
A.EmployeeName, 
A.EmployeeID, 
A.TeamName,
dbo.TongThanhTienNgoaiGio(A.EmployeeKey, @FromDate, @ToDate) AS NgoaiGio,           --TIỀN LÀM THÊM NGOÀI GIỜ GIÃN CA
dbo.FTR_Sum_Money(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSP ,             --TIỀN KHOÁN SẢN PHẨM
dbo.FTR_Sum_Money_Sunday(A.EmployeeKey,@FromDate,@ToDate) AS LamCN ,    --TIỀN LÀM THÊM NGÀY CHỦ NHẬT
dbo.FTR_Sum_Money_Holidays(A.EmployeeKey,@FromDate,@ToDate) AS LamLe ,      --TIỀN LÀM THÊM NGÀY LỄ
dbo.FTR_Sum_Money_Dif(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSPKhac ,    --TIỀN KHOÁN SẢN PHẨM
dbo.FTR_Sum_Money_Dif_Sunday(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSPCN , --TIỀN KHOÁN LÀM THÊM CHỦ NHẬT
dbo.FTR_Sum_Money_Dif_Holiday(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSPLe,  --TIỀN KHOÁN LÀM THÊM LỄ
B.Rank                                                                          --Sắp xếp theo tổ nhóm
FROM [dbo].[HRM_Team_Employee_Month] A 
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
WHERE A.RecordStatus <> 99 
AND (MONTH(A.DateWrite) = MONTH(@FromDate)) AND (YEAR(A.DateWrite) = YEAR(@FromDate))";
            if (TeamKey != 0)
                zSQL += " AND A.TeamKey = @TeamKey";
            else
            {
                zSQL += "AND A.BranchKey=4 AND  A.DepartmentKey != 26"; //chỉ lấy nhóm sản xuất
            }
            zSQL += @" 
GROUP BY A.TeamKey, A.EmployeeKey,A.EmployeeName, A.TeamName, A.EmployeeID ,B.Rank
ORDER BY B.Rank, LEN(A.EmployeeID), A.EmployeeID";
            
            //	[dbo].[FTR_Sum_MoneyTimeKeep](A.EmployeeKey,@FromDate,@ToDate) AS MoneyTimeKeep 

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TongHopLuongNangSuat_V2(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();

            string zSQL = @"
CREATE TABLE #temp(
DateWork datetime,
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
OverTime money,
MoneyBorrowShare money,		--chia lại
MoneyPersonal  money,		--ăn chung cá nhân
MoneyPrivate money,			--ăn riêng cá nhân
MoneyBorrowPrivate  money,	-- mượn ăn  riêng
MoneyTimePrivate money,		--công việc khác ăn riêng
MoneyTimeShare money		--công việc khác ăn chung

)
--Lấy thông tin Chia lại
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,MoneyBorrowShare,MoneyPersonal,MoneyPrivate,MoneyBorrowPrivate,MoneyTimePrivate,MoneyTimeShare)
SELECT OrderDate,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
0,
ISNULL(Sum(MoneyPersonal) ,0),
0,
0,
0,
0,
0
FROM [dbo].[FTR_Order_Adjusted]
WHERE  OrderDate BETWEEN  @FromDate AND  @ToDate AND MoneyPersonal > 0
AND Share=0 AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')
GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName
--Lấy thông tin tại tổ
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,MoneyBorrowShare,MoneyPersonal,MoneyPrivate,MoneyBorrowPrivate,MoneyTimePrivate,MoneyTimeShare)
SELECT OrderDate,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
0,
0,
ISNULL(SUM([Money]),0) ,
ISNULL(SUM(MoneyPrivate),0) ,
ISNULL(SUM(Money_Borrow),0) ,
ISNULL(SUM(Money_Dif_Private),0) ,
ISNULL(SUM(Money_Dif_General),0) 
FROM [dbo].[FTR_Order_Money]  
WHERE  OrderDate BETWEEN @FROMDATE AND @TODATE 
AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')
GROUP BY OrderDate,TeamKey,EmployeeKey,EmployeeID,EmployeeName
--Lấy thông tin giờ dư
INSERT INTO #temp (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,MoneyBorrowShare,MoneyPersonal,MoneyPrivate,MoneyBorrowPrivate,MoneyTimePrivate,MoneyTimeShare)
SELECT DateImport,[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate),EmployeeKey,EmployeeID,EmployeeName,
ISNULL([Money],0) ,
0,
0,
0,
0,
0,
0
FROM [dbo].[Temp_Import_Detail]
WHERE  DateImport BETWEEN @FROMDATE AND @TODATE AND [Money] > 0
AND RecordStatus <>99 @CustomParamater --AND EmployeeID in ('108')
---Tổng hợp thông tin từng ngày
CREATE TABLE #temp2(
DateWork datetime,
TeamKey int,
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(250),
NgoaiGio money,	--Tiền ngoài giờ
KhoanSP money,	--Tại tổ ngày thường
LamCN money,	--Tại tỏ chủ nhật
LamLe money,	--Tại tổ lễ
KhoanSPKhac money,	--Tổ khác  ngày thường
KhoanSPCN money, -- Tổ khác chủ nhật
KhoanSPLe money	--Tổ khác lễ
)

INSERT INTO #temp2 (DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
NgoaiGio,KhoanSP,LamCN,LamLe,KhoanSPKhac,KhoanSPCN,KhoanSPLe )
SELECT DateWork,TeamKey,EmployeeKey,EmployeeID,EmployeeName,
OverTime,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) !='SUNDAY' THEN ISNULL(MoneyPersonal + MoneyPrivate,0)
END AS KhoanSP,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) ='SUNDAY' THEN ISNULL( MoneyPersonal + MoneyPrivate ,0)
END AS LamCN,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) >0 THEN ISNULL( MoneyPersonal+MoneyPrivate,0)
END AS LamLe,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) !='SUNDAY' THEN ISNULL(MoneyBorrowPrivate + MoneyBorrowShare + MoneyTimePrivate + MoneyTimeShare ,0)
END AS KhoanSPKhac,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) =0 AND DATENAME(weekday,DateWork) ='SUNDAY' THEN ISNULL(MoneyBorrowPrivate + MoneyBorrowShare + MoneyTimePrivate + MoneyTimeShare ,0)
END AS KhoanSPCN,
CASE
	WHEN [dbo].[LayHeSoNgayLeDonHang](DateWork) >0 THEN ISNULL( MoneyBorrowPrivate + MoneyBorrowShare + MoneyTimePrivate + MoneyTimeShare ,0)
END AS KhoanSPLe

FROM #temp 

;WITH BangTam AS (
SELECT TeamKey,EmployeeKey,EmployeeID,EmployeeName,dbo.Fn_GetTeamName(TeamKey) AS TeamName,
ISNULL(SUM(NgoaiGio),0) AS NgoaiGio,
ISNULL(SUM(KhoanSP),0) AS KhoanSP,
ISNULL(SUM(LamCN),0) AS LamCN,
ISNULL(SUM(LamLe),0) AS LamLe,
ISNULL(SUM(KhoanSPKhac),0)AS KhoanSPKhac,
ISNULL(SUM(KhoanSPCN),0) AS KhoanSPCN,
ISNULL(SUM(KhoanSPLe),0) AS KhoanSPLe
FROM #temp2 
GROUP BY TeamKey,EmployeeKey,EmployeeID,EmployeeName,dbo.Fn_GetTeamName(TeamKey) 
)
SELECT A.*,D.RANK BranchRank,C.RANK AS DepartmentRank, B.RANK AS TeamRank  FROM BangTam A
LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK,C.RANK,B.RANK,LEN(EmployeeID), EmployeeID
DROP TABLE #temp
DROP TABLE #temp2";
            if (TeamKey > 0)
            {
                zSQL = zSQL.Replace("@CustomParamater", "AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@TeamKey");
            }
            else
            {
                zSQL = zSQL.Replace("@CustomParamater", "");
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        #region[Close-14/08/2020-TongHopLuongNangSuat- Thay thế HRM_Employee-->HRM_TeamEmployee]
        //        public static DataTable TongHopLuongNangSuat(int TeamKey, DateTime FromDate, DateTime ToDate)
        //        {
        //            DataTable zTable = new DataTable();

        //            string zSQL = @"
        //SELECT 
        //A.TeamKey,
        //A.EmployeeKey, 
        //dbo.[Fn_GetFullName](A.EmployeeKey) AS EmployeeName, 
        //A.EmployeeID, 
        //dbo.Fn_GetTeamName(A.TeamKey) AS TeamName,
        //dbo.TongThanhTienNgoaiGio(A.EmployeeKey, @FromDate, @ToDate) AS NgoaiGio,           --TIỀN LÀM THÊM NGOÀI GIỜ GIÃN CA
        //dbo.FTR_Sum_Money(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSP ,             --TIỀN KHOÁN SẢN PHẨM
        //dbo.FTR_Sum_Money_Sunday(A.EmployeeKey,@FromDate,@ToDate) AS LamCN ,    --TIỀN LÀM THÊM NGÀY CHỦ NHẬT
        //dbo.FTR_Sum_Money_Holidays(A.EmployeeKey,@FromDate,@ToDate) AS LamLe ,      --TIỀN LÀM THÊM NGÀY LỄ
        //dbo.FTR_Sum_Money_Dif(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSPKhac ,    --TIỀN KHOÁN SẢN PHẨM
        //dbo.FTR_Sum_Money_Dif_Sunday(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSPCN , --TIỀN KHOÁN LÀM THÊM CHỦ NHẬT
        //dbo.FTR_Sum_Money_Dif_Holiday(A.EmployeeKey,@FromDate,@ToDate) AS KhoanSPLe,  --TIỀN KHOÁN LÀM THÊM LỄ
        //B.Rank                                                                          --Sắp xếp theo tổ nhóm
        //FROM [dbo].[HRM_Employee] A 
        //LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
        //WHERE A.RecordStatus <> 99
        //AND (A.LeavingDate IS NULL OR A.LeavingDate >= @FromDate)";
        //            if (TeamKey != 0)
        //                zSQL += " AND A.TeamKey = @TeamKey";
        //            else
        //            {
        //                zSQL += "AND A.BranchKey=4 AND  A.DepartmentKey != 26"; //chỉ lấy nhóm sản xuất
        //            }
        //            zSQL += @" 
        //GROUP BY A.TeamKey, A.EmployeeKey, dbo.[Fn_GetFullName](A.EmployeeKey), A.EmployeeID ,B.Rank
        //ORDER BY B.Rank, LEN(A.EmployeeID), A.EmployeeID";

        //            //	[dbo].[FTR_Sum_MoneyTimeKeep](A.EmployeeKey,@FromDate,@ToDate) AS MoneyTimeKeep 

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }
        #endregion



        public static DataTable PhanBoSanPham(DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT ProductKey, ProductID, ProductName,
SUM(QuantityDocument) AS SLChungTu,
SUM(QuantityReality) AS SLThuTe,
SUM(QuantityLoss) AS SLHaohut,
dbo.TinhTongTienDonHang(ProductKey, @FromDate, @ToDate) AS TongCong
FROM FTR_Order
WHERE OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY ProductID, ProductName, ProductKey
ORDER BY LEN(ProductID), ProductID
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable TheoDoi(string CreatedBy)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM AUT_Run WHERE CreatedBy = @CreatedBy";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        //Không thấy sử dụng
        public static DataTable DonHangThucHienChiaLai(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT
B.OrderKey, B.OrderID, B.OrderDate, B.Category_Stage AS StageKey,
dbo.LayGiaCongViec(B.Category_Stage) AS Price,
dbo.LayTenCongViec(B.Category_Stage) AS StageName,
dbo.LayTinhTrangChiLai(B.OrderKey) AS [Status]
FROM  FTR_Order_Money A
LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
LEFT JOIN dbo.HRM_Team_Employee_Month E ON E.EmployeeKey=A.EmployeeKey
LEFT JOIN dbo.SYS_Team F ON F.TeamKey=E.TeamKey
WHERE A.RecordStatus <> 99 
AND F.TeamKey = @TeamKey
AND A.Money_Eat > 0
AND (MONTH(E.DateWrite) = MONTH(@FromDate)) AND (YEAR(E.DateWrite) = YEAR(@FromDate))
AND B.RecordStatus <> 99
AND B.OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY B.OrderKey, B.OrderID,  B.OrderDate, B.Category_Stage
ORDER BY B.OrderDate, B.OrderID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #region [lose 14/08/2020-DonHangThucHienChiaLai-Thay the bảng Employee-->TeamEmployee]
        //        public static DataTable DonHangThucHienChiaLai(int TeamKey, DateTime FromDate, DateTime ToDate)
        //        {
        //            DataTable zTable = new DataTable();
        //            string zSQL = @"
        //SELECT
        //B.OrderKey, B.OrderID, B.OrderDate, B.Category_Stage AS StageKey,
        //dbo.LayGiaCongViec(B.Category_Stage) AS Price,
        //dbo.LayTenCongViec(B.Category_Stage) AS StageName,
        //dbo.LayTinhTrangChiLai(B.OrderKey) AS [Status]
        //FROM  FTR_Order_Money A
        //LEFT JOIN FTR_Order B ON A.OrderKey = B.OrderKey
        //LEFT JOIN dbo.HRM_Employee E ON E.EmployeeKey=A.EmployeeKey
        //LEFT JOIN dbo.SYS_Team F ON F.TeamKey=E.TeamKey
        //WHERE A.RecordStatus <> 99 
        //AND F.TeamKey = @TeamKey
        //AND A.Money_Eat > 0
        //AND B.RecordStatus <> 99
        //AND B.OrderDate BETWEEN @FromDate AND @ToDate
        //GROUP BY B.OrderKey, B.OrderID,  B.OrderDate, B.Category_Stage
        //ORDER BY B.OrderDate, B.OrderID";

        //            string zConnectionString = ConnectDataBase.ConnectionString;
        //            try
        //            {
        //                SqlConnection zConnect = new SqlConnection(zConnectionString);
        //                zConnect.Open();
        //                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
        //                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
        //                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
        //                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //                zAdapter.Fill(zTable);
        //                zCommand.Dispose();
        //                zConnect.Close();
        //            }
        //            catch (Exception ex)
        //            {
        //                string zstrMessage = ex.ToString();
        //            }
        //            return zTable;
        //        }

        #endregion

        public static float LayTienNgoaiGio(DateTime DateTime)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.LayTienNgoaiGio(@DateTime)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = DateTime;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static float LayDonGiaNam(DateTime DateTime)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.LayDonGiaNam(@DateTime)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = DateTime;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static float LayDonGiaNu(DateTime DateTime)
        {
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.LayDonGiaNu(@DateTime)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = DateTime;
                if (float.TryParse(zCommand.ExecuteScalar().ToString(), out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static int LaySoGioLamHanhChanh(DateTime DateTime)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.LaySoGioLamHanhChanh(@DateTime)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = DateTime;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static int LayHeSoChuNhat(DateTime DateTime)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT dbo.LayHeSoChuNhat(@DateTime)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = DateTime;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static float LayHeSoNgayLe(DateTime DateTime)
        {
            DateTime zFromDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 23, 59, 59);
            float zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = @"
            DECLARE @Return FLOAT = 0
            SELECT @Return = ISNULL(Factor,0) FROM [dbo].[HRM_Time_Holiday] 
            WHERE Holiday BETWEEN @FromDate and @ToDate AND RecordStatus<>99 
            SELECT ISNULL(@Return,0)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                if(float.TryParse(zCommand.ExecuteScalar().ToString(),out zResult))
                {

                }
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {

            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        //OrderView
        public static DataTable CacNhanVienThucHien_OrderView(string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
                            SELECT  A.EmployeeKey,
                            dbo.Fn_GetEmployeeID(A.EmployeeKey) EmployeeID,
                            dbo.Fn_GetFullName(A.EmployeeKey) EmployeeName,
                            [dbo].[Fn_GetTeamID]([dbo].[Fn_LayTeamKey_TheoThang](A.EmployeeKey,B.OrderDate)) AS TeamID,
                            dbo.[LayThoiGianTNS_CaNhan](A.EmployeeKey,B.OrderKey) AS TGTNS,
                            ROUND(A.Kilogram, 2) AS Kilogram, 
                            ROUND(A.Basket, 2) AS Basket,
                            ROUND(A.Time, 2) AS Time,
                            dbo.FTR_SoLuongThanhPham(A.EmployeeKey,A.OrderKey) AS KgProduct,
                            [dbo].[FTR_SoTienThanhPham](A.EmployeeKey,B.OrderKey) AS Money,
                            A.Borrow,
                            A.Private,
                            A.Share
                            FROM FTR_Order_Employee A
                            LEFT JOIN [dbo].[FTR_Order] B ON B.OrderKey =A.OrderKey
                            WHERE A.RecordStatus <> 99 AND B.RecordStatus <> 99  
                            AND A.OrderKey = @OrderKey
                            ORDER BY A.Borrow,TeamID,LEN(EmployeeID), EmployeeID
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //kiểm soát đơn hàng V3
        /// <summary>
        /// Danh sách đơn hàng làm cùng 1 công việc
        /// </summary>
        /// <param name="TeamKey"></param>
        /// <param name="WorkKey"></param>
        /// <returns></returns>
        public static DataTable DonHangThucHienV3(int TeamKey, int WorkKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.OrderKey, A.OrderID, OrderID_Compare,
A.OrderDate, A.WorkStatus,
ROUND(A.QuantityReality,2) AS Quantity,
dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
dbo.FTR_Order_Employee_Kilogram(A.OrderKey) AS TotalKg,
dbo.FTR_Order_Employee_Basket(A.OrderKey) AS TotalBasket,
dbo.FTR_Order_Employee_Time(A.OrderKey) AS TotalTime,
dbo.FTR_Order_SumRate(A.OrderKey) AS RateOrder,
dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
dbo.LayGiaCongViec(A.Category_Stage) AS Price ,   --nên bỏ function nay xem chay nhanh hon không
[dbo].[Fn_TongGioNamTheoDonHang](A.OrderKey) AS GioNam, 
[dbo].[Fn_TongGioNuTheoDonHang](A.OrderKey) AS GioNu
FROM FTR_Order A
WHERE 
A.RecordStatus <> 99 
AND A.TeamKey = @TeamKey 
AND A.Category_Stage = @StageKey
AND A.OrderDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = WorkKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DonHangThucHienV2(string OrderID)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.OrderKey, A.OrderID, A.OrderID_Compare,A.TeamKey,A.Category_Stage,
A.OrderDate, A.WorkStatus,
ROUND(A.QuantityReality,2) AS Quantity,
dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
dbo.FTR_Order_Employee_Kilogram(A.OrderKey) AS TotalKg,
dbo.FTR_Order_Employee_Basket(A.OrderKey) AS TotalBasket,
dbo.FTR_Order_Employee_Time(A.OrderKey) AS TotalTime,
dbo.FTR_Order_SumRate(A.OrderKey) AS RateOrder,
dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
dbo.LayGiaCongViec(A.Category_Stage) AS Price,    --nên bỏ function nay xem chay nhanh hon không
[dbo].[Fn_TongGioNamTheoDonHang](A.OrderKey) AS GioNam, 
[dbo].[Fn_TongGioNuTheoDonHang](A.OrderKey) AS GioNu 
FROM FTR_Order A
WHERE 
A.RecordStatus <> 99 
AND A.OrderID = @OrderID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        /// <summary>
        /// Danh sách đơn hàng làm cùng 1 công việc
        /// </summary>
        /// <param name="TeamKey"></param>
        /// <param name="WorkKey"></param>
        /// <returns></returns>
        public static DataTable DonHangThucHienV2(int TeamKey, int WorkKey, DateTime FromDate, DateTime ToDate)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT 
A.OrderKey, A.OrderID, OrderID_Compare,
A.OrderDate, A.WorkStatus,
ROUND(A.QuantityReality,2) AS Quantity,
dbo.LayTyLeHoanThanhDonHang(A.OrderKey) AS PercentOrder,
dbo.FTR_Order_Employee_Kilogram(A.OrderKey) AS TotalKg,
dbo.FTR_Order_Employee_Basket(A.OrderKey) AS TotalBasket,
dbo.FTR_Order_Employee_Time(A.OrderKey) AS TotalTime,
dbo.FTR_Order_SumRate(A.OrderKey) AS RateOrder,
dbo.LayHeSoNgayLeDonHang(A.OrderDate) AS RateDay,
dbo.LayGiaCongViec(A.Category_Stage) AS Price,    --nên bỏ function nay xem chay nhanh hon không
[dbo].[Fn_TongGioNamTheoDonHang](A.OrderKey) AS GioNam, 
[dbo].[Fn_TongGioNuTheoDonHang](A.OrderKey) AS GioNu
FROM FTR_Order A
WHERE 
A.RecordStatus <> 99 
AND A.TeamKey = @TeamKey 
AND A.Category_Stage = @StageKey
AND A.OrderDate BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = WorkKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //Khoán chuyên cần
        public static DataTable KhoanChuyenCan(int TeamKey,string EmployeeKey, DateTime FromDate, DateTime ToDate, int GopTienGioDu)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zFillter = "";
            if(TeamKey >0)
            {
                zFillter += " AND TeamKey = @TeamKey ";

            }
            string zSQL = @"
SELECT EmployeeName,EmployeeID,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,
[dbo].[SLR_WORKER_KTCC_LaySotienKhoanDungDeTinhChuyenCan](EmployeeKey,@FromDate,@ToDate,@GopGioDu)
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
                ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	        OR  ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
          )
          AND BranchKey =4 
          AND DepartmentKey !=26  
            @Customer
     ORDER BY [dbo].[Fn_GetTeamID](TeamKey), LEN(EmployeeID),EmployeeID
";
            zSQL = zSQL.Replace("@Customer" ,zFillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@GopGioDu", SqlDbType.Int).Value = GopTienGioDu;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

    }
}
