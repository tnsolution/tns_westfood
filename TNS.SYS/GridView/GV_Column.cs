﻿using System.Data;
using System.Linq;
using System.Windows.Forms;
namespace TN.Toolbox
{
    public class GV_Column
    {
        #region [ Variables ]       
        private bool _IsPrimaryID = false;
        private bool _IsSum = false;
        /// <summary>
        /// _TableSource Table show to listview
        /// </summary>
        private DataTable _TableSource;
        /// <summary>
        /// _FilterColumn Set column need to search
        /// </summary>
        private bool _FilterColumn = false;

        /// <summary>
        /// _SelectColumn Set text when selected
        /// </summary>
        private int _ShowColumn = 0;

        /// <summary>
        /// _DisplayColumn Set Text when select value from listview
        /// </summary>
        private string _DisplayColumn = "";

        /// <summary>
        /// _CheckEmpty Check cell is require or not
        /// </summary>
        private bool _CheckEmpty = false;

        /// <summary>
        /// _ColumnName to show in header LV
        /// </summary>
        private string _ColumnName;
        private string _HeaderText;
        private string _Format = "";
        private string _Style;
        private bool _IsCenter;
        private int _Width = 100;
        private ListView _ComboBoxLV;     
        private string _ComboBoxName;      
        
        private string _Message;
        #endregion

        public GV_Column()
        {
            _ColumnName = "";
            _HeaderText = "";
            _Format = "Text";
        }
        public GV_Column(string ColumnName, string HeaderText)
        {
            _ColumnName = ColumnName;
            _HeaderText = HeaderText;
            _Style = "Text";
        }
        /// <summary>
        /// Normal Text Column align left
        /// </summary>
        /// <param name="ColumnName">Name of column</param>
        /// <param name="HeaderText"></param>
        /// <param name="Width"></param>
        public GV_Column(string ColumnName, string HeaderText, int Width)
        {
            _ColumnName = ColumnName;
            _HeaderText = HeaderText;
            _Width = Width;
            _Style = "Text";
        }
        /// <summary>
        /// Normal Text Column align Center
        /// </summary>
        /// <param name="ColumnName">Name of column</param>
        /// <param name="HeaderText"></param>
        /// <param name="Width"></param>
        /// <param name="IsCenter"></param>
        public GV_Column(string ColumnName, string HeaderText, int Width, bool IsCenter)
        {
            _ColumnName = ColumnName;
            _HeaderText = HeaderText;
            _Width = Width;
            _Style = "Text";
            _IsCenter = IsCenter;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="StyleColumn">ListView (Add external control)</param>
        /// <param name="Width">Width external control</param>
        /// <param name="Height">Height external control</param>
        public GV_Column(string ColumnName, string StyleColumn, int Width, int Height)
        {
            _ColumnName = ColumnName;
            _HeaderText = "";
            _Style = "Text";
            _Width = Width;
            _ComboBoxName = StyleColumn;

            switch (StyleColumn)
            {
                case "ListView":
                    _ComboBoxLV = new ListView();
                    _ComboBoxLV.Name = _ColumnName;
                    _ComboBoxLV.FullRowSelect = true;
                    _ComboBoxLV.GridLines = true;
                    _ComboBoxLV.Size = new System.Drawing.Size(Width, Height);
                    _ComboBoxLV.View = System.Windows.Forms.View.Details;
                    _ComboBoxLV.Visible = false;
                    break;
                case "PickDate":
                    break;

                case "CheckBox":
                    break;
            }
        }

        #region [ Properties ]
        public string ColumnName
        {
            set { _ColumnName = value; }
            get { return _ColumnName; }
        }
        public string HeaderText
        {
            set { _HeaderText = value; }
            get { return _HeaderText; }
        }
        public string Format
        {
            set { _Format = value; }
            get { return _Format; }
        }
        public string Style
        {
            set { _Style = value; }
            get { return _Style; }
        }
        public int Width
        {
            set { _Width = value; }
            get { return _Width; }
        }
        public string ComboBoxName
        {
            set { _ComboBoxName = value; }
            get { return _ComboBoxName; }
        }
      
        public ListView ComboBoxLV
        {
            set { _ComboBoxLV = value; }
            get { return _ComboBoxLV; }
        }

        public DataTable TableSource
        {
            get
            {
                return _TableSource;
            }

            set
            {
                _TableSource = value;
            }
        }

        public bool FilterColumn
        {
            get
            {
                return _FilterColumn;
            }

            set
            {
                _FilterColumn = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        public int ShowColumn
        {
            get
            {
                return _ShowColumn;
            }

            set
            {
                _ShowColumn = value;
            }
        }

        public bool IsSum
        {
            get
            {
                return _IsSum;
            }

            set
            {
                _IsSum = value;
            }
        }       
        public bool IsCenter
        {
            get
            {
                return _IsCenter;
            }

            set
            {
                _IsCenter = value;
            }
        }       

        public string DisplayNextColumn
        {
            get
            {
                return _DisplayColumn;
            }

            set
            {
                _DisplayColumn = value;
            }
        }

        public bool CheckEmpty
        {
            get
            {
                return _CheckEmpty;
            }

            set
            {
                _CheckEmpty = value;
            }
        }

        public bool IsPrimaryID
        {
            get
            {
                return _IsPrimaryID;
            }

            set
            {
                _IsPrimaryID = value;
            }
        }
        #endregion

        #region [ Function ]
        public void SetBoundListView(int x, int y)
        {
            _ComboBoxLV.Left = x;
            _ComboBoxLV.Top = y;
            _ComboBoxLV.Visible = true;
        }       
        #endregion

        #region [ Process Data To List View ]
        public void InitColumnLV(string[] Columns)
        {
            ColumnHeader colHead;
            for (int i = 0; i < Columns.Length; i++)
            {
                colHead = new ColumnHeader();

                if (_ShowColumn - 1 == i)
                {
                    colHead.Width = _Width;
                    colHead.AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                    colHead.Text = Columns[i];
                    colHead.TextAlign = HorizontalAlignment.Left;
                }
                else
                {
                    colHead.AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                    colHead.Text = Columns[i];
                    colHead.TextAlign = HorizontalAlignment.Left;
                    colHead.Width = 0;
                }

                _ComboBoxLV.Columns.Add(colHead);
            }

            _ComboBoxLV.HeaderStyle = ColumnHeaderStyle.None;
            if (_ComboBoxLV.Columns.Count > 0)
                _ComboBoxLV.Columns[_ComboBoxLV.Columns.Count - 1].Width = -2;
        }
        public void LoadDataToLV()
        {
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            _ComboBoxLV.Items.Clear();
            int n = _TableSource.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow zRow = _TableSource.Rows[i];
                lvi = new ListViewItem();
                lvi.Text = zRow[1].ToString();
                // nRow[""].ToString();
                lvi.Tag = zRow[0];
                // Set the tag to

                for (int j = 2; j <= _ComboBoxLV.Columns.Count; j++)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = zRow[j].ToString().Trim();
                    lvi.SubItems.Add(lvsi);

                    _ComboBoxLV.Items.Add(lvi);
                }
            }


        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Content">Value need search</param>
        public void FilterDataLV(string Content)
        {
            if (_FilterColumn)
            {
                DataRow[] Row = _TableSource.Select(_ColumnName + " LIKE '*" + Content + "*' ");
                if (Row.Count() <= 0)
                {
                    _ComboBoxLV.Items.Clear();

                    ListViewItem lvi;
                    ListViewItem.ListViewSubItem lvsi;

                    lvi = new ListViewItem();
                    lvi.Text = "";
                    lvi.Tag = 0;

                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Không có thông tin phù hợp";
                    lvi.SubItems.Add(lvsi);

                    _ComboBoxLV.Items.Add(lvi);
                }
                else
                {
                    DataTable zTable = Row.CopyToDataTable();

                    ListViewItem lvi;
                    ListViewItem.ListViewSubItem lvsi;
                    _ComboBoxLV.Items.Clear();
                    int n = zTable.Rows.Count;
                    for (int i = 0; i < n; i++)
                    {
                        DataRow zRow = zTable.Rows[i];
                        lvi = new ListViewItem();
                        lvi.Text = zRow[1].ToString();  // nRow[""].ToString();
                        lvi.Tag = zRow[0]; // Set the tag to 

                        for (int j = 2; j <= _ComboBoxLV.Columns.Count; j++)
                        {
                            lvsi = new ListViewItem.ListViewSubItem();
                            lvsi.Text = zRow[j].ToString().Trim();
                            lvi.SubItems.Add(lvsi);

                            _ComboBoxLV.Items.Add(lvi);
                        }
                    }
                }
            }
        }
        #endregion       
    }
}
