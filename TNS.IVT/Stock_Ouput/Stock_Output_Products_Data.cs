﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Stock_Output_Products_Data
    {
        public static DataTable GetProducts(string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT A.*,B.ProductID, B.ProductName,C.OrderID FROM IVT_Stock_Output_Products A 
INNER JOIN IVT_Product B ON A.ProductKey = B.ProductKey 
LEFT JOIN IVT_Stock_Input C ON C.OrderKey = A.OrderKeyFollow 
WHERE A.OrderKey = @OrderKey AND A.RecordStatus <99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey); ;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
