﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.LOG
{
    public class Application_Info
    {

        #region [ Field Name ]
        private string _RowGuid = "";
        private string _UserKey = "";
        private string _EmployeeKey = "";
        private DateTime _DateLog;
        private string _ActionName = "";
        private int _ActionType = 1;
        private string _Title = "";
        private string _SubDescription = "";
        private string _Description = "";
        private int _Form = 0;
        private string _FormName = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_RowGuid.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _RowGuid; }
            set { _RowGuid = value; }
        }
        public string UserKey
        {
            get { return _UserKey; }
            set { _UserKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public DateTime DateLog
        {
            get { return _DateLog; }
            set { _DateLog = value; }
        }
        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }
        public int ActionType
        {
            get { return _ActionType; }
            set { _ActionType = value; }
        }
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }
        public string SubDescription
        {
            get { return _SubDescription; }
            set { _SubDescription = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Form
        {
            get { return _Form; }
            set { _Form = value; }
        }
        public string FormName
        {
            get { return _FormName; }
            set { _FormName = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Application_Info()
        {
        }
        public Application_Info(string RowGuid)
        {
            string zSQL = "SELECT * FROM LOG_Application WHERE RowGuid = @RowGuid";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = Guid.Parse(RowGuid);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _RowGuid = zReader["RowGuid"].ToString();
                    _UserKey = zReader["UserKey"].ToString();
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    if (zReader["DateLog"] != DBNull.Value)
                        _DateLog = (DateTime)zReader["DateLog"];
                    _ActionName = zReader["ActionName"].ToString().Trim();
                    if (zReader["ActionType"] != DBNull.Value)
                        _ActionType = int.Parse(zReader["ActionType"].ToString());
                    _Title = zReader["Title"].ToString().Trim();
                    _SubDescription = zReader["SubDescription"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Form"] != DBNull.Value)
                        _Form = int.Parse(zReader["Form"].ToString());
                    _FormName = zReader["FormName"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO LOG_Application ("
+ " UserKey ,EmployeeKey ,DateLog ,ActionName ,ActionType ,Title ,SubDescription ,Description ,Form ,FormName ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
+ " VALUES ( "
+ "@UserKey ,@EmployeeKey ,@DateLog ,@ActionName ,@ActionType ,@Title ,@SubDescription ,@Description ,@Form ,@FormName ,0 ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_UserKey.Length > 0)
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                else
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                if (_DateLog == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateLog", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateLog", SqlDbType.DateTime).Value = _DateLog;
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = _ActionName.Trim();
                zCommand.Parameters.Add("@ActionType", SqlDbType.Int).Value = _ActionType;
                zCommand.Parameters.Add("@Title", SqlDbType.NText).Value = _Title.Trim();
                zCommand.Parameters.Add("@SubDescription", SqlDbType.NText).Value = _SubDescription.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description.Trim();
                zCommand.Parameters.Add("@Form", SqlDbType.Int).Value = _Form;
                zCommand.Parameters.Add("@FormName", SqlDbType.NVarChar).Value = _FormName.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE LOG_Application SET "
            + " UserKey = @UserKey,"
            + " EmployeeKey = @EmployeeKey,"
            + " DateLog = @DateLog,"
            + " ActionName = @ActionName,"
            + " ActionType = @ActionType,"
            + " Title = @Title,"
            + " SubDescription = @SubDescription,"
            + " Description = @Description,"
            + " Form = @Form,"
            + " FormName = @FormName,"
            + " RecordStatus = 1,"
            + " ModifiedOn = GETDATE(),"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
           + " WHERE RowGuid = @RowGuid";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_RowGuid.Length == 36)
                    zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_RowGuid);
                else
                    zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (_UserKey.Length == 36)
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_UserKey);
                else
                    zCommand.Parameters.Add("@UserKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                if (_DateLog == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateLog", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateLog", SqlDbType.DateTime).Value = _DateLog;
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = _ActionName.Trim();
                zCommand.Parameters.Add("@ActionType", SqlDbType.Int).Value = _ActionType;
                zCommand.Parameters.Add("@Title", SqlDbType.NText).Value = _Title.Trim();
                zCommand.Parameters.Add("@SubDescription", SqlDbType.NText).Value = _SubDescription.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NText).Value = _Description.Trim();
                zCommand.Parameters.Add("@Form", SqlDbType.Int).Value = _Form;
                zCommand.Parameters.Add("@FormName", SqlDbType.NVarChar).Value = _FormName.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOG_Application SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE RowGuid = @RowGuid";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@RowGuid", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_RowGuid);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_RowGuid.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
