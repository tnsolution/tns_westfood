﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.HRM
{
    public class Employee_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Employee";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Load_Data(string SelectColumn)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT "+ SelectColumn + " FROM [dbo].[HRM_Employee] A LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey WHERE A.RecordStatus != 99 Order by B.CreatedOn";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Load_Data()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.EmployeeKey,A.EmployeeID, B.FullName, B.CardID,B.PersonalKey, B.CreatedOn FROM [dbo].[HRM_Employee] A
LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey WHERE A.RecordStatus != 99 Order by B.CreatedOn";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Load_Data(int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.EmployeeKey, A.EmployeeID, B.FullName, B.CardID,B.PersonalKey, B.CreatedOn FROM [dbo].[HRM_Employee] A
LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey WHERE A.RecordStatus != 99";
            if(TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey";
            }
            zSQL +=  " Order by B.CreatedOn";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT TOP 10  A.*, B.PersonalKey,B.FullName,B.CardID  FROM[dbo].[HRM_Employee] A
LEFT JOIN[dbo].[SYS_Personal] B ON B.ParentKey = A.EmployeeKey
WHERE A.RecordStatus < 99 ";
            if (Name.Length > 0)
            {
                zSQL += " AND B.FullName LIKE @Name OR A.EmployeeID LIKE @Name ";
            }
            zSQL += " Order By B.CardID ASC ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

//        public static DataTable ListNames(int TeamKey)
//        {
//            DataTable zTable = new DataTable();
//            string zSQL = @"
//SELECT EmployeeKey,CardID AS EmployeeID,B.FullName 
//FROM [dbo].[HRM_Employee] A
//LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey
//WHERE A.RecordStatus < 99 AND TeamKey = @TeamKey
//ORDER BY LEN(EmployeeID), EmployeeID";
//            string zConnectionString = ConnectDataBase.ConnectionString;
//            try
//            {
//                SqlConnection zConnect = new SqlConnection(zConnectionString);
//                zConnect.Open();
//                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
//                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
//                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
//                zAdapter.Fill(zTable);
//                zCommand.Dispose();
//                zConnect.Close();
//            }
//            catch (Exception ex)
//            {
//                string zstrMessage = "08" + ex.ToString();
//            }
//            return zTable;
//        }
        public static DataTable ListNamesV2(int TeamKey,DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT 
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự
SELECT EmployeeKey,EmployeeID, EmployeeName AS FullName FROM #EmployeeTemp 
ORDER BY  LEN(EmployeeID),EmployeeID

DROP TABLE #EmployeeTemp
";
            string zFilter = "";

            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListNamesV3(int TeamKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime='2020-04-01 00:00:00'
--declare @ToDate datetime='2020-04-30 23:59:59'

--Nhân sự làm việc trong tháng
CREATE TABLE #EmployeeTemp
(
EmployeeKey NVARCHAR(50),
EmployeeID NVARCHAR(50),
EmployeeName NVARCHAR(500),
BranchKey INT,
DepartmentKey INT ,
TeamKey INT ,
PositionKey INT 
)
INSERT INTO #EmployeeTemp
SELECT EmployeeKey,EmployeeID,EmployeeName,
BranchKey,DepartmentKey,TeamKey, PositionKey
 FROM (
		SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,StartingDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS BranchKey,
            [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS DepartmentKey,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKey ,
            [dbo].[Fn_PositionKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS PositionKey
			FROM [dbo].[HRM_Employee] 
			WHERE RecordStatus <>99 
	) X
	WHERE (
            ( StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
	     OR ( StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate )
        )
    @Customer
----------Kết thúc tạo danh sách nhân sự
SELECT EmployeeKey,EmployeeID, EmployeeName AS FullName FROM #EmployeeTemp 
ORDER BY  LEN(EmployeeID),EmployeeID

DROP TABLE #EmployeeTemp
";
            string zFilter = "";

            if (TeamKey != 0)
            {
                zFilter += " AND TeamKey= @TeamKey";
            }
            if (zFilter.Length != 0)
            {
                zSQL = zSQL.Replace("@Customer", zFilter);
            }
            else
            {
                zSQL = zSQL.Replace("@Customer", "");
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Trong class EmployeeData

        public static string Check_Organization(string EmployeeKey, string ReportTo)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "select [dbo].[Get_Organization](@EmployeeKey,@ReportToKey)";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (EmployeeKey.Length == 36)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                if (ReportTo.Length == 36)
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ReportTo);
                else
                    zCommand.Parameters.Add("@ReportToKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                // Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static DataTable List_ReportTo(string ListField,string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B."+ ListField + " FROM[dbo].[HRM_Employee] A"+
" LEFT JOIN[dbo].[SYS_Personal] B ON B.ParentKey = A.EmployeeKey"+
" WHERE A.RecordStatus< 99 AND B.LastName LIKE N'" + ListField + "  AND B.ParentKey !=  '" + EmployeeKey + "' ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Report(string ListFiled, string EmployeeKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT B."+ ListFiled + " FROM[dbo].[HRM_Employee] A" +
" LEFT JOIN[dbo].[SYS_Personal] B ON B.ParentKey = A.EmployeeKey" +
" WHERE A.RecordStatus< 99 AND B.FullName LIKE "+ ListFiled + "  AND B.ParentKey !=  '" + EmployeeKey + "' ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int Count_CardID(string CardID,int TeamKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM HRM_Employee WHERE EmployeeID = @EmployeeID AND TeamKey = @TeamKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = CardID;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
       
        public static int Count(int TeamKey, string EmployeeID, string OrderKey)
        {
            int result = 0;
            string zSQL = @"
SELECT COUNT(*) FROM [dbo].[FTR_Order] A
LEFT JOIN [dbo].[FTR_Order_Employee] B ON A.OrderKey = B.OrderKey
WHERE A.OrderKey = @OrderKey AND TeamKey = @TeamKey AND B.EmployeeID = @EmployeeID And B.RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }

        public static DataTable Load_Data(int TeamKey, string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.EmployeeKey, A.RecordStatus, A.EmployeeID, 
B.IssueID, B.FullName, B.CardID,B.PersonalKey, B.CreatedOn, 
[dbo].[Fn_GetDepartmentID] (A.DepartmentKey) AS DepartmentID, [dbo].[Fn_GetTeamID] (A.TeamKey) AS TeamID
FROM [dbo].[HRM_Employee] A
LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey WHERE A.RecordStatus != 99";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey";
            }
            if (Name.Length > 0)
            {
                zSQL += " AND (B.FullName LIKE @Name OR A.EmployeeID LIKE @Name OR B.IssueID LIKE @Name)";
            }
            zSQL += " ORDER BY LEN(A.EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable Search(int Branch,int Department,int TeamKey, string Name, string EmployeeID, string IssueID,int Position ,int Status)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.EmployeeKey, A.RecordStatus, A.EmployeeID, C.WorkingName,
B.IssueID, B.FullName, B.CardID,B.PersonalKey, B.CreatedOn, 
[dbo].[Fn_GetDepartmentID] (A.DepartmentKey) AS DepartmentID, [dbo].[Fn_GetTeamID] (A.TeamKey) AS TeamID
FROM [dbo].[HRM_Employee] A
LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey 
LEFT JOIN [dbo].[HRM_WorkingStatus] C ON C.WorkingKey =A.WorkingStatus 
WHERE A.RecordStatus != 99";
            if(Branch >0)
            {
                zSQL += " AND A.BranchKey = @BranchKey";
            }
            if (Department > 0)
            {
                zSQL += " AND A.DepartmentKey = @DepartmentKey";
            }
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey = @TeamKey";
            }
            if (Name.Length > 0)
            {
                zSQL += " AND B.FullName LIKE @Name";
            }
            if (EmployeeID.Length > 0)
            {
                zSQL += " AND A.EmployeeID LIKE @EmployeeID";
            }
            if (IssueID.Length > 0)
            {
                zSQL += " AND B.IssueID LIKE @IssueID";
            }
            if (Position > 0)
            {
                zSQL += " AND A.PositionKey =@PositionKey";
            }
            if (Status > 0)
            {
                zSQL += " AND A.WorkingStatus =@WorkingStatus";
            }
            zSQL += " ORDER BY LEN(A.EmployeeID), A.EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = "%" + EmployeeID + "%";
                zCommand.Parameters.Add("@IssueID", SqlDbType.NVarChar).Value = "%" + IssueID + "%";

                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = Branch;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = Department;
                zCommand.Parameters.Add("@PositionKey", SqlDbType.Int).Value = Position;
                zCommand.Parameters.Add("@WorkingStatus", SqlDbType.Int).Value = Status;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable Load_Contract(string PersonalKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM HRM_Contract WHERE PersonalKey = @PersonalKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PersonalKey", SqlDbType.NVarChar).Value = PersonalKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int FindIssuedID(string IssueID, string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(A.EmployeeKey) FROM HRM_Employee A LEFT JOIN SYS_Personal B ON A.EmployeeKey = B.ParentKey  WHERE B.IssueID = @IssueID  AND  A.RecordStatus != 99";
            if (EmployeeKey != "")
            {
                zSQL += " AND B.ParentKey != @EmployeeKey ";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@IssueID", SqlDbType.NVarChar).Value = IssueID;
                if (EmployeeKey == "")
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                result = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        public static int FindCard(string EmployeeID, string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(EmployeeKey) FROM HRM_Employee WHERE EmployeeID = @EmployeeID AND RecordStatus != 99";
            if (EmployeeKey != "")
            {
                zSQL += " AND EmployeeKey != @EmployeeKey ";
            }

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                if (EmployeeKey == "")
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                result = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        public static int Gender(string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"SELECT B.Gender FROM [dbo].[HRM_Employee] A 
LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey
WHERE EmployeeKey = @EmployeeKey";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }

        public static int FindTeamKey(string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"SELECT TeamKey FROM HRM_Employee WHERE EmployeeKey = @EmployeeKey AND RecordStatus != 99";


            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                result = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
        public static int CountOverTime( string EmployeeKey)
        {
            int result = 0;
            string zSQL = @"
                            SELECT [dbo].[HRM_KiemTraTinhLuongThoiGian_HTSX_VP](@EmployeeKey)";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }

        public static int Check_CardID(string CardID)
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM HRM_Employee WHERE EmployeeID = @EmployeeID AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = CardID;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }

        public static int Count_CardID(string CardID, int TeamKey,DateTime Datetime)
        {
            DateTime zFromDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 23, 59, 59);
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM HRM_Employee 
                            WHERE EmployeeID = @EmployeeID 
                            AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) = @TeamKey
                            ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = CardID;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
    }
}
