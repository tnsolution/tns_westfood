﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;


namespace TNS.IVT
{
    public class Stock_Input_Object : Stock_Input_Info
    {
        private int _AutoKey = 0;
        private List<Stock_Input_Products_Info> _List_Product;
        private DataTable _ListNote;
        public List<Stock_Input_Products_Info> List_Product
        {
            get
            {
                return _List_Product;
            }

            set
            {
                _List_Product = value;
            }
        }
        public DataTable ListNote
        {
            get
            {
                return _ListNote;
            }

            set
            {
                _ListNote = value;
            }
        }
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public Stock_Input_Object(): base()
        {
            _List_Product = new List<Stock_Input_Products_Info>();
            ListNote = new DataTable();
        }

        #region [ Constructor Get Information ]
        public Stock_Input_Object(string OrderID) : base(OrderID)
        {
            DataTable ztb = Stock_Input_Products_Data.GetProducts(base.Key);
            _List_Product = new List<Stock_Input_Products_Info>();
            foreach (DataRow nRow in ztb.Rows)
            {
                Stock_Input_Products_Info Stock_Product = new Stock_Input_Products_Info(nRow);
                _List_Product.Add(Stock_Product);
            }
            GetNotes();
        }
        #endregion

        #region [ Constructor Update Information ]
        public void Save_Object()
        {
            string zResult = "";
            base.Save();
            if (base.HasInfo && base.IsCommandOK)
            {
                for (int i = 0; i < _List_Product.Count; i++)
                {
                    Stock_Input_Products_Info zProduct = (Stock_Input_Products_Info)_List_Product[i];
                    zProduct.OrderKey = base.Key;
                    zProduct.CreatedBy = base.CreatedBy;
                    zProduct.CreatedName = base.CreatedName;
                    zProduct.ModifiedBy = base.ModifiedBy;
                    zProduct.ModifiedName = base.ModifiedName;
                    switch (zProduct.RecordStatus)
                    {
                        case 99:
                            zProduct.DeleteDetail();
                            break;
                        case 1:
                            zProduct.Create();
                            break;
                        case 2:
                            zProduct.Update();
                            break;
                    }
                    zResult += zProduct.Message;
                }
            }
            base.Message += zResult;

        }
        public void DeleteObject()
        {
            string zResult = "";
            base.Delete();

            Stock_Input_Products_Info Stock_Product = new Stock_Input_Products_Info();
            Stock_Product.ModifiedBy = base.ModifiedBy;
            Stock_Product.ModifiedName = base.ModifiedName;
            Stock_Product.OrderKey = base.Key;
            Stock_Product.DeleteAll();
            zResult = Stock_Product.Message;
            base.Message = zResult;
        }
        public void InsertNote(string NoteContent)
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "[dbo].[IVT_Stock_Input_Note_INSERT]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(base.Key);
                zCommand.Parameters.Add("@NoteContent", SqlDbType.NText).Value = NoteContent;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = base.CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = base.CreatedName;

                base.Message = zCommand.ExecuteScalar().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                base.Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public void DeleteNote()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Stock_Input_Note SET RecordStatus = 99 WHERE AutoKey = @AutoKey ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;

                zResult = zCommand.ExecuteNonQuery().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                base.Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion
        private void GetNotes()
        {
            ListNote = new DataTable();
            string zSQL = "SELECT * FROM IVT_Stock_Input_Note "
                       + "WHERE OrderKey = @OrderKey AND RecordStatus != 99 ORDER BY CreatedOn DESC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);

                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = new Guid(base.Key);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(ListNote);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                base.Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

        }
    }
}
