﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Import_SanPham : Form
    {
        bool _IsSaved = false;//kiem tra đã lưu hay chưa
        FileInfo FileInfo;
        DataTable _GVEmployee;
        // private Employee_Object _Emp_Obj;
        int zError = 0;
        int zSuccess = 0;
        int _IndexGV = 0;
        int _TotalGV = 0;
        public Frm_Import_SanPham()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Open.Click += Btn_Open_Click;
            btn_SaveAll.Click += Btn_SaveAll_Click;
            GV_Employee.CellEndEdit += GV_Employee_CellEndEdit;
            GV_Employee.KeyDown += GV_Employee_KeyDown;
            timer1.Tick += Timer1_Tick;
            timer2.Tick += Timer2_Tick;
            timer3.Tick += Timer3_Tick;
        }

        private void Frm_Import_SanPham_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            PicLoading.Visible = false;
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Employee, true);
            Utils.DrawGVStyle(ref GV_Employee);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GV_Employee_Layout();
        }
        private void Btn_SaveAll_Click(object sender, EventArgs e)
        {
            if (GV_Employee.Rows.Count <= 0)
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu!", 1);
                return;
            }
            PicLoading.Visible = true;
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            _TotalGV = GV_Employee.Rows.Count;
            txt_Sql.Text = "0";
            timer1.Start();

        }

        private void Btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                GV_Employee.Rows.Clear();

                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!", 2);
                }
                else
                {
                    try
                    {
                        lblFileName.Text = FileInfo.Name;
                        //Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);

                        _GVEmployee = new DataTable();
                        _GVEmployee = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        GV_Employee_LoadData();
                        _IsSaved = false;
                        //  Cursor.Current = Cursors.Default;

                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(), 4);
                    }
                }
            }
        }


        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("ProductID", "Mã");
            GV_Employee.Columns.Add("ProductName", "Tên sản phẩm");
            GV_Employee.Columns.Add("BasicUnit", "Đơn vị tính");
            GV_Employee.Columns.Add("CategoryKey", "Loại");
            GV_Employee.Columns.Add("Parent", "Nhóm sản phẩm");
            GV_Employee.Columns.Add("Rank", "Sắp xếp");
            GV_Employee.Columns.Add("Description", "Ghi chú");
            GV_Employee.Columns.Add("Message", "Thông báo");

            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;
            GV_Employee.Columns["No"].Frozen = true;

            GV_Employee.Columns["ProductID"].Width = 100;
            GV_Employee.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["ProductID"].ReadOnly = true;
            GV_Employee.Columns["ProductID"].Frozen = true;

            GV_Employee.Columns["ProductName"].Width = 300;
            GV_Employee.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["ProductName"].Frozen = true;

            GV_Employee.Columns["BasicUnit"].Width = 100;
            GV_Employee.Columns["BasicUnit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["CategoryKey"].Width = 120;
            GV_Employee.Columns["CategoryKey"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["Parent"].Width = 100;
            GV_Employee.Columns["Parent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["Rank"].Width = 100;
            GV_Employee.Columns["Rank"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Description"].Width = 120;
            GV_Employee.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["Message"].Width = 150;
            GV_Employee.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.ColumnHeadersHeight = 50;
            GV_Employee.Rows.Clear();

        }
        private void GV_Employee_LoadData()
        {
            PicLoading.Visible = true;
            GV_Employee.Rows.Clear();
            _IndexGV = 0;
            _TotalGV = _GVEmployee.Rows.Count;
            timer3.Start();

        }

        private void GV_Employee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //CheckRow(GV_Employee.Rows[e.RowIndex]);
        }

        private void GV_Employee_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Utils.TNMessageBox("Bạn có xóa thông tin này ?", 2) == "Y")
                {
                    GV_Employee.CurrentRow.Visible = false;
                }
            }
        }
        #endregion

        private string CheckRow(DataGridViewRow zRow)
        {

            string Message = "";
            if (zRow.Cells["ProductID"].Value == null || zRow.Cells["ProductID"].Value.ToString() == "")
            {
                Message += "Tên sản phẩm trống  /";
            }
            if (zRow.Cells["ProductID"].Value == null || zRow.Cells["ProductID"].Value.ToString() == "")
            {
                Message += "Mã sản phẩm không bỏ trống /";
            }

            if (zRow.Cells["BasicUnit"].Value == null || zRow.Cells["BasicUnit"].Value.ToString() == "")
            {
                Message += "Đơn vị tính không bỏ trống /";
            }
            else
            {
                Product_Unit_Info zUnit = new Product_Unit_Info(zRow.Cells["BasicUnit"].Value.ToString());
                if (zUnit.Key == 0)
                {
                    Message += "Đơn vị tính chưa khai báo ! /";
                }
            }
            if (zRow.Cells["CategoryKey"].Value == null|| zRow.Cells["CategoryKey"].Value.ToString()== "")
            {
                zRow.Cells["CategoryKey"].Value = "";
            }
            else
            {
                Product_Category_Info zCategory = new Product_Category_Info(zRow.Cells["CategoryKey"].Value.ToString());

                if (zCategory.Key == 0)
                {
                    Message += " Mã loại không đúng ! /";
                }

            }
            if (zRow.Cells["Parent"].Value == null || zRow.Cells["Parent"].Value.ToString() == "")
            {
                Message += " Mã nhóm sản phẩm không bỏ trống ! /";
            }
            else
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(zRow.Cells["Parent"].Value.ToString().Trim());

                if (zProduct.ProductKey == "" || zProduct.Parent != "0")
                {
                    Message += " Mã nhóm sản phẩm không đúng ! /";
                }
            }

            if (zRow.Cells["Rank"].Value == null || zRow.Cells["Rank"].Value.ToString() == "")
            {
                zRow.Cells["Rank"].Value = "0";
            }
            else
            {
                int zTemp = 0;
                if (int.TryParse(zRow.Cells["Rank"].Value.ToString().Trim(), out zTemp))
                {

                }
                zRow.Cells["Rank"].Value = zTemp;
            }
            if (zRow.Cells["Description"].Value == null || zRow.Cells["Description"].Value.ToString() == "")
            {
                zRow.Cells["Description"].Value = "";
            }

            if (Message != "")
            {
                zRow.Cells["Message"].Value = Message;
                zRow.DefaultCellStyle.BackColor = Color.Tomato;
                zRow.Tag = null;
            }
            else
            {
                zRow.Cells["Message"].Value = "";
                zRow.DefaultCellStyle.BackColor = Color.White;
                zRow.Tag = 0;
            }
            return Message;
        }

        private void Save()
        {
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            txt_Sql.Text = zSuccess.ToString();
            txt_Error.Text = zError.ToString();

            _TotalGV = GV_Employee.Rows.Count;
            timer2.Start();
        }

        private string SaveRow(DataGridViewRow zRow)
        {

            string zMessage = "";

            string zProductID = zRow.Cells["ProductID"].Value.ToString().Trim();

            Product_Info zInfo = new Product_Info();
            zInfo.Get_Product_Info_ID(zProductID);
            zInfo.ProductID = zProductID;
            zInfo.ProductName = zRow.Cells["ProductName"].Value.ToString().Trim();
            Product_Unit_Info zUnit = new Product_Unit_Info(zRow.Cells["BasicUnit"].Value.ToString());
            zInfo.BasicUnit = zUnit.Key;
            zInfo.UnitName = zUnit.Unitname;

            Product_Category_Info zCategory = new Product_Category_Info(zRow.Cells["CategoryKey"].Value.ToString());
            zInfo.CategoryKey= zCategory.Key;

            Product_Info zProductParent = new Product_Info();
            zProductParent.Get_Product_Info_ID(zRow.Cells["Parent"].Value.ToString().Trim());
            zInfo.Parent = zProductParent.ProductKey;

            zInfo.Rank = zRow.Cells["Rank"].Value.ToInt();
            zInfo.Description = zRow.Cells["Description"].Value.ToString().Trim();

            zInfo.CreatedBy = SessionUser.UserLogin.Key;
            zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zInfo.ModifiedBy = SessionUser.UserLogin.Key;
            zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zInfo.Save();
            zMessage = TN_Message.Show(zInfo.Message);
            if (zMessage != "")
            {
                zRow.Cells["Message"].Value = zMessage;
                zRow.DefaultCellStyle.BackColor = Color.Tomato;
                zRow.Tag = null;
            }
            else
            {
                zRow.Tag = 1;
                if(zInfo.Message=="11")
                zRow.Cells["Message"].Value = "Thêm mới Thành công";
                else
                    zRow.Cells["Message"].Value = "Cập nhật Thành công";
            }
            return zMessage;
        }



        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (_IsSaved == true)
            {
                this.Close();
            }
            else
            {
                if (Utils.TNMessageBox("Dữ liệu bạn chưa được cập nhật !.", 2) == "Y")
                {
                    this.Close();
                }
            }
        }
        #endregion

        private void Timer1_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer1.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;

                if (GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag == null)
                {
                    string zMesage = CheckRow(GV_Employee.Rows[_IndexGV]);
                    if (zMesage != "")
                    {
                        zError++;
                        txt_Error.Text = zError.ToString();
                    }
                    else
                    {
                        zSuccess++;
                        //txt_Sql.Text = zSuccess.ToString();
                    }
                }
                _IndexGV++;
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                if (zError > 0)
                {
                    timer2.Stop();
                    PicLoading.Visible = false;
                    Utils.TNMessageBoxOK("Còn " + zError + " dòng không hợp lệ vui lòng kiểm tra lại!", 2);
                }
                else

                {
                    _IndexGV = 0;
                    zError = 0;
                    zSuccess = 0;
                    lbl_Sql.Text = "Số lượng đã lưu";
                    txt_Sql.Text = "0";
                    timer2.Start();
                }
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            //Save
            timer2.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;
                if (GV_Employee.Rows[_IndexGV].Cells["ProductID"].Value != null && GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag.ToString() == "0")
                {
                    string zMessage = SaveRow(GV_Employee.Rows[_IndexGV]);
                    if (zMessage != "")
                    {
                        zError++;
                    }
                    else
                    {
                        zSuccess++;
                    }

                }
                txt_Sql.Text = zSuccess.ToString();
                txt_Error.Text = zError.ToString();
                _IndexGV++;
                timer2.Start();
            }
            else
            {
                timer2.Stop();
                PicLoading.Visible = false;
                _IsSaved = true;
                if (zError > 0)
                {
                    string Status = "Import Excel form " + HeaderControl.Text + " > lỗi > SL Excel:" + txt_Amount_Excel.Text + " > SL Lỗi:" + txt_Error.Text + " > SL SQL:" + txt_Sql.Text;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    Utils.TNMessageBoxOK("Còn " + zError + " dòng chưa lưu !", 2);
                }
                else
                {
                    string Status = "Import Excel form " + HeaderControl.Text + " > thành công > SL Excel:" + txt_Amount_Excel.Text + " > SL Lỗi:" + txt_Error.Text + " > SL SQL:" + txt_Sql.Text;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    Utils.TNMessageBoxOK("Đã lưu thành công!", 3);
                }
            }
        }

        private void Timer3_Tick(object sender, EventArgs e)
        {
            timer3.Stop();
            if (_IndexGV < _TotalGV)
            {
                int i = _IndexGV;
                DataRow zRow = _GVEmployee.Rows[i];
                GV_Employee.Rows.Add();

                GV_Employee.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Employee.Rows[i].Cells["ProductID"].Value = zRow[1].ToString().Trim();
                GV_Employee.Rows[i].Cells["ProductName"].Value = zRow[2].ToString().Trim();
                GV_Employee.Rows[i].Cells["BasicUnit"].Value = zRow[3].ToString().Trim();
                GV_Employee.Rows[i].Cells["CategoryKey"].Value = zRow[4].ToString().Trim();
                GV_Employee.Rows[i].Cells["Parent"].Value = zRow[5].ToString().Trim();
                int zRank = 0;
                if (int.TryParse(zRow[6].ToString().Trim(), out zRank))
                {
                    GV_Employee.Rows[i].Cells["Rank"].Value = zRank;
                }
                GV_Employee.Rows[i].Cells["Description"].Value = zRow[7].ToString().Trim();
                txt_Amount_Excel.Text = (i + 1).ToString();
                _IndexGV++;
                timer3.Start();
            }
            else
            {
                PicLoading.Visible = false;
                timer3.Stop();
            }
        }
    }
}
