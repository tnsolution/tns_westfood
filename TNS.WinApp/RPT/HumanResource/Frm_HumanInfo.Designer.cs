﻿namespace TNS.WinApp
{
    partial class Frm_HumanInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_HumanInfo));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel_Full = new System.Windows.Forms.Panel();
            this.panel_right = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.GVWorkingLog = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader5 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel4 = new System.Windows.Forms.Panel();
            this.GVChild = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader4 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel3 = new System.Windows.Forms.Panel();
            this.GVFee = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel2 = new System.Windows.Forms.Panel();
            this.GVSalary = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_ReportTo = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_Position = new System.Windows.Forms.Label();
            this.txt_Team = new System.Windows.Forms.Label();
            this.txt_Department = new System.Windows.Forms.Label();
            this.txt_ThamNien = new System.Windows.Forms.Label();
            this.txt_WorkingStatus = new System.Windows.Forms.Label();
            this.txt_LeavingDate = new System.Windows.Forms.Label();
            this.txt_EmployeeID = new System.Windows.Forms.Label();
            this.txt_AccountCode2 = new System.Windows.Forms.Label();
            this.txt_TryDate = new System.Windows.Forms.Label();
            this.txt_AccountCode = new System.Windows.Forms.Label();
            this.txt_Score = new System.Windows.Forms.Label();
            this.txt_Overtime = new System.Windows.Forms.Label();
            this.txt_StatingDate = new System.Windows.Forms.Label();
            this.txt_Branch = new System.Windows.Forms.Label();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel_left = new System.Windows.Forms.Panel();
            this.txt_IsuePlace = new System.Windows.Forms.Label();
            this.txt_IsueDate = new System.Windows.Forms.Label();
            this.txt_IsuaID = new System.Windows.Forms.Label();
            this.txt_Address = new System.Windows.Forms.Label();
            this.txt_CompanyPhone = new System.Windows.Forms.Label();
            this.txt_BankName = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_BankCode = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Email = new System.Windows.Forms.Label();
            this.lbl_Email = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_CompanyPhone = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_Address = new System.Windows.Forms.Label();
            this.lbl_IsuaID = new System.Windows.Forms.Label();
            this.txt_Gender = new System.Windows.Forms.Label();
            this.lbl_Gender = new System.Windows.Forms.Label();
            this.txt_YearOld = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_Birthday = new System.Windows.Forms.Label();
            this.lbl_Birthday = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txt_FullName = new System.Windows.Forms.Label();
            this.panel_Full.SuspendLayout();
            this.panel_right.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVWorkingLog)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVChild)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVFee)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVSalary)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel_left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1306, 42);
            this.HeaderControl.TabIndex = 216;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "HỒ SƠ NHÂN NHÂN VIÊN - NGÀY THỐNG KÊ _/_/_";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel_Full
            // 
            this.panel_Full.AutoScrollMargin = new System.Drawing.Size(0, 500);
            this.panel_Full.BackColor = System.Drawing.Color.White;
            this.panel_Full.Controls.Add(this.panel_right);
            this.panel_Full.Controls.Add(this.panel_left);
            this.panel_Full.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Full.Location = new System.Drawing.Point(0, 42);
            this.panel_Full.Name = "panel_Full";
            this.panel_Full.Size = new System.Drawing.Size(1306, 746);
            this.panel_Full.TabIndex = 217;
            // 
            // panel_right
            // 
            this.panel_right.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_right.AutoScroll = true;
            this.panel_right.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel_right.Controls.Add(this.panel5);
            this.panel_right.Controls.Add(this.kryptonHeader5);
            this.panel_right.Controls.Add(this.panel4);
            this.panel_right.Controls.Add(this.kryptonHeader4);
            this.panel_right.Controls.Add(this.panel3);
            this.panel_right.Controls.Add(this.kryptonHeader3);
            this.panel_right.Controls.Add(this.panel2);
            this.panel_right.Controls.Add(this.kryptonHeader1);
            this.panel_right.Controls.Add(this.panel1);
            this.panel_right.Controls.Add(this.kryptonHeader2);
            this.panel_right.Location = new System.Drawing.Point(251, 3);
            this.panel_right.Name = "panel_right";
            this.panel_right.Size = new System.Drawing.Size(1052, 737);
            this.panel_right.TabIndex = 1;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel5.Controls.Add(this.GVWorkingLog);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 858);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1035, 264);
            this.panel5.TabIndex = 223;
            // 
            // GVWorkingLog
            // 
            this.GVWorkingLog.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVWorkingLog.AllowEditing = false;
            this.GVWorkingLog.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVWorkingLog.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVWorkingLog.AutoResize = true;
            this.GVWorkingLog.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVWorkingLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVWorkingLog.ExtendLastCol = true;
            this.GVWorkingLog.Location = new System.Drawing.Point(0, 0);
            this.GVWorkingLog.Name = "GVWorkingLog";
            this.GVWorkingLog.Rows.DefaultSize = 19;
            this.GVWorkingLog.Size = new System.Drawing.Size(1035, 264);
            this.GVWorkingLog.TabIndex = 284;
            // 
            // kryptonHeader5
            // 
            this.kryptonHeader5.AutoSize = false;
            this.kryptonHeader5.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader5.Location = new System.Drawing.Point(0, 828);
            this.kryptonHeader5.Name = "kryptonHeader5";
            this.kryptonHeader5.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader5.Size = new System.Drawing.Size(1035, 30);
            this.kryptonHeader5.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader5.TabIndex = 222;
            this.kryptonHeader5.Values.Description = "";
            this.kryptonHeader5.Values.Heading = "Lịch sử làm việc";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.GVChild);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 695);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1035, 133);
            this.panel4.TabIndex = 221;
            // 
            // GVChild
            // 
            this.GVChild.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVChild.AllowEditing = false;
            this.GVChild.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVChild.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVChild.AutoResize = true;
            this.GVChild.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVChild.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVChild.ExtendLastCol = true;
            this.GVChild.Location = new System.Drawing.Point(0, 0);
            this.GVChild.Name = "GVChild";
            this.GVChild.Rows.DefaultSize = 19;
            this.GVChild.Size = new System.Drawing.Size(1035, 133);
            this.GVChild.TabIndex = 284;
            // 
            // kryptonHeader4
            // 
            this.kryptonHeader4.AutoSize = false;
            this.kryptonHeader4.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader4.Location = new System.Drawing.Point(0, 665);
            this.kryptonHeader4.Name = "kryptonHeader4";
            this.kryptonHeader4.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader4.Size = new System.Drawing.Size(1035, 30);
            this.kryptonHeader4.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader4.TabIndex = 220;
            this.kryptonHeader4.Values.Description = "";
            this.kryptonHeader4.Values.Heading = "Hỗ trợ lao động nữ có con từ 1 đế 6 tuổi";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel3.Controls.Add(this.GVFee);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 452);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1035, 213);
            this.panel3.TabIndex = 219;
            // 
            // GVFee
            // 
            this.GVFee.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVFee.AllowEditing = false;
            this.GVFee.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVFee.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVFee.AutoResize = true;
            this.GVFee.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVFee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVFee.ExtendLastCol = true;
            this.GVFee.Location = new System.Drawing.Point(0, 0);
            this.GVFee.Name = "GVFee";
            this.GVFee.Rows.DefaultSize = 19;
            this.GVFee.Size = new System.Drawing.Size(1035, 213);
            this.GVFee.TabIndex = 284;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 422);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(1035, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 218;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Các khoản trích theo lương";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel2.Controls.Add(this.GVSalary);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 207);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1035, 215);
            this.panel2.TabIndex = 217;
            // 
            // GVSalary
            // 
            this.GVSalary.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVSalary.AllowEditing = false;
            this.GVSalary.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVSalary.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVSalary.AutoResize = true;
            this.GVSalary.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVSalary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVSalary.ExtendLastCol = true;
            this.GVSalary.Location = new System.Drawing.Point(0, 0);
            this.GVSalary.Name = "GVSalary";
            this.GVSalary.Rows.DefaultSize = 19;
            this.GVSalary.Size = new System.Drawing.Size(1035, 215);
            this.GVSalary.TabIndex = 284;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 177);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(1035, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 216;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Mức lương  và các khoản hỗ trợ";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.txt_ReportTo);
            this.panel1.Controls.Add(this.label35);
            this.panel1.Controls.Add(this.label37);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label50);
            this.panel1.Controls.Add(this.label48);
            this.panel1.Controls.Add(this.label42);
            this.panel1.Controls.Add(this.label40);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txt_Position);
            this.panel1.Controls.Add(this.txt_Team);
            this.panel1.Controls.Add(this.txt_Department);
            this.panel1.Controls.Add(this.txt_ThamNien);
            this.panel1.Controls.Add(this.txt_WorkingStatus);
            this.panel1.Controls.Add(this.txt_LeavingDate);
            this.panel1.Controls.Add(this.txt_EmployeeID);
            this.panel1.Controls.Add(this.txt_AccountCode2);
            this.panel1.Controls.Add(this.txt_TryDate);
            this.panel1.Controls.Add(this.txt_AccountCode);
            this.panel1.Controls.Add(this.txt_Score);
            this.panel1.Controls.Add(this.txt_Overtime);
            this.panel1.Controls.Add(this.txt_StatingDate);
            this.panel1.Controls.Add(this.txt_Branch);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1035, 147);
            this.panel1.TabIndex = 215;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label24.Location = new System.Drawing.Point(38, 124);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 16);
            this.label24.TabIndex = 213;
            this.label24.Text = "Quản lý";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label18.Location = new System.Drawing.Point(39, 32);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 16);
            this.label18.TabIndex = 213;
            this.label18.Text = "Chức vụ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label17.Location = new System.Drawing.Point(38, 56);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 16);
            this.label17.TabIndex = 213;
            this.label17.Text = "Tổ nhóm";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label16.Location = new System.Drawing.Point(39, 78);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 16);
            this.label16.TabIndex = 213;
            this.label16.Text = "Bộ phận";
            // 
            // txt_ReportTo
            // 
            this.txt_ReportTo.AutoSize = true;
            this.txt_ReportTo.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ReportTo.ForeColor = System.Drawing.Color.Navy;
            this.txt_ReportTo.Location = new System.Drawing.Point(107, 124);
            this.txt_ReportTo.Name = "txt_ReportTo";
            this.txt_ReportTo.Size = new System.Drawing.Size(12, 16);
            this.txt_ReportTo.TabIndex = 213;
            this.txt_ReportTo.Text = ":";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label35.Location = new System.Drawing.Point(383, 107);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(68, 16);
            this.label35.TabIndex = 213;
            this.label35.Text = "Thâm niên";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label37.Location = new System.Drawing.Point(382, 81);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(65, 16);
            this.label37.TabIndex = 213;
            this.label37.Text = "Tình trạng";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label30.Location = new System.Drawing.Point(383, 56);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(92, 16);
            this.label30.TabIndex = 213;
            this.label30.Text = "Ngày nghỉ việc";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label28.Location = new System.Drawing.Point(383, 32);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(116, 16);
            this.label28.TabIndex = 213;
            this.label28.Text = "Ngày cuối thử việc";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label32.Location = new System.Drawing.Point(39, 8);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(51, 16);
            this.label32.TabIndex = 213;
            this.label32.Text = "Mã Thẻ";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label50.Location = new System.Drawing.Point(768, 81);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(109, 16);
            this.label50.TabIndex = 213;
            this.label50.Text = "Mã kế toán cấp 2";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label48.Location = new System.Drawing.Point(768, 57);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(109, 16);
            this.label48.TabIndex = 213;
            this.label48.Text = "Mã kế toán cấp 1";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label42.Location = new System.Drawing.Point(768, 33);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(96, 16);
            this.label42.TabIndex = 213;
            this.label42.Text = "Loại tính lương";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label40.Location = new System.Drawing.Point(768, 8);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(79, 16);
            this.label40.TabIndex = 213;
            this.label40.Text = "Tính tăng ca";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label26.Location = new System.Drawing.Point(383, 7);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(85, 16);
            this.label26.TabIndex = 213;
            this.label26.Text = "Ngày bắt đầu";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label15.Location = new System.Drawing.Point(39, 102);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 16);
            this.label15.TabIndex = 213;
            this.label15.Text = "Khối";
            // 
            // txt_Position
            // 
            this.txt_Position.AutoSize = true;
            this.txt_Position.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Position.ForeColor = System.Drawing.Color.Navy;
            this.txt_Position.Location = new System.Drawing.Point(108, 33);
            this.txt_Position.Name = "txt_Position";
            this.txt_Position.Size = new System.Drawing.Size(12, 16);
            this.txt_Position.TabIndex = 213;
            this.txt_Position.Text = ":";
            // 
            // txt_Team
            // 
            this.txt_Team.AutoSize = true;
            this.txt_Team.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Team.ForeColor = System.Drawing.Color.Navy;
            this.txt_Team.Location = new System.Drawing.Point(108, 58);
            this.txt_Team.Name = "txt_Team";
            this.txt_Team.Size = new System.Drawing.Size(12, 16);
            this.txt_Team.TabIndex = 213;
            this.txt_Team.Text = ":";
            // 
            // txt_Department
            // 
            this.txt_Department.AutoSize = true;
            this.txt_Department.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Department.ForeColor = System.Drawing.Color.Navy;
            this.txt_Department.Location = new System.Drawing.Point(108, 79);
            this.txt_Department.Name = "txt_Department";
            this.txt_Department.Size = new System.Drawing.Size(12, 16);
            this.txt_Department.TabIndex = 213;
            this.txt_Department.Text = ":";
            // 
            // txt_ThamNien
            // 
            this.txt_ThamNien.AutoSize = true;
            this.txt_ThamNien.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ThamNien.ForeColor = System.Drawing.Color.Navy;
            this.txt_ThamNien.Location = new System.Drawing.Point(520, 105);
            this.txt_ThamNien.Name = "txt_ThamNien";
            this.txt_ThamNien.Size = new System.Drawing.Size(12, 16);
            this.txt_ThamNien.TabIndex = 213;
            this.txt_ThamNien.Text = ":";
            // 
            // txt_WorkingStatus
            // 
            this.txt_WorkingStatus.AutoSize = true;
            this.txt_WorkingStatus.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_WorkingStatus.ForeColor = System.Drawing.Color.Navy;
            this.txt_WorkingStatus.Location = new System.Drawing.Point(520, 82);
            this.txt_WorkingStatus.Name = "txt_WorkingStatus";
            this.txt_WorkingStatus.Size = new System.Drawing.Size(12, 16);
            this.txt_WorkingStatus.TabIndex = 213;
            this.txt_WorkingStatus.Text = ":";
            // 
            // txt_LeavingDate
            // 
            this.txt_LeavingDate.AutoSize = true;
            this.txt_LeavingDate.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_LeavingDate.ForeColor = System.Drawing.Color.Navy;
            this.txt_LeavingDate.Location = new System.Drawing.Point(520, 56);
            this.txt_LeavingDate.Name = "txt_LeavingDate";
            this.txt_LeavingDate.Size = new System.Drawing.Size(12, 16);
            this.txt_LeavingDate.TabIndex = 213;
            this.txt_LeavingDate.Text = ":";
            // 
            // txt_EmployeeID
            // 
            this.txt_EmployeeID.AutoSize = true;
            this.txt_EmployeeID.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_EmployeeID.ForeColor = System.Drawing.Color.Navy;
            this.txt_EmployeeID.Location = new System.Drawing.Point(108, 9);
            this.txt_EmployeeID.Name = "txt_EmployeeID";
            this.txt_EmployeeID.Size = new System.Drawing.Size(12, 16);
            this.txt_EmployeeID.TabIndex = 213;
            this.txt_EmployeeID.Text = ":";
            // 
            // txt_AccountCode2
            // 
            this.txt_AccountCode2.AutoSize = true;
            this.txt_AccountCode2.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AccountCode2.ForeColor = System.Drawing.Color.Navy;
            this.txt_AccountCode2.Location = new System.Drawing.Point(905, 81);
            this.txt_AccountCode2.Name = "txt_AccountCode2";
            this.txt_AccountCode2.Size = new System.Drawing.Size(12, 16);
            this.txt_AccountCode2.TabIndex = 213;
            this.txt_AccountCode2.Text = ":";
            // 
            // txt_TryDate
            // 
            this.txt_TryDate.AutoSize = true;
            this.txt_TryDate.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_TryDate.ForeColor = System.Drawing.Color.Navy;
            this.txt_TryDate.Location = new System.Drawing.Point(520, 31);
            this.txt_TryDate.Name = "txt_TryDate";
            this.txt_TryDate.Size = new System.Drawing.Size(12, 16);
            this.txt_TryDate.TabIndex = 213;
            this.txt_TryDate.Text = ":";
            // 
            // txt_AccountCode
            // 
            this.txt_AccountCode.AutoSize = true;
            this.txt_AccountCode.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_AccountCode.ForeColor = System.Drawing.Color.Navy;
            this.txt_AccountCode.Location = new System.Drawing.Point(905, 56);
            this.txt_AccountCode.Name = "txt_AccountCode";
            this.txt_AccountCode.Size = new System.Drawing.Size(12, 16);
            this.txt_AccountCode.TabIndex = 213;
            this.txt_AccountCode.Text = ":";
            // 
            // txt_Score
            // 
            this.txt_Score.AutoSize = true;
            this.txt_Score.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Score.ForeColor = System.Drawing.Color.Navy;
            this.txt_Score.Location = new System.Drawing.Point(905, 32);
            this.txt_Score.Name = "txt_Score";
            this.txt_Score.Size = new System.Drawing.Size(12, 16);
            this.txt_Score.TabIndex = 213;
            this.txt_Score.Text = ":";
            // 
            // txt_Overtime
            // 
            this.txt_Overtime.AutoSize = true;
            this.txt_Overtime.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Overtime.ForeColor = System.Drawing.Color.Navy;
            this.txt_Overtime.Location = new System.Drawing.Point(905, 7);
            this.txt_Overtime.Name = "txt_Overtime";
            this.txt_Overtime.Size = new System.Drawing.Size(12, 16);
            this.txt_Overtime.TabIndex = 213;
            this.txt_Overtime.Text = ":";
            // 
            // txt_StatingDate
            // 
            this.txt_StatingDate.AutoSize = true;
            this.txt_StatingDate.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_StatingDate.ForeColor = System.Drawing.Color.Navy;
            this.txt_StatingDate.Location = new System.Drawing.Point(520, 6);
            this.txt_StatingDate.Name = "txt_StatingDate";
            this.txt_StatingDate.Size = new System.Drawing.Size(12, 16);
            this.txt_StatingDate.TabIndex = 213;
            this.txt_StatingDate.Text = ":";
            // 
            // txt_Branch
            // 
            this.txt_Branch.AutoSize = true;
            this.txt_Branch.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Branch.ForeColor = System.Drawing.Color.Navy;
            this.txt_Branch.Location = new System.Drawing.Point(108, 102);
            this.txt_Branch.Name = "txt_Branch";
            this.txt_Branch.Size = new System.Drawing.Size(12, 16);
            this.txt_Branch.TabIndex = 213;
            this.txt_Branch.Text = ":";
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(1035, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 214;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Thông tin công tác";
            // 
            // panel_left
            // 
            this.panel_left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel_left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(167)))), ((int)(((byte)(69)))));
            this.panel_left.Controls.Add(this.txt_FullName);
            this.panel_left.Controls.Add(this.txt_IsuePlace);
            this.panel_left.Controls.Add(this.txt_IsueDate);
            this.panel_left.Controls.Add(this.txt_IsuaID);
            this.panel_left.Controls.Add(this.txt_Address);
            this.panel_left.Controls.Add(this.txt_CompanyPhone);
            this.panel_left.Controls.Add(this.txt_BankName);
            this.panel_left.Controls.Add(this.label6);
            this.panel_left.Controls.Add(this.txt_BankCode);
            this.panel_left.Controls.Add(this.label3);
            this.panel_left.Controls.Add(this.txt_Email);
            this.panel_left.Controls.Add(this.lbl_Email);
            this.panel_left.Controls.Add(this.label4);
            this.panel_left.Controls.Add(this.lbl_CompanyPhone);
            this.panel_left.Controls.Add(this.label2);
            this.panel_left.Controls.Add(this.lbl_Address);
            this.panel_left.Controls.Add(this.lbl_IsuaID);
            this.panel_left.Controls.Add(this.txt_Gender);
            this.panel_left.Controls.Add(this.lbl_Gender);
            this.panel_left.Controls.Add(this.txt_YearOld);
            this.panel_left.Controls.Add(this.label5);
            this.panel_left.Controls.Add(this.txt_Birthday);
            this.panel_left.Controls.Add(this.lbl_Birthday);
            this.panel_left.Controls.Add(this.label1);
            this.panel_left.Controls.Add(this.pictureBox1);
            this.panel_left.Location = new System.Drawing.Point(2, 3);
            this.panel_left.Name = "panel_left";
            this.panel_left.Size = new System.Drawing.Size(243, 740);
            this.panel_left.TabIndex = 0;
            // 
            // txt_IsuePlace
            // 
            this.txt_IsuePlace.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IsuePlace.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_IsuePlace.Location = new System.Drawing.Point(74, 407);
            this.txt_IsuePlace.Name = "txt_IsuePlace";
            this.txt_IsuePlace.Size = new System.Drawing.Size(164, 55);
            this.txt_IsuePlace.TabIndex = 213;
            this.txt_IsuePlace.Text = ":";
            // 
            // txt_IsueDate
            // 
            this.txt_IsueDate.AutoSize = true;
            this.txt_IsueDate.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IsueDate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_IsueDate.Location = new System.Drawing.Point(74, 382);
            this.txt_IsueDate.Name = "txt_IsueDate";
            this.txt_IsueDate.Size = new System.Drawing.Size(12, 16);
            this.txt_IsueDate.TabIndex = 213;
            this.txt_IsueDate.Text = ":";
            // 
            // txt_IsuaID
            // 
            this.txt_IsuaID.AutoSize = true;
            this.txt_IsuaID.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_IsuaID.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_IsuaID.Location = new System.Drawing.Point(74, 357);
            this.txt_IsuaID.Name = "txt_IsuaID";
            this.txt_IsuaID.Size = new System.Drawing.Size(12, 16);
            this.txt_IsuaID.TabIndex = 213;
            this.txt_IsuaID.Text = ":";
            // 
            // txt_Address
            // 
            this.txt_Address.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Address.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_Address.Location = new System.Drawing.Point(79, 466);
            this.txt_Address.Name = "txt_Address";
            this.txt_Address.Size = new System.Drawing.Size(164, 65);
            this.txt_Address.TabIndex = 213;
            this.txt_Address.Text = ":";
            // 
            // txt_CompanyPhone
            // 
            this.txt_CompanyPhone.AutoSize = true;
            this.txt_CompanyPhone.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_CompanyPhone.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_CompanyPhone.Location = new System.Drawing.Point(76, 531);
            this.txt_CompanyPhone.Name = "txt_CompanyPhone";
            this.txt_CompanyPhone.Size = new System.Drawing.Size(12, 16);
            this.txt_CompanyPhone.TabIndex = 213;
            this.txt_CompanyPhone.Text = ":";
            // 
            // txt_BankName
            // 
            this.txt_BankName.AutoSize = true;
            this.txt_BankName.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_BankName.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_BankName.Location = new System.Drawing.Point(77, 604);
            this.txt_BankName.Name = "txt_BankName";
            this.txt_BankName.Size = new System.Drawing.Size(12, 16);
            this.txt_BankName.TabIndex = 213;
            this.txt_BankName.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label6.Location = new System.Drawing.Point(8, 604);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 16);
            this.label6.TabIndex = 213;
            this.label6.Text = "Ngân hàng";
            // 
            // txt_BankCode
            // 
            this.txt_BankCode.AutoSize = true;
            this.txt_BankCode.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_BankCode.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_BankCode.Location = new System.Drawing.Point(77, 579);
            this.txt_BankCode.Name = "txt_BankCode";
            this.txt_BankCode.Size = new System.Drawing.Size(12, 16);
            this.txt_BankCode.TabIndex = 213;
            this.txt_BankCode.Text = ":";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label3.Location = new System.Drawing.Point(11, 579);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 16);
            this.label3.TabIndex = 213;
            this.label3.Text = "ATM";
            // 
            // txt_Email
            // 
            this.txt_Email.AutoSize = true;
            this.txt_Email.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Email.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_Email.Location = new System.Drawing.Point(76, 554);
            this.txt_Email.Name = "txt_Email";
            this.txt_Email.Size = new System.Drawing.Size(12, 16);
            this.txt_Email.TabIndex = 213;
            this.txt_Email.Text = ":";
            // 
            // lbl_Email
            // 
            this.lbl_Email.AutoSize = true;
            this.lbl_Email.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Email.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_Email.Location = new System.Drawing.Point(10, 554);
            this.lbl_Email.Name = "lbl_Email";
            this.lbl_Email.Size = new System.Drawing.Size(38, 16);
            this.lbl_Email.TabIndex = 213;
            this.lbl_Email.Text = "Emai";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label4.Location = new System.Drawing.Point(9, 407);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 16);
            this.label4.TabIndex = 213;
            this.label4.Text = "Nơi cấp";
            // 
            // lbl_CompanyPhone
            // 
            this.lbl_CompanyPhone.AutoSize = true;
            this.lbl_CompanyPhone.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_CompanyPhone.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_CompanyPhone.Location = new System.Drawing.Point(10, 531);
            this.lbl_CompanyPhone.Name = "lbl_CompanyPhone";
            this.lbl_CompanyPhone.Size = new System.Drawing.Size(33, 16);
            this.lbl_CompanyPhone.TabIndex = 213;
            this.lbl_CompanyPhone.Text = "SĐT";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label2.Location = new System.Drawing.Point(9, 380);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 16);
            this.label2.TabIndex = 213;
            this.label2.Text = "Ngày cấp";
            // 
            // lbl_Address
            // 
            this.lbl_Address.AutoSize = true;
            this.lbl_Address.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Address.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_Address.Location = new System.Drawing.Point(12, 466);
            this.lbl_Address.Name = "lbl_Address";
            this.lbl_Address.Size = new System.Drawing.Size(48, 16);
            this.lbl_Address.TabIndex = 213;
            this.lbl_Address.Text = "Địa chỉ";
            // 
            // lbl_IsuaID
            // 
            this.lbl_IsuaID.AutoSize = true;
            this.lbl_IsuaID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_IsuaID.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_IsuaID.Location = new System.Drawing.Point(9, 356);
            this.lbl_IsuaID.Name = "lbl_IsuaID";
            this.lbl_IsuaID.Size = new System.Drawing.Size(46, 16);
            this.lbl_IsuaID.TabIndex = 213;
            this.lbl_IsuaID.Text = "CMND";
            // 
            // txt_Gender
            // 
            this.txt_Gender.AutoSize = true;
            this.txt_Gender.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Gender.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_Gender.Location = new System.Drawing.Point(76, 329);
            this.txt_Gender.Name = "txt_Gender";
            this.txt_Gender.Size = new System.Drawing.Size(12, 16);
            this.txt_Gender.TabIndex = 213;
            this.txt_Gender.Text = ":";
            // 
            // lbl_Gender
            // 
            this.lbl_Gender.AutoSize = true;
            this.lbl_Gender.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Gender.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_Gender.Location = new System.Drawing.Point(7, 329);
            this.lbl_Gender.Name = "lbl_Gender";
            this.lbl_Gender.Size = new System.Drawing.Size(58, 16);
            this.lbl_Gender.TabIndex = 213;
            this.lbl_Gender.Text = "Giới tính";
            // 
            // txt_YearOld
            // 
            this.txt_YearOld.AutoSize = true;
            this.txt_YearOld.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_YearOld.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_YearOld.Location = new System.Drawing.Point(74, 303);
            this.txt_YearOld.Name = "txt_YearOld";
            this.txt_YearOld.Size = new System.Drawing.Size(12, 16);
            this.txt_YearOld.TabIndex = 213;
            this.txt_YearOld.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label5.Location = new System.Drawing.Point(6, 303);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 16);
            this.label5.TabIndex = 213;
            this.label5.Text = "Tuổi";
            // 
            // txt_Birthday
            // 
            this.txt_Birthday.AutoSize = true;
            this.txt_Birthday.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Birthday.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_Birthday.Location = new System.Drawing.Point(75, 278);
            this.txt_Birthday.Name = "txt_Birthday";
            this.txt_Birthday.Size = new System.Drawing.Size(12, 16);
            this.txt_Birthday.TabIndex = 213;
            this.txt_Birthday.Text = ":";
            // 
            // lbl_Birthday
            // 
            this.lbl_Birthday.AutoSize = true;
            this.lbl_Birthday.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Birthday.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lbl_Birthday.Location = new System.Drawing.Point(7, 278);
            this.lbl_Birthday.Name = "lbl_Birthday";
            this.lbl_Birthday.Size = new System.Drawing.Size(66, 16);
            this.lbl_Birthday.TabIndex = 213;
            this.lbl_Birthday.Text = "Ngày sinh";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.label1.Location = new System.Drawing.Point(8, 273);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 1);
            this.label1.TabIndex = 213;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(221, 215);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // txt_FullName
            // 
            this.txt_FullName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_FullName.Font = new System.Drawing.Font("Arial", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_FullName.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txt_FullName.Location = new System.Drawing.Point(9, 230);
            this.txt_FullName.Name = "txt_FullName";
            this.txt_FullName.Size = new System.Drawing.Size(223, 36);
            this.txt_FullName.TabIndex = 214;
            this.txt_FullName.Text = "HỌ VÀ TÊN";
            this.txt_FullName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frm_HumanInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1306, 788);
            this.Controls.Add(this.panel_Full);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_HumanInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_HumanInfo";
            this.Load += new System.EventHandler(this.Frm_HumanInfo_Load);
            this.panel_Full.ResumeLayout(false);
            this.panel_right.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVWorkingLog)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVChild)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVFee)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVSalary)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel_left.ResumeLayout(false);
            this.panel_left.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel_Full;
        private System.Windows.Forms.Panel panel_right;
        private System.Windows.Forms.Panel panel_left;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label txt_IsuaID;
        private System.Windows.Forms.Label txt_Address;
        private System.Windows.Forms.Label txt_CompanyPhone;
        private System.Windows.Forms.Label txt_Email;
        private System.Windows.Forms.Label lbl_Email;
        private System.Windows.Forms.Label lbl_CompanyPhone;
        private System.Windows.Forms.Label lbl_Address;
        private System.Windows.Forms.Label lbl_IsuaID;
        private System.Windows.Forms.Label lbl_Gender;
        private System.Windows.Forms.Label lbl_Birthday;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label txt_Gender;
        private System.Windows.Forms.Label txt_Birthday;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.Panel panel2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader5;
        private System.Windows.Forms.Panel panel4;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader4;
        private System.Windows.Forms.Panel panel3;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label txt_ReportTo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label txt_Position;
        private System.Windows.Forms.Label txt_Team;
        private System.Windows.Forms.Label txt_Department;
        private System.Windows.Forms.Label txt_LeavingDate;
        private System.Windows.Forms.Label txt_EmployeeID;
        private System.Windows.Forms.Label txt_TryDate;
        private System.Windows.Forms.Label txt_StatingDate;
        private System.Windows.Forms.Label txt_Branch;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label txt_ThamNien;
        private System.Windows.Forms.Label txt_WorkingStatus;
        private System.Windows.Forms.Label txt_AccountCode2;
        private System.Windows.Forms.Label txt_AccountCode;
        private System.Windows.Forms.Label txt_Score;
        private System.Windows.Forms.Label txt_Overtime;
        private C1.Win.C1FlexGrid.C1FlexGrid GVWorkingLog;
        private C1.Win.C1FlexGrid.C1FlexGrid GVChild;
        private C1.Win.C1FlexGrid.C1FlexGrid GVFee;
        private C1.Win.C1FlexGrid.C1FlexGrid GVSalary;
        private System.Windows.Forms.Label txt_IsuePlace;
        private System.Windows.Forms.Label txt_IsueDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txt_BankName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label txt_BankCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label txt_YearOld;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label txt_FullName;
    }
}