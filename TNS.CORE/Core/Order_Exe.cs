﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.CORE
{
    public class Order_Exe
    {
        public Order_Money_Model OrderMoney { get; set; }
        public int AutoKey { get; set; } = 0;
        public string Message { get; set; } = string.Empty;
        public string RoleID { get; set; } = string.Empty;

        public string CreatedBy { get; set; } = string.Empty;
        public string CreatedName { get; set; } = string.Empty;
        public string ModifiedBy { get; set; } = string.Empty;
        public string ModifiedName { get; set; } = string.Empty;
        public int RecordStatus { get; set; } = 0;

        public void Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Order_Money_INSERT";

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = OrderMoney.TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderMoney.OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = OrderMoney.EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = OrderMoney.EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = OrderMoney.EmployeeID.Trim();
                if (OrderMoney.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = OrderMoney.OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = OrderMoney.StageKey;
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = OrderMoney.Basket;
                zCommand.Parameters.Add("@Kg", SqlDbType.Float).Value = OrderMoney.Kg;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = OrderMoney.Time;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = OrderMoney.Money;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = OrderMoney.Category;
                zCommand.Parameters.Add("@Basket_Borrow", SqlDbType.Float).Value = OrderMoney.Basket_Borrow;
                zCommand.Parameters.Add("@Kg_Borrow", SqlDbType.Float).Value = OrderMoney.Kg_Borrow;
                zCommand.Parameters.Add("@Time_Borrow", SqlDbType.Float).Value = OrderMoney.Time_Borrow;
                zCommand.Parameters.Add("@Money_Borrow", SqlDbType.Float).Value = OrderMoney.Money_Borrow;
                zCommand.Parameters.Add("@Category_Borrow", SqlDbType.Int).Value = OrderMoney.Category_Borrow;
                zCommand.Parameters.Add("@Basket_Eat", SqlDbType.Float).Value = OrderMoney.Basket_Eat;
                zCommand.Parameters.Add("@Kg_Eat", SqlDbType.Float).Value = OrderMoney.Kg_Eat;
                zCommand.Parameters.Add("@Time_Eat", SqlDbType.Float).Value = OrderMoney.Time_Eat;
                zCommand.Parameters.Add("@Money_Eat", SqlDbType.Float).Value = OrderMoney.Money_Eat;
                zCommand.Parameters.Add("@Category_Eat", SqlDbType.Int).Value = OrderMoney.Category_Eat;
                zCommand.Parameters.Add("@TimePrivate", SqlDbType.Float).Value = OrderMoney.Time_Private;
                zCommand.Parameters.Add("@MoneyPrivate", SqlDbType.Float).Value = OrderMoney.Money_Private;
                zCommand.Parameters.Add("@KgPrivate", SqlDbType.Float).Value = OrderMoney.Kg_Private;
                zCommand.Parameters.Add("@BasketPrivate", SqlDbType.Float).Value = OrderMoney.Basket_Private;
                zCommand.Parameters.Add("@CategoryPrivate", SqlDbType.Int).Value = OrderMoney.Category_Private;
                zCommand.Parameters.Add("@Money_Dif_Private", SqlDbType.Money).Value = OrderMoney.Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday_Private", SqlDbType.Money).Value = OrderMoney.Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday_Private", SqlDbType.Money).Value = OrderMoney.Money_Dif_Holiday;
                zCommand.Parameters.Add("@Money_Dif_General", SqlDbType.Money).Value = OrderMoney.Money_Dif_General;
                zCommand.Parameters.Add("@Money_Dif_Sunday_General", SqlDbType.Money).Value = OrderMoney.Money_Dif_Sunday_General;
                zCommand.Parameters.Add("@Money_Dif_Holiday_General", SqlDbType.Money).Value = OrderMoney.Money_Dif_Holiday_General;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public void Update()
        {
            string zSQL = "FTR_Order_Money_UPDATE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = OrderMoney.TeamKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderMoney.OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = OrderMoney.EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = OrderMoney.EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = OrderMoney.EmployeeID.Trim();
                if (OrderMoney.OrderDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = OrderMoney.OrderDate;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = OrderMoney.StageKey;
                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = OrderMoney.Basket;
                zCommand.Parameters.Add("@Kg", SqlDbType.Float).Value = OrderMoney.Kg;
                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = OrderMoney.Time;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = OrderMoney.Money;
                zCommand.Parameters.Add("@Category", SqlDbType.Int).Value = OrderMoney.Category;
                zCommand.Parameters.Add("@Basket_Borrow", SqlDbType.Float).Value = OrderMoney.Basket_Borrow;
                zCommand.Parameters.Add("@Kg_Borrow", SqlDbType.Float).Value = OrderMoney.Kg_Borrow;
                zCommand.Parameters.Add("@Time_Borrow", SqlDbType.Float).Value = OrderMoney.Time_Borrow;
                zCommand.Parameters.Add("@Money_Borrow", SqlDbType.Float).Value = OrderMoney.Money_Borrow;
                zCommand.Parameters.Add("@Category_Borrow", SqlDbType.Int).Value = OrderMoney.Category_Borrow;
                zCommand.Parameters.Add("@Basket_Eat", SqlDbType.Float).Value = OrderMoney.Basket_Eat;
                zCommand.Parameters.Add("@Kg_Eat", SqlDbType.Float).Value = OrderMoney.Kg_Eat;
                zCommand.Parameters.Add("@Time_Eat", SqlDbType.Float).Value = OrderMoney.Time_Eat;
                zCommand.Parameters.Add("@Money_Eat", SqlDbType.Float).Value = OrderMoney.Money_Eat;
                zCommand.Parameters.Add("@Category_Eat", SqlDbType.Int).Value = OrderMoney.Category_Eat;
                zCommand.Parameters.Add("@TimePrivate", SqlDbType.Float).Value = OrderMoney.Time_Private;
                zCommand.Parameters.Add("@MoneyPrivate", SqlDbType.Float).Value = OrderMoney.Money_Private;
                zCommand.Parameters.Add("@KgPrivate", SqlDbType.Float).Value = OrderMoney.Kg_Private;
                zCommand.Parameters.Add("@BasketPrivate", SqlDbType.Float).Value = OrderMoney.Basket_Private;
                zCommand.Parameters.Add("@CategoryPrivate", SqlDbType.Int).Value = OrderMoney.Category_Private;
                zCommand.Parameters.Add("@Money_Dif_Private", SqlDbType.Money).Value = OrderMoney.Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday_Private", SqlDbType.Money).Value = OrderMoney.Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday_Private", SqlDbType.Money).Value = OrderMoney.Money_Dif_Holiday;
                zCommand.Parameters.Add("@Money_Dif_General", SqlDbType.Money).Value = OrderMoney.Money_Dif_General;
                zCommand.Parameters.Add("@Money_Dif_Sunday_General", SqlDbType.Money).Value = OrderMoney.Money_Dif_Sunday_General;
                zCommand.Parameters.Add("@Money_Dif_Holiday_General", SqlDbType.Money).Value = OrderMoney.Money_Dif_Holiday_General;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = RoleID;
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public void Save()
        {
            int IsExits = OrderExits();
            if (IsExits > 0)
                Update();
            else
                Create();
        }

        public int OrderExits()
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(AutoKey) FROM FTR_Order_Money WHERE OrderKey = @OrderKey AND EmployeeKey = @EmployeeKey AND RecordStatus <> 99 ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderMoney.OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = OrderMoney.EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return result;
        }

        public int TimeExits()
        {
            int result = 0;
            string zSQL = @"SELECT COUNT(AutoKey) FROM FTR_Money_TimeKeeping WHERE OrderKey = @OrderKey AND EmployeeKey = @EmployeeKey AND RecordStatus <> 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderMoney.OrderKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = OrderMoney.EmployeeKey;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                Message = ex.ToString();
            }
            return result;
        }

        public static string GetValue(List<Order_Object> ListObj)
        {
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            if (zConnect.State == ConnectionState.Closed)
            {
                zConnect.Open();
            }

            try
            {
                for (int i = 0; i < ListObj.Count; i++)
                {
                    Order_Object Obj = ListObj[i];
                    string OrderKey = "";
                    string zSQL = "FTR_Order_INSERT";

                    #region [Paramater ORDER]
                    SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                    zCommand.CommandType = CommandType.StoredProcedure;
                    zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = Obj.OrderID_Compare.Trim();
                    zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Obj.OrderID;
                    if (Obj.OrderDate == DateTime.MinValue)
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                    else
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Obj.OrderDate;
                    zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = Obj.TeamKey;
                    zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Obj.ProductKey;
                    zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Obj.ProductID;
                    zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Obj.ProductName.Trim();
                    zCommand.Parameters.Add("@QuantityDocument", SqlDbType.Float).Value = Obj.QuantityDocument;
                    zCommand.Parameters.Add("@QuantityReality", SqlDbType.Float).Value = Obj.QuantityReality;
                    zCommand.Parameters.Add("@QuantityLoss", SqlDbType.Float).Value = Obj.QuantityLoss;
                    zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Obj.UnitKey;
                    zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Obj.UnitName.Trim();
                    zCommand.Parameters.Add("@Category_Stage", SqlDbType.Int).Value = Obj.Category_Stage;
                    zCommand.Parameters.Add("@OrderIDFollow", SqlDbType.NVarChar).Value = Obj.OrderIDFollow.Trim();
                    zCommand.Parameters.Add("@PercentFinish", SqlDbType.Float).Value = Obj.PercentFinish;
                    zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Obj.RecordStatus;
                    zCommand.Parameters.Add("@WorkStatus", SqlDbType.Int).Value = Obj.WorkStatus;
                    zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Obj.Note.Trim();
                    zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Obj.Rank;
                    zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Obj.CreatedBy.Trim();
                    zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Obj.CreatedName.Trim();
                    zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Obj.ModifiedBy.Trim();
                    zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Obj.ModifiedName.Trim();
                    zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = "";
                    string Message = zCommand.ExecuteScalar().ToString();
                    if (Message.Substring(0, 2) == "11")
                        OrderKey = Message.Substring(2, 36);
                    else
                    {
                        OrderKey = "";
                        return Message;
                    }
                    zCommand.Dispose();
                    #endregion

                    foreach (Order_Rate_Info zRate in Obj.List_Rate)
                    {
                        #region [Paramater RATE]
                        zSQL = "FTR_Order_Rate_INSERT";
                        zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.StoredProcedure;
                        zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = zRate.OrderDate;
                        zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = zRate.CategoryKey;
                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = zRate.CreatedBy.Trim();
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = zRate.CreatedName.Trim();
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = zRate.ModifiedBy.Trim();
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = zRate.ModifiedName.Trim();
                        zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = "";
                        Message = zCommand.ExecuteScalar().ToString();
                        zCommand.Dispose();
                        #endregion
                    }

                    foreach (Order_Employee_Info zEmployee in Obj.List_Employee)
                    {
                        #region [Paramater EMPLOYEE]
                        zSQL = "FTR_Order_Employee_INSERT";
                        zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.StoredProcedure;
                        zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = zEmployee.OrderDate;
                        zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = zEmployee.EmployeeKey.Trim();
                        zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = zEmployee.EmployeeID.Trim();
                        zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                        zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = zEmployee.Basket;
                        zCommand.Parameters.Add("@Kilogram", SqlDbType.Float).Value = zEmployee.Kilogram;
                        zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = zEmployee.Time;
                        zCommand.Parameters.Add("@Borrow", SqlDbType.Int).Value = zEmployee.Borrow;
                        zCommand.Parameters.Add("@Share", SqlDbType.Int).Value = zEmployee.Share;
                        zCommand.Parameters.Add("@Private", SqlDbType.Int).Value = zEmployee.Private;
                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = zEmployee.CreatedBy.Trim();
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = zEmployee.CreatedName.Trim();
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = zEmployee.ModifiedBy.Trim();
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = zEmployee.ModifiedName.Trim();
                        zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = "";
                        Message = zCommand.ExecuteScalar().ToString();
                        zCommand.Dispose();
                        #endregion
                    }
                }
            }
            catch (Exception Err)
            {
                zResult = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }

            return zResult;
        }

        public static List<Order_Object> SaveList(List<Order_Object> ListObj)
        {
            List<Order_Object> zListError = new List<Order_Object>();

            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();

            if (zConnect.State == ConnectionState.Closed)
            {
                zConnect.Open();
            }

            try
            {
                for (int i = 0; i < ListObj.Count; i++)
                {
                    Order_Object Obj = ListObj[i];
                    if (Obj.Message == string.Empty)
                    {
                        string OrderKey = "";
                        string zSQL = "FTR_Order_INSERT";

                        #region [Paramater ORDER]
                        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                        zCommand.CommandType = CommandType.StoredProcedure;
                        zCommand.Parameters.Add("@OrderID_Compare", SqlDbType.NVarChar).Value = Obj.OrderID_Compare.Trim();
                        zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = Obj.OrderID;
                        if (Obj.OrderDate == DateTime.MinValue)
                            zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DBNull.Value;
                        else
                            zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = Obj.OrderDate;
                        zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = Obj.TeamKey;
                        zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = Obj.ProductKey;
                        zCommand.Parameters.Add("@ProductID", SqlDbType.NVarChar).Value = Obj.ProductID;
                        zCommand.Parameters.Add("@ProductName", SqlDbType.NVarChar).Value = Obj.ProductName.Trim();
                        zCommand.Parameters.Add("@QuantityDocument", SqlDbType.Float).Value = Obj.QuantityDocument;
                        zCommand.Parameters.Add("@QuantityReality", SqlDbType.Float).Value = Obj.QuantityReality;
                        zCommand.Parameters.Add("@QuantityLoss", SqlDbType.Float).Value = Obj.QuantityLoss;
                        zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = Obj.UnitKey;
                        zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = Obj.GroupKey;
                        zCommand.Parameters.Add("@GroupID", SqlDbType.NVarChar).Value = Obj.GroupID;
                        zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = Obj.UnitName.Trim();
                        zCommand.Parameters.Add("@Category_Stage", SqlDbType.Int).Value = Obj.Category_Stage;
                        zCommand.Parameters.Add("@OrderIDFollow", SqlDbType.NVarChar).Value = Obj.OrderIDFollow.Trim();
                        zCommand.Parameters.Add("@PercentFinish", SqlDbType.Float).Value = Obj.PercentFinish;
                        zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = Obj.RecordStatus;
                        zCommand.Parameters.Add("@WorkStatus", SqlDbType.Int).Value = Obj.WorkStatus;
                        zCommand.Parameters.Add("@Note", SqlDbType.NVarChar).Value = Obj.Note.Trim();
                        zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = Obj.Rank;
                        zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = Obj.CreatedBy.Trim();
                        zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = Obj.CreatedName.Trim();
                        zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = Obj.ModifiedBy.Trim();
                        zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = Obj.ModifiedName.Trim();
                        zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = "";
                        string Message = zCommand.ExecuteScalar().ToString();
                        zCommand.Dispose();
                        #endregion

                        if (Message.Substring(0, 2) == "11")
                        {
                            OrderKey = Message.Substring(2, 36);
                            foreach (Order_Rate_Info zRate in Obj.List_Rate)
                            {
                                #region [Paramater RATE]
                                zSQL = "FTR_Order_Rate_INSERT";
                                zCommand = new SqlCommand(zSQL, zConnect);
                                zCommand.CommandType = CommandType.StoredProcedure;
                                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                                zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = zRate.OrderDate;
                                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = zRate.CategoryKey;
                                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = zRate.CreatedBy.Trim();
                                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = zRate.CreatedName.Trim();
                                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = zRate.ModifiedBy.Trim();
                                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = zRate.ModifiedName.Trim();
                                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = "";
                                Message = zCommand.ExecuteScalar().ToString();
                                zCommand.Dispose();
                                #endregion
                            }
                            foreach (Order_Employee_Info zEmployee in Obj.List_Employee)
                            {
                                #region [Paramater EMPLOYEE]
                                zSQL = "FTR_Order_Employee_INSERT";
                                zCommand = new SqlCommand(zSQL, zConnect);
                                zCommand.CommandType = CommandType.StoredProcedure;
                                zCommand.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = zEmployee.OrderDate;
                                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = zEmployee.EmployeeKey.Trim();
                                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = zEmployee.EmployeeID.Trim();
                                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                                zCommand.Parameters.Add("@Basket", SqlDbType.Float).Value = zEmployee.Basket;
                                zCommand.Parameters.Add("@Kilogram", SqlDbType.Float).Value = zEmployee.Kilogram;
                                zCommand.Parameters.Add("@Time", SqlDbType.Float).Value = zEmployee.Time;
                                zCommand.Parameters.Add("@Borrow", SqlDbType.Int).Value = zEmployee.Borrow;
                                zCommand.Parameters.Add("@Share", SqlDbType.Int).Value = zEmployee.Share;
                                zCommand.Parameters.Add("@Private", SqlDbType.Int).Value = zEmployee.Private;
                                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = zEmployee.CreatedBy.Trim();
                                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = zEmployee.CreatedName.Trim();
                                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = zEmployee.ModifiedBy.Trim();
                                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = zEmployee.ModifiedName.Trim();
                                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = "";
                                Message = zCommand.ExecuteScalar().ToString();
                                zCommand.Dispose();
                                #endregion
                            }
                            
                        }
                        else
                        {
                            OrderKey = "";
                            zListError.Add(Obj);
                        }
                    }
                }
            }
            catch (Exception Err)
            {

            }
            finally
            {
                zConnect.Close();
            }

            return zListError;
        }
    }
}
