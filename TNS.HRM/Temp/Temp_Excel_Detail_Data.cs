﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
public class Temp_Excel_Detail_Data
{
    public static DataTable List()
    {
        DataTable zTable = new DataTable();
        string zSQL = "SELECT *,  AS [Key] FROM Temp_Excel_Detail WHERE RecordStatus != 99 ORDER BY [RANK]";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zTable);
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zstrMessage = "08" + ex.ToString();
        }
        return zTable;
    }
    public static DataTable EmployeeTimeList(string zEmployeeKey, DateTime zFromDate, DateTime zToDate)
    {
        DataTable zTable = new DataTable();
        string zSQL = @"SELECT  A.* FROM [dbo].[Temp_Excel_Detail] A
WHERE A.EmployeeKey =@EmployeeKey AND A.BeginTime BETWEEN @FromDate AND @ToDate AND A.RecordStatus <> 99
";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(zEmployeeKey);
            zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
            zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zTable);
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zstrMessage = ex.ToString();
        }
        return zTable;
    }
    public static DataTable LoadExcelData(int ExcelKey)
    {
        DataTable zTable = new DataTable();
        string zSQL = @" SELECT *,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName FROM [dbo].[Temp_Excel_Detail] 
 WHERE ExcelKey=@ExcelKey AND RecordStatus <> 99";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zAdapter.Fill(zTable);
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zstrMessage = ex.ToString();
        }
        return zTable;
    }

    public static string Delete_WK_Time_Permit(int ExcelKey)
    {
        string zResult = "";
        string zSQL = @"UPDATE dbo.HRM_Employee_Time SET RecordStatus =99 WHERE ExcelKey = @ExcelKey 
 UPDATE dbo.HRM_Time_Working SET RecordStatus = 99 WHERE WorkingID = @ExcelKey  
 UPDATE dbo.HRM_Employee_Rice SET RecordStatus= 99 WHERE ExcelKey = @ExcelKey 
";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string Mesages = ex.ToString();
        }
        return zResult;
    }
    public static int Count_Key_Rice(string RiceName)
    {
        int zResult = 0;
        string zSQL = @"SELECT COUNT(RiceKey) FROM [dbo].[HRM_Time_Rice] WHERE RiceName = @RiceName AND RecordStatus <> 99";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = RiceName.Trim();
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zResult = int.Parse(zCommand.ExecuteScalar().ToString());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zMessage = ex.ToString();
        }
        return zResult;
    }
    public static float Get_Money_Rice(string RiceName)
    {
        float zResult = 0;
        string zSQL = @"SELECT ISNULL([Money],0) FROM [dbo].[HRM_Time_Rice] WHERE RiceName = @RiceName AND RecordStatus <> 99";
        string zConnectionString = ConnectDataBase.ConnectionString;
        try
        {
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
            zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = RiceName.Trim();
            SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
            zResult = float.Parse(zCommand.ExecuteScalar().ToString());
            zCommand.Dispose();
            zConnect.Close();
        }
        catch (Exception ex)
        {
            string zMessage = ex.ToString();
        }
        return zResult;
    }
    
}
