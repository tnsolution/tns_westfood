﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.HRM
{
    public class Working_Month_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private DateTime _CheckDate;
        private float _Date_K;
        private float _Date_KP;
        private float _Date_TRE;
        private float _Date_LP;
        private float _Date_P;
        private float _Date_PT;
        private float _Date_VE0;
        private float _Date_RCG;
        private float _Date_different;
        private float _Overtime;
        private float _OverMoney;
        private float _Date_Working;
        private float _Date_Off;
        private float _Number_Rice;
        private float _Money_Rice;
        private float _Money_Rice_PTKP;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private float _Total_Time = 0;
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public DateTime CheckDate
        {
            get { return _CheckDate; }
            set { _CheckDate = value; }
        }
        public float Date_K
        {
            get { return _Date_K; }
            set { _Date_K = value; }
        }
        public float Date_KP
        {
            get { return _Date_KP; }
            set { _Date_KP = value; }
        }
        public float Date_TRE
        {
            get { return _Date_TRE; }
            set { _Date_TRE = value; }
        }
        public float Date_LP
        {
            get { return _Date_LP; }
            set { _Date_LP = value; }
        }
        public float Date_P
        {
            get { return _Date_P; }
            set { _Date_P = value; }
        }
        public float Date_PT
        {
            get { return _Date_PT; }
            set { _Date_PT = value; }
        }
        public float Date_VE0
        {
            get { return _Date_VE0; }
            set { _Date_VE0 = value; }
        }
        public float Date_RCG
        {
            get { return _Date_RCG; }
            set { _Date_RCG = value; }
        }
        public float Date_different
        {
            get { return _Date_different; }
            set { _Date_different = value; }
        }
        public float Overtime
        {
            get { return _Overtime; }
            set { _Overtime = value; }
        }
        public float OverMoney
        {
            get { return _OverMoney; }
            set { _OverMoney = value; }
        }
        public float Date_Working
        {
            get { return _Date_Working; }
            set { _Date_Working = value; }
        }
        public float Date_Off
        {
            get { return _Date_Off; }
            set { _Date_Off = value; }
        }
        public float Number_Rice
        {
            get { return _Number_Rice; }
            set { _Number_Rice = value; }
        }
        public float Money_Rice
        {
            get { return _Money_Rice; }
            set { _Money_Rice = value; }
        }
        public float Money_Rice_PTKP
        {
            get { return _Money_Rice_PTKP; }
            set { _Money_Rice_PTKP = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public float Total_Time
        {
            get
            {
                return _Total_Time;
            }

            set
            {
                _Total_Time = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Working_Month_Info()
        {
        }
        public Working_Month_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Working_Month WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["CheckDate"] != DBNull.Value)
                        _CheckDate = (DateTime)zReader["CheckDate"];
                    if (zReader["Date_K"] != DBNull.Value)
                        _Date_K = float.Parse(zReader["Date_K"].ToString());
                    if (zReader["Date_KP"] != DBNull.Value)
                        _Date_KP = float.Parse(zReader["Date_KP"].ToString());
                    if (zReader["Date_TRE"] != DBNull.Value)
                        _Date_TRE = float.Parse(zReader["Date_TRE"].ToString());
                    if (zReader["Date_LP"] != DBNull.Value)
                        _Date_LP = float.Parse(zReader["Date_LP"].ToString());
                    if (zReader["Date_P"] != DBNull.Value)
                        _Date_P = float.Parse(zReader["Date_P"].ToString());
                    if (zReader["Date_PT"] != DBNull.Value)
                        _Date_PT = float.Parse(zReader["Date_PT"].ToString());
                    if (zReader["Date_VE0"] != DBNull.Value)
                        _Date_VE0 = float.Parse(zReader["Date_VE0"].ToString());
                    if (zReader["Date_RCG"] != DBNull.Value)
                        _Date_RCG = float.Parse(zReader["Date_RCG"].ToString());
                    if (zReader["Date_different"] != DBNull.Value)
                        _Date_different = float.Parse(zReader["Date_different"].ToString());
                    if (zReader["Overtime"] != DBNull.Value)
                        _Overtime = float.Parse(zReader["Overtime"].ToString());
                    if (zReader["OverMoney"] != DBNull.Value)
                        _OverMoney = float.Parse(zReader["OverMoney"].ToString());
                    if (zReader["Date_Working"] != DBNull.Value)
                        _Date_Working = float.Parse(zReader["Date_Working"].ToString());
                    if (zReader["Date_Off"] != DBNull.Value)
                        _Date_Off = float.Parse(zReader["Date_Off"].ToString());
                    if (zReader["Number_Rice"] != DBNull.Value)
                        _Number_Rice = float.Parse(zReader["Number_Rice"].ToString());
                    if (zReader["Money_Rice"] != DBNull.Value)
                        _Money_Rice = float.Parse(zReader["Money_Rice"].ToString());
                    if (zReader["Money_Rice_PTKP"] != DBNull.Value)
                        _Money_Rice_PTKP = float.Parse(zReader["Money_Rice_PTKP"].ToString());
                    if (zReader["Total_Time"] != DBNull.Value)
                        _Total_Time = float.Parse(zReader["Total_Time"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }


        public Working_Month_Info(DataRow WorkingtInRow)
        {

            try
            {
                if (WorkingtInRow["AutoKey"] != DBNull.Value)
                    _AutoKey = int.Parse(WorkingtInRow["AutoKey"].ToString());
                _EmployeeKey = WorkingtInRow["EmployeeKey"].ToString();
                _EmployeeID = WorkingtInRow["EmployeeID"].ToString().Trim();
                _EmployeeName = WorkingtInRow["EmployeeName"].ToString().Trim();
                if (WorkingtInRow["CheckDate"] != DBNull.Value)
                    _CheckDate = (DateTime)WorkingtInRow["CheckDate"];
                if (WorkingtInRow["Date_K"] != DBNull.Value)
                    _Date_K = float.Parse(WorkingtInRow["Date_K"].ToString());
                if (WorkingtInRow["Date_KP"] != DBNull.Value)
                    _Date_KP = float.Parse(WorkingtInRow["Date_KP"].ToString());
                if (WorkingtInRow["Date_TRE"] != DBNull.Value)
                    _Date_TRE = float.Parse(WorkingtInRow["Date_TRE"].ToString());
                if (WorkingtInRow["Date_LP"] != DBNull.Value)
                    _Date_LP = float.Parse(WorkingtInRow["Date_LP"].ToString());
                if (WorkingtInRow["Date_P"] != DBNull.Value)
                    _Date_P = float.Parse(WorkingtInRow["Date_P"].ToString());
                if (WorkingtInRow["Date_PT"] != DBNull.Value)
                    _Date_PT = float.Parse(WorkingtInRow["Date_PT"].ToString());
                if (WorkingtInRow["Date_VE0"] != DBNull.Value)
                    _Date_VE0 = float.Parse(WorkingtInRow["Date_VE0"].ToString());
                if (WorkingtInRow["Date_RCG"] != DBNull.Value)
                    _Date_RCG = float.Parse(WorkingtInRow["Date_RCG"].ToString());
                if (WorkingtInRow["Date_different"] != DBNull.Value)
                    _Date_different = float.Parse(WorkingtInRow["Date_different"].ToString());
                if (WorkingtInRow["Overtime"] != DBNull.Value)
                    _Overtime = float.Parse(WorkingtInRow["Overtime"].ToString());
                if (WorkingtInRow["OverMoney"] != DBNull.Value)
                    _OverMoney = float.Parse(WorkingtInRow["OverMoney"].ToString());
                if (WorkingtInRow["Date_Working"] != DBNull.Value)
                    _Date_Working = float.Parse(WorkingtInRow["Date_Working"].ToString());
                if (WorkingtInRow["Date_Off"] != DBNull.Value)
                    _Date_Off = float.Parse(WorkingtInRow["Date_Off"].ToString());
                if (WorkingtInRow["Number_Rice"] != DBNull.Value)
                    _Number_Rice = float.Parse(WorkingtInRow["Number_Rice"].ToString());
                if (WorkingtInRow["Money_Rice"] != DBNull.Value)
                    _Money_Rice = float.Parse(WorkingtInRow["Money_Rice"].ToString());
                if (WorkingtInRow["Money_Rice_PTKP"] != DBNull.Value)
                    _Money_Rice_PTKP = float.Parse(WorkingtInRow["Money_Rice_PTKP"].ToString());
                if (WorkingtInRow["Total_Time"] != DBNull.Value)
                    _Total_Time = float.Parse(WorkingtInRow["Total_Time"].ToString());
                if (WorkingtInRow["RecordStatus"] != DBNull.Value)
                    _RecordStatus = int.Parse(WorkingtInRow["RecordStatus"].ToString());
                if (WorkingtInRow["CreatedOn"] != DBNull.Value)
                    _CreatedOn = (DateTime)WorkingtInRow["CreatedOn"];
                _CreatedBy = WorkingtInRow["CreatedBy"].ToString().Trim();
                _CreatedName = WorkingtInRow["CreatedName"].ToString().Trim();
                if (WorkingtInRow["ModifiedOn"] != DBNull.Value)
                    _ModifiedOn = (DateTime)WorkingtInRow["ModifiedOn"];
                _ModifiedBy = WorkingtInRow["ModifiedBy"].ToString().Trim();
                _ModifiedName = WorkingtInRow["ModifiedName"].ToString().Trim();

            }
            catch (Exception Err)
            { _Message = "08" + Err.ToString(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Working_Month ("
+ " EmployeeKey ,EmployeeID ,EmployeeName ,CheckDate ,Date_K ,Date_KP ,Date_TRE ,Date_LP ,Date_P ,Date_PT ,Date_VE0 ,Date_RCG ,Date_different ,Overtime ,OverMoney ,Date_Working ,Date_Off ,Number_Rice ,Money_Rice ,Money_Rice_PTKP,Total_Time ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
+ " VALUES ( "
+ "@EmployeeKey ,@EmployeeID ,@EmployeeName ,@CheckDate ,@Date_K ,@Date_KP ,@Date_TRE ,@Date_LP ,@Date_P ,@Date_PT ,@Date_VE0 ,@Date_RCG ,@Date_different ,@Overtime ,@OverMoney ,@Date_Working ,@Date_Off ,@Number_Rice ,@Money_Rice ,@Money_Rice_PTKP,@Total_Time ,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT 11 ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_CheckDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = _CheckDate;
                zCommand.Parameters.Add("@Date_K", SqlDbType.Float).Value = _Date_K;
                zCommand.Parameters.Add("@Date_KP", SqlDbType.Float).Value = _Date_KP;
                zCommand.Parameters.Add("@Date_TRE", SqlDbType.Float).Value = _Date_TRE;
                zCommand.Parameters.Add("@Date_LP", SqlDbType.Float).Value = _Date_LP;
                zCommand.Parameters.Add("@Date_P", SqlDbType.Float).Value = _Date_P;
                zCommand.Parameters.Add("@Date_PT", SqlDbType.Float).Value = _Date_PT;
                zCommand.Parameters.Add("@Date_VE0", SqlDbType.Float).Value = _Date_VE0;
                zCommand.Parameters.Add("@Date_RCG", SqlDbType.Float).Value = _Date_RCG;
                zCommand.Parameters.Add("@Date_different", SqlDbType.Float).Value = _Date_different;
                zCommand.Parameters.Add("@Overtime", SqlDbType.Float).Value = _Overtime;
                zCommand.Parameters.Add("@OverMoney", SqlDbType.Float).Value = _OverMoney;
                zCommand.Parameters.Add("@Date_Working", SqlDbType.Float).Value = _Date_Working;
                zCommand.Parameters.Add("@Date_Off", SqlDbType.Float).Value = _Date_Off;
                zCommand.Parameters.Add("@Number_Rice", SqlDbType.Float).Value = _Number_Rice;
                zCommand.Parameters.Add("@Money_Rice", SqlDbType.Float).Value = _Money_Rice;
                zCommand.Parameters.Add("@Money_Rice_PTKP", SqlDbType.Float).Value = _Money_Rice_PTKP;
                zCommand.Parameters.Add("@Total_Time", SqlDbType.Float).Value = _Total_Time;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Working_Month SET "
            + " EmployeeKey = @EmployeeKey,"
            + " EmployeeID = @EmployeeID,"
            + " EmployeeName = @EmployeeName,"
            + " CheckDate = @CheckDate,"
            + " Date_K = @Date_K,"
            + " Date_KP = @Date_KP,"
            + " Date_TRE = @Date_TRE,"
            + " Date_LP = @Date_LP,"
            + " Date_P = @Date_P,"
            + " Date_PT = @Date_PT,"
            + " Date_VE0 = @Date_VE0,"
            + " Date_RCG = @Date_RCG,"
            + " Date_different = @Date_different,"
            + " Overtime = @Overtime,"
            + " OverMoney = @OverMoney,"
            + " Date_Working = @Date_Working,"
            + " Date_Off = @Date_Off,"
            + " Number_Rice = @Number_Rice,"
            + " Money_Rice = @Money_Rice,"
            + " Money_Rice_PTKP = @Money_Rice_PTKP,"
              + " Total_Time = @Total_Time,"
            + " RecordStatus = @RecordStatus,"
            + " CreatedOn = @CreatedOn,"
            + " CreatedBy = @CreatedBy,"
            + " CreatedName = @CreatedName,"
            + " ModifiedOn = @ModifiedOn,"
            + " ModifiedBy = @ModifiedBy,"
            + " ModifiedName = @ModifiedName"
           + " WHERE AutoKey = @AutoKey"
            + " SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_CheckDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@CheckDate", SqlDbType.DateTime).Value = _CheckDate;
                zCommand.Parameters.Add("@Date_K", SqlDbType.Float).Value = _Date_K;
                zCommand.Parameters.Add("@Date_KP", SqlDbType.Float).Value = _Date_KP;
                zCommand.Parameters.Add("@Date_TRE", SqlDbType.Float).Value = _Date_TRE;
                zCommand.Parameters.Add("@Date_LP", SqlDbType.Float).Value = _Date_LP;
                zCommand.Parameters.Add("@Date_P", SqlDbType.Float).Value = _Date_P;
                zCommand.Parameters.Add("@Date_PT", SqlDbType.Float).Value = _Date_PT;
                zCommand.Parameters.Add("@Date_VE0", SqlDbType.Float).Value = _Date_VE0;
                zCommand.Parameters.Add("@Date_RCG", SqlDbType.Float).Value = _Date_RCG;
                zCommand.Parameters.Add("@Date_different", SqlDbType.Float).Value = _Date_different;
                zCommand.Parameters.Add("@Overtime", SqlDbType.Float).Value = _Overtime;
                zCommand.Parameters.Add("@OverMoney", SqlDbType.Float).Value = _OverMoney;
                zCommand.Parameters.Add("@Date_Working", SqlDbType.Float).Value = _Date_Working;
                zCommand.Parameters.Add("@Date_Off", SqlDbType.Float).Value = _Date_Off;
                zCommand.Parameters.Add("@Number_Rice", SqlDbType.Float).Value = _Number_Rice;
                zCommand.Parameters.Add("@Money_Rice", SqlDbType.Float).Value = _Money_Rice;
                zCommand.Parameters.Add("@Money_Rice_PTKP", SqlDbType.Float).Value = _Money_Rice_PTKP;
                zCommand.Parameters.Add("@Total_Time", SqlDbType.Float).Value = _Total_Time;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Working_Month SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(DateTime FromDate, DateTime ToDate)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Working_Month SET [RecordStatus] = 99 WHERE CheckDate BETWEEN @FromDate AND @ToDate SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion

    }
}
