﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNS.CORE
{
    public class Order_Money_Model
    {
        public string EmployeeKey { get; set; } = "";
        public string EmployeeName { get; set; } = "";
        public string EmployeeID { get; set; } = "";
        public DateTime OrderDate { get; set; } = new DateTime();
        public string OrderKey { get; set; } = "";
        public int TeamKey { get; set; } = 0;
        public int StageKey { get; set; } = 0;
        public float Basket { get; set; } = 0;
        public float Kg { get; set; } = 0;
        public float Time { get; set; } = 0;
        public float Money { get; set; } = 0;
        public int Category { get; set; } = 0;
        //Borrow
        public float Basket_Borrow { get; set; } = 0;
        public float Kg_Borrow { get; set; } = 0;
        public float Time_Borrow { get; set; } = 0;
        public float Money_Borrow { get; set; } = 0;
        public int Category_Borrow { get; set; } = 0;
        //Eat
        public float Basket_Eat { get; set; } = 0;
        public float Kg_Eat { get; set; } = 0;
        public float KgEat { get; set; } = 0;
        public float Time_Eat { get; set; } = 0;
        public float Money_Eat { get; set; } = 0;
        public float TimeEat { get; set; } = 0;
        public float MoneyEat { get; set; } = 0;
        public int Category_Eat { get; set; } = 0;
        //public
        public float Timepublic { get; set; } = 0;
        public float Moneypublic { get; set; } = 0;
        public float Kgpublic { get; set; } = 0;
        public float Basketpublic { get; set; } = 0;
        public int Categorypublic { get; set; } = 0;
        //dif
        public double Money_Dif { get; set; } = 0;
        public double Money_Dif_Sunday { get; set; } = 0;
        public double Money_Dif_Holiday { get; set; } = 0;
        public double Money_Dif_General { get; set; } = 0;
        public double Money_Dif_Sunday_General { get; set; } = 0;
        public double Money_Dif_Holiday_General { get; set; } = 0;
        //
        public float Basket_Private { get; set; } = 0;
        public float Time_Private { get; set; } = 0;
        public float Kg_Private { get; set; } = 0;
        public float Money_Private { get; set; } = 0;
        public int Category_Private { get; set; } =0;

        //

    }
}