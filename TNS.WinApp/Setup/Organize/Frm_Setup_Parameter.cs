﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Parameter : Form
    {
        public Frm_Setup_Parameter()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            txt_Parameter.KeyPress += txt_Parameter_KeyPress;
            txt_Parameter.Leave += Txt_Parameter_Leave;
            txt_Rank.KeyPress += txt_Rank_KeyPress;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Copy.Click += Btn_Copy_Click;
            rdo_Date.CheckedChanged += Rdo_Date_CheckedChanged;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

        }

        




        #region[Private]
        private int _Key = 0;
        DataTable _Active = new DataTable();
        #endregion
        private void Frm_Default_Parameter_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            dte_ApplyDate.Value = SessionUser.Date_Work;
            SetDefault();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            dte_DateSearch.Value= SessionUser.Date_Work;
            dte_DateSearch.Enabled = false;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Rdo_Date_CheckedChanged(object sender, EventArgs e)
        {
            if (rdo_Date.Checked == true)
                dte_DateSearch.Enabled = true;
            else
                dte_DateSearch.Enabled = false;
        }


        #region[Progess]

        private void LoadData()
        {

            Default_Parameter_Info zinfo = new Default_Parameter_Info(_Key);
            txt_ID.Text = zinfo.ID;
            txt_Name.Text = zinfo.Name;
            dte_ApplyDate.Value = zinfo.ApplyDate;
            txt_Parameter.Text = zinfo.Parameter.ToString("###,###,##0.##");
            txt_Rank.Text = zinfo.Rank.ToString();
            dte_EndDate.Value = zinfo.EndDate;
            txt_Description.Text = zinfo.Description;
            txt_Ext.Text = zinfo.Ext;
            btn_Del.Enabled = true;
            
            if (zinfo.EndDate == DateTime.MinValue)
                btn_Copy.Enabled = false;
            else
                btn_Copy.Enabled = true;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_ID.Text = "";
            txt_Name.Text = "";
            dte_ApplyDate.Value = SessionUser.Date_Work;
            txt_Parameter.Text = "0";
            txt_Rank.Text = "0";
            dte_EndDate.Value = DateTime.MinValue;
            txt_Description.Text = "";
            txt_Ext.Text = "";
            txt_ID.Focus();
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            btn_Del.Enabled = false;
            btn_Copy.Enabled = false;
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_ID.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã!",1);
                return;
            }
            float zParameter;
            if (!float.TryParse(txt_Parameter.Text, out zParameter))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!",1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!",1);
                return;
            }

            else
            {
                Default_Parameter_Info zinfo = new Default_Parameter_Info(_Key);
                zinfo.ID = txt_ID.Text.Trim().ToUpper();
                zinfo.Name = txt_Name.Text.Trim();
                zinfo.ApplyDate = dte_ApplyDate.Value;
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Parameter = float.Parse(txt_Parameter.Text);
                zinfo.EndDate = dte_EndDate.Value;
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Ext = txt_Ext.Text.Trim();

                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == "11" || zinfo.Message == "20")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",4);
                    _Key = zinfo.Key;
                    DisplayData();
                    LoadData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message,4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.",2) == "Y")
                {
                    Default_Parameter_Info zinfo = new Default_Parameter_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == "30")
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công!",3);
                        DisplayData();
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message,4);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }

        #endregion
        private void Txt_Parameter_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Parameter.Text.Trim(), out zAmount))
            {
                txt_Parameter.Text = zAmount.Toe1String();
                txt_Parameter.SelectionStart = txt_Parameter.Text.Length ;
            }
            else
            {
                MessageBox.Show("Lỗi!.Định dạng số.");
            }
        }
        private void txt_Parameter_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46||e.KeyChar==44)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46||e.KeyChar==44)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            _Key = 0;
            dte_EndDate.Value = DateTime.MinValue;
            Utils.TNMessageBoxOK("Sao chép thành công!Vui lòng chỉnh sửa thông tin.",1);
        }
        private void DisplayData()
        {
            DataTable In_Table = new DataTable();
            if (rdo_Date.Checked == true)
            In_Table = Default_Parameter_Data.Search(dte_DateSearch.Value, txt_Search.Text);
            else
            In_Table = Default_Parameter_Data.Search(txt_Search.Text);
            InitGV_Layout(In_Table);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][8].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _Key = 0;
                    LoadData();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh mục " + HeaderControl.Text + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }

        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 9;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã";
            GVData.Rows[0][2] = "Nội dung";
            GVData.Rows[0][3] = "Ngày áp dụng";
            GVData.Rows[0][4] = "Ngày hết hạn";
            GVData.Rows[0][5] = "Giá trị";
            GVData.Rows[0][6] = "Đơn vị";
            GVData.Rows[0][7] = "Diễn giải";
            GVData.Rows[0][8] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                if(rData[4].ToString()!="")
                    GVData.Rows[rIndex + 1].StyleNew.BackColor = Color.LightGray;
                GVData.Rows[rIndex + 1][5] = rData[5].Toe1String();
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[0];
            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].Width = 300;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 100;
            GVData.Cols[5].Width = 100;
            GVData.Cols[6].Width = 50;
            GVData.Cols[7].Width = 100;
            GVData.Cols[8].Visible = false;

            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
        }


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
                btn_Copy.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
