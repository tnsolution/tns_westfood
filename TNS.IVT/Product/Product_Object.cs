﻿using System.Collections.Generic;
using System.Data;
using TNS.Misc;
using TNS.SYS;

namespace TNS.IVT
{
    public class Product_Object : Product_Info
    {
        private List<Product_Info> _ChildProduct;
        public List<Product_Info> ChildProduct
        {
            get
            {
                return _ChildProduct;
            }

            set
            {
                _ChildProduct = value;
            }
        }

        public Product_Object() : base()
        {
            _ChildProduct = new List<Product_Info>();

        }
        public Product_Object(string Key) : base(Key)
        {
            _ChildProduct = new List<Product_Info>();
            DataTable zTable = Product_Data.List_Product_Of_Material_Search(Key, string.Empty);
            if (zTable.Rows.Count > 0)
            {
                foreach (DataRow r in zTable.Rows)
                {
                    _ChildProduct.Add(new Product_Info()
                    {
                        ProductKey = r["ProductKey"].ToString(),
                        ProductID = r["ProductID"].ToString(),
                        ProductName = r["ProductName"].ToString(),
                        Description = r["Description"].ToString(),
                        RecordStatus = r["RecordStatus"].ToInt(),
                        Parent = r["Parent"].ToString(),
                        BasicUnit = r["BasicUnit"].ToInt(),
                        CategoryKey = r["CategoryKey"].ToInt(),
                        Rank = r["Rank"].ToInt(),
                    });
                }
            }           
        }

        public void Save_Object()
        {
            string zResult = "";
            base.Save();
            if (base.Message.Substring(0, 2) == "20" ||
                base.Message.Substring(0, 2) == "11")
            {
                for (int i = 0; i < _ChildProduct.Count; i++)
                {
                    Product_Info zProduct = _ChildProduct[i];
                    zProduct.Parent = base.ProductKey;
                    zProduct.CreatedBy = base.ModifiedBy;
                    zProduct.CreatedName = base.ModifiedName;
                    zProduct.ModifiedBy = base.ModifiedBy;
                    zProduct.ModifiedName = base.ModifiedName;
                    switch (zProduct.RecordStatus)
                    {
                        case 99:
                            zProduct.Delete();
                            break;
                        case 1:
                            zProduct.Create();
                            break;
                        case 2:
                            zProduct.Update();
                            break;
                    }
                    zResult += zProduct.Message;
                }
            }
            base.Message += zResult;

        }
        public void DeleteObject()
        {
            base.Delete();
            for (int i = 0; i < _ChildProduct.Count; i++)
            {
                Product_Info zProduct = _ChildProduct[i];
                zProduct.ModifiedBy = SessionUser.UserLogin.Key;
                zProduct.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zProduct.Delete();
            }
        }
    }
}
