﻿namespace TNS.WinApp
{
    partial class Frm_Production_Adjusted
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Production_Adjusted));
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.Panel_Data = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.GVData1 = new System.Windows.Forms.DataGridView();
            this.GVSum1 = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.GVData2 = new System.Windows.Forms.DataGridView();
            this.GVSum2 = new System.Windows.Forms.DataGridView();
            this.txt_title = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Info = new System.Windows.Forms.Panel();
            this.btn_Apply = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_EditBegin = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_EditEnd = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Count = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label8 = new System.Windows.Forms.Label();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.Panel_Order = new System.Windows.Forms.Panel();
            this.GVOrder = new System.Windows.Forms.DataGridView();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Team = new System.Windows.Forms.Panel();
            this.GVTeam = new System.Windows.Forms.DataGridView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.dte_Date = new TNS.SYS.TNDateTimePicker();
            this.Header = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Panel_Right.SuspendLayout();
            this.Panel_Data.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSum1)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSum2)).BeginInit();
            this.panel1.SuspendLayout();
            this.Panel_Left.SuspendLayout();
            this.Panel_Order.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).BeginInit();
            this.Panel_Team.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).BeginInit();
            this.Panel_Search.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_Right
            // 
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.Panel_Right.Controls.Add(this.Panel_Data);
            this.Panel_Right.Controls.Add(this.Panel_Info);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(454, 42);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(912, 672);
            this.Panel_Right.TabIndex = 135;
            // 
            // Panel_Data
            // 
            this.Panel_Data.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Data.Controls.Add(this.tabControl1);
            this.Panel_Data.Controls.Add(this.txt_title);
            this.Panel_Data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Data.Location = new System.Drawing.Point(0, 55);
            this.Panel_Data.Name = "Panel_Data";
            this.Panel_Data.Size = new System.Drawing.Size(912, 617);
            this.Panel_Data.TabIndex = 204;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tabControl1.Location = new System.Drawing.Point(0, 30);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(912, 587);
            this.tabControl1.TabIndex = 139;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.GVData1);
            this.tabPage1.Controls.Add(this.GVSum1);
            this.tabPage1.Location = new System.Drawing.Point(4, 23);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(904, 560);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Chưa tính";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // GVData1
            // 
            this.GVData1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData1.Location = new System.Drawing.Point(3, 3);
            this.GVData1.Name = "GVData1";
            this.GVData1.RowHeadersWidth = 51;
            this.GVData1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData1.Size = new System.Drawing.Size(898, 489);
            this.GVData1.TabIndex = 0;
            // 
            // GVSum1
            // 
            this.GVSum1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVSum1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GVSum1.Enabled = false;
            this.GVSum1.Location = new System.Drawing.Point(3, 492);
            this.GVSum1.Name = "GVSum1";
            this.GVSum1.ReadOnly = true;
            this.GVSum1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.GVSum1.Size = new System.Drawing.Size(898, 65);
            this.GVSum1.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.GVData2);
            this.tabPage2.Controls.Add(this.GVSum2);
            this.tabPage2.Location = new System.Drawing.Point(4, 23);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(804, 560);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Đã tính";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // GVData2
            // 
            this.GVData2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData2.Location = new System.Drawing.Point(3, 3);
            this.GVData2.Name = "GVData2";
            this.GVData2.ReadOnly = true;
            this.GVData2.RowHeadersWidth = 51;
            this.GVData2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData2.Size = new System.Drawing.Size(798, 491);
            this.GVData2.TabIndex = 1;
            // 
            // GVSum2
            // 
            this.GVSum2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVSum2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GVSum2.Enabled = false;
            this.GVSum2.Location = new System.Drawing.Point(3, 494);
            this.GVSum2.Name = "GVSum2";
            this.GVSum2.ReadOnly = true;
            this.GVSum2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.GVSum2.Size = new System.Drawing.Size(798, 63);
            this.GVSum2.TabIndex = 2;
            // 
            // txt_title
            // 
            this.txt_title.AutoSize = false;
            this.txt_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.txt_title.Location = new System.Drawing.Point(0, 0);
            this.txt_title.Name = "txt_title";
            this.txt_title.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_title.Size = new System.Drawing.Size(912, 30);
            this.txt_title.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.txt_title.TabIndex = 200;
            this.txt_title.Values.Description = "";
            this.txt_title.Values.Heading = "Thông tin công nhân";
            // 
            // Panel_Info
            // 
            this.Panel_Info.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Info.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Info.Location = new System.Drawing.Point(0, 0);
            this.Panel_Info.Name = "Panel_Info";
            this.Panel_Info.Size = new System.Drawing.Size(912, 55);
            this.Panel_Info.TabIndex = 205;
            // 
            // btn_Apply
            // 
            this.btn_Apply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Apply.Location = new System.Drawing.Point(1110, 9);
            this.btn_Apply.Name = "btn_Apply";
            this.btn_Apply.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Apply.Size = new System.Drawing.Size(120, 40);
            this.btn_Apply.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Apply.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Apply.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Apply.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Apply.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Apply.TabIndex = 208;
            this.btn_Apply.Tag = "1";
            this.btn_Apply.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Apply.Values.Image")));
            this.btn_Apply.Values.Text = "Thêm nhanh";
            this.btn_Apply.Click += new System.EventHandler(this.btn_Apply_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Search.Location = new System.Drawing.Point(858, 9);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(120, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 206;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm";
            this.btn_Search.Visible = false;
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // btn_EditBegin
            // 
            this.btn_EditBegin.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_EditBegin.Location = new System.Drawing.Point(1110, 9);
            this.btn_EditBegin.Name = "btn_EditBegin";
            this.btn_EditBegin.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_EditBegin.Size = new System.Drawing.Size(120, 40);
            this.btn_EditBegin.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_EditBegin.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EditBegin.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_EditBegin.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EditBegin.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EditBegin.TabIndex = 206;
            this.btn_EditBegin.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditBegin.Values.Image")));
            this.btn_EditBegin.Values.Text = "Tính từ đầu";
            this.btn_EditBegin.Click += new System.EventHandler(this.btn_EditBegin_Click);
            // 
            // btn_EditEnd
            // 
            this.btn_EditEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_EditEnd.Location = new System.Drawing.Point(984, 9);
            this.btn_EditEnd.Name = "btn_EditEnd";
            this.btn_EditEnd.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_EditEnd.Size = new System.Drawing.Size(120, 40);
            this.btn_EditEnd.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_EditEnd.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EditEnd.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_EditEnd.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EditEnd.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_EditEnd.TabIndex = 206;
            this.btn_EditEnd.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_EditEnd.Values.Image")));
            this.btn_EditEnd.Values.Text = "Sửa dữ liệu";
            this.btn_EditEnd.Click += new System.EventHandler(this.btn_EditEnd_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btn_Apply);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.btn_Count);
            this.panel1.Controls.Add(this.btn_EditBegin);
            this.panel1.Controls.Add(this.btn_Save);
            this.panel1.Controls.Add(this.btn_EditEnd);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 714);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1366, 54);
            this.panel1.TabIndex = 203;
            // 
            // btn_Count
            // 
            this.btn_Count.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count.Location = new System.Drawing.Point(1239, 9);
            this.btn_Count.Name = "btn_Count";
            this.btn_Count.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Count.Size = new System.Drawing.Size(120, 40);
            this.btn_Count.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Count.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Count.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Count.TabIndex = 206;
            this.btn_Count.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Count.Values.Image")));
            this.btn_Count.Values.Text = "Tính toán";
            this.btn_Count.Click += new System.EventHandler(this.btn_Count_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(1236, 9);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(120, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 206;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Cập nhật";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label8.Location = new System.Drawing.Point(39, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 15);
            this.label8.TabIndex = 149;
            this.label8.Text = "Ngày";
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.Panel_Order);
            this.Panel_Left.Controls.Add(this.Panel_Team);
            this.Panel_Left.Controls.Add(this.Panel_Search);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 42);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(450, 672);
            this.Panel_Left.TabIndex = 136;
            // 
            // Panel_Order
            // 
            this.Panel_Order.Controls.Add(this.GVOrder);
            this.Panel_Order.Controls.Add(this.kryptonHeader2);
            this.Panel_Order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Order.Location = new System.Drawing.Point(0, 405);
            this.Panel_Order.Name = "Panel_Order";
            this.Panel_Order.Size = new System.Drawing.Size(450, 267);
            this.Panel_Order.TabIndex = 206;
            // 
            // GVOrder
            // 
            this.GVOrder.AllowUserToAddRows = false;
            this.GVOrder.AllowUserToDeleteRows = false;
            this.GVOrder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVOrder.ColumnHeadersHeight = 25;
            this.GVOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVOrder.Location = new System.Drawing.Point(0, 30);
            this.GVOrder.Name = "GVOrder";
            this.GVOrder.ReadOnly = true;
            this.GVOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVOrder.Size = new System.Drawing.Size(450, 237);
            this.GVOrder.TabIndex = 204;
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(450, 30);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader2.TabIndex = 200;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "Chọn đơn hàng";
            // 
            // Panel_Team
            // 
            this.Panel_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Team.Controls.Add(this.GVTeam);
            this.Panel_Team.Controls.Add(this.kryptonHeader1);
            this.Panel_Team.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Team.Location = new System.Drawing.Point(0, 55);
            this.Panel_Team.Name = "Panel_Team";
            this.Panel_Team.Size = new System.Drawing.Size(450, 350);
            this.Panel_Team.TabIndex = 205;
            // 
            // GVTeam
            // 
            this.GVTeam.AllowUserToAddRows = false;
            this.GVTeam.AllowUserToDeleteRows = false;
            this.GVTeam.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVTeam.ColumnHeadersHeight = 25;
            this.GVTeam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVTeam.Location = new System.Drawing.Point(0, 30);
            this.GVTeam.Name = "GVTeam";
            this.GVTeam.ReadOnly = true;
            this.GVTeam.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVTeam.Size = new System.Drawing.Size(450, 320);
            this.GVTeam.TabIndex = 204;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(450, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader1.TabIndex = 201;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Tổ nhóm";
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.Controls.Add(this.dte_Date);
            this.Panel_Search.Controls.Add(this.label8);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 0);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(450, 55);
            this.Panel_Search.TabIndex = 203;
            // 
            // dte_Date
            // 
            this.dte_Date.CustomFormat = "dd/MM/yyyy";
            this.dte_Date.Location = new System.Drawing.Point(76, 13);
            this.dte_Date.Name = "dte_Date";
            this.dte_Date.Size = new System.Drawing.Size(106, 20);
            this.dte_Date.TabIndex = 0;
            this.dte_Date.Value = new System.DateTime(((long)(0)));
            // 
            // Header
            // 
            this.Header.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Header.Size = new System.Drawing.Size(1366, 42);
            this.Header.TabIndex = 196;
            this.Header.Values.Description = "";
            this.Header.Values.Heading = "TÍNH NĂNG SUẤT CHIA LẠI CHO NHÓM";
            this.Header.Values.Image = ((System.Drawing.Image)(resources.GetObject("Header.Values.Image")));
            this.Header.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.Header.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.Header.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.DodgerBlue;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Location = new System.Drawing.Point(450, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(2, 672);
            this.label9.TabIndex = 204;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.DodgerBlue;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(452, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(2, 672);
            this.label1.TabIndex = 205;
            // 
            // Frm_Production_Adjusted
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 768);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.Header);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Production_Adjusted";
            this.Load += new System.EventHandler(this.Frm_Production_Adjusted_Load);
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Data.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSum1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSum2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.Panel_Left.ResumeLayout(false);
            this.Panel_Order.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVOrder)).EndInit();
            this.Panel_Team.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVTeam)).EndInit();
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Panel_Right;
        private TNS.SYS.TNDateTimePicker dte_Date;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.DataGridView GVData1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView GVData2;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader Header;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel Panel_Search;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel Panel_Data;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txt_title;
        private System.Windows.Forms.Panel Panel_Info;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Count;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_EditEnd;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_EditBegin;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Apply;
        private System.Windows.Forms.DataGridView GVSum1;
        private System.Windows.Forms.DataGridView GVSum2;
        private System.Windows.Forms.Panel Panel_Team;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Panel Panel_Order;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private System.Windows.Forms.DataGridView GVTeam;
        private System.Windows.Forms.DataGridView GVOrder;
    }
}