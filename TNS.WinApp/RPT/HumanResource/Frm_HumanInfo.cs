﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.Misc;
using TNS_Report;

namespace TNS.WinApp
{
    public partial class Frm_HumanInfo : Form
    {
        public DateTime ReportDate =DateTime.MinValue;
        public string EmployeeKey="";
        public Frm_HumanInfo()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            //btn_Import.Click += Btn_New_Click;
            //btn_Search.Click += Btn_Search_Click;
            //cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;

            //btn_Log.Click += Btn_Log_Click;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        private void Frm_HumanInfo_Load(object sender, EventArgs e)
        {
            HeaderControl.Text = "HỒ SƠ NHÂN NHÂN VIÊN - NGÀY THỐNG KÊ " + ReportDate.ToString("dd/MM/yyyy");

            DataTable ztb = Report_HumanResource.HOSONHANSU(ReportDate,EmployeeKey);
            if (ztb.Rows.Count > 0)
            {
                //Cá nhân
                DataRow r = ztb.Rows[0];
                if (r["EmployeeName"].ToString() != "")
                    txt_FullName.Text = r["EmployeeName"].ToString().ToUpper();
                else
                    txt_FullName.Text = "Chưa cập nhật.";
                DateTime zBirthday = DateTime.MinValue;
                if (r["BirthDay"].ToString() != "")
                {
                    zBirthday = DateTime.Parse(r["BirthDay"].ToString());
                    txt_Birthday.Text = PastValue(zBirthday.ToString("dd/MM/yyyy"));
                }
                else
                {
                    txt_Birthday.Text = PastValue(r["BirthDay"].ToString());
                }
                if (r["BirthDay"].ToString() != "")
                {
                    txt_YearOld.Text = PastValue(Utils.CalculateYearOld(zBirthday, DateTime.Now));
                }
                else
                    txt_YearOld.Text = PastValue("");

                if (r["Gender"].ToString() == "1")
                {
                    txt_Gender.Text = PastValue("Nam");
                }
                else
                {
                    txt_Gender.Text = PastValue("Nữ");
                }
                DateTime IsuaDate;
                if (r["IssueDate"].ToString() != "")
                {
                    IsuaDate = DateTime.Parse(r["IssueDate"].ToString());
                    txt_IsueDate.Text = PastValue(IsuaDate.ToString("dd/MM/yyyy"));
                }
                else
                {
                    txt_IsueDate.Text = PastValue("");
                }
                txt_IsuaID.Text = PastValue(r["IssueID"].ToString());
                txt_IsuePlace.Text = PastValue(r["IssuePlace"].ToString());

                txt_Address.Text = PastValue(r["HomeTown"].ToString());
                txt_CompanyPhone.Text = PastValue(r["CompanyPhone"].ToString());
                txt_Email.Text = PastValue(r["CompanyEmail"].ToString());
                txt_BankCode.Text = PastValue(r["BankCode"].ToString());
                txt_BankName.Text = PastValue(r["AddressBank"].ToString());
                //Công tác

                txt_Branch.Text = PastValue(r["BranchName"].ToString());
                txt_Department.Text = PastValue(r["DepartmentName"].ToString());
                txt_Team.Text = PastValue(r["TeamName"].ToString());
                txt_Position.Text = PastValue(r["PositionName"].ToString());
                txt_ReportTo.Text = PastValue(r["ReportTo"].ToString());
                txt_EmployeeID.Text = PastValue(r["EmployeeID"].ToString());
                DateTime zStartingDate = DateTime.MinValue;
                if (r["StartingDate"].ToString() != "")
                {
                    zStartingDate = DateTime.Parse(r["StartingDate"].ToString());
                    txt_StatingDate.Text = PastValue(zStartingDate.ToString("dd/MM/yyyy"));
                }
                else
                {
                    txt_StatingDate.Text = PastValue("");
                }

                DateTime zTryDate = DateTime.MinValue;
                if (r["LeavingTryDate"].ToString() != "")
                {
                    zTryDate = DateTime.Parse(r["LeavingTryDate"].ToString());
                    txt_TryDate.Text = PastValue(zTryDate.ToString("dd/MM/yyyy"));
                }
                else
                {
                    txt_TryDate.Text = PastValue("");
                }

                
                DateTime zLeavingDate = DateTime.MinValue;
                if (r["LeavingDate"].ToString() != "")
                {
                    zLeavingDate = DateTime.Parse(r["LeavingDate"].ToString());
                    txt_LeavingDate.Text = PastValue(zLeavingDate.ToString("dd/MM/yyyy"));
                }
                else
                {
                    txt_LeavingDate.Text = ":";
                }

                if (r["LeavingDate"].ToString() != "" && r["WorkingStatus"].ToString() == "2")
                {
                    txt_WorkingStatus.Text = PastValue("Đã nghỉ việc");
                    txt_WorkingStatus.ForeColor = Color.Red;
                }
                else
                {
                    txt_WorkingStatus.Text = PastValue("Đang làm việc");
                }
                //Thâm niên
                if (r["LeavingDate"].ToString() != "")
                {
                    string Temp = Utils.CalculatorTime(zStartingDate, zLeavingDate).ToString();
                    txt_ThamNien.Text = PastValue(Temp);
                }
                else
                {
                    string Temp = Utils.CalculatorTime(zStartingDate, ReportDate).ToString();
                    txt_ThamNien.Text = PastValue(Temp);
                }

                if (r["OverTime"].ToString() == "1")
                    txt_Overtime.Text = PastValue("Có");
                else
                    txt_Overtime.Text = PastValue("Không");
                if (r["ScoreStock"].ToString() == "1")
                    txt_Score.Text = PastValue("Bù công");
                else
                    txt_Score.Text = PastValue("Thời gian thực tế");

                txt_AccountCode.Text = PastValue(r["AccountCode"].ToString());
                txt_AccountCode2.Text = PastValue(r["AccountCode2"].ToString());
                
            }


            DataTable ztb_Salary = Report_HumanResource.MUCLUONG(ReportDate,EmployeeKey);
            if (ztb_Salary.Rows.Count > 0)
            {
                InitGVSalary_Layout(ztb_Salary);
            }
            else
            {
                GVSalary.Rows.Count = 0;
                GVSalary.Cols.Count = 0;
                GVSalary.Clear();
            }
            DataTable ztb_Fee = Report_HumanResource.TRICHLUONG(ReportDate,EmployeeKey);
            if (ztb_Fee.Rows.Count > 0)
            {
                InitGVFee_Layout(ztb_Fee);
            }
            else
            {
                GVFee.Rows.Count = 0;
                GVFee.Cols.Count = 0;
                GVFee.Clear();
            }
            DataTable ztb_Child = Report_HumanResource.CONNHO(ReportDate,EmployeeKey);
            if (ztb_Child.Rows.Count > 0)
            {
                InitGVChild_Layout(ztb_Child);
            }
            else
            {
                GVChild.Cols.Count = 0;
                GVChild.Rows.Count = 0;
                GVChild.Clear();
            }
            DataTable ztb_WorkingLog = Report_HumanResource.LICHSUCONGTAC(ReportDate,EmployeeKey);
            if (ztb_WorkingLog.Rows.Count > 0)
            {
                InitGVWorkingLog_Layout(ztb_WorkingLog);
            }
            else
            {
                GVWorkingLog.Cols.Count = 0;
                GVWorkingLog.Rows.Count = 0;
                GVWorkingLog.Clear();
            }

        }

        void InitGVSalary_Layout(DataTable TableView)
        {
            GVSalary.Rows.Count = 0;
            GVSalary.Cols.Count = 0;
            GVSalary.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GVSalary.Cols.Add(ToTalCol);
            GVSalary.Rows.Add(TotalRow);

            //Row Header
            GVSalary.Rows[0][0] = "Stt";
            GVSalary.Rows[0][1] = "Mã";
            GVSalary.Rows[0][2] = "Nội dung";
            GVSalary.Rows[0][3] = "Giá trị";
            GVSalary.Rows[0][4] = "Thời gian áp dụng";
            GVSalary.Rows[0][5] = "Diễn giải";
            GVSalary.Rows[0][6] = ""; // ẩn cột này

            //Style         
            //GVSalary.AllowFreezing = AllowFreezingEnum.Both;
            GVSalary.AllowResizing = AllowResizingEnum.Both;
            GVSalary.AllowMerging = AllowMergingEnum.Custom;
            GVSalary.SelectionMode = SelectionModeEnum.Row;
            GVSalary.VisualStyle = VisualStyle.Office2010Blue;
            GVSalary.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVSalary.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVSalary.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVSalary.Rows[rIndex + 1][0] = rIndex + 1;
                GVSalary.Rows[rIndex + 1][1] = rData[1];
                GVSalary.Rows[rIndex + 1][2] = rData[2];
                GVSalary.Rows[rIndex + 1][3] = rData[3].Toe1String();
                string zTime = "";
                DateTime zFromDate;
                DateTime zToDate;
                if (rData[4].ToString() != "")
                {
                    zFromDate = DateTime.Parse(rData[4].ToString());
                    zTime += zFromDate.ToString("dd/MM/yyyy") + " - ";
                }
                else
                {
                    zTime += "Chưa cập nhật - ";
                }
                if (rData[5].ToString() != "")
                {
                    zToDate = DateTime.Parse(rData[5].ToString());
                    zTime += zToDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    zTime += "Chưa cập nhật";
                }
                GVSalary.Rows[rIndex + 1][4] = zTime;

                if (rData[5].ToString() != "")
                    GVSalary.Rows[rIndex + 1].StyleNew.BackColor = Color.LightPink;
                GVSalary.Rows[rIndex + 1][5] = rData[6];
                GVSalary.Rows[rIndex + 1][6] = rData[0];
            }

            GVSalary.Select(-1, -1);

            //Freeze Row and Column                              
            GVSalary.Rows.Fixed = 1;
            GVSalary.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVSalary.Rows[0].Height = 40;

            GVSalary.Cols[0].Width = 30;
            GVSalary.Cols[1].Width = 70;
            GVSalary.Cols[2].Width = 300;
            GVSalary.Cols[3].Width = 100;
            GVSalary.Cols[4].Width = 200;
            GVSalary.Cols[5].Width = 100;
            GVSalary.Cols[6].Visible = false;

            GVSalary.Cols[3].TextAlign = TextAlignEnum.RightCenter;
        }
        void InitGVFee_Layout(DataTable TableView)
        {
            GVFee.Rows.Count = 0;
            GVFee.Cols.Count = 0;
            GVFee.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GVFee.Cols.Add(ToTalCol);
            GVFee.Rows.Add(TotalRow);

            //Row Header
            GVFee.Rows[0][0] = "Stt";
            GVFee.Rows[0][1] = "Mã";
            GVFee.Rows[0][2] = "Nội dung";
            GVFee.Rows[0][3] = "Giá trị";
            GVFee.Rows[0][4] = "Thời gian áp dụng";
            GVFee.Rows[0][5] = "Diễn giải";
            GVFee.Rows[0][6] = ""; // ẩn cột này

            //Style         
            //GVFee.AllowFreezing = AllowFreezingEnum.Both;
            GVFee.AllowResizing = AllowResizingEnum.Both;
            GVFee.AllowMerging = AllowMergingEnum.Custom;
            GVFee.SelectionMode = SelectionModeEnum.Row;
            GVFee.VisualStyle = VisualStyle.Office2010Blue;
            GVFee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVFee.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVFee.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVFee.Rows[rIndex + 1][0] = rIndex + 1;
                GVFee.Rows[rIndex + 1][1] = rData[1];
                GVFee.Rows[rIndex + 1][2] = rData[2];
                GVFee.Rows[rIndex + 1][3] = rData[3].Toe1String(); ;
                string zTime = "";
                DateTime zFromDate;
                DateTime zToDate;
                if (rData[4].ToString() != "")
                {
                    zFromDate = DateTime.Parse(rData[4].ToString());
                    zTime += zFromDate.ToString("dd/MM/yyyy") + " - ";
                }
                else
                {
                    zTime += "Chưa cập nhật - ";
                }
                if (rData[5].ToString() != "")
                {
                    zToDate = DateTime.Parse(rData[5].ToString());
                    zTime += zToDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    zTime += "Chưa cập nhật";
                }
                GVFee.Rows[rIndex + 1][4] = zTime;
                if (rData[5].ToString() != "")
                    GVFee.Rows[rIndex + 1].StyleNew.BackColor = Color.LightPink;
                GVFee.Rows[rIndex + 1][5] = rData[6];
                GVFee.Rows[rIndex + 1][6] = rData[0];
            }

            GVFee.Select(-1, -1);

            //Freeze Row and Column                              
            GVFee.Rows.Fixed = 1;
            GVFee.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVFee.Rows[0].Height = 40;

            GVFee.Cols[0].Width = 30;
            GVFee.Cols[1].Width = 70;
            GVFee.Cols[2].Width = 300;
            GVFee.Cols[3].Width = 100;
            GVFee.Cols[4].Width = 200;
            GVFee.Cols[5].Width = 100;
            GVFee.Cols[6].Visible = false;

            GVFee.Cols[3].TextAlign = TextAlignEnum.RightCenter;
        }
        void InitGVChild_Layout(DataTable TableView)
        {
            GVChild.Rows.Count = 0;
            GVChild.Cols.Count = 0;
            GVChild.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GVChild.Cols.Add(ToTalCol);
            GVChild.Rows.Add(TotalRow);

            //Row Header
            GVChild.Rows[0][0] = "Stt";
            GVChild.Rows[0][1] = "Mã";
            GVChild.Rows[0][2] = "Nội dung";
            GVChild.Rows[0][3] = "Giá trị";
            GVChild.Rows[0][4] = "Thời gian áp dụng";
            GVChild.Rows[0][5] = "Diễn giải";
            GVChild.Rows[0][6] = ""; // ẩn cột này

            //Style         
            //GVChild.AllowFreezing = AllowFreezingEnum.Both;
            GVChild.AllowResizing = AllowResizingEnum.Both;
            GVChild.AllowMerging = AllowMergingEnum.Custom;
            GVChild.SelectionMode = SelectionModeEnum.Row;
            GVChild.VisualStyle = VisualStyle.Office2010Blue;
            GVChild.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVChild.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVChild.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVChild.Rows[rIndex + 1][0] = rIndex + 1;
                GVChild.Rows[rIndex + 1][1] = rData[1];
                GVChild.Rows[rIndex + 1][2] = rData[2];
                GVChild.Rows[rIndex + 1][3] = rData[3].Toe1String(); ;
                string zTime = "";
                DateTime zFromDate =DateTime.MinValue;
                DateTime zToDate =DateTime.MinValue;
                if (rData[4].ToString() != "")
                {
                    zFromDate = DateTime.Parse(rData[4].ToString());
                    zTime += zFromDate.ToString("dd/MM/yyyy") + " - ";
                }
                else
                {
                    zTime += "Chưa cập nhật - ";
                }
                if (rData[5].ToString() != "")
                {
                    zToDate = DateTime.Parse(rData[5].ToString());
                    zTime += zToDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    zTime += "Chưa cập nhật";
                }
                GVChild.Rows[rIndex + 1][4] = zTime;
                if (rData[5].ToString() != "")
                {if(zToDate < ReportDate)
                    GVChild.Rows[rIndex + 1].StyleNew.BackColor = Color.LightPink;
                }

                GVChild.Rows[rIndex + 1][5] = rData[6];
                GVChild.Rows[rIndex + 1][6] = rData[0];
            }

            GVChild.Select(-1, -1);

            //Freeze Row and Column                              
            GVChild.Rows.Fixed = 1;
            GVChild.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVChild.Rows[0].Height = 40;

            GVChild.Cols[0].Width = 30;
            GVChild.Cols[1].Width = 70;
            GVChild.Cols[2].Width = 300;
            GVChild.Cols[3].Width = 100;
            GVChild.Cols[4].Width = 200;
            GVChild.Cols[5].Width = 100;
            GVChild.Cols[6].Visible = false;

            GVChild.Cols[3].TextAlign = TextAlignEnum.RightCenter;
        }
        void InitGVWorkingLog_Layout(DataTable TableView)
        {
            GVWorkingLog.Rows.Count = 0;
            GVWorkingLog.Cols.Count = 0;
            GVWorkingLog.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 8;

            GVWorkingLog.Cols.Add(ToTalCol);
            GVWorkingLog.Rows.Add(TotalRow);

            //Row Header
            GVWorkingLog.Rows[0][0] = "Stt";
            GVWorkingLog.Rows[0][1] = "Thời gian";
            GVWorkingLog.Rows[0][2] = "Khối";
            GVWorkingLog.Rows[0][3] = "Bộ phận";
            GVWorkingLog.Rows[0][4] = "Tổ nhóm";
            GVWorkingLog.Rows[0][5] = "Chức vụ";
            GVWorkingLog.Rows[0][6] = "Diễn giải";
            GVWorkingLog.Rows[0][7] = ""; // ẩn cột này

            //Style         
            //GVWorkingLog.AllowFreezing = AllowFreezingEnum.Both;
            GVWorkingLog.AllowResizing = AllowResizingEnum.Both;
            GVWorkingLog.AllowMerging = AllowMergingEnum.Custom;
            GVWorkingLog.SelectionMode = SelectionModeEnum.Row;
            GVWorkingLog.VisualStyle = VisualStyle.Office2010Blue;
            GVWorkingLog.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVWorkingLog.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVWorkingLog.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVWorkingLog.Rows[rIndex + 1][0] = rIndex + 1;
                string zTime = "";
                DateTime zFromDate;
                DateTime zToDate;
                if (rData[1].ToString() != "")
                {
                    zFromDate = DateTime.Parse(rData[1].ToString());
                    zTime += zFromDate.ToString("dd/MM/yyyy") + " - ";
                }
                else
                {
                    zTime += "Chưa cập nhật - ";
                }
                if (rData[2].ToString() != "")
                {
                    zToDate = DateTime.Parse(rData[2].ToString());
                    zTime += zToDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    zTime += "Chưa cập nhật";
                }
                GVWorkingLog.Rows[rIndex + 1][1] = zTime;
                GVWorkingLog.Rows[rIndex + 1][2] = rData[3];
                GVWorkingLog.Rows[rIndex + 1][3] = rData[4];
                GVWorkingLog.Rows[rIndex + 1][4] = rData[5];
                if (rData[2].ToString() != "")
                    GVWorkingLog.Rows[rIndex + 1].StyleNew.BackColor = Color.LightPink;
                GVWorkingLog.Rows[rIndex + 1][5] = rData[6];
                GVWorkingLog.Rows[rIndex + 1][6] = rData[7];
                GVWorkingLog.Rows[rIndex + 1][7] = rData[0];
            }

            GVWorkingLog.Select(-1,-1);
            //Freeze Row and Column                              
            GVWorkingLog.Rows.Fixed = 1;
            GVWorkingLog.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVWorkingLog.Rows[0].Height = 40;

            GVWorkingLog.AutoSizeCols();
            GVWorkingLog.Cols[0].Width = 30;
            GVWorkingLog.Cols[7].Visible = false;

            GVWorkingLog.Cols[3].TextAlign = TextAlignEnum.RightCenter;
        }

        private string PastValue(string s)
        {
            if (s == "" || s == null)
            {
                return ": Chưa cập nhật.";
            }
            else
            {

                return ": " + s;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
