﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Fee_Category_Info
    {

        #region [ Field Name ]
        private int _CategoryKey =0;
        private string _CategoryID = "";
        private string _CategoryName = "";
        private string _Description = "";
        private int _Slug = 0;
        private double _Price = 0;
        private float _Quantity;
        private double _Amount = 0;
        private int _Rank = 0;
        private string _Ext = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public string CategoryID
        {
            get { return _CategoryID; }
            set { _CategoryID = value; }
        }
        public string CategoryName
        {
            get { return _CategoryName; }
            set { _CategoryName = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public double Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public float Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public double Amount
        {
            get { return _Amount; }
            set { _Amount = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Ext
        {
            get { return _Ext; }
            set { _Ext = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Fee_Category_Info()
        {
        }
        public Fee_Category_Info(int CategoryKey)
        {
            string zSQL = "SELECT * FROM FNC_Fee_Category WHERE CategoryKey = @CategoryKey AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryID = zReader["CategoryID"].ToString().Trim();
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["Quantity"] != DBNull.Value)
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Fee_Category_Info(string CategoryName)
        {
            string zSQL = "SELECT * FROM FNC_Fee_Category WHERE CategoryName = @CategoryName AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = CategoryName;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryID = zReader["CategoryID"].ToString().Trim();
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["Quantity"] != DBNull.Value)
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public void GetID(string CategoryID)
        {
            string zSQL = "SELECT * FROM FNC_Fee_Category WHERE CategoryID = @CategoryID AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = CategoryID.Trim();
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    _CategoryID = zReader["CategoryID"].ToString().Trim();
                    _CategoryName = zReader["CategoryName"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = double.Parse(zReader["Price"].ToString());
                    if (zReader["Quantity"] != DBNull.Value)
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                    if (zReader["Amount"] != DBNull.Value)
                        _Amount = double.Parse(zReader["Amount"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"INSERT INTO FNC_Fee_Category
(CategoryID, CategoryName, Description, Slug, Price, Quantity, Amount, Rank, Ext, 
CreatedOn, CreatedBy, CreatedName, ModifiedOn, ModifiedBy, ModifiedName )
VALUES 
( @CategoryID, @CategoryName, @Description,  @Slug,  @Price, @Quantity,  @Amount, @Rank,  @Ext, 
GETDATE(), @CreatedBy, @CreatedName,  GETDATE(),@ModifiedBy,  @ModifiedName )";
            zSQL += " SELECT '11' + CAST(CategoryKey AS NVARCHAR) FROM FNC_Fee_Category WHERE CategoryKey = SCOPE_IDENTITY()";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = _CategoryID.Trim();
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                _Message = zResult.Substring(0, 2);
                if(_Message=="11")
                {
                    _CategoryKey = int.Parse(zResult.Substring(2));
                }
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @"UPDATE dbo.FNC_Fee_Category SET 
			CategoryID = @CategoryID,
			CategoryName = @CategoryName,
			Description = @Description,
			Slug = @Slug,
			Price = @Price,
			Quantity = @Quantity,
			Amount = @Amount,
			Rank = @Rank,
			Ext = @Ext,
			ModifiedOn = GETDATE(),
			ModifiedBy = @ModifiedBy,
			ModifiedName = @ModifiedName
          WHERE RecordStatus != 99 AND CategoryKey = @CategoryKey";
            zSQL += " SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_CategoryKey > 0)
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                else
                    zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = DBNull.Value;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = _CategoryID.Trim();
                zCommand.Parameters.Add("@CategoryName", SqlDbType.NVarChar).Value = _CategoryName.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = _Price;
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @" UPDATE dbo.FNC_Fee_Category SET
      [RecordStatus] = 99,
      [ModifiedOn] = GETDATE(),
      [ModifiedBy] = @ModifiedBy,
      [ModifiedName] = @ModifiedName
      WHERE [CategoryKey] = @CategoryKey";
            zSQL += " SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_CategoryKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
