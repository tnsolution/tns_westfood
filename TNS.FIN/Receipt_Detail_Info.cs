﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.FIN
{
    public class Receipt_Detail_Info
    {
    
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ReceiptKey = "";
        private string _InvoiceKey = "";
        private string _ProductKey = "";
        private string _Description = "";
        private string _CurrencyID = "";
        private float _CurrencyRate;
        private double _AmountOrderMain = 0;
        private double _AmountOrderForeign = 0;
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        
        private int _Slug = 0;
        private string _Organization = "";
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                if (_Message.Length > 0)
                {
                    int zNumber = 0;
                    int.TryParse(_Message.Substring(0, 1), out zNumber);
                    if (zNumber <= 3) return true; else return false;
                }
                else
                {
                    return false;
                }
            }
        }
       
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ReceiptKey
        {
            get { return _ReceiptKey; }
            set { _ReceiptKey = value; }
        }
        public string InvoiceKey
        {
            get { return _InvoiceKey; }
            set { _InvoiceKey = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string CurrencyID
        {
            get { return _CurrencyID; }
            set { _CurrencyID = value; }
        }
        public float CurrencyRate
        {
            get { return _CurrencyRate; }
            set { _CurrencyRate = value; }
        }
        public double AmountOrderMain
        {
            get { return _AmountOrderMain; }
            set { _AmountOrderMain = value; }
        }
        public double AmountOrderForeign
        {
            get { return _AmountOrderForeign; }
            set { _AmountOrderForeign = value; }
        }
        public string CreditAccount
        {
            get { return _CreditAccount; }
            set { _CreditAccount = value; }
        }
        public string DebitAccount
        {
            get { return _DebitAccount; }
            set { _DebitAccount = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Receipt_Detail_Info()
        {
        }
        public Receipt_Detail_Info(DataRow InRow)
        {
            if (InRow["AutoKey"] != DBNull.Value)
            {
                _AutoKey = int.Parse(InRow["AutoKey"].ToString());
            }
            _ReceiptKey = InRow["ReceiptKey"].ToString();
            _InvoiceKey = InRow["InvoiceKey"].ToString();
            _ProductKey = InRow["ProductKey"].ToString();
            _Description = InRow["Description"].ToString();
            _CurrencyID = InRow["CurrencyID"].ToString();
            if (InRow["CurrencyRate"] != DBNull.Value)
            {
                _CurrencyRate = float.Parse(InRow["CurrencyRate"].ToString());
            }
            if (InRow["AmountOrderMain"] != DBNull.Value)
            {
                _AmountOrderMain = double.Parse(InRow["AmountOrderMain"].ToString());
            }
            if (InRow["AmountOrderForeign"] != DBNull.Value)
            {
                _AmountOrderForeign = double.Parse(InRow["AmountOrderForeign"].ToString());
            }
            _CreditAccount = InRow["CreditAccount"].ToString();
            _DebitAccount = InRow["DebitAccount"].ToString();
            if (InRow["Slug"] != DBNull.Value)
            {
                _Slug = int.Parse(InRow["Slug"].ToString());
            }
            _Organization = InRow["Organization"].ToString();
        }
        public Receipt_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FNC_Receipt_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _ReceiptKey = zReader["ReceiptKey"].ToString().Trim();
                    _InvoiceKey = zReader["InvoiceKey"].ToString().Trim();
                    _ProductKey = zReader["ProductKey"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _CurrencyID = zReader["CurrencyID"].ToString().Trim();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        _CurrencyRate = float.Parse(zReader["CurrencyRate"].ToString());
                    if (zReader["AmountOrderMain"] != DBNull.Value)
                        _AmountOrderMain = double.Parse(zReader["AmountOrderMain"].ToString());
                    if (zReader["AmountOrderForeign"] != DBNull.Value)
                        _AmountOrderForeign = double.Parse(zReader["AmountOrderForeign"].ToString());
                    _CreditAccount = zReader["CreditAccount"].ToString().Trim();
                    _DebitAccount = zReader["DebitAccount"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Receipt_Detail_Info(string ReceiptKey)
        {
            string zSQL = "SELECT * FROM FNC_Receipt_Detail WHERE ReceiptKey = @ReceiptKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.NVarChar).Value = ReceiptKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _ReceiptKey = zReader["ReceiptKey"].ToString().Trim();
                    _InvoiceKey = zReader["InvoiceKey"].ToString().Trim();
                    _ProductKey = zReader["ProductKey"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _CurrencyID = zReader["CurrencyID"].ToString().Trim();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        _CurrencyRate = float.Parse(zReader["CurrencyRate"].ToString());
                    if (zReader["AmountOrderMain"] != DBNull.Value)
                        _AmountOrderMain = double.Parse(zReader["AmountOrderMain"].ToString());
                    if (zReader["AmountOrderForeign"] != DBNull.Value)
                        _AmountOrderForeign = double.Parse(zReader["AmountOrderForeign"].ToString());
                    _CreditAccount = zReader["CreditAccount"].ToString().Trim();
                    _DebitAccount = zReader["DebitAccount"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "FNC_Receipt_Detail_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.NVarChar).Value = _ReceiptKey.Trim();
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.NVarChar).Value = _InvoiceKey.Trim();
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID.Trim();
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = _CurrencyRate;
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount.Trim();
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zResult = _Message.Substring(0, 2);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "FNC_Receipt_Detail_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.NVarChar).Value = _ReceiptKey.Trim();
                zCommand.Parameters.Add("@InvoiceKey", SqlDbType.NVarChar).Value = _InvoiceKey.Trim();
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID.Trim();
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = _CurrencyRate;
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount.Trim();
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zResult = _Message.Substring(0, 2);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "FNC_Receipt_Detail_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zResult = _Message.Substring(0, 2);
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        public string DeleteAll()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE Receipt_Detail_Info SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ReceiptKey = @ReceiptKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ReceiptKey", SqlDbType.NVarChar).Value = _ReceiptKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        #endregion
    }
}
