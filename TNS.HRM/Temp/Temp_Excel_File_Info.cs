﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.HRM
{
    public class Temp_Excel_File_Info
    {

        #region [ Field Name ]

        private int _AutoKey = 0;
        private int _ExcelKey = 0;
        private byte[] _FileAttach;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public int ExcelKey
        {
            get
            {
                return _ExcelKey;
            }

            set
            {
                _ExcelKey = value;
            }
        }

        public byte[] FileAttach
        {
            get
            {
                return _FileAttach;
            }

            set
            {
                _FileAttach = value;
            }
        }

        public string RoleID
        {
            get
            {
                return _RoleID;
            }

            set
            {
                _RoleID = value;
            }
        }

        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }

        #endregion
        #region [ Constructor Get Information ]
        public Temp_Excel_File_Info()
        {

        }
        public Temp_Excel_File_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM Temp_Excel_File WHERE ExcelKey = @ExcelKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (AutoKey > 0)
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                else
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = DBNull.Value;

                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    if (zReader["FileAttach"] != DBNull.Value)
                       _FileAttach = (byte[])(zReader["FileAttach"]);

                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @" INSERT INTO Temp_Excel_File
          (
ExcelKey, FileAttach
) VALUES (
 @ExcelKey,@FileAttach
)";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
               
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int ).Value = ExcelKey;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.VarBinary).Value = _FileAttach;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE dbo.Temp_Excel_File SET 
			ExcelKey = @ExcelKey,
			FileAttach = @FileAttach
          WHERE  AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                zCommand.Parameters.Add("@FileAttach", SqlDbType.VarBinary).Value = _FileAttach;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
      //  public string Delete()
      //  {
      //      string zResult = "";
      //      //---------- String SQL Access Database ---------------
      //      string zSQL = @"UPDATE dbo.Temp_Excel_File SET
      //[RecordStatus] = 99,
      //[ModifiedOn] = GETDATE(),
      //[ModifiedBy] = @ModifiedBy,
      //[ModifiedName] = @ModifiedName
      //WHERE[ExcelKey] = @ExcelKey";
      //      string zConnectionString = ConnectDataBase.ConnectionString;
      //      SqlConnection zConnect = new SqlConnection(zConnectionString);
      //      zConnect.Open();
      //      try
      //      {
      //          SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
      //          zCommand.CommandType = CommandType.Text;
      //          zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
      //          zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
      //          zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
      //          zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
      //          zResult = zCommand.ExecuteNonQuery().ToString();
      //          zCommand.Dispose();
      //      }
      //      catch (Exception Err)
      //      {
      //          _Message = Err.ToString();
      //      }
      //      finally
      //      {
      //          zConnect.Close();
      //      }
      //      return zResult;
      //  }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
