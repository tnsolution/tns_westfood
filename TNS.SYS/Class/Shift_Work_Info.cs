﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SYS
{
    public class Shift_Work_Info
    {

        #region [ Field Name ]
        private int _ShiftKey = 0;
        private string _ShiftID = "";
        private string _ShiftName = "";
        private TimeSpan _BeginTime;
        private TimeSpan _EndTime;
        private int _Active = 0;
        private int _Rank = 0;
        private int _Slug = 0;
        private int _RecordStatus = 0;
        private string _Description = "";
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _ShiftKey; }
            set { _ShiftKey = value; }
        }
        public string ShiftID
        {
            get { return _ShiftID; }
            set { _ShiftID = value; }
        }
        public string ShiftName
        {
            get { return _ShiftName; }
            set { _ShiftName = value; }
        }
        public TimeSpan BeginTime
        {
            get { return _BeginTime; }
            set { _BeginTime = value; }
        }
        public TimeSpan EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }
        public int Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Shift_Work_Info()
        {
        }
        public Shift_Work_Info(int ShiftKey)
        {
            string zSQL = "SELECT * FROM HRM_Shift_Work WHERE ShiftKey = @ShiftKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ShiftKey", SqlDbType.Int).Value = ShiftKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["ShiftKey"] != DBNull.Value)
                        _ShiftKey = int.Parse(zReader["ShiftKey"].ToString());
                    _ShiftID = zReader["ShiftID"].ToString().Trim();
                    _ShiftName = zReader["ShiftName"].ToString().Trim();
                    if (zReader["BeginTime"] != DBNull.Value)
                        _BeginTime = (TimeSpan)zReader["BeginTime"];
                    if (zReader["EndTime"] != DBNull.Value)
                        _EndTime = (TimeSpan)zReader["EndTime"];
                    if (zReader["Active"] != DBNull.Value)
                        _Active = int.Parse(zReader["Active"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Shift_Work ("
        + " ShiftID ,ShiftName ,BeginTime ,EndTime ,Active ,Rank ,Slug  ,Description ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@ShiftID ,@ShiftName ,@BeginTime ,@EndTime ,@Active ,@Rank ,@Slug ,@Description ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT ShiftKey FROM HRM_Shift_Work WHERE ShiftKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ShiftID", SqlDbType.NVarChar).Value = _ShiftID.Trim();
                zCommand.Parameters.Add("@ShiftName", SqlDbType.NVarChar).Value = _ShiftName.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.Time).Value = _BeginTime;
                zCommand.Parameters.Add("@EndTime", SqlDbType.Time).Value = _EndTime;
                zCommand.Parameters.Add("@Active", SqlDbType.Int).Value = _Active;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
               
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Shift_Work SET "
                        + " ShiftID = @ShiftID,"
                        + " ShiftName = @ShiftName,"
                        + " BeginTime = @BeginTime,"
                        + " EndTime = @EndTime,"
                        + " Active = @Active,"
                        + " Rank = @Rank,"
                        + " Slug = @Slug,"
                        + " Description = @Description,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE ShiftKey = @ShiftKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ShiftKey", SqlDbType.Int).Value = _ShiftKey;
                zCommand.Parameters.Add("@ShiftID", SqlDbType.NVarChar).Value = _ShiftID.Trim();
                zCommand.Parameters.Add("@ShiftName", SqlDbType.NVarChar).Value = _ShiftName.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.Time).Value = _BeginTime;
                zCommand.Parameters.Add("@EndTime", SqlDbType.Time).Value = _EndTime;
                zCommand.Parameters.Add("@Active", SqlDbType.Int).Value = _Active;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Shift_Work SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ShiftKey = @ShiftKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ShiftKey", SqlDbType.Int).Value = _ShiftKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message =  Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ShiftKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
