﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CRM;
using TNS.LOC;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Customer_Company : Form
    {
        const int Slug = 2; //khach cty
        private int _Ward = 0;
        private int _Province = 0;
        private int _Country = 0;
        private int _District = 0;
       
        private string _Category = "";
      
        private string _CustomerKey = "";
        private string _CompanyKey = "";
        private List<Address_Info> _List_Address;
        private List<Contact_Person_Object> _List_Contact;
        private Customer_Company_Object _CusCom_Obj;
        private Company_Object _Company_Obj;
        private Company_Info _Company;
        public Frm_Customer_Company()
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Address, true);
            Utils.DrawGVStyle(ref GV_Address);
            Utils.DoubleBuffered(GV_Contact, true);
            Utils.DrawGVStyle(ref GV_Contact);
            Utils.DrawLVStyle(ref LV_Customer);
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            GV_Address_Layout();
            GV_Contact_Layout();
            btn_Save.Click += Btn_Save_Click;

            GV_Address.KeyDown += GV_Address_KeyDown;
            GV_Address.CellEndEdit += GV_Address_CellEndEdit;
            GV_Address.EditingControlShowing += GV_Address_EditingControlShowing;

            LV_Customer.Click += LV_Customer_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;

            GV_Contact.KeyDown += GV_Contact_KeyDown;
            GV_Contact.CellEndEdit += GV_Contact_CellEndEdit;

           // GV_Contact.ColumnHeadersHeight = 65;
            Utils.SizeLastColumn_LV(LV_Customer);
        }

        private void Frm_Customer_Company_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            LV_Layout(LV_Customer);
            //LV_LoadData();
            Load_Data();
            //if (txt_Slug.Text == "Doanh Nghiệp")
            //{
            //    Slug = 2;
            //}
            LoadDataToToolbox.ComboBoxData(cbo_Category.ComboBox, Customer_Category_Data.List(), false, 0, 1);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region ------  Left  ------
        #region ----- Design Layout----
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Khách Hàng";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Khách Hàng";
            colHead.Width = 400;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);



        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Customer;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Customer_Data.Search(txt_Search.Text.Trim(), Slug);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["CustomerKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //if (nRow["Slug"].ToString().Trim() == "2")
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Doanh Nghiệp";
                //    lvi.SubItems.Add(lvsi);
                //}
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CompanyKey"].ToString().Trim();
                lvi.SubItems.Add(lvsi);
                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region ----- Process Event -----
        private void LV_Customer_Click(object sender, EventArgs e)
        {
            _CustomerKey = LV_Customer.SelectedItems[0].Tag.ToString();
            _CompanyKey = LV_Customer.SelectedItems[0].SubItems[3].Text;
            Load_Data();
        }
        #endregion

        #region ----- Process Data -----
        private void Load_Data()
        {

            txt_CompanyName.BackColor = Color.FromArgb(255, 255, 225);
            txt_CustomerID.BackColor = Color.FromArgb(255, 255, 225);
            txt_Phone.BackColor = Color.FromArgb(255, 255, 225);
            txt_Taxcode.BackColor = Color.FromArgb(255, 255, 225);

            _CusCom_Obj = new Customer_Company_Object(_CustomerKey);
            //txt_Slug.Text = "Doanh Nghiệp";
            cbo_Category.SelectedValue = _CusCom_Obj.CategoryKey;
            txt_CustomerID.Text = _CusCom_Obj.CustomerID;
            txt_CompanyName.Text = _CusCom_Obj.CompanyObj.CompanyName;
            txt_Phone.Text = _CusCom_Obj.CompanyObj.CompanyPhone;
            txt_Taxcode.Text = _CusCom_Obj.CompanyObj.TaxCode;
            txt_Bankcode.Text = _CusCom_Obj.CompanyObj.BankCode;
            txt_AddressBank.Text = _CusCom_Obj.CompanyObj.AddressBank;
            txt_Email.Text = _CusCom_Obj.CompanyObj.Email;
            txt_Fax.Text = _CusCom_Obj.CompanyObj.Fax;
            txt_Description.Text = _CusCom_Obj.CompanyObj.Decription;
            GV_Contact_LoadData();
            GV_Address_LoadData();
            txt_CompanyName.Focus();
        }
        private void GV_Contact_LoadData()
        {
            GV_Contact.Rows.Clear();
            for (int i = 1; i <= 15; i++)
            {
                GV_Contact.Rows.Add();
            }
            for (int i = 0; i < _CusCom_Obj.CompanyObj.List_Contact.Count; i++)
            {
                GV_Contact.Rows.Add();
                Contact_Person_Object zContact = (Contact_Person_Object)_CusCom_Obj.CompanyObj.List_Contact[i];
                GV_Contact.Rows[i].Tag = zContact;
                GV_Contact.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Contact.Rows[i].Cells["No"].Tag = zContact.Key;
                GV_Contact.Rows[i].Cells["ContactName"].Value = zContact.FullName;
                GV_Contact.Rows[i].Cells["Position"].Value = zContact.Position;
                GV_Contact.Rows[i].Cells["ContactPhone"].Value = zContact.Phone;
                GV_Contact.Rows[i].Cells["ContactEmail"].Value = zContact.Email;
                GV_Contact.Rows[i].Cells["Description"].Value = zContact.Description;
            }
        }
        private void GV_Address_LoadData()
        {
            GV_Address.Rows.Clear();
            for (int i = 1; i <= 15; i++)
            {
                GV_Address.Rows.Add();
            }
            for (int i = 0; i < _CusCom_Obj.CompanyObj.List_Address.Count; i++)
            {
                Address_Info zAddress = (Address_Info)_CusCom_Obj.CompanyObj.List_Address[i];
                GV_Address.Rows[i].Tag = zAddress;
                GV_Address.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Address.Rows[i].Cells["Country"].Value = zAddress.CountryName;
                GV_Address.Rows[i].Cells["Country"].Tag = zAddress.CountryKey;
                GV_Address.Rows[i].Cells["Province"].Value = zAddress.ProvinceName;
                GV_Address.Rows[i].Cells["Province"].Tag = zAddress.ProvinceKey;
                GV_Address.Rows[i].Cells["District"].Value = zAddress.DistrictName;
                GV_Address.Rows[i].Cells["District"].Tag = zAddress.DistrictKey;
                GV_Address.Rows[i].Cells["Ward"].Value = zAddress.WardName;
                GV_Address.Rows[i].Cells["Ward"].Tag = zAddress.WardKey;
                GV_Address.Rows[i].Cells["Address"].Value = zAddress.Address;
                if (zAddress.CategoryKey == 1)
                    GV_Address.Rows[i].Cells["Category"].Value = "Thường trú";
                else if (zAddress.CategoryKey == 2)
                    GV_Address.Rows[i].Cells["Category"].Value = "Tạm trú";
                else
                    GV_Address.Rows[i].Cells["Category"].Value = "";
            }
        }
        #endregion
        #endregion

        #region ------  Right   ------
        #region ------Gidview Address-------
        private void GV_Address_Layout()
        {

            // Setup Column 
            GV_Address.Columns.Add("No", "STT");
            GV_Address.Columns.Add("Country", "Quốc Gia");
            GV_Address.Columns.Add("Province", "Tỉnh/Thành");
            GV_Address.Columns.Add("District", "Quận/Huyện");
            GV_Address.Columns.Add("Ward", "Phường/Xã");
            GV_Address.Columns.Add("Address", "Địa chỉ");

            DataGridViewComboBoxColumn combox = new DataGridViewComboBoxColumn();
            //combox.DataSource = ztbCategory;
            combox.HeaderText = "Loại";
            combox.Name = "Category";
            combox.Items.Add("Thường trú");
            combox.Items.Add("Tạm trú");

            GV_Address.Columns.Add(combox);
            GV_Address.Columns["No"].Width = 40;
            GV_Address.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Address.Columns["No"].ReadOnly = true;

            GV_Address.Columns["Country"].Width = 130;
            GV_Address.Columns["Country"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Address.Columns["Province"].Width = 130;
            GV_Address.Columns["Province"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["District"].Width = 130;
            GV_Address.Columns["District"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["Ward"].Width = 130;
            GV_Address.Columns["Ward"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["Address"].Width = 320;
            GV_Address.Columns["Address"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.Columns["Category"].Width = 130;
            GV_Address.Columns["Category"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Address.BackgroundColor = Color.White;
            GV_Address.GridColor = Color.FromArgb(227, 239, 255);
            GV_Address.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Address.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Address.DefaultCellStyle.Font = new Font("Arial", 9);
            //GV_Address.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 90, 200);
            GV_Address.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Address.EnableHeadersVisualStyles = false;

            GV_Address.AllowUserToResizeRows = false;
            GV_Address.AllowUserToResizeColumns = true;

            GV_Address.RowHeadersVisible = false;
            GV_Address.AllowUserToAddRows = true;
            GV_Address.AllowUserToDeleteRows = false;





            ////foreach (DataGridViewColumn column in GV_Address.Columns)
            ////{
            ////    column.SortMode = DataGridViewColumnSortMode.NotSortable;
            ////}

            //for (int i = 1; i <= 8; i++)
            //{
            //    GV_Address.Rows.Add();
            //}

        }
        private void GV_Address_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GV_Address.Rows[e.RowIndex];
            Address_Info zAddress;
            if (GV_Address.Rows[e.RowIndex].Tag == null)
            {
                zAddress = new Address_Info();
                GV_Address.Rows[e.RowIndex].Tag = zAddress;
            }
            else
            {
                zAddress = (Address_Info)GV_Address.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            string zName = "";
            switch (e.ColumnIndex)
            {
                case 1:
                    if (zRowEdit.Cells[1].Value == null)
                    {
                        zRowEdit.Cells[1].ErrorText = "Không tìm thấy quốc gia này!.";

                        zAddress.CountryKey = 0;
                        zAddress.CountryName = "";
                        zAddress.ProvinceKey = 0;
                        zAddress.ProvinceName = "";
                        zAddress.DistrictKey = 0;
                        zAddress.DistrictName = "";
                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[2].Value = "";
                        zRowEdit.Cells[3].Value = "";
                        zRowEdit.Cells[4].Value = "";
                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[1].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        Country_Info zCountry = new Country_Info();
                        zCountry.Get_Country_Info(zName);
                        //Không tìm được Quốc gia
                        if (zCountry.Key == 0)
                        {
                            zRowEdit.Cells[1].ErrorText = "Không tìm thấy quốc gia này!.";
                            zAddress.CountryKey = 0;
                            zAddress.CountryName = "";
                            zAddress.ProvinceKey = 0;
                            zAddress.ProvinceName = "";
                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[2].Value = "";
                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được quốc gia mới và Giá trị đưa vào # giá trị cũ
                        if (zCountry.Key != 0 && zCountry.Key != zAddress.CountryKey)
                        {
                            zAddress.CountryKey = zCountry.Key;
                            zAddress.CountryName = zCountry.CountryName;

                            zAddress.ProvinceKey = 0;
                            zAddress.ProvinceName = "";
                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[2].Value = "";
                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.CountryKey = zCountry.Key;
                            zAddress.CountryName = zCountry.CountryName;

                            zIsChangeValued = false;
                        }

                    }
                    break;
                case 2:
                    if (zRowEdit.Cells[2].Value == null)
                    {
                        zRowEdit.Cells[2].ErrorText = "Không tìm thấy tỉnh/thành phố này!.";

                        zAddress.ProvinceKey = 0;
                        zAddress.ProvinceName = "";
                        zAddress.DistrictKey = 0;
                        zAddress.DistrictName = "";
                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[3].Value = "";
                        zRowEdit.Cells[4].Value = "";
                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[2].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        Province_Info zProvince = new Province_Info();
                        zProvince.Get_Province_Info(zName);
                        // Không Tìm được tỉnh thành mới
                        if (zProvince.Key == 0)
                        {
                            zRowEdit.Cells[2].ErrorText = "Không tìm thấy tỉnh/thành phố này!.";

                            zAddress.ProvinceKey = 0;
                            zAddress.ProvinceName = "";
                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được tỉnh thành mới và Giá trị đưa vào # giá trị cũ
                        if (zProvince.Key != 0 && zProvince.Key != zAddress.ProvinceKey)
                        {
                            zAddress.ProvinceKey = zProvince.Key;
                            zAddress.ProvinceName = zProvince.ProvinceName;

                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[3].Value = "";
                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.ProvinceKey = zProvince.Key;
                            zAddress.ProvinceName = zProvince.ProvinceName;

                            zIsChangeValued = false;
                        }
                    }
                    break;
                case 3:
                    if (zRowEdit.Cells[3].Value == null)
                    {
                        zRowEdit.Cells[3].ErrorText = "Không tìm thấy quận/huyện này!.";

                        zAddress.DistrictKey = 0;
                        zAddress.DistrictName = "";
                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[4].Value = "";
                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[3].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        District_Info zDistrict = new District_Info();
                        zDistrict.Get_District_Info(zName, zAddress.ProvinceKey);

                        //Không Tìm được quận huyện mới
                        if (zDistrict.Key == 0)
                        {
                            zRowEdit.Cells[3].ErrorText = "Không tìm thấy quận/huyện này!";

                            zAddress.DistrictKey = 0;
                            zAddress.DistrictName = "";
                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được Quận /huyện mới và Giá trị đưa vào # giá trị cũ
                        if (zDistrict.Key != 0 && zDistrict.Key != zAddress.DistrictKey)
                        {

                            zAddress.DistrictName = zDistrict.DistrictName;
                            zAddress.DistrictKey = zDistrict.Key;

                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[4].Value = "";
                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.DistrictName = zDistrict.DistrictName;
                            zAddress.DistrictKey = zDistrict.Key;

                            zIsChangeValued = false;
                        }
                    }
                    break;
                case 4:
                    if (zRowEdit.Cells[4].Value == null)
                    {
                        zRowEdit.Cells[4].ErrorText = "Không tìm thấy phường xã này!.";

                        zAddress.WardKey = 0;
                        zAddress.WardName = "";
                        zAddress.Address = "";

                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[4].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        Ward_Info zWard = new Ward_Info();
                        zWard.Get_Ward_Info(zName, zAddress.DistrictKey);
                        //Không Tìm được phường xã mới
                        if (zWard.Key == 0)
                        {
                            zRowEdit.Cells[4].ErrorText = "Không tìm thấy phường xã này!.";

                            zAddress.WardKey = 0;
                            zAddress.WardName = "";
                            zAddress.Address = "";

                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        //Tìm được phường xã mới và Giá trị đưa vào # giá trị cũ
                        if (zWard.Key != 0 && zWard.Key != zAddress.WardKey)
                        {

                            zAddress.WardName = zWard.WardName;
                            zAddress.WardKey = zWard.Key;

                            zAddress.Address = "";

                            zRowEdit.Cells[5].Value = "";

                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.WardName = zWard.WardName;
                            zAddress.WardKey = zWard.Key;

                            zIsChangeValued = false;
                        }
                    }

                    break;
                case 5:
                    if (zRowEdit.Cells[5].Value == null)
                    {
                        zRowEdit.Cells[5].ErrorText = "Địa chỉ không được bỏ trống!.";

                        zAddress.Address = "";

                        zRowEdit.Cells[5].Value = "";

                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[5].ErrorText = "";
                        zName = GV_Address.CurrentCell.Value.ToString();
                        if (zName == "")
                        {
                            zRowEdit.Cells[5].ErrorText = "Địa chỉ không được bỏ trống!.";
                            zIsChangeValued = true;
                        }
                        else if (zName != zAddress.Address)
                        {
                            zAddress.Address = zName.Trim();
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zAddress.Address = zName.Trim();
                            zIsChangeValued = false;
                        }
                    }
                    if (zAddress.Key == 0)
                    {
                        zRowEdit.Cells[6].Value = "Thường trú";
                        zAddress.CategoryKey = 1;
                    }

                    break;
                case 6:
                    zRowEdit.Cells[5].ErrorText = "";
                    if (GV_Address.CurrentCell.Value == null)
                    {
                        zAddress.CategoryKey = 0;
                        zRowEdit.Cells[6].ErrorText = "Chọn hình thức cư trú!.";
                        zIsChangeValued = true;
                    }
                    else
                    {

                        zName = GV_Address.CurrentCell.Value.ToString();
                        if (zName == "Thường trú")
                        {
                            zAddress.CategoryKey = 1;
                        }
                        else if (zName == "Tạm trú")
                        {
                            zAddress.CategoryKey = 2;
                        }
                        else
                        {
                            zRowEdit.Cells[6].ErrorText = "Chọn hình thức cư trú!.";
                            zAddress.CategoryKey = 0;
                        }
                        zIsChangeValued = true;
                    }
                    break;

            }
            if (zIsChangeValued)
            {
                zAddress.RecordStatus = 1;
                // ShowProductInGridView_Address(e.RowIndex);
                if (zAddress.Key != 0)
                    zAddress.RecordStatus = 2;
                else
                    zAddress.RecordStatus = 1;
            }

            resetRow = true;
            currentRow = e.RowIndex;
            GV_Address.SelectionChanged += GV_Address_SelectionChanged;

        }
        private void GV_Address_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            Address_Info zAddress = new Address_Info();
            if (GV_Address.CurrentRow.Tag == null)
            {
                zAddress = new Address_Info();
                GV_Address.CurrentRow.Tag = zAddress;
            }
            else
            {
                zAddress = (Address_Info)GV_Address.CurrentRow.Tag;
            }
            _Country = zAddress.CountryKey;
            _Province = zAddress.ProvinceKey;
            _District = zAddress.DistrictKey;
            _Ward = zAddress.DistrictKey;

            switch (GV_Address.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    DataGridViewTextBoxEditingControl te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT CountryName FROM LOC_Country WHERE CountryKey = 237 AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 2: // autocomplete for product
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT ProvinceName FROM LOC_Province WHERE CountryKey = '" + _Country + "' AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 3:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT DistrictName FROM LOC_District WHERE ProvinceKey = '" + _Province + "' AND RecordStatus <99");

                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 4:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    LoadDataToToolbox.AutoCompleteTextBox(te, "SELECT WardName FROM LOC_Ward WHERE DistrictKey = '" + _District + "' AND RecordStatus <99");
                    e.Control.KeyPress += new KeyPressEventHandler(Control_KeyPress);
                    break;
                case 5:
                    te = (DataGridViewTextBoxEditingControl)e.Control;
                    te.AutoCompleteMode = AutoCompleteMode.None;
                    break;
            }
        }
        private void GV_Address_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    if (GV_Address.CurrentRow.Tag != null)
                    {
                        Address_Info zAddress = (Address_Info)GV_Address.CurrentRow.Tag;
                        GV_Address.CurrentRow.Visible = false;
                        zAddress.RecordStatus = 99;
                    }
                }
            }
        }
        private void Caculate(int RowIndex)
        {

        }
        private void ShowProductInGridView_Address(int RowIndex)
        {


        }
        #endregion

        private void GV_Contact_Layout()
        {
            // Setup Column 
            GV_Contact.Columns.Add("No", "STT");
            GV_Contact.Columns.Add("ContactName", "Tên Người Liên Hệ");
            GV_Contact.Columns.Add("Position", "Chức Vụ");
            GV_Contact.Columns.Add("ContactPhone", "Số Điện Thoại");
            GV_Contact.Columns.Add("ContactEmail", "Email");
            GV_Contact.Columns.Add("Description", "Ghi Chú");

            GV_Contact.Columns["No"].Width = 40;
            GV_Contact.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Contact.Columns["No"].ReadOnly = true;

            GV_Contact.Columns["ContactName"].Width = 250;
            GV_Contact.Columns["ContactName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Contact.Columns["Position"].Width = 150;
            GV_Contact.Columns["Position"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Contact.Columns["ContactPhone"].Width = 150;
            GV_Contact.Columns["ContactPhone"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Contact.Columns["ContactEmail"].Width = 250;
            GV_Contact.Columns["ContactEmail"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Contact.Columns["Description"].Width = 150;
            GV_Contact.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Contact.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Contact.BackgroundColor = Color.White;
            GV_Contact.GridColor = Color.FromArgb(227, 239, 255);
            GV_Contact.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Contact.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Contact.DefaultCellStyle.Font = new Font("Arial", 9);
            //GV_Contact.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 90, 200);
            GV_Contact.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Contact.EnableHeadersVisualStyles = false;

            GV_Contact.AllowUserToResizeRows = false;
            GV_Contact.AllowUserToResizeColumns = true;

            GV_Contact.RowHeadersVisible = false;
            GV_Contact.AllowUserToAddRows = true;
            GV_Contact.AllowUserToDeleteRows = false;

            //// setup Height Header
            // GV_Products.ColumnHeadersHeight = GV_Products.ColumnHeadersHeight * 3 / 2;
            GV_Contact.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Contact.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            foreach (DataGridViewColumn column in GV_Contact.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            for (int i = 1; i <= 8; i++)
            {
                GV_Contact.Rows.Add();
            }

        }
        private int currentRow;
        private bool resetRow = false;
        private void GV_Address_SelectionChanged(object sender, EventArgs e)
        {
            if (resetRow)
            {
                resetRow = false;
                GV_Address.CurrentCell = GV_Address.Rows[currentRow].Cells[0];
            }
        }
        private void GV_Contact_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {

            DataGridViewRow zRowEdit = GV_Contact.Rows[e.RowIndex];
            Contact_Person_Object zContact;
            if (GV_Contact.Rows[e.RowIndex].Tag == null)
            {
                zContact = new Contact_Person_Object();
                GV_Contact.Rows[e.RowIndex].Tag = zContact;
            }
            else
            {
                zContact = (Contact_Person_Object)GV_Contact.Rows[e.RowIndex].Tag;
            }
            bool zIsChangeValued = false;
            string zName = "";
            switch (e.ColumnIndex)
            {
                case 1:
                    if (GV_Contact.CurrentCell.Value == null)
                    {
                        zRowEdit.Cells[1].ErrorText = "Người liên hệ không được bỏ trống!.";
                        zContact.FullName = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[1].ErrorText = "";
                        zName = GV_Contact.CurrentCell.Value.ToString();
                        if (zName == "")
                        {
                            zRowEdit.Cells[1].ErrorText = "Người liên hệ không được bỏ trống!.";
                            zIsChangeValued = true;
                        }
                        else if (zName != zContact.FullName)
                        {
                            zContact.FullName = zName.Trim();
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zContact.FullName = zName.Trim();
                            zIsChangeValued = false;
                        }
                    }
                    break;
                case 2:
                    if (GV_Contact.CurrentCell.Value == null)
                    {
                        zRowEdit.Cells[2].ErrorText = "Chức vụ không được bỏ trống!.";
                        zContact.Position = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[2].ErrorText = "";
                        zName = GV_Contact.CurrentCell.Value.ToString();
                        if (zName == "")
                        {
                            zRowEdit.Cells[1].ErrorText = "Chức vụ không được bỏ trống!.";
                            zIsChangeValued = true;
                        }
                        else if (zName != zContact.Position)
                        {
                            zContact.Position = zName.Trim();
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zContact.Position = zName.Trim();
                            zIsChangeValued = false;
                        }

                    }
                    break;
                case 3:
                    if (GV_Contact.CurrentCell.Value == null)
                    {
                        zRowEdit.Cells[3].ErrorText = "Số điện thoại không được bỏ trống!.";
                        zContact.CompanyPhone = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        zRowEdit.Cells[3].ErrorText = "";
                        zName = GV_Contact.CurrentCell.Value.ToString();
                        if (zName == "")
                        {
                            zRowEdit.Cells[3].ErrorText = "Số điện thoại không được bỏ trống!.";
                            zIsChangeValued = true;
                        }
                        else if (zName != zContact.CompanyPhone)
                        {
                            zContact.CompanyPhone = zName.Trim();
                            zIsChangeValued = true;
                        }
                        else
                        {
                            zContact.CompanyPhone = zName.Trim();
                            zIsChangeValued = false;
                        }

                    }
                    break;
                case 4:
                    if (GV_Contact.CurrentCell.Value == null)
                    {
                        zContact.CompanyEmail = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zContact.CompanyEmail.ToString())
                        {
                            zContact.CompanyEmail = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            zIsChangeValued = true;
                        }
                    }
                    break;
                case 5:
                    if (GV_Contact.CurrentCell.Value == null)
                    {
                        zContact.Description = "";
                        zIsChangeValued = true;
                    }
                    else
                    {
                        if (zRowEdit.Cells[e.ColumnIndex].Value.ToString() != zContact.Description.ToString())
                        {
                            zContact.Description = zRowEdit.Cells[e.ColumnIndex].Value.ToString();
                            zIsChangeValued = true;
                        }
                    }
                    break;

            }
            if (zIsChangeValued)
            {
                zContact.RecordStatus = 1;
                //Caculate(e.RowIndex);
                //ShowProductInGridView_Contact(e.RowIndex);
                if (zContact.Key != "")
                    zContact.RecordStatus = 2;
                else
                    zContact.RecordStatus = 1;
            }



        }
        private void Control_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
        }
        private void ShowProductInGridView_Contact(int RowIndex)
        {

        }
        private void GV_Contact_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    if (GV_Contact.CurrentRow.Tag != null)
                    {
                        Contact_Person_Object zContact = (Contact_Person_Object)GV_Contact.CurrentRow.Tag;
                        GV_Contact.CurrentRow.Visible = false;
                        zContact.RecordStatus = 99;
                    }
                }
            }
        }
        #endregion

        #region ------SaveObject------
        private void SaveObject()
        {
            _CusCom_Obj = new Customer_Company_Object(_CustomerKey);
            _CusCom_Obj.Slug = 2;// 2 là doanh nghiệp
            _CusCom_Obj.CategoryKey = cbo_Category.SelectedValue.ToInt();
            _CusCom_Obj.CustomerID = txt_CustomerID.Text;

            _CusCom_Obj.CompanyObj.CompanyName = txt_CompanyName.Text;
            _CusCom_Obj.CompanyObj.CompanyPhone = txt_Phone.Text;
            _CusCom_Obj.CompanyObj.TaxCode = txt_Taxcode.Text;
            _CusCom_Obj.CompanyObj.BankCode = txt_Bankcode.Text;
            _CusCom_Obj.CompanyObj.AddressBank = txt_AddressBank.Text;
            _CusCom_Obj.CompanyObj.Email = txt_Email.Text;
            _CusCom_Obj.CompanyObj.Fax = txt_Fax.Text;
            _CusCom_Obj.CompanyObj.Decription = txt_Description.Text;

            _CusCom_Obj.CreatedBy = SessionUser.UserLogin.Key;
            _CusCom_Obj.CreatedName = SessionUser.Personal_Info.FullName;
            _CusCom_Obj.ModifiedBy = SessionUser.UserLogin.Key;
            _CusCom_Obj.ModifiedName = SessionUser.Personal_Info.FullName;
            //------ Contact -----
            _List_Contact = new List<Contact_Person_Object>();
            Contact_Person_Object zContact;
            for (int i = 0; i < GV_Contact.Rows.Count - 1; i++)
            {
                if (GV_Contact.Rows[i].Tag != null)
                {
                    zContact = (Contact_Person_Object)GV_Contact.Rows[i].Tag;
                    if (_CusCom_Obj.Key == "" && zContact.RecordStatus != 99)
                    {
                        zContact.RecordStatus = 1;
                    }
                    _List_Contact.Add(zContact);
                    _CusCom_Obj.CompanyObj.List_Contact = _List_Contact;

                }
            }

            //------ Address-----
            _List_Address = new List<Address_Info>();
            Address_Info zAddress;
            for (int i = 0; i < GV_Address.Rows.Count - 1; i++)
            {
                if (GV_Address.Rows[i].Tag != null)
                {
                    zAddress = (Address_Info)GV_Address.Rows[i].Tag;
                    if (_CusCom_Obj.Key == "" && zAddress.RecordStatus != 99)
                    {
                        zAddress.RecordStatus = 1;
                    }
                    _List_Address.Add(zAddress);
                    _CusCom_Obj.CompanyObj.List_Address = _List_Address;

                }
            }

            //------ Save ------
            _CusCom_Obj.Save_Object();
            _CustomerKey = _CusCom_Obj.Key;
            string zMessage = TN_Message.Show(_CusCom_Obj.Message);
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LV_LoadData();
                Load_Data();
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region -------Process Envent------
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (CheckBeforeSave().Trim().Length == 0)
            {
                SaveObject();
            }
            else
            {
                MessageBox.Show("Vui lòng điền đầy đủ thông tin bắt buộc !. \n" + zAmountErr, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        string zAmountErr = "";

        private string CheckBeforeSave()
        {
            zAmountErr = "";
            int zAddress = 0;
            int zcontact = 0;
            if (txt_CompanyName.Text.Length == 0)
            {
                txt_CompanyName.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Tên công ty.\n";
            }
            else
            {
                txt_CompanyName.BackColor = Color.FromArgb(255, 255, 225);
            }
            if (txt_CustomerID.Text.Trim().Length == 0)
            {
                txt_CustomerID.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Mã khách hàng.\n";
            }
            else
            {
                txt_CustomerID.BackColor = Color.FromArgb(255, 255, 225);
            }
            if (txt_Phone.Text.Trim().Length == 0)
            {
                txt_Phone.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Mã số điện thoại liên hệ.\n";
            }
            else
            {
                txt_Taxcode.BackColor = Color.FromArgb(255, 255, 225);
            }
            if (txt_Taxcode.Text.Trim().Length == 0)
            {
                txt_Taxcode.BackColor = Color.FromArgb(230, 100, 100);
                zAmountErr += "-Mã số thuế.\n";
            }
            else
            {
                txt_Taxcode.BackColor = Color.FromArgb(255, 255, 225);
            }
            for (int i = 0; i < GV_Address.Rows.Count - 1; i++)
            {
                if (GV_Address.Rows[i].Tag != null)
                {
                    if ((GV_Address.Rows[i].Cells["Country"].Value == null || GV_Address.Rows[i].Cells["Country"].Value == "" || GV_Address.Rows[i].Cells["Country"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Country"].ErrorText = "Không tìm thấy quốc gia này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["Province"].Value == null || GV_Address.Rows[i].Cells["Province"].Value == "" || GV_Address.Rows[i].Cells["Province"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Province"].ErrorText = "Không tìm thấy tỉnh/thành phố này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["District"].Value == null || GV_Address.Rows[i].Cells["District"].Value == "" || GV_Address.Rows[i].Cells["District"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["District"].ErrorText = "Không tìm thấy quận/huyện này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["Ward"].Value == null || GV_Address.Rows[i].Cells["Ward"].Value == "" || GV_Address.Rows[i].Cells["Ward"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Ward"].ErrorText = "Không tìm thấy phường/xã này!.";
                        zAddress++;
                    }
                    if ((GV_Address.Rows[i].Cells["Address"].Value == null || GV_Address.Rows[i].Cells["Address"].Value == "" || GV_Address.Rows[i].Cells["Address"].ErrorText != "") && GV_Address.Rows[i].Visible == true)
                    {
                        GV_Address.Rows[i].Cells["Address"].ErrorText = "Địa chỉ không được bỏ trống!.";
                        zAddress++;
                    }
                }

            }

            for (int i = 0; i < GV_Contact.Rows.Count - 1; i++)
            {
                if (GV_Contact.Rows[i].Tag != null)
                {
                    if ((GV_Contact.Rows[i].Cells["ContactName"].Value == null || GV_Contact.Rows[i].Cells["ContactName"].Value == "" || GV_Contact.Rows[i].Cells["ContactName"].ErrorText != "") && GV_Contact.Rows[i].Visible == true)
                    {
                        GV_Contact.Rows[i].Cells["ContactName"].ErrorText = "Người liên hệ không được bỏ trống!.";
                        zcontact++;
                    }
                    if ((GV_Contact.Rows[i].Cells["Position"].Value == null || GV_Contact.Rows[i].Cells["Position"].Value == "" || GV_Contact.Rows[i].Cells["Position"].ErrorText != "") && GV_Contact.Rows[i].Visible == true)
                    {
                        GV_Contact.Rows[i].Cells["Position"].ErrorText = "Chức vụ không được bỏ trống!.";
                        zcontact++;
                    }
                    if ((GV_Contact.Rows[i].Cells["ContactPhone"].Value == null || GV_Contact.Rows[i].Cells["ContactPhone"].Value == "" || GV_Contact.Rows[i].Cells["ContactPhone"].ErrorText != "") && GV_Contact.Rows[i].Visible == true)
                    {
                        GV_Contact.Rows[i].Cells["ContactPhone"].ErrorText = "Số điện thoại không được bỏ trống!.";
                        zcontact++;
                    }
                }
            }
            if (zAddress != 0)
            {
                zAmountErr += " -Thông tin địa chỉ.\n";
            }
            if (zcontact != 0)
            {
                zAmountErr += "-Thông tin người liên hệ.\n";
            }
            return zAmountErr;
        }

        private void Btn_New_Click(object sender, EventArgs e)
        {
            _CustomerKey = "";
            _CompanyKey = "";
            Load_Data();
            //if (txt_Slug.Text == "Doanh Nghiệp")
            //{
            //    Slug = 2;
            //}
            LoadDataToToolbox.ComboBoxData(cbo_Category.ComboBox, Customer_Category_Data.List(), false, 0, 1);

        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dlr == DialogResult.Yes)
            {
                _CusCom_Obj = new Customer_Company_Object();
                _CusCom_Obj.Key = _CustomerKey;
                _CusCom_Obj.CreatedBy = SessionUser.UserLogin.Key;
                _CusCom_Obj.CreatedName = SessionUser.Personal_Info.FullName;
                _CusCom_Obj.ModifiedBy = SessionUser.UserLogin.Key;
                _CusCom_Obj.ModifiedName = SessionUser.Personal_Info.FullName;
                _CusCom_Obj.Delete_Object();

                string zMessage = TN_Message.Show(_CusCom_Obj.Message);
                if (zMessage.Length == 0)
                {
                    MessageBox.Show("Xóa thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    _CustomerKey = "";
                    LV_LoadData();
                    Load_Data();
                }
                else
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LV_LoadData();
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
        //#endregion
    }
}
