﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.SYS
{
    public class Personal_Info
    {
        #region [ Field Name ]
        private string _PersonKey = "";
        private string _PersonID = "";
        private string _FirstName = "";
        private string _LastName = "";
        private string _FullName = "";
        private string _ParentKey = "";
        private string _ParentTable = "";
        private string _CardID = "";
        private int _Gender = 0;
        private int _Marriage = 0;
        private DateTime _BirthDay;
        private string _HomeTown = "";
        private string _IssueID = "";
        private DateTime _IssueDate;
        private string _IssuePlace = "";
        private string _Description = "";
        private int _RecordStatus = 0;
        private int _Slug = 0;
        private string _Phone = "";
        private string _Email = "";
        private string _TaxCode = "";
        private string _BankCode = "";
        private string _AddressBank = "";
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion

        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_PersonKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _PersonKey; }
            set { _PersonKey = value; }
        }
        public string PersonID
        {
            get { return _PersonID; }
            set { _PersonID = value; }
        }
        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }
        public string Name
        {
            get { return _LastName + " " + _FirstName; }
            set
            {
                string[] zName = value.Trim().Split(' ');
                if (zName.Length == 1)
                {
                    _FirstName = zName[0];
                    _LastName = "";
                }
                else
                {
                    _FirstName = zName[zName.Length - 1];
                    _LastName = "";
                    for (int i = 0; i < zName.Length - 1; i++)
                    {
                        _LastName += " " + zName[i];
                    }
                    _LastName = _LastName.Trim();
                }
            }
        }
        public string ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string ParentTable
        {
            get { return _ParentTable; }
            set { _ParentTable = value; }
        }
        public string CardID
        {
            get { return _CardID; }
            set { _CardID = value; }
        }
        public int Gender
        {
            get { return _Gender; }
            set { _Gender = value; }
        }
        public int Marriage
        {
            get { return _Marriage; }
            set { _Marriage = value; }
        }
        public DateTime BirthDay
        {
            get { return _BirthDay; }
            set { _BirthDay = value; }
        }
        public string HomeTown
        {
            get { return _HomeTown; }
            set { _HomeTown = value; }
        }
        public string IssueID
        {
            get { return _IssueID; }
            set { _IssueID = value; }
        }
        public DateTime IssueDate
        {
            get { return _IssueDate; }
            set { _IssueDate = value; }
        }
        public string IssuePlace
        {
            get { return _IssuePlace; }
            set { _IssuePlace = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public string Phone
        {
            get { return _Phone; }
            set { _Phone = value; }
        }
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        public string TaxCode
        {
            get { return _TaxCode; }
            set { _TaxCode = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string BankCode
        {
            get
            {
                return _BankCode;
            }

            set
            {
                _BankCode = value;
            }
        }

        public string AddressBank
        {
            get
            {
                return _AddressBank;
            }

            set
            {
                _AddressBank = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Personal_Info()
        {
        }
        public Personal_Info(string ParentKey)
        {
            string zSQL = "SELECT * FROM SYS_Personal WHERE ParentKey = @ParentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PersonKey = zReader["PersonalKey"].ToString();
                    _PersonID = zReader["PersonID"].ToString().Trim();
                    _FirstName = zReader["FirstName"].ToString().Trim();
                    _LastName = zReader["LastName"].ToString().Trim();
                    _FullName = zReader["FullName"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _CardID = zReader["CardID"].ToString().Trim();
                    if (zReader["Gender"] != DBNull.Value)
                        _Gender = int.Parse(zReader["Gender"].ToString());
                    if (zReader["Marriage"] != DBNull.Value)
                        _Marriage = int.Parse(zReader["Marriage"].ToString());
                    if (zReader["BirthDay"] != DBNull.Value)
                        _BirthDay = (DateTime)zReader["BirthDay"];
                    _HomeTown = zReader["HomeTown"].ToString().Trim();
                    _IssueID = zReader["IssueID"].ToString().Trim();
                    if (zReader["IssueDate"] != DBNull.Value)
                        _IssueDate = (DateTime)zReader["IssueDate"];
                    _IssuePlace = zReader["IssuePlace"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Phone = zReader["Phone"].ToString().Trim();
                    _Email = zReader["Email"].ToString().Trim();
                    _TaxCode = zReader["TaxCode"].ToString().Trim();
                    _BankCode = zReader["BankCode"].ToString().Trim();
                    _AddressBank = zReader["AddressBank"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        /// <summary>
        /// Lấy tên , truyền ParentKey
        /// </summary>
        /// <param name="ParentKey"></param>
        /// <param name="Parent"></param>
        public Personal_Info(string ParentKey, bool Parent)
        {
            string zSQL = "SELECT * FROM SYS_Personal WHERE ParentKey = @ParentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ParentKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PersonKey = zReader["PersonKey"].ToString();
                    _PersonID = zReader["PersonID"].ToString().Trim();
                    _FirstName = zReader["FirstName"].ToString().Trim();
                    _LastName = zReader["LastName"].ToString().Trim();
                    _FullName = zReader["FullName"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _CardID = zReader["CardID"].ToString().Trim();
                    if (zReader["Gender"] != DBNull.Value)
                        _Gender = int.Parse(zReader["Gender"].ToString());
                    if (zReader["Marriage"] != DBNull.Value)
                        _Marriage = int.Parse(zReader["Marriage"].ToString());
                    if (zReader["BirthDay"] != DBNull.Value)
                        _BirthDay = (DateTime)zReader["BirthDay"];
                    _HomeTown = zReader["HomeTown"].ToString().Trim();
                    _IssueID = zReader["IssueID"].ToString().Trim();
                    if (zReader["IssueDate"] != DBNull.Value)
                        _IssueDate = (DateTime)zReader["IssueDate"];
                    _IssuePlace = zReader["IssuePlace"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _BankCode = zReader["BankCode"].ToString().Trim();
                    _AddressBank = zReader["AddressBank"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Phone = zReader["Phone"].ToString().Trim();
                    _Email = zReader["Email"].ToString().Trim();
                    _TaxCode = zReader["TaxCode"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        /// <summary>
        /// Lấy tên , truyền CardID
        /// </summary>
        /// <param name="CardID"></param>
        /// <param name="Card"></param>
        public Personal_Info(string CardID, int Card)
        {
            string zSQL = "SELECT * FROM SYS_Personal WHERE CardID = @CardID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = CardID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PersonKey = zReader["PersonKey"].ToString();
                    _PersonID = zReader["PersonID"].ToString().Trim();
                    _FirstName = zReader["FirstName"].ToString().Trim();
                    _LastName = zReader["LastName"].ToString().Trim();
                    _FullName = zReader["FullName"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _CardID = zReader["CardID"].ToString().Trim();
                    if (zReader["Gender"] != DBNull.Value)
                        _Gender = int.Parse(zReader["Gender"].ToString());
                    if (zReader["Marriage"] != DBNull.Value)
                        _Marriage = int.Parse(zReader["Marriage"].ToString());
                    if (zReader["BirthDay"] != DBNull.Value)
                        _BirthDay = (DateTime)zReader["BirthDay"];
                    _HomeTown = zReader["HomeTown"].ToString().Trim();
                    _IssueID = zReader["IssueID"].ToString().Trim();
                    if (zReader["IssueDate"] != DBNull.Value)
                        _IssueDate = (DateTime)zReader["IssueDate"];
                    _IssuePlace = zReader["IssuePlace"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Phone = zReader["Phone"].ToString().Trim();
                    _Email = zReader["Email"].ToString().Trim();
                    _TaxCode = zReader["TaxCode"].ToString().Trim();
                    _BankCode = zReader["BankCode"].ToString().Trim();
                    _AddressBank = zReader["AddressBank"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Personal_Info(string EmployeeName,bool Employee,int Count)
        {
            string zSQL = "SELECT * FROM SYS_Personal WHERE FullName = @FullName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = EmployeeName;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _PersonKey = zReader["PersonalKey"].ToString();
                    _PersonID = zReader["PersonID"].ToString().Trim();
                    _FirstName = zReader["FirstName"].ToString().Trim();
                    _LastName = zReader["LastName"].ToString().Trim();
                    _FullName = zReader["FullName"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _CardID = zReader["CardID"].ToString().Trim();
                    if (zReader["Gender"] != DBNull.Value)
                        _Gender = int.Parse(zReader["Gender"].ToString());
                    if (zReader["Marriage"] != DBNull.Value)
                        _Marriage = int.Parse(zReader["Marriage"].ToString());
                    if (zReader["BirthDay"] != DBNull.Value)
                        _BirthDay = (DateTime)zReader["BirthDay"];
                    _HomeTown = zReader["HomeTown"].ToString().Trim();
                    _IssueID = zReader["IssueID"].ToString().Trim();
                    if (zReader["IssueDate"] != DBNull.Value)
                        _IssueDate = (DateTime)zReader["IssueDate"];
                    _IssuePlace = zReader["IssuePlace"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _Phone = zReader["Phone"].ToString().Trim();
                    _Email = zReader["Email"].ToString().Trim();
                    _TaxCode = zReader["TaxCode"].ToString().Trim();
                    _BankCode = zReader["BankCode"].ToString().Trim();
                    _AddressBank = zReader["AddressBank"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }


        public Personal_Info(DataRow nRow)
        {
            _PersonKey = nRow["PersonalKey"].ToString();
            _PersonID = nRow["PersonID"].ToString().Trim();
            _FirstName = nRow["FirstName"].ToString().Trim();
            _LastName = nRow["LastName"].ToString().Trim();
            _FullName = nRow["FullName"].ToString().Trim();
            _ParentKey = nRow["ParentKey"].ToString().Trim();
            _ParentTable = nRow["ParentTable"].ToString().Trim();
            _CardID = nRow["CardID"].ToString().Trim();
            if (nRow["Gender"] != DBNull.Value)
                _Gender = int.Parse(nRow["Gender"].ToString());
            if (nRow["Marriage"] != DBNull.Value)
                _Marriage = int.Parse(nRow["Marriage"].ToString());
            if (nRow["BirthDay"] != DBNull.Value)
                _BirthDay = (DateTime)nRow["BirthDay"];
            _HomeTown = nRow["HomeTown"].ToString().Trim();
            _IssueID = nRow["IssueID"].ToString().Trim();
            if (nRow["IssueDate"] != DBNull.Value)
                _IssueDate = (DateTime)nRow["IssueDate"];
            _IssuePlace = nRow["IssuePlace"].ToString().Trim();
            _Description = nRow["Description"].ToString().Trim();
            if (nRow["RecordStatus"] != DBNull.Value)
                _RecordStatus = int.Parse(nRow["RecordStatus"].ToString());
            if (nRow["Slug"] != DBNull.Value)
                _Slug = int.Parse(nRow["Slug"].ToString());
            _Phone = nRow["Phone"].ToString().Trim();
            _Email = nRow["Email"].ToString().Trim();
            _TaxCode = nRow["TaxCode"].ToString().Trim();
            _BankCode = nRow["BankCode"].ToString().Trim();
            _AddressBank = nRow["AddressBank"].ToString().Trim();
            if (nRow["CreatedOn"] != DBNull.Value)
                _CreatedOn = (DateTime)nRow["CreatedOn"];
            _CreatedBy = nRow["CreatedBy"].ToString().Trim();
            _CreatedName = nRow["CreatedName"].ToString().Trim();
            if (nRow["ModifiedOn"] != DBNull.Value)
                _ModifiedOn = (DateTime)nRow["ModifiedOn"];
            _ModifiedBy = nRow["ModifiedBy"].ToString().Trim();
            _ModifiedName = nRow["ModifiedName"].ToString().Trim();
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Personal_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@PersonID", SqlDbType.NVarChar).Value = _PersonID.Trim();
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName.Trim();
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName.Trim();
                zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = _FullName.Trim();
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = _CardID.Trim();
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                zCommand.Parameters.Add("@Marriage", SqlDbType.Int).Value = _Marriage;
                if (_BirthDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = _BirthDay;
                zCommand.Parameters.Add("@HomeTown", SqlDbType.NVarChar).Value = _HomeTown.Trim();
                zCommand.Parameters.Add("@IssueID", SqlDbType.NVarChar).Value = _IssueID.Trim();
                if (_IssueDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace.Trim();
                zCommand.Parameters.Add("@BankCode", SqlDbType.NVarChar).Value = _BankCode.Trim();
                zCommand.Parameters.Add("@AddressBank", SqlDbType.NVarChar).Value = _AddressBank.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone.Trim();
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email.Trim();
                zCommand.Parameters.Add("@TaxCode", SqlDbType.NVarChar).Value = _TaxCode.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _PersonKey = _Message.Substring(2, 36);
                else
                    _PersonKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "SYS_Personal_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_PersonKey.Length == 36)
                    zCommand.Parameters.Add("@PersonalKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_PersonKey);
                else
                    zCommand.Parameters.Add("@PersonalKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@PersonID", SqlDbType.NVarChar).Value = _PersonID.Trim();
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@FirstName", SqlDbType.NVarChar).Value = _FirstName.Trim();
                zCommand.Parameters.Add("@LastName", SqlDbType.NVarChar).Value = _LastName.Trim();
                zCommand.Parameters.Add("@FullName", SqlDbType.NVarChar).Value = _FullName.Trim();
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = _CardID.Trim();
                zCommand.Parameters.Add("@Gender", SqlDbType.Int).Value = _Gender;
                zCommand.Parameters.Add("@Marriage", SqlDbType.Int).Value = _Marriage;
                if (_BirthDay == DateTime.MinValue)
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BirthDay", SqlDbType.DateTime).Value = _BirthDay;
                zCommand.Parameters.Add("@HomeTown", SqlDbType.NVarChar).Value = _HomeTown.Trim();
                zCommand.Parameters.Add("@IssueID", SqlDbType.NVarChar).Value = _IssueID.Trim();
                if (_IssueDate == DateTime.MinValue)
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@IssueDate", SqlDbType.DateTime).Value = _IssueDate;
                zCommand.Parameters.Add("@IssuePlace", SqlDbType.NVarChar).Value = _IssuePlace.Trim();
                zCommand.Parameters.Add("@BankCode", SqlDbType.NVarChar).Value = _BankCode.Trim();
                zCommand.Parameters.Add("@AddressBank", SqlDbType.NVarChar).Value = _AddressBank.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Phone", SqlDbType.NVarChar).Value = _Phone.Trim();
                zCommand.Parameters.Add("@Email", SqlDbType.NVarChar).Value = _Email.Trim();
                zCommand.Parameters.Add("@TaxCode", SqlDbType.NVarChar).Value = _TaxCode.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Personal_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_PersonKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
