﻿namespace TNS.WinApp
{
    partial class Frm_Production_Record
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Production_Record));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Panel_LV = new System.Windows.Forms.Panel();
            this.LVData = new System.Windows.Forms.ListView();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_Up = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_Down = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_Open_Order = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Del = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Import = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Copy = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.GV_Employee = new System.Windows.Forms.DataGridView();
            this.GV_Sum = new System.Windows.Forms.DataGridView();
            this.Panel_Search = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cbo_Compare = new System.Windows.Forms.ComboBox();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.label2 = new System.Windows.Forms.Label();
            this.cbo_Category = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbo_Team = new System.Windows.Forms.ComboBox();
            this.txt_Stage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.txt_Product = new System.Windows.Forms.TextBox();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.LV_Stage = new System.Windows.Forms.ListView();
            this.LV_Product = new System.Windows.Forms.ListView();
            this.Panel_GV = new System.Windows.Forms.Panel();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_LV.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Sum)).BeginInit();
            this.Panel_Search.SuspendLayout();
            this.Panel_GV.SuspendLayout();
            this.SuspendLayout();
            // 
            // Panel_LV
            // 
            this.Panel_LV.Controls.Add(this.LVData);
            this.Panel_LV.Controls.Add(this.kryptonHeader3);
            this.Panel_LV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_LV.Location = new System.Drawing.Point(0, 155);
            this.Panel_LV.Name = "Panel_LV";
            this.Panel_LV.Size = new System.Drawing.Size(1266, 320);
            this.Panel_LV.TabIndex = 177;
            // 
            // LVData
            // 
            this.LVData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVData.FullRowSelect = true;
            this.LVData.GridLines = true;
            this.LVData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LVData.HideSelection = false;
            this.LVData.Location = new System.Drawing.Point(0, 30);
            this.LVData.Name = "LVData";
            this.LVData.Size = new System.Drawing.Size(1266, 290);
            this.LVData.TabIndex = 0;
            this.LVData.UseCompatibleStateImageBehavior = false;
            this.LVData.View = System.Windows.Forms.View.Details;
            this.LVData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LVData_KeyDown);
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_Up,
            this.btn_Down});
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(1266, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 201;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "THÔNG TIN ĐƠN HÀNG";
            // 
            // btn_Up
            // 
            this.btn_Up.Image = ((System.Drawing.Image)(resources.GetObject("btn_Up.Image")));
            this.btn_Up.UniqueName = "ABE0F8FEF4FF4DE3448D0BAE7F1C6F22";
            // 
            // btn_Down
            // 
            this.btn_Down.Image = ((System.Drawing.Image)(resources.GetObject("btn_Down.Image")));
            this.btn_Down.UniqueName = "D6E0AB28C2CE49FBDD866175ABE4E274";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox4.Controls.Add(this.btn_Open_Order);
            this.groupBox4.Controls.Add(this.btn_Del);
            this.groupBox4.Controls.Add(this.btn_Import);
            this.groupBox4.Controls.Add(this.btn_Copy);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox4.Location = new System.Drawing.Point(0, 708);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1266, 60);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            // 
            // btn_Open_Order
            // 
            this.btn_Open_Order.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Open_Order.Location = new System.Drawing.Point(1014, 13);
            this.btn_Open_Order.Name = "btn_Open_Order";
            this.btn_Open_Order.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Open_Order.Size = new System.Drawing.Size(120, 40);
            this.btn_Open_Order.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Open_Order.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Open_Order.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Open_Order.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Open_Order.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Open_Order.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Open_Order.TabIndex = 10;
            this.btn_Open_Order.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Open_Order.Values.Image")));
            this.btn_Open_Order.Values.Text = "Tạo đơn";
            // 
            // btn_Del
            // 
            this.btn_Del.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Del.Location = new System.Drawing.Point(1140, 12);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Del.Size = new System.Drawing.Size(120, 40);
            this.btn_Del.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.TabIndex = 9;
            this.btn_Del.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Del.Values.Image")));
            this.btn_Del.Values.Text = "Xóa";
            // 
            // btn_Import
            // 
            this.btn_Import.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Import.Location = new System.Drawing.Point(138, 13);
            this.btn_Import.Name = "btn_Import";
            this.btn_Import.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Import.Size = new System.Drawing.Size(120, 40);
            this.btn_Import.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Import.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import.TabIndex = 8;
            this.btn_Import.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Import.Values.Image")));
            this.btn_Import.Values.Text = "Nhập Excel";
            // 
            // btn_Copy
            // 
            this.btn_Copy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Copy.Location = new System.Drawing.Point(12, 12);
            this.btn_Copy.Name = "btn_Copy";
            this.btn_Copy.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Copy.Size = new System.Drawing.Size(120, 40);
            this.btn_Copy.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Copy.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Copy.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Copy.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Copy.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Copy.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Copy.TabIndex = 7;
            this.btn_Copy.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Copy.Values.Image")));
            this.btn_Copy.Values.Text = "Sao chép";
            // 
            // GV_Employee
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.GV_Employee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.GV_Employee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Employee.Location = new System.Drawing.Point(0, 30);
            this.GV_Employee.Name = "GV_Employee";
            this.GV_Employee.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Employee.Size = new System.Drawing.Size(1266, 153);
            this.GV_Employee.TabIndex = 0;
            // 
            // GV_Sum
            // 
            this.GV_Sum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Sum.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GV_Sum.Location = new System.Drawing.Point(0, 183);
            this.GV_Sum.Name = "GV_Sum";
            this.GV_Sum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.GV_Sum.Size = new System.Drawing.Size(1266, 50);
            this.GV_Sum.TabIndex = 1;
            // 
            // Panel_Search
            // 
            this.Panel_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Search.Controls.Add(this.label1);
            this.Panel_Search.Controls.Add(this.cbo_Compare);
            this.Panel_Search.Controls.Add(this.btn_Search);
            this.Panel_Search.Controls.Add(this.label2);
            this.Panel_Search.Controls.Add(this.cbo_Category);
            this.Panel_Search.Controls.Add(this.label7);
            this.Panel_Search.Controls.Add(this.label6);
            this.Panel_Search.Controls.Add(this.cbo_Team);
            this.Panel_Search.Controls.Add(this.txt_Stage);
            this.Panel_Search.Controls.Add(this.label3);
            this.Panel_Search.Controls.Add(this.label5);
            this.Panel_Search.Controls.Add(this.dte_FromDate);
            this.Panel_Search.Controls.Add(this.txt_Product);
            this.Panel_Search.Controls.Add(this.dte_ToDate);
            this.Panel_Search.Controls.Add(this.label4);
            this.Panel_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Search.Location = new System.Drawing.Point(0, 42);
            this.Panel_Search.Name = "Panel_Search";
            this.Panel_Search.Size = new System.Drawing.Size(1266, 113);
            this.Panel_Search.TabIndex = 200;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(908, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 15);
            this.label1.TabIndex = 181;
            this.label1.Text = "Mã đối chiếu";
            this.label1.Visible = false;
            // 
            // cbo_Compare
            // 
            this.cbo_Compare.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cbo_Compare.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Compare.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Compare.FormattingEnabled = true;
            this.cbo_Compare.Location = new System.Drawing.Point(911, 30);
            this.cbo_Compare.Name = "cbo_Compare";
            this.cbo_Compare.Size = new System.Drawing.Size(230, 23);
            this.cbo_Compare.TabIndex = 180;
            this.cbo_Compare.Visible = false;
            // 
            // btn_Search
            // 
            this.btn_Search.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Search.Location = new System.Drawing.Point(1142, 61);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(120, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 179;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(2, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 15);
            this.label2.TabIndex = 108;
            this.label2.Text = "Từ Ngày";
            // 
            // cbo_Category
            // 
            this.cbo_Category.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cbo_Category.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Category.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Category.FormattingEnabled = true;
            this.cbo_Category.Location = new System.Drawing.Point(110, 73);
            this.cbo_Category.Name = "cbo_Category";
            this.cbo_Category.Size = new System.Drawing.Size(280, 23);
            this.cbo_Category.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(107, 12);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 15);
            this.label7.TabIndex = 106;
            this.label7.Text = "Nhóm Thực Hiện";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(107, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 15);
            this.label6.TabIndex = 178;
            this.label6.Text = "Hệ Số";
            // 
            // cbo_Team
            // 
            this.cbo_Team.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cbo_Team.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Team.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbo_Team.FormattingEnabled = true;
            this.cbo_Team.Location = new System.Drawing.Point(110, 30);
            this.cbo_Team.Name = "cbo_Team";
            this.cbo_Team.Size = new System.Drawing.Size(280, 23);
            this.cbo_Team.TabIndex = 2;
            // 
            // txt_Stage
            // 
            this.txt_Stage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Stage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Stage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Stage.Location = new System.Drawing.Point(396, 75);
            this.txt_Stage.Name = "txt_Stage";
            this.txt_Stage.Size = new System.Drawing.Size(512, 21);
            this.txt_Stage.TabIndex = 177;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(2, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 127;
            this.label3.Text = "Đến Ngày";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(393, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 15);
            this.label5.TabIndex = 176;
            this.label5.Text = "Tên Công Đoạn";
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(5, 31);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(100, 20);
            this.dte_FromDate.TabIndex = 0;
            this.dte_FromDate.Value = new System.DateTime(((long)(0)));
            // 
            // txt_Product
            // 
            this.txt_Product.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Product.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Product.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Product.Location = new System.Drawing.Point(396, 30);
            this.txt_Product.Name = "txt_Product";
            this.txt_Product.Size = new System.Drawing.Size(512, 21);
            this.txt_Product.TabIndex = 4;
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(5, 72);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(100, 20);
            this.dte_ToDate.TabIndex = 1;
            this.dte_ToDate.Value = new System.DateTime(((long)(0)));
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(393, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 15);
            this.label4.TabIndex = 131;
            this.label4.Text = "Sản Phẩm";
            // 
            // LV_Stage
            // 
            this.LV_Stage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LV_Stage.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Stage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Stage.FullRowSelect = true;
            this.LV_Stage.GridLines = true;
            this.LV_Stage.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.LV_Stage.HideSelection = false;
            this.LV_Stage.Location = new System.Drawing.Point(396, 137);
            this.LV_Stage.Name = "LV_Stage";
            this.LV_Stage.Size = new System.Drawing.Size(700, 250);
            this.LV_Stage.TabIndex = 180;
            this.LV_Stage.UseCompatibleStateImageBehavior = false;
            this.LV_Stage.View = System.Windows.Forms.View.Details;
            // 
            // LV_Product
            // 
            this.LV_Product.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LV_Product.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Product.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Product.FullRowSelect = true;
            this.LV_Product.GridLines = true;
            this.LV_Product.HideSelection = false;
            this.LV_Product.Location = new System.Drawing.Point(396, 92);
            this.LV_Product.Name = "LV_Product";
            this.LV_Product.Size = new System.Drawing.Size(512, 250);
            this.LV_Product.TabIndex = 175;
            this.LV_Product.UseCompatibleStateImageBehavior = false;
            this.LV_Product.View = System.Windows.Forms.View.Details;
            // 
            // Panel_GV
            // 
            this.Panel_GV.Controls.Add(this.GV_Employee);
            this.Panel_GV.Controls.Add(this.GV_Sum);
            this.Panel_GV.Controls.Add(this.kryptonHeader1);
            this.Panel_GV.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Panel_GV.Location = new System.Drawing.Point(0, 475);
            this.Panel_GV.Name = "Panel_GV";
            this.Panel_GV.Size = new System.Drawing.Size(1266, 233);
            this.Panel_GV.TabIndex = 201;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(1266, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.TabIndex = 185;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "THÔNG TIN NĂNG SUẤT";
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1266, 42);
            this.HeaderControl.TabIndex = 202;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Ghi năng xuất";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            this.HeaderControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.HeaderControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.HeaderControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Frm_Production_Record
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.ClientSize = new System.Drawing.Size(1266, 768);
            this.Controls.Add(this.LV_Stage);
            this.Controls.Add(this.LV_Product);
            this.Controls.Add(this.Panel_LV);
            this.Controls.Add(this.Panel_GV);
            this.Controls.Add(this.Panel_Search);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Production_Record";
            this.Text = "Ghi Năng Suất";
            this.Load += new System.EventHandler(this.Frm_Record_Productivity_Load);
            this.Panel_LV.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Sum)).EndInit();
            this.Panel_Search.ResumeLayout(false);
            this.Panel_Search.PerformLayout();
            this.Panel_GV.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Panel_LV;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView GV_Employee;
        private System.Windows.Forms.DataGridView GV_Sum;
        private System.Windows.Forms.Panel Panel_Search;
        private System.Windows.Forms.ListView LV_Product;
        private System.Windows.Forms.ListView LV_Stage;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_Stage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_Product;
        private System.Windows.Forms.Label label4;
        private TNS.SYS.TNDateTimePicker dte_ToDate;
        private TNS.SYS.TNDateTimePicker dte_FromDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbo_Team;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbo_Category;
        private System.Windows.Forms.Panel Panel_GV;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Copy;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Import;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Del;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Open_Order;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private System.Windows.Forms.ListView LVData;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Up;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Down;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbo_Compare;
    }
}