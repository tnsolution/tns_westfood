﻿namespace TNS.WinApp
{
    partial class Frm_Search_Customer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Search_Customer));
            this.Panel_Left_Search = new System.Windows.Forms.Panel();
            this.cbo_Slug_Customer = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.txt_Search = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.GV_Customer = new System.Windows.Forms.DataGridView();
            this.Header = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Left_Search.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Slug_Customer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Customer)).BeginInit();
            this.SuspendLayout();
            // 
            // Panel_Left_Search
            // 
            this.Panel_Left_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Left_Search.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Left_Search.Controls.Add(this.cbo_Slug_Customer);
            this.Panel_Left_Search.Controls.Add(this.txt_Search);
            this.Panel_Left_Search.Controls.Add(this.label1);
            this.Panel_Left_Search.Controls.Add(this.label5);
            this.Panel_Left_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Left_Search.Location = new System.Drawing.Point(0, 42);
            this.Panel_Left_Search.Name = "Panel_Left_Search";
            this.Panel_Left_Search.Size = new System.Drawing.Size(818, 46);
            this.Panel_Left_Search.TabIndex = 0;
            // 
            // cbo_Slug_Customer
            // 
            this.cbo_Slug_Customer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbo_Slug_Customer.DropDownWidth = 119;
            this.cbo_Slug_Customer.Location = new System.Drawing.Point(86, 10);
            this.cbo_Slug_Customer.Name = "cbo_Slug_Customer";
            this.cbo_Slug_Customer.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Slug_Customer.Size = new System.Drawing.Size(186, 24);
            this.cbo_Slug_Customer.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Slug_Customer.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Slug_Customer.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Slug_Customer.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Slug_Customer.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Slug_Customer.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Slug_Customer.StateCommon.Item.Border.Width = 1;
            this.cbo_Slug_Customer.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Slug_Customer.TabIndex = 187;
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(440, 10);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search.Size = new System.Drawing.Size(366, 26);
            this.txt_Search.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search.StateCommon.Border.Rounding = 4;
            this.txt_Search.StateCommon.Border.Width = 1;
            this.txt_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.TabIndex = 157;
            this.txt_Search.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txt_Search_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(341, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 15);
            this.label1.TabIndex = 111;
            this.label1.Text = "Tên khách hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(34, 15);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 15);
            this.label5.TabIndex = 111;
            this.label5.Text = "Loại KH";
            // 
            // GV_Customer
            // 
            this.GV_Customer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GV_Customer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Customer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Customer.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GV_Customer.Location = new System.Drawing.Point(0, 88);
            this.GV_Customer.Name = "GV_Customer";
            this.GV_Customer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Customer.Size = new System.Drawing.Size(818, 287);
            this.GV_Customer.TabIndex = 1;
            this.GV_Customer.DoubleClick += new System.EventHandler(this.GV_Customer_DoubleClick);
            this.GV_Customer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GV_Customer_KeyDown);
            // 
            // Header
            // 
            this.Header.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.Header.Size = new System.Drawing.Size(818, 42);
            this.Header.TabIndex = 195;
            this.Header.Values.Description = "";
            this.Header.Values.Heading = "Nhập kho";
            this.Header.Values.Image = ((System.Drawing.Image)(resources.GetObject("Header.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Visible = false;
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Frm_Search_Customer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 375);
            this.Controls.Add(this.GV_Customer);
            this.Controls.Add(this.Panel_Left_Search);
            this.Controls.Add(this.Header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "Frm_Search_Customer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tìm khách hàng";
            this.Load += new System.EventHandler(this.Frm_Search_Customer_Load);
            this.Panel_Left_Search.ResumeLayout(false);
            this.Panel_Left_Search.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Slug_Customer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Customer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Panel_Left_Search;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView GV_Customer;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader Header;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Slug_Customer;
    }
}