﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.LOG
{
    public class Record_Data
    {
        public static DataTable List(DateTime DateWrite, string UserKey)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT CreatedOn ,ActionName,ActionType FROM [dbo].[LOG_Record] 
WHERE CreatedOn BETWEEN @FromDate AND @ToDate
AND CreatedBy =@CreatedBy
ORDER BY CreatedOn DESC
";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = UserKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

    }
}
