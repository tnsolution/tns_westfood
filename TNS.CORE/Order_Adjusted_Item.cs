﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TNS.CORE
{
    public class Order_Adjusted_Item
    {
        public int AutoKey;
        public int TeamKey = 0;
        public string TeamID = "";
        public string TeamName = "";
        public string OrderKey = "";
        public string OrderIDCompare = "";
        public string OrderID = "";
        public string EmployeeKey = "";
        public string EmployeeName = "";
        public string EmployeeID = "";
        public DateTime OrderDate;
        public int StageKey = 0;
        public string StageID = "";
        public string StageName = "";
        public float Price = 0;
        public float Time = 0;
        public float TimeEat = 0;
        public float Basket = 0;
        public float Kg = 0;
        public float Money = 0;
        public int Borrow = 0;
        public int Private = 0;
        public int Share = 0;
        public float KgProduct = 0;
        public float TimeProduct = 0;
        public float BasketProduct = 0;
        public int CategoryKey = 0;
        public string CategoryName = "";
        public float MoneyPersonal = 0;
        public float MoneyShare = 0;
        public int RecordStatus = 0;
        public string CreatedBy = "";
        public string CreatedName = "";
        public DateTime CreatedOn;
        public string ModifiedBy = "";
        public string ModifiedName = "";
        public DateTime ModifiedOn;
        public string Message = "";
    }
}
