﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TN.SLR
{
    public class Salary13_Data
    {
        //Tính tạm ứng lương tháng 13 - nhóm mặc định
        public static DataTable TinhLuongThang13_TamUng_NhomMacDinh(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey,double PhanTram,DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
                SoThang++;
            }
            PivotDate += "[Tổng cộng],[Bình quân thu nhập],[Số tiền]";
            //if (PivotDate.Contains(","))
            //{
            //    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            //}
            
           

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();

            #region[SQL]
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-08-31 23:59:59'
--declare @SoThang int =12
--declare @Phantram float=0.35
--declare @DateOff datetime ='2020-08-10 23:59:59'

CREATE TABLE RPT
(	
	Months NVARCHAR(500),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng từ tháng 1--12
		-- công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
--//Tổng cộng
		-- công nhân
		INSERT INTO RPT
		SELECT N'Tổng cộng', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- hỗ trợ
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- văn phòng
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
--//Tổng bình quân thu nhập
		-- công nhân
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- Hỗ trợ
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- Văn phòng
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
--//Tiền ứng
		-- công nhân
		INSERT INTO RPT
		SELECT N'Số tiền', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang*@Phantram,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- hỗ trợ
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey

;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Months] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);
            #endregion

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@PhanTram", SqlDbType.Decimal).Value = PhanTram;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Tính tạm ứng lương tháng 13 - nhóm phân bổ
        public static DataTable TinhLuongThang13_TamUng_NhomPhanBo(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, double PhanTram, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
                SoThang++;
            }
            PivotDate += "[Tổng cộng],[Bình quân thu nhập],[Số tiền]";
            //if (PivotDate.Contains(","))
            //{
            //    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            //}



            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();

            #region[SQL]
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-08-31 23:59:59'
--declare @SoThang int =12
--declare @Phantram float=0.35
--declare @DateOff datetime ='2020-08-10 23:59:59'

CREATE TABLE RPT
(	
	Months NVARCHAR(50),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng từ tháng 1--12
		-- công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tổng cộng
		-- công nhân
		INSERT INTO RPT
		SELECT N'Tổng cộng', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- hỗ trợ
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- văn phòng
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tổng bình quân thu nhập
		-- công nhân
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- Hỗ trợ
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- Văn phòng
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tiền ứng
		-- công nhân
		INSERT INTO RPT
		SELECT N'Số tiền', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang*@Phantram,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		-- hỗ trợ
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)

;WITH N AS (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Months] IN ( @Pivot)
			) P  
	    )		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT";

            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);

            #endregion

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@PhanTram", SqlDbType.Decimal).Value = PhanTram;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        //Tính tất chi lương tháng 13- nhóm mặc định // fix 12 tháng
        public static DataTable TinhLuongThang13_ChiTat_NhomMacDinh(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
                SoThang++;
            }
            PivotDate += "[Tổng cộng],[Bình quân thu nhập],[Đã ứng],[Chi tất]";
            //if (PivotDate.Contains(","))
            //{
            //    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            //}



            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();

            #region[SQL]
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-08-31 23:59:59'
--declare @SoThang int =12
--declare @Phantram float=0.35
--declare @DateOff datetime ='2020-08-10 23:59:59'

CREATE TABLE RPT
(	
	Months NVARCHAR(50),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	TeamID NVARCHAR(50),
	TeamName NVARCHAR(MAX),
	Amount FLOAT
);
--Tổng từng tháng từ tháng 1--12
		--Công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
--//Tổng cộng
		--Công nhân
		INSERT INTO RPT
		SELECT N'Tổng cộng', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ 
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
--//Tổng bình quân thu nhập
		--Công nhân
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName

--Đã ứng các đợt trước
		INSERT INTO RPT
		SELECT N'Đã ứng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate) AS Amount 
		FROM [dbo].[SLR_Salary13_Detail]
		WHERE  [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
--Chi tất còn lại
		--Công nhân
		INSERT INTO RPT
		SELECT N'Chi tất', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0)-[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ 
		INSERT INTO RPT
		SELECT N'Chi tất',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0)-[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate)  AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Chi tất',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) -[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate)   AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
";
            zSQL += @" SELECT * FROM (

                SELECT * FROM RPT WHERE 1=1 @Parameter
                    ) 
                X PIVOT(
                     SUM(Amount)
                     FOR [Months] IN (" + PivotDate + ")) P ORDER BY TeamKey,LEN (EmployeeID),EmployeeID  ";

            zSQL += " DROP TABLE RPT";
            zSQL = zSQL.Replace("@Parameter", Fillter);

            #endregion

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //Tính tất chi lương tháng 13- nhóm phân bổ // fix 12 tháng
        public static DataTable TinhLuongThang13_ChiTat_NhomPhanBo(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
                SoThang++;
            }
            PivotDate += "[Tổng cộng],[Bình quân thu nhập],[Đã ứng],[Chi tất]";
            //if (PivotDate.Contains(","))
            //{
            //    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            //}



            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();

            #region[SQL]
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-08-31 23:59:59'
--declare @SoThang int =12
--declare @Phantram float=0.35
--declare @DateOff datetime ='2020-08-10 23:59:59'

CREATE TABLE RPT
(	
	Months NVARCHAR(50),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng từ tháng 1--12
		--Công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tổng cộng
		--Công nhân
		INSERT INTO RPT
		SELECT N'Tổng cộng', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ 
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tổng bình quân thu nhập
		--Công nhân
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)

--Đã ứng các đợt trước 
        INSERT INTO RPT
		SELECT N'Đã ứng',EmployeeKey,EmployeeID,EmployeeName,TeamKeyTam,SUM(AmountTam) FROM(
			SELECT EmployeeKey,EmployeeID,EmployeeName,
			CASE WHEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate) !=0 THEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
			ELSE TeamKey
			END AS  TeamKeyTam,
			CASE WHEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate) !=0 THEN [dbo].[Fn_LayTienUngLuongThang13]([dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),EmployeeKey,@FromDate,@ToDate)
			ELSE [dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate) 
			END AS AmountTam 
			FROM [dbo].[SLR_Salary13_Detail]
			WHERE  [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		)K
		GROUP BY TeamKeyTam,EmployeeKey,EmployeeID,EmployeeName
--Chi tất còn lại
		--Công nhân
		INSERT INTO RPT
		SELECT N'Chi tất', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0)-[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ 
		INSERT INTO RPT
		SELECT N'Chi tất',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0)-[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate)  AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Chi tất',EmployeeKey,EmployeeID,EmployeeName,
		CASE WHEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate) !=0 THEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
		ELSE TeamKey
        END AS  TeamKey,
        CASE WHEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate) !=0 THEN Round(SUM(Amount)/@SoThang,0) -[dbo].[Fn_LayTienUngLuongThang13]([dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),EmployeeKey,@FromDate,@ToDate)
        ELSE Round(SUM(Amount)/@SoThang,0) -[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate)
        END AS Amount 
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Months] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT";

            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);

            #endregion

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        //Save tạm ứng tháng 13
        public static string  LuuLuongThang13_TamUng(DateTime FromDate, DateTime ToDate,double PhanTram, int SoLan, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                SoThang++;
            }
            DataTable zTable = new DataTable();

            #region[SQL]
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-10-31 23:59:59'
--declare @SoThang int =0
--declare @Phantram float=0
--declare @SoLan int =0
--declare @DateOff datetime ='2020-08-10 23:59:59'

declare @StringTB nvarchar(500)= CONCAT(N'BÌNH QUÂN THU NHẬP TÍNH TẠM ỨNG LƯƠNG THÁNG 13 ĐỢT ',@SoLan,N' (Chốt tháng ',MONTH(@ToDate),'/',YEAR(@ToDate),')')
declare @StringTU nvarchar(500)= CONCAT(N'THÀNH TIỀN TẠM ỨNG LƯƠNG THÁNG 13 ĐỢT ',@SoLan,N' (Mức chi: ',REPLACE(Round(@Phantram*100,0), '.0', ''),N'% - Ngày chi: ',DAY(@DateOff),'/',MONTH(@DateOff),'/',YEAR(@DateOff),')')
declare @Parent nvarchar(50) = newid()
--Bảng cha
INSERT INTO [dbo].[SLR_Salary13] (AutoKey,FromDate,ToDate,Persent,Type,Description,Class,DateOff)
VALUES (@Parent,@FromDate,@ToDate,@Phantram,@SoLan,CONCAT(N'SỐ TIỀN TẠM ỨNG LƯƠNG THÁNG 13 ĐỢT ',@SoLan),N'TU',@DateOff)

--Lọc dữ liệu vào bảng tạm
CREATE TABLE RPT
(	
	ID NVARCHAR(50),
	Name NVARCHAR(550),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	TeamID NVARCHAR(50),
	TeamName NVARCHAR(MAX),
	Amount FLOAT,
	SoLan Int,
	Class CHAR(2)
);

--//Tổng bình quân thu nhập
		--Công nhân
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan), @StringTB, EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TB'
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan),@StringTB,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TB'
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan),@StringTB,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TB'
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
--//Tiền ứng
		--Công nhân 
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU, EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang*@Phantram,0) AS Amount,@SoLan,'TU'
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount,@SoLan,'TU'
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount,@SoLan,'TU'
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName

--Lưu từ bảng tạm vào bảng con
		INSERT INTO [dbo].[SLR_Salary13_Detail](AutoKey,Parent,ID,Name,DateWrite,
		EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,Amount,Type,Class)
		SELECT newid(),@Parent,ID,Name,@ToDate,
		EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,Amount,SoLan,Class
		FROM RPT

DROP Table RPT

";
            #endregion

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@PhanTram", SqlDbType.Decimal).Value = PhanTram;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@SoLan", SqlDbType.Int).Value = SoLan;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                string a = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = "08" + ex.ToString();
            }
            return zResult;
        }
        //Save chi tất tháng 13
        public static string LuuLuongThang13_TatChi(DateTime FromDate, DateTime ToDate, double PhanTram, int SoLan, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                SoThang++;
            }
            DataTable zTable = new DataTable();

            #region[SQL]
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @SoThang int =12
--declare @Phantram float=40
--declare @SoLan int =9
--declare @DateOff datetime ='2020-08-10 23:59:59'


declare @StringTU nvarchar(500)= CONCAT(N'CHI TẤT LƯƠNG THÁNG 13 NĂM ',YEAR(@FromDate),N' (Mức chi:',REPLACE(Round(@Phantram*100,0), '.0', ''),N'% - Ngày chi: ',DAY(@DateOff),'/',MONTH(@DateOff),'/',YEAR(@DateOff),')')
declare @StringTB nvarchar(500)= CONCAT(N'BÌNH QUÂN THU NHẬP NĂM ',YEAR(@FromDate))
declare @Parent nvarchar(50) = newid()

INSERT INTO [dbo].[SLR_Salary13] (AutoKey,FromDate,ToDate,Persent,Type,Description,Class,DateOff)
VALUES (@Parent,@FromDate,@ToDate,@Phantram,@SoLan,CONCAT(N'Chi tất năm NĂM ',YEAR(@FromDate)),N'CT',@DateOff)


CREATE TABLE RPT
(	
	ID NVARCHAR(500),
	Name NVARCHAR(500),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	TeamID NVARCHAR(50),
	TeamName NVARCHAR(MAX),
	Amount FLOAT,
	SoLan Int,
	Class CHAR(2)
);

--//Tổng bình quân thu nhập
		--Công nhân
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan),@StringTB , EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TT'
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan),@StringTB,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TT'
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan),@StringTB,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TT'
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
--//Tiền ứng
		--Công nhân
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU, EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) -[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate) AS Amount,@SoLan,'CT'
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Hỗ trợ
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) -[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate)  AS Amount,@SoLan,'CT'
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--Văn phòng
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0)-[dbo].[Fn_LayTienUngLuongThang13](TeamKey,EmployeeKey,@FromDate,@ToDate)   AS Amount,@SoLan,'CT'
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
--Lưu chi tất

		INSERT INTO [dbo].[SLR_Salary13_Detail](AutoKey,Parent,ID,Name,DateWrite,
		EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,Amount,Type,Class)
		SELECT newid(),@Parent,ID,Name,@ToDate,
		EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,Amount,SoLan,Class
		FROM RPT

DROP Table RPT
";
            #endregion

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@PhanTram", SqlDbType.Decimal).Value = PhanTram;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@SoLan", SqlDbType.Int).Value = SoLan;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                string a = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = "08" + ex.ToString();
            }
            return zResult;
        }





        // báo cáo lương tháng 13 - nhóm mặc định
        public static DataTable BaoCaoLuongThang13_NhomMacDinh(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
            }
            PivotDate += "[TỔNG CỘNG],";
            DataTable ztb = Salary13_Data.GetName13(zFromDate, zToDate);
            if(ztb.Rows.Count>0)
            {
                for (int i= 0;i< ztb.Rows.Count; i++)
                {
                    PivotDate += "[" + ztb.Rows[i]["Name"]+ "],";
                }
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @SoThang int =12

CREATE TABLE RPT
(	
	[Name] NVARCHAR(500),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng
		--Công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
--Tổng cộng fix 13
		--Công nhân
		INSERT INTO RPT
		SELECT N'TỔNG CỘNG', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
--Lưu tất chi tháng 13
		INSERT INTO RPT
		SELECT [Name],EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		SUM(Amount)
		FROM [dbo].[SLR_Salary13_Detail]
		WHERE DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY [Name], EmployeeKey,EmployeeID,EmployeeName,TeamKey
;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Name] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        // báo cáo lương tháng 13 - nhóm phân bổ chi phí
        public static DataTable BaoCaoLuongThang13_NhomPhanBo(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
            }
            PivotDate += "[TỔNG CỘNG],";
            DataTable ztb = Salary13_Data.GetName13(zFromDate, zToDate);
            if (ztb.Rows.Count > 0)
            {
                for (int i = 0; i < ztb.Rows.Count; i++)
                {
                    PivotDate += "[" + ztb.Rows[i]["Name"] + "],";
                }
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @SoThang int =12

CREATE TABLE RPT
(	
	[Name] NVARCHAR(500),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng
		--Công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--Tổng cộng fix 13
		--Công nhân
		INSERT INTO RPT
		SELECT N'TỔNG CỘNG', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Hỗ trợ
		INSERT INTO RPT
		SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--Lưu tất chi tháng 13
		INSERT INTO RPT
		SELECT [Name],EmployeeKey,EmployeeID,EmployeeName,
        CASE WHEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate) !=0 THEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
		ELSE TeamKey
        END TeamKey,
		SUM(Amount)
		FROM [dbo].[SLR_Salary13_Detail]
		WHERE DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		GROUP BY [Name], EmployeeKey,EmployeeID,EmployeeName,TeamKey

    ;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Name] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        // báo cáo lương tháng 13
        public static DataTable GetName13(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

SELECT Name,ID,Type FROM [dbo].[SLR_Salary13_Detail]
WHERE RecordStatus <>99
AND DateWrite BETWEEN @FromDate AND @ToDate
GROUP BY Name,ID,Type
ORDER BY Type,ID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
