﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TN.SLR
{
    public class Fee_Company_Data
    {        //
        public static DataTable TRICHTHEOLUONG_CTY(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey,int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND TeamKey =@TeamKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT,
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] date,
	[BHXH] FLOAT,
	[BHYT] FLOAT ,
	[BHTN] FLOAT ,
	[KPCD] FLOAT 
)
--1.Lây BHXH
INSERT INTO Temp
SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
SUM(Amount),0,0,0
FROM [dbo].[SLR_FeeCompany]WHERE RecordStatus <> 99 
AND DateWrite BETWEEN @FromDate AND @ToDate AND ID='BHXH'
GROUP BY DateWrite,
[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName

--2.Lây BHYT
INSERT INTO Temp
SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
0,SUM(Amount),0,0
FROM [dbo].[SLR_FeeCompany]WHERE RecordStatus <> 99 
AND DateWrite BETWEEN @FromDate AND @ToDate AND ID='BHYT'
GROUP BY DateWrite,
[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
--3.Lây BHTN
INSERT INTO Temp
SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
0,0,SUM(Amount),0
FROM [dbo].[SLR_FeeCompany]WHERE RecordStatus <> 99 
AND DateWrite BETWEEN @FromDate AND @ToDate AND ID='BHTN'
GROUP BY DateWrite,
[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
--3.Lây KPCD
INSERT INTO Temp
SELECT [dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
0,0,0,SUM(Amount)
FROM [dbo].[SLR_FeeCompany]WHERE RecordStatus <> 99 
AND DateWrite BETWEEN @FromDate AND @ToDate AND ID='KPCD'
GROUP BY DateWrite,
[dbo].[Fn_GetBranchKey_FromTeamKey](TeamKey),[dbo].[Fn_GetDepartmentKey_FromTeamKey](TeamKey),
TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName

--SELECT * FROM Temp
--Group 3 dòng riêng biệt thành 1
CREATE TABLE RPT
(
	[BranchKey] INT,
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[MONTH] INT,
	[YEAR] INT,
	[BHXH] FLOAT,
	[BHYT] FLOAT ,
	[BHTN] FLOAT ,
	[KPCD] FLOAT 
)

INSERT INTO RPT
SELECT BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,
EmployeeKey,EmployeeID,EmployeeName,MONTh(DateWrite),YEAR(DateWrite),
SUM(BHXH),SUM(BHYT),SUM(BHTN),SUM(KPCD)
FROM Temp WHERE 1=1 @Parameter
GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,
EmployeeKey,EmployeeID,EmployeeName,DateWrite

--SELECT * FROM RPT

SELECT 
P.TeamName + '|' + P.EmployeeName + '|' + P.EmployeeID+'|'+CAST(BranchRank AS NVARCHAR(10))+'|'+CAST(DepartmentRank AS NVARCHAR(10))+'|'+CAST(TeamRank AS NVARCHAR(10)) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.BHXH, P.BHYT, P.BHTN,P.KPCD
FROM
(
	SELECT A.BranchKey,A.TeamKey,A.TeamID,A.TeamName,
	A.EmployeeKey,A.EmployeeID,A.EmployeeName,CONCAT( N'THÁNG ',[Month],'/',YEAR) AS DateWrite ,
	A.BHXH, A.BHYT, A.BHTN,A.KPCD,
	D.RANK AS BranchRank,C.RANK AS DepartmentRank,B.RANK as TeamRank FROM RPT A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
) P
    ORDER BY DateWrite,BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE Temp
DROP TABLE RPT";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable TRICHTHEOLUONG_CTY_V2(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND BranchKey = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND DepartmentKey =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND TeamKey =@TeamKey ";

            string zSQL = @"
--declare @FromDate datetime ='2020-04-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @TeamKey int = 79

CREATE TABLE Temp
(
	[BranchKey] INT,
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[DateWrite] date,
	[BHXH] FLOAT,
	[BHYT] FLOAT ,
	[BHTN] FLOAT ,
	[KPCD] FLOAT 
)
--1.Lây BHXH
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        SUM(Amount),0,0,0
        FROM [dbo].[SLR_ReportOffice_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='XHCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        SUM(Amount),0,0,0
        FROM [dbo].[SLR_ReportSupport_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='XHCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        SUM(Amount),0,0,0
        FROM [dbo].[SLR_ReportWorker_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='XHCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName

--2.Lây BHYT
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,SUM(Amount),0,0
        FROM [dbo].[SLR_ReportOffice_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='YTCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,SUM(Amount),0,0
        FROM [dbo].[SLR_ReportSupport_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='YTCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,SUM(Amount),0,0
        FROM [dbo].[SLR_ReportWorker_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='YTCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
--3.Lây BHTN
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,0,SUM(Amount),0
        FROM [dbo].[SLR_ReportOffice_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='NVCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,0,SUM(Amount),0
        FROM [dbo].[SLR_ReportSupport_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='NVCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,0,SUM(Amount),0
        FROM [dbo].[SLR_ReportWorker_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='NVCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
--3.Lây KPCD
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,0,0,SUM(Amount)
        FROM [dbo].[SLR_ReportOffice_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='CDCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,0,0,SUM(Amount)
        FROM [dbo].[SLR_ReportSupport_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='CDCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName
        INSERT INTO Temp
        SELECT BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName,DateWrite,
        0,0,0,SUM(Amount)
        FROM [dbo].[SLR_ReportWorker_Close] WHERE RecordStatus <> 99 
        AND DateWrite BETWEEN @FromDate AND @ToDate AND CodeID ='CDCT'
        GROUP BY DateWrite,
        BranchKey,DepartmentKey,
        TeamKey,TeamName,TeamID,EmployeeKey,EmployeeID,EmployeeName

--SELECT * FROM Temp
--Group 3 dòng riêng biệt thành 1
CREATE TABLE RPT
(
	[BranchKey] INT,
	[DepartmentKey] INT ,
	[TeamKey] INT,
	[TeamName] nvarchar(500) ,
	[TeamID] nvarchar(500) ,
	EmployeeKey nvarchar(500),
	EmployeeID nvarchar(500),
	EmployeeName nvarchar(500),
	[MONTH] INT,
	[YEAR] INT,
	[BHXH] FLOAT,
	[BHYT] FLOAT ,
	[BHTN] FLOAT ,
	[KPCD] FLOAT 
)

INSERT INTO RPT
SELECT BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,
EmployeeKey,EmployeeID,EmployeeName,MONTh(DateWrite),YEAR(DateWrite),
SUM(BHXH),SUM(BHYT),SUM(BHTN),SUM(KPCD)
FROM Temp WHERE 1=1 @Parameter
GROUP BY BranchKey,DepartmentKey,TeamKey,TeamName,TeamID,
EmployeeKey,EmployeeID,EmployeeName,DateWrite

--SELECT * FROM RPT
SELECT 
P.TeamName + '|' + P.EmployeeName + '|' + P.EmployeeID+'|'+CAST(P.BranchRank AS NVARCHAR(10))+'|'+CAST(P.DepartmentRank AS NVARCHAR(10))+'|'+CAST(P.TeamRank AS NVARCHAR(10)) AS LeftColumn,
P.DateWrite AS HeaderColumn,
P.BHXH, P.BHYT, P.BHTN,P.KPCD
FROM
(
	SELECT A.BranchKey,A.TeamKey,A.TeamID,A.TeamName,
	A.EmployeeKey,A.EmployeeID,A.EmployeeName,CONCAT( N'THÁNG ',[Month],'/',YEAR) AS DateWrite ,
	A.BHXH, A.BHYT, A.BHTN,A.KPCD,
	D.RANK AS BranchRank,C.RANK AS DepartmentRank,B.RANK as TeamRank FROM RPT A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
) P
    ORDER BY DateWrite,BranchRank,DepartmentRank,TeamRank,LEN(EmployeeID), EmployeeID

DROP TABLE Temp
DROP TABLE RPT";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            DataTable zTable = new DataTable();
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        //Save  chốt các khoản công ty đóng --Đã tính khi tính lương
        public static string LuuCacKhoanTrich_CONGTY(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-06-01 00:00:00'
--declare @ToDate datetime ='2020-06-30 23:59:59'

--Khai báo tham số
declare @PhanTramBHXH_CTY float  =0;
declare @PhanTramBHYT_CTY float  =0;
declare @PhanTramBHTN_CTY float  =0;
declare @PhanTramKPCD_CTY float  =0;

SET @PhanTramBHXH_CTY =[dbo].[LayPhanTramBHXH_CTY](@ToDate);
SET @PhanTramBHYT_CTY =[dbo].[LayPhanTramBHYT_CTY](@ToDate);
SET @PhanTramBHTN_CTY =[dbo].[LayPhanTramBHTN_CTY](@ToDate);
SET @PhanTramKPCD_CTY =[dbo].[LayPhanTramKPCD_CTY](@ToDate);
IF(@PhanTramBHXH_CTY <>0)
	SET @PhanTramBHXH_CTY=@PhanTramBHXH_CTY/100
IF(@PhanTramBHYT_CTY <>0)
	SET @PhanTramBHYT_CTY=@PhanTramBHYT_CTY/100
IF(@PhanTramBHTN_CTY <>0)
	SET @PhanTramBHTN_CTY=@PhanTramBHTN_CTY/100
IF(@PhanTramKPCD_CTY <>0)
	SET @PhanTramKPCD_CTY=@PhanTramKPCD_CTY/100

--Xóa tháng đang chốt
DELETE FROM [dbo].[SLR_FeeCompany] WHERE  DateWrite BETWEEN @FromDate AND @ToDate

--Lấy tất cả danh sách  nhân viên có đóng BHXH trong bảng lương chốt.
--Tiếp lấy mức lương chính để tính toán
declare @Parent nvarchar(50) =newid()
CREATE TABLE Temp
(	
	EmployeeKey NVARCHAR(500) ,
	EmployeeID NVARCHAR(500),
	EmployeeName NVARCHAR(500),
	TeamKey INT,
	TeamID NVARCHAR(500),
	TeamName NVARCHAR(500),
	MLC MONEY
)
--Lấy dữ liệu
INSERT INTO Temp
SELECT  EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,[dbo].[SLR_SoTien_MucLuongTuNgayDenNgay](EmployeeKey,'MLC',@FromDate,@ToDate)
FROM [dbo].[SLR_ReportWorker_Close]
WHERE CodeID ='BHXH' 
AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
AND Amount >0

INSERT INTO Temp
SELECT  EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,
[dbo].[SLR_SoTien_MucLuongTuNgayDenNgay](EmployeeKey,'MLC',@FromDate,@ToDate)
FROM [dbo].[SLR_ReportSupport_Close]
WHERE CodeID ='BHXH' 
AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
AND Amount >0

INSERT INTO Temp
SELECT  EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,
[dbo].[SLR_SoTien_MucLuongTuNgayDenNgay](EmployeeKey,'MLC',@FromDate,@ToDate)
FROM [dbo].[SLR_ReportOffice_Close]
WHERE CodeID ='BHXH' 
AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
AND Amount >0

--SELECT * FROM Temp
--1. Insert BHXH
INSERT INTO  [dbo].[SLR_FeeCompany] (
AutoKey,Parent,ID,DateWrite,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,Amount)
SELECT newid(),@Parent,'BHXH',@ToDate,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,ROUND(MLC * @PhanTramBHXH_CTY,0) FROM Temp
--2. Insert BHYT
INSERT INTO  [dbo].[SLR_FeeCompany] (
AutoKey,Parent,ID,DateWrite,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,Amount)
SELECT newid(),@Parent,'BHYT',@ToDate,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,ROUND(MLC * @PhanTramBHYT_CTY,0) FROM Temp
--3. Insert BHTN
INSERT INTO  [dbo].[SLR_FeeCompany] (
AutoKey,Parent,ID,DateWrite,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,Amount)
SELECT newid(),@Parent,'BHTN',@ToDate,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,ROUND(MLC * @PhanTramBHTN_CTY,0) FROM Temp
--4. Insert KPCD
INSERT INTO  [dbo].[SLR_FeeCompany] (
AutoKey,Parent,ID,DateWrite,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,Amount)
SELECT newid(),@Parent,'KPCD',@ToDate,EmployeeKey,EmployeeID,EmployeeName,
TeamKey,TeamID,TeamName,ROUND(MLC * @PhanTramKPCD_CTY,0) FROM Temp

DROP TABLE Temp

";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                string a = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = "08" + ex.ToString();
            }
            return zResult;
        }
    }
}
