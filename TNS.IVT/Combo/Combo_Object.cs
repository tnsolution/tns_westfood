﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.IVT
{
    public class Combo_Object:Combo_Info
    {
        private int _AutoKey = 0;
        private List<ComboItem_Info> _List_Item;
        public List<ComboItem_Info> List_Item
        {
            get
            {
                return _List_Item;
            }

            set
            {
                _List_Item = value;
            }
        }
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }

        public Combo_Object() : base()
        {
            _List_Item = new List<ComboItem_Info>();
        }

        #region [ Constructor Get Information ]
        public Combo_Object(string Key) : base(Key)
        {
            DataTable ztb = Combo_Data.ListItem(base.Key);
            _List_Item = new List<ComboItem_Info>();
            foreach (DataRow nRow in ztb.Rows)
            {
                ComboItem_Info Stock_Product = new ComboItem_Info(nRow);
                _List_Item.Add(Stock_Product);
            }
        }
        #endregion

        #region [ Constructor Update Information ]
        public void Save_Object()
        {
            string zResult = "";
            base.Save();
            if (base.HasInfo)// if (base.HasInfo && base.IsCommandOK)
            {
                for (int i = 0; i < _List_Item.Count; i++)
                {
                    ComboItem_Info zItem = (ComboItem_Info)_List_Item[i];
                    zItem.ComboKey = base.Key;
                    zItem.CreatedBy = base.CreatedBy;
                    zItem.CreatedName = base.CreatedName;
                    zItem.ModifiedBy = base.ModifiedBy;
                    zItem.ModifiedName = base.ModifiedName;
                    switch (zItem.RecordStatus)
                    {
                        case 99:
                            zItem.DeleteItem();
                            break;
                        case 1:
                            zItem.Create();
                            break;
                        case 2:
                            zItem.Update();
                            break;
                    }
                    zResult += zItem.Message;
                }
            }
            base.Message += zResult;

        }
        public void DeleteObject()
        {
            string zResult = "";
            base.Delete();

            ComboItem_Info zItem = new ComboItem_Info();
            zItem.ModifiedBy = base.ModifiedBy;
            zItem.ModifiedName = base.ModifiedName;
            zItem.ComboKey = base.Key;
            zItem.DeleteAll();
            zResult = zItem.Message;
            base.Message = zResult;
        }
       
        #endregion
    }
}
