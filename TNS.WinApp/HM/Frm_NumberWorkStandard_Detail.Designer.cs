﻿namespace TNS.WinApp
{
    partial class Frm_NumberWorkStandard_Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_NumberWorkStandard_Detail));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_Del = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.lbl_Modified = new System.Windows.Forms.Label();
            this.lbl_Created = new System.Windows.Forms.Label();
            this.dte_DateWrite = new TN_Tools.TNDateTime();
            this.txt_Number = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Persent = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_EmployeeID = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_EmployeeName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6.SuspendLayout();
            this.Panel_Right.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(374, 42);
            this.HeaderControl.TabIndex = 218;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Thông tin NCQĐ và TL HTCV";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btn_Del);
            this.panel6.Controls.Add(this.btn_Save);
            this.panel6.Controls.Add(this.btn_New);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 422);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(374, 64);
            this.panel6.TabIndex = 219;
            // 
            // btn_Del
            // 
            this.btn_Del.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Del.Location = new System.Drawing.Point(245, 11);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Del.Size = new System.Drawing.Size(120, 40);
            this.btn_Del.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.TabIndex = 15;
            this.btn_Del.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Del.Values.Image")));
            this.btn_Del.Values.Text = "Xóa";
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(124, 11);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(120, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 14;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Cập nhật";
            // 
            // btn_New
            // 
            this.btn_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_New.Location = new System.Drawing.Point(3, 11);
            this.btn_New.Name = "btn_New";
            this.btn_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_New.Size = new System.Drawing.Size(120, 40);
            this.btn_New.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.TabIndex = 9;
            this.btn_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_New.Values.Text = "Làm mới";
            // 
            // Panel_Right
            // 
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Right.Controls.Add(this.lbl_Modified);
            this.Panel_Right.Controls.Add(this.lbl_Created);
            this.Panel_Right.Controls.Add(this.dte_DateWrite);
            this.Panel_Right.Controls.Add(this.txt_Number);
            this.Panel_Right.Controls.Add(this.txt_Persent);
            this.Panel_Right.Controls.Add(this.label3);
            this.Panel_Right.Controls.Add(this.txt_EmployeeID);
            this.Panel_Right.Controls.Add(this.txt_EmployeeName);
            this.Panel_Right.Controls.Add(this.label2);
            this.Panel_Right.Controls.Add(this.label5);
            this.Panel_Right.Controls.Add(this.label6);
            this.Panel_Right.Controls.Add(this.label1);
            this.Panel_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panel_Right.Location = new System.Drawing.Point(0, 42);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(374, 380);
            this.Panel_Right.TabIndex = 224;
            // 
            // lbl_Modified
            // 
            this.lbl_Modified.AutoSize = true;
            this.lbl_Modified.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Modified.Location = new System.Drawing.Point(3, 359);
            this.lbl_Modified.Name = "lbl_Modified";
            this.lbl_Modified.Size = new System.Drawing.Size(60, 13);
            this.lbl_Modified.TabIndex = 283;
            this.lbl_Modified.Text = "Chỉnh sửa: ";
            // 
            // lbl_Created
            // 
            this.lbl_Created.AutoSize = true;
            this.lbl_Created.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Created.Location = new System.Drawing.Point(14, 337);
            this.lbl_Created.Name = "lbl_Created";
            this.lbl_Created.Size = new System.Drawing.Size(49, 13);
            this.lbl_Created.TabIndex = 284;
            this.lbl_Created.Text = "Tạo bởi: ";
            // 
            // dte_DateWrite
            // 
            this.dte_DateWrite.CustomFormat = "dd/MM/yyyy";
            this.dte_DateWrite.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_DateWrite.Location = new System.Drawing.Point(123, 20);
            this.dte_DateWrite.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_DateWrite.Name = "dte_DateWrite";
            this.dte_DateWrite.Size = new System.Drawing.Size(121, 26);
            this.dte_DateWrite.TabIndex = 281;
            this.dte_DateWrite.Value = new System.DateTime(((long)(0)));
            // 
            // txt_Number
            // 
            this.txt_Number.Location = new System.Drawing.Point(123, 127);
            this.txt_Number.Name = "txt_Number";
            this.txt_Number.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Number.Size = new System.Drawing.Size(226, 26);
            this.txt_Number.StateCommon.Border.ColorAngle = 1F;
            this.txt_Number.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Number.StateCommon.Border.Rounding = 4;
            this.txt_Number.StateCommon.Border.Width = 1;
            this.txt_Number.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Number.TabIndex = 279;
            this.txt_Number.Text = "0";
            this.txt_Number.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Persent
            // 
            this.txt_Persent.Location = new System.Drawing.Point(123, 168);
            this.txt_Persent.Name = "txt_Persent";
            this.txt_Persent.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Persent.Size = new System.Drawing.Size(226, 26);
            this.txt_Persent.StateCommon.Border.ColorAngle = 1F;
            this.txt_Persent.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Persent.StateCommon.Border.Rounding = 4;
            this.txt_Persent.StateCommon.Border.Width = 1;
            this.txt_Persent.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Persent.TabIndex = 279;
            this.txt_Persent.Text = "0";
            this.txt_Persent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(43, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 15);
            this.label3.TabIndex = 278;
            this.label3.Text = "Ngày nhập";
            // 
            // txt_EmployeeID
            // 
            this.txt_EmployeeID.Location = new System.Drawing.Point(123, 57);
            this.txt_EmployeeID.Name = "txt_EmployeeID";
            this.txt_EmployeeID.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_EmployeeID.Size = new System.Drawing.Size(226, 26);
            this.txt_EmployeeID.StateCommon.Border.ColorAngle = 1F;
            this.txt_EmployeeID.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_EmployeeID.StateCommon.Border.Rounding = 4;
            this.txt_EmployeeID.StateCommon.Border.Width = 1;
            this.txt_EmployeeID.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_EmployeeID.TabIndex = 277;
            this.txt_EmployeeID.Text = "Nhập mã thẻ";
            // 
            // txt_EmployeeName
            // 
            this.txt_EmployeeName.Location = new System.Drawing.Point(123, 92);
            this.txt_EmployeeName.Name = "txt_EmployeeName";
            this.txt_EmployeeName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_EmployeeName.ReadOnly = true;
            this.txt_EmployeeName.Size = new System.Drawing.Size(226, 26);
            this.txt_EmployeeName.StateCommon.Border.ColorAngle = 1F;
            this.txt_EmployeeName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_EmployeeName.StateCommon.Border.Rounding = 4;
            this.txt_EmployeeName.StateCommon.Border.Width = 1;
            this.txt_EmployeeName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_EmployeeName.TabIndex = 276;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(67, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 15);
            this.label2.TabIndex = 272;
            this.label2.Text = "NCQĐ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(50, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 15);
            this.label5.TabIndex = 272;
            this.label5.Text = "Họ và tên";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(56, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 15);
            this.label6.TabIndex = 272;
            this.label6.Text = "TL HTCV";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(64, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 272;
            this.label1.Text = "Mã thẻ";
            // 
            // Frm_NumberWorkStandard_Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 486);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_NumberWorkStandard_Detail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Salary_Default_Detail";
            this.Load += new System.EventHandler(this.Frm_Salary_Default_Detail_Load);
            this.panel6.ResumeLayout(false);
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Right.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Del;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_New;
        private System.Windows.Forms.Panel Panel_Right;
        private System.Windows.Forms.Label label3;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_EmployeeID;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_EmployeeName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Persent;
        private System.Windows.Forms.Label label6;
        private TN_Tools.TNDateTime dte_DateWrite;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Number;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_Modified;
        private System.Windows.Forms.Label lbl_Created;
    }
}