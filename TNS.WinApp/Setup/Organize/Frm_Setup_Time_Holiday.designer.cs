﻿using System.Windows.Forms;

namespace TNS.WinApp
{
    partial class Frm_Setup_Time_Holiday
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Setup_Time_Holiday));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dte_Search = new ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txt_Search = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.Panel_Right = new System.Windows.Forms.Panel();
            this.cbo_Support = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.dte_Holiday = new TN_Tools.TNDateTime();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Del = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonHeader2 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_Modified = new System.Windows.Forms.Label();
            this.lbl_Created = new System.Windows.Forms.Label();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.txt_HolidayName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Description = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Factor = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Rank = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.GVData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.panel1.SuspendLayout();
            this.Panel_Right.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Support)).BeginInit();
            this.Panel_Left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1366, 42);
            this.HeaderControl.TabIndex = 218;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Cài đặt ngày nghỉ/ngày lễ";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.DodgerBlue;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Location = new System.Drawing.Point(0, 42);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(2, 558);
            this.label9.TabIndex = 221;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.dte_Search);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.txt_Search);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(2, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1364, 80);
            this.panel1.TabIndex = 308;
            // 
            // dte_Search
            // 
            this.dte_Search.CustomFormat = "yyyy";
            this.dte_Search.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Search.Location = new System.Drawing.Point(67, 12);
            this.dte_Search.Name = "dte_Search";
            this.dte_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.dte_Search.ShowUpDown = true;
            this.dte_Search.Size = new System.Drawing.Size(111, 23);
            this.dte_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.dte_Search.StateCommon.Border.Rounding = 4;
            this.dte_Search.StateCommon.Border.Width = 1;
            this.dte_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Search.TabIndex = 335;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(24, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 15);
            this.label4.TabIndex = 267;
            this.label4.Text = "Năm";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(4, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 15);
            this.label15.TabIndex = 267;
            this.label15.Text = "Nội dung ";
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(446, 18);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(113, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 238;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(335, 18);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(105, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 206;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(67, 39);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search.Size = new System.Drawing.Size(249, 26);
            this.txt_Search.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search.StateCommon.Border.Rounding = 4;
            this.txt_Search.StateCommon.Border.Width = 1;
            this.txt_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.TabIndex = 110;
            // 
            // Panel_Right
            // 
            this.Panel_Right.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel_Right.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Right.Controls.Add(this.cbo_Support);
            this.Panel_Right.Controls.Add(this.dte_Holiday);
            this.Panel_Right.Controls.Add(this.btn_Save);
            this.Panel_Right.Controls.Add(this.btn_Del);
            this.Panel_Right.Controls.Add(this.btn_New);
            this.Panel_Right.Controls.Add(this.kryptonHeader2);
            this.Panel_Right.Controls.Add(this.label17);
            this.Panel_Right.Controls.Add(this.label1);
            this.Panel_Right.Controls.Add(this.label16);
            this.Panel_Right.Controls.Add(this.label3);
            this.Panel_Right.Controls.Add(this.label2);
            this.Panel_Right.Controls.Add(this.label6);
            this.Panel_Right.Controls.Add(this.lbl_Modified);
            this.Panel_Right.Controls.Add(this.lbl_Created);
            this.Panel_Right.Controls.Add(this.kryptonHeader1);
            this.Panel_Right.Controls.Add(this.txt_HolidayName);
            this.Panel_Right.Controls.Add(this.txt_Description);
            this.Panel_Right.Controls.Add(this.txt_Factor);
            this.Panel_Right.Controls.Add(this.txt_Rank);
            this.Panel_Right.Location = new System.Drawing.Point(996, 122);
            this.Panel_Right.Name = "Panel_Right";
            this.Panel_Right.Size = new System.Drawing.Size(370, 480);
            this.Panel_Right.TabIndex = 309;
            // 
            // cbo_Support
            // 
            this.cbo_Support.DropDownWidth = 119;
            this.cbo_Support.Items.AddRange(new object[] {
            "Có ",
            "Không"});
            this.cbo_Support.Location = new System.Drawing.Point(110, 189);
            this.cbo_Support.Name = "cbo_Support";
            this.cbo_Support.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Support.Size = new System.Drawing.Size(127, 22);
            this.cbo_Support.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Support.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Support.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Support.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Support.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Support.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Support.StateCommon.Item.Border.Width = 1;
            this.cbo_Support.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Support.TabIndex = 335;
            // 
            // dte_Holiday
            // 
            this.dte_Holiday.CustomFormat = "dd/MM/yyyy";
            this.dte_Holiday.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_Holiday.Location = new System.Drawing.Point(111, 52);
            this.dte_Holiday.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_Holiday.Name = "dte_Holiday";
            this.dte_Holiday.Size = new System.Drawing.Size(121, 26);
            this.dte_Holiday.TabIndex = 334;
            this.dte_Holiday.Value = new System.DateTime(((long)(0)));
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(126, 435);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(123, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 332;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Cập nhật";
            // 
            // btn_Del
            // 
            this.btn_Del.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Del.Location = new System.Drawing.Point(256, 435);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Del.Size = new System.Drawing.Size(109, 40);
            this.btn_Del.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.TabIndex = 333;
            this.btn_Del.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Del.Values.Image")));
            this.btn_Del.Values.Text = "Xóa";
            // 
            // btn_New
            // 
            this.btn_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_New.Location = new System.Drawing.Point(4, 435);
            this.btn_New.Name = "btn_New";
            this.btn_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_New.Size = new System.Drawing.Size(116, 40);
            this.btn_New.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.TabIndex = 331;
            this.btn_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_New.Values.Text = "Làm mới";
            // 
            // kryptonHeader2
            // 
            this.kryptonHeader2.AutoSize = false;
            this.kryptonHeader2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.kryptonHeader2.Location = new System.Drawing.Point(0, 430);
            this.kryptonHeader2.Name = "kryptonHeader2";
            this.kryptonHeader2.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader2.Size = new System.Drawing.Size(370, 50);
            this.kryptonHeader2.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader2.TabIndex = 330;
            this.kryptonHeader2.Values.Description = "";
            this.kryptonHeader2.Values.Heading = "";
            this.kryptonHeader2.Values.Image = null;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(49, 281);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 15);
            this.label17.TabIndex = 267;
            this.label17.Text = "Sắp xếp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(16, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 15);
            this.label1.TabIndex = 267;
            this.label1.Text = "Nội dung nghỉ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(47, 222);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(50, 15);
            this.label16.TabIndex = 267;
            this.label16.Text = "Ghi chú";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(5, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 30);
            this.label3.TabIndex = 267;
            this.label3.Text = "Nghỉ lễ tết \r\nđược tính hỗ trợ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(42, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 30);
            this.label2.TabIndex = 267;
            this.label2.Text = "Hệ số\r\nlàm lễ tết";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(62, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 15);
            this.label6.TabIndex = 267;
            this.label6.Text = "Ngày";
            // 
            // lbl_Modified
            // 
            this.lbl_Modified.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_Modified.AutoSize = true;
            this.lbl_Modified.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Modified.Location = new System.Drawing.Point(6, 409);
            this.lbl_Modified.Name = "lbl_Modified";
            this.lbl_Modified.Size = new System.Drawing.Size(60, 13);
            this.lbl_Modified.TabIndex = 285;
            this.lbl_Modified.Text = "Chỉnh sửa: ";
            // 
            // lbl_Created
            // 
            this.lbl_Created.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lbl_Created.AutoSize = true;
            this.lbl_Created.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lbl_Created.Location = new System.Drawing.Point(17, 387);
            this.lbl_Created.Name = "lbl_Created";
            this.lbl_Created.Size = new System.Drawing.Size(49, 13);
            this.lbl_Created.TabIndex = 286;
            this.lbl_Created.Text = "Tạo bởi: ";
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(370, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 223;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Chi tiết";
            // 
            // txt_HolidayName
            // 
            this.txt_HolidayName.Location = new System.Drawing.Point(110, 85);
            this.txt_HolidayName.Multiline = true;
            this.txt_HolidayName.Name = "txt_HolidayName";
            this.txt_HolidayName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_HolidayName.Size = new System.Drawing.Size(250, 53);
            this.txt_HolidayName.StateCommon.Border.ColorAngle = 1F;
            this.txt_HolidayName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_HolidayName.StateCommon.Border.Rounding = 4;
            this.txt_HolidayName.StateCommon.Border.Width = 1;
            this.txt_HolidayName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_HolidayName.TabIndex = 110;
            // 
            // txt_Description
            // 
            this.txt_Description.Location = new System.Drawing.Point(110, 222);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Description.Size = new System.Drawing.Size(250, 53);
            this.txt_Description.StateCommon.Border.ColorAngle = 1F;
            this.txt_Description.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Description.StateCommon.Border.Rounding = 4;
            this.txt_Description.StateCommon.Border.Width = 1;
            this.txt_Description.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Description.TabIndex = 110;
            // 
            // txt_Factor
            // 
            this.txt_Factor.Location = new System.Drawing.Point(110, 147);
            this.txt_Factor.Name = "txt_Factor";
            this.txt_Factor.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Factor.Size = new System.Drawing.Size(127, 26);
            this.txt_Factor.StateCommon.Border.ColorAngle = 1F;
            this.txt_Factor.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Factor.StateCommon.Border.Rounding = 4;
            this.txt_Factor.StateCommon.Border.Width = 1;
            this.txt_Factor.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Factor.TabIndex = 110;
            this.txt_Factor.Text = "0";
            this.txt_Factor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_Rank
            // 
            this.txt_Rank.Location = new System.Drawing.Point(110, 281);
            this.txt_Rank.Name = "txt_Rank";
            this.txt_Rank.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Rank.Size = new System.Drawing.Size(126, 26);
            this.txt_Rank.StateCommon.Border.ColorAngle = 1F;
            this.txt_Rank.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Rank.StateCommon.Border.Rounding = 4;
            this.txt_Rank.StateCommon.Border.Width = 1;
            this.txt_Rank.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Rank.TabIndex = 110;
            this.txt_Rank.Text = "0";
            this.txt_Rank.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Panel_Left
            // 
            this.Panel_Left.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Panel_Left.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Left.Controls.Add(this.GVData);
            this.Panel_Left.Controls.Add(this.kryptonHeader3);
            this.Panel_Left.Location = new System.Drawing.Point(0, 122);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(995, 478);
            this.Panel_Left.TabIndex = 310;
            // 
            // GVData
            // 
            this.GVData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVData.AllowEditing = false;
            this.GVData.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVData.AutoResize = true;
            this.GVData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.ExtendLastCol = true;
            this.GVData.Location = new System.Drawing.Point(0, 30);
            this.GVData.Name = "GVData";
            this.GVData.Rows.DefaultSize = 19;
            this.GVData.Size = new System.Drawing.Size(995, 448);
            this.GVData.TabIndex = 286;
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(995, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 287;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "DANH SÁCH";
            // 
            // Frm_Setup_Time_Holiday
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DodgerBlue;
            this.ClientSize = new System.Drawing.Size(1366, 600);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.Panel_Right);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Setup_Time_Holiday";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BẢNG CÀI ĐẶT NGÀY NGHỈ";
            this.Load += new System.EventHandler(this.Frm_CategoryHoliday_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.Panel_Right.ResumeLayout(false);
            this.Panel_Right.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Support)).EndInit();
            this.Panel_Left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private Label label9;
        private Panel panel1;
        private Label label15;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search;
        private Panel Panel_Right;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Del;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_New;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader2;
        private Label label17;
        private Label label16;
        private Label label6;
        private Label lbl_Modified;
        private Label lbl_Created;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Description;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Factor;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Rank;
        private Panel Panel_Left;
        private C1.Win.C1FlexGrid.C1FlexGrid GVData;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private TN_Tools.TNDateTime dte_Holiday;
        private Label label1;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_HolidayName;
        private Label label2;
        private ComponentFactory.Krypton.Toolkit.KryptonDateTimePicker dte_Search;
        private Label label4;
        private Label label3;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Support;
    }
}