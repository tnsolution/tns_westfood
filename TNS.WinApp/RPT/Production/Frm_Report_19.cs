﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_19 : Form
    {
        int _TeamKey = 0;
        string _InEmployeeID = "";
        public Frm_Report_19()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            Utils.DrawLVStyle(ref LVData);
            Utils.SizeLastColumn_LV(LVData);

            LVData.Click += LVData_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Hide.Click += Btn_Hide_Click;
            btn_Show.Click += Btn_Show_Click;
            btn_Export.Click += Btn_Export_Click;
        }
        

        

        private void Frm_Report_19_Load(object sender, EventArgs e)
        {
            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            InitLayout_LV(LVData);
            InitData();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            splitContainer1.SplitterDistance = 300;
        }
        private void Btn_Show_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 300;
            btn_Hide.Visible = true;
            btn_Show.Visible = false;
        }

        private void Btn_Hide_Click(object sender, EventArgs e)
        {
            splitContainer1.SplitterDistance = 120;
            btn_Hide.Visible = false;
            btn_Show.Visible = true;
        }
        private void LVData_Click(object sender, EventArgs e)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            if (LVData.SelectedItems.Count > 0)
            {
                _TeamKey = LVData.SelectedItems[0].Tag.ToInt();
                DataTable zEmployee = Report.ListEmployeeOfTeam(_TeamKey);
                _InEmployeeID = "";
                if (zEmployee.Rows.Count > 0)
                {
                    for (int i = 0; i < zEmployee.Rows.Count; i++)
                    {

                        _InEmployeeID += "'" + zEmployee.Rows[i]["EmployeeID"] + "',";

                    }
                    _InEmployeeID = _InEmployeeID.Remove(_InEmployeeID.Length - 1, 1);

                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
                else
                {
                    MessageBox.Show("Không tìm thấy nhân viên!");
                }
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            if (LVData.SelectedItems.Count > 0)
            {
                _TeamKey = LVData.SelectedItems[0].Tag.ToInt();
                DataTable zEmployee = Report.ListEmployeeOfTeam(_TeamKey);
                _InEmployeeID = "";
                if (zEmployee.Rows.Count > 0)
                {
                    for (int i = 0; i < zEmployee.Rows.Count; i++)
                    {

                        _InEmployeeID += "'" + zEmployee.Rows[i]["EmployeeID"] + "',";

                    }
                    _InEmployeeID = _InEmployeeID.Remove(_InEmployeeID.Length - 1, 1);

                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
                else
                {
                    MessageBox.Show("Không tìm thấy nhân viên!");
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Luong_Khoan_Nang_Suat_Nhan_Su_Day_Du.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVEmployee.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã nhóm";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên nhóm";
            colHead.Width = 180;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            DataTable In_Table = new DataTable();

            In_Table = HoTroSanXuat.DanhSachToNhom();
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["TeamKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        private void DisplayData()
        {
            DataTable zTable = Report.Report_19(dte_FromDate.Value, dte_ToDate.Value, _InEmployeeID);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);

                DataTable zTablePivot = zPvt.Generate("WORK", "ORDER",
                     new string[] { "LK", "TG", "SR", "TP" }, "CÔNG NHÂN", "TỔNG", "TỔNG",
                     new string[] { "LƯƠNG KHOÁN", "THỜI GIAN", "SỐ RỔ", "THÀNH PHẨM" }, 0, false);

                //ĐIỀU CHỈNH BẢNG ĐÃ PIVOT
                DataTable zTableView = new DataTable();

                zTableView.Columns.Add("EmployeeName", typeof(string));
                zTableView.Columns.Add("EmployeeID", typeof(string));
                zTableView.Columns.Add("OverMoney", typeof(string));

                //------------Xóa dong trống
                int r = zTablePivot.Columns.Count;
                for (int i = 1; i < r; i++)
                {

                    string ColumnName = zTablePivot.Columns[i].ColumnName;
                    if (ColumnName.Substring(0, 3) == "|0|")
                    {
                        zTablePivot.Columns.Remove(zTablePivot.Columns[i].ColumnName);
                        r--;
                        i--;
                    }
                }
                //----------------------------
                for (int i = 1; i < zTablePivot.Columns.Count; i++)
                {
                    string ColumnName = zTablePivot.Columns[i].ColumnName;
                    zTableView.Columns.Add(ColumnName);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    DataRow rPivot = zTablePivot.Rows[i];
                    DataRow zRowView = zTableView.NewRow();

                    string rColumn0 = rPivot[0].ToString();
                    if (rColumn0.Contains("|"))
                    {
                        string[] temp = rColumn0.Split('|');

                        // Convert.ToDateTime(temp[0]).ToString("dd/MM/yyyy");
                        zRowView["EmployeeName"] = temp[0];
                        zRowView["EmployeeID"] = temp[1];
                        zRowView["OverMoney"] = FormatMoney(temp[2]);
                    }
                    else
                    {
                        zRowView["EmployeeName"] = rColumn0;
                    }

                    for (int j = 1; j < zTablePivot.Columns.Count; j++)
                    {
                        zRowView[j + 2] = FormatMoney(rPivot[j]);
                    }

                    zTableView.Rows.Add(zRowView);
                }

                DataView dv = zTableView.DefaultView;
                dv.Sort = " EmployeeID ASC";
                DataTable zSortTable = dv.ToTable();

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitLayoutGV(zSortTable);
                    FillDataGV(zSortTable);
                }));
            }
        }
        void InitLayoutGV(DataTable zTable)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();

            GVEmployee.Cols.Add(zTable.Columns.Count + 1);
            GVEmployee.Rows.Add(4);

            CellRange zRange = GVEmployee.GetCellRange(0, 0, 3, 0);
            zRange.Data = "STT";

            zRange = GVEmployee.GetCellRange(0, 1, 3, 1);
            zRange.Data = "HỌ VÀ TÊN";

            zRange = GVEmployee.GetCellRange(0, 2, 3, 2);
            zRange.Data = "MÃ THẺ";

            zRange = GVEmployee.GetCellRange(0, 3, 3, 3);
            zRange.Data = "GIÃN CA";

            for (int i = 3; i < zTable.Columns.Count; i++)
            {
                DataColumn Col = zTable.Columns[i];

                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length >= 4)
                {
                    GVEmployee.Rows[0][i+1] = strCaption[0];  //Sản phẩm
                    GVEmployee.Rows[1][i+1] = strCaption[1];  //Công việc
                    GVEmployee.Rows[2][i+1] = strCaption[2];  //Đơn giá
                    GVEmployee.Rows[3][i+1] = strCaption[3];  //"LK", "TG", "SLTP", "NS"
                }
                else
                {
                    GVEmployee.Rows[0][i+1] = strCaption[0];  //Tổng
                    GVEmployee.Rows[3][i+1] = strCaption[1];   //"LK", "TG", "SLTP", "NS"
                }
                GVEmployee.Cols[i].Width = 110;
            }

            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;

            GVEmployee.Rows[0].AllowMerging = true;
            GVEmployee.Rows[1].AllowMerging = true;
            GVEmployee.Rows[2].AllowMerging = true;
            GVEmployee.Rows[3].AllowMerging = true;

            GVEmployee.Cols[0].Width = 40;
            GVEmployee.Cols[0].AllowMerging = true;

            GVEmployee.Cols[1].Width = 150;
            GVEmployee.Cols[1].AllowMerging = true;

            GVEmployee.Cols[2].AllowMerging = true;
            GVEmployee.Cols[2].Width = 100;

            GVEmployee.Cols[3].AllowMerging = true;
            GVEmployee.Cols[3].Width = 100;

            GVEmployee.Rows.Fixed = 4;
            GVEmployee.Cols.Fixed = 3;

            GVEmployee.Rows[0].StyleNew.WordWrap = true;
            GVEmployee.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[1].StyleNew.WordWrap = true;
            GVEmployee.Rows[1].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[2].StyleNew.WordWrap = true;
            GVEmployee.Rows[2].TextAlign = TextAlignEnum.CenterCenter;

            GVEmployee.Rows[3].StyleNew.WordWrap = true;
            GVEmployee.Rows[3].TextAlign = TextAlignEnum.CenterCenter;

            for (int i = 3; i < GVEmployee.Cols.Count; i++)
            {
                GVEmployee.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        void FillDataGV(DataTable zTable)
        {
            int Row = 4;
            for (int i = 0; i < zTable.Rows.Count; i++)
            {
                GVEmployee.Rows.Add();
                GVEmployee.Rows[Row][0] = i + 1;
                for (int j = 0; j < zTable.Columns.Count; j++)
                {

                    //cộng thêm tiền giơ dư
                    if ((j) == (zTable.Columns.Count - 4))
                    {
                        float Sub = UnFormatMoney(zTable.Rows[i][zTable.Columns.Count - 4]).ToFloat();
                        float OverMoney = UnFormatMoney(zTable.Rows[i][2]).ToFloat();
                        float Total = Sub + OverMoney;
                        GVEmployee.Rows[Row][j+1] = FormatMoney(Total);
                    }
                    else
                    {
                        GVEmployee.Rows[Row][j + 1] = zTable.Rows[i][j].ToString();
                    }
                }

                Row++;
            }
        }
        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        string UnFormatMoney(object input)
        {
            try
            {
                if (input.ToString().Length > 0)
                {
                    string zTemp = input.ToString().Replace(".", "");
                    zTemp = zTemp.Replace(",", ".");
                    double zResult = double.Parse(zTemp);
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("en-US"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
