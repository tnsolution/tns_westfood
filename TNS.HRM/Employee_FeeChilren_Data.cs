﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Employee_FeeChildren_Data
    {
        public static DataTable ListDepartment()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Department WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListTeam(int DepartmentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Team WHERE DepartmentKey =@DepartmentKey AND RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT * FROM [dbo].[HRM_FeeChildren] 
WHERE RecordStatus <> 99 
AND TeamKey =@TeamKey AND CategoryID = 'HTCN'
ORDER BY LEN(EmployeeID) ,EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        #region[đóng 17/02/2021]
//        public static DataTable List01(int BranchKey, int DepartmentKey, int TeamKey, int CategoryKey, int Type, string Search)
//        {
//            DataTable zTable = new DataTable();
//            string zSQL = ";WITH #TEMP AS (";
//            zSQL += @"SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID, [dbo].[Fn_GetTeamName](TeamKey)AS TeamName,[dbo].[Fn_GetBranchKey_FromEmployeeKey](EmployeeKey) AS BranchKey FROM [dbo].[HRM_FeeChildren] WHERE RecordStatus <> 99 ";
//            if (Type == 1)
//                zSQL += " AND ToDate is null ";
//            if (Type == 2)
//                zSQL += " AND ToDate is not null ";
//            if (TeamKey > 0)
//                zSQL += " AND TeamKey =@TeamKey ";
//            if (DepartmentKey > 0)
//                zSQL += " AND DepartmentKey =@DepartmentKey ";
//            if (BranchKey > 0)
//            {
//                zSQL += " AND [dbo].[Fn_GetBranchKey_FromEmployeeKey](EmployeeKey) = @BranchKey";
//            }
//            if (Search.Trim().Length > 0)
//            {
//                zSQL += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
//            }
//            if (CategoryKey > 0)
//            {
//                zSQL += " AND CategoryKey =@CategoryKey ";
//            }
//            zSQL += ")";
//            zSQL += @"SELECT A.AutoKey,A.EmployeeName,A.EmployeeID,A.TeamKey,A.TeamID,A.TeamName,A.CategoryID,A.Number,A.FromDate,A.ToDate,A.LeavingDate,A.Description FROM #TEMP A
//LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
//LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
//LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
//ORDER BY D.RANK, C.RANK, B.RANK, LEN(EmployeeID), EmployeeID,FromDate DESC";

//            string zConnectionString = ConnectDataBase.ConnectionString;
//            try
//            {
//                SqlConnection zConnect = new SqlConnection(zConnectionString);
//                zConnect.Open();
//                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
//                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
//                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
//                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
//                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
//                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
//                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
//                zAdapter.Fill(zTable);
//                zCommand.Dispose();
//                zConnect.Close();
//            }
//            catch (Exception ex)
//            {
//                string zstrMessage = "08" + ex.ToString();
//            }
//            return zTable;
//        }
        #endregion

        public static DataTable List01_V2(int BranchKey, int DepartmentKey, int TeamKey, int CategoryKey, DateTime LeavingDate, string Search)
        {
            DateTime zLeavingDate = new DateTime(LeavingDate.Year, LeavingDate.Month, LeavingDate.Day, 0, 0, 0);
            DataTable zTable = new DataTable();

            string zFillter = "";
            if (TeamKey > 0)
                zFillter += " AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@Date,@Date) =@TeamKey ";
            if (DepartmentKey > 0)
                zFillter += " AND [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@Date,@Date) =@DepartmentKey ";
            if (BranchKey > 0)
            {
                zFillter += " AND [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@Date,@Date) = @BranchKey";
            }
            if (Search.Trim().Length > 0)
            {
                zFillter += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }
            if (CategoryKey > 0)
            {
                zFillter += " AND CategoryKey =@CategoryKey ";
            }

            string zSQL = @";WITH #TEMP AS (
            SELECT *,
            [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@Date,@Date) AS TeamKeys,
            [dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@Date,@Date)) AS TeamID,
            [dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@Date,@Date))AS TeamName,
            [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@Date,@Date) AS BranchKey 
            FROM [dbo].[HRM_FeeChildren] WHERE RecordStatus <> 99  @Customer 
            AND (LeavingDate IS NULL OR LeavingDate >=  @Date)
            AND (ToDate >= @Date)
            )

SELECT A.AutoKey,A.EmployeeName,A.EmployeeID,A.TeamKeys AS TeamKey,A.TeamID,A.TeamName,A.CategoryID,A.Number,A.BirthDay,A.FromDate,A.ToDate,A.LeavingDate,A.Description FROM #TEMP A
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeys
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK, C.RANK, B.RANK, LEN(EmployeeID), EmployeeID,FromDate DESC";

            zSQL = zSQL.Replace("@Customer",zFillter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zLeavingDate;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static string Child_CapNhatNgayHetHan_NghiViec(string EmployeeKey, DateTime DateTime, string ModifiedBy, string ModifiedName)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE HRM_FeeChildren SET LeavingDate = @LeavingDate , ModifiedOn =GETDATE(),ModifiedBy =@ModifiedBy, ModifiedName=@ModifiedName
WHERE EmployeeKey= @EmployeeKey AND LeavingDate IS NULL AND RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@LeavingDate", SqlDbType.DateTime).Value = DateTime;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Ex)
            {
                zResult = Ex.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
