﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;
using TN.Library.System;

namespace TN.Library.HumanResource
{
    public class Employee_Time_Data
    {
        public static DataTable List(DateTime TimeCheck, int TeamKey)
        {
            DateTime FromDate = new DateTime(TimeCheck.Year, TimeCheck.Month, TimeCheck.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(TimeCheck.Year, TimeCheck.Month, TimeCheck.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Employee_Time A WHERE A.TeamKey = @Team AND A.CheckTime BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                zCommand.Parameters.Add("@Team", SqlDbType.NVarChar).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List(DateTime TimeCheck, string TeamName)
        {
            DateTime FromDate = new DateTime(TimeCheck.Year, TimeCheck.Month, TimeCheck.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(TimeCheck.Year, TimeCheck.Month, TimeCheck.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM HRM_Employee_Time A WHERE A.TeamName = N'" + TeamName + "' AND A.CheckTime BETWEEN @FromDate AND @ToDate";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_TimeKeeping(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeName,EmployeeID,COUNT(BeginTime) AS WorkDate,ROUND(SUM(OverTime),1) AS ToTal_Time 
FROM[dbo].[HRM_Employee_Time]
WHERE CheckTime BETWEEN @FromDate AND @ToDate
AND BeginTime > 0
GROUP BY EmployeeName,CardID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_TimeKeeping(string CardID, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeName,EmployeeID,ROUND(BeginTime,1) AS BeginTime,ROUND(EndTime,1) AS EndTime,
ROUND(OffTime,1) AS OffTime,ROUND(TotalTime,1) AS ToTalTime,ROUND(OverTime,1) AS OverTime,CheckTime,
ROUND(Rice_Number,1) AS Rice_Number,ROUND(Rice_Value,1) AS Rice_Value, ROUND(Rice_Money,1) AS Rice_Money
FROM [dbo].[HRM_Employee_Time] 
WHERE EmployeeID = @EmployeeID AND CheckTime BETWEEN @FromDate AND @ToDate

--,ROUND(PointTime,1) AS PointTime,ROUND(NoPermit,1) AS NoPermit,ROUND(NoPermit_Money,1) AS NoPermit_Money,
--ROUND(AlternateOff,1) AS AlternateOff";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@CardID", SqlDbType.NVarChar).Value = CardID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static int Count(string EmployeeID, DateTime FromDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM [dbo].[HRM_Employee_Time] WHERE EmployeeID = @EmployeeID AND CheckTime BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
    }
}
