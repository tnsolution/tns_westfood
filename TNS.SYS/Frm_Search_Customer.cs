﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.Customer;
using TN.Library.Miscellaneous;

namespace TN_WinApp
{
    public partial class Frm_Search_Customer : Form
    {
        public Frm_Search_Customer()
        {
            InitializeComponent();
        }
        DataTable ztb = new DataTable();
        public string CustomerKey = "";
        public string CustomerID = "";
        public string CustomerName = "";
        private void Frm_Search_Customer_Load(object sender, EventArgs e)
        {
            Intable();
            LV_Search_Layout(LV_Search);
            LoadDataToToolbox.ComboBoxData(cbo_Slug_Customer, ztb, false, 0, 1);
        }

        private void Intable()
        {
            ztb.Columns.Add("Value");
            ztb.Columns.Add("Name");
            ztb.Rows.Add("0","Khách hàng cá nhân");
            ztb.Rows.Add("1","Khách hàng doanh nghiệp");
        }

        private void txt_Search_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            {
                LV_Search.Focus();
                if (LV_Search.Items.Count > 0)
                    LV_Search.Items[0].Selected = true;
            }
            else
            {
                if (e.KeyCode != Keys.Enter)
                {
                    if (txt_Search.Text.Length > 0)

                    {
                        LV_Search.Visible = true;
                        LV_SearchLoadData();
                    }
                    else
                        LV_Search.Visible = false;
                }
            }
        }

        private void LV_Search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txt_Search.Tag = LV_Search.SelectedItems[0].Tag;
            }
        }
        #region[ListView]
        private void LV_Search_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã khách hàng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên khách hàng";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }

        public void LV_SearchLoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Search;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();
            int _Slug = int.Parse(cbo_Slug_Customer.SelectedValue.ToString());
            In_Table = Customer_Data.Search(txt_Search.Text, _Slug);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["CustomerKey"].ToString();
                lvi.BackColor = Color.White; ;

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CustomerName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;
        }

        #endregion

        private void LV_Search_ItemActivate(object sender, EventArgs e)
        {
            CustomerKey = LV_Search.SelectedItems[0].Tag.ToString();
            CustomerID= LV_Search.SelectedItems[0].SubItems[1].Text;
            CustomerName = LV_Search.SelectedItems[0].SubItems[2].Text;
            this.Close();
        }
    }
}
