﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_3_V2 : Form
    {
        public Frm_Report_3_V2()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
        }



        private void Frm_Report_3_V2_Load(object sender, EventArgs e)
        {
            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBox("Thông báo", "Bạn chỉ được chọn trong 1 tháng", 2);
                return;
            }

            try
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;

                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = ".xlsx";
            FDialog.FileName = "Phân_Bổ_Lương_Khoán_Theo_Thành_Phẩm_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if(checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }
                
            }
        }
        #region  //Process Data          
        private void DisplayData()
        {
            DataTable zTable = Report.No3_V3(dte_FromDate.Value, dte_ToDate.Value);
            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable TableView = zPvt.Generate("TeamName", "PRODUCT", new string[] { "NGAYTHUONG", "CHUNHAT", "NGAYLE", "TONG" }, "PRODUCT", "TỔNG", "TỔNG", new string[] { "NGÀY THƯỜNG", "CHỦ NHẬT", "NGÀY LỄ", "TỔNG" }, 0);

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataDetail(TableView);
                }));
            }
        }
        void InitDataDetail(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 2;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zRange = GVData.GetCellRange(0, 0, 1, 0);
            zRange.Data = "STT";
            GVData.MergedRanges.Add(zRange);

            zRange = GVData.GetCellRange(0, 1, 1, 1);
            zRange.Data = "MÃ";
            GVData.MergedRanges.Add(zRange);

            zRange = GVData.GetCellRange(0, 2, 1, 2);
            zRange.Data = "SẢN PHẨM";
            GVData.MergedRanges.Add(zRange);



            //Row Header
            int ColStart = 3;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');

                GVData.Rows[0][ColStart] = strCaption[0];           //Ten Nhom
                GVData.Rows[1][ColStart] = strCaption[1];           //SL - LK                  

                ColStart++;
            }

            for (int i = 3; i < ToTalCol; i++)
            {
                if (i % 4 == 0)
                {
                    GVData.MergedRanges.Add(0, i - 1, 0, i + 2);
                }
            }

            ColStart = 2;
            int RowStart = 2;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                GVData.Rows[rIndex + RowStart][0] = (rIndex + 1);

                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[0].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[1];
                    //GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                }

                for (int cIndex = 1; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = FormatMoney(rData[cIndex]);
                }
            }

            //Style         
            GVData.ExtendLastCol = false;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows.Fixed = 2;
            GVData.Cols.Fixed = 3;

            GVData.Rows[0].Height = 40;
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[1].Height = 40;
            GVData.Cols[0].Width = 50;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].Width = 300;


            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;


            zRange = GVData.GetCellRange(TotalRow - 1, 0, TotalRow - 1, 2);
            zRange.Data = "TỔNG";
            GVData.MergedRanges.Add(zRange);

            //GVData.Rows[TotalRow - 1][1] = "TỔNG";
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        #endregion

        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #region [Dùng kéo rê form]
        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion                
    }
}
