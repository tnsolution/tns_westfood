﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;
//Nếu có tổng số nguyên liệu ưu tiên nguyên liệu
//    Ngược lại ưu tiên số rổ nếu có tổng số rổ
//        Ngược lại lấy số giờ
namespace TNS.WinApp
{
    public partial class Frm_Production_Calculator_V2 : Form
    {
        #region[Khai báo hằng số]



        //float _OMoney = HoTroSanXuat.LayTienNgoaiGio(SessionUser.Date_Work);        //tiền dư giờ
        const int _TeamSCCD = 88;
        const int _TeamSCDH = 89;
        int _DayOfWeek = 0;
        #endregion

        private string _TeamName = "";
        private int _TeamKey = 0;
        private string _WorkName = "";
        private int _WorkKey = 0;
        private string _OrderID = "";
        private string _OrderKey = "";

        DateTime _FromDate = new DateTime();
        DateTime _ToDate = new DateTime();


        public Frm_Production_Calculator_V2()
        {
            InitializeComponent();

            InitLayout_GVTeam(GVTeam);
            InitLayout_GVWork(GVWork);
            InitLayout_GVOrder(GVOrder);
            InitLayout_GVRate(GVRate);
            InitLayout_GVData(GVEmployee);
        }

        private void Frm_Production_Calculator_V2_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVEmployee, true);
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DoubleBuffered(GVWork, true);
            Utils.DoubleBuffered(GVRate, true);
            Utils.DoubleBuffered(GVOrder, true);

            Utils.DrawGVStyle(ref GVEmployee);
            Utils.DrawGVStyle(ref GVTeam);
            Utils.DrawGVStyle(ref GVWork);
            Utils.DrawGVStyle(ref GVRate);
            Utils.DrawGVStyle(ref GVOrder);

            Panel_Right.Visible = true;
            btnHideInfo.Visible = true;
            btnShowInfo.Visible = false;

            InitData_GVTeam(GVTeam, Team_Data.List_Team());
            InitData_GVOrder(GVOrder, null);

            GVTeam.SelectionChanged += GVTeam_Click;
            GVWork.SelectionChanged += GVWork_Click;
            GVOrder.SelectionChanged += GVOrder_Click;

            //btn_Count.Click += Btn_Count_Click;
            //btn_Save.Click += Btn_Save_Click;

            #region [Datetime Process]
            DateTime DateWork = SessionUser.Date_Work;
            dte_Date.Value = DateWork;
            dte_Date.Validated += (o, s) =>
            {
                DateWork = dte_Date.Value;

                _FromDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 0, 0, 0);
                _ToDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 23, 59, 59);


            };
            _FromDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 0, 0, 0);
            _ToDate = new DateTime(DateWork.Year, DateWork.Month, DateWork.Day, 23, 59, 59);
            #endregion
        }
        private void GVTeam_Click(object sender, EventArgs e)
        {
            if (GVTeam.SelectedRows.Count > 0 &&
                GVTeam.CurrentRow.Cells["TeamName"].Tag != null)
            {
                //Clear các bảng con khi chọn lại
                _WorkKey = 0;
                _OrderKey = "";
                _TeamKey = int.Parse(GVTeam.CurrentRow.Cells["TeamName"].Tag.ToString());
                _TeamName = GVTeam.CurrentRow.Cells["TeamName"].Value.ToString();

                txt_title.Text = _TeamName;
                DateTime zFromDate = new DateTime(_FromDate.Year, _FromDate.Month, _FromDate.Day, 0, 0, 0);
                DateTime zToDate = new DateTime(_ToDate.Year, _ToDate.Month, _ToDate.Day, 23, 59, 59);
                _DayOfWeek = HoTroSanXuat.LayHeSoChuNhat(dte_Date.Value);    //hệ số chủ nhật

                DataTable zTableWork = HoTroSanXuat.CongViecNhom(_TeamKey, zFromDate, zToDate);
                InitData_GVWork(GVWork, zTableWork);
                InitData_GVOrder(GVOrder, null);
                InitData_GVData(GVEmployee, null);
            }
        }
        private void GVWork_Click(object sender, EventArgs e)
        {
            if (GVWork.SelectedRows.Count > 0 &&
                GVWork.CurrentRow.Cells["StageName"].Tag != null)
            {
                //Clear các bảng con khi chọn lại
                _OrderKey = "";
                _WorkKey = int.Parse(GVWork.CurrentRow.Cells["StageName"].Tag.ToString());
                _WorkName = GVWork.CurrentRow.Cells["StageName"].Value.ToString();

                txt_title.Text = _TeamName + " > " + _WorkName;
                DataTable zTableOrder = HoTroSanXuat.DonHangThucHien(_TeamKey, _WorkKey, _FromDate, _ToDate);
                InitData_GVOrder(GVOrder, zTableOrder);
                InitData_GVData(GVEmployee, null);

                //--------auto select 
                if (GVOrder.Rows.Count > 0)
                {
                    LoadInfo_Order(GVOrder.Rows[0]);
                }
            }
        }
        private void GVOrder_Click(object sender, EventArgs e)
        {
            if (GVOrder.SelectedRows.Count > 0 &&
                GVOrder.CurrentRow.Cells["OrderID"].Tag != null)
            {
                Cursor = Cursors.WaitCursor;
                LoadInfo_Order(GVOrder.CurrentRow);
                Cursor = Cursors.Default;
            }
        }

        //Load chi tiết đơn hàng
        private void LoadInfo_Order(DataGridViewRow nRow)
        {
            string Name = GVOrder.CurrentRow.Cells["OrderID"].Value.ToString();
            txt_title.Text = _TeamName + " > " + _WorkName + " > " + Name;

            _OrderKey = nRow.Cells["OrderID"].Tag.ToString();
            DateTime zOrderDate = Convert.ToDateTime(nRow.Cells["OrderDate"].Value.ToString());
            string zOrderID = nRow.Cells["OrderID"].Value.ToString();

            float zQuantity = 0;
            if (float.TryParse(nRow.Cells["Quantity"].Value.ToString(), out zQuantity))
            {

            }
            float zPrice = 0;
            if (float.TryParse(nRow.Cells["Price"].Value.ToString(), out zPrice))
            {

            }
            float zTotalBasket = 0;
            if (float.TryParse(nRow.Cells["TotalBasket"].Value.ToString(), out zTotalBasket))
            {

            }
            float zTotalKg = 0;
            if (float.TryParse(nRow.Cells["TotalKg"].Value.ToString(), out zTotalKg))
            {

            }
            float zTotalTime = 0;
            if (float.TryParse(nRow.Cells["TotalTime"].Value.ToString(), out zTotalTime))
            {

            }
            float zRateOrder = 0;
            if (float.TryParse(nRow.Cells["RateOrder"].Value.ToString(), out zRateOrder))
            {

            }
            float zRateDay = 0;
            if (float.TryParse(nRow.Cells["RateDay"].Value.ToString(), out zRateDay))
            {

            }
            float zPercent = 0;
            if (float.TryParse(nRow.Cells["PercentOrder"].Value.ToString(), out zPercent))
            {

            }

            txt_Price.Text = zPrice.Ton1String();
            txt_Quantity.Text = zQuantity.Ton2String();
            txt_Basket.Text = zTotalBasket.Ton1String();
            txt_Kilogram.Text = zTotalKg.Ton1String();
            txt_Time.Text = zTotalTime.Ton2String();
            txt_Percent.Text = zPercent.ToString();

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void DisplayData()
        {
            DataTable zTableRate = Order_Rate_Data.List(_OrderKey);
            DataTable zTableEmployee = HoTroSanXuat.CacNhanVienThucHien(_OrderKey);
            this.Invoke(new MethodInvoker(delegate ()
            {
                InitData_GVRate(GVRate, zTableRate);
                InitData_GVData(GVEmployee, zTableEmployee);
                InitData_GVSum(GVEmployee);
            }));
        }

        #region[HÀM LAYOUT SƯ DUNG CHUNG]
        //Lay out Listview List nhóm
        private void InitLayout_GVTeam(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("TeamID", "Mã");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["TeamName"].Width = 200;
            GV.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVTeam(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["TeamName"].Tag = r["TeamKey"].ToString();
                    GvRow.Cells["TeamName"].Value = r["TeamName"].ToString();
                    GvRow.Cells["TeamID"].Value = r["TeamID"].ToString();
                }
            }
            GV.ClearSelection();
        }
        //Lay out Listview List công việc
        private void InitLayout_GVWork(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("StageID", "Mã");
            GV.Columns.Add("StageName", "Tên công việc");
            GV.Columns.Add("StagePrice", "Đơn giá");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["StageName"].Width = 200;
            GV.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.Columns["StagePrice"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["StagePrice"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVWork(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["StageName"].Tag = r["StageKey"].ToString();
                    GvRow.Cells["StageName"].Value = r["StageName"].ToString();
                    GvRow.Cells["StagePrice"].Value = r["StagePrice"].Ton1String();
                    GvRow.Cells["StageID"].Value = r["StageID"].ToString();
                }
            }
            GV.ClearSelection();
        }
        //Lay out Listview List Đơn hang
        private void InitLayout_GVOrder(DataGridView GV)
        {
            GV.Columns.Clear();
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("OrderIDCompare", "Mã tham chiếu");
            GV.Columns.Add("OrderID", "Mã đơn hàng");
            GV.Columns.Add("WorkStatus", "Tính trạng");
            GV.Columns.Add("OrderDate", "Ngày đơn hàng");
            GV.Columns.Add("Quantity", "Số lượng");
            GV.Columns.Add("Price", "Đơn giá");
            GV.Columns.Add("TotalKg", "Tổng kí");
            GV.Columns.Add("TotalBasket", "Tổng rổ");
            GV.Columns.Add("TotalTime", "Tổng giờ");
            GV.Columns.Add("RateOrder", "Tổng hệ số");
            GV.Columns.Add("RateDay", "Hệ số lễ");
            GV.Columns.Add("PercentOrder", "Tỷ lệ hoàn thành");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["OrderIDCompare"].Width = 150;
            GV.Columns["OrderIDCompare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["OrderID"].Width = 150;
            GV.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["WorkStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["WorkStatus"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVOrder(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["OrderID"].Tag = r["OrderKey"].ToString();
                    GvRow.Cells["OrderID"].Value = r["OrderID"].ToString();
                    GvRow.Cells["OrderIDCompare"].Value = r["OrderID_Compare"].ToString();
                    if (r["WorkStatus"].ToInt() == 1)
                    {
                        GvRow.Cells["WorkStatus"].Value = "Đã làm xong";
                    }
                    else if (r["WorkStatus"].ToInt() == 0)
                    {
                        GvRow.Cells["WorkStatus"].Value = "Đang thực hiện";
                    }
                    else
                    {
                        GvRow.Cells["WorkStatus"].Value = "Lỗi";
                    }
                    GvRow.Cells["OrderDate"].Value = r["OrderDate"].ToString();
                    GvRow.Cells["Quantity"].Value = r["Quantity"].ToString();
                    GvRow.Cells["Price"].Value = r["Price"].ToString();
                    GvRow.Cells["TotalBasket"].Value = r["TotalBasket"].ToString();
                    GvRow.Cells["TotalKg"].Value = r["TotalKg"].ToString();
                    GvRow.Cells["TotalTime"].Value = r["TotalTime"].ToString();
                    GvRow.Cells["RateOrder"].Value = r["RateOrder"].ToString();
                    GvRow.Cells["RateDay"].Value = r["RateDay"].ToString();
                    GvRow.Cells["PercentOrder"].Value = r["PercentOrder"].ToString();

                }
            }
            GV.Columns["OrderDate"].Visible = false;
            GV.Columns["Quantity"].Visible = false;
            GV.Columns["Price"].Visible = false;
            GV.Columns["TotalBasket"].Visible = false;
            GV.Columns["TotalKg"].Visible = false;
            GV.Columns["TotalTime"].Visible = false;
            GV.Columns["RateOrder"].Visible = false;
            GV.Columns["RateDay"].Visible = false;
            GV.Columns["PercentOrder"].Visible = false;
            GV.ClearSelection();
        }
        //Layout Gidview chi tiết hệ số
        private void InitLayout_GVRate(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "#");
            GV.Columns.Add("CategoryName", "Tên Hệ Số");
            GV.Columns.Add("Rate", "Hệ Số");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["CategoryName"].Width = 100;
            GV.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["CategoryName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Rate"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVRate(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    string RateName = r["RateName"].ToString();
                    float RateValue = 0;
                    if (float.TryParse(r["Rate"].ToString(), out RateValue))
                    {

                    }

                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["CategoryName"].Value = RateName;
                    GvRow.Cells["Rate"].Value = RateValue.Ton2String();
                    //GvRow.Cells["Rate"].Value = RateValue.ToString("0.00");
                }
            }
            GV.ClearSelection();
        }
        //Layout Gidview chi tiết nhân viên
        private void InitLayout_GVData(DataGridView GV)
        {
            DataGridViewCheckBoxColumn zColumn;
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeID", "Mã NV");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("Kilogram", "SL nguyên Liệu");
            GV.Columns.Add("Basket", "Số Rổ");
            GV.Columns.Add("Time", "Thời Gian");
            GV.Columns.Add("KgProduct", "SL thành Phẩm");
            GV.Columns.Add("Money", "Tiền");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Borrow";
            zColumn.HeaderText = "Mượn Người";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Private";
            zColumn.HeaderText = "Ăn Riêng";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Share";
            zColumn.HeaderText = "Ăn Chung";
            GV.Columns.Add(zColumn);

            GV.Columns.Add("Gender", "Nam/Nữ");
            GV.Columns.Add("Message", "Thông Báo");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EmployeeID"].Width = 70;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 170;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;
            GV.Columns["EmployeeName"].ReadOnly = true;

            GV.Columns["Basket"].Width = 80;
            GV.Columns["Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Basket"].ReadOnly = true;

            GV.Columns["Kilogram"].Width = 120;
            GV.Columns["Kilogram"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Kilogram"].ReadOnly = true;

            GV.Columns["KgProduct"].Width = 120;
            GV.Columns["KgProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KgProduct"].ReadOnly = true;


            GV.Columns["Time"].Width = 80;
            GV.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Time"].ReadOnly = true;

            GV.Columns["Money"].Width = 120;
            GV.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Money"].ReadOnly = true;

            GV.Columns["Borrow"].Width = 90;
            GV.Columns["Borrow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Borrow"].ReadOnly = true;

            GV.Columns["Private"].Width = 80;
            GV.Columns["Private"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Private"].ReadOnly = true;

            GV.Columns["Share"].Width = 80;
            GV.Columns["Share"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Share"].ReadOnly = true;

            GV.Columns["Gender"].Visible = false;
            GV.Columns["Gender"].Width = 0;
            GV.Columns["Gender"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Gender"].ReadOnly = true;

            GV.Columns["Message"].Width = 120;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Message"].ReadOnly = true;

        }
        private void InitData_GVData(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    GV.Rows.Add();
                    DataGridViewRow nRowView = GV.Rows[i];

                    DataRow r = Table.Rows[i];
                    nRowView.Cells["No"].Value = (i + 1).ToString();
                    nRowView.Cells["EmployeeID"].Tag = r["EmployeeKey"].ToString().Trim();
                    nRowView.Cells["EmployeeID"].Value = r["EmployeeID"].ToString().Trim();
                    nRowView.Cells["EmployeeName"].Value = r["EmployeeName"].ToString().Trim();
                    nRowView.Cells["Basket"].Value = r["Basket"].Ton2String();
                    nRowView.Cells["Kilogram"].Value = r["Kilogram"].Ton2String();
                    nRowView.Cells["KgProduct"].Value = r["KgProduct"].Ton2String();
                    nRowView.Cells["Time"].Value = r["Time"].Ton2String();
                    nRowView.Cells["Money"].Value = r["Money"].Ton2String();
                    if (r["Borrow"].ToInt() == 1)
                    {
                        nRowView.DefaultCellStyle.BackColor = Color.LightBlue;
                        nRowView.Cells["Borrow"].Value = true;
                    }
                    if (r["Private"].ToInt() == 1)
                    {
                        nRowView.Cells["Private"].Value = true;
                    }
                    if (r["Share"].ToInt() == 1)
                    {
                        nRowView.Cells["Share"].Value = true;
                    }
                    nRowView.Cells["Gender"].Value = r["Gender"].ToString();
                }

                GV.Rows.Add();
            }
        }
        private void InitData_GVSum(DataGridView GV)
        {
            float totalBasket = 0;
            float totalKilogram = 0;
            float totaltime = 0;
            float totalkgProduct = 0;
            float totalmoney = 0;

            int no = GV.Rows.Count - 1;
            GV.Rows[no].Cells["No"].Value = "";
            GV.Rows[no].Cells["EmployeeID"].Value = "";
            GV.Rows[no].Cells["EmployeeName"].Value = "TỔNG";

            for (int k = 0; k < GV.Rows.Count - 1; k++)
            {
                float zBasket = 0;
                float zKilogram = 0;
                float ztime = 0;
                float zkgProduct = 0;
                float zmoney = 0;

                if (float.TryParse(GV.Rows[k].Cells["Basket"].Value.ToString(), out zBasket))
                {

                }
                totalBasket += zBasket;
                if (float.TryParse(GV.Rows[k].Cells["Kilogram"].Value.ToString(), out zKilogram))
                {

                }
                totalKilogram += zKilogram;
                if (float.TryParse(GV.Rows[k].Cells["Time"].Value.ToString(), out ztime))
                {

                }
                totaltime += ztime;
                if (float.TryParse(GV.Rows[k].Cells["KgProduct"].Value.ToString(), out zkgProduct))
                {

                }
                totalkgProduct += zkgProduct;
                if (float.TryParse(GV.Rows[k].Cells["Money"].Value.ToString(), out zmoney))
                {

                }
                totalmoney += zmoney;
            }

            GV.Rows[no].Cells["Basket"].Value = totalBasket.Ton1String();
            GV.Rows[no].Cells["Kilogram"].Value = totalKilogram.Ton1String();
            GV.Rows[no].Cells["Time"].Value = totaltime.Ton2String();
            GV.Rows[no].Cells["KgProduct"].Value = totalkgProduct.Ton2String();
            GV.Rows[no].Cells["Money"].Value = totalmoney.Ton1String();
            GV.Rows[no].DefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        #endregion

        #region[Năng suất tại tổ]
        private void Calculator_Order(DataGridViewRow nRow) //tính toán số liệu đơn hàng, công nhân
        {

            string zOrderID = nRow.Cells["OrderID"].Value.ToString();
            DateTime zOrderDate = Convert.ToDateTime(nRow.Cells["OrderDate"].Value.ToString());
            float zQuantity = 0;
            if (float.TryParse(nRow.Cells["Quantity"].Value.ToString(), out zQuantity))
            {

            }
            float zPrice = 0;
            if (float.TryParse(nRow.Cells["Price"].Value.ToString(), out zPrice))
            {

            }
            float zTotalBasket = 0;
            if (float.TryParse(nRow.Cells["TotalBasket"].Value.ToString(), out zTotalBasket))
            {

            }
            float zTotalKg = 0;
            if (float.TryParse(nRow.Cells["TotalKg"].Value.ToString(), out zTotalKg))
            {

            }
            float zTotalTime = 0;
            if (float.TryParse(nRow.Cells["TotalTime"].Value.ToString(), out zTotalTime))
            {

            }
            float zRateOrder = 0;
            if (float.TryParse(nRow.Cells["RateOrder"].Value.ToString(), out zRateOrder))
            {

            }
            float zRateDay = 0;
            if (float.TryParse(nRow.Cells["RateDay"].Value.ToString(), out zRateDay))
            {

            }
            float zPercentFinish = 0;
            if (float.TryParse(nRow.Cells["PercentOrder"].Value.ToString(), out zPercentFinish))
            {

            }

            #region [---------   Chuẩn bị các tham số   -----------] 

            float zTempMoney = 0;   //code không rõ có sử dụng hay không
            float zTempBasket = 0;
            float zTempTime = 0;
            float zTempKg = 0;

            #region Cách tính năng xuất ngày 21/02/2020 3 trường hợp Fix hệ số chiều 21/02/2020
            if (zTotalKg > 0)
            {
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateDay;
                    zTempKg = zQuantity / zTotalKg;
                }
                if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalKg) * zPrice;
                    zTempKg = zQuantity / zTotalKg;
                }
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalKg) * zPrice * _DayOfWeek;
                    zTempKg = zQuantity / zTotalKg;
                }
            }
            else if (zTotalBasket > 0)
            {
                //TH 1.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateDay;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * _DayOfWeek;
                    zTempBasket = zQuantity / zTotalBasket;
                }
            }
            else if (zTotalTime > 0)
            {
                //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateDay;
                    zTempTime = zQuantity / zTotalTime;
                }
                //TH 2.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice;
                    zTempTime = zQuantity / zTotalTime;
                }
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * _DayOfWeek;
                    zTempTime = zQuantity / zTotalTime;
                }
            }
            #endregion

            #region [CLose]
            #region Cách tính năng xuất ngày 21/02/2020 3 trường hợp
            //if (zTotalKg > 0)
            //{
            //    if (zRateDay > 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * zRateDay;
            //        zTempKg = zQuantity / zTotalKg;
            //    }
            //    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder;
            //        zTempKg = zQuantity / zTotalKg;
            //    }
            //    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * 2;
            //        zTempKg = zQuantity / zTotalKg;
            //    }
            //}
            //if (zTotalBasket > 0)
            //{
            //    //TH 1.1 Nếu Ngày hiện tại là ngày lễ
            //    if (zRateDay > 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
            //        zTempBasket = zQuantity / zTotalBasket;
            //    }
            //    //TH 1.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
            //    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
            //        zTempBasket = zQuantity / zTotalBasket;
            //    }
            //    //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
            //    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
            //        zTempBasket = zQuantity / zTotalBasket;
            //    }
            //}
            //if (zTotalTime > 0)
            //{
            //    //TH 2.1 Nếu Ngày hiện tại là ngày lễ
            //    if (zRateDay > 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
            //        zTempTime = zQuantity / zTotalTime;
            //    }
            //    //TH 2.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
            //    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
            //        zTempTime = zQuantity / zTotalTime;
            //    }
            //    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
            //        zTempTime = zQuantity / zTotalTime;
            //    }
            //}
            #endregion
            //#region  //	Trường hợp 1: Chỉ có tổng số rổ, tổng số kí=0, tổng thời gian=0
            ////if (zTotalBasket > 0 &&
            ////    zTotalKg == 0 &&
            ////    zTotalTime == 0)
            ////{
            ////    //TH 1.1 Nếu Ngày hiện tại là ngày lễ
            ////    if (zRateDay > 0)
            ////    {
            ////        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
            ////        zTempBasket = zQuantity / zTotalBasket;
            ////    }
            ////    //TH 1.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
            ////    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            ////    {
            ////        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
            ////        zTempBasket = zQuantity / zTotalBasket;
            ////    }
            ////    //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
            ////    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            ////    {
            ////        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
            ////        zTempBasket = zQuantity / zTotalBasket;
            ////    }
            ////}
            //#endregion
            //#region  //	Trường hợp 2: Chỉ tổng thời gian, tổng số kí=0, tổng số rổ =0
            //if (zTotalBasket == 0 &&
            //    zTotalKg == 0 &&
            //    zTotalTime > 0)
            //{
            //    //TH 2.1 Nếu Ngày hiện tại là ngày lễ
            //    if (zRateDay > 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
            //        zTempTime = zQuantity / zTotalTime;
            //    }
            //    //TH 2.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
            //    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
            //        zTempTime = zQuantity / zTotalTime;
            //    }
            //    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
            //        zTempTime = zQuantity / zTotalTime;
            //    }
            //}
            //#endregion
            //#region  //	Trường hợp 3: Chỉ tổng số kí, tổng thời gian = 0 ,tổng số rổ = 0
            //if (zTotalBasket == 0 &&
            //   zTotalKg > 0 &&
            //   zTotalTime == 0)
            //{
            //    if (zRateDay > 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
            //        zTempKg = zQuantity / zTotalTime;

            //    }
            //    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
            //        zTempKg = zQuantity / zTotalTime;

            //    }
            //    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
            //        zTempKg = zQuantity / zTotalTime;

            //    }
            //}
            //#endregion
            //#region  //	Trường hợp 4: tổng số kí >0, tổng thời gian >0 ĐÃ EDIT NGÀY 19/02/2020 HỌP TẠI VP WESTFOOD, EDIT NGƯỢC LẠI BAN ĐẦU NGÀY 21/02/2020
            ////if (zTotalKg > 0 && zTotalTime > 0)
            ////{
            ////if (zRateDay > 0)
            ////{
            ////    zTempMoney = (zTotalKg / zQuantity) * zPrice * zRateOrder * zRateDay;
            ////    zTempKg = zTotalKg / zQuantity;
            ////}
            ////if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            ////{
            ////    zTempMoney = (zTotalKg / zQuantity) * zPrice * zRateOrder;
            ////    zTempKg = zTotalKg / zQuantity;
            ////}
            ////if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            ////{
            ////    zTempMoney = (zTotalKg / zQuantity) * zPrice * zRateOrder * 2;
            ////    zTempKg = zTotalKg / zQuantity;
            ////}
            ////}

            //if (zTotalKg > 0 && zTotalTime > 0)
            //{
            //    if (zRateDay > 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * zRateDay;
            //        zTempKg = (zQuantity / zTotalKg);
            //    }
            //    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder;
            //        zTempKg = (zQuantity / zTotalKg);
            //    }
            //    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        zTempMoney = (zQuantity / zTotalKg) * zPrice * zRateOrder * 2;
            //        zTempKg = (zQuantity / zTotalKg);
            //    }
            //}
            //#endregion
            //#region //	Trường hợp 5:  tổng số rổ >0, tổng thời gian >0 
            //if (zTotalBasket > 0 && zTotalTime > 0)
            //{
            //    if (zRateDay > 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
            //        zTempBasket = zQuantity / zTotalBasket;
            //    }
            //    if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            //    {
            //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
            //        zTempBasket = zQuantity / zTotalBasket;
            //    }
            //    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
            //    {
            //        zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
            //        zTempBasket = zQuantity / zTotalBasket;
            //    }
            //}
            //#endregion
            #endregion

            #endregion

            for (int i = 0; i < GVEmployee.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVEmployee.Rows[i];
                int Gender = r.Cells["Gender"].Value.ToInt(); //Nam nu
                float Time = 0;
                if (float.TryParse(r.Cells["Time"].Value.ToString(), out Time))
                {
                    //Số giờ 
                }
                float Kilogram = 0;
                if (float.TryParse(r.Cells["Kilogram"].Value.ToString(), out Kilogram))
                {
                    //Số kí 
                }
                float Basket = 0;
                if (float.TryParse(r.Cells["Basket"].Value.ToString(), out Basket))
                {
                    //Số rỗ 
                }
                float ResultKg = 0;     //ket quả số kg
                float ResultMoney = 0;// kết quả số tiền

                if (zPrice > 0)
                {
                    //TH 1.1 Số giờ >0 và số rổ=0 và số kí=0
                    if (Time > 0 && Basket == 0 && Kilogram == 0)
                    {
                        //TH 1.1.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempTime * Time).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateDay * zRateOrder);
                        }
                        //TH 1.1.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempTime * Time).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder);
                        }
                        //TH 1.1.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempTime * Time).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder * _DayOfWeek);
                        }
                    }
                    //TH 1.2 (Số giờ >0 và số rổ=0 ) hoặc (Số giờ ==0 và số kí>0)
                    if (Time > 0 && Basket > 0 || Time == 0 && Basket > 0)
                    {
                        //TH 1.2.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempBasket * Basket).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder * zRateDay);
                        }
                        //TH 1.2.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempBasket * Basket).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder);
                        }
                        //TH 1.2.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempBasket * Basket).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * _DayOfWeek * zRateOrder);
                        }
                    }
                    //TH 1.3 (Số giờ >0 và Số kí >0)
                    if (Time > 0 && Kilogram > 0)
                    {
                        //TH 1.3.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = 0;
                            if (float.TryParse((zTempKg * Kilogram).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateDay * zRateOrder);
                        }
                        //TH 1.3.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (zOrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            if (float.TryParse((zTempKg * Kilogram).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * zRateOrder);
                        }
                        //TH 1.3.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            if (float.TryParse((zTempKg * Kilogram).ToString(), out ResultKg))
                            {

                            }
                            ResultMoney = (ResultKg * zPrice * _DayOfWeek * zRateOrder);
                        }
                    }
                }
                else
                {
                    int _Hour = HoTroSanXuat.LaySoGioLamHanhChanh(dte_Date.Value);     //thời gian làm việc hành chính
                    float _MMoney = HoTroSanXuat.LayDonGiaNam(dte_Date.Value);      //tiền danh cho công nhân nam
                    float _FMoney = HoTroSanXuat.LayDonGiaNu(dte_Date.Value);       //tiền danh cho công nhân nữ
                    float MoneyTime = 0;
                    if (Gender == 0)
                    {
                        MoneyTime = (Time / _Hour) * _MMoney;
                    }
                    else
                    {
                        MoneyTime = (Time / _Hour) * _FMoney;
                    }
                    ResultMoney = MoneyTime;
                }

                ///bổ sung thêm nhân % hoàn thành
                ResultMoney = (ResultMoney * zPercentFinish) / 100;
                GVEmployee.Rows[i].Cells["KgProduct"].Value = ResultKg.Ton2String();
                GVEmployee.Rows[i].Cells["Money"].Value = ResultMoney.Ton2String();
                GVEmployee.Rows[i].Cells["Message"].Value = "Đã tính xong";
            }
        }
        //lưu thông tin đã tính toán
        private void SaveResult_Order()
        {
            DateTime zOrderDate = Convert.ToDateTime(GVOrder.CurrentRow.Cells["OrderDate"].Value.ToString());

            for (int i = 0; i < GVEmployee.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVEmployee.Rows[i];
                int Borrow = 0;   //mượn
                int Private = 0;  //ăn riêng
                int Share = 0; //ăn chung
                if (r.Cells["Borrow"].Value != null)
                    Borrow = 1;
                if (r.Cells["Private"].Value != null)
                    Private = 1;
                if (r.Cells["Share"].Value != null)
                    Share = 1;
                float Time = 0;
                if (float.TryParse(r.Cells["Time"].Value.ToString(), out Time))
                {
                    //Số giờ 
                }
                float Basket = 0;
                if (float.TryParse(r.Cells["Basket"].Value.ToString(), out Basket))
                {
                    //Số rỗ 
                }
                float ResultKg = 0;
                if (float.TryParse(r.Cells["KgProduct"].Value.ToString(), out ResultKg))
                {
                    //ket quả số kg
                }
                float ResultMoney = 0;
                if (float.TryParse(r.Cells["Money"].Value.ToString(), out ResultMoney))
                {
                    // kết quả số tiền
                }
                string EmployeeKey = r.Cells["EmployeeID"].Tag.ToString();
                string EmployeeID = r.Cells["EmployeeID"].Value.ToString();
                string EmployeeName = r.Cells["EmployeeName"].Value.ToString();


                Order_Money_Model _OrderMoney = new Order_Money_Model();
                _OrderMoney.OrderDate = zOrderDate;
                _OrderMoney.EmployeeKey = EmployeeKey;      //**KEY UDPATE**
                _OrderMoney.EmployeeID = EmployeeID;
                _OrderMoney.EmployeeName = EmployeeName;
                _OrderMoney.StageKey = _WorkKey;
                _OrderMoney.TeamKey = _TeamKey;
                _OrderMoney.OrderKey = _OrderKey;               //**KEY UDPATE**

                //-----KHÔNG MƯỢN, HOẶC ĂN CHUNG TẤT CẢ (TÌNH HUỐNG MẶT ĐỊNH)
                if ((Borrow == 0 && Private == 0 && Share == 0) ||
                    (Borrow == 0 && Private == 0 && Share == 1))
                {
                    _OrderMoney.Time = Time;
                    _OrderMoney.Kg = ResultKg;
                    _OrderMoney.Money = ResultMoney;
                    _OrderMoney.Basket = Basket;

                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;
                    _OrderMoney.Basket_Private = 0;

                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;
                    _OrderMoney.Basket_Borrow = 0;

                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 1;//**Lưu ý**
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 0;
                }
                //-----KHÔNG MƯỢN MÀ ĂN RIÊNG
                if (Borrow == 0 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = Basket;
                    _OrderMoney.Time_Private = Time;
                    _OrderMoney.Kg_Private = ResultKg;
                    _OrderMoney.Money_Private = ResultMoney;

                    _OrderMoney.Basket_Borrow = 0;
                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;

                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 1;//**Lưu ý**
                }
                //-----MƯỢN MÀ ĂN RIÊNG
                if (Borrow == 1 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = 0;
                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;

                    _OrderMoney.Basket_Borrow = Basket;
                    _OrderMoney.Time_Borrow = Time;
                    _OrderMoney.Kg_Borrow = ResultKg;
                    _OrderMoney.Money_Borrow = ResultMoney;

                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 1;//**Lưu ý**
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 0;
                }
                //-----MƯỢN MÀ ĂN CHUNG
                if (Borrow == 1 && Private == 0 && Share == 1)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = 0;
                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;

                    _OrderMoney.Basket_Borrow = 0;
                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;

                    _OrderMoney.Money_Eat = ResultMoney;
                    _OrderMoney.Time_Eat = Time;
                    _OrderMoney.Kg_Eat = ResultKg;
                    _OrderMoney.Basket_Eat = Basket;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 1; //**Lưu ý**
                    _OrderMoney.Category_Private = 0;
                }

                //----- LƯU DB
                Order_Exe OrderExe = new Order_Exe();
                OrderExe.OrderMoney = _OrderMoney;
                OrderExe.CreatedBy = SessionUser.UserLogin.Key;
                OrderExe.ModifiedBy = SessionUser.UserLogin.Key;
                OrderExe.CreatedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.ModifiedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.Save();

                if (OrderExe.Message != "11" &&
                    OrderExe.Message != "20")
                    r.Cells["Message"].Value = OrderExe.Message;
                else
                    r.Cells["Message"].Value = "Đã cập nhật";

                //----- 
                Order_Employee_Info zEmployee = new Order_Employee_Info();
                zEmployee.EmployeeKey = EmployeeKey;
                zEmployee.OrderKey = _OrderKey;
                zEmployee.Money = ResultMoney;
                zEmployee.UpdateMoney();

                //SCCĐ và SCĐH
                if (_TeamKey == _TeamSCCD ||
                    _TeamKey == _TeamSCDH)
                {
                    int zCount = OrderExe.TimeExits();
                    Money_TimeKeep_Info zInfo = new Money_TimeKeep_Info();

                    zInfo.OrderKey = _OrderKey;
                    zInfo.DateTimeKeep = _OrderMoney.OrderDate;
                    zInfo.EmployeeKey = _OrderMoney.EmployeeKey;
                    zInfo.EmployeeID = _OrderMoney.EmployeeID;
                    zInfo.EmployeeName = _OrderMoney.EmployeeName;
                    zInfo.MoneyTimeKeep = ResultMoney;

                    if (zCount == 0)
                    {
                        zInfo.CreatedBy = SessionUser.UserLogin.Key;
                        zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                        zInfo.Create();
                    }
                    else
                    {
                        zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                        zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                        zInfo.Update();
                    }
                }
                //----- 
            }
        }
        #endregion
        //đông 17/09/2020 không thấy có trường hợp này nên chưa sữa hàm parse
        #region[Năng suất công việc khác]
        private void Calculator_Order_Dif(DataGridViewRow nRow)
        {
            string zOrderID = nRow.Cells["OrderID"].Value.ToString();
            DateTime zOrderDate = Convert.ToDateTime(nRow.Cells["OrderDate"].Value.ToString());
            float zQuantity = 0;
            if (float.TryParse(nRow.Cells["Quantity"].Value.ToString(), out zQuantity))
            {

            }
            float zPrice = 0;
            if (float.TryParse(nRow.Cells["Price"].Value.ToString(), out zPrice))
            {

            }
            float zTotalBasket = 0;
            if (float.TryParse(nRow.Cells["TotalBasket"].Value.ToString(), out zTotalBasket))
            {

            }
            float zTotalKg = 0;
            if (float.TryParse(nRow.Cells["TotalKg"].Value.ToString(), out zTotalKg))
            {

            }
            float zTotalTime = 0;
            if (float.TryParse(nRow.Cells["TotalTime"].Value.ToString(), out zTotalTime))
            {

            }
            float zRateOrder = 0;
            if (float.TryParse(nRow.Cells["RateOrder"].Value.ToString(), out zRateOrder))
            {

            }
            float zRateDay = 0; 
            if (float.TryParse(nRow.Cells["RateDay"].Value.ToString(), out zRateDay))
            {

            }
            // [---------   Chuẩn bị các tham số   -----------] 
            // tính toán phải làm tròn 2 chữ số thập phân n2
            float zTempMoney = 0;   // số tiền tb 1 rỗ
            float zTempBasket = 0;// sô kí tb 1 rỗ
            float zTempTime = 0; //số kg tb trong 1 giờ

            if (zTotalBasket > 0)
            {
                //TH 1.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.2 Nếu Ngày hiện tại là ngày thường
                if (zRateDay == 0 && zOrderDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.3 Nếu Ngày hiện tại là ngày chủ nhật
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * _DayOfWeek;
                    zTempBasket = zQuantity / zTotalBasket;
                }
            }
            else if (zTotalTime > 0)
            { //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                    zTempTime = zQuantity / zTotalTime;
                }
                //TH 2.2 Nếu Ngày hiện tại là ngày thường
                if (zRateDay == 0 && zOrderDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                    zTempTime = zQuantity / zTotalTime;
                }
                //TH 2.3 Nếu Ngày hiện tại là ngày chủ nhật
                if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * _DayOfWeek;
                    zTempTime = zQuantity / zTotalTime;
                }
            }
            //Làm tròn 2 chữ số thập phân
            zTempMoney = float.Parse(Math.Round(zTempMoney, 2).ToString());
            zTempTime = float.Parse(Math.Round(zTempTime, 2).ToString());
            zTempBasket = float.Parse(Math.Round(zTempBasket, 2).ToString());

            for (int i = 0; i < GVEmployee.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVEmployee.Rows[i];
                float Time = 0;
                if(float.TryParse(r.Cells["Time"].ToString(),out Time))
                {
                    //Số giờ 
                }
                float Kilogram = 0;
                if (float.TryParse(r.Cells["Kilogram"].ToString(), out Kilogram))
                {
                    //Số kí 
                }
                float Basket = 0;
                if (float.TryParse(r.Cells["Basket"].ToString(), out Kilogram))
                {
                    //Số rỗ 
                }   

                float ResultKg = 0;     //ket quả số kg
                float ResultMoney = 0;// kết quả số tiền

                if (Kilogram > 0)
                {
                    ResultMoney = Kilogram * zTempMoney;
                }
                if (Basket > 0)
                {
                    ResultKg = Basket * zTempBasket;
                    ResultMoney = Basket * zTempMoney;
                }
                if (Time > 0)
                {
                    ResultMoney = Time * zTempMoney;

                }
                GVEmployee.Rows[i].Cells["Money"].Value = ResultMoney.Ton0String();
                GVEmployee.Rows[i].Cells["Kilogram"].Value = Basket * ResultKg;
                GVEmployee.Rows[i].Cells["Message"].Value = "Đã tính xong";
            }
        }
        private void SaveResult_Order_Dif()
        {
            DateTime zOrderDate = Convert.ToDateTime(GVOrder.CurrentRow.Cells["OrderDate"].Value.ToString());
            float zRateDay = 0;
            if (float.TryParse(GVOrder.CurrentRow.Cells["RateDay"].Value.ToString(), out zRateDay))
            {
                //Số kí 
            }

            for (int i = 0; i < GVEmployee.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVEmployee.Rows[i];
                int Borrow = 0;   //mượn
                int Private = 0;  //ăn riêng
                int Share = 0; //ăn chung

                if (r.Cells["Borrow"].Value != null)
                    Borrow = 1;
                if (r.Cells["Private"].Value != null)
                    Private = 1;
                if (r.Cells["Share"].Value != null)
                    Share = 1;
                float zResultMoney = 0;
                if(float.TryParse(r.Cells["Money"].Value.ToString(),out zResultMoney))
                {
                    // kết quả số tiền
                }
                string zEmployeeKey = r.Cells["EmployeeID"].Tag.ToString();
                string zEmployeeID = r.Cells["EmployeeID"].Value.ToString();
                string zEmployeeName = r.Cells["EmployeeName"].Value.ToString();


                Order_Money_Model _OrderMoney = new Order_Money_Model();
                _OrderMoney.OrderDate = zOrderDate;
                _OrderMoney.EmployeeKey = zEmployeeKey;      //**KEY UDPATE**
                _OrderMoney.EmployeeID = zEmployeeID;
                _OrderMoney.EmployeeName = zEmployeeName;
                _OrderMoney.StageKey = _WorkKey;
                _OrderMoney.TeamKey = _TeamKey;
                _OrderMoney.OrderKey = _OrderKey;

                if (Borrow == 1 && Private == 1 && Share == 0)  //Trường hợp 1 :MƯỢN VÀ ĂN RIÊNG
                {
                    //TH 1.1 ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        _OrderMoney.Money_Dif_Holiday = zResultMoney;
                    }
                    //TH 1.1 ngày hiện tại là ngày thường
                    if (zRateDay == 0 && zOrderDate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif = zResultMoney;
                    }
                    //TH 1.3 ngày hiện tại là ngày chủ nhật
                    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_Sunday = zResultMoney;
                    }
                }
                if (Borrow == 1 && Private == 0 && Share == 1)  //Trường hợp 1 :MƯỢN VÀ ĂN CHUNG
                {
                    //TH 2.1 ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        _OrderMoney.Money_Dif_Holiday_General = zResultMoney;
                    }
                    //TH 2.1 ngày hiện tại là ngày thường
                    if (zRateDay == 0 && zOrderDate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_General = zResultMoney;
                    }
                    //TH 2.3 ngày hiện tại là ngày chủ nhật
                    if (zOrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_Sunday_General = zResultMoney;
                    }
                }

                Order_Exe OrderExe = new Order_Exe();
                OrderExe.OrderMoney = _OrderMoney;
                OrderExe.CreatedBy = SessionUser.UserLogin.Key;
                OrderExe.ModifiedBy = SessionUser.UserLogin.Key;
                OrderExe.CreatedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.ModifiedName = SessionUser.UserLogin.EmployeeName;
                OrderExe.Save();

                if (OrderExe.Message != "11" &&
                 OrderExe.Message != "20")
                    r.Cells["Message"].Value = OrderExe.Message;
                else
                    r.Cells["Message"].Value = "Đã cập nhật";
            }
        }
        #endregion

        private void Btn_Count_Click(object sender, EventArgs e)
        {
            if (_OrderKey != "")
            {
                if (_TeamKey == 98)
                {
                    Calculator_Order_Dif(GVOrder.CurrentRow);
                }
                else
                {
                    Calculator_Order(GVOrder.CurrentRow);
                }
                InitData_GVSum(GVEmployee);
            }
            else
            {
                MessageBox.Show("Bạn phải chọn 1 đơn hàng !.");
            }

        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (_OrderKey != "")
            {
                if (_TeamKey == 98)
                {
                    SaveResult_Order_Dif();
                }
                else
                {
                    SaveResult_Order();
                }
            }
            else
            {
                MessageBox.Show("Bạn phải chọn 1 đơn hàng !.");
            }
            Cursor = Cursors.Default;
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [Show Hide]      
        private void btnShowInfo_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = true;
            btnHideInfo.Visible = true;
            btnShowInfo.Visible = false;
        }

        private void btnHideInfo_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = false;
            btnHideInfo.Visible = false;
            btnShowInfo.Visible = true;
        }
        #endregion
    }
}