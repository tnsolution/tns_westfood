﻿using System.Collections.Generic;
using System.Data;

namespace TNS.FIN.Receipt
{
    public class Receipt_Object : Receipt_Info
    {
        private List<Receipt_Detail_Info> _List_Detail;
        public List<Receipt_Detail_Info> List_Detail
        {
            get
            {
                return _List_Detail;
            }
            set
            {
                _List_Detail = value;
            }
        }
        public Receipt_Object() : base() {
            List_Detail = new List<Receipt_Detail_Info>();
        }
        public Receipt_Object(string ReceiptKey) : base(ReceiptKey)
        {
            DataTable zTable = Receipt_Detail_Data.List_Detail(base.Key);
            List_Detail = new List<Receipt_Detail_Info>();
            foreach (DataRow nRow in zTable.Rows)
            {
                Receipt_Detail_Info zInfo = new Receipt_Detail_Info(nRow);
                List_Detail.Add(zInfo);
            }
        }
        public void Save_Object()
        {
            base.Save();
            if (base.HasInfo && base.IsCommandOK)
            {
                for (int i = 0; i < List_Detail.Count; i++)
                {
                    Receipt_Detail_Info zInfo = new Receipt_Detail_Info();
                    zInfo = List_Detail[i];
                    zInfo.CreatedBy = base.CreatedBy;
                    zInfo.ModifiedName = base.ModifiedName;
                    zInfo.ModifiedBy = base.ModifiedBy;
                    zInfo.ModifiedName = base.ModifiedName;
                    zInfo.ReceiptKey = base.Key;
                    switch (zInfo.RecordStatus)
                    {
                        case 0:
                            break;
                        case 1:
                            zInfo.Create();
                            break;
                        case 2:
                            zInfo.Update();
                            break;
                        case 99:
                            zInfo.Delete();
                            break;
                    }

                    base.Message += zInfo.Message;
                }
            }
        }
        public void Delete_Object()
        {
            base.Delete();
            if (base.IsCommandOK)
            {
                Receipt_Detail_Info zInfo = new Receipt_Detail_Info();
                zInfo.ModifiedBy = base.ModifiedBy;
                zInfo.ModifiedName = base.ModifiedName;
                zInfo.ReceiptKey = base.Key;
                zInfo.DeleteAll();
                
            }
        }
    }
}
