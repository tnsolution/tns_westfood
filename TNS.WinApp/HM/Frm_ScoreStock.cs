﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_ScoreStock : Form
    {
        private int _TeamKeyClose = 0;
        private DateTime _DateClose;
        public Frm_ScoreStock()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Caculator.Click += Btn_Caculator_Click;
            //GVData.DoubleClick += GVData_DoubleClick;
            btn_Done.Click += Btn_Done_Click;

            dteDate.Value = SessionUser.Date_Work;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            string zSQL = @"SELECT A.TeamKey, A.TeamID + ' - ' + A.TeamName AS TeamName FROM SYS_Team A
WHERE A.RecordStatus <> 99
AND A.TeamKey NOT IN (SELECT TeamKey FROM SYS_Team
WHERE RecordStatus <> 99 AND BranchKey = 4 AND DepartmentKey != 26)
ORDER BY A.Rank";
            LoadDataToToolbox.KryptonComboBox(cbo_TeamID_Search, zSQL, "---Tất cả---");
        }

        private void Btn_Caculator_Click(object sender, EventArgs e)
        {
            int no = 0;
            foreach (DataGridViewRow nRowView in GVData.Rows)
            {
                float zStockBegin = 0;
                zStockBegin = nRowView.Cells["StockBegin"].Value.ToFloat();
                nRowView.Cells["StockBegin"].Value = zStockBegin.Ton1String();
                float zNCQD = 0;
                zNCQD = nRowView.Cells["NCQD"].Value.ToFloat();
                nRowView.Cells["NCQD"].Value = zNCQD.Ton1String();
                float zNCTT = 0;
                zNCTT = nRowView.Cells["NCTT"].Value.ToFloat();
                nRowView.Cells["NCTT"].Value = zNCTT.Ton1String();
                float zOverTime = 0;

                zOverTime = zNCTT - zNCQD;
                nRowView.Cells["OverTime"].Value = zOverTime.Ton1String();

                float zEdit = 0;
                zEdit = nRowView.Cells["Edit"].Value.ToFloat();
                nRowView.Cells["Edit"].Value = zEdit.Ton1String();

                float zStockEnd = 0;
                zStockEnd = zOverTime + zEdit;
                if (zStockEnd < 0)
                    zStockEnd = 0;
                nRowView.Cells["StockEnd"].Value = zStockEnd.Ton1String();



                //double zTotal = 0;
                //for (int i = 3; i < zTable.Columns.Count; i++)
                //{
                //    double zrow = nRow[i].ToDouble();
                //    nRowView.Cells[i + 1].Value = zrow.Ton1String();
                //    if (nRow[i].ToString() != "")
                //    {
                //        zTotal += nRow[i].ToDouble();
                //    }
                //}
                //  nRowView.Cells["Total"].Value = zTotal.Ton0String();
                no++;
            }
        }

        private void Frm_ScoreStock_Load(object sender, EventArgs e)
        {

        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {

            //if (GVData.Rows.Count > 0)
            //{
            //    if (MessageBox.Show("Bạn có chắc chốt dữ liệu này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
            //    {
            //        Score_Stock_Info zInfo;
            //        int no = 0;
            //        string zMessage = "";
            //        zInfo = new Score_Stock_Info();
            //        zInfo.DeleteEmpty(_TeamKeyClose,_DateClose);
            //        zMessage += zInfo.Message;
            //        foreach (DataGridViewRow nRowView in GVData.Rows)
            //        {
            //            if (nRowView.Cells["No"].Tag != null)
            //            {
            //                string zEmployeeKey = nRowView.Cells["No"].Tag.ToString().Trim();
            //                zInfo = new Score_Stock_Info(zEmployeeKey, _DateClose);
            //                zInfo.DateWrite = _DateClose;
            //                zInfo.EmployeeKey = zEmployeeKey;
            //                zInfo.EmployeeID = nRowView.Cells["EmployeeID"].Value.ToString().Trim();
            //                zInfo.EmployeeName = nRowView.Cells["EmployeeName"].Value.ToString().Trim();

            //                Employee_Info zEmp = new Employee_Info(zEmployeeKey);
            //                zInfo.TeamKey = zEmp.TeamKey;
            //                zInfo.DepartmentKey = zEmp.DepartmentKey;
            //                zInfo.Number = nRowView.Cells["StockEnd"].Value.ToFloat();
            //                zInfo.CreatedBy = SessionUser.UserLogin.Key;
            //                zInfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            //                zInfo.ModifiedBy = SessionUser.UserLogin.Key;
            //                zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            //                zInfo.Save(); //lưu cha
            //                if (zInfo.Message != string.Empty)
            //                {
            //                    zMessage += zInfo.Message;
            //                }
            //            }
            //            no++;
            //        }
            //        if(zMessage==string.Empty)
            //        {
            //            MessageBox.Show("Chốt thành công!");
            //        }
            //        else
            //        {
            //            MessageBox.Show(zMessage);
            //        }
            //    }
               
            //}
            //else
            //{
            //    MessageBox.Show("Không tìm thấy dữ liệu");
            //}

        }
        private void DisplayData()
        {

            DataTable _InTable = Score_Stock_Data.Search_List(_TeamKeyClose, dteDate.Value);
            if (_InTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(GVData, _InTable);
                    InitGV_Data(GVData, _InTable);
                }));
            }

        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            _TeamKeyClose = cbo_TeamID_Search.SelectedValue.ToInt();
            _DateClose = dteDate.Value;
            GVData.Rows.Clear();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        void InitGV_Layout(DataGridView GV, DataTable Table)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "HỌ VÀ TÊN");
            GV.Columns.Add("EmployeeID", "SỐ THẺ");
            GV.Columns.Add("TeamID", "Tổ nhóm");
            GV.Columns.Add("StockBegin", "Công tháng trước");
            GV.Columns.Add("NCQD", "Công quy định");
            GV.Columns.Add("NCTT", "Công thực tế");
            GV.Columns.Add("OverTime", "Công thừa- thiếu");
            GV.Columns.Add("Edit", "Công Xét duyệt");
            GV.Columns.Add("StockEnd", "Tồn công ");
            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 200;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 80;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["TeamID"].Width = 80;
            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamID"].Frozen = true;

            GV.Columns["StockBegin"].Width = 100;
            GV.Columns["StockBegin"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["NCQD"].Width = 100;
            GV.Columns["NCQD"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["NCTT"].Width = 100;
            GV.Columns["NCTT"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["OverTime"].Width = 100;
            GV.Columns["OverTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Edit"].Width = 100;
            GV.Columns["Edit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["StockEnd"].Width = 100;
            GV.Columns["StockEnd"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.ColumnHeadersHeight = 45;
            #endregion
        }
        void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GVData.Rows.Clear();
            foreach (DataRow nRow in zTable.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Cells["No"].Value = (no + 1).ToString();
                nRowView.Cells["No"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
                float zStockBegin = 0;
                zStockBegin = nRow["StockBegin"].ToFloat();
                nRowView.Cells["StockBegin"].Value = zStockBegin.Ton1String();
                float zNCQD = 0;
                zNCQD = nRow["NCQD"].ToFloat();
                nRowView.Cells["NCQD"].Value = zNCQD.Ton1String();
                float zNCTT = 0;
                zNCTT = nRow["NCTT"].ToFloat();
                nRowView.Cells["NCTT"].Value = zNCTT.Ton1String();

                float zOverTime = 0;

                zOverTime = zNCTT - zNCQD;
                nRowView.Cells["OverTime"].Value = zOverTime.Ton1String();

                float zEdit = 0;
                nRowView.Cells["Edit"].Value = zEdit.Ton1String();

                float zStockEnd = 0;
                zStockEnd = zStockBegin + zOverTime;
                if (zStockEnd < 0)
                    zStockEnd = 0;
                nRowView.Cells["StockEnd"].Value = zStockEnd.Ton1String();


                //double zTotal = 0;
                //for (int i = 3; i < zTable.Columns.Count; i++)
                //{
                //    double zrow = nRow[i].ToDouble();
                //    nRowView.Cells[i + 1].Value = zrow.Ton1String();
                //    if (nRow[i].ToString() != "")
                //    {
                //        zTotal += nRow[i].ToDouble();
                //    }
                //}
                //  nRowView.Cells["Total"].Value = zTotal.Ton0String();
                no++;
            }
            GV.Rows.Add();
            // InitSum_GVData(GV.Rows[zTable.Rows.Count], zTable);
        }


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        private void kryptonButton1_Click(object sender, EventArgs e)
        {
            Frm_ScoreBorrow frm = new Frm_ScoreBorrow();
            frm.Show();
        }
    }
}
