﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Calculator_Money : Form
    {
        /// <summary>
        /// vòng lặp timer: 0 là team, 1 là date, 2 là nhan viên
        /// </summary>
        int _Circle = 0;

        #region [Khai Báo Biến]       
        private DataTable _TableTeam;
        private int _TotalTeam = 0;
        private int _IndexTeam = 0;
        private int _TeamKey = 0;
        private string _TeamName = "";

        private DataTable _TableEmployee;
        private int _TotalEmployee = 0;
        private int _IndexEmployee = 0;

        private int _IndexDate = 0;
        private int _TotalDate = 0;

        #endregion

        float _TotalTimeTeam = 0;
        float _TotalTimeTeamShare = 0;
        float _TotalTimeTeamDifBorrowShare = 0;
        double _TotalMoneyTeam = 0;                         //Tổng Tiền Làm Tại Tổ
        double _TotalMoneyDifShare = 0;                   //Tổng Tiền Việc Khác Ăn Chung
        double _TotalMoneyTeamMedium = 0;             //Thời Gian Trung Bình Làm Việc Khác Ăn Chung
        double _Money = 0;
        List<Employee_Money_Model> _ListEmployee;

        public Frm_Calculator_Money()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DrawGVStyle(ref GVTeam);

            InitGVTeam_Layout(GVTeam);

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;

            timerTeam.Enabled = true;
            timerTeam.Stop();

            timerDate.Enabled = true;
            timerDate.Stop();

            timerEmployee.Enabled = false;
            timerEmployee.Stop();
        }
        private void Frm_Production_Money_View_Load(object sender, EventArgs e)
        {
            PicLoading.Visible = false;
            Panel_Right.Visible = false;
            btnHideLog.Visible = false;
            btnShowLog.Visible = true;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
                            WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
                            ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "---- Tất cả ----");
        }
        void LoadTeam()
        {
            if (cboTeam.SelectedValue.ToInt() > 0)
            {
                _TableTeam = HoTroSanXuat.DanhSachToNhom(cboTeam.SelectedValue.ToInt());
                _TotalTeam = _TableTeam.Rows.Count;
            }
            else
            {
                _TableTeam = HoTroSanXuat.DanhSachToNhom();
                _TotalTeam = _TableTeam.Rows.Count;
            }
        }
        void LoadEmployee(int TeamKey, string TeamName, DateTime FromDate, DateTime ToDate)
        {
            _TableEmployee = HoTroSanXuat.TinhTienDonHangCongNhan(TeamKey, FromDate, ToDate);
            _TotalEmployee = _TableEmployee.Rows.Count;

            ShowLog("\\----------------------------------------------------------------------------\\");
            ShowLog("---[" + TeamName + "] Ngày [" + FromDate.ToString("dd") + "]---");
            ShowLog("---[   Xử lý thông tin nhóm    ]---");

            if (_TableEmployee.Rows.Count > 0)
            {
                _ListEmployee = new List<Employee_Money_Model>();
                foreach (DataRow r in _TableEmployee.Rows)
                {
                    Employee_Money_Model zModel = new Employee_Money_Model();
                    //zModel.PercentOrder = r["PercentOrder"].ToFloat();
                    zModel.EmployeeID = r["EmployeeID"].ToString();
                    zModel.EmployeeKey = r["EmployeeKey"].ToString();
                    zModel.EmployeeName = r["EmployeeName"].ToString();
                    zModel.OrderDate = Convert.ToDateTime(r["OrderDate"]);
                    zModel.Slug = r["Slug"].ToInt();
                    zModel.RateDay = r["RateDay"].ToFloat();
                    zModel.TimeTeam = r["TimeTeam"].ToFloat();
                    zModel.TimeTeamPrivate = r["TimeTeamPrivate"].ToFloat();
                    zModel.TimeTeamBorrowPrivate = r["TimeTeamBorrowPrivate"].ToFloat();
                    zModel.TimeTeamBorrowShare = r["TimeTeamBorrowShare"].ToFloat();
                    zModel.TimeTeamDifBorrowShare = r["TimeTeamDifBorrowShare"].ToFloat();
                    zModel.MoneyPersonal = r["MoneyPersonal"].ToDouble(); //Tiền làm ăn chung,không mượn
                    zModel.MoneyPrivate = r["MoneyPrivate"].ToDouble();//Tiền làm ăn riêng, không mượn
                    zModel.MoneyBorrowPrivate = r["MoneyBorrowPrivate"].ToDouble();
                    zModel.MoneyBorrowShare = r["MoneyBorrowShare"].ToDouble();
                    zModel.MoneyTimePrivate = r["MoneyTimePrivate"].ToDouble();
                    zModel.MoneyTimeShare = r["MoneyTimeShare"].ToDouble();

                    _ListEmployee.Add(zModel);
                }
                InitData_Calculator();
            }
            else
            {
                ShowLog("Không có công nhân làm việc.");
            }
        }
        void ShowGVTeam(DataRow nRow, DataGridView GV)
        {
            GV.Rows.Add();
            DataGridViewRow nRowView = GV.Rows[_IndexTeam];
            nRowView.Tag = nRow["TeamKey"];
            nRowView.Cells["No"].Value = (_IndexTeam + 1).ToString();
            nRowView.Cells["TeamName"].Value = nRow["TeamName"].ToString().Trim();
            nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
        }
        void InitGVTeam_Layout(DataGridView GV)
        {
            GV.Columns.Add("No", "#");
            GV.Columns.Add("TeamID", "Mã nhóm");
            GV.Columns.Add("TeamName", "Tên nhóm");

            GV.Columns["No"].Width = 45;
            GV.Columns["TeamID"].Width = 100;
            GV.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV.ColumnHeadersHeight = 25;
            GV.FirstDisplayedScrollingRowIndex = GV.RowCount - 1;
        }
        void InitData_Calculator()
        {
            ShowLog("Chuẩn bị số liệu của nhóm.");

            #region Chuẩn bị số liệu chung của nhóm

            _TotalTimeTeam = _ListEmployee.Sum(item => item.TimeTeam);                                                  //_TableEmployee.Compute("SUM(TimeTeam)", string.Empty).ToFloat();
            _TotalTimeTeamShare = _ListEmployee.Sum(item => item.TimeTeamBorrowShare);                      // _TableEmployee.Compute("SUM(TimeTeamBorrowShare)", string.Empty).ToFloat();
            _TotalTimeTeamDifBorrowShare = _ListEmployee.Sum(item => item.TimeTeamDifBorrowShare);      //_TableEmployee.Compute("SUM(TimeTeamDifBorrowShare)", string.Empty).ToFloat();
            _TotalMoneyTeam = _ListEmployee.Sum(item => item.MoneyPersonal);                                        //_TableEmployee.Compute("SUM(MoneyPersonal)", string.Empty).ToDouble();
            _TotalMoneyDifShare = _ListEmployee.Sum(item => item.MoneyTimeShare);                                   //_TableEmployee.Compute("SUM(MoneyTimeShare)", string.Empty).ToDouble();
            _TotalMoneyTeamMedium = Math.Round((_TotalMoneyDifShare / 8), 1);

            ShowLog("Thời gian của tổ: " + _TotalTimeTeam.ToString("n2"));
            ShowLog("Thời gian làm việc khác: " + _TotalTimeTeamDifBorrowShare.ToString("n2"));
            ShowLog("Số tiền làm tại tổ: " + _TotalMoneyTeam.ToString("n2"));

            #endregion

            //---------------------------------------------------------------------------------------CHIA TIỀN ĂN CHUNG
            if (_TotalTimeTeam > 0 && _TotalMoneyDifShare > 0)
            {
                //
                //Tiền thời gian làm việc
                //tiền trung bình             
                _Money = _TotalMoneyDifShare / _TotalTimeTeam;
                ShowLog("Chia tiền số ăn chung [" + _TotalMoneyDifShare.ToString("n2") + "]");
            }
        }
        void Process_Data(Employee_Money_Model zEmp)
        {
            string Message = string.Empty;

            double zTotal = 0;
            DateTime OrderDate = zEmp.OrderDate;
            float zRateDay = zEmp.RateDay;
            //thời gian cá nhân làm được
            float Time = zEmp.TimeTeam;
            //Tổng tiền của 1 công nhân
            zTotal = zEmp.MoneyPrivate + zEmp.MoneyPersonal + zEmp.MoneyBorrowPrivate + zEmp.MoneyBorrowShare + zEmp.MoneyTimePrivate + zEmp.MoneyTimeShare;
            zEmp.MoneyTotal = zTotal;
            //lương thời gian ăn chung
            zEmp.MoneyTimeShare = _Money * Time;

            //------------------------------------------------------------------------------------------------------------------------------------------Lưu vào tiền ngày của công nhân
            Order_Money_Date_Info zMoneyDate = new Order_Money_Date_Info();
            zMoneyDate.TeamKey = _TeamKey;
            zMoneyDate.EmployeeID = zEmp.EmployeeID;
            zMoneyDate.EmployeeKey = zEmp.EmployeeKey;
            zMoneyDate.EmployeeName = zEmp.EmployeeName;
            zMoneyDate.Date = zEmp.OrderDate;

            // Đông Gộp tiền MoneyPersonal + MoneyPrivate  ngày 15/05/2020
            if (zRateDay > 0)
            {
                zMoneyDate.Money = 0;
                zMoneyDate.Money_Dif = 0;
                zMoneyDate.Money_Sundays = 0;
                zMoneyDate.Money_Dif_Sunday = 0;
                zMoneyDate.Money_Holidays = zEmp.MoneyPersonal +zEmp.MoneyPrivate;
                zMoneyDate.Money_Dif_Holiday = zEmp.MoneyBorrowPrivate + zEmp.MoneyBorrowShare + zEmp.MoneyTimePrivate + zEmp.MoneyTimeShare;
                zMoneyDate.Total = zTotal;
            }
            if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
            {
                zMoneyDate.Money = zEmp.MoneyPersonal + zEmp.MoneyPrivate;
                zMoneyDate.Money_Dif = zEmp.MoneyBorrowPrivate + zEmp.MoneyBorrowShare + zEmp.MoneyTimePrivate + zEmp.MoneyTimeShare;
                zMoneyDate.Money_Sundays = 0;
                zMoneyDate.Money_Dif_Sunday = 0;
                zMoneyDate.Money_Holidays = 0;
                zMoneyDate.Money_Dif_Holiday = 0;
                zMoneyDate.Total = zTotal;
            }
            if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
            {
                zMoneyDate.Money = 0;
                zMoneyDate.Money_Dif = 0;
                zMoneyDate.Money_Sundays = zEmp.MoneyPersonal + zEmp.MoneyPrivate;
                zMoneyDate.Money_Dif_Sunday = zEmp.MoneyBorrowPrivate + zEmp.MoneyBorrowShare + zEmp.MoneyTimePrivate + zEmp.MoneyTimeShare;
                zMoneyDate.Money_Holidays = 0;
                zMoneyDate.Money_Dif_Holiday = 0;
                zMoneyDate.Total = zTotal;
            }

            //Xử lý lệnh lưu
            Employee_Exe zDb = new Employee_Exe();
            zDb.Info = zMoneyDate;
            zDb.CreatedBy = SessionUser.UserLogin.Key;
            zDb.ModifiedBy = SessionUser.UserLogin.Key;
            zDb.CreatedName = SessionUser.UserLogin.EmployeeName;
            zDb.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zDb.Save();

            if (zDb.Message != "11" &&
                zDb.Message != "20")
            {
                ShowLog("Lỗi [" + zDb.Message + "]");
            }
        }
        private void btn_Run_Click(object sender, EventArgs e)
        {
            if (btn_Run.Tag.ToInt() == 0)
            {
                if (_Circle == 0)
                {
                    if (dte_ToDate.Value.Month != dte_FromDate.Value.Month)
                    {
                        Utils.TNMessageBoxOK("Vui lòng chỉ chọn trong 1 tháng !",1);
                        return;
                    }

                    _IndexDate = dte_FromDate.Value.Day;
                    _TotalDate = dte_ToDate.Value.Day;

                    _IndexTeam = 0;
                    _TotalTeam = 0;

                    _IndexEmployee = 0;
                    _TotalEmployee = 0;
                    _Circle = 1;

                    LoadTeam();
                    timerTeam.Start();
                }
                if (_Circle == 1)
                {
                    timerTeam.Start();
                }
                if (_Circle == 2)
                {
                    timerDate.Start();
                }
                if (_Circle == 3)
                {
                    timerEmployee.Start();
                }

                ShowLog("---Khởi chạy---");
                ShowLog("---Lúc " + DateTime.Now.ToString("hh:mm") + "---");
                PicLoading.Visible = true;
                btn_Run.Text = "Tạm dừng";
                btn_Run.Tag = 1;
            }
            else
            {
                if (_Circle == 1)
                {
                    timerTeam.Stop();
                    //---
                    timerDate.Stop();
                    timerEmployee.Stop();
                }
                if (_Circle == 2)
                {
                    timerDate.Stop();
                    timerEmployee.Stop();//dong them
                }
                if (_Circle == 3)
                {
                    timerEmployee.Stop();
                }

                ShowLog("---Tạm dừng---");
                ShowLog("---Lúc " + DateTime.Now.ToString("hh:mm") + "---");
                PicLoading.Visible = false;
                btn_Run.Text = "Khởi động";
                btn_Run.Tag = 0;
            }
        }
        private void timerTeam_Tick(object sender, EventArgs e)
        {
            timerTeam.Stop();

            if (_IndexTeam < _TotalTeam)
            {
                if (_TableTeam.Rows.Count > 0)
                {
                    DataRow r = _TableTeam.Rows[_IndexTeam];
                    _TeamKey = r["TeamKey"].ToInt();
                    _TeamName = r["TeamName"].ToString();

                    ShowGVTeam(r, GVTeam);

                    _IndexDate = dte_FromDate.Value.Day;
                    _TotalDate = dte_ToDate.Value.Day;

                    //khởi chạy date
                    //_Circle = 1;
                    timerDate.Start();
                }
                else
                {
                    _Circle = 0;
                    _IndexTeam = 0;
                    timerTeam.Stop();
                    btn_Run.Enabled = true;
                    btn_Run.Text = "Khởi động";
                    btn_Run.Tag = 0;
                    PicLoading.Visible = false;

                    ShowLog("---Đã tính xong lúc " + DateTime.Now.ToString("hh:mm") + "---");
                }
            }
            else
            {
                _Circle = 0;
                _IndexTeam = 0;
                timerTeam.Stop();
                btn_Run.Enabled = true;
                btn_Run.Text = "Khởi động";
                btn_Run.Tag = 0;
                PicLoading.Visible = false;

                ShowLog("---Đã tính xong lúc " + DateTime.Now.ToString("hh:mm") + "---");
            }
        }
        private void timerDate_Tick(object sender, EventArgs e)
        {
            timerDate.Stop();
            if (_IndexDate <= _TotalDate)
            {
                int Month = dte_ToDate.Value.Month;
                int Year = dte_ToDate.Value.Year;
                DateTime FromDate = new DateTime(Year, Month, _IndexDate, 0, 0, 0);
                DateTime ToDate = new DateTime(Year, Month, _IndexDate, 23, 59, 59);

                LoadEmployee(_TeamKey, _TeamName, FromDate, ToDate);
                //nếu có nhân viên làm việc trong ngày
                if (_TableEmployee.Rows.Count > 0)
                {
                    //khởi chạy employee
                    _Circle = 3;
                    _IndexEmployee = 0;
                    timerEmployee.Start();
                }
                else
                {
                    _Circle = 2;//dong sua thanh 1-->2
                    _IndexDate++;
                    timerDate.Start();
                }
            }
            else
            {
                _Circle = 1;// dong sua 0-->1
                _IndexTeam++;
                timerTeam.Start();
            }
        }
        private void timerEmployee_Tick(object sender, EventArgs e)
        {
            timerEmployee.Stop();
            if (_IndexEmployee < _TotalEmployee)
            {
                Employee_Money_Model zEmp = _ListEmployee[_IndexEmployee];
                Process_Data(zEmp);

                _Circle = 3;//2-->3
                _IndexEmployee++;
                timerEmployee.Start();
            }
            else
            {
                ShowLog("---Đã xử lý xong ngày [" + _IndexDate + "]---");

                //khởi chạy date
                _Circle = 2;//dong sua 1-->2
                _IndexEmployee = 0; //dong them 29/04/2020
                _IndexDate++;
                timerDate.Start();
            }
        }
        void ShowLog(string Text)
        {
            txtLog.AppendText(Text + Environment.NewLine);
            txtLog.ScrollToCaret();
        }

        #region [Custom Control Box Form]
        private void btnHideLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = false;
            txtLog.Visible = false;
            btnShowLog.Visible = true;
            btnHideLog.Visible = false;
        }
        private void btnShowLog_Click(object sender, EventArgs e)
        {
            Panel_Right.Visible = true;
            txtLog.Visible = true;
            btnShowLog.Visible = false;
            btnHideLog.Visible = true;
        }

        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion        
    }
}
