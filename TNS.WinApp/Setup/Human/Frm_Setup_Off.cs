﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Off : Form
    {
        public Frm_Setup_Off()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_List, true);
            Utils.DrawGVStyle(ref GV_List);
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;           
            txt_Rank.KeyPress += txt_Rank_KeyPress;
            GV_List.Click += GV_List_Click;
            GV_List.KeyDown += GV_List_KeyDown;
            GV_List_Layout();
        }

        #region[Private]
        private int _Key = 0;
        #endregion
        private void Frm_Time_Off_Load(object sender, EventArgs e)
        {
            GV_List_LoadData();
            LoadData();
        }

        #region[Progess]


        private void LoadData()
        {

            Time_Off_Info zinfo = new Time_Off_Info(_Key);
            txt_RiceName.Text = zinfo.OffName;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;        
        }

        #endregion
        private void GV_List_KeyDown(object sender, KeyEventArgs e)
        {
            string zMessage = "";
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GV_List.Rows.Count; i++)
                    {
                        if (GV_List.Rows[i].Selected)
                        {
                            if (GV_List.Rows[i].Tag != null)
                            {
                                Time_Off_Info zInfo = new Time_Off_Info();
                                zInfo.Key = int.Parse(GV_List.Rows[i].Tag.ToString());
                                zInfo.Delete();
                                zMessage += TN_Message.Show(zInfo.Message);
                            }
                        }
                    }
                    if (zMessage == "")
                    {
                        MessageBox.Show("Đã xóa !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GV_List_LoadData();
                        _Key = 0;
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (txt_RiceName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã phép trừ!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            float zMoney;
            if (!float.TryParse(txt_Rank.Text, out zMoney))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Time_Off_Data.CountID(txt_RiceName.Text.Trim().ToUpper()) > 0 && _Key == 0)
            {
                MessageBox.Show("Mã phép trừ đã tồn tại !.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                Time_Off_Info zinfo = new Time_Off_Info(_Key);
                zinfo.OffName = txt_RiceName.Text.Trim().ToUpper();
               
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                zMessage = TN_Message.Show(zinfo.Message);
                if (zMessage == "")
                {
                    MessageBox.Show("Cập nhật thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GV_List_LoadData();
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Time_Off_Info zinfo = new Time_Off_Info(_Key);
                    zinfo.Delete();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        MessageBox.Show("Xóa thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GV_List_LoadData();
                        _Key = 0;
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = 0;
            LoadData();
            txt_RiceName.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            GV_List_LoadData();
        }
        #endregion

        #region[ListView]

        private void GV_List_Layout()
        {
            // Setup Column 
            GV_List.Columns.Add("No", "STT");
            GV_List.Columns.Add("OffName", "Mã");
            GV_List.Columns.Add("Description", "Nội dung");

            GV_List.Columns["No"].Width = 40;
            GV_List.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_List.Columns["No"].ReadOnly = true;

            GV_List.Columns["OffName"].Width = 70;
            GV_List.Columns["OffName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_List.Columns["Description"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GV_List.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_List.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
        }
        private void GV_List_LoadData()
        {

            GV_List.Rows.Clear();
            DataTable In_Table = new DataTable();
            In_Table = Time_Off_Data.Search(txt_Search.Text);
            int i = 0;
            foreach (DataRow nRow in In_Table.Rows)
            {
                GV_List.Rows.Add();
                DataGridViewRow nRowView = GV_List.Rows[i];
                nRowView.Tag = nRow["OffKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["OffName"].Value = nRow["OffName"].ToString().Trim();
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                i++;
            }

        }

        #endregion

        private void GV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GV_List.Rows.Count; i++)
            {
                if (GV_List.CurrentRow.Selected == true)
                {
                    GV_List.Rows[i].DefaultCellStyle.BackColor = SystemColors.Window; // normal item

                }
                else
                {
                    GV_List.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue; // highlighted item
                }
            }

            if (GV_List.CurrentRow.Tag != null)
            {
                _Key = int.Parse(GV_List.CurrentRow.Tag.ToString());
                LoadData();
            }
            else
            {
                _Key = 0;
                LoadData();
            }

        }
        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        private void txt_Money_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
