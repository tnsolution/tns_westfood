﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TN.Library.System
{
    public class File_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _FileID = "";
        private string _FileName = "";
        private string _FileLink = "";
        private string _FileExt = "";
        private string _FileTitle = "";
        private string _Description = "";
        private DateTime _DateEffect;
        private DateTime _DateExpired;
        private DateTime _DateSign;
        private int _CategoryKey = 0;
        private int _Slug = 0;
        private int _Rank = 0;
        private int _IsPrimary = 0;
        private string _ParentTable = "";
        private string _ParentKey = "";
        private string _Organization = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string FileID
        {
            get { return _FileID; }
            set { _FileID = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public string FileLink
        {
            get { return _FileLink; }
            set { _FileLink = value; }
        }
        public string FileExt
        {
            get { return _FileExt; }
            set { _FileExt = value; }
        }
        public string FileTitle
        {
            get { return _FileTitle; }
            set { _FileTitle = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime DateEffect
        {
            get { return _DateEffect; }
            set { _DateEffect = value; }
        }
        public DateTime DateExpired
        {
            get { return _DateExpired; }
            set { _DateExpired = value; }
        }
        public DateTime DateSign
        {
            get { return _DateSign; }
            set { _DateSign = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int IsPrimary
        {
            get { return _IsPrimary; }
            set { _IsPrimary = value; }
        }
        public string ParentTable
        {
            get { return _ParentTable; }
            set { _ParentTable = value; }
        }
        public string ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string Organization
        {
            get { return _Organization; }
            set { _Organization = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public File_Info()
        {
        }
        public File_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM SYS_File WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _FileID = zReader["FileID"].ToString().Trim();
                    _FileName = zReader["FileName"].ToString().Trim();
                    _FileLink = zReader["FileLink"].ToString().Trim();
                    _FileExt = zReader["FileExt"].ToString().Trim();
                    _FileTitle = zReader["FileTitle"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["DateEffect"] != DBNull.Value)
                        _DateEffect = (DateTime)zReader["DateEffect"];
                    if (zReader["DateExpired"] != DBNull.Value)
                        _DateExpired = (DateTime)zReader["DateExpired"];
                    if (zReader["DateSign"] != DBNull.Value)
                        _DateSign = (DateTime)zReader["DateSign"];
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["IsPrimary"] != DBNull.Value)
                        _IsPrimary = int.Parse(zReader["IsPrimary"].ToString());
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public File_Info(string ParentKey)
        {
            string zSQL = "SELECT * FROM SYS_File WHERE ParentKey = @ParentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = ParentKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _FileID = zReader["FileID"].ToString().Trim();
                    _FileName = zReader["FileName"].ToString().Trim();
                    _FileLink = zReader["FileLink"].ToString().Trim();
                    _FileExt = zReader["FileExt"].ToString().Trim();
                    _FileTitle = zReader["FileTitle"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["DateEffect"] != DBNull.Value)
                        _DateEffect = (DateTime)zReader["DateEffect"];
                    if (zReader["DateExpired"] != DBNull.Value)
                        _DateExpired = (DateTime)zReader["DateExpired"];
                    if (zReader["DateSign"] != DBNull.Value)
                        _DateSign = (DateTime)zReader["DateSign"];
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["IsPrimary"] != DBNull.Value)
                        _IsPrimary = int.Parse(zReader["IsPrimary"].ToString());
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _Organization = zReader["Organization"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public File_Info(DataRow nRow)
        {
            if (nRow["AutoKey"] != DBNull.Value)
                _AutoKey = int.Parse(nRow["AutoKey"].ToString());
            _FileID = nRow["FileID"].ToString().Trim();
            _FileName = nRow["FileName"].ToString().Trim();
            _FileLink = nRow["FileLink"].ToString().Trim();
            _FileExt = nRow["FileExt"].ToString().Trim();
            _FileTitle = nRow["FileTitle"].ToString().Trim();
            _Description = nRow["Description"].ToString().Trim();
            if (nRow["DateEffect"] != DBNull.Value)
                _DateEffect = (DateTime)nRow["DateEffect"];
            if (nRow["DateExpired"] != DBNull.Value)
                _DateExpired = (DateTime)nRow["DateExpired"];
            if (nRow["DateSign"] != DBNull.Value)
                _DateSign = (DateTime)nRow["DateSign"];
            if (nRow["CategoryKey"] != DBNull.Value)
                _CategoryKey = int.Parse(nRow["CategoryKey"].ToString());
            if (nRow["Slug"] != DBNull.Value)
                _Slug = int.Parse(nRow["Slug"].ToString());
            if (nRow["Rank"] != DBNull.Value)
                _Rank = int.Parse(nRow["Rank"].ToString());
            if (nRow["IsPrimary"] != DBNull.Value)
                _IsPrimary = int.Parse(nRow["IsPrimary"].ToString());
            _ParentTable = nRow["ParentTable"].ToString().Trim();
            _ParentKey = nRow["ParentKey"].ToString().Trim();
            _Organization = nRow["Organization"].ToString().Trim();
            if (nRow["RecordStatus"] != DBNull.Value)
                _RecordStatus = int.Parse(nRow["RecordStatus"].ToString());
            if (nRow["CreatedOn"] != DBNull.Value)
                _CreatedOn = (DateTime)nRow["CreatedOn"];
            _CreatedBy = nRow["CreatedBy"].ToString().Trim();
            _CreatedName = nRow["CreatedName"].ToString().Trim();
            if (nRow["ModifiedOn"] != DBNull.Value)
                _ModifiedOn = (DateTime)nRow["ModifiedOn"];
            _ModifiedBy = nRow["ModifiedBy"].ToString().Trim();
            _ModifiedName = nRow["ModifiedName"].ToString().Trim();
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO SYS_File ("
        + "FileID ,FileName ,FileLink ,FileExt ,FileTitle ,Description ,DateEffect ,DateExpired ,DateSign ,CategoryKey ,Slug ,Rank ,IsPrimary ,ParentTable ,ParentKey ,Organization ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@FileID ,@FileName ,@FileLink ,@FileExt ,@FileTitle ,@Description ,@DateEffect ,@DateExpired ,@DateSign ,@CategoryKey ,@Slug ,@Rank ,@IsPrimary ,@ParentTable ,@ParentKey ,@Organization ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT AutoKey FROM SYS_File WHERE AutoKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FileID", SqlDbType.NVarChar).Value = _FileID.Trim();
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName.Trim();
                zCommand.Parameters.Add("@FileLink", SqlDbType.NVarChar).Value = _FileLink.Trim();
                zCommand.Parameters.Add("@FileExt", SqlDbType.NVarChar).Value = _FileExt.Trim();
                zCommand.Parameters.Add("@FileTitle", SqlDbType.NVarChar).Value = _FileTitle.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                if (_DateEffect == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateEffect", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateEffect", SqlDbType.DateTime).Value = _DateEffect;
                if (_DateExpired == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateExpired", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateExpired", SqlDbType.DateTime).Value = _DateExpired;
                if (_DateSign == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = _DateSign;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@IsPrimary", SqlDbType.Int).Value = _IsPrimary;
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE SYS_File SET "
                        + " FileID = @FileID,"
                        + " FileName = @FileName,"
                        + " FileLink = @FileLink,"
                        + " FileExt = @FileExt,"
                        + " FileTitle = @FileTitle,"
                        + " Description = @Description,"
                        + " DateEffect = @DateEffect,"
                        + " DateExpired = @DateExpired,"
                        + " DateSign = @DateSign,"
                        + " CategoryKey = @CategoryKey,"
                        + " Slug = @Slug,"
                        + " Rank = @Rank,"
                        + " IsPrimary = @IsPrimary,"
                        + " ParentTable = @ParentTable,"
                        + " ParentKey = @ParentKey,"
                        + " Organization = @Organization,"
                        + " CreatedOn = @CreatedOn,"
                        + " CreatedBy = @CreatedBy,"
                        + " CreatedName = @CreatedName,"
                        + " ModifiedOn = @ModifiedOn,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@FileID", SqlDbType.NVarChar).Value = _FileID.Trim();
                zCommand.Parameters.Add("@FileName", SqlDbType.NVarChar).Value = _FileName.Trim();
                zCommand.Parameters.Add("@FileLink", SqlDbType.NVarChar).Value = _FileLink.Trim();
                zCommand.Parameters.Add("@FileExt", SqlDbType.NVarChar).Value = _FileExt.Trim();
                zCommand.Parameters.Add("@FileTitle", SqlDbType.NVarChar).Value = _FileTitle.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                if (_DateEffect == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateEffect", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateEffect", SqlDbType.DateTime).Value = _DateEffect;
                if (_DateExpired == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateExpired", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateExpired", SqlDbType.DateTime).Value = _DateExpired;
                if (_DateSign == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateSign", SqlDbType.DateTime).Value = _DateSign;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@IsPrimary", SqlDbType.Int).Value = _IsPrimary;
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@Organization", SqlDbType.NVarChar).Value = _Organization.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE SYS_File SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ParentKey = @ParentKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.Int).Value = _ParentKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
