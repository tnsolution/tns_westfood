﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.SLR;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_FeeCompany : Form
    {
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        public Frm_FeeCompany()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            btn_Search.Click += Btn_Search_Click;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            btn_Done.Click += Btn_Done_Click;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        private void Frm_FeeCompany_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            DateTime DateView = SessionUser.Date_Work;
            dte_FromDate.Value = new DateTime(DateView.Year, 1, 1);
            dte_ToDate.Value = new DateTime(DateView.Year, DateView.Month, 1);
            dte_Done.Value = SessionUser.Date_Work;
            if (Role.AccessBranch == "2,4")
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "--Chọn tất cả--");
            else
            {
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "");
                Cbo_Branch_SelectedIndexChanged(null, null);
            }
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
        }
        private void DisplayData()
        {

            DataTable zTable = Fee_Company_Data.TRICHTHEOLUONG_CTY_V2(_FromDate, _ToDate, _BranchKey, _DepartmentKey, _TeamKey);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "BHXH", "BHYT", "BHTN", "KPCD" }, "HeaderColumn", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "BHXH", "BHYT", "BHTN", "KPCD" }, 0);


                zTablePivot.Columns.Add("EmployeeRank");
                zTablePivot.Columns.Add("BranchRank", typeof(int));
                zTablePivot.Columns.Add("DepartmentRank", typeof(int));
                zTablePivot.Columns.Add("TeamRank", typeof(int));
                zTablePivot.Columns.Add("TeamName").SetOrdinal(0);

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    if (i < zTablePivot.Rows.Count-1)
                    {
                        string[] temp = zTablePivot.Rows[i][1].ToString().Split('|');
                        zTablePivot.Rows[i]["EmployeeRank"] = ConvertIDRank(temp[2]);
                        zTablePivot.Rows[i]["BranchRank"] = temp[3];
                        zTablePivot.Rows[i]["DepartmentRank"] = temp[4];
                        zTablePivot.Rows[i]["TeamRank"] = temp[5];
                    }
                    else
                    {
                        zTablePivot.Rows[i]["EmployeeRank"] = "99999999";
                        zTablePivot.Rows[i]["BranchRank"] = "99999999";
                        zTablePivot.Rows[i]["DepartmentRank"] = "99999999";
                        zTablePivot.Rows[i]["TeamRank"] = "99999999";
                    }

                }
                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string loai = zTablePivot.Rows[i][1].ToString().Split('|')[0];
                    zTablePivot.Rows[i][0] = loai;
                }
                //Sắp xếp nhân viên
                DataView dv = zTablePivot.DefaultView;
                dv.Sort = " BranchRank ASC,DepartmentRank ASC,TeamRank ASC,  EmployeeRank ASC";
                zTablePivot = dv.ToTable();
                zTablePivot.Columns.Remove("EmployeeRank");
                zTablePivot.Columns.Remove("BranchRank");
                zTablePivot.Columns.Remove("DepartmentRank");
                zTablePivot.Columns.Remove("TeamRank");

                string Loai = zTablePivot.Rows[0][0].ToString();
                DataRow[] Array = zTablePivot.Select("TeamName='" + Loai + "'");
                if (Array.Length > 0)
                {
                    AddGroup(zTablePivot, Loai, Array, 0);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string temploai = zTablePivot.Rows[i][0].ToString();

                    if (temploai != Loai && temploai.ToUpper() != "TỔNG CỘNG")
                    {
                        Loai = temploai;
                        Array = zTablePivot.Select("TeamName='" + Loai + "'");
                        if (Array.Length > 0)
                        {
                            AddGroup(zTablePivot, Loai, Array, i);
                        }
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
        }

        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "Stt";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "Họ và tên";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "Mã thẻ";

            //Row Header
            int ColStart = 3;
            for (int i = 2; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[1];
                    GVData.Rows[1][ColStart] = strCaption[2];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                ColStart++;
            }

            ColStart = 1;
            int RowStart = 2;
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {

                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[1].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][0] = zNo;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                    zNo++;
                }
                else
                {
                    zNo = 1;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart].StyleNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                }

                for (int cIndex = 2; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe0String();
                }
            }


            ////Style
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 250;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 100;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            GVData.Cols[ToTalCol - 4].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 4].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 3].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 4].StyleNew.ForeColor = Color.Blue;
        }
        private void AddGroup(DataTable zTablePivot, string Loai, DataRow[] Array, int Index)
        {
            DataTable zGroup = Array.CopyToDataTable();
            DataRow dr = zTablePivot.NewRow();
            dr[0] = Loai;
            dr[1] = Loai.ToUpper();

            for (int c = 2; c < zGroup.Columns.Count; c++)
            {
                float Tong = 0;
                foreach (DataRow r in zGroup.Rows)
                {
                    float ztemp = 0;
                    if (float.TryParse(r[c].ToString(), out ztemp))
                    {

                    }
                    Tong += ztemp;
                }
                dr[c] = Tong;
            }

            zTablePivot.Rows.InsertAt(dr, Index);
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            DateTime DateWrite = dte_Done.Value;
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);

            if (Utils.TNMessageBox("Bạn có chắc chốt các khoản trích lương tháng " + dte_Done.Value.ToString("MM/yyyy") + "  ?.",2) == "Y")
            {

                string Mesage = Fee_Company_Data.LuuCacKhoanTrich_CONGTY(zFromDate, zToDate);
                if (Mesage == string.Empty)
                {
                    Utils.TNMessageBoxOK("Lưu thông tin thành công.",3);
                }
                else
                {
                    Utils.TNMessageBoxOK("Lỗi.Vui lòng liên hệ IT " +Mesage ,4);
                }
            }
        }
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            string zResult = "";
            string temp = ID.Replace("A", "");
            string s = "";
            //Thiếu bao nhieu số kí tự thì thêm vào bấy nhiêu cho đủ chuỗi VD: 11--->0011, A11--->1000011
            for (int i = 0; i < 4 - temp.Trim().Length; i++)
            {
                s += "0";
            }
            if (ID.Substring(0, 1) == "A")
            {
                zResult = "100" + s + temp;
            }
            else
            {
                zResult = s + temp;
            }

            return zResult;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        Access_Role_Info Role;
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            
        }
        #endregion
    }
}
