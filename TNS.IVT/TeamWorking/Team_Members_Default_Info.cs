﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.IVT
{
    public class Team_Members_Default_Info
    {

        #region [ Field Name ]
        private int m_TeamKey = 0;
        private string m_EmployeeKey = "";
        private int m_Numerical = 0;
        private string m_Message = "";
        #endregion

        #region [ Constructor Get Information ]
        public Team_Members_Default_Info()
        {
        }
        public Team_Members_Default_Info(int TeamKey)
        {
            string nSQL = "SELECT * FROM HRM_Team_Member_Default WHERE TeamKey = @TeamKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    m_TeamKey = int.Parse(nReader["TeamKey"].ToString());
                    m_EmployeeKey = nReader["EmployeeKey"].ToString();
                    m_Numerical = int.Parse(nReader["Numerical"].ToString());
                }
                nReader.Close(); nCommand.Dispose();
            }
            catch (Exception Err) { m_Message = Err.ToString(); }
            finally { nConnect.Close(); }
        }

        public Team_Members_Default_Info(int TeamKey,string EmployeeKey)
        {
            string nSQL = "SELECT * FROM HRM_Team_Member_Default WHERE TeamKey = @TeamKey AND EmployeeKey = @EmployeeKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                SqlDataReader nReader = nCommand.ExecuteReader();
                if (nReader.HasRows)
                {
                    nReader.Read();
                    m_TeamKey = int.Parse(nReader["TeamKey"].ToString());
                    m_EmployeeKey = nReader["EmployeeKey"].ToString();
                    m_Numerical = int.Parse(nReader["Numerical"].ToString());
                }
                nReader.Close(); nCommand.Dispose();
            }
            catch (Exception Err) { m_Message = Err.ToString(); }
            finally { nConnect.Close(); }
        }

        #endregion

        #region [ Properties ]
        public int TeamKey
        {
            get { return m_TeamKey; }
            set { m_TeamKey = value; }
        }

        public string EmployeeKey
        {
            get { return m_EmployeeKey; }
            set { m_EmployeeKey = value; }
        }
        public string Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }

        public int Numerical { get => m_Numerical; set => m_Numerical = value; }
        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string nSQL = "INSERT INTO HRM_Team_Member_Default ("
        + " TeamKey,EmployeeKey,Numerical ) "
         + " VALUES ( "
         + "@TeamKey,@EmployeeKey,@Numerical ) ";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = m_TeamKey;
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(m_EmployeeKey);
                nCommand.Parameters.Add("@Numerical", SqlDbType.Int).Value = m_Numerical; 
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }


        public string Update()
        {
            string nSQL = "UPDATE HRM_Team_Member_Default SET "
                        + " EmployeeKey = @EmployeeKey,"
                        + " Numerical = @Numerical"
                        + " WHERE TeamKey = @TeamKey";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = m_TeamKey;
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(m_EmployeeKey);
                nCommand.Parameters.Add("@Numerical", SqlDbType.Int).Value = m_Numerical;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Update(string EmployeeKey,int TeamKey )
        {
            string nSQL = "UPDATE HRM_Team_Member_Default SET "
                        + " Numerical = @Numerical"
                        + " WHERE TeamKey = @TeamKey AND EmployeeKey = @EmployeeKey";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                nCommand.Parameters.Add("@Numerical", SqlDbType.Int).Value = m_Numerical;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Save()
        {
            string nResult;
            if (m_TeamKey == 0 && m_EmployeeKey.Length ==0)
                nResult = Create();
            else
                nResult = Update();
            return nResult;
        }
        public string Delete(string EmployeeKey ,int TeamKey)
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM HRM_Team_Member_Default WHERE TeamKey = @TeamKey AND EmployeeKey = @EmployeeKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                nCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey); ;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Delete(int TeamKey)
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM HRM_Team_Member_Default WHERE TeamKey = @TeamKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                m_Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
