﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_WorkingHistory_Detail : Form
    {
        private string _Key = "";
        public string Key
        {
            get
            {
                return _Key;
            }

            set
            {
                _Key = value;
            }
        }

        private int _BrachKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        public Frm_WorkingHistory_Detail()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnClose.Click += btnClose_Click;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Copy.Click += Btn_Copy_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            cbo_Branch.Leave += Cbo_Branch_Leave;
            cbo_Department.Leave += Cbo_Department_Leave;
        }



        private void Cbo_Branch_Leave(object sender, EventArgs e)
        {
            if (cbo_Branch.SelectedValue.ToInt() != _BrachKey)
            {
                _BrachKey = cbo_Branch.SelectedValue.ToInt();
                LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey="+ _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
                if(cbo_Team.SelectedValue!=null)
                cbo_Team.SelectedIndex = 0;
            }
        }
        private void Cbo_Department_Leave(object sender, EventArgs e)
        {
            if (cbo_Department.SelectedValue.ToInt() != _DepartmentKey)
            {
                _DepartmentKey = cbo_Department.SelectedValue.ToInt();
                LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey ="+ _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
            }
        }
        private void Frm_WorkingHistory_Detail_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE RecordStatus <> 99  ORDER BY [Rank]", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Position, "SELECT PositionKey,PositionName FROM SYS_Position WHERE RecordStatus <> 99 ORDER BY [Rank]", "--Chọn--");
            if (_Key == "")
            {
                SetDefault();
            }
            else
                LoadData();
        }
        private void SetDefault()
        {
            _Key = "";
            _BrachKey = 0;
            _DepartmentKey = 0;
            _TeamKey = 0;
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Text = "";
            cbo_Branch.SelectedValue = "0";
            cbo_Department.SelectedValue = "0";
            cbo_Team.SelectedValue = "0";
            cbo_Position.SelectedValue = "0";
            txt_Description.Text = "";
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = DateTime.MinValue;
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_EmployeeID.Focus();
        }
        private void LoadData()
        {
            WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            _BrachKey = zinfo.BranchKey;
            _DepartmentKey = zinfo.DepartmentKey;
            _TeamKey = zinfo.TeamKey;
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey=" + _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey =" + _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
            cbo_Branch.SelectedValue = zinfo.BranchKey;
            cbo_Department.SelectedValue = zinfo.DepartmentKey;
            cbo_Team.SelectedValue = zinfo.TeamKey;
            cbo_Position.SelectedValue = zinfo.PositionKey;
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.StartingDate;
            if (zinfo.LeavingDate == DateTime.MinValue)
            {
                dte_ToDate.Value = DateTime.MinValue;
                btn_Copy.Enabled = false;
            }
            else
            {
                dte_ToDate.Value = zinfo.LeavingDate;
                btn_Copy.Enabled = true;

            }
            btn_Del.Enabled = true;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.ModifiedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã nhân viên!");
                return;
            }
            if (cbo_Branch.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chọn khối!");
                return;
            }
            if (cbo_Branch.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chọn khối!");
                return;
            }
            if (cbo_Department.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chọn bộ phận!");
                return;
            }
            if (cbo_Team.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chọn tổ nhóm!");
                return;
            }
            if (cbo_Position.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Chọn chức vụ!");
                return;
            }
            else
            {
                WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim().ToString();
                zinfo.BranchKey = cbo_Branch.SelectedValue.ToInt();
                zinfo.BranchName = cbo_Branch.Text;
                zinfo.DepartmentKey = cbo_Department.SelectedValue.ToInt();
                zinfo.DepartmentName = cbo_Department.Text;
                zinfo.TeamKey = cbo_Team.SelectedValue.ToInt();
                zinfo.TeamName = cbo_Team.Text;
                zinfo.PositionKey = cbo_Position.SelectedValue.ToInt();
                zinfo.PositionName = cbo_Position.Text;
                zinfo.Description = txt_Description.Text;
                DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
                DateTime zToDate = DateTime.MinValue;
                if (dte_ToDate.Value != DateTime.MinValue)
                {
                    zToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
                }
                zinfo.StartingDate = zFromDate;
                zinfo.LeavingDate = zToDate;

                //string[] s = cbo_Salary.Text.Split('-');
                //zinfo.CategoryID = s[0];
                //zinfo.CategoryName = s[1];
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == string.Empty )
                {
                    MessageBox.Show("Cập nhật thành công!");
                    UpdateTeam();
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show(zinfo.Message);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == "")
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?. Nếu xóa thông tin này vui lòng trở về cài đặt nhân sự lại thông tin nhân sự này!", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        MessageBox.Show("Đã xóa !");
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            _BrachKey = zinfo.BranchKey;
            _DepartmentKey = zinfo.DepartmentKey;
            _TeamKey = zinfo.TeamKey;
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey=" + _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey =" + _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
            cbo_Branch.SelectedValue = zinfo.BranchKey;
            cbo_Department.SelectedValue = zinfo.DepartmentKey;
            cbo_Team.SelectedValue = zinfo.TeamKey;
            cbo_Position.SelectedValue = zinfo.PositionKey;
            txt_Description.Text = zinfo.Description;
            DateTime zFromDate = new DateTime(zinfo.LeavingDate.Year, zinfo.LeavingDate.Month, zinfo.LeavingDate.Day, 0, 0, 0);
            dte_FromDate.Value = zFromDate.AddDays(1);
            dte_ToDate.Value = DateTime.MinValue;
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            _Key = "";

            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            MessageBox.Show("Sao chép thành công!Vui lòng chỉnh sửa thông tin.");
        }
        private void UpdateTeam()
        {
            Team_Info zTeam;
            Employee_Info zEmployee = new Employee_Info(txt_EmployeeID.Tag.ToString());
            zTeam = new Team_Info(cbo_Team.SelectedValue.ToInt());
            zEmployee.TeamKey = zTeam.Key;
            zEmployee.DepartmentKey = zTeam.DepartmentKey;
            zEmployee.BranchKey = zTeam.BranchKey;
            zEmployee.PositionKey = cbo_Position.SelectedValue.ToInt();
            zEmployee.Save();
        }


        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    _BrachKey = zinfo.BranchKey;
                    _DepartmentKey = zinfo.DepartmentKey;
                    _TeamKey = zinfo.TeamKey;
                    LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey=" + _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
                    LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey =" + _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    cbo_Branch.SelectedValue = zinfo.BranchKey;
                    cbo_Department.SelectedValue = zinfo.DepartmentKey;
                    cbo_Team.SelectedValue = zinfo.TeamKey;
                    cbo_Position.SelectedValue = zinfo.PositionKey;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;



        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
