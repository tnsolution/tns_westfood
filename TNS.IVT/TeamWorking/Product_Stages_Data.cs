﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.WinApp
{
    public class Product_Stages_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *,  AS [Key] FROM IVT_Product_Stages WHERE RecordStatus != 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Product(int TeamKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM [dbo].[IVT_Product_Stages] WHERE TeamKey = '" + TeamKey + "' AND RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(string Name, int TeamKey,int GroupKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Product_Stages WHERE RecordStatus <> 99 ";
            if (Name != string.Empty)
                zSQL += " AND ( StageName LIKE N'%" + Name + "%' OR StagesID LIKE N'%" + Name + "%') ";
            if (TeamKey != 0)
                zSQL += " AND TeamKey = @TeamKey";
            if (GroupKey != 0)
                zSQL += " AND GroupKey = @GroupKey";

            zSQL += " ORDER BY StagesID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = GroupKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(int CategoryParent, string Name, double Price)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT COUNT(*) FROM [dbo].[IVT_Product_Stages] 
WHERE TeamKey = @TeamKey AND StageName = @StageName AND Price = @Price AND RecordStatus < 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = CategoryParent;
                zCommand.Parameters.Add("@StageName", SqlDbType.NVarChar).Value = Name;
                zCommand.Parameters.Add("@Price", SqlDbType.Money).Value = Price;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Product_Stages WHERE StageName LIKE N'%" + Name + "%' ORDER BY StageName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


        public static DataTable DataPage(int TeamKey, string Name, int Index)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Product_Stages WHERE RecordStatus <> 99 ";
            if (Name != string.Empty)
                zSQL += " AND StageName LIKE N'%" + Name + "%' ";
            if (TeamKey != 0)
                zSQL += " AND TeamKey = @TeamKey";
            zSQL += " ORDER BY StageName";

            string MasterSQL = "SELECT * FROM (" + zSQL + ") X WHERE X.RowNum BETWEEN (@Index -1) * 30 + 1 AND (((@Index -1) * 30 + 1) + 30) - 1";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@Index", SqlDbType.Int).Value = Index;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
