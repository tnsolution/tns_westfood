﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.IVT
{
    public class Team_Working_Info
    {
        #region [File Name]
        private string _TeamWorkingKey = "";
        private DateTime _WorkingDate;
        private int _TeamKey = 0;
        private string _TeamName = "";
        private DateTime _WorkingBegin;
        private DateTime _WorkingEnd;
        private int _WorkingStatus = 0;
        private string _Message = "";


        #endregion

        #region [Properties]
        public string TeamWorkingKey { get => _TeamWorkingKey; set => _TeamWorkingKey = value; }
        public DateTime WorkingDate { get => _WorkingDate; set => _WorkingDate = value; }
        public int TeamKey { get => _TeamKey; set => _TeamKey = value; }
        public string TeamName { get => _TeamName; set => _TeamName = value; }
        public DateTime WorkingBegin { get => _WorkingBegin; set => _WorkingBegin = value; }
        public DateTime WorkingEnd { get => _WorkingEnd; set => _WorkingEnd = value; }
        public int WorkingStatus { get => _WorkingStatus; set => _WorkingStatus = value; }
        public string Message { get => _Message; set => _Message = value; }
        #endregion

        #region [Constructor Get Information] 
        public Team_Working_Info() { }

        public Team_Working_Info(string TeamWorkingKey)
        {
            string zSQL = "SELECT * FROM FTR_Team_Working WHERE TeamWorkingKey = @TeamWorkingKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TeamWorkingKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _TeamWorkingKey = zReader["TeamWorkingKey"].ToString();
                    _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamName = zReader["TeamName"].ToString();
                    if (zReader["WorkBegin"] != DBNull.Value)
                        _WorkingBegin = (DateTime)zReader["WorkBegin"];
                    if (zReader["WorkEnd"] != DBNull.Value)
                        _WorkingEnd = (DateTime)zReader["WorkEnd"];
                    _WorkingStatus = int.Parse(zReader["WorkingStatus"].ToString());
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [Constructor Update Information]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string nSQL = "Team_Member_INSERT";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.StoredProcedure;
                if (WorkingBegin.Year == 0001)
                    nCommand.Parameters.Add("@WorkBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@WorkBegin", SqlDbType.DateTime).Value = WorkingBegin;
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
               
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Update()
        {
            string nSQL = "UPDATE FTR_Team_Working SET "
                        + " TeamKey = @TeamKey,"
                        + " TeamName = @TeamName,"
                        + " WorkBegin = @WorkBegin,"
                        + " WorkEnd = @WorkEnd,"
                        + " WorkingStatus = @WorkingStatus"
                       + " WHERE  TeamWorkingKey = @TeamWorkingKey";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TeamWorkingKey);
                nCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                nCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = TeamName;
                if (WorkingBegin.Year == 0001)
                    nCommand.Parameters.Add("@WorkBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@WorkBegin", SqlDbType.DateTime).Value = WorkingBegin;
                if (WorkingEnd.Year == 0001)
                    nCommand.Parameters.Add("@WorkEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@WorkEnd", SqlDbType.DateTime).Value = WorkingEnd;
                nCommand.Parameters.Add("@WorkingStatus", SqlDbType.Int).Value = WorkingStatus;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Delete()
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FTR_Team_Working WHERE TeamWorkingKey = @TeamWorkingKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = TeamWorkingKey;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }


        public string Delete(string TeamWorkingKey)
        {
            string nResult = "";
            //---------- String SQL Access Database ---------------
            string nSQL = "DELETE FROM FTR_Team_Working WHERE TeamWorkingKey = @TeamWorkingKey";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(TeamWorkingKey);
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

        #region [Update_Start/End]
        public string Update_Start(string Team)
        {
            string nSQL = "UPDATE FTR_Team_Working SET "
                        + " WorkingStatus = 1,"
                        + " WorkBegin = @WorkBegin"
                        + " WHERE  TeamWorkingKey = @TeamWorkingKey";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Team);
                nCommand.Parameters.Add("@WorkBegin", SqlDbType.DateTime).Value = _WorkingBegin;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }

        public string Update_End(string Team)
        {
            string nSQL = "UPDATE FTR_Team_Working SET "
                        + " WorkingStatus = 5,"
                        + " WorkEnd = @WorkEnd"
                        + " WHERE  TeamWorkingKey = @TeamWorkingKey";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Team);
                nCommand.Parameters.Add("@WorkEnd", SqlDbType.DateTime).Value = _WorkingEnd;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion

        #region [Update TeamWorking_Conti]
        public string Update(string Team)
        {
            string nSQL = "UPDATE FTR_Team_Working SET "
                        + " TeamName = @TeamName,"
                        + " WorkBegin = @WorkBegin,"
                        + " WorkEnd = @WorkEnd"
                       + " WHERE  TeamWorkingKey = @TeamWorkingKey";
            string nResult = "";
            string nConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection nConnect = new SqlConnection(nConnectionString);
            nConnect.Open();
            try
            {
                SqlCommand nCommand = new SqlCommand(nSQL, nConnect);
                nCommand.CommandType = CommandType.Text;
                nCommand.Parameters.Add("@TeamWorkingKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(Team);                              
                nCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName;
                if (_WorkingBegin.Year == 0001)
                    nCommand.Parameters.Add("@WorkBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@WorkBegin", SqlDbType.DateTime).Value = _WorkingBegin;
                if (_WorkingEnd.Year == 0001)
                    nCommand.Parameters.Add("@WorkEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    nCommand.Parameters.Add("@WorkEnd", SqlDbType.DateTime).Value = _WorkingEnd;
                nResult = nCommand.ExecuteNonQuery().ToString();
                nCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                nConnect.Close();
            }
            return nResult;
        }
        #endregion
    }
}
