﻿namespace TNS.WinApp
{
    partial class Frm_Stock_Local_Output
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Stock_Local_Output));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dte_OrderDate = new TNS.SYS.TNDateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Order_ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Description = new System.Windows.Forms.TextBox();
            this.cbo_Warehouse = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.GroupCustomer = new System.Windows.Forms.GroupBox();
            this.cbo_Warehouse_Local = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.GVData = new System.Windows.Forms.DataGridView();
            this.GVSum = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt_Message = new System.Windows.Forms.TextBox();
            this.btn_Post = new System.Windows.Forms.Button();
            this.GVNote = new System.Windows.Forms.DataGridView();
            this.Pn_Right = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Deny = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Approve = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Send = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Edit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Copy = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Import = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_New = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Pn_Left = new System.Windows.Forms.Panel();
            this.LVData = new System.Windows.Forms.ListView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_Del = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.dte_ToDate = new TNS.SYS.TNDateTimePicker();
            this.dte_FromDate = new TNS.SYS.TNDateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Line = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.GroupCustomer.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSum)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVNote)).BeginInit();
            this.Pn_Right.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Pn_Left.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox3.Controls.Add(this.dte_OrderDate);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txt_Order_ID);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txt_Description);
            this.groupBox3.Controls.Add(this.cbo_Warehouse);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox3.Location = new System.Drawing.Point(1, 32);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(436, 125);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thông tin";
            // 
            // dte_OrderDate
            // 
            this.dte_OrderDate.CustomFormat = "dd/MM/yyyy";
            this.dte_OrderDate.Location = new System.Drawing.Point(318, 21);
            this.dte_OrderDate.Name = "dte_OrderDate";
            this.dte_OrderDate.Size = new System.Drawing.Size(111, 31);
            this.dte_OrderDate.TabIndex = 1;
            this.dte_OrderDate.Value = new System.DateTime(((long)(0)));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(41, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 15);
            this.label3.TabIndex = 107;
            this.label3.Text = "Số phiếu";
            // 
            // txt_Order_ID
            // 
            this.txt_Order_ID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_Order_ID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_Order_ID.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Order_ID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(68)))), ((int)(((byte)(67)))));
            this.txt_Order_ID.Location = new System.Drawing.Point(99, 23);
            this.txt_Order_ID.Name = "txt_Order_ID";
            this.txt_Order_ID.Size = new System.Drawing.Size(135, 21);
            this.txt_Order_ID.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(281, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 15);
            this.label1.TabIndex = 106;
            this.label1.Text = "Ngày";
            // 
            // txt_Description
            // 
            this.txt_Description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_Description.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Description.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(68)))), ((int)(((byte)(67)))));
            this.txt_Description.Location = new System.Drawing.Point(99, 79);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.Size = new System.Drawing.Size(331, 37);
            this.txt_Description.TabIndex = 3;
            // 
            // cbo_Warehouse
            // 
            this.cbo_Warehouse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cbo_Warehouse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Warehouse.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(68)))), ((int)(((byte)(67)))));
            this.cbo_Warehouse.FormattingEnabled = true;
            this.cbo_Warehouse.Location = new System.Drawing.Point(99, 50);
            this.cbo_Warehouse.Name = "cbo_Warehouse";
            this.cbo_Warehouse.Size = new System.Drawing.Size(331, 23);
            this.cbo_Warehouse.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label4.Location = new System.Drawing.Point(36, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 108;
            this.label4.Text = "Lý do xuất";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(68, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 15);
            this.label7.TabIndex = 104;
            this.label7.Text = "Kho";
            // 
            // GroupCustomer
            // 
            this.GroupCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.GroupCustomer.Controls.Add(this.cbo_Warehouse_Local);
            this.GroupCustomer.Controls.Add(this.label2);
            this.GroupCustomer.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupCustomer.ForeColor = System.Drawing.Color.Maroon;
            this.GroupCustomer.Location = new System.Drawing.Point(441, 32);
            this.GroupCustomer.Name = "GroupCustomer";
            this.GroupCustomer.Size = new System.Drawing.Size(496, 125);
            this.GroupCustomer.TabIndex = 1;
            this.GroupCustomer.TabStop = false;
            this.GroupCustomer.Text = "Xuất cho ";
            // 
            // cbo_Warehouse_Local
            // 
            this.cbo_Warehouse_Local.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cbo_Warehouse_Local.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.cbo_Warehouse_Local.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(68)))), ((int)(((byte)(67)))));
            this.cbo_Warehouse_Local.FormattingEnabled = true;
            this.cbo_Warehouse_Local.Location = new System.Drawing.Point(39, 18);
            this.cbo_Warehouse_Local.Name = "cbo_Warehouse_Local";
            this.cbo_Warehouse_Local.Size = new System.Drawing.Size(258, 23);
            this.cbo_Warehouse_Local.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(8, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 15);
            this.label2.TabIndex = 104;
            this.label2.Text = "Kho";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.tabControl1.Location = new System.Drawing.Point(1, 161);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(936, 497);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.GVData);
            this.tabPage2.Controls.Add(this.GVSum);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(928, 468);
            this.tabPage2.TabIndex = 6;
            this.tabPage2.Text = "Thông tin Nguyên Liệu";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // GVData
            // 
            this.GVData.ColumnHeadersHeight = 40;
            this.GVData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnKeystroke;
            this.GVData.Location = new System.Drawing.Point(3, 3);
            this.GVData.Name = "GVData";
            this.GVData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData.Size = new System.Drawing.Size(922, 412);
            this.GVData.TabIndex = 0;
            // 
            // GVSum
            // 
            this.GVSum.AllowUserToAddRows = false;
            this.GVSum.AllowUserToDeleteRows = false;
            this.GVSum.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVSum.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GVSum.Location = new System.Drawing.Point(3, 415);
            this.GVSum.Name = "GVSum";
            this.GVSum.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.GVSum.Size = new System.Drawing.Size(922, 50);
            this.GVSum.TabIndex = 197;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Controls.Add(this.GVNote);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(928, 468);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Thông Tin Trao Đổi";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txt_Message);
            this.panel1.Controls.Add(this.btn_Post);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 410);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(922, 55);
            this.panel1.TabIndex = 114;
            // 
            // txt_Message
            // 
            this.txt_Message.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_Message.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Message.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Message.ForeColor = System.Drawing.Color.Navy;
            this.txt_Message.Location = new System.Drawing.Point(2, 4);
            this.txt_Message.Multiline = true;
            this.txt_Message.Name = "txt_Message";
            this.txt_Message.Size = new System.Drawing.Size(802, 47);
            this.txt_Message.TabIndex = 111;
            // 
            // btn_Post
            // 
            this.btn_Post.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Post.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Post.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Post.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Post.Image = ((System.Drawing.Image)(resources.GetObject("btn_Post.Image")));
            this.btn_Post.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Post.Location = new System.Drawing.Point(803, 4);
            this.btn_Post.Name = "btn_Post";
            this.btn_Post.Size = new System.Drawing.Size(113, 47);
            this.btn_Post.TabIndex = 113;
            this.btn_Post.Text = "TRAO ĐỔI";
            this.btn_Post.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Post.UseVisualStyleBackColor = true;
            // 
            // GVNote
            // 
            this.GVNote.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVNote.Location = new System.Drawing.Point(3, 3);
            this.GVNote.Name = "GVNote";
            this.GVNote.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVNote.Size = new System.Drawing.Size(922, 462);
            this.GVNote.TabIndex = 14;
            // 
            // Pn_Right
            // 
            this.Pn_Right.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Pn_Right.Controls.Add(this.tabControl1);
            this.Pn_Right.Controls.Add(this.groupBox1);
            this.Pn_Right.Controls.Add(this.GroupCustomer);
            this.Pn_Right.Controls.Add(this.groupBox3);
            this.Pn_Right.Controls.Add(this.txtTitle);
            this.Pn_Right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Pn_Right.Location = new System.Drawing.Point(327, 42);
            this.Pn_Right.Name = "Pn_Right";
            this.Pn_Right.Size = new System.Drawing.Size(939, 726);
            this.Pn_Right.TabIndex = 182;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox1.Controls.Add(this.btn_Deny);
            this.groupBox1.Controls.Add(this.btn_Save);
            this.groupBox1.Controls.Add(this.btn_Approve);
            this.groupBox1.Controls.Add(this.btn_Send);
            this.groupBox1.Controls.Add(this.btn_Edit);
            this.groupBox1.Controls.Add(this.btn_Copy);
            this.groupBox1.Controls.Add(this.btn_Import);
            this.groupBox1.Controls.Add(this.btn_New);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox1.Location = new System.Drawing.Point(0, 664);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(937, 60);
            this.groupBox1.TabIndex = 188;
            this.groupBox1.TabStop = false;
            // 
            // btn_Deny
            // 
            this.btn_Deny.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Deny.Location = new System.Drawing.Point(813, 10);
            this.btn_Deny.Name = "btn_Deny";
            this.btn_Deny.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Deny.Size = new System.Drawing.Size(120, 40);
            this.btn_Deny.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Deny.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Deny.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Deny.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Deny.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Deny.TabIndex = 15;
            this.btn_Deny.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Deny.Values.Image")));
            this.btn_Deny.Values.Text = "Từ chối";
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(814, 10);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(120, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 14;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Cập nhật";
            // 
            // btn_Approve
            // 
            this.btn_Approve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Approve.Location = new System.Drawing.Point(688, 10);
            this.btn_Approve.Name = "btn_Approve";
            this.btn_Approve.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Approve.Size = new System.Drawing.Size(120, 40);
            this.btn_Approve.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Approve.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Approve.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Approve.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Approve.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Approve.TabIndex = 13;
            this.btn_Approve.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Approve.Values.Image")));
            this.btn_Approve.Values.Text = "Duyệt";
            // 
            // btn_Send
            // 
            this.btn_Send.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Send.Location = new System.Drawing.Point(687, 10);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Send.Size = new System.Drawing.Size(120, 40);
            this.btn_Send.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Send.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Send.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Send.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Send.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Send.TabIndex = 13;
            this.btn_Send.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Send.Values.Image")));
            this.btn_Send.Values.Text = "Gửi";
            // 
            // btn_Edit
            // 
            this.btn_Edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Edit.Location = new System.Drawing.Point(561, 10);
            this.btn_Edit.Name = "btn_Edit";
            this.btn_Edit.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Edit.Size = new System.Drawing.Size(120, 40);
            this.btn_Edit.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Edit.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Edit.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Edit.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Edit.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Edit.TabIndex = 12;
            this.btn_Edit.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Edit.Values.Image")));
            this.btn_Edit.Values.Text = "Sửa";
            // 
            // btn_Copy
            // 
            this.btn_Copy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Copy.Location = new System.Drawing.Point(131, 10);
            this.btn_Copy.Name = "btn_Copy";
            this.btn_Copy.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Copy.Size = new System.Drawing.Size(120, 40);
            this.btn_Copy.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_Copy.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Copy.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Copy.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Copy.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Copy.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Copy.TabIndex = 11;
            this.btn_Copy.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Copy.Values.Image")));
            this.btn_Copy.Values.Text = "Sao chép";
            // 
            // btn_Import
            // 
            this.btn_Import.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Import.Location = new System.Drawing.Point(257, 10);
            this.btn_Import.Name = "btn_Import";
            this.btn_Import.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Import.Size = new System.Drawing.Size(120, 40);
            this.btn_Import.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Import.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Import.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Import.TabIndex = 10;
            this.btn_Import.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Import.Values.Image")));
            this.btn_Import.Values.Text = "Nhập Excel";
            // 
            // btn_New
            // 
            this.btn_New.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_New.Location = new System.Drawing.Point(5, 10);
            this.btn_New.Name = "btn_New";
            this.btn_New.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_New.Size = new System.Drawing.Size(120, 40);
            this.btn_New.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.btn_New.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_New.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_New.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_New.TabIndex = 9;
            this.btn_New.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_New.Values.Text = "Làm mới";
            // 
            // txtTitle
            // 
            this.txtTitle.AutoSize = false;
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtTitle.Size = new System.Drawing.Size(937, 30);
            this.txtTitle.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.txtTitle.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.txtTitle.TabIndex = 187;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Chi tiết đơn hàng";
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1266, 42);
            this.HeaderControl.TabIndex = 184;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "Xuất Kho";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            this.HeaderControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseDown);
            this.HeaderControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseMove);
            this.HeaderControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frm_Main_MouseUp);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            this.btnMini.Click += new System.EventHandler(this.btnMini_Click);
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            this.btnMax.Click += new System.EventHandler(this.btnMax_Click);
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Pn_Left
            // 
            this.Pn_Left.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Pn_Left.Controls.Add(this.LVData);
            this.Pn_Left.Controls.Add(this.groupBox4);
            this.Pn_Left.Controls.Add(this.panel4);
            this.Pn_Left.Controls.Add(this.kryptonHeader1);
            this.Pn_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Pn_Left.Location = new System.Drawing.Point(0, 42);
            this.Pn_Left.Name = "Pn_Left";
            this.Pn_Left.Size = new System.Drawing.Size(325, 726);
            this.Pn_Left.TabIndex = 185;
            // 
            // LVData
            // 
            this.LVData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVData.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVData.FullRowSelect = true;
            this.LVData.GridLines = true;
            this.LVData.HideSelection = false;
            this.LVData.Location = new System.Drawing.Point(0, 129);
            this.LVData.Name = "LVData";
            this.LVData.Size = new System.Drawing.Size(323, 535);
            this.LVData.TabIndex = 1;
            this.LVData.UseCompatibleStateImageBehavior = false;
            this.LVData.View = System.Windows.Forms.View.Details;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox4.Controls.Add(this.btn_Del);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox4.Location = new System.Drawing.Point(0, 664);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(323, 60);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            // 
            // btn_Del
            // 
            this.btn_Del.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Del.Location = new System.Drawing.Point(197, 10);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Del.Size = new System.Drawing.Size(120, 40);
            this.btn_Del.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Del.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Del.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Del.TabIndex = 10;
            this.btn_Del.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Del.Values.Image")));
            this.btn_Del.Values.Text = "Xóa";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel4.Controls.Add(this.btn_Search);
            this.panel4.Controls.Add(this.dte_ToDate);
            this.panel4.Controls.Add(this.dte_FromDate);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.label20);
            this.panel4.Controls.Add(this.txt_Search);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 30);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(323, 99);
            this.panel4.TabIndex = 0;
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(217, 43);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(100, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 180;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Location = new System.Drawing.Point(68, 33);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(98, 20);
            this.dte_ToDate.TabIndex = 1;
            this.dte_ToDate.Value = new System.DateTime(((long)(0)));
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Location = new System.Drawing.Point(68, 7);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(98, 20);
            this.dte_FromDate.TabIndex = 0;
            this.dte_FromDate.Value = new System.DateTime(((long)(0)));
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(7, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 15);
            this.label8.TabIndex = 154;
            this.label8.Text = "Đến ngày";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(15, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 15);
            this.label6.TabIndex = 155;
            this.label6.Text = "Từ ngày";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label20.Location = new System.Drawing.Point(9, 63);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 15);
            this.label20.TabIndex = 147;
            this.label20.Text = "Mã phiếu";
            // 
            // txt_Search
            // 
            this.txt_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(68)))), ((int)(((byte)(67)))));
            this.txt_Search.Location = new System.Drawing.Point(68, 60);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(143, 21);
            this.txt_Search.TabIndex = 2;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(323, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonHeader1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.StateCommon.Content.ShortText.TextV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonHeader1.TabIndex = 185;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Các đơn hàng xuất";
            // 
            // Line
            // 
            this.Line.BackColor = System.Drawing.Color.DodgerBlue;
            this.Line.Dock = System.Windows.Forms.DockStyle.Left;
            this.Line.Location = new System.Drawing.Point(325, 42);
            this.Line.Name = "Line";
            this.Line.Size = new System.Drawing.Size(2, 726);
            this.Line.TabIndex = 186;
            // 
            // Frm_Stock_Local_Output
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.ClientSize = new System.Drawing.Size(1266, 768);
            this.Controls.Add(this.Pn_Right);
            this.Controls.Add(this.Line);
            this.Controls.Add(this.Pn_Left);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Stock_Local_Output";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Frm_Stock_Output_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.GroupCustomer.ResumeLayout(false);
            this.GroupCustomer.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVSum)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVNote)).EndInit();
            this.Pn_Right.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.Pn_Left.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_Order_ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Description;
        private System.Windows.Forms.ComboBox cbo_Warehouse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox GroupCustomer;
        private System.Windows.Forms.ComboBox cbo_Warehouse_Local;
        private System.Windows.Forms.Label label2;
        private TNS.SYS.TNDateTimePicker dte_OrderDate;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView GVData;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btn_Post;
        private System.Windows.Forms.TextBox txt_Message;
        private System.Windows.Forms.DataGridView GVNote;
        private System.Windows.Forms.DataGridView GVSum;
        private System.Windows.Forms.Panel Pn_Right;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Deny;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Approve;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Send;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Edit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Copy;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Import;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_New;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Pn_Left;
        private System.Windows.Forms.ListView LVData;
        private System.Windows.Forms.GroupBox groupBox4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Del;
        private System.Windows.Forms.Panel panel4;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private TNS.SYS.TNDateTimePicker dte_ToDate;
        private TNS.SYS.TNDateTimePicker dte_FromDate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txt_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private System.Windows.Forms.Label Line;
    }
}