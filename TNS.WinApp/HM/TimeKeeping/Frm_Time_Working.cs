﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TN.Library.System;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Time_Working : Form
    {
        public Frm_Time_Working()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            LV_List.Click += LV_List_Click;
        }
        #region[Private]
        private string _Key = "";
        private string _Name = "";
        private string _CardID = "";
        private DateTime _Date;
        private string _WorkingKey = "";
        #endregion

        private void Frm_WorkingTime_Load(object sender, EventArgs e)
        {
            LoadDataToToolbox.ComboBoxData(cbo_Code, Time_Defines_Data.ListTable(), false, 0, 1);
            LV_Layout(LV_List);
            LV_Layout_Detail(LV_Detail);
            _Date = dte_Month.Value;
            SetDefault();
        }
        #region[Progess]
        private void LoadData()
        {
            Time_Working_Info zinfo = new Time_Working_Info(_WorkingKey);
            dte_FromDay.Value = zinfo.BeginTime;
            dte_ToDay.Value = zinfo.EndTime;
            dte_OffTime.Text = zinfo.OffTime.ToString(@"hh\:mm");
            cbo_Code.SelectedValue = zinfo.CodeKey;
            txt_CardID.Text = _CardID;
            txt_EmployeeName.Text = _Name;
            txt_Description.Text = zinfo.Description;

        }
        private void SetDefault()
        {
            _WorkingKey = "";
            dte_FromDay.Value = DateTime.Now;
            dte_ToDay.Value = DateTime.Now;
            dte_OffTime.Text ="00:00";
            txt_Description.Text = "";
            txt_CardID.Focus();
        }
        #endregion

        #region[LV_List]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Họ tên";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã nhân viên";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);
        }
        private void LV_LoadData(DataTable In_Table)
        {
            _WorkingKey = "";
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["EmployeeKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FullName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CardID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Key = LV_List.SelectedItems[0].Tag.ToString();
            _Name = LV_List.SelectedItems[0].SubItems[1].Text;
            _CardID = LV_List.SelectedItems[0].SubItems[2].Text;
            txt_CardID.Text = _CardID;
            txt_EmployeeName.Text = _Name;
            // SetDefault();
            LV_LoadData_Detail();
        }

        #endregion

        #region[LV_Detail]
        private void LV_Layout_Detail(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã công";
            colHead.Width = 200;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giờ vào";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giờ ra";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Giờ nghỉ";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);


        }
        private void LV_LoadData_Detail()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Detail;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DateTime FromDate;
            DateTime ToDate;
            FromDate = new DateTime(_Date.Year, _Date.Month, 1, 0, 0, 0);
            ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            lbl_TitleDetail.Text = "CHI TIÊT CHẤM CÔNG THÁNG " + _Date.Month + "/" + _Date.Year;
            DataTable ztb = Time_Working_Data.EmployeeWorkingList(_Key, FromDate, ToDate);
            int n = ztb.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["WorkingKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CodeName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zDateBegin = DateTime.Parse(nRow["BeginTime"].ToString().Trim());
                lvsi.Text = zDateBegin.ToString("dd/MM/yyyy HH:mm");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zDateEnd = DateTime.Parse(nRow["EndTime"].ToString().Trim());
                lvsi.Text = zDateEnd.ToString("dd/MM/yyyy HH:mm");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                TimeSpan zOffTime = TimeSpan.Parse(nRow["OffTime"].ToString().Trim());
                lvsi.Text = zOffTime.ToString(@"hh\:mm");
                lvi.SubItems.Add(lvsi);


                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LV_Detail_Click(object sender, EventArgs e)
        {
            _WorkingKey = LV_Detail.SelectedItems[0].Tag.ToString();
            LoadData();
        }
        #endregion

        #region[Event]
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DataTable zTable = Employee_Data.Search(txt_Search.Text.Trim());
            LV_LoadData(zTable);

        }
        private void dte_Month_ValueChanged(object sender, EventArgs e)
        {
            _Date = dte_Month.Value;
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (_Key.Length > 0)
            {
                Time_Working_Info zinfo = new Time_Working_Info(_WorkingKey);
                zinfo.EmployeeKey = _Key;
                //zinfo.PermitDate = dte_CheckTime.Value;
                zinfo.BeginTime = dte_FromDay.Value;
                zinfo.EndTime = dte_ToDay.Value;
                zinfo.OffTime = float.Parse(dte_OffTime.Text);
                zinfo.CodeKey = int.Parse(cbo_Code.SelectedValue.ToString());
                zinfo.Description = txt_Description.Text;
                zinfo.Save();

                if (zinfo.Message == string.Empty)
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LV_LoadData_Detail();
                    SetDefault();
                }
                else
                    MessageBox.Show(zinfo.Message);
            }
            else
            {
                MessageBox.Show("Chưa chọn nhân viên!");
            }

        }

        private void dte_ToDay_ValueChanged(object sender, EventArgs e)
        {
        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_WorkingKey.Trim().Length==0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Time_Working_Info zinfo = new  Time_Working_Info(_WorkingKey);
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        MessageBox.Show("Đã xóa !");
                        LV_LoadData_Detail();
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }
        }

        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        #endregion


    }
}
