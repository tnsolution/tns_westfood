﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;

namespace TNS.SLR
{
    public class ReportWorker_Close_Data
    {
       
        //Load danh sách báo cáo lương bộ phận sản xuất
        public static DataTable ListCodeReportWorker()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey, A.ID,A.Name FROM [dbo].[SLR_CodeReportWorker] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

       
    }
}
