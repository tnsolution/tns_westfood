﻿namespace TNS.WinApp
{
    partial class Frm_Import_Production_Time
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Import_Production_Time));
            this.txtTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.Panel_Left = new System.Windows.Forms.Panel();
            this.PnTeam = new System.Windows.Forms.Panel();
            this.LVTeam = new System.Windows.Forms.ListView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.GVTitle = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.GVData = new System.Windows.Forms.DataGridView();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btn_Save = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Open = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PnSheet = new System.Windows.Forms.Panel();
            this.LVSheet = new System.Windows.Forms.ListView();
            this.kryptonHeader1 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btn_Up = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_Down = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.Panel_Left.SuspendLayout();
            this.PnTeam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.panel6.SuspendLayout();
            this.PnSheet.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTitle
            // 
            this.txtTitle.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.txtTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtTitle.Location = new System.Drawing.Point(0, 0);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txtTitle.Size = new System.Drawing.Size(1266, 42);
            this.txtTitle.TabIndex = 146;
            this.txtTitle.Values.Description = "";
            this.txtTitle.Values.Heading = "Tập tin Excel thời gian công nhân thực hiện sản xuất";
            this.txtTitle.Values.Image = ((System.Drawing.Image)(resources.GetObject("txtTitle.Values.Image")));
            // 
            // Panel_Left
            // 
            this.Panel_Left.Controls.Add(this.PnSheet);
            this.Panel_Left.Controls.Add(this.PnTeam);
            this.Panel_Left.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Left.Location = new System.Drawing.Point(0, 92);
            this.Panel_Left.Name = "Panel_Left";
            this.Panel_Left.Size = new System.Drawing.Size(300, 676);
            this.Panel_Left.TabIndex = 147;
            // 
            // PnTeam
            // 
            this.PnTeam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.PnTeam.Controls.Add(this.LVTeam);
            this.PnTeam.Controls.Add(this.kryptonHeader3);
            this.PnTeam.Controls.Add(this.panel5);
            this.PnTeam.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PnTeam.Location = new System.Drawing.Point(0, 140);
            this.PnTeam.Name = "PnTeam";
            this.PnTeam.Size = new System.Drawing.Size(300, 536);
            this.PnTeam.TabIndex = 206;
            // 
            // LVTeam
            // 
            this.LVTeam.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVTeam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVTeam.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVTeam.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVTeam.FullRowSelect = true;
            this.LVTeam.GridLines = true;
            this.LVTeam.HideSelection = false;
            this.LVTeam.Location = new System.Drawing.Point(0, 30);
            this.LVTeam.Name = "LVTeam";
            this.LVTeam.Size = new System.Drawing.Size(300, 456);
            this.LVTeam.TabIndex = 206;
            this.LVTeam.UseCompatibleStateImageBehavior = false;
            this.LVTeam.View = System.Windows.Forms.View.Details;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 486);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(300, 50);
            this.panel5.TabIndex = 207;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Dock = System.Windows.Forms.DockStyle.Left;
            this.label9.Location = new System.Drawing.Point(300, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(4, 676);
            this.label9.TabIndex = 176;
            // 
            // GVTitle
            // 
            this.GVTitle.AutoSize = false;
            this.GVTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.GVTitle.Location = new System.Drawing.Point(304, 92);
            this.GVTitle.Name = "GVTitle";
            this.GVTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.GVTitle.Size = new System.Drawing.Size(962, 30);
            this.GVTitle.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.GVTitle.TabIndex = 214;
            this.GVTitle.Values.Description = "";
            this.GVTitle.Values.Heading = "Chi tiết ";
            // 
            // GVData
            // 
            this.GVData.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.GVData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GVData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.Location = new System.Drawing.Point(304, 122);
            this.GVData.Name = "GVData";
            this.GVData.RowHeadersWidth = 51;
            this.GVData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GVData.Size = new System.Drawing.Size(962, 596);
            this.GVData.TabIndex = 215;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.btn_Open);
            this.panel6.Controls.Add(this.btn_Save);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(304, 718);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(962, 50);
            this.panel6.TabIndex = 216;
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.Location = new System.Drawing.Point(837, 4);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Save.Size = new System.Drawing.Size(120, 40);
            this.btn_Save.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Save.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Save.TabIndex = 14;
            this.btn_Save.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Save.Values.Image")));
            this.btn_Save.Values.Text = "Cập nhật";
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // btn_Open
            // 
            this.btn_Open.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Open.Location = new System.Drawing.Point(711, 5);
            this.btn_Open.Name = "btn_Open";
            this.btn_Open.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Open.Size = new System.Drawing.Size(120, 40);
            this.btn_Open.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Open.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Open.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Open.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Open.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Open.TabIndex = 17;
            this.btn_Open.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_New.Values.Image")));
            this.btn_Open.Values.Text = "Chọn Excel";
            this.btn_Open.Click += new System.EventHandler(this.btn_Open_Click);
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1266, 50);
            this.panel1.TabIndex = 217;
            // 
            // PnSheet
            // 
            this.PnSheet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.PnSheet.Controls.Add(this.LVSheet);
            this.PnSheet.Controls.Add(this.kryptonHeader1);
            this.PnSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnSheet.Location = new System.Drawing.Point(0, 0);
            this.PnSheet.Name = "PnSheet";
            this.PnSheet.Size = new System.Drawing.Size(300, 140);
            this.PnSheet.TabIndex = 218;
            // 
            // LVSheet
            // 
            this.LVSheet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LVSheet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LVSheet.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LVSheet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LVSheet.FullRowSelect = true;
            this.LVSheet.GridLines = true;
            this.LVSheet.HideSelection = false;
            this.LVSheet.Location = new System.Drawing.Point(0, 30);
            this.LVSheet.Name = "LVSheet";
            this.LVSheet.Size = new System.Drawing.Size(300, 110);
            this.LVSheet.TabIndex = 235;
            this.LVSheet.UseCompatibleStateImageBehavior = false;
            this.LVSheet.View = System.Windows.Forms.View.Details;
            // 
            // kryptonHeader1
            // 
            this.kryptonHeader1.AutoSize = false;
            this.kryptonHeader1.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader1.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader1.Name = "kryptonHeader1";
            this.kryptonHeader1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader1.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader1.TabIndex = 234;
            this.kryptonHeader1.Values.Description = "";
            this.kryptonHeader1.Values.Heading = "Sheet Excel";
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btn_Up,
            this.btn_Down});
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(300, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 237;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "Tố/ nhóm";
            // 
            // btn_Up
            // 
            this.btn_Up.Image = ((System.Drawing.Image)(resources.GetObject("btn_Up.Image")));
            this.btn_Up.UniqueName = "ABE0F8FEF4FF4DE3448D0BAE7F1C6F22";
            // 
            // btn_Down
            // 
            this.btn_Down.Image = ((System.Drawing.Image)(resources.GetObject("btn_Down.Image")));
            this.btn_Down.UniqueName = "D6E0AB28C2CE49FBDD866175ABE4E274";
            // 
            // Frm_Import_Production_Time
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1266, 768);
            this.Controls.Add(this.GVData);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.GVTitle);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Panel_Left);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_Import_Production_Time";
            this.Text = "Frm_Import_Production_Time";
            this.Load += new System.EventHandler(this.Frm_Import_Production_Time_Load);
            this.Panel_Left.ResumeLayout(false);
            this.PnTeam.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.panel6.ResumeLayout(false);
            this.PnSheet.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader txtTitle;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel Panel_Left;
        private System.Windows.Forms.Panel PnTeam;
        private System.Windows.Forms.ListView LVTeam;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label9;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader GVTitle;
        private System.Windows.Forms.DataGridView GVData;
        private System.Windows.Forms.Panel panel6;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Save;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Open;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel PnSheet;
        private System.Windows.Forms.ListView LVSheet;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Up;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btn_Down;
    }
}