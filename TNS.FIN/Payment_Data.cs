﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.FIN
{
    public class Payment_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *, PaymentKey AS [Key] FROM FNC_Payment WHERE RecordStatus != 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(DateTime FromDate, DateTime ToDate, string ID)
        {


            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.PaymentKey, A.PaymentID, A.PaymentDate,
 dbo.Fn_GetCustomerID(A.ParentKey) AS CustomerID, 
CASE 
	WHEN B.Slug = 1 THEN dbo.Fn_GetFullName(A.ParentKey)
	WHEN B.Slug = 2 THEN dbo.Fn_GetCompanyName(A.ParentKey)
END AS CustomerName
FROM FNC_Payment A LEFT JOIN  CRM_Customer B ON A.ParentKey = B.CustomerKey
WHERE  A.RecordStatus <> 99";

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {

                zSQL += " AND PaymentDate between @FromDate and @ToDate";
            }
            if (ID.Trim().Length > 0)
            {
                zSQL += " AND PaymentID LIKE @ID";
            }


            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                }
                if (ID.Trim().Length > 0)
                {
                    zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = "%" + ID + "%";
                }


                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Payment_Employee()
        {
            DataTable zTable = new DataTable();
            string zSQL = @" SELECT TOP 20  A.*
FROM FNC_Payment A LEFT JOIN  dbo.HRM_Employee B ON A.ParentKey = B.EmployeeKey
WHERE A.RecordStatus <> 99 AND A.ParentKey != '' ORDER BY A.PaymentDate ASC  ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search_Payment_Employee(DateTime FromDate, DateTime ToDate, string ID, int CategoryKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @" SELECT A.*,C.TeamID,
  [dbo].[Fn_GetFullName](A.ParentKey) AS EmployeeName,B.EmployeeID 
FROM FNC_Payment A
 LEFT JOIN  dbo.HRM_Employee B ON A.ParentKey = B.EmployeeKey
 LEFT JOIN [dbo].[SYS_Team] c ON C.TeamKey =B.TeamKey
WHERE A.RecordStatus <> 99 AND  A.ParentKey !='' ";

            if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
            {

                zSQL += " AND PaymentDate between @FromDate and @ToDate ";
            }
            if (ID.Trim().Length > 0)
            {
                zSQL += " AND PaymentID LIKE @ID";
            }
            if (CategoryKey > 0)
            {
                zSQL += " AND CategoryKey = @CategoryKey";
            }
            zSQL += " Order by PaymentID DESC";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }
                if (ID.Trim().Length > 0)
                {
                    zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = "%" + ID + "%";
                }
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        //
        public static int CheckPaymentID(string PaymentID)
        {
            int zResult = 0;
            //---------- String SQL Access Database ---------------
            string zSQL = "SELECT COUNT(*) FROM FNC_Payment WHERE RecordStatus <> 99 AND PaymentID = @PaymentID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = PaymentID.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                //Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static string TramaID(string PaymentID)
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"DECLARE @AssetIDNew nvarchar(50)
	SELECT  @AssetIDNew = ISNULL(MAX(RIGHT(PaymentID,4)) + 1 ,1)  FROM FNC_Payment 
	WHERE LEN(PaymentID) = 11 AND ISNUMERIC(RIGHT(PaymentID,4)) = 1 and Left(PaymentID,6)= @PaymentID AND RecordStatus <> 99 
	SET @AssetIDNew =   @PaymentID +'-'+ RIGHT('000' + @AssetIDNew,4)
	select @AssetIDNew";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@PaymentID", SqlDbType.NVarChar).Value = PaymentID.Trim();
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                //Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public static DataTable ListPayment(int BranchKey, int DepartmentKey, int TeamKey, int CategoryKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 0, 0, 0);
            DataTable zTable = new DataTable();
            string zFilter = "";
            if (TeamKey > 0)
                zFilter += " AND A.TeamKey =@TeamKey ";
            if (DepartmentKey > 0)
                zFilter += " AND C.DepartmentKey =@DepartmentKey ";
            if (BranchKey > 0)
            {
                zFilter += " AND D.BranchKey = @BranchKey";
            }
            if (Search.Trim().Length > 0)
            {
                zFilter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            if (CategoryKey > 0)
            {
                zFilter += " AND A.CategoryKey =@CategoryKey ";
            }
            string zSQL = @";WITH #TEMP AS (
SELECT 
  *,
  [dbo].[Fn_GetFullName] (ParentKey) AS EmployeeName,
  [dbo].[Fn_GetEmployeeID](ParentKey)AS EmployeeID,
  [dbo].[Fn_TeamKeyWorkingHistory](ParentKey,@FromDate,@ToDate) AS TeamKey
 FROM FNC_Payment 
 WHERE RecordStatus<> 99 AND ParentKey !='' AND PaymentDate between @FromDate and @ToDate
)

SELECT A.PaymentKey,A.PaymentDate,A.PaymentID, A.EmployeeName,A.EmployeeID,B.TeamID,(E.CategoryID+'-'+E.CategoryName)AS Catgory,A.PaymentDescription ,A.AmountOrderMain,A.Note 
FROM #TEMP A
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
LEFT JOIN [dbo].[FNC_Fee_Category] E ON E.CategoryKey =A.CategoryKey
WHERE  1 = 1  @Parameter
ORDER BY A.PaymentDate ASC, A.PaymentID, E.Rank,  D.RANK, C.RANK, B.RANK, LEN(EmployeeID), EmployeeID DESC";

            zSQL = zSQL.Replace("@Parameter", zFilter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable DanhSachUngLuong(int BranchKey, int DepartmentKey, int TeamKey, int Type, string Search, DateTime DateView)
        {
            DateTime zFromDate = new DateTime(DateView.Year, DateView.Month, DateView.Day, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();

            string zFilter = "";
            if (TeamKey > 0)
                zFilter += " AND A.TeamKey =@TeamKey ";
            if (DepartmentKey > 0)
                zFilter += " AND C.DepartmentKey =@DepartmentKey ";
            if (BranchKey > 0)
            {
                zFilter += " AND D.BranchKey = @BranchKey";
            }
            if (Search.Trim().Length > 0)
            {
                zFilter += " AND ( A.EmployeeID LIKE @Search OR A.EmployeeName LIKE @Search )";
            }
            if (Type > 0)
            {
                if (Type == 1)
                    zFilter += " AND A.TeamKey = 96 ";
                if (Type == 2)
                    zFilter += " AND A.TeamKey != 96 ";
            }
            string zSQL = @"
--Declare  @FromDate Datetime ='2020-06-01 00:00:00'
--Declare  @ToDate Datetime ='2020-06-30 23:59:59'
 ;WITH #TEMP AS (
SELECT 
  *,
  [dbo].[Fn_GetFullName] (ParentKey) AS EmployeeName,
  [dbo].[Fn_GetEmployeeID](ParentKey)AS EmployeeID,
  [dbo].[Fn_TeamKeyWorkingHistory](ParentKey,@FromDate,@ToDate) AS TeamKey
 FROM FNC_Payment 
 WHERE RecordStatus<> 99 AND ParentKey !='' AND PaymentDate between @FromDate and @ToDate
)
SELECT A.EmployeeName,B.TeamID,A.EmployeeID,
A.AmountOrderMain,'' AS SignBy
FROM #TEMP A
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
LEFT JOIN [dbo].[FNC_Fee_Category] E ON E.CategoryKey =A.CategoryKey
WHERE E.CategoryID='UNGLUONG' @Parameter
ORDER BY   D.RANK, C.RANK, B.RANK, LEN(EmployeeID), EmployeeID DESC";

            zSQL = zSQL.Replace("@Parameter", zFilter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

    }
}
