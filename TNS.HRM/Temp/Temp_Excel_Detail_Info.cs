﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Temp_Excel_Detail_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _ExcelKey = 0;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private DateTime _BeginTime;
        private DateTime _EndTime;
        private float _OffTime =0;
        private float _DifferentTime=0;
        private float _TotalTime =0;
        private float _OverTime = 0;

        private string _Description = "";
        private int _CodeKey = 0;
        private string _CodeName = "";
        private int _RiceKey = 0;
        private string _RiceName = "";
        private string _Code = "";
        private double _Money = 0;
        private string _RoleID = "";
        private string _Message = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private float _Value = 0;
        private float _Money_Rice = 0;
        private int _Hand = 0;
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public DateTime BeginTime
        {
            get { return _BeginTime; }
            set { _BeginTime = value; }
        }
        public DateTime EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }
        public float OffTime
        {
            get { return _OffTime; }
            set { _OffTime = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string CodeName
        {
            get { return _CodeName; }
            set { _CodeName = value; }
        }
        public int RiceKey
        {
            get { return _RiceKey; }
            set { _RiceKey = value; }
        }
        public string RiceName
        {
            get { return _RiceName; }
            set { _RiceName = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }

        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }

        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }

        public int CodeKey
        {
            get
            {
                return _CodeKey;
            }

            set
            {
                _CodeKey = value;
            }
        }

        public int ExcelKey
        {
            get
            {
                return _ExcelKey;
            }

            set
            {
                _ExcelKey = value;
            }
        }

        public float DifferentTime
        {
            get
            {
                return _DifferentTime;
            }

            set
            {
                _DifferentTime = value;
            }
        }

        public float TotalTime
        {
            get
            {
                return _TotalTime;
            }

            set
            {
                _TotalTime = value;
            }
        }

        public float OverTime
        {
            get
            {
                return _OverTime;
            }

            set
            {
                _OverTime = value;
            }
        }

        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }

        public string Code
        {
            get
            {
                return _Code;
            }

            set
            {
                _Code = value;
            }
        }


        public float Value
        {
            get
            {
                return _Value;
            }

            set
            {
                _Value = value;
            }
        }

        public float Money_Rice
        {
            get
            {
                return _Money_Rice;
            }

            set
            {
                _Money_Rice = value;
            }
        }

        public int Hand
        {
            get
            {
                return _Hand;
            }

            set
            {
                _Hand = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Temp_Excel_Detail_Info()
        {
        }
        public Temp_Excel_Detail_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM Temp_Excel_Detail WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString().Trim());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["BeginTime"] != DBNull.Value)
                        _BeginTime = (DateTime)zReader["BeginTime"];
                    if (zReader["EndTime"] != DBNull.Value)
                        _EndTime = (DateTime)zReader["EndTime"];
                    if (zReader["OffTime"] != DBNull.Value)
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    if (zReader["DifferentTime"] != DBNull.Value)
                        _DifferentTime = float.Parse(zReader["DifferentTime"].ToString());
                    if (zReader["TotalTime"] != DBNull.Value)
                        _TotalTime = float.Parse(zReader["TotalTime"].ToString());
                    if (zReader["OverTime"] != DBNull.Value)
                        _OverTime = float.Parse(zReader["OverTime"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString().Trim());
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["RiceKey"] != DBNull.Value)
                        _RiceKey = int.Parse(zReader["RiceKey"].ToString());
                    _RiceName = zReader["RiceName"].ToString();
                    _Code = zReader["Code"].ToString();
                    if (zReader["Value"] != DBNull.Value)
                        _Value = float.Parse(zReader["Value"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = float.Parse(zReader["Money"].ToString());
                    if (zReader["Money_Rice"] != DBNull.Value)
                        _Money_Rice = float.Parse(zReader["Money_Rice"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Hand"] != DBNull.Value)
                        _Hand = int.Parse(zReader["Hand"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Temp_Excel_Detail_Info(string EmployeeID, DateTime FromDate, DateTime ToDate)
        {
            string zSQL = "SELECT * FROM Temp_Excel_Detail WHERE  EmployeeID = @EmployeeID AND BeginTime BETWEEN @FromDate AND @ToDate AND RecordStatus <>99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = FromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = ToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ExcelKey"] != DBNull.Value)
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString().Trim());
                    _EmployeeKey = zReader["EmployeeKey"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["BeginTime"] != DBNull.Value)
                        _BeginTime = (DateTime)zReader["BeginTime"];
                    if (zReader["EndTime"] != DBNull.Value)
                        _EndTime = (DateTime)zReader["EndTime"];
                    if (zReader["OffTime"] != DBNull.Value)
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    if (zReader["DifferentTime"] != DBNull.Value)
                        _DifferentTime = float.Parse(zReader["DifferentTime"].ToString());
                    if (zReader["TotalTime"] != DBNull.Value)
                        _TotalTime = float.Parse(zReader["TotalTime"].ToString());
                    if (zReader["OverTime"] != DBNull.Value)
                        _OverTime = float.Parse(zReader["OverTime"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CodeKey"] != DBNull.Value)
                        _CodeKey = int.Parse(zReader["CodeKey"].ToString().Trim());
                    _CodeName = zReader["CodeName"].ToString().Trim();
                    if (zReader["RiceKey"] != DBNull.Value)
                        _RiceKey = int.Parse(zReader["RiceKey"].ToString());
                    _RiceName = zReader["RiceName"].ToString();
                    _Code = zReader["Code"].ToString();
                    if (zReader["Value"] != DBNull.Value)
                        _Value = float.Parse(zReader["Value"].ToString());
                    if (zReader["Money"] != DBNull.Value)
                        _Money = float.Parse(zReader["Money"].ToString());
                    if (zReader["Money_Rice"] != DBNull.Value)
                        _Money_Rice = float.Parse(zReader["Money_Rice"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Hand"] != DBNull.Value)
                        _Hand = int.Parse(zReader["Hand"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public Temp_Excel_Detail_Info(DataRow InRow)
        {
            try
            {

                _AutoKey = int.Parse(InRow["AutoKey"].ToString());
                if (InRow["ExcelKey"] != DBNull.Value)
                    _ExcelKey = int.Parse(InRow["ExcelKey"].ToString().Trim());
                _EmployeeKey = InRow["EmployeeKey"].ToString();
                _EmployeeID = InRow["EmployeeID"].ToString().Trim();
                _EmployeeName = InRow["EmployeeName"].ToString().Trim();
                if (InRow["BeginTime"] != DBNull.Value)
                    _BeginTime = (DateTime)InRow["BeginTime"];
                if (InRow["EndTime"] != DBNull.Value)
                    _EndTime = (DateTime)InRow["EndTime"];
                if (InRow["OffTime"] != DBNull.Value)
                    _OffTime = float.Parse(InRow["OffTime"].ToString());
                if (InRow["DifferentTime"] != DBNull.Value)
                    _DifferentTime = float.Parse(InRow["DifferentTime"].ToString());
                if (InRow["TotalTime"] != DBNull.Value)
                    _TotalTime = float.Parse(InRow["TotalTime"].ToString());
                if (InRow["OverTime"] != DBNull.Value)
                    _OverTime = float.Parse(InRow["OverTime"].ToString());
                _Description = InRow["Description"].ToString().Trim();
                if (InRow["CodeKey"] != DBNull.Value)
                    _CodeKey = int.Parse(InRow["CodeKey"].ToString().Trim());
                _CodeName = InRow["CodeName"].ToString().Trim();
                if (InRow["RiceKey"] != DBNull.Value)
                    _RiceKey = int.Parse(InRow["RiceKey"].ToString());
                _RiceName = InRow["RiceName"].ToString();
                _Code = InRow["Code"].ToString();

                if (InRow["Value"] != DBNull.Value)
                    _Value = float.Parse(InRow["Value"].ToString());
                if (InRow["Money"] != DBNull.Value)
                    _Money = float.Parse(InRow["Money"].ToString());
                if (InRow["Money_Rice"] != DBNull.Value)
                    _Money_Rice = float.Parse(InRow["Money_Rice"].ToString());
                if (InRow["RecordStatus"] != DBNull.Value)
                    _RecordStatus = int.Parse(InRow["RecordStatus"].ToString());
                if (InRow["Hand"] != DBNull.Value)
                    _Hand = int.Parse(InRow["Hand"].ToString());
                if (InRow["CreatedOn"] != DBNull.Value)
                {
                    _CreatedOn = (DateTime)InRow["CreatedOn"];
                }

                _CreatedBy = InRow["CreatedBy"].ToString();
                _CreatedName = InRow["CreatedName"].ToString();
                if (InRow["ModifiedOn"] != DBNull.Value)
                {
                    _ModifiedOn = (DateTime)InRow["ModifiedOn"];
                }

                _ModifiedBy = InRow["ModifiedBy"].ToString();
                _ModifiedName = InRow["ModifiedName"].ToString();

            }
            catch (Exception Err) { _Message = Err.ToString(); }
        }

        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @" INSERT INTO Temp_Excel_Detail
(ExcelKey,EmployeeKey, EmployeeID,EmployeeName, BeginTime, EndTime,OffTime,DifferentTime,TotalTime,OverTime,Description,CodeKey,Hand,
CodeName, RiceKey, RiceName,Code,Value, Money,Money_Rice,RecordStatus,CreatedOn, CreatedBy,CreatedName,ModifiedOn ,ModifiedBy, ModifiedName ) 
VALUES(
@ExcelKey,@EmployeeKey, @EmployeeID,@EmployeeName,@BeginTime,@EndTime, @OffTime,@DifferentTime,@TotalTime,@OverTime,@Description,@CodeKey,@Hand, @CodeName,@RiceKey,
@RiceName,@Code,@Value, @Money ,@Money_Rice,0,GETDATE(),@CreatedBy,@CreatedName,GETDATE(),@ModifiedBy,@ModifiedName)
SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (ExcelKey > 0)
                    zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                else
                    zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = DBNull.Value;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_BeginTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = _BeginTime;
                if (_EndTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = _EndTime;

                    zCommand.Parameters.Add("@OffTime", SqlDbType.Float).Value = _OffTime;

                    zCommand.Parameters.Add("@DifferentTime", SqlDbType.Float).Value = _DifferentTime;

                zCommand.Parameters.Add("@TotalTime", SqlDbType.Float).Value = _TotalTime;

                zCommand.Parameters.Add("@OverTime ", SqlDbType.Float).Value = _OverTime;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = _RiceName;
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = _RiceKey;
                zCommand.Parameters.Add("@Code", SqlDbType.NVarChar).Value = _Code;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = _Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Money_Rice", SqlDbType.Float).Value = _Money_Rice;
                zCommand.Parameters.Add("@Hand", SqlDbType.Int).Value = _Hand;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                Message = zCommand.ExecuteScalar().ToString();

                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @"UPDATE dbo.Temp_Excel_Detail SET 
            ExcelKey = @ExcelKey,
			EmployeeKey = @EmployeeKey,
			EmployeeID = @EmployeeID,
            EmployeeName = @EmployeeName,
			BeginTime = @BeginTime,
			EndTime = @EndTime,
			OffTime = @OffTime,
            DifferentTime = @DifferentTime,
            TotalTime = @TotalTime,
            OverTime = @OverTime,
			Description = @Description,
            CodeKey =@CodeKey,
			CodeName = @CodeName,
			RiceKey = @RiceKey,
			RiceName = @RiceName,
            Code = @Code,
            Value = @Value,
            Money = @Money,
            Money_Rice = @Money_Rice,
            RecordStatus =1,
            Hand =@Hand,
            ModifiedOn = GETDATE(),
            ModifiedBy = @ModifiedBy,
            ModifiedName = @ModifiedName
            WHERE AutoKey = @AutoKey 
            SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_AutoKey > 0)
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                else
                    zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = DBNull.Value;
                if (ExcelKey > 0)
                    zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                else
                    zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = DBNull.Value;
                if (_EmployeeKey.Length > 0)
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                else
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_BeginTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = _BeginTime;
                if (_EndTime == DateTime.MinValue)
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = _EndTime;

                    zCommand.Parameters.Add("@OffTime", SqlDbType.Float).Value = _OffTime;
                    zCommand.Parameters.Add("@DifferentTime", SqlDbType.Float).Value = _DifferentTime;

                zCommand.Parameters.Add("@TotalTime", SqlDbType.Float).Value = _TotalTime;

                zCommand.Parameters.Add("@OverTime ", SqlDbType.Float).Value = _OverTime;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CodeKey", SqlDbType.Int).Value = _CodeKey;
                zCommand.Parameters.Add("@CodeName", SqlDbType.NVarChar).Value = _CodeName.Trim();
                zCommand.Parameters.Add("@RiceName", SqlDbType.NVarChar).Value = _RiceName;
                zCommand.Parameters.Add("@RiceKey", SqlDbType.Int).Value = _RiceKey;
                zCommand.Parameters.Add("@Code", SqlDbType.NVarChar).Value = _Code;
                zCommand.Parameters.Add("@Value", SqlDbType.Float).Value = _Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Float).Value = _Money;
                zCommand.Parameters.Add("@Money_Rice", SqlDbType.Float).Value = _Money_Rice;
                zCommand.Parameters.Add("@Hand", SqlDbType.Int).Value = _Hand;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @" UPDATE dbo.Temp_Excel_Detail SET
      [RecordStatus] = 99,
      [ModifiedOn] = GETDATE(),
      [ModifiedBy] = @ModifiedBy,
      [ModifiedName] = @ModifiedName
      WHERE[AutoKey] = @AutoKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete_Working_Time_Rice(int ExcelKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE dbo.HRM_Employee_Time SET RecordStatus = 99 WHERE ExcelKey = @ExcelKey
 UPDATE dbo.HRM_Time_Working SET RecordStatus = 99 WHERE WorkingID = @ExcelKey
 UPDATE dbo.HRM_Employee_Rice SET RecordStatus = 99 WHERE ExcelKey = @ExcelKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = ExcelKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
