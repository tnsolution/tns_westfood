﻿namespace TNS.WinApp
{
    partial class Frm_TimePermit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_TimePermit));
            this.panel1 = new System.Windows.Forms.Panel();
            this.LV_List = new System.Windows.Forms.ListView();
            this.Panel_Left_Search = new System.Windows.Forms.Panel();
            this.btn_Search = new System.Windows.Forms.Button();
            this.txt_Search = new System.Windows.Forms.TextBox();
            this.dte_Month = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.LV_Detail = new System.Windows.Forms.ListView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_New = new System.Windows.Forms.Button();
            this.btn_Del = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.btn_Approve = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dte_ToDay = new System.Windows.Forms.DateTimePicker();
            this.dte_FromDay = new System.Windows.Forms.DateTimePicker();
            this.dte_CheckTime = new System.Windows.Forms.DateTimePicker();
            this.cbo_Code = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_EmployeeName = new System.Windows.Forms.TextBox();
            this.txt_CardID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_Description = new System.Windows.Forms.TextBox();
            this.txt_NumberDay = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbl_TitleDetail = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.Panel_Left_Search.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.LV_List);
            this.panel1.Controls.Add(this.Panel_Left_Search);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.splitter1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(303, 530);
            this.panel1.TabIndex = 1;
            // 
            // LV_List
            // 
            this.LV_List.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_List.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_List.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LV_List.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_List.FullRowSelect = true;
            this.LV_List.GridLines = true;
            this.LV_List.Location = new System.Drawing.Point(0, 111);
            this.LV_List.Name = "LV_List";
            this.LV_List.Size = new System.Drawing.Size(298, 419);
            this.LV_List.TabIndex = 1;
            this.LV_List.UseCompatibleStateImageBehavior = false;
            this.LV_List.View = System.Windows.Forms.View.Details;
            this.LV_List.Click += new System.EventHandler(this.LV_List_Click);
            // 
            // Panel_Left_Search
            // 
            this.Panel_Left_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Left_Search.Controls.Add(this.btn_Search);
            this.Panel_Left_Search.Controls.Add(this.txt_Search);
            this.Panel_Left_Search.Controls.Add(this.dte_Month);
            this.Panel_Left_Search.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Left_Search.Location = new System.Drawing.Point(0, 39);
            this.Panel_Left_Search.Name = "Panel_Left_Search";
            this.Panel_Left_Search.Size = new System.Drawing.Size(298, 72);
            this.Panel_Left_Search.TabIndex = 0;
            // 
            // btn_Search
            // 
            this.btn_Search.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Search.BackgroundImage")));
            this.btn_Search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Search.FlatAppearance.BorderSize = 0;
            this.btn_Search.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Search.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.ForeColor = System.Drawing.Color.White;
            this.btn_Search.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Search.Location = new System.Drawing.Point(201, 35);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(33, 27);
            this.btn_Search.TabIndex = 2;
            this.btn_Search.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Search.UseVisualStyleBackColor = false;
            // 
            // txt_Search
            // 
            this.txt_Search.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_Search.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Search.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(68)))), ((int)(((byte)(67)))));
            this.txt_Search.Location = new System.Drawing.Point(12, 38);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.Size = new System.Drawing.Size(169, 21);
            this.txt_Search.TabIndex = 1;
            // 
            // dte_Month
            // 
            this.dte_Month.CalendarTrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dte_Month.CustomFormat = "MM/yyyyy";
            this.dte_Month.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_Month.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_Month.Location = new System.Drawing.Point(12, 9);
            this.dte_Month.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dte_Month.Name = "dte_Month";
            this.dte_Month.ShowUpDown = true;
            this.dte_Month.Size = new System.Drawing.Size(169, 21);
            this.dte_Month.TabIndex = 0;
            this.dte_Month.ValueChanged += new System.EventHandler(this.dte_Month_ValueChanged);
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(199)))), ((int)(((byte)(231)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(298, 39);
            this.label6.TabIndex = 5;
            this.label6.Text = "NHÂN SỰ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.DodgerBlue;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(298, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 530);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(304, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(832, 530);
            this.panel2.TabIndex = 2;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.LV_Detail);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 269);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(832, 207);
            this.panel3.TabIndex = 188;
            // 
            // LV_Detail
            // 
            this.LV_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Detail.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.LV_Detail.FullRowSelect = true;
            this.LV_Detail.GridLines = true;
            this.LV_Detail.Location = new System.Drawing.Point(0, 0);
            this.LV_Detail.Name = "LV_Detail";
            this.LV_Detail.Size = new System.Drawing.Size(832, 207);
            this.LV_Detail.TabIndex = 0;
            this.LV_Detail.UseCompatibleStateImageBehavior = false;
            this.LV_Detail.View = System.Windows.Forms.View.Details;
            this.LV_Detail.Click += new System.EventHandler(this.LV_Detail_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox2.Controls.Add(this.btn_New);
            this.groupBox2.Controls.Add(this.btn_Del);
            this.groupBox2.Controls.Add(this.btn_Save);
            this.groupBox2.Controls.Add(this.btn_Approve);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.groupBox2.Location = new System.Drawing.Point(0, 476);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(832, 54);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thao tác";
            // 
            // btn_New
            // 
            this.btn_New.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_New.BackgroundImage")));
            this.btn_New.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_New.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_New.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_New.FlatAppearance.BorderSize = 0;
            this.btn_New.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_New.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_New.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_New.Font = new System.Drawing.Font("Arial", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.btn_New.ForeColor = System.Drawing.Color.Black;
            this.btn_New.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_New.Location = new System.Drawing.Point(8, 21);
            this.btn_New.Name = "btn_New";
            this.btn_New.Size = new System.Drawing.Size(67, 25);
            this.btn_New.TabIndex = 0;
            this.btn_New.Text = "LÀM MỚI";
            this.btn_New.UseVisualStyleBackColor = true;
            // 
            // btn_Del
            // 
            this.btn_Del.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Del.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Del.BackgroundImage")));
            this.btn_Del.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Del.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Del.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Del.FlatAppearance.BorderSize = 0;
            this.btn_Del.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Del.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Del.Font = new System.Drawing.Font("Arial", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.btn_Del.ForeColor = System.Drawing.Color.Black;
            this.btn_Del.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Del.Location = new System.Drawing.Point(753, 21);
            this.btn_Del.Name = "btn_Del";
            this.btn_Del.Size = new System.Drawing.Size(67, 25);
            this.btn_Del.TabIndex = 2;
            this.btn_Del.Text = "XÓA";
            this.btn_Del.UseVisualStyleBackColor = true;
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Save.BackgroundImage")));
            this.btn_Save.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Save.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Save.FlatAppearance.BorderSize = 0;
            this.btn_Save.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Save.Font = new System.Drawing.Font("Arial", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.btn_Save.ForeColor = System.Drawing.Color.Black;
            this.btn_Save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Save.Location = new System.Drawing.Point(680, 21);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(67, 25);
            this.btn_Save.TabIndex = 1;
            this.btn_Save.Text = "CẬP NHẬT";
            this.btn_Save.UseVisualStyleBackColor = true;
            // 
            // btn_Approve
            // 
            this.btn_Approve.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Approve.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Approve.BackgroundImage")));
            this.btn_Approve.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Approve.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Approve.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Approve.FlatAppearance.BorderSize = 0;
            this.btn_Approve.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Approve.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.btn_Approve.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Approve.Font = new System.Drawing.Font("Arial", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.btn_Approve.ForeColor = System.Drawing.Color.Black;
            this.btn_Approve.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Approve.Location = new System.Drawing.Point(680, 21);
            this.btn_Approve.Name = "btn_Approve";
            this.btn_Approve.Size = new System.Drawing.Size(67, 25);
            this.btn_Approve.TabIndex = 3;
            this.btn_Approve.Text = "DUYỆT";
            this.btn_Approve.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.groupBox1.Controls.Add(this.dte_ToDay);
            this.groupBox1.Controls.Add(this.dte_FromDay);
            this.groupBox1.Controls.Add(this.dte_CheckTime);
            this.groupBox1.Controls.Add(this.cbo_Code);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txt_EmployeeName);
            this.groupBox1.Controls.Add(this.txt_CardID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txt_Description);
            this.groupBox1.Controls.Add(this.txt_NumberDay);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.lbl_TitleDetail);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox1.Location = new System.Drawing.Point(0, 39);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(832, 230);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin";
            // 
            // dte_ToDay
            // 
            this.dte_ToDay.CalendarTrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dte_ToDay.CustomFormat = "dd/MM/yyyy  HH:mm";
            this.dte_ToDay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_ToDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_ToDay.Location = new System.Drawing.Point(469, 64);
            this.dte_ToDay.Name = "dte_ToDay";
            this.dte_ToDay.Size = new System.Drawing.Size(206, 21);
            this.dte_ToDay.TabIndex = 5;
            this.dte_ToDay.ValueChanged += new System.EventHandler(this.dte_ToDay_ValueChanged);
            // 
            // dte_FromDay
            // 
            this.dte_FromDay.CalendarTrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dte_FromDay.CustomFormat = "dd/MM/yyyy  HH:mm";
            this.dte_FromDay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_FromDay.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_FromDay.Location = new System.Drawing.Point(469, 27);
            this.dte_FromDay.Name = "dte_FromDay";
            this.dte_FromDay.Size = new System.Drawing.Size(206, 21);
            this.dte_FromDay.TabIndex = 4;
            // 
            // dte_CheckTime
            // 
            this.dte_CheckTime.CalendarTrailingForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dte_CheckTime.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dte_CheckTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dte_CheckTime.Location = new System.Drawing.Point(109, 20);
            this.dte_CheckTime.Name = "dte_CheckTime";
            this.dte_CheckTime.Size = new System.Drawing.Size(221, 21);
            this.dte_CheckTime.TabIndex = 0;
            // 
            // cbo_Code
            // 
            this.cbo_Code.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.cbo_Code.FormattingEnabled = true;
            this.cbo_Code.Location = new System.Drawing.Point(109, 136);
            this.cbo_Code.Name = "cbo_Code";
            this.cbo_Code.Size = new System.Drawing.Size(221, 23);
            this.cbo_Code.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 15);
            this.label4.TabIndex = 111;
            this.label4.Text = "Ngày đề nghị";
            // 
            // txt_EmployeeName
            // 
            this.txt_EmployeeName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_EmployeeName.Location = new System.Drawing.Point(109, 95);
            this.txt_EmployeeName.Name = "txt_EmployeeName";
            this.txt_EmployeeName.ReadOnly = true;
            this.txt_EmployeeName.Size = new System.Drawing.Size(221, 21);
            this.txt_EmployeeName.TabIndex = 2;
            // 
            // txt_CardID
            // 
            this.txt_CardID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_CardID.Location = new System.Drawing.Point(109, 59);
            this.txt_CardID.Name = "txt_CardID";
            this.txt_CardID.ReadOnly = true;
            this.txt_CardID.Size = new System.Drawing.Size(221, 21);
            this.txt_CardID.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(374, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 15);
            this.label5.TabIndex = 112;
            this.label5.Text = "Từ ngày ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(376, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 15);
            this.label3.TabIndex = 113;
            this.label3.Text = "Số ngày nghỉ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(376, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 15);
            this.label7.TabIndex = 114;
            this.label7.Text = "Đến ngày";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 115;
            this.label2.Text = "Họ và tên";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(29, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 15);
            this.label8.TabIndex = 116;
            this.label8.Text = "Mã thẻ";
            // 
            // txt_Description
            // 
            this.txt_Description.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_Description.Location = new System.Drawing.Point(469, 141);
            this.txt_Description.Multiline = true;
            this.txt_Description.Name = "txt_Description";
            this.txt_Description.Size = new System.Drawing.Size(206, 47);
            this.txt_Description.TabIndex = 7;
            // 
            // txt_NumberDay
            // 
            this.txt_NumberDay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(230)))), ((int)(((byte)(230)))));
            this.txt_NumberDay.Location = new System.Drawing.Point(469, 104);
            this.txt_NumberDay.Name = "txt_NumberDay";
            this.txt_NumberDay.ReadOnly = true;
            this.txt_NumberDay.Size = new System.Drawing.Size(206, 21);
            this.txt_NumberDay.TabIndex = 6;
            this.txt_NumberDay.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 15);
            this.label9.TabIndex = 117;
            this.label9.Text = "Loại phép";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(380, 142);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 15);
            this.label11.TabIndex = 118;
            this.label11.Text = "Nội dung";
            // 
            // lbl_TitleDetail
            // 
            this.lbl_TitleDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(199)))), ((int)(((byte)(231)))));
            this.lbl_TitleDetail.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbl_TitleDetail.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.lbl_TitleDetail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.lbl_TitleDetail.Location = new System.Drawing.Point(3, 191);
            this.lbl_TitleDetail.Name = "lbl_TitleDetail";
            this.lbl_TitleDetail.Size = new System.Drawing.Size(826, 36);
            this.lbl_TitleDetail.TabIndex = 12;
            this.lbl_TitleDetail.Text = "CHI TIẾT NGHỈ PHÉP THÁNG ";
            this.lbl_TitleDetail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(199)))), ((int)(((byte)(231)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(9)))), ((int)(((byte)(1)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(832, 39);
            this.label1.TabIndex = 6;
            this.label1.Text = "CÔNG NGÀY NGÀY NGHỈ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frm_TimePermit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 530);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Frm_TimePermit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BẢNG CÔNG NGÀY VÀ NGÀY NGHỈ";
            this.Load += new System.EventHandler(this.Frm_TimeWorking_Load);
            this.panel1.ResumeLayout(false);
            this.Panel_Left_Search.ResumeLayout(false);
            this.Panel_Left_Search.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView LV_List;
        private System.Windows.Forms.Panel Panel_Left_Search;
        private System.Windows.Forms.Button btn_Search;
        private System.Windows.Forms.TextBox txt_Search;
        private System.Windows.Forms.DateTimePicker dte_Month;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListView LV_Detail;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_New;
        private System.Windows.Forms.Button btn_Del;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.Button btn_Approve;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbl_TitleDetail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dte_ToDay;
        private System.Windows.Forms.DateTimePicker dte_FromDay;
        private System.Windows.Forms.DateTimePicker dte_CheckTime;
        private System.Windows.Forms.ComboBox cbo_Code;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txt_EmployeeName;
        private System.Windows.Forms.TextBox txt_CardID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_Description;
        private System.Windows.Forms.TextBox txt_NumberDay;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
    }
}