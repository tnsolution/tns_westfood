﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TN.Library.HumanResource
{
    public class Employee_Time_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _TeamKey = 0;
        private string _TeamName = "";
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private string _EmployeeID = "";
        private DateTime _BeginTime;
        private DateTime _EndTime;
        private float _OffTime = 0;
        private float _DifferentTime = 0;
        private float _TotalTime = 0;
        private float _OverTime = 0;
        private DateTime _CheckTime;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private int _ExcelKey = 0;
        private float _Value = 0;
        private float _Money = 0;
        #endregion
        #region [ Properties ]



        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public DateTime BeginTime
        {
            get { return _BeginTime; }
            set { _BeginTime = value; }
        }
        public DateTime EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }
        public float OffTime
        {
            get { return _OffTime; }
            set { _OffTime = value; }
        }
        public float TotalTime
        {
            get { return _TotalTime; }
            set { _TotalTime = value; }
        }
        public float OverTime
        {
            get { return _OverTime; }
            set { _OverTime = value; }
        }
        public DateTime CheckTime
        {
            get { return _CheckTime; }
            set { _CheckTime = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }

        public int ExcelKey
        {
            get
            {
                return _ExcelKey;
            }

            set
            {
                _ExcelKey = value;
            }
        }

        public float DifferentTime
        {
            get
            {
                return _DifferentTime;
            }

            set
            {
                _DifferentTime = value;
            }
        }

        public float Value
        {
            get
            {
                return _Value;
            }

            set
            {
                _Value = value;
            }
        }

        public float Money
        {
            get
            {
                return _Money;
            }

            set
            {
                _Money = value;
            }
        }



        #endregion
        #region [ Constructor Get Information ]
        public Employee_Time_Info()
        {
        }
        public Employee_Time_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Employee_Time WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    if (zReader["TeamKey"] != DBNull.Value)
                    {
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    }
                    if (zReader["ExcelKey"] != DBNull.Value)
                    {
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    }

                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    if (zReader["BeginTime"] != DBNull.Value)
                    {
                        _BeginTime = (DateTime)zReader["BeginTime"];
                    }

                    if (zReader["EndTime"] != DBNull.Value)
                    {
                        _EndTime = (DateTime)zReader["EndTime"];
                    }

                    if (zReader["OffTime"] != DBNull.Value)
                    {
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    }

                    if (zReader["TotalTime"] != DBNull.Value)
                    {
                        _TotalTime = float.Parse(zReader["TotalTime"].ToString());
                    }

                    if (zReader["OverTime"] != DBNull.Value)
                    {
                        _OverTime = float.Parse(zReader["OverTime"].ToString());
                    }

                    if (zReader["CheckTime"] != DBNull.Value)
                    {
                        _CheckTime = (DateTime)zReader["CheckTime"];
                    }

                    _Description = zReader["Description"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }

                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["DifferentTime"] != DBNull.Value)
                    {
                        _DifferentTime = float.Parse(zReader["DifferentTime"].ToString());
                    }
                    if (zReader["Value"] != DBNull.Value)
                    {
                        _Value = float.Parse(zReader["Value"].ToString());
                    }
                    if (zReader["Money"] != DBNull.Value)
                    {
                        _Money = float.Parse(zReader["Money"].ToString());
                    }
                    if (zReader["OffTime"] != DBNull.Value)
                    {
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    }
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Employee_Time_Info(string EmployeeKey, DateTime BeginTime, DateTime EndTime)
        {
            string zSQL = "SELECT * FROM HRM_Employee_Time WHERE EmployeeKey = @EmployeeKey AND BeginTime = @BeginTime AND EndTime = @EndTime AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (EmployeeKey.Trim().Length == 0)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(EmployeeKey);
                }
                zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = BeginTime;
                zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = EndTime;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                    {
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    }

                    if (zReader["TeamKey"] != DBNull.Value)
                    {
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    }
                    if (zReader["ExcelKey"] != DBNull.Value)
                    {
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    }
                    _EmployeeName = zReader["EmployeeName"].ToString();
                    _EmployeeID = zReader["EmployeeID"].ToString();
                    _TeamName = zReader["TeamName"].ToString();
                    if (zReader["BeginTime"] != DBNull.Value)
                    {
                        _BeginTime = (DateTime)zReader["BeginTime"];
                    }

                    if (zReader["EndTime"] != DBNull.Value)
                    {
                        _EndTime = (DateTime)zReader["EndTime"];
                    }

                    if (zReader["OffTime"] != DBNull.Value)
                    {
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    }

                    if (zReader["TotalTime"] != DBNull.Value)
                    {
                        _TotalTime = float.Parse(zReader["TotalTime"].ToString());
                    }

                    if (zReader["OverTime"] != DBNull.Value)
                    {
                        _OverTime = float.Parse(zReader["OverTime"].ToString());
                    }

                    if (zReader["CheckTime"] != DBNull.Value)
                    {
                        _CheckTime = (DateTime)zReader["CheckTime"];
                    }

                    _Description = zReader["Description"].ToString();
                    if (zReader["RecordStatus"] != DBNull.Value)
                    {
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    }
                    if (zReader["ExcelKey"] != DBNull.Value)
                    {
                        _ExcelKey = int.Parse(zReader["ExcelKey"].ToString());
                    }
                    if (zReader["CreatedOn"] != DBNull.Value)
                    {
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    }

                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                    {
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    }

                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    if (zReader["DifferentTime"] != DBNull.Value)
                    {
                        _DifferentTime = float.Parse(zReader["DifferentTime"].ToString());
                    }
                    if (zReader["Value"] != DBNull.Value)
                    {
                        _Value = float.Parse(zReader["Value"].ToString());
                    }
                    if (zReader["Money"] != DBNull.Value)
                    {
                        _Money = float.Parse(zReader["Money"].ToString());
                    }
                    if (zReader["OffTime"] != DBNull.Value)
                    {
                        _OffTime = float.Parse(zReader["OffTime"].ToString());
                    }
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = @" INSERT INTO dbo.HRM_Employee_Time (TeamKey, TeamName,ExcelKey,EmployeeKey,EmployeeName, EmployeeID, BeginTime, EndTime, OffTime, 
TotalTime, OverTime, CheckTime,Description, RecordStatus,DifferentTime,Value,Money,
CreatedOn, CreatedBy,CreatedName, ModifiedOn, ModifiedBy, ModifiedName) 
VALUES (
@TeamKey, @TeamName,@ExcelKey,@EmployeeKey, @EmployeeName, @EmployeeID, @BeginTime,  @EndTime,  @OffTime,
@TotalTime, @OverTime, @CheckTime,@Description, 0,@DifferentTime,@Value,@Money,
GETDATE(),@CreatedBy, @CreatedName,GETDATE(),@ModifiedBy, @ModifiedName  ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                if (_EmployeeKey.Length > 0)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }

                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;

                if (_BeginTime == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = _BeginTime;
                }

                if (_EndTime == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = _EndTime;
                }

                zCommand.Parameters.Add("@OffTime", SqlDbType.Float).Value = _OffTime;
                zCommand.Parameters.Add("@TotalTime", SqlDbType.Float).Value = _TotalTime;

                zCommand.Parameters.Add("@OverTime", SqlDbType.Float).Value = _OverTime;

                if (_CheckTime == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@CheckTime", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@CheckTime", SqlDbType.DateTime).Value = _CheckTime;
                }

                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                zCommand.Parameters.Add("@DifferentTime", SqlDbType.Float).Value = _DifferentTime;
                zCommand.Parameters.Add("@Value", SqlDbType.Money).Value = _Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = @" UPDATE dbo.HRM_Employee_Time SET 
			TeamKey = @TeamKey,
			TeamName = @TeamName,
			EmployeeKey = @EmployeeKey,
			EmployeeName = @EmployeeName,
			EmployeeID = @EmployeeID,
			BeginTime = @BeginTime,
			EndTime = @EndTime,
			OffTime = @OffTime,
			TotalTime = @TotalTime,
			OverTime = @OverTime,
			CheckTime = @CheckTime,
            ExcelKey = @ExcelKey,
			Description = @Description,
			RecordStatus = 1,
			ModifiedOn = GETDATE(),
			ModifiedBy = @ModifiedBy,
			ModifiedName = @ModifiedName,
            DifferentTime = @DifferentTime,
            Value = @Value,
            Money = @Money
          WHERE RecordStatus < 99 AND AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName;
                zCommand.Parameters.Add("@ExcelKey", SqlDbType.Int).Value = _ExcelKey;
                if (_EmployeeKey.Length > 0)
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_EmployeeKey);
                }
                else
                {
                    zCommand.Parameters.Add("@EmployeeKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                }
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID;
                if (_BeginTime == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@BeginTime", SqlDbType.DateTime).Value = _BeginTime;
                }

                if (_EndTime == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@EndTime", SqlDbType.DateTime).Value = _EndTime;
                }

                zCommand.Parameters.Add("@OffTime", SqlDbType.Float).Value = _OffTime;

                zCommand.Parameters.Add("@TotalTime", SqlDbType.Float).Value = _TotalTime;


                zCommand.Parameters.Add("@OverTime", SqlDbType.Float).Value = _OverTime;

                if (_CheckTime == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@CheckTime", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@CheckTime", SqlDbType.DateTime).Value = _CheckTime;
                }
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;

                zCommand.Parameters.Add("@DifferentTime", SqlDbType.Float).Value = _DifferentTime;


                zCommand.Parameters.Add("@Value", SqlDbType.Money).Value = _Value;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE dbo.HRM_Employee_Time SET
      [RecordStatus] = 99,
      [ModifiedOn] = GETDATE(),
      [ModifiedBy] = @ModifiedBy,
      [ModifiedName] = @ModifiedName
      WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                if (_CheckTime == DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@CheckTime", SqlDbType.DateTime).Value = DBNull.Value;
                }
                else
                {
                    zCommand.Parameters.Add("@CheckTime", SqlDbType.DateTime).Value = _CheckTime;
                }
                _Message = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
            {
                zResult = Create();
            }
            else
            {
                zResult = Update();
            }

            return zResult;
        }
        #endregion
    }
}
