﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TN.Library.System
{
    public class File_Object : File_Package_Info
    {
        private List<File_Info> _List_File;

        public List<File_Info> List_File
        {
            get
            {
                return _List_File;
            }

            set
            {
                _List_File = value;
            }
        }

        public File_Object() : base()
        {
        }
        
        public File_Object(int FiledPackage):base(FiledPackage)
        {
            DataTable zTable = File_Data.List(FiledPackage.ToString());
            List_File = new List<File_Info>();
            foreach (DataRow nRow in zTable.Rows)
            {
                File_Info zFile = new File_Info(nRow);
                List_File.Add(zFile);
            }
        }

        public void Save_Object()
        {
            base.Save();
            File_Info zFile = new File_Info();
            zFile.ParentKey = base.Key.ToString();
            zFile.ParentTable = "SYS_File_Package";
            zFile.Save();
        }
        public void Delete_Object()
        {
            base.Delete();
            File_Info zFile = new File_Info(base.Key.ToString());
            zFile.Delete();
        }       
    }
}
