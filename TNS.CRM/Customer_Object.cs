﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Library.HumanResource.CRM_Company;
using TN.Library.HumanResource.SYS_Personal;
using TN.Library.Loc;

namespace TN.Library.HumanResource.CRM_Customer
{
    public class Customer_Object : Customer_Info
    {
        public int i = 0;
        private string _ContactKey;
        private string _CompanyKey;
        private string _PersonKey;
        private Company_Info _Company;
        private Personal_Info _Person;
        private Contact_Info _Contact;
        private Address_Info _Address; 
        //private Contact_Personal _Contact_Person;
        //private List<Address_Info> _List_Address;
        //private List<Contact_Personal> _List_Contact;
        //private List<Personal_Info> _List_Person;

        #region -----Properties-----
        public Company_Info Company
        {
            get
            {
                return _Company;
            }

            set
            {
                _Company = value;
            }
        }

        public Personal_Info Person
        {
            get
            {
                return _Person;
            }

            set
            {
                _Person = value;
            }
        }

        public List<Contact_Personal> List_Contact
        {
            get
            {
                return _List_Contact;
            }

            set
            {
                _List_Contact = value;
            }
        }

        public List<Personal_Info> List_Person
        {
            get
            {
                return _List_Person;
            }

            set
            {
                _List_Person = value;
            }
        }

        public List<Address_Info> List_Address
        {
            get
            {
                return _List_Address;
            }

            set
            {
                _List_Address = value;
            }
        }

        public Contact_Info Contact
        {
            get
            {
                return _Contact;
            }

            set
            {
                _Contact = value;
            }
        }

        public Contact_Personal Contact_Person
        {
            get
            {
                return _Contact_Person;
            }

            set
            {
                _Contact_Person = value;
            }
        }

        public Address_Info Address
        {
            get
            {
                return _Address;
            }

            set
            {
                _Address = value;
            }
        }

        public string ContactKey
        {
            get
            {
                return _ContactKey;
            }

            set
            {
                _ContactKey = value;
            }
        }

        public string CompanyKey
        {
            get
            {
                return _CompanyKey;
            }

            set
            {
                _CompanyKey = value;
            }
        }

        public string PersonKey
        {
            get
            {
                return _PersonKey;
            }

            set
            {
                _PersonKey = value;
            }
        }
        #endregion
        public Customer_Object() : base()
        {

        }
        public Customer_Object(string CustomerKey) : base(CustomerKey)
        {
            if (base.Slug == 0)
            {
                Company = new Company_Info();
                Contact = new Contact_Info();
                Person = new Personal_Info();
            }
            if (base.Slug == 1)
            {
                Person = new Personal_Info(CustomerKey, true);
            }
            if (base.Slug == 2)
            {
                Company = new Company_Info(CustomerKey);
            }

        }

        //#region ------Create Customer Company version 1------
        //private void CreateContact()
        //{
        //    int Leng = List_Contact.Count;
        //    for (int i = 0; i < Leng; i++)
        //    {
        //        Contact_Personal zCus_Cont = (Contact_Personal)List_Contact[i];
        //        zCus_Cont.CustomerKey = base.Key;
        //        zCus_Cont.Create();
        //    }
        //}
        //private void CreateAddress()
        //{
        //    int Leng = List_Address.Count;
        //    for (int i = 0; i < Leng; i++)
        //    {
        //        Address_Info zAddress = (Address_Info)List_Address[i];
        //        zAddress.ParentKey = base.Key;
        //        zAddress.ParentTable = "CRM_Customer";
        //        zAddress.Create();
        //    }
        //}
        //private void CreatePerson()
        //{
        //    int Leng = List_Person.Count;
        //    for (int i = 0; i < Leng; i++)
        //    {
        //        Personal_Info zPerson = (Personal_Info)List_Person[i];
        //        zPerson.ParentKey = base.Key;
        //        zPerson.ParentTable = "CRM_Customer";
        //        zPerson.Create();
        //    }
        //}
        //private void CreateCompany()
        //{
        //    _Company.ParentKey = base.Key;
        //    _Company.ParentTable = "CRM_Customer";
        //    _Company.Create();
        //}
        //public void CreateObject()
        //{
        //    base.Create();
        //    CreateCompany();
        //    CreateContact();
        //}
        //#endregion

        #region ------Create Customer Company version 2------
        private void Create_Company()
        {
            _Company.ParentKey = base.Key;
            _Company.ParentTable = "CRM_Customer";
            _Company.Create();
            CompanyKey = _Company.Key;
        }
        public void Create_ContactPerson()
        {
            _Contact_Person = new Contact_Personal();
            _Contact.Create();
        }
        public void Create_Address()
        {
            _Address.ParentTable = "CRM_Company";
            _Address.Create();
        }
        public void Update_Address()
        {
            _Address.Update();
        }
        private void Create_Person()
        {
            _Person = new Personal_Info();
            _Person.ParentKey = _Contact.Key;
            _Person.ParentTable = "CRM_Customer_Contact";
            _Person.Create();
        }
        public void Create_Object()
        {
            base.Create();
            Create_Company();
            if (base.Key.Length > 0 && _Company.Key.Length > 0)
            {
                i = 0;
            }
        }
        private void Update_Company()
        {
            _Company.Update();
        }
        public void Update_Object()
        {
            base.Update();
            Update_Company();
            if (base.Message.Length == 0 && _Company.Message.Length == 0)
            {
                i = 0;
            }
        }
        #endregion

        #region -----Create Customer Person------

        private void Create_Cus_Person()
        {
            _Person.ParentKey = base.Key;
            _Person.ParentTable = "CRM_Customer";
            _Person.Create();
            PersonKey = _Person.Key;
        }
        public void Create_Assress_Person()
        {
            _Address.ParentTable = "CRM_Person";
            _Address.Create();
        }
        public void Create_ObjectPerson()
        {
            base.Create();
            Create_Cus_Person();
        }
        private void UpdatePerson()
        {
            _Person.Update();
        }
        public void UpdateObject()
        {
            base.Update();
            UpdatePerson();
        }
        #endregion
    }
}
