﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_WorkingHistory : Form
    {
        private string _Key = "";

        private int _BrachKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        public Frm_WorkingHistory()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_New.Click += Btn_New_Click;
            btn_Save.Click += Btn_Save_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Copy.Click += Btn_Copy_Click;
            GVData.Click += GVData_Click;

            txt_EmployeeID.Enter += Txt_EmployeeID_Enter;
            txt_EmployeeID.Leave += Txt_EmployeeID_Leave;
            cbo_Branch.Leave += Cbo_Branch_Leave;
            cbo_Department.Leave += Cbo_Department_Leave;

        }



        private void Frm_WorkingHistory_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm(); 
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE RecordStatus <> 99  ORDER BY [Rank]", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Position, "SELECT PositionKey,PositionName FROM SYS_Position WHERE RecordStatus <> 99 ORDER BY [Rank]", "--Chọn--");
            SetDefault();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }
        private void GVData_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count >0 && GVData.Rows[GVData.RowSel][10].ToString() != "")
            {

                _Key = GVData.Rows[GVData.RowSel][10].ToString();
                LoadData();
            }
        }
       
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Lịch sử và luân chuyển công việc.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void DisplayData()
        {
            DataTable _Intable = WorkingHistory_Data.List(txt_Search.Text.Trim());
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);
                }));
            }
        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 11;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Số thẻ";
            GVData.Rows[0][2] = "Họ và tên";
            GVData.Rows[0][3] = "Tên khối";
            GVData.Rows[0][4] = "Tên bộ phận";
            GVData.Rows[0][5] = "Tên nhóm";
            GVData.Rows[0][6] = "Tên chức vụ";
            GVData.Rows[0][7] = "Từ ngày";
            GVData.Rows[0][8] = "Đến ngày";
            GVData.Rows[0][9] = "Ghi chú";

            GVData.Rows[0][10] = ""; // ẩn cột này

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];
                GVData.Rows.Add();
                GVData.Rows[rIndex + 1][0] = (rIndex + 1);
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[8];
                GVData.Rows[rIndex + 1][9] = rData[9];
                GVData.Rows[rIndex + 1][10] = rData[0];
                if (rData[8] != null && rData[8].ToString() != "")
                {
                    GVData.Rows[rIndex + 1].StyleNew.BackColor = Color.LightGray;
                }
            }

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 120;
            GVData.Cols[2].Width = 200;
            GVData.Cols[3].Width = 120;
            GVData.Cols[4].Width = 120;
            GVData.Cols[5].Width = 300;
            GVData.Cols[6].Width = 120;
            GVData.Cols[7].Width = 120;
            GVData.Cols[8].Width = 120;
            GVData.Cols[9].Width = 120;
            GVData.Cols[10].Visible = false;
        }

        #region[cREAETE INFO]

        private void Cbo_Branch_Leave(object sender, EventArgs e)
        {
            if (cbo_Branch.SelectedValue.ToInt() != _BrachKey)
            {
                _BrachKey = cbo_Branch.SelectedValue.ToInt();
                LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey=" + _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
                if (cbo_Team.SelectedValue != null)
                    cbo_Team.SelectedIndex = 0;
            }
        }
        private void Cbo_Department_Leave(object sender, EventArgs e)
        {
            if (cbo_Department.SelectedValue.ToInt() != _DepartmentKey)
            {
                _DepartmentKey = cbo_Department.SelectedValue.ToInt();
                LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey =" + _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
            }
        }
        private void SetDefault()
        {
            _Key = "";
            _BrachKey = 0;
            _DepartmentKey = 0;
            _TeamKey = 0;
            txt_EmployeeID.Text = "";
            txt_EmployeeID.Tag = null;
            txt_EmployeeName.Text = "";
            cbo_Branch.SelectedValue = "0";
            cbo_Department.SelectedValue = "0";
            cbo_Team.SelectedValue = "0";
            cbo_Position.SelectedValue = "0";
            txt_Description.Text = "";
            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = DateTime.MinValue;
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_EmployeeID.Focus();
        }
        private void LoadData()
        {
            WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            _BrachKey = zinfo.BranchKey;
            _DepartmentKey = zinfo.DepartmentKey;
            _TeamKey = zinfo.TeamKey;
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey=" + _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey =" + _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
            cbo_Branch.SelectedValue = zinfo.BranchKey;
            cbo_Department.SelectedValue = zinfo.DepartmentKey;
            cbo_Team.SelectedValue = zinfo.TeamKey;
            cbo_Position.SelectedValue = zinfo.PositionKey;
            txt_Description.Text = zinfo.Description;
            dte_FromDate.Value = zinfo.StartingDate;
            if (zinfo.LeavingDate == DateTime.MinValue)
            {
                dte_ToDate.Value = DateTime.MinValue;
                btn_Copy.Enabled = false;
            }
            else
            {
                dte_ToDate.Value = zinfo.LeavingDate;
                btn_Copy.Enabled = true;

            }
            btn_Del.Enabled = true;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.ModifiedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Tag == null)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã nhân viên!",1);
                return;
            }
            if (cbo_Branch.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chọn khối!",1);
                return;
            }
            if (cbo_Branch.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chọn khối!",1);
                return;
            }
            if (cbo_Department.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chọn bộ phận!",1);
                return;
            }
            if (cbo_Team.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chọn tổ nhóm!",1);
                return;
            }
            if (cbo_Position.SelectedValue.ToInt() == 0)
            {
                Utils.TNMessageBoxOK("Chọn chức vụ!",1);
                return;
            }
            else
            {
                WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
                zinfo.EmployeeKey = txt_EmployeeID.Tag.ToString();
                zinfo.EmployeeID = txt_EmployeeID.Text.Trim().ToUpper();
                zinfo.EmployeeName = txt_EmployeeName.Text.Trim().ToString();
                zinfo.BranchKey = cbo_Branch.SelectedValue.ToInt();
                zinfo.BranchName = cbo_Branch.Text;
                zinfo.DepartmentKey = cbo_Department.SelectedValue.ToInt();
                zinfo.DepartmentName = cbo_Department.Text;
                zinfo.TeamKey = cbo_Team.SelectedValue.ToInt();
                zinfo.TeamName = cbo_Team.Text;
                zinfo.PositionKey = cbo_Position.SelectedValue.ToInt();
                zinfo.PositionName = cbo_Position.Text;
                zinfo.Description = txt_Description.Text;
                DateTime zFromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
                DateTime zToDate = DateTime.MinValue;
                if (dte_ToDate.Value != DateTime.MinValue)
                {
                    zToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
                }
                zinfo.StartingDate = zFromDate;
                zinfo.LeavingDate = zToDate;

                //string[] s = cbo_Salary.Text.Split('-');
                //zinfo.CategoryID = s[0];
                //zinfo.CategoryName = s[1];
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == string.Empty)
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",3);
                    if(dte_ToDate.Value == DateTime.MinValue)
                    {
                        UpdateTeam();
                    }
                    _Key = zinfo.Key;
                    LoadData();
                    DisplayData();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message,4);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == "")
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?. Nếu xóa thông tin này vui lòng trở về cài đặt nhân sự lại thông tin nhân sự này!",2) == "Y")
                {
                    WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !",3);
                        SetDefault();
                        DisplayData();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message,4);
                    }

                }
            }

        }
        private void Btn_Copy_Click(object sender, EventArgs e)
        {
            WorkingHistory_Detail zinfo = new WorkingHistory_Detail(_Key);
            txt_EmployeeID.Tag = zinfo.EmployeeKey;
            txt_EmployeeID.Text = zinfo.EmployeeID;
            txt_EmployeeName.Text = zinfo.EmployeeName;
            _BrachKey = zinfo.BranchKey;
            _DepartmentKey = zinfo.DepartmentKey;
            _TeamKey = zinfo.TeamKey;
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey=" + _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey =" + _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
            cbo_Branch.SelectedValue = zinfo.BranchKey;
            cbo_Department.SelectedValue = zinfo.DepartmentKey;
            cbo_Team.SelectedValue = zinfo.TeamKey;
            cbo_Position.SelectedValue = zinfo.PositionKey;
            txt_Description.Text = zinfo.Description;
            DateTime zFromDate = new DateTime(zinfo.LeavingDate.Year, zinfo.LeavingDate.Month, zinfo.LeavingDate.Day, 0, 0, 0);
            dte_FromDate.Value = zFromDate.AddDays(1);
            dte_ToDate.Value = DateTime.MinValue;
            btn_Copy.Enabled = false;
            btn_Del.Enabled = false;
            _Key = "";

            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            Utils.TNMessageBoxOK("Sao chép thành công!Vui lòng chỉnh sửa thông tin.",1);
        }
        private void UpdateTeam()
        {
            Team_Info zTeam;
            Employee_Info zEmployee = new Employee_Info(txt_EmployeeID.Tag.ToString());
            zTeam = new Team_Info(cbo_Team.SelectedValue.ToInt());
            zEmployee.TeamKey = zTeam.Key;
            zEmployee.DepartmentKey = zTeam.DepartmentKey;
            zEmployee.BranchKey = zTeam.BranchKey;
            zEmployee.PositionKey = cbo_Position.SelectedValue.ToInt();
            zEmployee.Save();
        }


        private void Txt_EmployeeID_Leave(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim().Length < 1)
            {
                txt_EmployeeID.Text = "Nhập mã thẻ";
                txt_EmployeeID.ForeColor = Color.White;
            }
            else
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployee(txt_EmployeeID.Text.Trim());
                if (zinfo.Key == "")
                {
                    txt_EmployeeID.Text = "Nhập mã thẻ";
                    txt_EmployeeID.ForeColor = Color.White;
                    txt_EmployeeID.Tag = null;
                }
                else
                {
                    _BrachKey = zinfo.BranchKey;
                    _DepartmentKey = zinfo.DepartmentKey;
                    _TeamKey = zinfo.TeamKey;
                    LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus <> 99 AND BranchKey=" + _BrachKey + "  ORDER BY [Rank]", "--Chọn--");
                    LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE RecordStatus <> 99 AND DepartmentKey =" + _DepartmentKey + "  ORDER BY [Rank]", "--Chọn--");
                    txt_EmployeeID.Text = zinfo.EmployeeID;
                    txt_EmployeeName.Text = zinfo.FullName;
                    txt_EmployeeID.Tag = zinfo.Key;
                    cbo_Branch.SelectedValue = zinfo.BranchKey;
                    cbo_Department.SelectedValue = zinfo.DepartmentKey;
                    cbo_Team.SelectedValue = zinfo.TeamKey;
                    cbo_Position.SelectedValue = zinfo.PositionKey;
                    txt_EmployeeID.BackColor = Color.Black;
                }
            }
        }
        private void Txt_EmployeeID_Enter(object sender, EventArgs e)
        {
            if (txt_EmployeeID.Text.Trim() == "Nhập mã thẻ")
            {
                txt_EmployeeID.Text = "";
                txt_EmployeeID.ForeColor = Color.Brown;
            }
        }

        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;

                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
