﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class FeeLeaderMonth_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *,  AS [Key] FROM HRM_FeeLeaderMonth WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static string LayMaID_HoTroToTruong_ToPho()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "select  [dbo].HRM_LayMaID_HoTroToTruong_ToPho()";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                // Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static DataTable List(DateTime DateWrite, int TeamKey,string CategoryID)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.*,TeamID FROM HRM_FeeLeaderMonth A
            LEFT JOIN [dbo].[SYS_Team] B ON A.TeamKey =B.TeamKey
            WHERE A.RecordStatus != 99";
            if (TeamKey > 0)
            {
                zSQL += " AND A.TeamKey =@TeamKey  ";
            }
            if(CategoryID.Trim()!="0")
            {
                zSQL += " AND A.CategoryID =@CategoryID  ";
            }
            zSQL += " AND A.DateWrite BETWEEN @FromDate AND @ToDate ORDER BY LEN(A.EmployeeID) ,A.EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@CategoryID", SqlDbType.NVarChar).Value = CategoryID;
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListV2(DateTime DateWrite ,int BranchKey, int DepartmentKey, int TeamKey, int CategoryKey, string Search)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zFilter = "";
            if (TeamKey > 0)
                zFilter += " AND [dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@TeamKey ";
            if (DepartmentKey > 0)
                zFilter += " AND [dbo].[Fn_DepartmentKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) =@DepartmentKey ";
            if (BranchKey > 0)
            {
                zFilter += " AND [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) = @BranchKey";
            }
            if (Search.Trim().Length > 0)
            {
                zFilter += " AND ( EmployeeID LIKE @Search OR EmployeeName LIKE @Search )";
            }
            if (CategoryKey > 0)
            {
                zFilter += " AND CategoryKey =@CategoryKey ";
            }
            string zSQL = @";WITH #TEMP AS (
SELECT *,
[dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate) AS TeamKeys,
[dbo].[Fn_GetTeamID]([dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)) AS TeamID, [dbo].[Fn_GetTeamName]([dbo].[Fn_TeamKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate))AS TeamName,
[dbo].[Fn_GetBranchKey_FromEmployeeKey](EmployeeKey) AS BranchKey 
FROM [dbo].[HRM_FeeLeaderMonth] WHERE RecordStatus <> 99  
AND DateWrite BETWEEN @FromDate AND @ToDate @Parameter
)

SELECT A.AutoKey,A.DateWrite,A.EmployeeName,A.EmployeeID,A.TeamKeys AS TeamKey, A.TeamID,A.TeamName,A.CategoryID,A.CategoryName,A.Value,A.Ext,A.Description FROM #TEMP A
LEFT JOIN[dbo].[SYS_Team] B ON B.TeamKey=A.TeamKeys
LEFT JOIN[dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
LEFT JOIN[dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
ORDER BY D.RANK, C.RANK, B.RANK, LEN(EmployeeID), EmployeeID,FromDate DESC";

            zSQL = zSQL.Replace("@Parameter",zFilter);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = CategoryKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search.Trim() + "%";
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
