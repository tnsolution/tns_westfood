﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.LOG
{
    public class Record_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ParentKey = "";
        private string _ParentTable = "";
        private string _Description = "";
        private string _ActionName = "";
        private string _ActionValue = "";
        private int _ActionType = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string ParentTable
        {
            get { return _ParentTable; }
            set { _ParentTable = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public string ActionName
        {
            get { return _ActionName; }
            set { _ActionName = value; }
        }
        public string ActionValue
        {
            get { return _ActionValue; }
            set { _ActionValue = value; }
        }
        public int ActionType
        {
            get { return _ActionType; }
            set { _ActionType = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Record_Info()
        {
        }
        public Record_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM LOG_Record WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _ParentKey = zReader["ParentKey"].ToString().Trim();
                    _ParentTable = zReader["ParentTable"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    _ActionName = zReader["ActionName"].ToString().Trim();
                    _ActionValue = zReader["ActionValue"].ToString().Trim();
                    if (zReader["ActionType"] != DBNull.Value)
                        _ActionType = int.Parse(zReader["ActionType"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO LOG_Record ("
        + " ParentKey ,ParentTable ,Description ,ActionName ,ActionValue ,ActionType ,CreatedBy ,CreatedName ,CreatedOn ) "
         + " VALUES ( "
         + "@ParentKey ,@ParentTable ,@Description ,@ActionName ,@ActionValue ,@ActionType ,@CreatedBy ,@CreatedName ,GETDATE()) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = _ActionName.Trim();
                zCommand.Parameters.Add("@ActionValue", SqlDbType.NVarChar).Value = _ActionValue.Trim();
                zCommand.Parameters.Add("@ActionType", SqlDbType.Int).Value = _ActionType;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE LOG_Record SET "
                        + " ParentKey = @ParentKey,"
                        + " ParentTable = @ParentTable,"
                        + " Description = @Description,"
                        + " ActionName = @ActionName,"
                        + " ActionValue = @ActionValue,"
                        + " ActionType = @ActionType"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey.Trim();
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ActionName", SqlDbType.NVarChar).Value = _ActionName.Trim();
                zCommand.Parameters.Add("@ActionValue", SqlDbType.NVarChar).Value = _ActionValue.Trim();
                zCommand.Parameters.Add("@ActionType", SqlDbType.Int).Value = _ActionType;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOG_Record SET [RecordStatus] = 99  WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }

       
        #endregion
    }
}
