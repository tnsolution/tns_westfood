﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Setup_Unit_Convert : Form
    {
        #region[Private]
        private int _Key = 0;
        #endregion
        public Frm_Setup_Unit_Convert()
        {
            InitializeComponent();

            LoadDataToToolbox.KryptonComboBox(cbo_Unit1, "SELECT UnitKey, UnitName FROM IVT_Product_Unit WHERE RecordStatus < 99 ORDER BY Rank ASC", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Unit2, "SELECT UnitKey, UnitName FROM IVT_Product_Unit WHERE RecordStatus < 99 ORDER BY Rank ASC", "--Chọn--");

            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;

            txt_ProductID.Leave += Txt_ProductID_Leave;
            txt_Quantity2.Leave += Txt_Quantity2_Leave;
        }

      

        private void Txt_ProductID_Leave(object sender, EventArgs e)
        {
            Product_Info zProduct = new Product_Info();
            zProduct.Get_Product_Info_ID(txt_ProductID.Text.Trim());
            if(zProduct.ProductKey!="")
            {
                txt_ProductID.Tag = zProduct.ProductKey;
                txt_ProductName.Text = zProduct.ProductName;
                txt_Quantity1.Text = "1";
                cbo_Unit1.SelectedValue = zProduct.BasicUnit;
                
            }
            else
            {
                txt_ProductID.Tag = null;
                txt_ProductName.Text = "";
                txt_Quantity1.Text = "0";
                cbo_Unit1.SelectedValue = 0;
            }
        }

        private void Frm_Convert_Unit_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            SetDefault();
        }
        private void Txt_Quantity2_Leave(object sender, EventArgs e)
        {
            double zAmount = 0;
            if (double.TryParse(txt_Quantity2.Text.Trim(), out zAmount))
            {
                txt_Quantity2.Text = zAmount.Toe1String();
                txt_Quantity2.Focus();
                txt_Quantity2.SelectionStart = txt_Quantity2.Text.Length - 2;
            }
            else
            {
                MessageBox.Show("Lỗi!.Định dạng số.");
            }
        }
        #region[Progess]

        private void LoadData()
        {

            Convert_Unit_Info zinfo = new Convert_Unit_Info(_Key);
            txt_ProductID.Tag = zinfo.ProductKey;
            txt_ProductID.Text = zinfo.ProductID;
            txt_ProductName.Text = zinfo.ProductName;
            cbo_Unit1.SelectedValue = zinfo.Unit1;
            cbo_Unit2.SelectedValue = zinfo.Unit2;
            txt_Quantity1.Text = zinfo.Quantity1.ToString();
            txt_Quantity2.Text = zinfo.Quantity2.ToString();
            txt_Description.Text = zinfo.Description;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_ProductID.Tag = null;
            txt_ProductID.Text = "";
            txt_ProductName.Text = "";
            cbo_Unit1.SelectedIndex = 0;
            cbo_Unit2.SelectedIndex = 1;
            txt_Quantity1.Text = "0";
            txt_Quantity2.Text = "0";
            txt_Description.Text = "";
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            cbo_Unit1.Focus();
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if(txt_ProductID.Tag==null || txt_ProductID.Text.Trim()=="")
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng sản phẩm!",1);
                return;
            }
            float zQuantity1;
            if (!float.TryParse(txt_Quantity1.Text, out zQuantity1))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!",1);
                return;
            }
            float zQuantity2;
            if (!float.TryParse(txt_Quantity2.Text, out zQuantity2))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!",1);
                return;
            }
            else
            {
                Convert_Unit_Info zinfo = new Convert_Unit_Info(_Key);
                zinfo.ProductKey = txt_ProductID.Tag.ToString();
                zinfo.ProductID = txt_ProductID.Text.ToString().Trim();
                zinfo.ProductName = txt_ProductName.Text.ToString();
                zinfo.Unit1 = int.Parse(cbo_Unit1.SelectedValue.ToString());
                zinfo.Unit2 = int.Parse(cbo_Unit2.SelectedValue.ToString());
                zinfo.Quantity1 = float.Parse(txt_Quantity1.Text.Trim());
                zinfo.Quantity2 = float.Parse(txt_Quantity2.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message == string.Empty)
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",3);
                    DisplayData();
                    SetDefault();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message,4);
                }
            }


        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.", 2) == "Y")
                {
                    Convert_Unit_Info zinfo = new Convert_Unit_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công!",3);
                        DisplayData();
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message,4);
                    }

                }
            }

        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DisplayData();
        }
        #endregion
        #region[ListView]
        private void DisplayData()
        {

            DataTable ztb = Convert_Unit_Data.ListConvert();
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][5].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _Key = 0;
                    LoadData();
                }
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách chuyển đổi đơn vị.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 6;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã sản phẩm";
            GVData.Rows[0][2] = "Tên sản phẩm";
            GVData.Rows[0][3] = "Quy đổi";
            GVData.Rows[0][4] = "Ghi chú";
            GVData.Rows[0][5] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];

                string zQuyDoi = rData[3].Toe2String().Trim() + " " +
                rData[4].ToString().Trim() + " = " +
                rData[5].Toe2String().Trim() + " " +
                rData[6].ToString().Trim();
                GVData.Rows[rIndex + 1][3] = zQuyDoi;

                GVData.Rows[rIndex + 1][4] = rData[7];
                GVData.Rows[rIndex + 1][5] = rData[0];

            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 100;
            GVData.Cols[2].Width = 250;
            GVData.Cols[3].Width = 250;
            GVData.Cols[4].Width = 50;
            GVData.Cols[5].Visible = false;

            GVData.Cols[3].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion
      

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
