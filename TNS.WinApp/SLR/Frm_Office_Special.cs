﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.SLR;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Office_Special : Form
    {
        private DataTable _ReportCode;
        private DataTable _TableEmployee;
        private int _TotalEmployee = 0;
        private int _IndexEmployee = 0;
        int _Key = 0;
        string _ID = "";
        string _Name = "";
        double _Amount = 0;
        private DataTable _KetThucThuViec;
        private DataTable _TableTangLuong;

        public Frm_Office_Special()
        {
            InitializeComponent();
            //btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            btn_Run.Click += Btn_Run_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += Btn_Export_Click;
            dteDate.Value = SessionUser.Date_Work;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

        }

       

        private void Frm_Office_Special_Load(object sender, EventArgs e)
        {
            _ReportCode = Office_Special_Data.ListCodeReportOffice();
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Btn_Run_Click(object sender, EventArgs e)
        {
            LoadDanhSachKetThucThuViec();
            LoadDanhSachTangLuong();
            int zLoaiTinhLuong = 0; //0:bình thường,1:thử việc,2 tăng lương
           
            double zSoNgayCongMucCu = 0;// số ngày công chưa thay đổi
            int zCoThuViec = 0; //cờ thử việc
            int zCoTangLuong = 0;

           

            #region[Xét trường hợp tăng lương]
            int zCoMLC = 0;
            int zCoLHQ = 0;
            int zCoHTX = 0;
            float zTienMCL_Cu = 0;
            float zTienLHQ_Cu = 0;
            float zTienHTX_Cu = 0;

            DateTime zNgayKT_MLC = DateTime.MinValue;
            DateTime zNgayKT_LHQ = DateTime.MinValue;
            DateTime zNgayKT_HTX = DateTime.MinValue;
            DateTime zNgayKT = DateTime.MinValue;
            if (_TableTangLuong.Rows.Count > 0)
            {
                if (_TableTangLuong.Select("EmployeeKey='" + _WorkerInfo.EmployeeKey + "' AND MaDanhMuc='MLC'").Length > 0)
                {
                    zCoMLC = 1;

                    DataRow[] Array = _TableTangLuong.Select("EmployeeKey='" + _WorkerInfo.EmployeeKey + "' AND MaDanhMuc='MLC'");
                    DataTable ztam = Array.CopyToDataTable();

                    zNgayKT_MLC = DateTime.Parse(ztam.Rows[0]["HetHan"].ToString());
                    zTienMCL_Cu = float.Parse(ztam.Rows[0]["SoTien"].ToString());

                    zNgayKT = zNgayKT_MLC;
                    zSoNgayCongMucCu = ReportOffice_Data.SoNgayCongMucCu(_WorkerInfo.EmployeeKey, zNgayKT_MLC);

                }
                if (_TableTangLuong.Select("EmployeeKey='" + _WorkerInfo.EmployeeKey + "' AND MaDanhMuc='LHQ'").Length > 0)
                {
                    zCoLHQ = 1;

                    DataRow[] Array = _TableTangLuong.Select("EmployeeKey='" + _WorkerInfo.EmployeeKey + "' AND MaDanhMuc='LHQ'");
                    DataTable ztam = Array.CopyToDataTable();

                    zNgayKT_LHQ = DateTime.Parse(ztam.Rows[0]["HetHan"].ToString());
                    zTienLHQ_Cu = float.Parse(ztam.Rows[0]["SoTien"].ToString());

                    zNgayKT = zNgayKT_LHQ;
                }
                if (_TableTangLuong.Select("EmployeeKey='" + _WorkerInfo.EmployeeKey + "' AND MaDanhMuc='HTX'").Length > 0)
                {
                    zCoLHQ = 1;

                    DataRow[] Array = _TableTangLuong.Select("EmployeeKey='" + _WorkerInfo.EmployeeKey + "' AND MaDanhMuc='HTX'");
                    DataTable ztam = Array.CopyToDataTable();

                    zNgayKT_HTX = DateTime.Parse(ztam.Rows[0]["HetHan"].ToString());
                    zTienHTX_Cu = float.Parse(ztam.Rows[0]["SoTien"].ToString());

                    zNgayKT = zNgayKT_HTX;
                    zSoNgayCongMucCu = ReportOffice_Data.SoNgayCongMucCu(_WorkerInfo.EmployeeKey, zNgayKT_HTX);
                }
                int zTotal = 0;
                zTotal = zCoMLC + zCoLHQ + zCoLHQ;
                if (zTotal == 3)//Tăng 3 mã lương
                {
                    if (zNgayKT_MLC.Day == zNgayKT_LHQ.Day && zNgayKT_LHQ.Day == zNgayKT_HTX.Day)
                    {
                        zCoTangLuong = 1; // nếu  tăng 3 mã lương cùng 1 ngày thì tính.

                    }
                    else
                    {
                        //Trường hợp này chưa tính
                    }
                }
                if (zTotal == 2)//Tăng 2 mã lương
                {
                    if (zCoMLC == 1 && zCoLHQ == 1 && zCoHTX == 0)
                    {
                        if (zNgayKT_MLC.Day == zNgayKT_LHQ.Day)
                        {
                            zCoTangLuong = 1; // nếu  tăng 2 mã lương cùng ngày thì tính

                        }
                        else
                        {
                            //Trường hợp này chưa tính
                        }
                    }
                    if (zCoMLC == 1 && zCoLHQ == 0 && zCoHTX == 1)
                    {
                        if (zNgayKT_MLC.Day == zNgayKT_HTX.Day)
                        {
                            zCoTangLuong = 1; // nếu  tăng 2 mã lương cùng ngày thì tính

                        }
                        else
                        {
                            //Trường hợp này chưa tính
                        }
                    }
                    if (zCoMLC == 0 && zCoLHQ == 1 && zCoHTX == 1)
                    {
                        if (zNgayKT_LHQ.Day == zNgayKT_HTX.Day)
                        {
                            zCoTangLuong = 1; // nếu  tăng 2 mã lương cùng ngày thì tính

                        }
                        else
                        {
                            //Trường hợp này chưa tính
                        }
                    }
                }
                if (zTotal == 1)
                {
                    zCoTangLuong = 1;
                }
            }
            #endregion

            //Nếu kết thúc thử việc và có tăng lương --Tình huống  này lưu ý
            if (zCoThuViec == 1 && zCoTangLuong == 1)
            {
                zLoaiTinhLuong = 1; //Fix thử việc
            }
            else if (zCoThuViec == 0 && zCoTangLuong == 1)
            {
                zLoaiTinhLuong = 2; //tăng lương
            }
            else if (zCoThuViec == 1 && zCoTangLuong == 1)
            {
                zLoaiTinhLuong = 2; //tăng lương
            }
            else
            {
                zLoaiTinhLuong = 0; // bình thường
            }

        }

        void LoadDanhSachKetThucThuViec()
        {
            _KetThucThuViec = Office_Special_Data.DanhSachKetThucThuViec(dteDate.Value);
            if (_KetThucThuViec.Rows.Count > 0)
            {
                foreach (DataRow r in _KetThucThuViec.Rows)
                {
                    string zEmployeeKey = r["EmployeeKey"].ToString();
                    string zEmployeeID = r["EmployeeID"].ToString();
                    string zEmployeeName = r["EmployeeName"].ToString();
                    DateTime zNgayHetThuViec = DateTime.Parse(r["HetThuViec"].ToString());

                    for (int i = 0; i < _ReportCode.Rows.Count; i++)
                    {
                        _Key = _ReportCode.Rows[i]["AutoKey"].ToInt();
                        _ID = _ReportCode.Rows[i]["ID"].ToString().Trim();
                        _Name = _ReportCode.Rows[i]["Name"].ToString().Trim();
                        switch (_ID)
                        {
                            case "MLTT"://Mưc lương tối thiểu (Chính)
                                _Amount = 0;
                                break;
                            case "HQCV": //Mức lương theo hiệu quả công việc
                                _Amount = ReportOffice_Data.HQCV(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "HTTX":  //Hỗ trợ tiền xăng
                                _Amount = ReportOffice_Data.HTTX(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            //Lưu ý: Phải tính hỗ trợ tiền xăng trước khi tính lương thỏa thuận
                            case "TLTT": //Mức lương thỏa thuận
                                _Amount = ReportOffice_Data.TLTT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "TLHT": //Tỉ lệ hoàn thành công việc
                                _Amount = ReportOffice_Data.TLHT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "CVTT":  //HỖ TRỢ THEO KẾT QUẢ HOÀN THÀNH CÔNG VIỆC THỰC TẾ
                                _Amount = ReportOffice_Data.CVTT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "LCHQ": //Tổng lương chính và hỗ trợ theo hiệu quả công việc
                                _Amount = ReportOffice_Data.LCHQ(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "NCQD": //ngày công tính lương hay ngày công quy định
                                _Amount = ReportOffice_Data.NCQD(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "NCTT": //ngày công thực tế -- thay đổi kết thúc thử việc
                                _Amount = ReportOffice_Data.NCTT_ThuViec(_WorkerInfo.EmployeeKey, dte_FromDate.Value, zSoNgayCongMucCu);
                                break;
                            case "NKHL": //ngày nghỉ không hưởng lương
                                _Amount = ReportOffice_Data.NKHL(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "TKHL": //số tiền nghỉ không hưởng lương
                                _Amount = ReportOffice_Data.TKHL(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "TLNC": //Tổng lương theo ngày công
                                _Amount = ReportOffice_Data.TLNC(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "NCPN": //Số ngày công lễ phép năm
                                _Amount = ReportOffice_Data.NCPN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "HTPN": //Hỗ trợ phép,lễ, phép năm
                                _Amount = ReportOffice_Data.HTPN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "NCCV": //Ngày công công việc khác -- thử việc
                                _Amount = ReportOffice_Data.NCCV_ThuViec(_WorkerInfo.EmployeeKey, dte_FromDate.Value, zSoNgayCongMucCu);
                                break;
                            case "KCVK": //Tiền  khoán công việc khác-- tăng lương
                                _Amount = ReportOffice_Data.KCVK_TangLuong(_WorkerInfo.EmployeeKey, dte_FromDate.Value, zNgayKT);
                                break;
                            case "TKXH": // Số tiền khoán xuất hàng
                                _Amount = ReportOffice_Data.TKXH(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "TCSX": // Tổng cộng tiền
                                _Amount = ReportOffice_Data.TCSX(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "SLCN": // Só lượng con nhỏ
                                _Amount = ReportOffice_Data.SLCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "STCN": // Só tiền con nhỏ
                                _Amount = ReportOffice_Data.STCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "SLCC": // Số phần cơm loại C
                                _Amount = ReportOffice_Data.SLCC(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "STCC": // Số Tiền cơm loại C
                                _Amount = ReportOffice_Data.STCC(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "SLCU": // Số phần cơm loại U
                                _Amount = ReportOffice_Data.SLCU(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "STCU": // Số Tiền cơm loại U
                                _Amount = ReportOffice_Data.STCU(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "BHXH": // Số tiền đóng bảo hiểm xã hội
                                _Amount = ReportOffice_Data.BHXH(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "STCD": // Số tiền đóng công đoàn
                                _Amount = ReportOffice_Data.STCD(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "HUUL": // Số tiền hoàn ứng lương
                                _Amount = ReportOffice_Data.HUUL(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "HUXH": // Số tiền hoàn ứng xuất hàng
                                _Amount = ReportOffice_Data.HUXH(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "CKTK": // Các khoản trừ khác
                                _Amount = ReportOffice_Data.CKTK(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "TNCT": //Thu nhập chịu thuế
                                _Amount = ReportOffice_Data.TNCT(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "TNCN": //Thuế thu nhập cá nhân
                                _Amount = ReportOffice_Data.TNCN(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "TTLV": // Số tiền tấm lòng vàng
                                _Amount = ReportOffice_Data.TTLV(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            case "STTL": // Số tiền thực lãnh
                                _Amount = ReportOffice_Data.STTL(_WorkerInfo.EmployeeKey, dte_FromDate.Value);
                                break;
                            default:
                                _Amount = 0;
                                break;
                        }

                        Insert();
                    }
                }
            }
        }
        void LoadDanhSachTangLuong()
        {
            _TableTangLuong = Office_Special_Data.DanhSachTangLuong(dteDate.Value);
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Bảng số liệu những trường hợp đặt biệt" + dteDate.Value.ToString("MM-yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
