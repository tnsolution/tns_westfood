﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.Misc;
using ReportAll = TNS.CORE.Report;

namespace TNS.WinApp
{
    public partial class Frm_BaoCao_Ngay : Form
    {
        int _STT = 1;
        int _TeamKey = 0;
        DataTable _Table;

        public Frm_BaoCao_Ngay()
        {
            InitializeComponent();

            string zSQL = @"
SELECT TeamKey, TeamID +'-'+ TeamName AS TeamName  
FROM SYS_Team 
WHERE RecordStatus <> 99 
AND BranchKey = 4 
AND  DepartmentKey != 26
ORDER BY Rank";

            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "--Tất cả--");
            btn_Search.Click += Btn_Search_Click;
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            try
            {
                _STT = 1;
                _TeamKey = cboTeam.SelectedValue.ToInt();
                _Table = new DataTable();

                if (rdoLuongKhoan.Checked)
                {
                    using (Frm_Loading frm = new Frm_Loading(LuongKhoan))
                    {
                        frm.ShowDialog(this);
                    }
                }
                if (rdoTongGio.Checked)
                {
                    using (Frm_Loading frm = new Frm_Loading(TongGio))
                    {
                        frm.ShowDialog(this);
                    }
                }
                if (rdoGiodu.Checked)
                {
                    using (Frm_Loading frm = new Frm_Loading(GioDu))
                    {
                        frm.ShowDialog(this);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }

        #region LƯƠNG KHOÁN
        void LuongKhoan()
        {
            DateTime FromDate = dte_FromDate.Value;
            DateTime ToDate = dte_ToDate.Value;
            _Table = ReportAll.BaoCaoLuongKhoan(FromDate, ToDate, _TeamKey);
            if (_Table.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    GV_LuongKhoan(_Table);

                    Row zGvRow;

                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    string MAPHONGBAN = _Table.Rows[0]["MAPHONGBAN"].ToString();
                    string TENPHONGBAN = _Table.Rows[0]["TENPHONGBAN"].ToString();
                    DataRow[] Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow_LuongKhoan(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, 0);
                    }
                    for (int i = 0; i < _Table.Rows.Count; i++)
                    {
                        DataRow r = _Table.Rows[i];
                        if (MAPHONGBAN != r["MAPHONGBAN"].ToString())
                        {
                            #region [GROUP]
                            MAPHONGBAN = r["MAPHONGBAN"].ToString();
                            TENPHONGBAN = r["TENPHONGBAN"].ToString();
                            Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_LuongKhoan(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_LuongKhoan(zGvRow, r, i + NoGroup - 1);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_LuongKhoan(zGvRow, _Table, RowTam + 1);
                }));
            }
        }
        void GV_LuongKhoan(DataTable Table)
        {
            int ToTalCol = Table.Columns.Count;
            GVData.Cols.Add(ToTalCol + 1);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                GVData.Rows[0][nCol] = "Ngày " + Table.Columns[i].ColumnName;
                nCol++;
            }
            GVData.Rows[0][ToTalCol] = "TỔNG CỘNG";

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Cols[0].Width = 40;
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].Width = 200;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].Width = 120;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 3; i <= ToTalCol; i++)
            {

                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            GVData.Cols[ToTalCol].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void HeaderRow_LuongKhoan(Row RowView, DataTable Table, string MaPhongBan, string TenPhongBan, int No)
        {
            RowView[0] = "";
            RowView[1] = TenPhongBan;
            RowView[2] = "Số công nhân " + Table.Select("MAPHONGBAN='" + MaPhongBan + "'").Length;

            int nCol = 3;
            float zTotal = 0;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "MAPHONGBAN='" + MaPhongBan + "'").Ton0String();
                zTotal += RowView[nCol].ToFloat();
                nCol++;
            }
            RowView[nCol] = zTotal.Ton0String();
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void DetailRow_LuongKhoan(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[2] = rDetail["MANHANVIEN"].ToString().Trim();
            RowView[1] = rDetail["HOTEN"].ToString().Trim();
            double zTotalRow = 0;
            int nCol = 3;
            for (int i = 4; i < rDetail.ItemArray.Length; i++)
            {
                RowView[nCol] = rDetail[i].Ton0String();
                zTotalRow += rDetail[i].ToDouble();
                nCol++;
            }
            RowView[nCol] = zTotalRow.Ton0String();
        }
        private void TotalRow_LuongKhoan(Row RowView, DataTable Table, int No)
        {

            RowView[0] = "";
            RowView[1] = "Tổng ";
            RowView[2] = "Số công nhân " + Table.Rows.Count;

            float zTotal = 0;
            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "").Ton0String();
                zTotal += RowView[nCol].ToFloat();
                nCol++;
            }
            RowView[nCol] = zTotal.Ton0String();
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        #endregion

        #region TONGGIO
        void TongGio()
        {
            string FromDate = dte_FromDate.Value.ToString("yyyy-MM-dd 00:00:00");
            string ToDate = dte_ToDate.Value.ToString("yyyy-MM-dd 23:59:59");
            _Table = ReportAll.BaoCaoThoiGian(_FromDate, _ToDate, _TeamKey);
            if (_Table.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    GV_TongGio(_Table);

                    Row zGvRow;

                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    string MAPHONGBAN = _Table.Rows[0]["MAPHONGBAN"].ToString();
                    string TENPHONGBAN = _Table.Rows[0]["TENPHONGBAN"].ToString();
                    DataRow[] Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow_TongGio(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, 0);
                    }
                    for (int i = 0; i < _Table.Rows.Count; i++)
                    {
                        DataRow r = _Table.Rows[i];
                        if (MAPHONGBAN != r["MAPHONGBAN"].ToString())
                        {
                            #region [GROUP]
                            MAPHONGBAN = r["MAPHONGBAN"].ToString();
                            TENPHONGBAN = r["TENPHONGBAN"].ToString();
                            Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_TongGio(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_TongGio(zGvRow, r, i + NoGroup - 1);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_TongGio(zGvRow, _Table, RowTam + 1);
                }));
            }
        }
        void GV_TongGio(DataTable Table)
        {
            int ToTalCol = Table.Columns.Count;
            GVData.Cols.Add(ToTalCol + 1);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                GVData.Rows[0][nCol] = "Ngày " + Table.Columns[i].ColumnName;
                nCol++;
            }
            GVData.Rows[0][ToTalCol] = "TỔNG CỘNG";

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Cols[0].Width = 40;
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].Width = 200;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].Width = 120;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 3; i <= ToTalCol; i++)
            {

                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            GVData.Cols[ToTalCol].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void HeaderRow_TongGio(Row RowView, DataTable Table, string MaPhongBan, string TenPhongBan, int No)
        {
            RowView[0] = "";
            RowView[1] = TenPhongBan;
            RowView[2] = "Số công nhân " + Table.Select("MAPHONGBAN='" + MaPhongBan + "'").Length;

            int nCol = 3;

            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double TongSoPhut = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        TongSoPhut += phut + gio * 60;
                    }
                }

                RowView[nCol] = SoGio(TongSoPhut);
                nCol++;
            }
            RowView[nCol] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void DetailRow_TongGio(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[2] = rDetail["MANHANVIEN"].ToString().Trim();
            RowView[1] = rDetail["HOTEN"].ToString().Trim();
            double tongthuong = 0;
            int nCol = 3;
            for (int i = 4; i < rDetail.ItemArray.Length; i++)
            {
                if (rDetail[i].ToString().Contains(":"))
                {
                    string[] temp = rDetail[i].ToString().Split(':');
                    int gio = temp[0].ToInt();
                    int phut = temp[1].ToInt();
                    if (gio > 0)
                    {
                        RowView[nCol] = gio + ":" + phut;
                        tongthuong += phut + gio * 60;
                    }
                }
                nCol++;
            }
            RowView[nCol] = SoGio(tongthuong);
        }
        private void TotalRow_TongGio(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "Tổng ";
            RowView[2] = "Số công nhân " + Table.Rows.Count;

            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double TongSoPhut = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        if (gio > 0)
                        {
                            TongSoPhut += phut + gio * 60;
                        }
                    }
                }

                RowView[nCol] = SoGio(TongSoPhut);
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        
        #endregion

        #region GIODU
        void GioDu()
        {
            string FromDate = dte_FromDate.Value.ToString("yyyy-MM-dd 00:00:00");
            string ToDate = dte_ToDate.Value.ToString("yyyy-MM-dd 23:59:59");
            _Table = ReportAll.BaoCaoThoiGian(FromDate, ToDate, _TeamKey);
            if (_Table.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    GV_GioDu(_Table);

                    Row zGvRow;

                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    string MAPHONGBAN = _Table.Rows[0]["MAPHONGBAN"].ToString();
                    string TENPHONGBAN = _Table.Rows[0]["TENPHONGBAN"].ToString();
                    DataRow[] Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow_GioDu(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, 0);
                    }
                    for (int i = 0; i < _Table.Rows.Count; i++)
                    {
                        DataRow r = _Table.Rows[i];
                        if (MAPHONGBAN != r["MAPHONGBAN"].ToString())
                        {
                            #region [GROUP]
                            MAPHONGBAN = r["MAPHONGBAN"].ToString();
                            TENPHONGBAN = r["TENPHONGBAN"].ToString();
                            Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_GioDu(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_GioDu(zGvRow, r, i + NoGroup - 1);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_GioDu(zGvRow, _Table, RowTam + 1);
                }));
            }
        }
        void GV_GioDu(DataTable Table)
        {
            int ToTalCol = Table.Columns.Count;
            GVData.Cols.Add(ToTalCol + 2);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                GVData.Rows[0][nCol] = "Ngày " + Table.Columns[i].ColumnName;
                nCol++;
            }
            GVData.Rows[0][GVData.Cols.Count - 3] = "TỔNG CỘNG NGÀY THƯỜNG";
            GVData.Rows[0][GVData.Cols.Count - 2] = "TỔNG CỘNG NGÀY CN";
            GVData.Rows[0][GVData.Cols.Count - 1] = "TỔNG CỘNG";

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Cols[0].Width = 40;
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].Width = 200;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].Width = 120;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 3; i <= ToTalCol; i++)
            {

                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            GVData.Cols[GVData.Cols.Count - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[GVData.Cols.Count - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[GVData.Cols.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void HeaderRow_GioDu(Row RowView, DataTable Table, string MaPhongBan, string TenPhongBan, int No)
        {
            RowView[0] = "";
            RowView[1] = TenPhongBan;
            RowView[2] = "Số công nhân " + Table.Select("MAPHONGBAN='" + MaPhongBan + "'").Length;

            int nCol = 3;

            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double TongSoPhut = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        if (gio > 0 && gio - 8 > 0)
                        {
                            TongSoPhut += phut + (gio - 8) * 60;
                        }
                    }
                }

                RowView[nCol] = SoGio(TongSoPhut);
                nCol++;
            }
            RowView[nCol] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void DetailRow_GioDu(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[2] = rDetail["MANHANVIEN"].ToString().Trim();
            RowView[1] = rDetail["HOTEN"].ToString().Trim();

            double tongthuong = 0;
            double tongCN = 0;
            double tong = 0;

            int nCol = 3;
            for (int i = 4; i < rDetail.ItemArray.Length; i++)
            {
                if (rDetail[i].ToString().Contains(":"))
                {
                    string[] temp = rDetail[i].ToString().Split(':');
                    int gio = temp[0].ToInt();
                    int phut = temp[1].ToInt();
                    if (gio > 0 && gio - 8 > 0)
                    {
                        RowView[nCol] = gio - 8 + ":" + phut;

                        DateTime zD = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, i - 3);
                        if (zD.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tongCN += phut + (gio - 8) * 60;
                        }
                        else
                        {
                            tongthuong += phut + (gio - 8) * 60;
                        }

                        tong = tongCN + tongthuong;
                    }
                }
                nCol++;
            }

            RowView[nCol] = SoGio(tongthuong);
            RowView[nCol + 1] = SoGio(tongCN);
            RowView[nCol + 2] = SoGio(tong);
        }
        private void TotalRow_GioDu(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "Tổng ";
            RowView[2] = "Số công nhân " + Table.Rows.Count;

            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double TongSoPhut = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        if (gio > 0 && gio - 8 > 0)
                        {
                            TongSoPhut += phut + (gio - 8) * 60;
                        }
                    }
                }

                RowView[nCol] = SoGio(TongSoPhut);
                nCol++;
            }
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        #endregion

        string SoGio(double TongSoPhut)
        {
            if (TongSoPhut > 0)
            {
                double zTam = TongSoPhut / 60;
                string[] zTam1 = zTam.Ton1String().Split('.');
                return zTam1[0] + ":" + ((zTam1[1].ToFloat() * 60) / 10).ToString();
            }
            return string.Empty;
        }
    }
}