﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_FeeChildren : Form
    {
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        public Frm_FeeChildren()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            Utils.DoubleBuffered(GV_Department, true);
            Utils.DrawGVStyle(ref GV_Department);
            Utils.DoubleBuffered(GVTeam, true);
            Utils.DrawGVStyle(ref GVTeam);

            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            btn_Import.Click += Btn_Import_Click;
            btn_Add.Click += Btn_Save_Click;
            GV_Department.Click += GVDepartment_Click;
            GVTeam.Click += GVTeam_Click;
            GVData.CellEndEdit += GVData_CellEndEdit;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        private void Frm_FeeChildren_Load(object sender, EventArgs e)
        {
            InitLayout_GVDepartment(GV_Department);
            DataTable zTable = Employee_FeeChildren_Data.ListDepartment();
            InitData_GVDepartment(GV_Department, zTable);
            InitLayout_GVTeam(GVTeam);
            InitGV_Layout(GVData);
        }
        private void GVDepartment_Click(object sender, EventArgs e)
        {
            if (GV_Department.SelectedRows.Count > 0 &&
                GV_Department.CurrentRow.Cells["DepartmentID"].Tag != null)
            {
                //Clear các bảng con khi chọn lại
                _DepartmentKey = 0;
                _DepartmentKey = int.Parse(GV_Department.CurrentRow.Cells["DepartmentID"].Tag.ToString());

                DataTable zTable = Employee_FeeChildren_Data.ListTeam(_DepartmentKey);
                InitData_GVTeam(GVTeam, zTable);
            }
        }
        private void GVTeam_Click(object sender, EventArgs e)
        {
            if (GVTeam.SelectedRows.Count > 0 &&
                GVTeam.CurrentRow.Cells["TeamID"].Tag != null)
            {
                _TeamKey = 0;
                _TeamKey = int.Parse(GVTeam.CurrentRow.Cells["TeamID"].Tag.ToString());
                DataTable zTable = Employee_FeeChildren_Data.List(_TeamKey);
                InitGV_Data(GVData, zTable);
            }
        }

        private void InitLayout_GVDepartment(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("DepartmentID", "Mã");
            GV.Columns.Add("DepartmentName", "Tên");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["DepartmentID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["DepartmentName"].Width = 200;
            GV.Columns["DepartmentName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["DepartmentName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVDepartment(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["DepartmentID"].Tag = r["DepartmentKey"].ToString();
                    GvRow.Cells["DepartmentID"].Value = r["DepartmentID"].ToString();
                    GvRow.Cells["DepartmentName"].Value = r["DepartmentName"].ToString();
                }
            }
            GV.ClearSelection();
        }
        //Lay out Listview List công việc
        private void InitLayout_GVTeam(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("TeamID", "Mã");
            GV.Columns.Add("TeamName", "Tên");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["TeamName"].Width = 200;
            GV.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["TeamName"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void InitData_GVTeam(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            if (Table != null)
            {
                for (int i = 0; i < Table.Rows.Count; i++)
                {
                    DataRow r = Table.Rows[i];
                    GV.Rows.Add();
                    DataGridViewRow GvRow = GV.Rows[i];
                    GvRow.Cells["No"].Value = (i + 1).ToString();
                    GvRow.Cells["TeamID"].Tag = r["TeamKey"].ToString();
                    GvRow.Cells["TeamID"].Value = r["TeamID"].ToString();
                    GvRow.Cells["TeamName"].Value = r["TeamName"].ToString();
                }
            }
            GV.ClearSelection();
        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_FeeChildren frm = new Frm_Import_FeeChildren();
            frm.Show();
        }

        void InitGV_Layout(DataGridView GV)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "HỌ VÀ TÊN");
            GV.Columns.Add("EmployeeID", "SỐ THẺ");
            GV.Columns.Add("Number", "Số con");
            GV.Columns.Add("FromDate", "Từ ngày");
            GV.Columns.Add("ToDate", "Đến ngày");
            GV.Columns.Add("Description", "Ghi chú");
            GV.Columns.Add("Message", "Thông báo");

            GV.Columns["Message"].Width = 200;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 200;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 100;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["Number"].Width = 60;
            GV.Columns["Number"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Number"].ReadOnly = true;

            GV.Columns["FromDate"].Width = 100;
            GV.Columns["FromDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ToDate"].Width = 100;
            GV.Columns["ToDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Description"].Width = 150;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.ColumnHeadersHeight = 45;
            #endregion
        }

        void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GVData.Rows.Clear();
            foreach (DataRow nRow in zTable.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (no + 1).ToString();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();

                double zNumber = 0;
                if (nRow["Number"] != null)
                {
                    zNumber = nRow["Number"].ToDouble();
                }
                nRowView.Cells["Number"].Value = zNumber.Ton0String();
                if (nRow["FromDate"].ToString() == "")
                {
                    nRowView.Cells["FromDate"].Value = "";
                }
                else
                {
                    DateTime zFromDate = DateTime.Parse(nRow["FromDate"].ToString());
                    nRowView.Cells["FromDate"].Value = zFromDate.ToString("dd/MM/yyyy");
                }

                if (nRow["ToDate"].ToString() == "")
                {
                    nRowView.Cells["ToDate"].Value = "";
                }
                else
                {
                    DateTime zToDate = DateTime.Parse(nRow["ToDate"].ToString());
                    nRowView.Cells["ToDate"].Value = zToDate.ToString("dd/MM/yyyy");
                }
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                nRowView.Cells["Message"].Tag = 0; // 0 là không có sửa, 1 là đã có sửa
                no++;
            }
        }
        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 4)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["FromDate"].Value == null)
                    zRowEdit.Cells["FromDate"].Value = "";
                else
                {
                    DateTime zFromDate = DateTime.MinValue;
                    if (!DateTime.TryParse(zRowEdit.Cells["FromDate"].Value.ToString(), out zFromDate))
                    {
                        zRowEdit.Cells["FromDate"].Value = "";
                    }
                }
            }
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 5)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["ToDate"].Value == null)
                    zRowEdit.Cells["ToDate"].Value = "";
                else
                {
                    DateTime zToDate = DateTime.MinValue;
                    if (!DateTime.TryParse(zRowEdit.Cells["ToDate"].Value.ToString(), out zToDate))
                    {
                        zRowEdit.Cells["ToDate"].Value = "";
                    }
                }
            }
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 6)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["Description"].Value == null)
                {
                    zRowEdit.Cells["Description"].Value = "";
                }
            }
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if(GVData.Rows.Count==0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu");
                this.Cursor = Cursors.Default;
            }
           else
            {
                Save();
            }
        }
        void Save()
        {
            int zErr = 0;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Tag != null && GVData.Rows[i].Cells["EmployeeID"].Value != null && GVData.Rows[i].Cells["Message"].Tag.ToInt() == 1)
                {
                    Employee_FeeChildren_Info zinfo = new Employee_FeeChildren_Info(GVData.Rows[i].Tag.ToInt());
                    DateTime zFromDate = DateTime.MinValue;
                    if (GVData.Rows[i].Cells["FromDate"].Value.ToString().Trim() != "")
                    {
                        zFromDate = DateTime.Parse(GVData.Rows[i].Cells["FromDate"].Value.ToString().Trim());
                    }
                    DateTime zToDate = DateTime.MinValue;
                    if (GVData.Rows[i].Cells["ToDate"].Value.ToString().Trim() != "")
                    {
                        zToDate = DateTime.Parse(GVData.Rows[i].Cells["ToDate"].Value.ToString().Trim());
                        zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
                    }
                    zinfo.FromDate = zFromDate;
                    zinfo.ToDate = zToDate;
                    zinfo.Description = GVData.Rows[i].Cells["Description"].Value.ToString().Trim();
                    zinfo.CreatedBy = SessionUser.UserLogin.Key;
                    zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Update();
                    if (zinfo.Message == "20")
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Cập nhật thành công!";
                    }
                    else
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Lỗi.Vui lòng liên hệ IT!";
                        zErr ++;
                    }
                }
            }
            if(zErr==0)
            {
                MessageBox.Show("Cập nhật thành công");
                this.Cursor = Cursors.Default;
            }
            else
            {
                MessageBox.Show("Cập nhật còn "+zErr.ToString()+" lỗi.Vui lòng kiểm tra lại");
                this.Cursor = Cursors.Default;
            }
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
