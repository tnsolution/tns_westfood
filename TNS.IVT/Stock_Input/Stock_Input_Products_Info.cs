﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.IVT
{
    public class Stock_Input_Products_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _OrderKey = "";
        private string _ProductKey = "";
        private string _ProductID = "";
        private string _ProductName = "";
        private int _UnitKey = 0;
        private string _UnitName = "";
        private float _QuantityDocument = 0;
        private float _QuantityReality = 0;
        private int _UnitBasicKey = 0;
        private string _UnitBasicName = "";
        private float _Proportion = 0;
        private float _Difference = 0;
        private float _QuantityBasic;
        private double _UnitPrice = 0;
        private double _AmountOrderMain = 0;
        private double _AmountOrderForeign = 0;
        private double _AmountToTalMain = 0;
        private string _CurrencyID = "";
        private float _CurrencyRate = 0;
        //private int _CodeFollow = 0;
        private string _OrderKeyFollow = "";
        private string _OrderIDFollow = "";
        private int _ProductType = 0;
        private int _RecordStatus = 0;
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _Message = "";
        private string _RoleID = "";
        private string _NoteStatus = "";
        private string _CreditAccount = "";
        private string _DebitAccount = "";
        private int _Slug = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private string _ProductFollow = "";
        private string _ProductKeyFollow = "";
        #endregion

        #region [ Constructor Get Information ]
        public Stock_Input_Products_Info()
        {
        }
        public Stock_Input_Products_Info(int AutoKey)
        {
            string zSQL = "SELECT A.*,B.ProductID, B.ProductName FROM IVT_Stock_Input_Products A "
                        + "INNER JOIN IVT_Product B ON A.ProductKey = B.ProductKey "
                        + "WHERE A.AutoKey = @AutoKey AND A.RecordStatus <99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _OrderKey = zReader["OrderKey"].ToString();
                    _ProductKey = zReader["ProductKey"].ToString();
                    _ProductID = zReader["ProductID"].ToString();
                    _ProductName = zReader["ProductName"].ToString();
                    _UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    _UnitName = zReader["UnitName"].ToString();
                    _QuantityDocument = float.Parse(zReader["QuantityDocument"].ToString());
                    _QuantityReality = float.Parse(zReader["QuantityReality"].ToString());
                    _UnitBasicKey = int.Parse(zReader["UnitBasicKey"].ToString());
                    _UnitBasicName = zReader["UnitBasicName"].ToString();
                    _QuantityBasic = float.Parse(zReader["QuantityBasic"].ToString());
                    _UnitPrice = double.Parse(zReader["UnitPrice"].ToString());
                    if (zReader["AmountOrderMain"] != DBNull.Value)
                        _AmountOrderMain = double.Parse(zReader["AmountOrderMain"].ToString());
                    if (zReader["AmountOrderForeign"] != DBNull.Value)
                        _AmountOrderForeign = double.Parse(zReader["AmountOrderForeign"].ToString());
                    if (zReader["AmountTotalMain"] != DBNull.Value)
                        _AmountToTalMain = double.Parse(zReader["AmountTotalMain"].ToString());
                    _CurrencyID = zReader["CurrencyID"].ToString();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        _CurrencyRate = float.Parse(zReader["CurrencyRate"].ToString());

                    // _CodeFollow = int.Parse(zReader["CodeFollow"].ToString());
                    _OrderIDFollow = zReader["OrderIDFollow"].ToString();
                    _OrderKeyFollow = zReader["OrderKeyFollow"].ToString();
                    if (zReader["ProductType"] != null)
                        _ProductType = int.Parse(zReader["ProductType"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _NoteStatus = zReader["NoteStatus"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _CreditAccount = zReader["CreditAccount"].ToString();
                    _DebitAccount = zReader["DebitAccount"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    _ProductFollow = zReader["ProductFollow"].ToString();
                    _ProductKeyFollow = zReader["ProductKeyFollow"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally { zConnect.Close(); }
        }
        public Stock_Input_Products_Info(string OrderKey, string ProductKey)
        {
            string zSQL = "SELECT A.*,B.ProductID, B.ProductName FROM IVT_Stock_Input_Products A "
                         + "INNER JOIN IVT_Product B ON A.ProductKey = B.ProductKey "
                         + "WHERE A.OrderKey = @OrderKey AND A.ProductKey = @ProductKey AND A.RecordStatus <99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(ProductKey);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(OrderKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _OrderKey = zReader["OrderKey"].ToString();
                    _ProductKey = zReader["ProductKey"].ToString();
                    _ProductID = zReader["ProductID"].ToString();
                    _ProductName = zReader["ProductName"].ToString();
                    _UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    _UnitName = zReader["UnitName"].ToString();
                    _QuantityDocument = float.Parse(zReader["QuantityDocument"].ToString());
                    _QuantityReality = float.Parse(zReader["QuantityReality"].ToString());
                    _UnitBasicKey = int.Parse(zReader["UnitBasicKey"].ToString());
                    _UnitBasicName = zReader["UnitBasicName"].ToString();
                    _QuantityBasic = float.Parse(zReader["QuantityBasic"].ToString());
                    _UnitPrice = double.Parse(zReader["UnitPrice"].ToString());
                    if (zReader["AmountOrderMain"] != DBNull.Value)
                        _AmountOrderMain = double.Parse(zReader["AmountOrderMain"].ToString());
                    if (zReader["AmountOrderForeign"] != DBNull.Value)
                        _AmountOrderForeign = double.Parse(zReader["AmountOrderForeign"].ToString());
                    if (zReader["AmountTotalMain"] != DBNull.Value)
                        _AmountToTalMain = double.Parse(zReader["AmountTotalMain"].ToString());
                    _CurrencyID = zReader["CurrencyID"].ToString();
                    if (zReader["CurrencyRate"] != DBNull.Value)
                        _CurrencyRate = float.Parse(zReader["CurrencyRate"].ToString());
                    //_CodeFollow = int.Parse(zReader["CodeFollow"].ToString());
                    _OrderIDFollow = zReader["OrderIDFollow"].ToString();
                    _OrderKeyFollow = zReader["OrderKeyFollow"].ToString();
                    if (zReader["ProductType"] != null)
                        _ProductType = int.Parse(zReader["ProductType"].ToString());
                    _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _NoteStatus = zReader["NoteStatus"].ToString();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _CreditAccount = zReader["CreditAccount"].ToString();
                    _DebitAccount = zReader["DebitAccount"].ToString();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    _ProductFollow = zReader["ProductFollow"].ToString();
                    _ProductKeyFollow = zReader["ProductKeyFollow"].ToString();
                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Stock_Input_Products_Info(DataRow ProductInRow)
        {
            try
            {
                _AutoKey = int.Parse(ProductInRow["AutoKey"].ToString());
                _OrderKey = ProductInRow["OrderKey"].ToString();
                _ProductKey = ProductInRow["ProductKey"].ToString();
                _ProductID = ProductInRow["ProductID"].ToString();
                _ProductName = ProductInRow["ProductName"].ToString();
                _UnitKey = int.Parse(ProductInRow["UnitKey"].ToString());
                _UnitName = ProductInRow["UnitName"].ToString();
                _QuantityDocument = float.Parse(ProductInRow["QuantityDocument"].ToString());
                _QuantityReality = float.Parse(ProductInRow["QuantityReality"].ToString());
                _UnitBasicKey = int.Parse(ProductInRow["UnitBasicKey"].ToString());
                _UnitBasicName = ProductInRow["UnitBasicName"].ToString();
                _QuantityBasic = float.Parse(ProductInRow["QuantityBasic"].ToString());
                _UnitPrice = double.Parse(ProductInRow["UnitPrice"].ToString());
                if (ProductInRow["AmountOrderForeign"] != DBNull.Value)
                    _AmountOrderForeign = double.Parse(ProductInRow["AmountOrderForeign"].ToString());
                if (ProductInRow["AmountTotalMain"] != DBNull.Value)
                    _AmountToTalMain = double.Parse(ProductInRow["AmountTotalMain"].ToString());
                _CurrencyID = ProductInRow["CurrencyID"].ToString();
                if (ProductInRow["CurrencyRate"] != DBNull.Value)
                    _CurrencyRate = float.Parse(ProductInRow["CurrencyRate"].ToString());
                //_CodeFollow = int.Parse(ProductInRow["CodeFollow"].ToString());
                _OrderKeyFollow = ProductInRow["OrderKeyFollow"].ToString();
                _OrderIDFollow = ProductInRow["OrderIDFollow"].ToString();
                if (ProductInRow["ProductType"] != null)
                    _ProductType = int.Parse(ProductInRow["ProductType"].ToString());
                _RecordStatus = int.Parse(ProductInRow["RecordStatus"].ToString());
                _NoteStatus = ProductInRow["NoteStatus"].ToString();
                if (ProductInRow["ModifiedOn"] != DBNull.Value)
                    _ModifiedOn = (DateTime)ProductInRow["ModifiedOn"];
                _ModifiedBy = ProductInRow["ModifiedBy"].ToString();
                _ModifiedName = ProductInRow["ModifiedName"].ToString();
                _CreditAccount = ProductInRow["CreditAccount"].ToString();
                _DebitAccount = ProductInRow["DebitAccount"].ToString();
                if (ProductInRow["Slug"] != DBNull.Value)
                    _Slug = int.Parse(ProductInRow["Slug"].ToString());
                if (ProductInRow["CreatedOn"] != DBNull.Value)
                    _CreatedOn = (DateTime)ProductInRow["CreatedOn"];
                _CreatedBy = ProductInRow["CreatedBy"].ToString();
                _CreatedName = ProductInRow["CreatedName"].ToString();
                _ProductFollow = ProductInRow["ProductFollow"].ToString();
                _ProductKeyFollow = ProductInRow["ProductKeyFollow"].ToString();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
        }

        public void Get_Stock_Input(string OrderID)
        {
            string zSQL = @"SELECT B.OrderKey ,B.ProductKey 
FROM [dbo].[IVT_Stock_Input] A
LEFT JOIN[dbo].[IVT_Stock_Input_Products] B ON A.OrderKey = B.OrderKey
WHERE A.OrderID = @OrderID AND A.RecordStatus < 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _OrderKey = zReader["OrderKey"].ToString();
                    _ProductKey = zReader["ProductKey"].ToString();

                }
                zReader.Close(); zCommand.Dispose();
            }
            catch (Exception Err) { _Message = Err.ToString(); }
            finally { zConnect.Close(); }
        }


        #endregion

        #region [ Properties ]
        public int AutoKey
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string OrderKey
        {
            get { return _OrderKey; }
            set { _OrderKey = value; }
        }
        public string ProductKey
        {
            get { return _ProductKey; }
            set { _ProductKey = value; }
        }
        public string ProductID
        {
            get { return _ProductID; }
            set { _ProductID = value; }
        }
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }
        public int UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public float QuantityDocument
        {
            get { return _QuantityDocument; }
            set { _QuantityDocument = value; }
        }
        public float QuantityReality
        {
            get { return _QuantityReality; }
            set { _QuantityReality = value; }
        }
        public int UnitBasicKey
        {
            get { return _UnitBasicKey; }
            set { _UnitBasicKey = value; }
        }
        public string UnitBasicName
        {
            get { return _UnitBasicName; }
            set { _UnitBasicName = value; }
        }
        public float Proportion
        {
            get { return _Proportion; }
            set { _Proportion = value; }
        }
        public float Difference
        {
            get { return _Difference; }
            set { _Difference = value; }
        }
        public float QuantityBasic
        {
            get { return _QuantityBasic; }
            set { _QuantityBasic = value; }
        }
        public double UnitPrice
        {
            get { return _UnitPrice; }
            set { _UnitPrice = value; }
        }
        public double AmountOrderMain
        {
            get { return _AmountOrderMain; }
            set { _AmountOrderMain = value; }
        }
        //public int CodeFollow
        //{
        //    get { return _CodeFollow; }
        //    set { _CodeFollow = value; }
        //}
        public string OrderKeyFollow
        {
            get { return _OrderKeyFollow; }
            set { _OrderKeyFollow = value; }
        }
        public string OrderIDFollow
        {
            get { return _OrderIDFollow; }
            set { _OrderIDFollow = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }

        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        public string RoleID
        {
            set { _RoleID = value; }
        }
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                if (_Message.Length > 0)
                {
                    int.TryParse(_Message.Substring(0, 1), out zNumber);
                }
                if (zNumber <= 3)
                    return true;
                else
                    return false;
            }
        }

        public string NoteStatus
        {
            get
            {
                return _NoteStatus;
            }

            set
            {
                _NoteStatus = value;
            }
        }

        public string CreditAccount
        {
            get
            {
                return _CreditAccount;
            }

            set
            {
                _CreditAccount = value;
            }
        }

        public string DebitAccount
        {
            get
            {
                return _DebitAccount;
            }

            set
            {
                _DebitAccount = value;
            }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }

        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }

        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }

        public string CreatedName
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }

        public double AmountOrderForegin
        {
            get
            {
                return _AmountOrderForeign;
            }

            set
            {
                _AmountOrderForeign = value;
            }
        }

        public double AmountToTalMain
        {
            get
            {
                return _AmountToTalMain;
            }

            set
            {
                _AmountToTalMain = value;
            }
        }

        public string CurrentID
        {
            get
            {
                return _CurrencyID;
            }

            set
            {
                _CurrencyID = value;
            }
        }

        public float CurrencyRate
        {
            get
            {
                return _CurrencyRate;
            }

            set
            {
                _CurrencyRate = value;
            }
        }

        public int ProductType
        {
            get
            {
                return _ProductType;
            }

            set
            {
                _ProductType = value;
            }
        }

        public string ProductFollow
        {
            get
            {
                return _ProductFollow;
            }

            set
            {
                _ProductFollow = value;
            }
        }

        public string ProductKeyFollow
        {
            get
            {
                return _ProductKeyFollow;
            }

            set
            {
                _ProductKeyFollow = value;
            }
        }


        #endregion

        #region [ Constructor Update Information ]

        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "IVT_Stock_Input_Products_INSERT ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                zCommand.Parameters.Add("@QuantityDocument", SqlDbType.Float).Value = _QuantityDocument;
                zCommand.Parameters.Add("@QuantityReality", SqlDbType.Float).Value = _QuantityReality;
                zCommand.Parameters.Add("@UnitBasicKey", SqlDbType.Int).Value = _UnitBasicKey;
                zCommand.Parameters.Add("@UnitBasicName", SqlDbType.NVarChar).Value = _UnitBasicName;
                zCommand.Parameters.Add("@QuantityBasic", SqlDbType.Float).Value = _QuantityBasic;
                zCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = _UnitPrice;
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@AmountTotalMain", SqlDbType.Money).Value = _AmountToTalMain;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = _CurrencyRate;
                zCommand.Parameters.Add("@OrderIDFollow", SqlDbType.NVarChar).Value = _OrderIDFollow;
                zCommand.Parameters.Add("@OrderKeyFollow", SqlDbType.NVarChar).Value = _OrderKeyFollow;
                zCommand.Parameters.Add("@ProductType", SqlDbType.Int).Value = _ProductType;
                zCommand.Parameters.Add("@NoteStatus", SqlDbType.NVarChar).Value = _NoteStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ProductFollow", SqlDbType.NVarChar).Value = _ProductFollow;
                zCommand.Parameters.Add("@ProductKeyFollow", SqlDbType.NVarChar).Value = _ProductKeyFollow;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public void Update()
        {
            string zSQL = "IVT_Stock_Input_Products_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@ProductKey", SqlDbType.NVarChar).Value = _ProductKey;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName;
                zCommand.Parameters.Add("@QuantityDocument", SqlDbType.Float).Value = _QuantityDocument;
                zCommand.Parameters.Add("@QuantityReality", SqlDbType.Float).Value = _QuantityReality;
                zCommand.Parameters.Add("@UnitBasicKey", SqlDbType.Int).Value = _UnitBasicKey;
                zCommand.Parameters.Add("@UnitBasicName", SqlDbType.NVarChar).Value = _UnitBasicName;
                zCommand.Parameters.Add("@QuantityBasic", SqlDbType.Float).Value = _QuantityBasic;
                zCommand.Parameters.Add("@UnitPrice", SqlDbType.Money).Value = _UnitPrice;
                zCommand.Parameters.Add("@AmountOrderMain", SqlDbType.Money).Value = _AmountOrderMain;
                zCommand.Parameters.Add("@AmountOrderForeign", SqlDbType.Money).Value = _AmountOrderForeign;
                zCommand.Parameters.Add("@AmountTotalMain", SqlDbType.Money).Value = _AmountToTalMain;
                zCommand.Parameters.Add("@CurrencyID", SqlDbType.NVarChar).Value = _CurrencyID;
                zCommand.Parameters.Add("@CurrencyRate", SqlDbType.Float).Value = _CurrencyRate;
                zCommand.Parameters.Add("@OrderIDFollow", SqlDbType.NVarChar).Value = _OrderIDFollow;
                zCommand.Parameters.Add("@OrderKeyFollow", SqlDbType.NVarChar).Value = _OrderKeyFollow;
                zCommand.Parameters.Add("@ProductType", SqlDbType.Int).Value = _ProductType;
                zCommand.Parameters.Add("@NoteStatus", SqlDbType.NVarChar).Value = _NoteStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@CreditAccount", SqlDbType.NVarChar).Value = _CreditAccount;
                zCommand.Parameters.Add("@DebitAccount", SqlDbType.NVarChar).Value = _DebitAccount;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@ProductFollow", SqlDbType.NVarChar).Value = _ProductFollow;
                zCommand.Parameters.Add("@ProductKeyFollow", SqlDbType.NVarChar).Value = _ProductKeyFollow;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        public void Save()
        {
            //if (_AutoKey == 0 && _ProductKey > 0)
            //    Create();
            //if (_AutoKey > 0 && _ProductKey > 0)
            //    Update();
            //if (_AutoKey > 0 && _ProductKey == 0)
            //    Delete();
        }
        public string DeleteAll()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "IVT_Stock_Input_Products_DELETE_ALL";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = _OrderKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteDetail()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "IVT_Stock_Input_Products_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public void UpdateStutas()
        {
            string zSQL = "UPDATE IVT_Stock_Input_Products SET Slug = 1 WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "80" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }
        #endregion
    }
}
