﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Time_Holiday : Form
    {
        public Frm_Time_Holiday()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            dte_Search.ValueChanged += dte_Search_ValueChanged;
            LV_List.Click += LV_List_Click;
            LV_List.KeyDown += LV_List_KeyDown;
        }

        private void LV_List_KeyDown(object sender, KeyEventArgs e)
        {
            string zMessage = "";
            Time_Holiday_Info zinfo;
            if (e.KeyData == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < LV_List.Items.Count; i++)
                    {
                        if (LV_List.Items[i].Selected)
                        {
                            zinfo = new Time_Holiday_Info();
                            zinfo.Key = int.Parse(LV_List.Items[i].Tag.ToString());
                            zinfo.Delete();
                            zMessage += zinfo.Message; 
                        }
                    }
                    if (zMessage.Length == 0)
                    {
                        MessageBox.Show("Xóa Thành Công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LV_LoadData();
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
        }

        #region[Private]
        private int _HolidayKey = 0;
        private DateTime _Date;
        DateTime FromDate;
        DateTime ToDate;
        #endregion

        private void Frm_CategoryHoliday_Load(object sender, EventArgs e)
        {
            _Date = dte_Search.Value;
            LV_Layout(LV_List);
            LV_LoadData();
            dte_SearchDate.Value = SessionUser.Date_Work;
            dte_Holiday.Value = SessionUser.Date_Work;
        }

        #region[Progess]
        private void LoadData()
        {
            Time_Holiday_Info zinfo = new Time_Holiday_Info(_HolidayKey);
            dte_Holiday.Value = zinfo.Holiday;
            txt_HolidayName.Text = zinfo.HolidayName;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            txt_Factor.Text = zinfo.Factor.ToString();
        }

        private void SetDefault()
        {
            _HolidayKey = 0;
            dte_Holiday.Value = DateTime.Now;
            txt_HolidayName.Text = "";
            txt_Rank.Text = "";
            txt_HolidayName.Text = "";
            txt_Factor.Text = "1";
            dte_Holiday.Focus();
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_HolidayName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Vui lòng nhập tên ngày nghỉ/lễ!");
                return;
            }
            float zFactor;
            if (!float.TryParse(txt_Factor.Text, out zFactor))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            else
            {
                txt_HolidayName.BackColor = Color.White;
                Time_Holiday_Info zinfo = new Time_Holiday_Info(_HolidayKey);
                zinfo.Holiday = dte_Holiday.Value;
                zinfo.HolidayName = txt_HolidayName.Text.Trim();
                if (txt_Rank.Text.Length == 0)
                {
                    zinfo.Rank = 0;
                }
                else
                    zinfo.Rank = int.Parse(txt_Rank.Text);
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Factor = float.Parse(txt_Factor.Text.Trim());
                zinfo.Save();
                if (zinfo.Message == string.Empty)
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LV_LoadData();
                    SetDefault();
                }
                else
                {
                    MessageBox.Show(zinfo.Message);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_HolidayKey == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Time_Holiday_Info zinfo = new Time_Holiday_Info(_HolidayKey);
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        MessageBox.Show("Đã xóa !");
                        LV_LoadData();
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }

        }
        private void dte_Search_ValueChanged(object sender, EventArgs e)
        {
            _Date = dte_Search.Value;
            LV_LoadData();
        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            DataTable zTable = Time_Holiday_Data.Search(dte_Search.Value,txt_SearchName.Text);
            LV_LoadDataSearch(zTable);
        }
        private void txt_Factor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar > '0' && e.KeyChar < '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar > '0' && e.KeyChar < '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
        #endregion

        #region[ListView]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Nội dung";
            colHead.Width = 190;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Hệ số";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Ghi chú ";
            //colHead.Width = 200;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Sắp xếp ";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Center;
            //LV.Columns.Add(colHead);
        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();

            FromDate = new DateTime(_Date.Year, 1, 1, 0, 0, 0);
            ToDate = FromDate.AddYears(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            //lbl_Title.Text = "DANH SÁCH NGÀY LỄ TRONG NĂM " + _Date.Year;
            DataTable ztb = Time_Holiday_Data.ListYear(FromDate, ToDate);
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["HolidayKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zHoliday = DateTime.Parse(nRow["Holiday"].ToString().Trim());
                lvsi.Text = zHoliday.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["HolidayName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //double zFactor = double.Parse(nRow["Factor"].ToString().Trim());
                //lvsi.Text = Math.Round(zFactor,1).ToString();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Rank"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
            SetDefault();

        }
        private void LV_LoadDataSearch(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();

            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["HolidayKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zHoliday = DateTime.Parse(nRow["Holiday"].ToString().Trim());
                lvsi.Text = zHoliday.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["HolidayName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Rank"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
            SetDefault();
        }
        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _HolidayKey = int.Parse(LV_List.SelectedItems[0].Tag.ToString());
            LoadData();
        }
        #endregion


    }
}
