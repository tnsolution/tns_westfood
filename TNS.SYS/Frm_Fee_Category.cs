﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Libary.HumanResources;
using TN.Library.HumanResources;
using TN.Library.Miscellaneous;

namespace TN_WinApp
{
    public partial class Frm_Fee_Category : Form
    {
        public Frm_Fee_Category()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            LV_List.Click += LV_List_Click;
        }
        #region[Private]
        private int _Key = 0;
        #endregion
        DataTable ztbSlug = new DataTable();
        private void Fee_Category_Load(object sender, EventArgs e)
        {
            LoadTableSlug();
            LoadDataToToolbox.ComboBoxData(cbo_Slug, ztbSlug, false, 0, 1);
            LV_Layout(LV_List);
            LV_LoadData();
        }
        #region[Progess]

        void LoadTableSlug()
        {
            ztbSlug = new DataTable();
            ztbSlug.Columns.Add("Value");
            ztbSlug.Columns.Add("Name");
            ztbSlug.Rows.Add("0", "--Chọn--");
            ztbSlug.Rows.Add("1", "Hỗ trợ");
            ztbSlug.Rows.Add("2", "Khấu trừ");
            ztbSlug.Rows.Add("3", "Phí thu");
            ztbSlug.Rows.Add("4", "Phí chi");
        }
        private void LoadData()
        {

            Fee_Category_Info zinfo = new Fee_Category_Info(_Key);
            txt_CategoryID.Text = zinfo.CategoryID;
            txt_CategoryName.Text = zinfo.CategoryName;
            txt_Quantity.Text = zinfo.Quantity.ToString();
            txt_Price.Text = zinfo.Price.ToString();
            txt_Amount.Text = zinfo.Amount.ToString();
            txt_Ext.Text = zinfo.Ext;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            cbo_Slug.SelectedValue = zinfo.Slug.ToString();
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_CategoryID.Text = "";
            txt_CategoryName.Text = "";
            txt_Quantity.Text = "0";
            txt_Price.Text = "0";
            txt_Amount.Text = "0";
            txt_Rank.Text = "0";
            txt_Description.Text = "";
            txt_Ext.Text = "";
            cbo_Slug.SelectedIndex = 0;
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_CategoryID.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã!");
                return;
            }
            if (txt_CategoryName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập tên chi phí!");
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            if (cbo_Slug.SelectedIndex == 0)
            {
                MessageBox.Show("Chưa chọn loại!");
                return;
            }
            else
            {
                Fee_Category_Info zinfo = new Fee_Category_Info(_Key);
                zinfo.CategoryID = txt_CategoryID.Text.Trim().ToUpper();
                zinfo.CategoryName = txt_CategoryName.Text.Trim();
                zinfo.Quantity = float.Parse(txt_Quantity.Text.Trim());
                zinfo.Price = float.Parse(txt_Price.Text.Trim().Replace(".",""));
                zinfo.Amount = float.Parse(txt_Amount.Text.Trim().Replace(".", ""));
                zinfo.Ext = txt_Ext.Text.Trim();
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Slug = int.Parse(cbo_Slug.SelectedValue.ToString());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                if (zinfo.Message == string.Empty)
                {
                    MessageBox.Show("Cập nhật thành công!");
                    LV_LoadData();
                    SetDefault();
                }
                else
                {
                    MessageBox.Show(zinfo.Message);
                }
            }


        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!");
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Fee_Category_Info zinfo = new Fee_Category_Info(_Key);
                    zinfo.Delete();
                    if (zinfo.Message == string.Empty)
                    {
                        MessageBox.Show("Đã xóa !");
                        LV_LoadData();
                        SetDefault();
                    }
                    else
                    {
                        MessageBox.Show(zinfo.Message);
                    }

                }
            }

        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DataTable zTable = Fee_Category_Data.Search(txt_Search.Text.Trim());
            LV_LoadDataSearch(zTable);
        }
        #endregion


        #region[ListView]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên chi phí";
            colHead.Width = 195;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Hình thức";
            //colHead.Width = 80;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Số lượng";
            //colHead.Width = 80;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Đơn giá";
            //colHead.Width = 80;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Thành tiền";
            //colHead.Width = 100;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Loại";
            //colHead.Width = 80;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Diễn giải";
            //colHead.Width = 125;
            //colHead.TextAlign = HorizontalAlignment.Left;
            //LV.Columns.Add(colHead);

            //colHead = new ColumnHeader();
            //colHead.Text = "Sắp xếp ";
            //colHead.Width = 70;
            //colHead.TextAlign = HorizontalAlignment.Right;
            //LV.Columns.Add(colHead);
        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            DataTable ztb = Fee_Category_Data.List();
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["CategoryKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Ext"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //double zQuantity = double.Parse(nRow["Quantity"].ToString().Trim());
                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = Math.Round(zQuantity, 3).ToString();
                //lvi.SubItems.Add(lvsi);

                //double zPrice = double.Parse(nRow["Price"].ToString().Trim());
                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = zPrice.ToString("###,###,###");
                //lvi.SubItems.Add(lvsi);

                //double zAmount = double.Parse(nRow["Amount"].ToString().Trim());
                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = zAmount.ToString("###,###,###");
                //lvi.SubItems.Add(lvsi);

                //int zCategoryKey = int.Parse(nRow["Slug"].ToString().Trim());
                //if (zCategoryKey == 1)
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Hỗ trợ";
                //    lvi.SubItems.Add(lvsi);
                //}
                //else if (zCategoryKey == 2)
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Khấu trừ";
                //    lvi.SubItems.Add(lvsi);
                //}
                //else if (zCategoryKey == 3)
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Khác";
                //    lvi.SubItems.Add(lvsi);
                //}
                //else
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Chưa xác định";
                //    lvi.SubItems.Add(lvsi);
                //}

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Rank"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }

        private void LV_LoadDataSearch(DataTable In_Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_List;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            for (int i = 0; i < In_Table.Rows.Count; i++)
            {
                DataRow nRow = In_Table.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["CategoryKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["CategoryName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Ext"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //double zQuantity = double.Parse(nRow["Quantity"].ToString().Trim());
                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = Math.Round(zQuantity, 3).ToString();
                //lvi.SubItems.Add(lvsi);

                //double zPrice = double.Parse(nRow["Price"].ToString().Trim());
                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = zPrice.ToString("###,###,###");
                //lvi.SubItems.Add(lvsi);

                //double zAmount = double.Parse(nRow["Amount"].ToString().Trim());
                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text =  zAmount.ToString("###,###,###");
                //lvi.SubItems.Add(lvsi);



                //int zCategoryKey = int.Parse(nRow["Slug"].ToString().Trim());
                //if (zCategoryKey == 1)
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Hỗ trợ";
                //    lvi.SubItems.Add(lvsi);
                //}
                //else if (zCategoryKey == 2)
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Khấu trừ";
                //    lvi.SubItems.Add(lvsi);
                //}
                //else if (zCategoryKey == 3)
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Khác";
                //    lvi.SubItems.Add(lvsi);
                //}
                //else
                //{
                //    lvsi = new ListViewItem.ListViewSubItem();
                //    lvsi.Text = "Chưa xác định";
                //    lvi.SubItems.Add(lvsi);
                //}

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Description"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                //lvsi = new ListViewItem.ListViewSubItem();
                //lvsi.Text = nRow["Rank"].ToString().Trim();
                //lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }




        #endregion
        private void LV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_List.Items.Count; i++)
            {
                if (LV_List.Items[i].Selected == true)
                {
                    LV_List.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_List.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Key = int.Parse(LV_List.SelectedItems[0].Tag.ToString());
            LoadData();
        }

        private void txt_Quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }
           
        }

        private void txt_Price_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }
          

        }


        private void txt_Quantity_Leave(object sender, EventArgs e)
        {
            double zQuantity;
            if (!double.TryParse(txt_Quantity.Text, out zQuantity))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            double zPrice;
            if (!double.TryParse(txt_Price.Text, out zPrice))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            txt_Amount.Text = Math.Round((zQuantity * zPrice), 2).ToString();
        }

        private void txt_Price_Leave(object sender, EventArgs e)
        {
            double zQuantity;
            if (!double.TryParse(txt_Quantity.Text, out zQuantity))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            double zPrice;
            if (!double.TryParse(txt_Price.Text, out zPrice))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!");
                return;
            }
            txt_Amount.Text = Math.Round((zQuantity * zPrice), 2).ToString();
        }
    }
}
