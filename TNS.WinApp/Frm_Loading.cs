﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TNS.WinApp
{
    public partial class Frm_Loading : Form
    {
        public Action _Worker { get; set; }
        public Frm_Loading(Action Worker)
        {
            InitializeComponent();
            if (Worker != null)
            {
                _Worker = Worker;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (_Worker != null)
                Task.Factory.StartNew(_Worker).ContinueWith(t => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
