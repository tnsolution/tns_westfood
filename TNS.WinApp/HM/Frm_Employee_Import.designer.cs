﻿namespace TNS.WinApp
{
    partial class Frm_Employee_Import
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Employee_Import));
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_Error = new System.Windows.Forms.TextBox();
            this.txt_Amout_SQL = new System.Windows.Forms.TextBox();
            this.txt_Amount_Excel = new System.Windows.Forms.TextBox();
            this.txt_Excel = new System.Windows.Forms.TextBox();
            this.btn_Often = new System.Windows.Forms.Button();
            this.btn_Up = new System.Windows.Forms.Button();
            this.cbo_Sheet = new System.Windows.Forms.ComboBox();
            this.GV_Employee = new System.Windows.Forms.DataGridView();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1019, 58);
            this.panel2.TabIndex = 37;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(3, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 55);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.label2.Location = new System.Drawing.Point(100, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "IMPORT NHÂN SỰ";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txt_Error);
            this.panel3.Controls.Add(this.txt_Amout_SQL);
            this.panel3.Controls.Add(this.txt_Amount_Excel);
            this.panel3.Controls.Add(this.txt_Excel);
            this.panel3.Controls.Add(this.btn_Often);
            this.panel3.Controls.Add(this.btn_Up);
            this.panel3.Controls.Add(this.cbo_Sheet);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 58);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1019, 91);
            this.panel3.TabIndex = 38;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.Location = new System.Drawing.Point(578, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Số Lượng Lỗi";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label4.Location = new System.Drawing.Point(571, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Số Lượng SQL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label3.Location = new System.Drawing.Point(563, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Số Lượng Excel";
            // 
            // txt_Error
            // 
            this.txt_Error.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Error.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.txt_Error.Location = new System.Drawing.Point(672, 33);
            this.txt_Error.Name = "txt_Error";
            this.txt_Error.ReadOnly = true;
            this.txt_Error.Size = new System.Drawing.Size(99, 21);
            this.txt_Error.TabIndex = 5;
            // 
            // txt_Amout_SQL
            // 
            this.txt_Amout_SQL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Amout_SQL.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.txt_Amout_SQL.Location = new System.Drawing.Point(672, 58);
            this.txt_Amout_SQL.Name = "txt_Amout_SQL";
            this.txt_Amout_SQL.ReadOnly = true;
            this.txt_Amout_SQL.Size = new System.Drawing.Size(99, 21);
            this.txt_Amout_SQL.TabIndex = 5;
            // 
            // txt_Amount_Excel
            // 
            this.txt_Amount_Excel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Amount_Excel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.txt_Amount_Excel.Location = new System.Drawing.Point(672, 6);
            this.txt_Amount_Excel.Name = "txt_Amount_Excel";
            this.txt_Amount_Excel.ReadOnly = true;
            this.txt_Amount_Excel.Size = new System.Drawing.Size(99, 21);
            this.txt_Amount_Excel.TabIndex = 5;
            // 
            // txt_Excel
            // 
            this.txt_Excel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Excel.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.txt_Excel.Location = new System.Drawing.Point(15, 12);
            this.txt_Excel.Name = "txt_Excel";
            this.txt_Excel.Size = new System.Drawing.Size(265, 21);
            this.txt_Excel.TabIndex = 0;
            // 
            // btn_Often
            // 
            this.btn_Often.BackColor = System.Drawing.Color.Transparent;
            this.btn_Often.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Often.BackgroundImage")));
            this.btn_Often.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Often.FlatAppearance.BorderSize = 0;
            this.btn_Often.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Often.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Often.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_Often.Location = new System.Drawing.Point(295, 12);
            this.btn_Often.Name = "btn_Often";
            this.btn_Often.Size = new System.Drawing.Size(75, 23);
            this.btn_Often.TabIndex = 1;
            this.btn_Often.Text = "Mở File";
            this.btn_Often.UseVisualStyleBackColor = false;
            // 
            // btn_Up
            // 
            this.btn_Up.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Up.BackgroundImage")));
            this.btn_Up.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Up.FlatAppearance.BorderSize = 0;
            this.btn_Up.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Up.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Up.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_Up.Location = new System.Drawing.Point(777, 6);
            this.btn_Up.Name = "btn_Up";
            this.btn_Up.Size = new System.Drawing.Size(75, 23);
            this.btn_Up.TabIndex = 3;
            this.btn_Up.Text = "Upload";
            this.btn_Up.UseVisualStyleBackColor = true;
            // 
            // cbo_Sheet
            // 
            this.cbo_Sheet.Enabled = false;
            this.cbo_Sheet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbo_Sheet.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.cbo_Sheet.FormattingEnabled = true;
            this.cbo_Sheet.Location = new System.Drawing.Point(15, 38);
            this.cbo_Sheet.Name = "cbo_Sheet";
            this.cbo_Sheet.Size = new System.Drawing.Size(265, 23);
            this.cbo_Sheet.TabIndex = 4;
            // 
            // GV_Employee
            // 
            this.GV_Employee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Employee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Employee.Location = new System.Drawing.Point(0, 149);
            this.GV_Employee.Name = "GV_Employee";
            this.GV_Employee.Size = new System.Drawing.Size(1019, 360);
            this.GV_Employee.TabIndex = 39;
            // 
            // Frm_Employee_Import
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 509);
            this.Controls.Add(this.GV_Employee);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "Frm_Employee_Import";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_Employee_Import";
            this.Load += new System.EventHandler(this.Frm_Employee_Import_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_Error;
        private System.Windows.Forms.TextBox txt_Amout_SQL;
        private System.Windows.Forms.TextBox txt_Amount_Excel;
        private System.Windows.Forms.TextBox txt_Excel;
        private System.Windows.Forms.Button btn_Often;
        private System.Windows.Forms.Button btn_Up;
        private System.Windows.Forms.ComboBox cbo_Sheet;
        private System.Windows.Forms.DataGridView GV_Employee;
    }
}