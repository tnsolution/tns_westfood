﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.HumanResources;
using TN.Library.Miscellaneous;
using TN.Library.Productivity;
using TN.Library.Salary;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Money : Form
    {
        private DateTime _Date;
        private float _ToTal_Time_Team = 0;
        private int _Person_Team = 0;
        private double _ToTal_Money_Team = 0;
        private int _ToTalTime = 0;
        private double _Money_Work = 0;
        private double _Money_Rieng = 0;
        private double _Money_General = 0;
        private int _TeamKey;
        private int _Count;
        private string _Message = "";
        private DataTable _Table;
        private Salary_Product_Worker_Info zSalary;
        public Frm_Money()
        {
            InitializeComponent();
            dte_Month.CustomFormat = "MM/yyyy";
            dte_Teams.CustomFormat = "dd/MM/yyyy";
            #region [Tab_Control_1]
            //--- ListView
            LV_Team_Layout(LV_Teams);
            LV_Teams.Click += LV_Teams_Click;
            //--- DataGridView
            GV_Team_Layout();
            //--- Button
            btn_Count_All.Click += Btn_Count_All_Click;
            btn_Save_All.Click += Btn_Save_All_Click;
            #endregion

            #region [Tab_Control_2]
            //--- ListView
            LV_Team_Month_Layout(LV_Teams_Month);
            LV_Teams_Month.Click += LV_Teams_Month_Click;
            //--- DataGridView
            GV_Team_Month_Layout();
            //--- Button
            btn_Count_Month.Click += Btn_Count_Month_Click;
            timer1.Tick += Timer1_Tick;
            #endregion

            btn_Hide_Log.Visible = false;
            btn_Show_Log.Visible = true;
            groupBox6.Visible = false;
            btn_Hide_Log.Click += Btn_Hide_Click;
            btn_Show_Log.Click += Btn_Show_Log_Click;
        }

        private void Btn_Show_Log_Click(object sender, EventArgs e)
        {
            groupBox6.Visible = true;
            btn_Hide_Log.Visible = true;
            btn_Show_Log.Visible = false;
        }
        private void Btn_Hide_Click(object sender, EventArgs e)
        {
            groupBox6.Visible = false;
            btn_Hide_Log.Visible = false;
            btn_Show_Log.Visible = true;
        }

        private void Frm_Money_Load(object sender, EventArgs e)
        {
            dte_Teams.Value = SessionUser.Date_Work;
            dte_Month.Value = SessionUser.Date_Work;
            LV_Team_LoadData();
            LV_Team_Month_LoadData();
            btn_Save_All.Visible = false;
            _Date = dte_Month.Value;
            timer1.Interval = 500;
        }

        #region [TỔNG HỢP LƯƠNG NĂNG SUẤT NGÀY]

        #region ----- Panel Left -----

        #region [Desgin Layout ListView]
        private void LV_Team_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


        }

        public void LV_Team_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Teams;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Teams_Data.List();

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region [Process Envent]
        private void LV_Teams_Click(object sender, EventArgs e)
        {
            lbl_Teams.Text = "TÍNH LƯƠNG NĂNG SUẤT NGÀY " + dte_Teams.Value.ToString("dd/MM/yyyy");
            for (int i = 0; i < LV_Teams.Items.Count; i++)
            {
                if (LV_Teams.Items[i].Selected == true)
                {
                    LV_Teams.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_Teams.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _TeamKey = LV_Teams.SelectedItems[0].Tag.ToInt();
            GV_Team_LoadData();
            btn_Count_All.Visible = true;
            btn_Save_All.Visible = false;
        }
        #endregion

        #endregion

        #region [Biến]
        private double _TienLam = 0;
        private double _TienMuon = 0;

        private float _Time = 0;
        private float _TimeTeam = 0;
        private float _TimeEat = 0;
        private float _Time_Private = 0;
        private float _TimeBorrowGenaral = 0;
        private float zToTal_Time = 0;
        private float zToTalTimeDifBorrowEat = 0;
        private float _ToTalDifTimeBorrowEat = 0;
        private float _TimeDifBorrowGeneral = 0;
        private float _Time_Borrow = 0;
        private float _Time_Eat = 0;
        private float _ToTalTimeTeamBorrowEat = 0;

        private double _ToTal_Money = 0;
        private double _ToTal_Money_Borrow = 0;
        private double _ToTal_MOney_Eat = 0;

        private double _Money = 0;
        private double _Money_Eat = 0;
        private double _MoneyPrivate = 0;
        private double _Money_Dif = 0;
        private double _Money_Dif_Sunday = 0;
        private double _Money_Dif_Holiday = 0;
        private double _Money_Dif_General = 0;
        private double _Money_Dif_Sunday_General = 0;
        private double _Money_Dif_Holiday_General = 0;
        #endregion

        #region ----- Panel Right -----

        #region [Design Layout DataGridView]
        private void GV_Team_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;
            // Setup Column 
            GV_Team.Columns.Add("No", "STT");
            GV_Team.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Team.Columns.Add("CardID", "Mã NV");
            GV_Team.Columns.Add("Time", "TG Làm Tại Tổ");
            GV_Team.Columns.Add("TimeBorrowPrivate", "TG Làm Tổ Khác (Ăn Riêng)");
            GV_Team.Columns.Add("TimeBorrowGeneral", "TG Làm Tổ Khác (Ăn Chung)");
            GV_Team.Columns.Add("TimeDifBorrowPrivate", "TG Làm Việc Khác (Ăn Riêng)");
            GV_Team.Columns.Add("TimeDifBorrowEat", "TG Làm Việc Khác (Ăn Chung)");
            GV_Team.Columns.Add("Money", "Lương Tổ");
            GV_Team.Columns.Add("Money_Private", "Lương Tổ (Ăn Riêng)");
            GV_Team.Columns.Add("Money_Borrow", "Lương Tổ Khác (Ăn riêng)");
            GV_Team.Columns.Add("Money_Eat", "Lương Tổ Khác (Ăn Chung)");
            GV_Team.Columns.Add("Money_Dif", "Lương Việc Khác (Ăn Riêng)");
            GV_Team.Columns.Add("Money_Dif_Sunday", "Lương Việc Khác (Ăn Riêng) (Chủ Nhật)");
            GV_Team.Columns.Add("Money_Dif_Holiday", "Lương Việc Khác (Ăn Riêng) (Ngày Lễ)");
            GV_Team.Columns.Add("Money_Dif_General", "Lương Việc Khác (Ăn Chung)");
            GV_Team.Columns.Add("Money_Dif_Sunday_General", "Lương Việc Khác (Ăn Chung) (Chủ Nhật)");
            GV_Team.Columns.Add("Money_Dif_Holiday_General", "Lương Việc Khác (Ăn Chung) (Ngày Lễ)");
            GV_Team.Columns.Add("ToTal", "Tổng Tiền Làm Việc Ngày Thường");
            GV_Team.Columns.Add("ToTal_Dif", "Tổng Tiền Làm Việc Khác");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Befor15days";
            zColumn.HeaderText = "15 ngày đầu";
            GV_Team.Columns.Add(zColumn);


            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "After15days";
            zColumn.HeaderText = "15 ngày sau";
            GV_Team.Columns.Add(zColumn);

            GV_Team.Columns["No"].Width = 60;
            GV_Team.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Team.Columns["No"].ReadOnly = true;

            GV_Team.Columns["EmployeeName"].Width = 170;
            GV_Team.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Team.Columns["EmployeeName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["EmployeeName"].ReadOnly = true;

            GV_Team.Columns["CardID"].Width = 70;
            GV_Team.Columns["CardID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Team.Columns["CardID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["CardID"].ReadOnly = true;
            GV_Team.Columns["CardID"].Frozen = true;


            GV_Team.Columns["Time"].Width = 95;
            GV_Team.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Time"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["Time"].ReadOnly = true;

            GV_Team.Columns["TimeBorrowPrivate"].Width = 95;
            GV_Team.Columns["TimeBorrowPrivate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["TimeBorrowPrivate"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["TimeBorrowPrivate"].ReadOnly = true;

            GV_Team.Columns["TimeBorrowGeneral"].Width = 95;
            GV_Team.Columns["TimeBorrowGeneral"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["TimeBorrowGeneral"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["TimeBorrowGeneral"].ReadOnly = true;

            GV_Team.Columns["TimeDifBorrowPrivate"].Width = 95;
            GV_Team.Columns["TimeDifBorrowPrivate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["TimeDifBorrowPrivate"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["TimeDifBorrowPrivate"].ReadOnly = true;

            GV_Team.Columns["TimeDifBorrowEat"].Width = 95;
            GV_Team.Columns["TimeDifBorrowEat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["TimeDifBorrowEat"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["TimeDifBorrowEat"].ReadOnly = true;

            GV_Team.Columns["Money"].Width = 95;
            GV_Team.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["Money"].ReadOnly = true;

            GV_Team.Columns["Money_Private"].Width = 80;
            GV_Team.Columns["Money_Private"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Private"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Team.Columns["Money_Private"].ReadOnly = true;

            GV_Team.Columns["Money_Borrow"].Width = 110;
            GV_Team.Columns["Money_Borrow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Borrow"].ReadOnly = true;

            GV_Team.Columns["Money_Eat"].Width = 110;
            GV_Team.Columns["Money_Eat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Eat"].ReadOnly = true;

            GV_Team.Columns["Money_Dif"].Width = 110;
            GV_Team.Columns["Money_Dif"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Dif"].ReadOnly = true;

            GV_Team.Columns["Money_Dif_Sunday"].Width = 130;
            GV_Team.Columns["Money_Dif_Sunday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Dif_Sunday"].ReadOnly = true;

            GV_Team.Columns["Money_Dif_Holiday"].Width = 130;
            GV_Team.Columns["Money_Dif_Holiday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Dif_Holiday"].ReadOnly = true;

            GV_Team.Columns["Money_Dif_General"].Width = 110;
            GV_Team.Columns["Money_Dif_General"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Dif_General"].ReadOnly = true;

            GV_Team.Columns["Money_Dif_Sunday_General"].Width = 130;
            GV_Team.Columns["Money_Dif_Sunday_General"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Dif_Sunday_General"].ReadOnly = true;

            GV_Team.Columns["Money_Dif_Holiday_General"].Width = 130;
            GV_Team.Columns["Money_Dif_Holiday_General"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Money_Dif_Holiday_General"].ReadOnly = true;


            GV_Team.Columns["ToTal"].Width = 130;
            GV_Team.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["ToTal"].ReadOnly = true;

            GV_Team.Columns["ToTal_Dif"].Width = 130;
            GV_Team.Columns["ToTal_Dif"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["ToTal_Dif"].ReadOnly = true;

            GV_Team.Columns["Befor15days"].Width = 95;
            GV_Team.Columns["Befor15days"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["Befor15days"].ReadOnly = true;

            GV_Team.Columns["After15days"].Width = 95;
            GV_Team.Columns["After15days"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Team.Columns["After15days"].ReadOnly = true;
            // setup style view

            GV_Team.BackgroundColor = Color.White;
            GV_Team.GridColor = Color.FromArgb(227, 239, 255);
            GV_Team.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Team.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Team.DefaultCellStyle.Font = new Font("Arial", 9);
            GV_Team.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 90, 200);
            GV_Team.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(206, 246, 255);
            GV_Team.EnableHeadersVisualStyles = false;
            GV_Team.ColumnHeadersHeight = 40;

            GV_Team.AllowUserToResizeRows = false;
            GV_Team.AllowUserToResizeColumns = true;

            GV_Team.RowHeadersVisible = false;

            //// setup Height Header
            // GV_Employees.ColumnHeadersHeight = GV_Employees.ColumnHeadersHeight * 3 / 2;
            GV_Team.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Team.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            foreach (DataGridViewColumn column in GV_Team.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
            }

            for (int i = 1; i <= 8; i++)
            {
                GV_Team.Rows.Add();
            }
        }
        private void GV_Team_LoadData()
        {
            double zMoneyDifGeneral = 0;
            _ToTal_Time_Team = 0;
            zToTal_Time = 0;
            zToTalTimeDifBorrowEat = 0;
            GV_Team.Rows.Clear();
            _Table = Order_Money_Data.List_Team(_TeamKey, dte_Teams.Value);

            int i = 0;


            foreach (DataRow nRow in _Table.Rows)
            {
                GV_Team.Rows.Add();
                DataGridViewRow nRowView = GV_Team.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeName"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["CardID"].Value = nRow["CardID"].ToString().Trim();

                #region [Thời Gian Làm Tại Tổ, Mượn Ăn Riêng ,Mượn Ăn Chung]
                _TimeTeam = float.Parse(nRow["TGTaiTo"].ToString().Trim());
                nRowView.Cells["Time"].Value = _TimeTeam.ToString();

                if (nRow["TGMuonAnRieng"].ToString() != "")
                {
                    _Time_Private = float.Parse(nRow["TGMuonAnRieng"].ToString().Trim());
                }
                else
                {
                    _Time_Private = 0;
                }
                nRowView.Cells["TimeBorrowGeneral"].Value = _Time_Private.ToString();

                if (nRow["TGMuonAnChung"].ToString() != "")
                {
                    _TimeEat = float.Parse(nRow["TGMuonAnChung"].ToString().Trim());
                    _ToTalTimeTeamBorrowEat += _TimeEat;
                }
                else
                {
                    _TimeEat = 0;
                }
                nRowView.Cells["TimeBorrowGeneral"].Value = _TimeEat.ToString();

                if (nRow["TGKhacMuonAnChung"].ToString() != "")
                {
                    _TimeBorrowGenaral = float.Parse(nRow["TGKhacMuonAnChung"].ToString().Trim());
                }
                else
                {
                    _TimeBorrowGenaral = 0;
                }
                nRowView.Cells["TimeDifBorrowEat"].Value = _TimeBorrowGenaral.ToString();
                zToTalTimeDifBorrowEat += _TimeBorrowGenaral;

                _Time = (_TimeTeam + _Time_Borrow + _Time_Eat + _Time_Private);
                zToTal_Time += _Time;

                #endregion

                #region [Làm Việc Tại Tổ , Mượn Ăn Riêng ,Mượn Ăn Chung]
                _TienLam = double.Parse(nRow["SoTien"].ToString().Trim());
                _Money = double.Parse(nRow["SoTien"].ToString().Trim());

                _MoneyPrivate = double.Parse(nRow["MoneyPrivate"].ToString().Trim());
                nRowView.Cells["Money_Private"].Value = _MoneyPrivate.ToString("#,###,###");

                _ToTal_Money += double.Parse(nRow["SoTien"].ToString().Trim());
                nRowView.Cells["Money"].Value = _TienLam.ToString("#,###,###");

                _TienMuon = double.Parse(nRow["Tienmuon"].ToString().Trim());
                _ToTal_Money_Borrow += double.Parse(nRow["Tienmuon"].ToString().Trim());
                nRowView.Cells["Money_Borrow"].Value = _TienMuon.ToString("#,###,###");

                _Money_Eat = double.Parse(nRow["Tienchung"].ToString().Trim());
                _ToTal_MOney_Eat = double.Parse(nRow["Tienchung"].ToString().Trim());
                nRowView.Cells["Money_Eat"].Value = _Money_Eat.ToString("#,###,###");
                #endregion

                #region [Làm Việc Khác Ăn Riêng]
                if (nRow["MoneyDifPrivate"].ToString() != "")
                {
                    _Money_Dif = double.Parse(nRow["MoneyDifPrivate"].ToString().Trim());
                }
                else
                {
                    _Money_Dif = 0;
                }
                nRowView.Cells["Money_Dif"].Value = _Money_Dif.ToString("#,###,###");

                if (nRow["MoneyDifSundayPrivate"].ToString() != "")
                {
                    _Money_Dif_Sunday = double.Parse(nRow["MoneyDifSundayPrivate"].ToString().Trim());
                }
                else
                {
                    _Money_Dif_Sunday = 0;
                }
                nRowView.Cells["Money_Dif_Sunday"].Value = _Money_Dif_Sunday.ToString("#,###,###");

                if (nRow["MoneyDifHolidayPrivate"].ToString() != "")
                {
                    _Money_Dif_Holiday = double.Parse(nRow["MoneyDifHolidayPrivate"].ToString().Trim());
                }
                else
                {
                    _Money_Dif_Holiday = 0;
                }
                nRowView.Cells["Money_Dif_Holiday"].Value = _Money_Dif_Holiday.ToString("#,###,###");
                #endregion

                #region [Làm Việc Khác Ăn Chung]
                if (nRow["MoneyDifGeneral"].ToString() != "")
                {
                    _Money_Dif_General = double.Parse(nRow["MoneyDifGeneral"].ToString().Trim());
                    zMoneyDifGeneral += _Money_Dif_General;
                }
                else
                {
                    _Money_Dif_General = 0;
                }
                nRowView.Cells["Money_Dif_General"].Value = _Money_Dif_General.ToString("#,###,###");

                if (nRow["MoneyDifSundayGeneral"].ToString() != "")
                {
                    _Money_Dif_Sunday_General = double.Parse(nRow["MoneyDifSundayGeneral"].ToString().Trim());
                    zMoneyDifGeneral += _Money_Dif_Sunday_General;
                }
                else
                {
                    _Money_Dif_Sunday_General = 0;
                }
                nRowView.Cells["Money_Dif_Sunday_General"].Value = _Money_Dif_Sunday_General.ToString("#,###,###");

                if (nRow["MoneyDifHolidayGeneral"].ToString() != "")
                {
                    _Money_Dif_Holiday_General = double.Parse(nRow["MoneyDifHolidayGeneral"].ToString().Trim());
                    zMoneyDifGeneral += _Money_Dif_Sunday_General;
                }
                else
                {
                    _Money_Dif_Holiday_General = 0;
                }
                nRowView.Cells["Money_Dif_Holiday_General"].Value = _Money_Dif_Holiday_General.ToString("#,###,###");
                #endregion

                #region [15 ngày đầu, 15 ngày sau]
                if (nRow["Slug"].ToString() == "0")
                {
                    nRowView.Cells["Befor15days"].Value = true;
                }
                if (nRow["Slug"].ToString() == "1")
                {
                    nRowView.Cells["After15days"].Value = true;
                }
                if (nRow["Slug"].ToString() == "")
                {
                    nRowView.Cells["Befor15days"].Value = false;
                    nRowView.Cells["After15days"].Value = false;
                }
                #endregion

                i++;

            }


            _ToTal_Time_Team = zToTal_Time;
            txt_TotalTimeTeam.Text = _ToTal_Time_Team.ToString();

            _ToTalDifTimeBorrowEat = zToTalTimeDifBorrowEat;
            _TimeDifBorrowGeneral = (float)Math.Round((_ToTalDifTimeBorrowEat / 8), 1);
            txtTimeDifGeneral.Text = _TimeDifBorrowGeneral.ToString();

            // zToTal_Money = _Money + _Money_Borrow;
            _Person_Team = i;
            _ToTal_Money_Team = _ToTal_Money;
            txt_ToTalMoneyTeam.Text = _ToTal_Money_Team.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
            txtTimeDifBorrowEat.Text = _ToTalDifTimeBorrowEat.ToString();

            txtMoneyDifGeneral.Text = zMoneyDifGeneral.ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);

        }
        #endregion

        #region [Test]
        private void Count_All()
        {
            double _Money = 0;
            double _Money_Eat = 0;
            double _Money_Private = 0;
            double _Money_Borrow = 0;
            double _Money_Plus = 0;
            double _MoneyDif = 0;
            double _MoneyDifSunday = 0;
            double _MoneyDifHoliday = 0;
            double _ToTal_money = 0;
            float _Time = 0;
            float _ToTal_Time = 0;
            for (int i = 0; i < GV_Team.Rows.Count - 1; i++)
            {
                if (GV_Team.Rows[i].Cells["Befor15days"].Value != DBNull.Value && (bool)GV_Team.Rows[i].Cells["Befor15days"].Value == false)
                {
                    if (GV_Team.Rows[i].Cells["Money"].Value.ToString() == "")
                    {
                        _Money += 0;
                    }
                    else
                    {
                        _Money += double.Parse(GV_Team.Rows[i].Cells["Money"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Eat"].Value.ToString() == "")
                    {
                        _Money_Eat += 0;
                    }
                    else
                    {
                        _Money_Eat += double.Parse(GV_Team.Rows[i].Cells["Money_Eat"].Value.ToString());
                    }

                    _Time += float.Parse(GV_Team.Rows[i].Cells["Time"].Value.ToString());
                }
            }
            _ToTal_Time = _Time;
            _ToTal_money = _Money + _Money_Eat;
            double _TB = _ToTal_money / _ToTal_Time;
            if (_ToTalTime > 0)
            {
                for (int i = 0; i < GV_Team.Rows.Count - 1; i++)
                {
                    if (GV_Team.Rows[i].Cells["Money"].Value.ToString() == "")
                    {
                        _Money_Work = 0;
                    }
                    else
                    {
                        _Money_Work = double.Parse(GV_Team.Rows[i].Cells["Money"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Borrow"].Value.ToString() == "")
                    {
                        _Money_Rieng = 0;
                    }
                    else
                    {

                        _Money_Borrow = double.Parse(GV_Team.Rows[i].Cells["Money_Borrow"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Plus"].Value == null)
                    {
                        _Money_Plus = 0;
                    }
                    else
                    {
                        _Money_Plus = double.Parse(GV_Team.Rows[i].Cells["Money_Plus"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Private"].Value.ToString() == "")
                    {
                        _Money_Private = 0;
                    }
                    else
                    {
                        _Money_Private = double.Parse(GV_Team.Rows[i].Cells["Money_Private"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Eat"].Value.ToString() == "")
                    {
                        _Money_General = 0;
                    }
                    else
                    {
                        _Money_General = double.Parse(GV_Team.Rows[i].Cells["Money_Eat"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Dif"].Value.ToString() == "")
                    {
                        _MoneyDif = 0;
                    }
                    else
                    {
                        _MoneyDif = double.Parse(GV_Team.Rows[i].Cells["Money_Dif"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Dif_Sunday"].Value.ToString() == "")
                    {
                        _MoneyDifSunday = 0;
                    }
                    else
                    {
                        _MoneyDifSunday = double.Parse(GV_Team.Rows[i].Cells["Money_Dif_Sunday"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Dif_Holiday"].Value.ToString() == "")
                    {
                        _MoneyDifHoliday = 0;
                    }
                    else
                    {
                        _MoneyDifHoliday = double.Parse(GV_Team.Rows[i].Cells["Money_Dif_Holiday"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Befor15days"].Value == null)
                    {
                        _Time = float.Parse(GV_Team.Rows[i].Cells["Time"].Value.ToString());
                        GV_Team.Rows[i].Cells["ToTal"].Value = (_Money_Work + _Money_Rieng + _Money_General + _Money_Private).ToString("#,###,###");
                        GV_Team.Rows[i].Cells["ToTal_Dif"].Value = (_MoneyDif + _MoneyDifSunday + _MoneyDifHoliday).ToString("#,###,###");
                        if (GV_Team.Rows[i].Cells["After15days"].Value != null)
                        {
                            GV_Team.Rows[i].Cells["ToTal"].Value = ((_TB * _Time * 0.85) + _Money_Borrow + _Money_Plus + _Money_Private).ToString("#,###,###");
                            GV_Team.Rows[i].Cells["ToTal_Dif"].Value = ((_TB * _Time * 0.85) + _MoneyDif + _MoneyDifSunday + _MoneyDifHoliday).ToString("#,###,###");
                        }
                    }
                    if (GV_Team.Rows[i].Cells["Befor15days"].Value != null)
                    {
                        _Time = float.Parse(GV_Team.Rows[i].Cells["Time"].Value.ToString());
                        GV_Team.Rows[i].Cells["ToTal"].Value = ((_TB * _Time * (0.85)) + _Money_Borrow + _Money_Plus + _Money_Private).ToString("#,###,###");
                        GV_Team.Rows[i].Cells["ToTal_Dif"].Value = ((_TB * _Time * (0.85)) + _MoneyDif + _MoneyDifSunday + _MoneyDifHoliday).ToString("#,###,###");
                    }
                }
            }
            else
            {
                for (int i = 0; i < GV_Team.Rows.Count - 1; i++)
                {
                    if (GV_Team.Rows[i].Cells["Money"].Value.ToString() == "")
                    {
                        _Money_Work = 0;
                    }
                    else
                    {
                        _Money_Work = double.Parse(GV_Team.Rows[i].Cells["Money"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Borrow"].Value.ToString() == "")
                    {
                        _Money_Rieng = 0;
                    }
                    else
                    {

                        _Money_Rieng = double.Parse(GV_Team.Rows[i].Cells["Money_Borrow"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Private"].Value.ToString() == "")
                    {
                        _Money_Private = 0;
                    }
                    else
                    {
                        _Money_Private = double.Parse(GV_Team.Rows[i].Cells["Money_Private"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Eat"].Value.ToString() == "")
                    {
                        _Money_General = 0;
                    }
                    else
                    {
                        _Money_General = double.Parse(GV_Team.Rows[i].Cells["Money_Eat"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Dif"].Value.ToString() == "")
                    {
                        _MoneyDif = 0;
                    }
                    else
                    {
                        _MoneyDif = double.Parse(GV_Team.Rows[i].Cells["Money_Dif"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Dif_Sunday"].Value.ToString() == "")
                    {
                        _MoneyDifSunday = 0;
                    }
                    else
                    {
                        _MoneyDifSunday = double.Parse(GV_Team.Rows[i].Cells["Money_Dif_Sunday"].Value.ToString());
                    }

                    if (GV_Team.Rows[i].Cells["Money_Dif_Holiday"].Value.ToString() == "")
                    {
                        _MoneyDifHoliday = 0;
                    }
                    else
                    {
                        _MoneyDifHoliday = double.Parse(GV_Team.Rows[i].Cells["Money_Dif_Holiday"].Value.ToString());
                    }

                    GV_Team.Rows[i].Cells["ToTal"].Value = (_Money_Work + _Money_Rieng + _Money_General + _Money_Private).ToString("#,###,###");
                    GV_Team.Rows[i].Cells["ToTal_Dif"].Value = (_MoneyDif + _MoneyDifSunday + _MoneyDifHoliday).ToString("#,###,###");
                }
            }

            btn_Save_All.Visible = true;
            btn_Count_All.Visible = false;
        }
        private void Save_All()
        {
            int zFind_Date = 0;
            for (int i = 0; i < GV_Team.Rows.Count - 1; i++)
            {
                Order_Money_Date_Info zMoneyDate = new Order_Money_Date_Info();
                zMoneyDate.EmployeeKey = GV_Team.Rows[i].Cells["EmployeeName"].Tag.ToString();
                zMoneyDate.EmployeeID = GV_Team.Rows[i].Cells["CardID"].Value.ToString();
                zMoneyDate.EmployeeName = GV_Team.Rows[i].Cells["EmployeeName"].Value.ToString();
                zMoneyDate.TeamKey = _TeamKey;
                zMoneyDate.Date = dte_Teams.Value;
                zFind_Date = Caculater_Productivity_Data.Find_Date(dte_Teams.Value);
                if (zFind_Date > 0)
                {
                    zMoneyDate.Money = 0;
                    zMoneyDate.Money_Sundays = 0;
                    if (GV_Team.Rows[i].Cells["ToTal"].Value.ToString() != "")
                        zMoneyDate.Money_Holidays = float.Parse(GV_Team.Rows[i].Cells["ToTal"].Value.ToString());
                    else
                        zMoneyDate.Money_Holidays = 0;

                    zMoneyDate.Money_Dif = 0;
                    if (GV_Team.Rows[i].Cells["ToTal_Dif"].Value.ToString() != "")
                        zMoneyDate.Money_Dif_Holiday = float.Parse(GV_Team.Rows[i].Cells["ToTal_Dif"].Value.ToString());
                    else
                        zMoneyDate.Money_Dif_Holiday = 0;
                    zMoneyDate.Money_Dif_Sunday = 0;
                }
                if (zFind_Date == 0 && dte_Teams.Value.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (GV_Team.Rows[i].Cells["ToTal"].Value.ToString() != "")
                        zMoneyDate.Money = float.Parse(GV_Team.Rows[i].Cells["ToTal"].Value.ToString());
                    else
                        zMoneyDate.Money = 0;
                    zMoneyDate.Money_Sundays = 0;
                    zMoneyDate.Money_Holidays = 0;
                    if (GV_Team.Rows[i].Cells["ToTal_Dif"].Value.ToString() != "")
                        zMoneyDate.Money_Dif = float.Parse(GV_Team.Rows[i].Cells["ToTal_Dif"].Value.ToString());
                    else
                        zMoneyDate.Money_Dif = 0;
                    zMoneyDate.Money_Dif_Holiday = 0;
                    zMoneyDate.Money_Dif_Sunday = 0;
                }
                if (dte_Teams.Value.DayOfWeek == DayOfWeek.Sunday)
                {
                    zMoneyDate.Money = 0;
                    if (GV_Team.Rows[i].Cells["ToTal"].Value.ToString() != "")
                        zMoneyDate.Money_Sundays = float.Parse(GV_Team.Rows[i].Cells["ToTal"].Value.ToString());
                    else
                        zMoneyDate.Money_Sundays = 0;
                    zMoneyDate.Money_Holidays = 0;
                    zMoneyDate.Money_Dif = 0;
                    zMoneyDate.Money_Dif_Holiday = 0;
                    if (GV_Team.Rows[i].Cells["ToTal_Dif"].Value.ToString() != "")
                        zMoneyDate.Money_Dif_Sunday = float.Parse(GV_Team.Rows[i].Cells["ToTal_Dif"].Value.ToString());
                    else
                        zMoneyDate.Money_Dif_Sunday = 0;
                }
                _Count = Order_Money_Date_Data.Find_Employee(zMoneyDate.EmployeeKey, dte_Teams.Value);
                if (_Count == 0)
                {
                    zMoneyDate.Create();
                }
                else
                {
                    zMoneyDate.Update();
                }
                _Message += zMoneyDate.Message;
            }
            string zMessage = TN_Message.Show(_Message);
            if (zMessage.Length == 0)
            {
                MessageBox.Show("Câp nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region [Tamplate]
        private void CountDataGridView()
        {
            #region Tiền Làm Việc Khác Mượn Ăn Chung
            float zTimeDif = 0;
            for (int i = 0; i < GV_Team.Rows.Count - 1; i++)
            {
                if (float.Parse(GV_Team.Rows[i].Cells["Time"].Value.ToString()) != 0)
                {
                    int zGender = Employee_Data.Gender(GV_Team.Rows[i].Cells["EmployeeName"].Tag.ToString());
                    float zTimeOnePerson = float.Parse(GV_Team.Rows[i].Cells["Time"].Value.ToString());
                    zTimeDif = zToTalTimeDifBorrowEat / 8;
                    float zTimeDifTB =zTimeDif / _ToTal_Time_Team;
                    float zTime = (zTimeDifTB * zTimeOnePerson);
                    if (zGender == 1)
                    {
                        GV_Team.Rows[i].Cells["TimeDifBorrowEat"].Value = zTime.ToString("n3");
                        GV_Team.Rows[i].Cells["Money_Dif_General"].Value = (zTime * 116000).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                    }
                    if (zGender == 0)
                    {
                        GV_Team.Rows[i].Cells["TimeDifBorrowEat"].Value = zTime.ToString("n3");
                        GV_Team.Rows[i].Cells["Money_Dif_General"].Value = (zTime * 104000).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                    }
                }
            }
            #endregion

            #region Tiền Làm Việc Tổ Khác (Ăn Chung)
            float zTimeTeamEat = 0;
            for (int i = 0; i < GV_Team.Rows.Count - 1; i++)
            {
                if (float.Parse(GV_Team.Rows[i].Cells["Time"].Value.ToString()) != 0)
                {
                    int zGender = Employee_Data.Gender(GV_Team.Rows[i].Cells["EmployeeName"].Tag.ToString());
                    float zTimeOnePerson = float.Parse(GV_Team.Rows[i].Cells["Time"].Value.ToString());
                    zTimeTeamEat = _ToTalTimeTeamBorrowEat / 8;
                    float zTimeEatTB = zTimeTeamEat / _ToTal_Time_Team;
                    float zTime = (zTimeTeamEat * zTimeOnePerson);
                    if (zGender == 1)
                    {
                        GV_Team.Rows[i].Cells["TimeBorrowGeneral"].Value = zTime.ToString("n3");
                        GV_Team.Rows[i].Cells["Money_Eat"].Value = (zTime * 116000).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                    }
                    if (zGender == 0)
                    {
                        GV_Team.Rows[i].Cells["TimeBorrowGeneral"].Value = zTime.ToString("n3");
                        GV_Team.Rows[i].Cells["Money_Eat"].Value = (zTime * 104000).ToString(GlobalConfig.String_Format_Currency, GlobalConfig.Provider);
                    }
                }
            }
            #endregion

            #region []
            #endregion
        }
        #endregion



        #region [Process Event]
        private void Btn_Count_All_Click(object sender, EventArgs e)
        {
            CountDataGridView();
        }
        private void Btn_Save_All_Click(object sender, EventArgs e)
        {
            Save_All();
        }
        #endregion

        #endregion

        #endregion

        #region [TỔNG HỢP LƯƠNG NĂNG SUẤT THÁNG]

        #region ----- Panel Left -----

        #region [Desgin Layout ListView]
        private void LV_Team_Month_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


        }

        public void LV_Team_Month_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Teams_Month;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Teams_Data.List();

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region [Process Envent]
        private void LV_Teams_Month_Click(object sender, EventArgs e)
        {
            lbl_Teams.Text = "TÍNH LƯƠNG NĂNG SUẤT THÁNG " + dte_Month.Value.ToString("MM/yyyy");
            for (int i = 0; i < LV_Teams_Month.Items.Count; i++)
            {
                if (LV_Teams_Month.Items[i].Selected == true)
                {
                    LV_Teams_Month.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_Teams_Month.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _TeamKey = LV_Teams_Month.SelectedItems[0].Tag.ToInt();
            GV_Team_Month_LoadData();
            btn_Count_Month.Visible = true;

            LB_Log.Items.Clear();
        }
        #endregion

        #endregion

        #region ----- Panel Right -----

        #region [Design Layout DataGridView]
        private void GV_Team_Month_Layout()
        {
            // Setup Column 
            GV_Employee_Month.Columns.Add("No", "STT");
            GV_Employee_Month.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Employee_Month.Columns.Add("CardID", "Mã NV");
            GV_Employee_Month.Columns.Add("Money", "Lương Ngày Thường");
            GV_Employee_Month.Columns.Add("Money_Holiday", "Lương Ngày Lễ");
            GV_Employee_Month.Columns.Add("Money_Sunday", "Lương Ngày Chủ Nhật");
            GV_Employee_Month.Columns.Add("Money_Dif", "Lương Khác Ngày Thường");
            GV_Employee_Month.Columns.Add("Money_Dif_Sunday", "Lương Khác Ngày Chủ Nhật");
            GV_Employee_Month.Columns.Add("Money_Dif_Holiday", "Lương Việc Ngày Ngày Lễ");
            GV_Employee_Month.Columns.Add("Money_Order", "Lương Khác");
            GV_Employee_Month.Columns.Add("Money_TimeKeep", "Lương Ngày Công");
            GV_Employee_Month.Columns.Add("ToTal", "Tổng Tiền");

            GV_Employee_Month.Columns["No"].Width = 60;
            GV_Employee_Month.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee_Month.Columns["No"].ReadOnly = true;

            GV_Employee_Month.Columns["EmployeeName"].Width = 170;
            GV_Employee_Month.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee_Month.Columns["EmployeeName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Employee_Month.Columns["EmployeeName"].ReadOnly = true;

            GV_Employee_Month.Columns["CardID"].Width = 70;
            GV_Employee_Month.Columns["CardID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee_Month.Columns["CardID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            GV_Employee_Month.Columns["CardID"].ReadOnly = true;
            GV_Employee_Month.Columns["CardID"].Frozen = true;

            GV_Employee_Month.Columns["Money"].Width = 95;
            GV_Employee_Month.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee_Month.Columns["Money"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Employee_Month.Columns["Money_Holiday"].Width = 95;
            GV_Employee_Month.Columns["Money_Holiday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee_Month.Columns["Money_Holiday"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Employee_Month.Columns["Money_Sunday"].Width = 150;
            GV_Employee_Month.Columns["Money_Sunday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee_Month.Columns["Money_Dif"].Width = 130;
            GV_Employee_Month.Columns["Money_Dif"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee_Month.Columns["Money_Dif_Sunday"].Width = 130;
            GV_Employee_Month.Columns["Money_Dif_Sunday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee_Month.Columns["Money_Dif_Holiday"].Width = 130;
            GV_Employee_Month.Columns["Money_Dif_Holiday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee_Month.Columns["Money_Order"].Width = 130;
            GV_Employee_Month.Columns["Money_Order"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee_Month.Columns["Money_TimeKeep"].Width = 130;
            GV_Employee_Month.Columns["Money_TimeKeep"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee_Month.Columns["ToTal"].Width = 130;
            GV_Employee_Month.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Employee_Month.Columns["ToTal"].ReadOnly = true;
            // setup style view

            GV_Employee_Month.BackgroundColor = Color.White;
            GV_Employee_Month.GridColor = Color.FromArgb(227, 239, 255);
            GV_Employee_Month.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Employee_Month.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Employee_Month.DefaultCellStyle.Font = new Font("Arial", 9);
            GV_Employee_Month.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 90, 200);
            GV_Employee_Month.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(206, 246, 255);
            GV_Employee_Month.EnableHeadersVisualStyles = false;
            GV_Employee_Month.ColumnHeadersHeight = 40;

            GV_Employee_Month.AllowUserToResizeRows = false;
            GV_Employee_Month.AllowUserToResizeColumns = true;

            GV_Employee_Month.RowHeadersVisible = false;

            //// setup Height Header
            // GV_Employees.ColumnHeadersHeight = GV_Employees.ColumnHeadersHeight * 3 / 2;
            GV_Employee_Month.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Employee_Month.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            foreach (DataGridViewColumn column in GV_Employee_Month.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.NotSortable;
                column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }

            for (int i = 1; i <= 8; i++)
            {
                GV_Employee_Month.Rows.Add();
            }
        }
        private void GV_Team_Month_LoadData()
        {
            GV_Employee_Month.Rows.Clear();
            _Table = Order_Money_Data.List_Team_Month(_TeamKey, dte_Month.Value);

            int i = 0;
            foreach (DataRow nRow in _Table.Rows)
            {
                GV_Employee_Month.Rows.Add();
                DataGridViewRow nRowView = GV_Employee_Month.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeName"].Tag = nRow["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["CardID"].Value = nRow["EmployeeID"].ToString().Trim();
                float zMoney = float.Parse(nRow["Money"].ToString());
                nRowView.Cells["Money"].Value = zMoney.ToString("#,###,###");
                float zMoneyholiday = float.Parse(nRow["MoneyHoliday"].ToString());
                nRowView.Cells["Money_Holiday"].Value = zMoneyholiday.ToString("#,###,###");
                float zMoneSyunday = float.Parse(nRow["MoneySunday"].ToString().Trim());
                nRowView.Cells["MOney_Sunday"].Value = zMoneSyunday.ToString("#,###,###");
                float zMoneyDif = float.Parse(nRow["MoneyDif"].ToString());
                nRowView.Cells["Money_Dif"].Value = zMoneyDif.ToString("#,###,###");
                float zMoneyDifHoliday = float.Parse(nRow["MoneyDifSunday"].ToString());
                nRowView.Cells["Money_Dif_Sunday"].Value = zMoneyDifHoliday.ToString("#,###,###");
                float zMoneyDifSunday = float.Parse(nRow["MoneyDifHoliday"].ToString());
                nRowView.Cells["Money_Dif_Holiday"].Value = zMoneyDifSunday.ToString("#,###,###");
                float zMoneyTimeKeep = float.Parse(nRow["MoneyTimeKeep"].ToString());
                nRowView.Cells["Money_TimeKeep"].Value = zMoneyTimeKeep.ToString("#,###,###");
                i++;
            }

        }
        #endregion

        #region [Process Data]
        private void Message_System(string Message)
        {
            Invoke(new MethodInvoker(delegate
            {
                LB_Log.Items.Add(DateTime.Now.ToString("dd-MM-yy hh:mm:ss") + " : " + Message);
            }));
        }
        private void Count_Month()
        {
            float zMoney = 0;
            float zMoneyHoliday = 0;
            float zMoneySunday = 0;
            float zMoneyDif = 0;
            float zMoneyDifHoliday = 0;
            float zMoneyDifSunday = 0;
            float zMoney_ToTal = 0;
            if (GV_Employee_Month.Rows[_indexttable].Cells["CardID"].Value != null)
            {
                if (GV_Employee_Month.Rows[_indexttable].Cells["Money"].Value.ToString() != "")
                    zMoney = float.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money"].Value.ToString());
                else
                    zMoney = 0;

                if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString() != "")
                    zMoneyHoliday = float.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString());
                else
                    zMoneyHoliday = 0;

                if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString() != "")
                    zMoneySunday = float.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString());
                else
                    zMoneySunday = 0;

                if (GV_Employee_Month.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString() != "")
                    zMoneyDif = float.Parse(GV_Employee_Month.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString());
                else
                    zMoneyDif = 0;

                if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString() != "")
                    zMoneyDifHoliday = float.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString());
                else
                    zMoneyDifSunday = 0;

                if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString() != "")
                    zMoneyDifSunday = float.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString());
                else
                    zMoneyDifSunday = 0;

                float zToTal = zMoney + zMoneyHoliday + zMoneySunday + zMoneyDif + zMoneyDifHoliday + zMoneyDifSunday;
                if (zToTal > 0)
                    GV_Employee_Month.Rows[_indexttable].Cells["Total"].Value = zToTal.ToString("#,###,###");
                else
                    GV_Employee_Month.Rows[_indexttable].Cells["Total"].Value = "0";
                if (GV_Employee_Month.Rows[_indexttable].Cells["Total"].Value.ToString() != "")
                {
                    Save();
                    Message_System("Đang xử lý " + GV_Employee_Month.Rows[_indexttable].Cells["EmployeeName"].Value.ToString());
                    if (_Message == "11" || _Message == "20")
                    {
                        Message_System("Đã chuyển thành công");
                        Message_System("--------------------");
                        timer1.Start();
                    }
                    else
                    {
                        timer1.Stop();
                    }
                }
            }
            if (GV_Employee_Month.Rows[_indexttable].Cells["CardID"].Value == null)
            {
                MessageBox.Show("Đã xong");
                timer1.Stop();
                for (int i = 0; i < GV_Employee_Month.Rows.Count; i++)
                {
                    if (GV_Employee_Month.Rows[i].Cells["CardID"].Value != null)
                    {
                        float zMoneyToTal = float.Parse(GV_Employee_Month.Rows[i].Cells["Total"].Value.ToString());
                        zMoney_ToTal += zMoneyToTal;
                    }
                }
                txt_Money_Month.Text = zMoney_ToTal.ToString("#,###,###");
            }

        }

        private void Save()
        {
            DateTime zDate = dte_Month.Value;
            DateTime zFromDate = new DateTime(zDate.Year, zDate.Month, 1);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day);

            zSalary = new Salary_Product_Worker_Info();
            zSalary.SalaryMonth = zToDate;
            zSalary.EmployeeKey = GV_Employee_Month.Rows[_indexttable].Cells["EmployeeName"].Tag.ToString();
            zSalary.EmployeeID = GV_Employee_Month.Rows[_indexttable].Cells["CardID"].Value.ToString();
            zSalary.EmployeeName = GV_Employee_Month.Rows[_indexttable].Cells["EmployeeName"].Value.ToString();
            if (GV_Employee_Month.Rows[_indexttable].Cells["Money"].Value.ToString() != "")
                zSalary.SalaryProduct = double.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money"].Value.ToString());
            else
                zSalary.SalaryProduct = 0;
            if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString() != "")
                zSalary.SalarySunday = double.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Sunday"].Value.ToString());
            else
                zSalary.SalarySunday = 0;
            if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString() != "")
                zSalary.SalaryHoliday = double.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Holiday"].Value.ToString());
            else
                zSalary.SalaryHoliday = 0;
            if (GV_Employee_Month.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString() != "")
                zSalary.SalaryDif = double.Parse(GV_Employee_Month.Rows[_indexttable].Cells["MOney_Dif"].Value.ToString());
            else
                zSalary.SalaryDif = 0;
            if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString() != "")
                zSalary.SalaryDifSunday = double.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Sunday"].Value.ToString());
            else
                zSalary.SalaryDifSunday = 0;
            if (GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString() != "")
                zSalary.SalaryDifHoliday = double.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_Dif_Holiday"].Value.ToString());
            else
                zSalary.SalaryDifHoliday = 0;
            if (GV_Employee_Month.Rows[_indexttable].Cells["Money_TimeKeep"].Value.ToString() != "")
                zSalary.MoneyTimeKeep = double.Parse(GV_Employee_Month.Rows[_indexttable].Cells["Money_TimeKeep"].Value.ToString());
            else
                zSalary.MoneyTimeKeep = 0;
            int zCount = Salary_Product_Worker_Data.Find_Employee(zSalary.EmployeeKey, dte_Month.Value);
            if (zCount == 0)
            {
                zSalary.CreatedBy = SessionUser.UserLogin.Key;
                zSalary.CreatedName = SessionUser.Personal_Info.FullName;
                zSalary.Create();
                _Message = zSalary.Message;
            }
            if (zCount > 0)
            {
                zSalary.Update();
                _Message = zSalary.Message;
            }
        }

        #endregion

        #region [Process Event]
        private int _indexttable = 0;
        private void Btn_Count_Month_Click(object sender, EventArgs e)
        {
            timer1.Start();
            _indexttable = -1;
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            _indexttable++;
            Count_Month();
        }

        #endregion

        #endregion

        #endregion
    }
}
