﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Rpt_44 : Form
    {
        public Frm_Rpt_44()
        {
            InitializeComponent();
            this.Load += Frm_Rpt_44_Load;
            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            cbo_Team.SelectedIndexChanged += Cbo_Team_SelectedIndexChanged;
        }

        private void Cbo_Team_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DataTable zTable = Employee_Data.ListNames(cbo_Team.SelectedValue.ToInt());
            //InitDataMaster(zTable);
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void Frm_Rpt_44_Load(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Click += GVData_SelChange;

            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LoadDataToToolbox.KryptonComboBox(cbo_Team, @"
SELECT TeamKey,TeamID +'-'+ TeamName AS TeamName  FROM SYS_Team 
WHERE RecordStatus <> 99 AND BranchKey=4 AND  DepartmentKey != 26
ORDER BY Rank", "--Tất cả--");
        }

        private void GVData_SelChange(object sender, EventArgs e)
        {
            if (GVData.RowSel > 0)
            {
                GVEmployee.Rows.Count = 0;
                GVEmployee.Cols.Count = 0;
                GVEmployee.Clear();

                kryptonHeader1.Text = "Thông tin chi tiết lương, năng suất công nhân thực hiện công việc của [Họ tên: " + GVData.Rows[GVData.RowSel][1].ToString() + ", Mã số: " + GVData.Rows[GVData.RowSel][2].ToString() + "]";

                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
        }

        private void DisplayData()
        {
            string ID = GVData.Rows[GVData.RowSel][2].ToString();
            DataTable zTable = Report.Report_44(dte_FromDate.Value, dte_ToDate.Value, ID);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable TableView = zPvt.Generate("CongViec", "NgayLamViec", new string[] { "LK", "TG", "SLTP", "NS" }, "NGÀY CÔNG", "TỔNG", "TỔNG", new string[] { "LƯƠNG KHOÁN", "THỜI GIAN", "SỐ LƯỢNG THÀNH PHẨM", "NĂNG SUẤT (SL/KG)" }, 0);

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataDetail(TableView);
                }));
            }
        }

        private void DisplayDataEmployee()
        {
            string ID = txt_ID.Text.Trim();
            Employee_Info zEmp = new Employee_Info();
            zEmp.GetEmployeeID(ID);

            kryptonHeader1.Text = "Thông tin chi tiết lương, năng suất công nhân thực hiện công việc của [Họ tên: " + zEmp.FullName + ", Mã số: " + ID + "]";

            DataTable zTable = Report.Report_44(dte_FromDate.Value, dte_ToDate.Value, ID);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable TableView = zPvt.Generate("CongViec", "NgayLamViec", new string[] { "LK", "TG", "SLTP", "NS" }, "NGÀY CÔNG", "TỔNG", "TỔNG", new string[] { "LƯƠNG KHOÁN", "THỜI GIAN", "SỐ LƯỢNG THÀNH PHẨM", "NĂNG SUẤT (SL/KG)" }, 0);

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitDataDetail(TableView);
                }));
            }
        }

        void InitDataMaster(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ TÊN";
            GVData.Rows[0][2] = "MÃ THẺ";

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData["FullName"];
                GVData.Rows[rIndex + 1][2] = rData["EmployeeID"];
            }

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 50;
            GVData.Cols[1].Width = 250;
            GVData.Cols[2].Width = 100;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Fixed = 1;

            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

        }
        void InitDataDetail(DataTable TableView)
        {
            GVEmployee.Rows.Count = 0;
            GVEmployee.Cols.Count = 0;
            GVEmployee.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVEmployee.Cols.Add(ToTalCol);
            GVEmployee.Rows.Add(TotalRow);

            CellRange zRange = GVEmployee.GetCellRange(0, 0, 1, 0);
            zRange.Data = "STT";

            zRange = GVEmployee.GetCellRange(0, 1, 1, 1);
            zRange.Data = "NGÀY";

            //Row Header
            int ColStart = 2;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');

                GVEmployee.Rows[0][ColStart] = strCaption[0];           //Ten Nhom
                GVEmployee.Rows[1][ColStart] = strCaption[1];           //SL - LK                  

                ColStart++;
            }

            ColStart = 1;
            int RowStart = 2;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[0].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][0] = strTemp[0];
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    //GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                }
                else
                {
                    GVEmployee.Rows[rIndex + RowStart][0] = rIndex + 1;
                    GVEmployee.Rows[rIndex + RowStart][1] = rData[0];
                }

                for (int cIndex = 1; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVEmployee.Rows[RowStart + rIndex][ColStart + cIndex] = FormatMoney(rData[cIndex]);
                }
            }

            //Style         
            GVEmployee.ExtendLastCol = false;
            GVEmployee.AllowResizing = AllowResizingEnum.Both;
            GVEmployee.AllowMerging = AllowMergingEnum.FixedOnly;
            GVEmployee.SelectionMode = SelectionModeEnum.Row;
            GVEmployee.VisualStyle = VisualStyle.Office2010Blue;
            GVEmployee.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVEmployee.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVEmployee.Styles.Normal.WordWrap = true;

            GVEmployee.Rows.Fixed = 2;
            GVEmployee.Cols.Fixed = 2;

            GVEmployee.Rows[0].Height = 40;
            GVEmployee.Rows[0].AllowMerging = true;
            GVEmployee.Rows[1].Height = 40;
            GVEmployee.Cols[0].Width = 50;
            GVEmployee.Cols[1].Width = 100;

            GVEmployee.Cols[0].AllowMerging = true;
            GVEmployee.Cols[1].AllowMerging = true;

            GVEmployee.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVEmployee.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 1; i < GVEmployee.Cols.Count; i++)
            {
                GVEmployee.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
        }
        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            DataTable zTable = new DataTable();
            if (txt_ID.Text.Trim().Length > 0)
            {
                zTable = Employee_Data.Search(txt_ID.Text.Trim());
                InitDataMaster(zTable);

                GVEmployee.Rows.Count = 0;
                GVEmployee.Cols.Count = 0;
                GVEmployee.Clear();

                using (Frm_Loading frm = new Frm_Loading(DisplayDataEmployee)) { frm.ShowDialog(this); }
                txt_ID.Clear();
                return;
            }
            else
            {
                zTable = Employee_Data.ListNamesV3(cbo_Team.SelectedValue.ToInt(),dte_FromDate.Value,dte_ToDate.Value);
                InitDataMaster(zTable);
            }
        }
    }
}
