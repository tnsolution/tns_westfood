﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;
using Excel1 = Microsoft.Office.Interop.Excel;

namespace TNS.WinApp
{
    public partial class Frm_Search_Order : Form
    {
        #region [Field Name]
        private string _OrderID = "";
        private string _ProductKey = "";
        private string _ProductName = "";
        private string _ProductID = "";
        private float _Quantity_Document = 0;
        private int _Coupon = 0;
        private int _UnitKey = 0;
        private string _UnitName = "";
        private int _Autokey = 0;
        private int _Slug = 0;
        #endregion

        #region  [Properties]
        public string OrderID
        {
            get
            {
                return _OrderID;
            }

            set
            {
                _OrderID = value;
            }
        }

        public string ProductKey
        {
            get
            {
                return _ProductKey;
            }

            set
            {
                _ProductKey = value;
            }
        }

        public string ProductName
        {
            get
            {
                return _ProductName;
            }

            set
            {
                _ProductName = value;
            }
        }

        public string ProductID
        {
            get
            {
                return _ProductID;
            }

            set
            {
                _ProductID = value;
            }
        }

        public float Quantity_Document
        {
            get
            {
                return _Quantity_Document;
            }

            set
            {
                _Quantity_Document = value;
            }
        }

        public int Coupon
        {
            get
            {
                return _Coupon;
            }

            set
            {
                _Coupon = value;
            }
        }

        public int UnitKey
        {
            get
            {
                return _UnitKey;
            }

            set
            {
                _UnitKey = value;
            }
        }

        public string UnitName
        {
            get
            {
                return _UnitName;
            }

            set
            {
                _UnitName = value;
            }
        }

        public int Autokey
        {
            get
            {
                return _Autokey;
            }

            set
            {
                _Autokey = value;
            }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }
        #endregion

        public Frm_Search_Order()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DrawLVStyle(ref LVData);
            //----- ListView 
            LV_Order(LVData);
            LVData.ItemActivate += LVData_ItemActivate;
            //----- Button
            btn_Search.Click += Btn_Search_Click;
        }

        private void Frm_Choose_ID_Load(object sender, EventArgs e)
        {
            cbo_Coupon.SelectedIndex = 0;
            Picker_From.Value = SessionUser.Date_Work;
            Picker_To.Value = SessionUser.Date_Work;
            LoadDataWareHouse();
        }

        private void LoadDataWareHouse()
        {
            LoadDataToToolbox.ComboBoxData(cbo_Warehouse, Warehouse_Data.List(), false, 0, 1);
        }

        #region ----- Desgin Layout -----
        private void LV_Order(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Ngày";
            colHead.Width = 80;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Tên Sản Phẩm";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã Sản Phẩm";
            colHead.Width = 120;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn vị";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Lượng";
            colHead.Width = 90;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Chứng Từ";
            colHead.Width = 110;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Kho";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Right;
            LV.Columns.Add(colHead);
        }

        public void LV_OrderLoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;


            DataTable In_Table = new DataTable();
            if ((int)cbo_Coupon.SelectedIndex == 0)
            {
                Coupon = 0;
                In_Table = Stock_Data.List_Input(txt_Search.Text, (int)cbo_Warehouse.SelectedValue, Picker_From.Value, Picker_To.Value);

            }
            else if ((int)cbo_Coupon.SelectedIndex == 1)
            {
                Coupon = 1;
                In_Table = Stock_Data.List_Output(txt_Search.Text, (int)cbo_Warehouse.SelectedValue, Picker_From.Value, Picker_To.Value);

            }

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["ProductKey"].ToString();
                lvi.BackColor = Color.White;

                DateTime z_trdate = (DateTime)nRow["OrderDate"];
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = z_trdate.ToString("dd/MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["ProductID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["UnitName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Quantity"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["WarseHouseName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["UnitKey"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["AutoKey"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                if (nRow["Slug"].ToString().Trim() == "1")
                {
                    lvi.BackColor = Color.FromArgb(173, 199, 231);
                }
                if (nRow["Slug"].ToString().Trim() == "0")
                {
                    lvi.BackColor = Color.White;
                }
                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }
        #endregion

        #region ----- Process Event -----
        private void LVData_ItemActivate(object sender, EventArgs e)
        {
            _OrderID = LVData.SelectedItems[0].SubItems[6].Text;
            _ProductKey = LVData.SelectedItems[0].Tag.ToString();
            _ProductName = LVData.SelectedItems[0].SubItems[2].Text;
            _ProductID = LVData.SelectedItems[0].SubItems[3].Text;
            _Quantity_Document = float.Parse(LVData.SelectedItems[0].SubItems[5].Text);
            _UnitName = LVData.SelectedItems[0].SubItems[4].Text;
            _UnitKey = LVData.SelectedItems[0].SubItems[8].Text.ToInt();
            _Autokey = int.Parse(LVData.SelectedItems[0].SubItems[9].Text);
            _Slug = cbo_Coupon.SelectedIndex;
            this.Close();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LV_OrderLoadData();
        }

        #endregion

        private void btn_Export_Click(object sender, EventArgs e)
        {
            SaveFileDialog fsave = new SaveFileDialog();
            fsave.Filter = "(Tất cả các tệp)|*.*|(Các tệp Excel)|*.xlsx";
            fsave.ShowDialog();
            if (fsave.FileName != "")
            {
                Excel1.Application app = new Excel1.Application();
                Excel1.Workbook wb = app.Workbooks.Add(Type.Missing);
                Excel1.Worksheet sheet = null;
                try
                {
                    sheet = wb.ActiveSheet;
                    for (int i = 1; i <= LVData.Columns.Count; i++)
                    {
                        sheet.Cells[1, i] = LVData.Columns[i - 1].Text;
                    }
                    for (int i = 1; i <= LVData.Items.Count; i++)
                    {
                        ListViewItem item = LVData.Items[i - 1];
                        sheet.Cells[i + 1, 1] = item.Text;
                        for (int j = 2; j <= LVData.Columns.Count; j++)
                        {
                            sheet.Cells[i + 1, j] = item.SubItems[j - 1].Text;
                        }
                    }
                    wb.SaveAs(fsave.FileName);
                    MessageBox.Show("Export file Excel Success ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                }
                finally
                {
                    app.Quit();
                    wb = null;
                }
            }
            else
            {
                MessageBox.Show("Bạn không chọn tệp tin nào", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
