﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;
using TNS.SYS;

namespace TNS.IVT
{
    public class Stock_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 ";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInput_NotApprove()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 AND RecordStatus != 43 ";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInput_WaitingApprove()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 AND RecordStatus = 41 ";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        //---- List Input
        //#region ---- List Input Local ----
        //public static DataTable ListInput_NotApprove(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 ";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey AND WarehouseLocal > 0 ";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //public static DataTable ListInput_WaitingApprove(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 AND RecordStatus = 41 ";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //#endregion

        //#region ---- List Input Customer ----
        //public static DataTable ListInput_NotApprove_Customer(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 ";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //public static DataTable ListInput_Approve_Customer(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  TOP 20 * FROM IVT_Stock_Input WHERE RecordStatus = 41";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID DESC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //#endregion

        #region[List Input Customer Search ]
        public static DataTable ListInput_NotApprove_Customer_Search(int WarehouseKey, string Search,DateTime FromDate,DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();

            string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 AND  OrderDate BETWEEN  @FromDate AND @ToDate ";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInput_Approve_Customer_Search(int WarehouseKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus = 41 AND  OrderDate BETWEEN  @FromDate AND @ToDate";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[List Input Local Search]
        public static DataTable ListInput_NotApprove_Search(int WarehouseKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM IVT_Stock_Input WHERE RecordStatus < 99 AND OrderDate BETWEEN  @FromDate AND @ToDate ";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND WarehouseLocal > 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListInput_WaitingApprove_Search(int WarehouseKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Input WHERE RecordStatus < 99 AND RecordStatus = 41 AND  OrderDate BETWEEN  @FromDate AND @ToDate ";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND WarehouseLocal > 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        //---- List Output
        //#region ---- List Output Local ----
        //public static DataTable ListOutput_NotApprove(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE RecordStatus < 99  ";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey AND WarehouseLocal > 0";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //public static DataTable ListOutput_WaitingApprove(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE RecordStatus < 99 AND RecordStatus = 41 ";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey AND WarehouseLocal > 0";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //#endregion

        //#region ---- List Output Customer ----
        //public static DataTable ListOutput_NotApprove_Customer(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE RecordStatus < 99 ";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //public static DataTable ListOutput_Approve_Customer(int WarehouseKey)
        //{
        //    DataTable zTable = new DataTable();
        //    string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE  RecordStatus = 41";
        //    if (WarehouseKey > 0)
        //        zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0";
        //    zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zAdapter.Fill(zTable);
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zstrMessage = ex.ToString();
        //    }
        //    return zTable;
        //}
        //#endregion

        #region[List Output Customer Search ]
        public static DataTable ListOuput_NotApprove_Customer_Search(int WarehouseKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();

            string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE RecordStatus < 99 AND  OrderDate BETWEEN  @FromDate AND @ToDate  ";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListOutput_Approve_Customer_Search(int WarehouseKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE RecordStatus = 41 AND  OrderDate BETWEEN  @FromDate AND @ToDate ";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND  WarehouseLocal = 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        #region[List Output Local search]
        public static DataTable ListOutput_NotApprove_Search(int WarehouseKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE RecordStatus < 99 AND  OrderDate BETWEEN  @FromDate AND @ToDate ";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND WarehouseLocal > 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListOutput_WaitingApprove_Search(int WarehouseKey, string Search, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = "SELECT  * FROM IVT_Stock_Output WHERE RecordStatus < 99 AND RecordStatus = 41 AND  OrderDate BETWEEN  @FromDate AND @ToDate ";
            if (WarehouseKey > 0)
                zSQL += " AND WarehouseKey = @WarehouseKey AND WarehouseLocal > 0 AND OrderID LIKE @Search";
            zSQL += " ORDER BY OrderDate DESC  , OrderID ASC";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@Search", SqlDbType.NVarChar).Value = "%" + Search + "%";
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion
        //-----------------------------------------


        public static DataTable ListOrderInput(int WarehouseKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT OrderID FROM IVT_Stock_Input "
                        + "WHERE WarehouseKey = @WarehouseKey "
                        + "AND RecordStatus = 43";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListOrderOutput(int WarehouseKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT OrderID FROM IVT_Stock_Output "
                        + "WHERE WarehouseKey = @WarehouseKey "
                        + "AND RecordStatus<99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static float SumInput(int WarehouseKey, string OrderKey, string ProductKey)
        {
            string zResult = Data_Access.GetValue(@"
SELECT 
ISNULL(SUM(A.QuantityBasic),0) [Input]
FROM IVT_Stock_Input_Products A
LEFT JOIN IVT_Stock_Input B ON A.OrderKey = B.OrderKey
WHERE B.WarehouseKey = " + WarehouseKey + " AND A.ProductKey = " + ProductKey + " AND B.OrderKey = '" + OrderKey + "'");
            return float.Parse(zResult);
        }


        #region [Input/Output]
        public static DataTable List_Input(DateTime FromDate, DateTime ToDate, int WareHouseKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ProductKey,B.ProductID,B.ProductName,SUM(ROUND(A.Quantity,1)) AS Input,A.UnitName,D.WarehouseName
FROM dbo.IVT_Stock_Input_Products A
LEFT JOIN dbo.IVT_Products B ON B.ProductKey = A.ProductKey
LEFT JOIN dbo.IVT_Stock_Input C ON C.OrderKey = A.OrderKey
LEFT JOIN  dbo.IVT_Warehouse D ON D.WarehouseKey = C.WarehouseLocal
WHERE C.WarehouseKey = @WareHouseKey AND C.OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY  A.ProductKey,B.ProductID,B.ProductName,A.UnitName,D.WarehouseName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WareHouseKey", SqlDbType.Int).Value = WareHouseKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Output(DateTime FromDate, DateTime ToDate, int WareHouseKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 1);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.ProductKey,B.ProductID,B.ProductName,SUM(ROUND(A.Quantity,1)) AS Output,A.UnitName,D.WarehouseName
FROM dbo.IVT_Stock_Output_Products A
LEFT JOIN dbo.IVT_Products B ON B.ProductKey = A.ProductKey
LEFT JOIN dbo.IVT_Stock_Output C ON C.OrderKey = A.OrderKey
LEFT JOIN  dbo.IVT_Warehouse D ON D.WarehouseKey = C.WarehouseLocal
WHERE C.WarehouseKey = @WareHouseKey AND C.OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY  A.ProductKey,B.ProductID,B.ProductName,A.UnitName,D.WarehouseName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@WareHouseKey", SqlDbType.Int).Value = WareHouseKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        #endregion

        #region [Choose Input/Output]
        public static DataTable List_Input(string ContentSearch, int WarehouseKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey AS AutoKey,A.ProductKey AS ProductKey,A.UnitKey AS UnitKey,
C.ProductName AS ProductName,C.ProductID AS ProductID,A.UnitName AS UnitName ,
ROUND(A.QuantityReality,2) AS Quantity,B.OrderID AS OrderID,
B.OrderDate AS OrderDate,D.WarehouseName AS WarseHouseName,A.Slug AS Slug
FROM[dbo].[IVT_Stock_Input_Products] A
LEFT JOIN[dbo].[IVT_Stock_Input] B ON A.OrderKey = B.OrderKey
LEFT JOIN[dbo].[IVT_Product] C ON A.ProductKey = C.ProductKey
LEFT JOIN [dbo].[IVT_Warehouse] D ON B.WarehouseKey = D.WarehouseKey
WHERE A.RecordStatus < 99 AND B.WarehouseKey = @WarehouseKey ";
            if (ContentSearch.Length > 0)
            {
                zSQL += "AND B.OrderID LIKE @ContentSearch ";
            }
            else
            {
                zSQL += "AND B.OrderDate BETWEEN @FromDate AND @ToDate ORDER BY B.OrderDate DESC";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable List_Output(string ContentSearch, int WarehouseKey, DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 1);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey AS AutoKey,A.ProductKey AS ProductKey,A.UnitKey AS UnitKey,
C.ProductName AS ProductName,C.ProductID AS ProductID,A.UnitName AS UnitName ,
ROUND(A.QuantityReality,2) AS Quantity,B.OrderID AS OrderID,
B.OrderDate AS OrderDate,D.WarehouseName AS WarseHouseName,A.Slug AS Slug
FROM[dbo].[IVT_Stock_Output_Products] A
LEFT JOIN[dbo].[IVT_Stock_Output] B ON A.OrderKey = B.OrderKey
LEFT JOIN[dbo].[IVT_Product] C ON A.ProductKey = C.ProductKey
LEFT JOIN [dbo].[IVT_Warehouse] D ON B.WarehouseKey = D.WarehouseKey
WHERE A.RecordStatus < 99 AND B.WarehouseKey = @WarehouseKey ";
            if (ContentSearch.Length > 0)
            {
                zSQL += "AND B.OrderID LIKE @ContentSearch ";
            }
            else
            {
                zSQL += "AND B.OrderDate BETWEEN @FromDate AND @ToDate ORDER BY B.OrderDate DESC";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@WarehouseKey", SqlDbType.Int).Value = WarehouseKey;
                zCommand.Parameters.Add("@ContentSearch", SqlDbType.NVarChar).Value = "%" + @ContentSearch + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        #endregion

        public static int FindOrdderID_Input(string OrderID)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) FROM IVT_Stock_Input WHERE OrderID = @OrderID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        public static int FindOrdderID_Output(string OrderID)
        {
            int zResult = 0;
            string zSQL = @"SELECT COUNT(*) FROM IVT_Stock_Output WHERE OrderID = @OrderID AND RecordStatus < 99";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderID", SqlDbType.NVarChar).Value = OrderID;
                zResult = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
    }
}
