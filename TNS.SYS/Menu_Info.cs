﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SYS
{
    public class Menu_Info
    {

        #region [ Field Name ]
        private string _MenuKey = "";
        private string _Module = "";
        private string _MenuName = "";
        private string _MenuLink = "";
        private string _ParamName = "";
        private string _ParamValue = "";
        private string _Icon = "";
        private int _MenuLevel = 0;
        private string _Parent = "";
        private int _Type = 0;
        private int _Rank = 0;
        private int _Ref = 0;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedName = "";
        private string _ModifiedBy = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public bool HasInfo
        {
            get
            {
                if (_MenuKey.Trim().Length > 0) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public string Key
        {
            get { return _MenuKey; }
            set { _MenuKey = value; }
        }
        public string Module
        {
            get { return _Module; }
            set { _Module = value; }
        }
        public string MenuName
        {
            get { return _MenuName; }
            set { _MenuName = value; }
        }
        public string MenuLink
        {
            get { return _MenuLink; }
            set { _MenuLink = value; }
        }
        public string ParamName
        {
            get { return _ParamName; }
            set { _ParamName = value; }
        }
        public string ParamValue
        {
            get { return _ParamValue; }
            set { _ParamValue = value; }
        }
        public string Icon
        {
            get { return _Icon; }
            set { _Icon = value; }
        }
        public int MenuLevel
        {
            get { return _MenuLevel; }
            set { _MenuLevel = value; }
        }
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }
        public int Type
        {
            get { return _Type; }
            set { _Type = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public int Ref
        {
            get { return _Ref; }
            set { _Ref = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Menu_Info()
        {
        }
        public Menu_Info(string MenuKey)
        {
            string zSQL = "SELECT * FROM SYS_Menu WHERE MenuKey = @MenuKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(MenuKey);
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    _MenuKey = zReader["MenuKey"].ToString();
                    _Module = zReader["Module"].ToString();
                    _MenuName = zReader["MenuName"].ToString();
                    _MenuLink = zReader["MenuLink"].ToString();
                    _ParamName = zReader["ParamName"].ToString();
                    _ParamValue = zReader["ParamValue"].ToString();
                    _Icon = zReader["Icon"].ToString();
                    if (zReader["MenuLevel"] != DBNull.Value)
                        _MenuLevel = int.Parse(zReader["MenuLevel"].ToString());
                    _Parent = zReader["Parent"].ToString();
                    if (zReader["Type"] != DBNull.Value)
                        _Type = int.Parse(zReader["Type"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["Ref"] != DBNull.Value)
                        _Ref = int.Parse(zReader["Ref"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _CreatedBy = zReader["CreatedBy"].ToString();
                    _CreatedName = zReader["CreatedName"].ToString();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedName = zReader["ModifiedName"].ToString();
                    _ModifiedBy = zReader["ModifiedBy"].ToString();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Menu_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = _Module;
                zCommand.Parameters.Add("@MenuName", SqlDbType.NVarChar).Value = _MenuName;
                zCommand.Parameters.Add("@MenuLink", SqlDbType.NVarChar).Value = _MenuLink;
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = _ParamName;
                zCommand.Parameters.Add("@ParamValue", SqlDbType.NVarChar).Value = _ParamValue;
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = _Icon;
                zCommand.Parameters.Add("@MenuLevel", SqlDbType.Int).Value = _MenuLevel;
                if (_Parent.Length > 0)
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_Parent);
                else
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ref", SqlDbType.Int).Value = _Ref;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy;
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                if (_Message.Substring(0, 2) == "11")
                    _MenuKey = _Message.Substring(2, 36);
                else
                    _MenuKey = "";
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "SYS_Menu_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                if (_MenuKey.Length == 36)
                    zCommand.Parameters.Add("@MenuKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_MenuKey);
                else
                    zCommand.Parameters.Add("@MenuKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@Module", SqlDbType.NVarChar).Value = _Module;
                zCommand.Parameters.Add("@MenuName", SqlDbType.NVarChar).Value = _MenuName;
                zCommand.Parameters.Add("@MenuLink", SqlDbType.NVarChar).Value = _MenuLink;
                zCommand.Parameters.Add("@ParamName", SqlDbType.NVarChar).Value = _ParamName;
                zCommand.Parameters.Add("@ParamValue", SqlDbType.NVarChar).Value = _ParamValue;
                zCommand.Parameters.Add("@Icon", SqlDbType.NVarChar).Value = _Icon;
                zCommand.Parameters.Add("@MenuLevel", SqlDbType.Int).Value = _MenuLevel;
                if (_Parent.Length == 36)
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_Parent);
                else
                    zCommand.Parameters.Add("@Parent", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@Type", SqlDbType.Int).Value = _Type;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ref", SqlDbType.Int).Value = _Ref;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "SYS_Menu_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@MenuKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_MenuKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_MenuKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
