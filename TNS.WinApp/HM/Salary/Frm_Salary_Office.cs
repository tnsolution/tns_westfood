﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TNS.WinApp
{
    public partial class Frm_Salary_Office : Form
    {
        public Frm_Salary_Office()
        {
            InitializeComponent();
            GV_Product_Layout();
        }

        private void GV_Product_Layout()
        {
            DataGridViewButtonColumn btn;
            // Setup Column 
            GV_Product.Columns.Add("No", "STT");
            GV_Product.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Product.Columns.Add("EmployeeID", "Mã Nhân Viên");
            GV_Product.Columns.Add("DepartmentName", "Tên Bộ Phận");
            GV_Product.Columns.Add("Position", "Chức Vụ");
            GV_Product.Columns.Add("ATM", "ATM");
            GV_Product.Columns.Add("SalaryBasic", "Lương Cơ Bản");
            GV_Product.Columns.Add("SalaryContract", "Lương Hợp Đồng");
            GV_Product.Columns.Add("KPI", "KPI");
            GV_Product.Columns.Add("Support_Effective", "Hiệu Quả Công Việc");
            GV_Product.Columns.Add("Support_Gas", "Hỗ Trợ Xăng ");
            GV_Product.Columns.Add("Support_Effective_Real", "Hỗ Trợ Hiệu Quả Công Việc");
            GV_Product.Columns.Add("Total_SalaryMain_Effective", "Tổng Lương Công Việc");
            GV_Product.Columns.Add("WorkingDate_Caculator", "Tính Ngày Làm Việc");
            GV_Product.Columns.Add("WorkingDate_Real", "Ngày Làm Việc Thực");
            ///-----
            GV_Product.Columns.Add("DayOff", "Ngày Nghĩ");
            GV_Product.Columns.Add("DayOff_Money", "Tiền Ngày");
            GV_Product.Columns.Add("DayOff_Holiday", "Ngày Lễ");
            GV_Product.Columns.Add("DayOff_Holiday_Money", "Tiền Ngày Lễ");
            GV_Product.Columns.Add("Salary_WorkingDate", "Mức Lương Làm Việc");
            GV_Product.Columns.Add("Day_Salary_Different", "Ngày Lương Khác Nhau");
            GV_Product.Columns.Add("Working_Different", "Làm Việc Khác Nhau");
            GV_Product.Columns.Add("Shipment", "Lô Hàng");
            GV_Product.Columns.Add("Total_Salary", "Tổng Số Tiền Lương");
            GV_Product.Columns.Add("Children_Number", "Số Trẻ Em");
            GV_Product.Columns.Add("Children_Money", "Tiền Trẻ Em");
            GV_Product.Columns.Add("Rice_Number_16", "Gạo Số 16");
            GV_Product.Columns.Add("Rice_Money_16", "Tiền Gạo 16");
            GV_Product.Columns.Add("Rice_Number_20", "Gạo Số 20");
            GV_Product.Columns.Add("Rice_Money_20", "Tiền Gạo 20");
            GV_Product.Columns.Add("BHXH", "BHXH");
            GV_Product.Columns.Add("Federation", "Liên Đoàn");
            GV_Product.Columns.Add("Re_Shipment", "Gửi Lại");
            GV_Product.Columns.Add("Re_Salary", "Lương Lại");
            GV_Product.Columns.Add("Tax", "Tax");
            GV_Product.Columns.Add("PersonalTax", "Thuế Cá Nhân");
            GV_Product.Columns.Add("TLV", "TLV");
            GV_Product.Columns.Add("RealLeaders", "Thực Lãnh");

            btn = new DataGridViewButtonColumn();
            btn.Name = "";
            btn.Text = "...";
            btn.UseColumnTextForButtonValue = true;
            GV_Product.Columns.Add(btn);

            #region
            GV_Product.Columns["No"].Width = 40;
            GV_Product.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Product.Columns["No"].ReadOnly = true;

            GV_Product.Columns["EmployeeName"].Width = 100;
            GV_Product.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Product.Columns["EmployeeID"].Width = 250;
            GV_Product.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Product.Columns["EmployeeID"].Frozen = true;

            GV_Product.Columns["DepartmentName"].Width = 100;
            GV_Product.Columns["DepartmentName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Product.Columns["Position"].Width = 100;
            GV_Product.Columns["Position"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["ATM"].Width = 100;
            GV_Product.Columns["ATM"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["SalaryBasic"].Width = 100;
            GV_Product.Columns["SalaryBasic"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["SalaryBasic"].ReadOnly = true;

            GV_Product.Columns["SalaryContract"].Width = 100;
            GV_Product.Columns["SalaryContract"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["SalaryContract"].Visible = false;

            GV_Product.Columns["KPI"].Width = 100;
            GV_Product.Columns["KPI"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["KPI"].Visible = false;

            GV_Product.Columns["Support_Effective"].Width = 100;
            GV_Product.Columns["Support_Effective"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Support_Effective"].Visible = false;

            GV_Product.Columns["Support_Gas"].Width = 100;
            GV_Product.Columns["Support_Gas"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["Support_Effective_Real"].Width = 160;
            GV_Product.Columns["Support_Effective_Real"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            // setup style view
            GV_Product.Columns["Total_SalaryMain_Effective"].Width = 60;
            GV_Product.Columns["Total_SalaryMain_Effective"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            #endregion
            GV_Product.Columns["Total_SalaryMain_Effective"].Width = 100;
            GV_Product.Columns["Total_SalaryMain_Effective"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["WorkingDate_Caculator"].Width = 100;
            GV_Product.Columns["WorkingDate_Caculator"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["WorkingDate_Real"].Width = 100;
            GV_Product.Columns["WorkingDate_Real"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["DayOff"].Width = 100;
            GV_Product.Columns["DayOff"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["DayOff_Money"].Width = 100;
            GV_Product.Columns["DayOff_Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["DayOff_Holiday"].Width = 100;
            GV_Product.Columns["DayOff_Holiday"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["DayOff_Holiday_Money"].Width = 100;
            GV_Product.Columns["DayOff_Holiday_Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Salary_WorkingDate"].Width = 100;
            GV_Product.Columns["Salary_WorkingDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Day_Salary_Different"].Width = 100;
            GV_Product.Columns["Day_Salary_Different"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Working_Different"].Width = 100;
            GV_Product.Columns["Working_Different"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Shipment"].Width = 100;
            GV_Product.Columns["Shipment"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Total_Salary"].Width = 100;
            GV_Product.Columns["Total_Salary"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Children_Number"].Width = 100;
            GV_Product.Columns["Children_Number"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Children_Money"].Width = 100;
            GV_Product.Columns["Children_Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Rice_Number_16"].Width = 100;
            GV_Product.Columns["Rice_Number_16"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Rice_Money_16"].Width = 100;
            GV_Product.Columns["Rice_Money_16"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Rice_Number_20"].Width = 100;
            GV_Product.Columns["Rice_Number_20"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Rice_Money_20"].Width = 100;
            GV_Product.Columns["Rice_Money_20"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Product.Columns["BHXH"].Width = 100;
            GV_Product.Columns["BHXH"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Federation"].Width = 100;
            GV_Product.Columns["Federation"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Re_Shipment"].Width = 100;
            GV_Product.Columns["Re_Shipment"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Re_Salary"].Width = 100;
            GV_Product.Columns["Re_Salary"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["Tax"].Width = 100;
            GV_Product.Columns["Tax"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["PersonalTax"].Width = 100;
            GV_Product.Columns["PersonalTax"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["TLV"].Width = 100;
            GV_Product.Columns["TLV"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Product.Columns["RealLeaders"].Width = 100;
            GV_Product.Columns["RealLeaders"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            GV_Product.BackgroundColor = Color.White;
            GV_Product.GridColor = Color.FromArgb(227, 239, 255);
            GV_Product.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Product.DefaultCellStyle.ForeColor = Color.Navy;
            GV_Product.DefaultCellStyle.Font = new Font("Arial", 9);
            GV_Product.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(0, 90, 200);
            GV_Product.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(206, 246, 255);
            GV_Product.EnableHeadersVisualStyles = false;

            GV_Product.AllowUserToResizeRows = false;
            GV_Product.AllowUserToResizeColumns = true;

            GV_Product.RowHeadersVisible = false;
            GV_Product.AllowUserToAddRows = true;
            GV_Product.AllowUserToDeleteRows = false;

            //// setup Height Header
            // GV_Products.ColumnHeadersHeight = GV_Products.ColumnHeadersHeight * 3 / 2;
            GV_Product.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Product.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;

            for (int i = 1; i <= 8; i++)
            {
                GV_Product.Rows.Add();
            }

            //this.GV_Product.KeyDown += new System.Windows.Forms.KeyEventHandler(GV_Product_KeyDown);
            //this.GV_Product.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.GV_Product_CellEndEdit);
            //this.GV_Product.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GV_Product_EditingControlShowing);

        }
    }
}
