﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.CORE
{
    public class Order_Money_Date_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private int _TeamKey = 0;
        private string _EmployeeKey = "";
        private string _EmployeeID = "";
        private string _EmployeeName = "";
        private DateTime _Date;
        private double _Money;
        private double _Money_Holidays;
        private double _Money_Sundays;
        private double _Money_Dif;
        private double _Money_Dif_Sunday;
        private double _Money_Dif_Holiday;
        private double _Total = 0;
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public double Money_Holidays
        {
            get { return _Money_Holidays; }
            set { _Money_Holidays = value; }
        }
        public double Money_Sundays
        {
            get { return _Money_Sundays; }
            set { _Money_Sundays = value; }
        }
        public double Money_Dif
        {
            get { return _Money_Dif; }
            set { _Money_Dif = value; }
        }
        public double Money_Dif_Sunday
        {
            get { return _Money_Dif_Sunday; }
            set { _Money_Dif_Sunday = value; }
        }
        public double Money_Dif_Holiday
        {
            get { return _Money_Dif_Holiday; }
            set { _Money_Dif_Holiday = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int TeamKey
        {
            get
            {
                return _TeamKey;
            }

            set
            {
                _TeamKey = value;
            }
        }

        public double Total
        {
            get
            {
                return _Total;
            }

            set
            {
                _Total = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Order_Money_Date_Info()
        {
        }
        public Order_Money_Date_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM FTR_Money_Date WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["Date"] != DBNull.Value)
                        _Date = (DateTime)zReader["Date"];
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Money_Holidays"] != DBNull.Value)
                        _Money_Holidays = double.Parse(zReader["Money_Holidays"].ToString());
                    if (zReader["Money_Sundays"] != DBNull.Value)
                        _Money_Sundays = double.Parse(zReader["Money_Sundays"].ToString());
                    if (zReader["Money_Dif"] != DBNull.Value)
                        _Money_Dif = double.Parse(zReader["Money_Dif"].ToString());
                    if (zReader["Money_Dif_Sunday"] != DBNull.Value)
                        _Money_Dif_Sunday = double.Parse(zReader["Money_Dif_Sunday"].ToString());
                    if (zReader["Money_Dif_Holiday"] != DBNull.Value)
                        _Money_Dif_Holiday = double.Parse(zReader["Money_Dif_Holiday"].ToString());
                    if (zReader["Total"] != DBNull.Value)
                        _Total = double.Parse(zReader["Total"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Order_Money_Date_Info(string EmployeeKey, DateTime OrderDate)
        {
            string zSQL = "SELECT * FROM FTR_Money_Date WHERE EmployeeKey = @EmployeeKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = EmployeeKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    if (zReader["Date"] != DBNull.Value)
                        _Date = (DateTime)zReader["Date"];
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["Money_Holidays"] != DBNull.Value)
                        _Money_Holidays = double.Parse(zReader["Money_Holidays"].ToString());
                    if (zReader["Money_Sundays"] != DBNull.Value)
                        _Money_Sundays = double.Parse(zReader["Money_Sundays"].ToString());
                    if (zReader["Money_Dif"] != DBNull.Value)
                        _Money_Dif = double.Parse(zReader["Money_Dif"].ToString());
                    if (zReader["Money_Dif_Sunday"] != DBNull.Value)
                        _Money_Dif_Sunday = double.Parse(zReader["Money_Dif_Sunday"].ToString());
                    if (zReader["Money_Dif_Holiday"] != DBNull.Value)
                        _Money_Dif_Holiday = double.Parse(zReader["Money_Dif_Holiday"].ToString());
                    if (zReader["Total"] != DBNull.Value)
                        _Total = double.Parse(zReader["Total"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Money_Date_INSERT";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_Date == DateTime.MinValue)
                    zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = _Date;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Money_Holidays", SqlDbType.Money).Value = _Money_Holidays;
                zCommand.Parameters.Add("@Money_Sundays", SqlDbType.Money).Value = _Money_Sundays;
                zCommand.Parameters.Add("@Money_Dif", SqlDbType.Money).Value = _Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday", SqlDbType.Money).Value = _Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday", SqlDbType.Money).Value = _Money_Dif_Holiday;
                zCommand.Parameters.Add("@ToTal", SqlDbType.Money).Value = _Total;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            DateTime zFromDate = new DateTime(_Date.Year, _Date.Month, _Date.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(_Date.Year, _Date.Month, _Date.Day, 23, 59, 59);

            string zSQL = "FTR_Money_Date_UPDATE";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                if (_Date == DateTime.MinValue)
                    zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = _Date;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                zCommand.Parameters.Add("@Money_Holidays", SqlDbType.Money).Value = _Money_Holidays;
                zCommand.Parameters.Add("@Money_Sundays", SqlDbType.Money).Value = _Money_Sundays;
                zCommand.Parameters.Add("@Money_Dif", SqlDbType.Money).Value = _Money_Dif;
                zCommand.Parameters.Add("@Money_Dif_Sunday", SqlDbType.Money).Value = _Money_Dif_Sunday;
                zCommand.Parameters.Add("@Money_Dif_Holiday", SqlDbType.Money).Value = _Money_Dif_Holiday;
                zCommand.Parameters.Add("@ToTal", SqlDbType.Money).Value = _Total;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "FTR_Money_Date_DELETE";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.StoredProcedure;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
