﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_FeeLeader_Month : Form
    {
        public Frm_FeeLeader_Month()
        {
            InitializeComponent();
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Import.Click += Btn_Import_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.CellEndEdit += GVData_CellEndEdit;
            btn_Add.Click += Btn_Save_Click;
            InitGV_Layout(GVData);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey, TeamID +'-'+ TeamName AS TeamName FROM [dbo].[SYS_Team] WHERE RecordStatus <> 99 ORDER BY Rank", "---Tất cả---");
            LoadDataToToolbox.KryptonComboBox(cbo_Fee, "SELECT A.CategoryID,A.CategoryID+'-'+A.CategoryName AS CategoryName  FROM [dbo].[HRM_FeeLeaderMonth] A WHERE A.RecordStatus <> 99 Group by A.CategoryID,A.CategoryName", "---Tất cả---");
        }

        private void Frm_FeeLeader_Month_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            dteDate.Value = SessionUser.Date_Work;
            LoadData();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            DataTable ztb = FeeLeaderMonth_Data.List(dteDate.Value, cbo_Team.SelectedValue.ToInt(),cbo_Fee.SelectedValue.ToString());
            if (ztb.Rows.Count > 0)
                InitGV_Data(GVData, ztb);
            else
                GVData.Rows.Clear();
        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_FeeLeaderMonth frm = new Frm_Import_FeeLeaderMonth();
            frm.Show();
        }
        void InitGV_Layout(DataGridView GV)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "Stt");
            GV.Columns.Add("EmployeeName", "Họ và tên");
            GV.Columns.Add("EmployeeID", "Số thẻ");
            GV.Columns.Add("TeamID", "Tổ/nhóm");
            GV.Columns.Add("Value", "Giá trị/Số tiền");
            GV.Columns.Add("Ext", "Đơn vị tính");
            GV.Columns.Add("Description", "Ghi chú");
            GV.Columns.Add("Message", "Thông báo");

            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 200;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 80;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            GV.Columns["TeamID"].Width = 100;
            GV.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomLeft;

            GV.Columns["Value"].Width = 100;
            GV.Columns["Value"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["Ext"].Width = 100;
            GV.Columns["Ext"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["Description"].Width = 200;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Message"].Width = 200;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.ColumnHeadersHeight = 45;
            #endregion
        }

        void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            int no = 0;
            GVData.Rows.Clear();
            foreach (DataRow nRow in zTable.Rows)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[no];
                nRowView.Tag = nRow["AutoKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (no + 1).ToString();
                nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();
                nRowView.Cells["TeamID"].Value = nRow["TeamID"].ToString().Trim();
                float zValue = 0;
                if (nRow["Value"].ToString().Trim() != "")
                {
                    float.TryParse(nRow["Value"].ToString(),out zValue);
                }
                nRowView.Cells["Value"].Value = zValue.Ton1String();
                nRowView.Cells["Ext"].Value = nRow["Ext"].ToString().Trim();
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                nRowView.Cells["Message"].Tag = 0; // 0 là không có sửa, 1 là đã có sửa
                no++;
            }
        }

        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow zRowEdit = GVData.Rows[e.RowIndex];
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 4)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["Value"].Value == null)
                {
                    zRowEdit.Cells["Value"].Value = 0;
                }

            }
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 5)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["Ext"].Value == null || zRowEdit.Cells["Ext"].Value.ToString()=="")
                {
                    zRowEdit.Cells["Ext"].Value = "%";
                    zRowEdit.Cells["Message"].Tag = 1;
                }
            }
            if (GVData.Rows[e.RowIndex].Tag != null && e.ColumnIndex == 6)
            {
                zRowEdit.Cells["Message"].Tag = 1;
                if (zRowEdit.Cells["Description"].Value == null)
                {
                    zRowEdit.Cells["Description"].Value = 0;
                }

            }
        }
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            if (GVData.Rows.Count == 0)
            {
                MessageBox.Show("Không tìm thấy dữ liệu");
                this.Cursor = Cursors.Default;
            }
            else
            {
                Save();
            }
        }
        void Save()
        {
            int zErr = 0;
            for (int i = 0; i < GVData.Rows.Count; i++)
            {
                if (GVData.Rows[i].Tag != null && GVData.Rows[i].Cells["EmployeeID"].Value != null && GVData.Rows[i].Cells["Message"].Tag.ToInt() == 1)
                {
                    FeeLeaderMonth_Info zinfo = new FeeLeaderMonth_Info(GVData.Rows[i].Tag.ToInt());
                    float zValue = 0;
                    if(float.TryParse(GVData.Rows[i].Cells["Value"].Value.ToString(), out zValue))
                    {

                    }
                    zinfo.Value = zValue;
                    zinfo.Description = GVData.Rows[i].Cells["Description"].Value.ToString();

                    zinfo.CreatedBy = SessionUser.UserLogin.Key;
                    zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    zinfo.Update();
                    if (zinfo.Message == "20")
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Cập nhật thành công!";
                    }
                    else
                    {
                        GVData.Rows[i].Cells["Message"].Value = "Lỗi.Vui lòng liên hệ IT!";
                        zErr++;
                    }
                }
            }
            if (zErr == 0)
            {
                MessageBox.Show("Cập nhật thành công");
                this.Cursor = Cursors.Default;
            }
            else
            {
                MessageBox.Show("Cập nhật còn " + zErr.ToString() + " lỗi.Vui lòng kiểm tra lại");
                this.Cursor = Cursors.Default;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
