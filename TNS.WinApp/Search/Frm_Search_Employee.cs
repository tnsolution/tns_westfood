﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.CORE;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Search_Employee : Form
    {
        
        private string _OrderKey = "";
        private string _EmployeeKey = "";
        private string _EmployeeName = "";
        private string _EmployeeID = "";
        private float _Time = 0;
        private float _KgProduct = 0;
        private double _Money = 0;
        public string OrderKey
        {
            get
            {
                return _OrderKey;
            }

            set
            {
                _OrderKey = value;
            }
        }
        public string EmployeeKey
        {
            get
            {
                return _EmployeeKey;
            }

            set
            {
                _EmployeeKey = value;
            }
        }
        public string EmployeeName
        {
            get
            {
                return _EmployeeName;
            }

            set
            {
                _EmployeeName = value;
            }
        }
        public string EmployeeID
        {
            get
            {
                return _EmployeeID;
            }

            set
            {
                _EmployeeID = value;
            }
        }
        public float Time
        {
            get
            {
                return _Time;
            }

            set
            {
                _Time = value;
            }
        }
        public float KgProduct
        {
            get
            {
                return _KgProduct;
            }

            set
            {
                _KgProduct = value;
            }
        }
        public double Money
        {
            get
            {
                return _Money;
            }

            set
            {
                _Money = value;
            }
        }

        public Frm_Search_Employee()
        {
            InitializeComponent();
        }

        private void Frm_ShowEmployee_Load(object sender, EventArgs e)
        {
            Order_Info _Order = new Order_Info(_OrderKey);
            txt_OrderID.Text = _Order.OrderID;
            dte_OrderDate.Value = SessionUser.Date_Work;
            LV_Team(LV_Employee);
            LoadDataToToolbox.ComboBoxData(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Choose Teams ----");
        }

        private void cbo_Team_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_Team.SelectedIndex != 0)
            {
                LoadDataToToolbox.ComboBoxData(cbo_EmployeeID, @"SELECT EmployeeKey,A.EmployeeID + ' | ' + B.FullName FROM [dbo].[HRM_Employee] A
                                                           LEFT JOIN[dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey
                                                           WHERE A.TeamKey = '" + int.Parse(cbo_Team.SelectedValue.ToString()) + "' AND A.RecordStatus < 99 AND B.RecordStatus < 99"
                                                           , "---- Choose Teams ----");
            }
        }
        #region [Design Layout]
        private void LV_Team(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Mã NV";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhân Viên";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Thời Gian";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Số Kg";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);


            colHead = new ColumnHeader();
            colHead.Text = "Số Tiền";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_Employee;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Order_Data.List_Employee(_OrderKey,cbo_EmployeeID.SelectedValue.ToString());

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["EmployeeKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["EmployeeID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["FullName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                float zTime = float.Parse(nRow["Time"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zTime.ToString(); 
                lvi.SubItems.Add(lvsi);

                float zKgProduct = float.Parse(nRow["KgProduct"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zKgProduct.ToString("n1"); 
                lvi.SubItems.Add(lvsi);


                float zMoney = float.Parse(nRow["Money"].ToString().Trim());
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zMoney.ToString("n0"); 
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }


        #endregion

        private void btn_ConvertTeam_Click(object sender, EventArgs e)
        {
            LV_LoadData();
        }

        private void LV_Employee_ItemActivate(object sender, EventArgs e)
        {
            _EmployeeKey = "";
            _EmployeeID = "";
            _EmployeeName = "";
            _Time = 0;
            _KgProduct = 0;
            _Money = 0;
            if (LV_Employee.SelectedItems.Count == 0)
            {
                MessageBox.Show("Vui lòng chọn đơn hàng!");
            }
            else
            {
                _EmployeeKey = LV_Employee.SelectedItems[0].Tag.ToString();
                _EmployeeID = LV_Employee.SelectedItems[0].SubItems[1].Text;
                _EmployeeName = LV_Employee.SelectedItems[0].SubItems[2].Text;
                _Time = float.Parse(LV_Employee.SelectedItems[0].SubItems[3].Text);
                _KgProduct = float.Parse(LV_Employee.SelectedItems[0].SubItems[4].Text);
                _Money = double.Parse(LV_Employee.SelectedItems[0].SubItems[5].Text);
                this.Close();
            }
        }

    }
}
