﻿namespace TNS.WinApp
{
    partial class Frm_BaoCaoUngLuong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_BaoCaoUngLuong));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Print = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdo_NoVU = new System.Windows.Forms.RadioButton();
            this.rdo_VU = new System.Windows.Forms.RadioButton();
            this.rdo_All = new System.Windows.Forms.RadioButton();
            this.dte_Month = new TN_Tools.TNDateTime();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonButton1 = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.cbo_Team = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbo_Department = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_Search = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.cbo_Branch = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.GVData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.Panel_Done = new System.Windows.Forms.Panel();
            this.dte_RpDate = new TN_Tools.TNDateTime();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_RpGiamDoc = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpHCNS = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpKeToan = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_RpNguoiLap = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txt_RpTitle = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.kryptonHeader6 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnClose_Panel_Message = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btn_Done = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Team)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Department)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Branch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.Panel_Done.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1350, 42);
            this.HeaderControl.TabIndex = 239;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "DANH SÁCH TẠM ỨNG LƯƠNG";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.btn_Print);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.dte_Month);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.kryptonButton1);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Controls.Add(this.cbo_Team);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cbo_Department);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.txt_Search);
            this.panel1.Controls.Add(this.cbo_Branch);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1350, 80);
            this.panel1.TabIndex = 283;
            // 
            // btn_Print
            // 
            this.btn_Print.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_Print.Location = new System.Drawing.Point(1093, 21);
            this.btn_Print.Name = "btn_Print";
            this.btn_Print.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Print.Size = new System.Drawing.Size(111, 40);
            this.btn_Print.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Print.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Print.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Print.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Print.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Print.TabIndex = 318;
            this.btn_Print.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Print.Values.Image")));
            this.btn_Print.Values.Text = "Xuất In";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdo_NoVU);
            this.groupBox2.Controls.Add(this.rdo_VU);
            this.groupBox2.Controls.Add(this.rdo_All);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.groupBox2.ForeColor = System.Drawing.Color.Navy;
            this.groupBox2.Location = new System.Drawing.Point(12, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(168, 75);
            this.groupBox2.TabIndex = 317;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Hiển thị";
            // 
            // rdo_NoVU
            // 
            this.rdo_NoVU.AutoSize = true;
            this.rdo_NoVU.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_NoVU.Location = new System.Drawing.Point(6, 51);
            this.rdo_NoVU.Name = "rdo_NoVU";
            this.rdo_NoVU.Size = new System.Drawing.Size(153, 18);
            this.rdo_NoVU.TabIndex = 210;
            this.rdo_NoVU.Text = "3. Không có vườn ươm";
            this.rdo_NoVU.UseVisualStyleBackColor = true;
            // 
            // rdo_VU
            // 
            this.rdo_VU.AutoSize = true;
            this.rdo_VU.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_VU.Location = new System.Drawing.Point(6, 32);
            this.rdo_VU.Name = "rdo_VU";
            this.rdo_VU.Size = new System.Drawing.Size(130, 18);
            this.rdo_VU.TabIndex = 210;
            this.rdo_VU.Text = "2.Chỉ có vườn ươm";
            this.rdo_VU.UseVisualStyleBackColor = true;
            // 
            // rdo_All
            // 
            this.rdo_All.AutoSize = true;
            this.rdo_All.Checked = true;
            this.rdo_All.Font = new System.Drawing.Font("Tahoma", 9F);
            this.rdo_All.Location = new System.Drawing.Point(6, 15);
            this.rdo_All.Name = "rdo_All";
            this.rdo_All.Size = new System.Drawing.Size(75, 18);
            this.rdo_All.TabIndex = 209;
            this.rdo_All.TabStop = true;
            this.rdo_All.Text = "1. Tất cả";
            this.rdo_All.UseVisualStyleBackColor = true;
            // 
            // dte_Month
            // 
            this.dte_Month.CustomFormat = "MM/yyyy";
            this.dte_Month.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_Month.Location = new System.Drawing.Point(766, 43);
            this.dte_Month.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_Month.Name = "dte_Month";
            this.dte_Month.Size = new System.Drawing.Size(104, 26);
            this.dte_Month.TabIndex = 316;
            this.dte_Month.Value = new System.DateTime(((long)(0)));
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(878, 21);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(106, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 294;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // kryptonButton1
            // 
            this.kryptonButton1.Location = new System.Drawing.Point(1250, 21);
            this.kryptonButton1.Name = "kryptonButton1";
            this.kryptonButton1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonButton1.Size = new System.Drawing.Size(100, 40);
            this.kryptonButton1.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.kryptonButton1.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.kryptonButton1.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kryptonButton1.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.kryptonButton1.TabIndex = 293;
            this.kryptonButton1.Values.Image = ((System.Drawing.Image)(resources.GetObject("kryptonButton1.Values.Image")));
            this.kryptonButton1.Values.Text = "Xuất Excel";
            this.kryptonButton1.Visible = false;
            this.kryptonButton1.Click += new System.EventHandler(this.kryptonButton1_Click);
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(990, 21);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(100, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 293;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // cbo_Team
            // 
            this.cbo_Team.DropDownWidth = 119;
            this.cbo_Team.Location = new System.Drawing.Point(530, 16);
            this.cbo_Team.Name = "cbo_Team";
            this.cbo_Team.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Team.Size = new System.Drawing.Size(340, 22);
            this.cbo_Team.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Team.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Team.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Team.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Team.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Team.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Team.StateCommon.Item.Border.Width = 1;
            this.cbo_Team.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Team.TabIndex = 281;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label7.Location = new System.Drawing.Point(468, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 15);
            this.label7.TabIndex = 282;
            this.label7.Text = "Tổ nhóm";
            // 
            // cbo_Department
            // 
            this.cbo_Department.DropDownWidth = 119;
            this.cbo_Department.Location = new System.Drawing.Point(240, 44);
            this.cbo_Department.Name = "cbo_Department";
            this.cbo_Department.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Department.Size = new System.Drawing.Size(214, 22);
            this.cbo_Department.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Department.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Department.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Department.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Department.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Department.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Department.StateCommon.Item.Border.Width = 1;
            this.cbo_Department.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Department.TabIndex = 283;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(460, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 284;
            this.label2.Text = "Mã thẻ/ tên";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label17.Location = new System.Drawing.Point(718, 50);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 15);
            this.label17.TabIndex = 285;
            this.label17.Text = "Tháng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label9.Location = new System.Drawing.Point(206, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 15);
            this.label9.TabIndex = 285;
            this.label9.Text = "Khối";
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(530, 43);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search.Size = new System.Drawing.Size(182, 26);
            this.txt_Search.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search.StateCommon.Border.Rounding = 4;
            this.txt_Search.StateCommon.Border.Width = 1;
            this.txt_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.TabIndex = 280;
            // 
            // cbo_Branch
            // 
            this.cbo_Branch.DropDownWidth = 119;
            this.cbo_Branch.Location = new System.Drawing.Point(240, 16);
            this.cbo_Branch.Name = "cbo_Branch";
            this.cbo_Branch.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Branch.Size = new System.Drawing.Size(214, 22);
            this.cbo_Branch.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Branch.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Branch.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Branch.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Branch.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Branch.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Branch.StateCommon.Item.Border.Width = 1;
            this.cbo_Branch.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Branch.TabIndex = 289;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label5.Location = new System.Drawing.Point(187, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 290;
            this.label5.Text = "Bộ phận";
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 122);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(1350, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 288;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "DANH SÁCH";
            // 
            // GVData
            // 
            this.GVData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVData.AllowEditing = false;
            this.GVData.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.ExtendLastCol = true;
            this.GVData.Location = new System.Drawing.Point(0, 152);
            this.GVData.Name = "GVData";
            this.GVData.Rows.DefaultSize = 19;
            this.GVData.Size = new System.Drawing.Size(1350, 409);
            this.GVData.TabIndex = 289;
            // 
            // Panel_Done
            // 
            this.Panel_Done.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Panel_Done.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.Panel_Done.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel_Done.Controls.Add(this.dte_RpDate);
            this.Panel_Done.Controls.Add(this.label10);
            this.Panel_Done.Controls.Add(this.label1);
            this.Panel_Done.Controls.Add(this.label3);
            this.Panel_Done.Controls.Add(this.label6);
            this.Panel_Done.Controls.Add(this.txt_RpGiamDoc);
            this.Panel_Done.Controls.Add(this.txt_RpHCNS);
            this.Panel_Done.Controls.Add(this.txt_RpKeToan);
            this.Panel_Done.Controls.Add(this.txt_RpNguoiLap);
            this.Panel_Done.Controls.Add(this.label8);
            this.Panel_Done.Controls.Add(this.label16);
            this.Panel_Done.Controls.Add(this.txt_RpTitle);
            this.Panel_Done.Controls.Add(this.kryptonHeader6);
            this.Panel_Done.Controls.Add(this.btn_Done);
            this.Panel_Done.Location = new System.Drawing.Point(296, 228);
            this.Panel_Done.Name = "Panel_Done";
            this.Panel_Done.Size = new System.Drawing.Size(492, 325);
            this.Panel_Done.TabIndex = 291;
            // 
            // dte_RpDate
            // 
            this.dte_RpDate.CustomFormat = "dd/MM/yyyy";
            this.dte_RpDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_RpDate.Location = new System.Drawing.Point(183, 35);
            this.dte_RpDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_RpDate.Name = "dte_RpDate";
            this.dte_RpDate.Size = new System.Drawing.Size(121, 26);
            this.dte_RpDate.TabIndex = 316;
            this.dte_RpDate.Value = new System.DateTime(((long)(0)));
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label10.Location = new System.Drawing.Point(51, 250);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 15);
            this.label10.TabIndex = 269;
            this.label10.Text = "Tổng giám đốc";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(78, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 15);
            this.label1.TabIndex = 269;
            this.label1.Text = "BP HCNS";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(47, 181);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 15);
            this.label3.TabIndex = 269;
            this.label3.Text = "Kế toán trưởng";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(78, 149);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 15);
            this.label6.TabIndex = 269;
            this.label6.Text = "Người lập";
            // 
            // txt_RpGiamDoc
            // 
            this.txt_RpGiamDoc.Location = new System.Drawing.Point(183, 241);
            this.txt_RpGiamDoc.Name = "txt_RpGiamDoc";
            this.txt_RpGiamDoc.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpGiamDoc.Size = new System.Drawing.Size(282, 26);
            this.txt_RpGiamDoc.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpGiamDoc.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpGiamDoc.StateCommon.Border.Rounding = 4;
            this.txt_RpGiamDoc.StateCommon.Border.Width = 1;
            this.txt_RpGiamDoc.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpGiamDoc.TabIndex = 289;
            // 
            // txt_RpHCNS
            // 
            this.txt_RpHCNS.Location = new System.Drawing.Point(183, 209);
            this.txt_RpHCNS.Name = "txt_RpHCNS";
            this.txt_RpHCNS.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpHCNS.Size = new System.Drawing.Size(282, 26);
            this.txt_RpHCNS.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpHCNS.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpHCNS.StateCommon.Border.Rounding = 4;
            this.txt_RpHCNS.StateCommon.Border.Width = 1;
            this.txt_RpHCNS.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpHCNS.TabIndex = 289;
            // 
            // txt_RpKeToan
            // 
            this.txt_RpKeToan.Location = new System.Drawing.Point(183, 178);
            this.txt_RpKeToan.Name = "txt_RpKeToan";
            this.txt_RpKeToan.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpKeToan.Size = new System.Drawing.Size(282, 26);
            this.txt_RpKeToan.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpKeToan.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpKeToan.StateCommon.Border.Rounding = 4;
            this.txt_RpKeToan.StateCommon.Border.Width = 1;
            this.txt_RpKeToan.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpKeToan.TabIndex = 289;
            // 
            // txt_RpNguoiLap
            // 
            this.txt_RpNguoiLap.Location = new System.Drawing.Point(183, 147);
            this.txt_RpNguoiLap.Name = "txt_RpNguoiLap";
            this.txt_RpNguoiLap.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpNguoiLap.Size = new System.Drawing.Size(282, 26);
            this.txt_RpNguoiLap.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpNguoiLap.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpNguoiLap.StateCommon.Border.Rounding = 4;
            this.txt_RpNguoiLap.StateCommon.Border.Width = 1;
            this.txt_RpNguoiLap.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpNguoiLap.TabIndex = 289;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label8.Location = new System.Drawing.Point(94, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 15);
            this.label8.TabIndex = 269;
            this.label8.Text = "Ngày in";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label16.Location = new System.Drawing.Point(97, 67);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(48, 15);
            this.label16.TabIndex = 269;
            this.label16.Text = "Tiêu đề";
            // 
            // txt_RpTitle
            // 
            this.txt_RpTitle.Location = new System.Drawing.Point(183, 67);
            this.txt_RpTitle.Multiline = true;
            this.txt_RpTitle.Name = "txt_RpTitle";
            this.txt_RpTitle.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_RpTitle.Size = new System.Drawing.Size(282, 76);
            this.txt_RpTitle.StateCommon.Border.ColorAngle = 1F;
            this.txt_RpTitle.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_RpTitle.StateCommon.Border.Rounding = 4;
            this.txt_RpTitle.StateCommon.Border.Width = 1;
            this.txt_RpTitle.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_RpTitle.TabIndex = 268;
            // 
            // kryptonHeader6
            // 
            this.kryptonHeader6.AutoSize = false;
            this.kryptonHeader6.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnClose_Panel_Message});
            this.kryptonHeader6.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader6.HeaderStyle = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.kryptonHeader6.Location = new System.Drawing.Point(0, 0);
            this.kryptonHeader6.Name = "kryptonHeader6";
            this.kryptonHeader6.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader6.Size = new System.Drawing.Size(490, 30);
            this.kryptonHeader6.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.kryptonHeader6.TabIndex = 206;
            this.kryptonHeader6.Values.Description = "";
            this.kryptonHeader6.Values.Heading = "!.Thông tin xuất báo cáo";
            this.kryptonHeader6.Values.Image = null;
            // 
            // btnClose_Panel_Message
            // 
            this.btnClose_Panel_Message.Checked = ComponentFactory.Krypton.Toolkit.ButtonCheckState.Checked;
            this.btnClose_Panel_Message.Enabled = ComponentFactory.Krypton.Toolkit.ButtonEnabled.True;
            this.btnClose_Panel_Message.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.Close;
            this.btnClose_Panel_Message.UniqueName = "BEA2AE54CDCE44F263AB208C74E6907A";
            // 
            // btn_Done
            // 
            this.btn_Done.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Done.Location = new System.Drawing.Point(203, 274);
            this.btn_Done.Name = "btn_Done";
            this.btn_Done.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Done.Size = new System.Drawing.Size(119, 40);
            this.btn_Done.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Done.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Done.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Done.TabIndex = 12;
            this.btn_Done.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Done.Values.Image")));
            this.btn_Done.Values.Text = "OK";
            // 
            // Frm_BaoCaoUngLuong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 561);
            this.Controls.Add(this.Panel_Done);
            this.Controls.Add(this.GVData);
            this.Controls.Add(this.kryptonHeader3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_BaoCaoUngLuong";
            this.Text = "Frm_BaoCaoUngLuong";
            this.Load += new System.EventHandler(this.Frm_BaoCaoUngLuong_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Team)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Department)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Branch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.Panel_Done.ResumeLayout(false);
            this.Panel_Done.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private TN_Tools.TNDateTime dte_Month;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Team;
        private System.Windows.Forms.Label label7;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Department;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label9;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Branch;
        private System.Windows.Forms.Label label5;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private C1.Win.C1FlexGrid.C1FlexGrid GVData;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdo_NoVU;
        private System.Windows.Forms.RadioButton rdo_VU;
        private System.Windows.Forms.RadioButton rdo_All;
        private ComponentFactory.Krypton.Toolkit.KryptonButton kryptonButton1;
        private System.Windows.Forms.Panel Panel_Done;
        private TN_Tools.TNDateTime dte_RpDate;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpGiamDoc;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpHCNS;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpKeToan;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpNguoiLap;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_RpTitle;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader6;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose_Panel_Message;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Done;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Print;
    }
}