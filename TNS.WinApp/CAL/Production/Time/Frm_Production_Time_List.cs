﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Production_Time_List : Form
    {
        double _Sotien = 0;
        string _FileName = "";
        int _SheetKey = 0;
        DateTime _DateTime;
        DataTable _tblSheet;
        DataTable _TableDefines = new DataTable();
        Frm_Loading _FrmLoading = new Frm_Loading(null);
        BackgroundWorker _Worker = new BackgroundWorker();

        public Frm_Production_Time_List()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DrawLVStyle(ref LVData);

            LoadTableDefines();                      
            LVData.Click += LV_List_Click;

            _Worker.DoWork += (sender, args) => PerformReading();
            _Worker.RunWorkerCompleted += (sender, args) => ReadingCompleted();

            Utils.SizeLastColumn_LV(LVData);
        }

        private int _Key = 0;
        private string _Text = "";

        private void Frm_List_ImportTimeWorking_Load(object sender, EventArgs e)
        {
            _Sotien = HoTroSanXuat.LayTienNgoaiGio();

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LV_Layout(LVData);
            LV_LoadData();
        }
        #region[ListView]
        private void LV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tháng Import";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên file";
            colHead.Width = 650;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

        }

        private void LV_LoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();


            DataTable ztb = Temp_Import_Data.List(dte_FromDate.Value, dte_ToDate.Value);
            for (int i = 0; i < ztb.Rows.Count; i++)
            {
                DataRow nRow = ztb.Rows[i];

                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.Tag = nRow["ExcelKey"];
                lvi.ForeColor = Color.Navy;

                lvi.BackColor = Color.White;
                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                DateTime zDateImport = DateTime.Parse(nRow["DateImport"].ToString().Trim());
                lvsi.Text = zDateImport.ToString("MM/yyyy");
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["File"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;

        }
        private void LV_List_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {

                for (int i = 0; i < LVData.Items.Count; i++)
                {
                    if (LVData.Items[i].Selected == true)
                    {
                        LVData.Items[i].BackColor = Color.LightBlue; // highlighted item
                    }
                    else
                    {
                        LVData.Items[i].BackColor = SystemColors.Window; // normal item
                    }
                }
                _Key = int.Parse(LVData.SelectedItems[0].Tag.ToString());
                _Text = LVData.SelectedItems[0].SubItems[2].Text;
                Frm_Production_Time_Detail frm = new Frm_Production_Time_Detail();
                frm.ExcelKey = _Key;
                frm.Title = _Text;
                frm.Show();
                LV_LoadData();
            }
        }
        #endregion
        private void Btn_New_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "Excel Files|*.xls;*.xlsx;*";
            of.Multiselect = false;
            if (DialogResult.OK == of.ShowDialog())
            {
                _FileName = of.FileName;
                _tblSheet = new DataTable();
                _tblSheet = Data_Access.GetSheet(of.FileName);
                if (_tblSheet.Rows.Count <= 0)
                {
                    MessageBox.Show("Vui lòng đóng File trước khi upload  !");
                }
                else
                {
                    _DateTime = SessionUser.Date_Work;
                    string strFileName = Path.GetFileName(of.FileName).Trim();
                    Temp_Import_Info ztemp = new Temp_Import_Info();
                    ztemp.DateImport = SessionUser.Date_Work;
                    ztemp.File = strFileName;
                    ztemp.CreatedBy = SessionUser.UserLogin.EmployeeKey;
                    ztemp.CreatedName = SessionUser.UserLogin.EmployeeName;
                    ztemp.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
                    ztemp.ModifiedName = SessionUser.UserLogin.EmployeeName;
                    ztemp.Save();
                    _SheetKey = ztemp.Key;
                    _Worker.RunWorkerAsync();
                    //_FrmLoading.ShowDialog();
                }
            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    Temp_Import_Info zinfo = new Temp_Import_Info();
                    zinfo.Key = LVData.SelectedItems[0].Tag.ToInt();
                    zinfo.DeleteAll();
                    string zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        MessageBox.Show("Đã xóa thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LV_LoadData();
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        string GetTime(string Time)
        {
            string zResult = "";
            string Minute = "0.0";
            string[] s = Time.Split('.');
            string Hour = s[0];
            if (s.Length > 1)
            {
                Minute = "0." + s[1];
            }
            float temp = float.Parse(Minute);

            foreach (DataRow r in _TableDefines.Rows)
            {
                float from = r[0].ToFloat();
                float to = r[1].ToFloat();
                float min = r[2].ToFloat();
                if (temp >= from && temp <= to)
                {
                    if (temp < (float)0.95)
                    {
                        zResult = Hour + "." + min;
                    }
                    else
                    {
                        int zHour = Hour.ToInt() + 1;
                        zResult = zHour.ToString() + ".0";

                    }
                    break;
                }
            }

            return zResult;
        }
        void PerformReading()
        {
            for (int i = 0; i < _tblSheet.Rows.Count; i++)
            {
                //Luu thong tin sheet
                string _sheetname = _tblSheet.Rows[i]["sheetName"].ToString();
                DateTime zDateImport = new DateTime(_DateTime.Year, _DateTime.Month, _sheetname.ToInt(), 0, 0, 0);

                Temp_Import_Sheet_Info zSheet = new Temp_Import_Sheet_Info();
                zSheet.SheetName = _sheetname;
                zSheet.DateImport = zDateImport;
                zSheet.ExcelKey = _SheetKey;
                zSheet.CreatedBy = SessionUser.UserLogin.EmployeeKey;
                zSheet.CreatedName = SessionUser.UserLogin.EmployeeName;
                zSheet.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
                zSheet.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zSheet.Save();

                DataTable ztbDetail = Data_Access.GetTable(_FileName, _sheetname, 2);
                int n = 0;
                if (ztbDetail.Rows.Count > 0)
                {
                    //Luu thong tin chi tiet thoi gian cong nhan
                    foreach (DataRow row in ztbDetail.Rows)
                    {

                        Temp_Import_Detail_Info zDetail = new Temp_Import_Detail_Info();
                        zDetail.SheetKey = zSheet.Key;
                        zDetail.DateImport = zDateImport;
                        zDetail.ExcelKey = _SheetKey;

                        Employee_Info zEmployee = new Employee_Info();
                        zEmployee.GetEmployeeID(row[2].ToString());
                        zDetail.EmployeeKey = zEmployee.Key;
                        zDetail.EmployeeID = row[2].ToString();
                        zDetail.EmployeeName = row[1].ToString();

                        Team_Info zTeam = new Team_Info();
                        zTeam.Get_Team_ID(row[3].ToString());
                        zDetail.TeamKey = zTeam.Key;
                        zDetail.TeamName = zTeam.TeamName;
                        zDetail.TeamID = row[3].ToString();

                        string zBeginTime = "0.0";
                        if (row[4].ToString().Trim() != "")
                            zBeginTime = row[4].ToString().Trim();
                        zDetail.BeginTime = GetTime(zBeginTime);

                        string zEndTime = "0.0";
                        if (row[5].ToString().Trim() != "")
                            zEndTime = row[5].ToString().Trim();
                        zDetail.EndTime = GetTime(zEndTime);

                        string zOffTime = "0.0";
                        if (row[6].ToString().Trim() != "")
                            zOffTime = row[6].ToString().Trim();
                        zDetail.OffTime = GetTime(zOffTime);

                        string zDifferentTime = "0.0";
                        if (row[7].ToString().Trim() != "")
                            zDifferentTime = row[7].ToString().Trim();
                        zDetail.DifferentTime = GetTime(zDifferentTime);

                        string zTotalTime = "0.0";
                        if (row[8].ToString().Trim() != "")
                            zTotalTime = row[8].ToString().Trim();
                        zDetail.TotalTime = GetTime(zTotalTime);

                        string zOverTime = "0.0";
                        if (row[9].ToString().Trim() != "")
                            zOverTime = row[9].ToString().Trim();
                        zDetail.OverTime = GetTime(zOverTime);

                        if (row[10].ToString().Trim() != "")
                            zDetail.OverTimeBegin = DateTime.Parse(row[10].ToString());
                        else
                        {
                            zDetail.OverTimeBegin = DateTime.MinValue;
                        }

                        if (row[11].ToString().Trim() != "")
                            zDetail.OverTimeEnd = DateTime.Parse(row[11].ToString());
                        else
                        {
                            zDetail.OverTimeEnd = DateTime.MinValue;
                        }

                        zDetail.CreatedBy = SessionUser.UserLogin.EmployeeKey;
                        zDetail.CreatedName = SessionUser.UserLogin.EmployeeName;
                        zDetail.ModifiedBy = SessionUser.UserLogin.EmployeeKey;
                        zDetail.ModifiedName = SessionUser.UserLogin.EmployeeName;

                        //XỬ LÝ TÍNH TIỀN

                        float GioDu = zOverTime.ToFloat();
                        if (GioDu > 0)
                        {
                            zDetail.Amount = _Sotien;
                            zDetail.Money = _Sotien * GioDu;
                        }
                        //
                        zDetail.Save();
                        n++;
                    }
                }
            }
        }
        void ReadingCompleted()
        {
            _FrmLoading.Close();
            LV_LoadData();
        }
        void LoadTableDefines()
        {
            string File = Application.StartupPath + "\\wfTime.xlsx";
            _TableDefines = Data_Access.GetTable(File, "1");
        }

        private void LV_List_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);

            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;

            LV_LoadData();
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void btn_Done_Click(object sender, EventArgs e)
        {

        }
    }
}