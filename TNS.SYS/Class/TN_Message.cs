﻿namespace TNS.SYS
{
    public class TN_Message
    {
        public static string Show(string Message)
        {
            if (Message == string.Empty)
            {
                return string.Empty;
            }
            if (Message.Substring(0, 2) == "71")
            {
                return "Bạn không có quyền nhập mới dữ liệu";
            }
            if (Message.Substring(0, 2) == "72")
            {
                return "Bạn không có quyền nhập sửa dữ liệu";
            }
            if (Message.Substring(0, 2) == "73")
            {
                return "Bạn không có quyền xóa dữ liệu";
            }
            if (Message.Substring(0, 2) == "74")
            {
                return "Bạn không có quyền nhập duyệt dữ liệu";
            }
            if (Message.Substring(0, 2) == "08")
            {
                return "Có phát sinh lỗi !" + Message;
            }
            if (Message.Substring(0, 2) == "41")
            {
                return string.Empty;
                //return "Yêu cầu duyệt đã được gởi";
            }
            if (Message.Substring(0, 2) == "42")
            {
                return string.Empty;
            }
            if (Message.Substring(0, 2) == "43")
            {
                return string.Empty;
            }

            //đã khóa record
            if (Message.Substring(0, 2) == "49")
            {
                return "";
            }

            //duyệt và gửi thành công
            if (Message.Substring(0, 2) == "44")
            {
                return string.Empty;
                //return "Nhập dữ liệu thành công";
            }

            //Nhập dữ liệu thành công
            if (Message.Substring(0, 2) == "11")
            {
                return string.Empty;
                //return "Nhập dữ liệu thành công";
            }

            //Xóa dữ liệu thành công
            if (Message.Substring(0, 2) == "30")
            {
                return string.Empty;

                //return "Nhập dữ liệu thành công";
            }

            //Cập nhật dữ liệu thành công
            if (Message.Substring(0, 2) == "20")
            {
                return string.Empty;
                //return "Cập nhật dữ liệu thành công";
            }

            //hết hạn cookies
            if (Message.Substring(0, 2) == "98")
            {
                return "Bạn phải đăng nhập lại !.";
            }

            if (Message.Substring(0, 2) == "99")
            {
                return "Người dùng này đã bị xóa !.";
            }

            if (Message.Substring(0, 2) == "52")
            {
                return "User này chưa kích hoạt, vui lòng liên hệ cấp quản lý !.";
            }
            if (Message.Substring(0, 2) == "31")
            {
                return "Vui lòng kiểm tra Username và Password !.";
            }
            if (Message.Substring(0, 2) == "32")
            {
                return "User này đã hết hạn, vui lòng liên hệ cấp quản lý !.";
            }

            return "Không có thông báo xử lý, vui lòng liên hệ IT kiểm tra !.";
        }
    }
}
