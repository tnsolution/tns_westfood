﻿using C1.Win.C1FlexGrid;
using HHT.HRM;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.FIN;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_RiceCompany : Form
    {
        DateTime _DateLog;
        int _CheckErr = 0;
        string _txtLog = "";
        FileInfo _FileInfo;
        DataTable _TableErr;
        DataTable _TableTime = new DataTable();
        DateTime _ExcelDate = SessionUser.Date_Work;
        private string _Log_Detail = "";
        private string _FileName = "";

        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;

        public Frm_RiceCompany()
        {
            InitializeComponent();

            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            btn_Import.Click += Btn_New_Click;
            btn_Search.Click += Btn_Search_Click;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;

            btn_Log.Click += Btn_Log_Click;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        private void Frm_RiceCompany_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            DateTime DateView = SessionUser.Date_Work;
            dte_FromDate.Value = new DateTime(DateView.Year, 1, 1);
            dte_ToDate.Value = new DateTime(DateView.Year, DateView.Month, 1);
            if (Role.AccessBranch == "2,4")
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "--Chọn tất cả--");
            else
            {
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "");
                Cbo_Branch_SelectedIndexChanged(null,null);
            }
                GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            if (rdo_Phan.Checked)
            {
                //txt_TitleGrid.Text = "Thông tin chi tiết sản phẩm theo công đoạn, nhóm, ngày thường, chủ nhật, lễ, tổng.";
                using (Frm_Loading frm = new Frm_Loading(DisplayDataPhan)) { frm.ShowDialog(this); }
            }
            if (rdo_Tien.Checked)
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayDataTien)) { frm.ShowDialog(this); }
            }
        }
        #region[Báo cáo]
        private void DisplayDataTien()
        {

            DataTable zTable = RiceTeamCompany_Data.TIENCOMCONGTY(_FromDate, _ToDate, _BranchKey, _DepartmentKey);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "COMTRUA", "COMCONGTY", "DONVINAU", "COMNGOAI" }, "TỔ NHÓM", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Cơm trưa", "Tăng ca", "Trả đơn vị nấu", "Tăng ca ngoài" }, 0);

                zTablePivot.Columns.Add("DepartmentName").SetOrdinal(0);

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string loai = zTablePivot.Rows[i][1].ToString().Split('|')[0];
                    zTablePivot.Rows[i][0] = loai;
                }

                string Loai = zTablePivot.Rows[0][0].ToString();
                DataRow[] Array = zTablePivot.Select("DepartmentName='" + Loai + "'");
                if (Array.Length > 0)
                {
                    AddGroup(zTablePivot, Loai, Array, 0);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string temploai = zTablePivot.Rows[i][0].ToString();

                    if (temploai != Loai && temploai.ToUpper() != "TỔNG CỘNG")
                    {
                        Loai = temploai;
                        Array = zTablePivot.Select("DepartmentName='" + Loai + "'");
                        if (Array.Length > 0)
                        {
                            AddGroup(zTablePivot, Loai, Array, i);
                        }
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProductTien(zTablePivot);
                }));
            }
        }
        private void DisplayDataPhan()
        {

            DataTable zTable = RiceTeamCompany_Data.PHANCOMCONGTY(_FromDate, _ToDate, _BranchKey, _DepartmentKey);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "COMTRUA", "COMCONGTY" }, "TỔ NHÓM", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Cơm trưa", "Tăng ca" }, 0);

                zTablePivot.Columns.Add("DepartmentName").SetOrdinal(0);

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string loai = zTablePivot.Rows[i][1].ToString().Split('|')[0];
                    zTablePivot.Rows[i][0] = loai;
                }

                string Loai = zTablePivot.Rows[0][0].ToString();
                DataRow[] Array = zTablePivot.Select("DepartmentName='" + Loai + "'");
                if (Array.Length > 0)
                {
                    AddGroup(zTablePivot, Loai, Array, 0);
                }

                for (int i = 0; i < zTablePivot.Rows.Count; i++)
                {
                    string temploai = zTablePivot.Rows[i][0].ToString();

                    if (temploai != Loai && temploai.ToUpper() != "TỔNG CỘNG")
                    {
                        Loai = temploai;
                        Array = zTablePivot.Select("DepartmentName='" + Loai + "'");
                        if (Array.Length > 0)
                        {
                            AddGroup(zTablePivot, Loai, Array, i);
                        }
                    }
                }

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProductPhan(zTablePivot);
                }));
            }
        }


        void InitGVProductTien(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "Stt";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "Tên nhóm";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "Mã nhóm";

            //Row Header
            int ColStart = 3;
            for (int i = 2; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[1];
                    GVData.Rows[1][ColStart] = strCaption[2];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                ColStart++;
            }

            ColStart = 1;
            int RowStart = 2;
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {

                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[1].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][0] = zNo;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                    zNo++;
                }
                else
                {
                    zNo = 1;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart].StyleNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                }

                for (int cIndex = 2; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe0String();
                }
            }


            ////Style
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 250;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 100;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            GVData.Cols[ToTalCol - 4].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 4].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            GVData.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 3].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 4].StyleNew.ForeColor = Color.Blue;
        }
        void InitGVProductPhan(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = TableView.Columns.Count + 1;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 1, 0);
            zCellRange.Data = "Stt";

            zCellRange = GVData.GetCellRange(0, 1, 1, 1);
            zCellRange.Data = "Tên nhóm";

            zCellRange = GVData.GetCellRange(0, 2, 1, 2);
            zCellRange.Data = "Mã nhóm";

            //Row Header
            int ColStart = 3;
            for (int i = 2; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[1];
                    GVData.Rows[1][ColStart] = strCaption[2];
                }
                else
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1];
                }
                ColStart++;
            }

            ColStart = 1;
            int RowStart = 2;
            int zNo = 1;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {

                DataRow rData = TableView.Rows[rIndex];

                string[] strTemp = rData[1].ToString().Split('|');

                if (strTemp.Length > 1)
                {
                    GVData.Rows[rIndex + RowStart][0] = zNo;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1];
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2];
                    zNo++;
                }
                else
                {
                    zNo = 1;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[0];
                    GVData.Rows[rIndex + RowStart].StyleNew.Font = new Font("Tahoma", 9F, FontStyle.Bold | FontStyle.Italic);
                }

                for (int cIndex = 2; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = rData[cIndex].Toe0String();
                }
            }


            ////Style
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 30;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 45;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 250;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 100;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            GVData.Cols[ToTalCol - 4].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 2;
            GVData.Cols.Frozen = 3;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.ForeColor = Color.Blue;
            GVData.Cols[ToTalCol - 2].StyleNew.ForeColor = Color.Blue;
        }

        private void AddGroup(DataTable zTablePivot, string Loai, DataRow[] Array, int Index)
        {
            DataTable zGroup = Array.CopyToDataTable();
            DataRow dr = zTablePivot.NewRow();
            dr[0] = Loai;
            dr[1] = Loai;

            for (int c = 2; c < zGroup.Columns.Count; c++)
            {
                float Tong = 0;
                foreach (DataRow r in zGroup.Rows)
                {
                    float ztemp = 0;
                    if (float.TryParse(r[c].ToString(), out ztemp))
                    {

                    }
                    Tong += ztemp;
                }
                dr[c] = Tong;
            }

            zTablePivot.Rows.InsertAt(dr, Index);
        }
        #endregion
        #region[import]
        private void Btn_Log_Click(object sender, EventArgs e)
        {
            if (btn_Log.Tag.ToInt() == 0)
            {
                Panel_Done.Visible = true;
                btn_Log.Tag = 1;
            }
            else
            {
                Panel_Done.Visible = false;
                btn_Log.Tag = 0;
            }
        }
        void CreateTableError()
        {
            _TableErr = new DataTable();
            _TableErr.Columns.Add("No");
            _TableErr.Columns.Add("MaNhom");
            _TableErr.Columns.Add("TenNhom");
            _TableErr.Columns.Add("ComTrua");
            _TableErr.Columns.Add("ComTangCa");
            _TableErr.Columns.Add("ComDonViNau");
            _TableErr.Columns.Add("TangCaNgoai");
            _TableErr.Columns.Add("ThongBao");
        }

        private void Btn_New_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.DefaultExt = "txt";
            zOpf.Title = "Browse Excel Files";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;
            zOpf.RestoreDirectory = true;
            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;
            _FileName = "";
            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                _FileInfo = new FileInfo(zOpf.FileName);
                string[] s = _FileInfo.Name.Split('_');
                _FileName = _FileInfo.Name;

                if ((s.Length < 2) || (s[0].Length != 4 && s[1].Length != 2))
                {
                    Utils.TNMessageBoxOK("Lỗi: Định dạng tên file không đúng! \n Định dạng đúng là: yyyy_mm_Tenfile.xlsx",1);
                }
                else
                {
                    bool zcheck = Data_Access.CheckFileStatus(_FileInfo);
                    string Path = @"C:\Loi_Excel_Chua_Com_Cong_Ty_Import\";
                    //if (Directory.Exists(Path))
                    //{
                    //    Directory.Delete(Path);
                    //}
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác. Vui lòng đóng file để tải lên!", 2);
                    }
                    else
                    {
                        try
                        {
                            _DateLog = new DateTime(s[0].ToInt(), s[1].ToInt(), 1, 0, 0, 0);

                            using (Frm_Loading frm = new Frm_Loading(ProcessData)) { frm.ShowDialog(this); }

                            //string Path = @"C:\Loi_Excel_Chua_Import\";
                            if (Directory.Exists(Path))
                            {
                                Utils.TNMessageBoxOK("Có lỗi tập tin Excel, click OK để mở thư mục chứa !.",2);
                                Process.Start(Path);
                            }
                        }
                        catch (Exception ex)
                        {
                            Utils.TNMessageBoxOK(ex.ToString(),4);
                            _txtLog += "Lỗi.Xử lý Import" + Environment.NewLine;
                        }
                        if (_txtLog != "")
                        {
                            txtLog.Text = _txtLog;
                            txtLog.ScrollToCaret();
                        }
                    }
                }
            }
        }
        //-------------------------------------Help function process excel file
        void AddRowError(DataRow zRow, string Message)
        {
            string ztext = "";
            _CheckErr++;
            DataRow r = _TableErr.NewRow();
            r["No"] = zRow[0].ToString().Trim();
            r["MaNhom"] = zRow[1].ToString().Trim();
            r["TenNhom"] = zRow[2].ToString().Trim();
            r["ComTrua"] = zRow[3].ToString().Trim();
            r["ComTangCa"] = zRow[4].ToString().Trim();
            r["ComDonViNau"] = zRow[5].ToString().Trim();
            r["TangCaNgoai"] = zRow[6].ToString().Trim();
            r["ThongBao"] = Message;
            _TableErr.Rows.Add(r);
            ztext = Environment.NewLine;
            ztext += "Dòng                          :" + zRow[0].ToString().Trim() + Environment.NewLine;
            ztext += "Mã nhóm                       :" + zRow[1].ToString().Trim() + Environment.NewLine;
            ztext += "Tên nhóm                      :" + zRow[2].ToString().Trim() + Environment.NewLine;
            ztext += "Cơm trưa cty nấu              :" + zRow[3].ToString().Trim() + Environment.NewLine;
            ztext += "Cơm tăng ca  cty nấu          :" + zRow[4].ToString().Trim() + Environment.NewLine;
            ztext += "Cơm hỗ trợ đơn vị nấu         :" + zRow[5].ToString().Trim() + Environment.NewLine;
            ztext += "Tiền tăng ca cty không nấu    :" + zRow[6].ToString().Trim() + Environment.NewLine;
            ztext += "Lỗi        :" + Message;
            AddLog_Detail("--- " + ztext + " ---");
        }
        string CheckExcelRow(DataRow zRow, DateTime SheetDate)
        {
            string zMessage = "";
            string zTeamID = "";

            zTeamID = zRow[1].ToString().Trim();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeam.Key == 0)
            {
                zMessage += "\n Stt:" + (zRow[0].ToString().Trim()) + ";Lỗi!Mã nhóm:" + zTeamID + "không tồn tại";
            }
            return zMessage;
        }

        string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    // worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    // worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    //for (int j = 4; j <= zTable.Columns.Count; j++)
                    //{
                    //    worksheet.Column(j).Width = 15;
                    //}
                    //worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //worksheet.Column(1).Width = 5;
                    //worksheet.Column(2).Width = 30;
                    //worksheet.Column(3).Width = 15;
                    //worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    //for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    //{
                    //    for (int j = 4; j <= zTable.Columns.Count; j++)
                    //    {
                    //        worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //    }
                    //}
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }

        //-------------------------------------Main process excel data file
        void ProcessData()
        {
            #region[Log]
            _txtLog = "";
            txtLog.Text = "";
            string zStatus = "Khởi động Import Cơm công ty > tháng:" + _DateLog.ToString("MM/yyyy") + " >Tên file:" + _FileName; ;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, zStatus, 3);

            AddLog_Detail("/--- " + zStatus + " ---/");
            AddLog_Detail("/---Lúc " + DateTime.Now.ToString("HH: mm") + "---/");
            AddLog_Detail(Environment.NewLine);

            #endregion

            _txtLog += "---Bắt đầu : " + DateTime.Now.ToString("HH:mm") + " --//" + Environment.NewLine;

            ReadingExcel();
            //Load lại màn hình nếu cần
            _txtLog += "---Đã xong: " + DateTime.Now.ToString("HH:mm") + " --//" + Environment.NewLine;

            #region[Log]
            zStatus = "Kết thúc Import Cơm công ty > tháng:" + _DateLog.ToString("MM/yyyy") + " >Tên file:" + _FileName; ;

            AddLog_Detail("/--- " + zStatus + " ---/");
            AddLog_Detail("/---Lúc " + DateTime.Now.ToString("HH: mm") + "---/");
            Save_LogCaculator_Import(zStatus);
            #endregion
        }

        void ReadingExcel()
        {
            DataTable TableSheet = Data_Access.GetSheet(_FileInfo.FullName);
            for (int i = 0; i < TableSheet.Rows.Count; i++)
            {
                string SheetNo = TableSheet.Rows[i][0].ToString();
                //kiem tra nếu sheet là số thì thực hiện xử lý dữ liệu
                int isNumber = 0;
                if (int.TryParse(SheetNo, out isNumber))
                {
                    ReadingSheet(SheetNo);
                }
                else
                {
                    AddLog_Detail("--Sheet vi phạm nguyên tắc đặt tên.Bỏ qua không tính > Tên Sheet:[" + SheetNo + "]--");
                }
            }
        }
        void ReadingSheet(string SheetNo)
        {
            _txtLog += "//-------Đang đọc sheet " + SheetNo + " --------//" + Environment.NewLine;
            AddLog_Detail("//-------Đang đọc sheet " + SheetNo + " --------//");

            //--------------
            ExcelPackage package = new ExcelPackage(_FileInfo);
            DataTable TableData = Data_Access.GetTable(_FileInfo.FullName, SheetNo);
            package.Dispose();
            if (TableData.Rows.Count > 0)
                SaveDataSheet(TableData, SheetNo);
            else
            {
                _txtLog += "Sheet " + SheetNo + " không tìm có dữ liệu." + Environment.NewLine;
                AddLog_Detail("Sheet " + SheetNo + " không tìm có dữ liệu.");
            }
        }
        void SaveDataSheet(DataTable InTable, string SheetNo)
        {
            if (SheetNo.Length == 2)
            {
                string[] temp = _FileInfo.Name.Split('_');
                string year = temp[0].ToString();
                string month = temp[1].ToString();
                string day = SheetNo.ToString();
                int DayInMonth = System.DateTime.DaysInMonth(year.ToInt(), month.ToInt());
                if (day.ToInt() <= DayInMonth)
                {
                    DateTime zDate;
                    if (DateTime.TryParse(new DateTime(year.ToInt(), month.ToInt(), day.ToInt(), 0, 0, 0).ToString(), out zDate))
                    {
                        try
                        {
                            _txtLog += "Tổng dòng :" + InTable.Rows.Count + " " + Environment.NewLine;
                            AddLog_Detail("Tổng dòng :" + InTable.Rows.Count + "");

                            CreateTableError();
                            int zCountErr = 0;
                            int zCountSuccess = 0;
                            for (int i = 0; i < InTable.Rows.Count; i++)
                            {
                                DataRow zRow = InTable.Rows[i];
                                string MessageExcel = "";

                                AddLog_Detail("--Bắt đầu kiểm tra lỗi dòng:[" + i + "]--");

                                MessageExcel = CheckExcelRow(zRow, zDate);
                                if (MessageExcel == string.Empty)
                                {
                                    AddLog_Detail("--Kết thúc kiểm tra lỗi dòng:[" + i + "] > Tình trạng:Không lỗi >Bắt đầu lưu SQL--");
                                    string MessageSQL = InsertToSQL(zRow, zDate);
                                    if (MessageSQL != string.Empty)
                                    {
                                        AddRowError(zRow, MessageSQL);
                                        zCountErr++;
                                    }
                                    else
                                    {
                                        zCountSuccess++;
                                    }
                                }
                                else
                                {
                                    AddLog_Detail("--Kết thúc kiểm tra lỗi dòng:[" + i + "] > Tình trạng:Lỗi > Bắt đầu lưu lỗi--");
                                    AddRowError(zRow, MessageExcel);
                                    zCountErr++;
                                }
                            }
                            _txtLog += "Thành công : " + zCountSuccess.ToString() + " " + Environment.NewLine;
                            _txtLog += "Bị lỗi : " + zCountErr + " " + Environment.NewLine;

                            AddLog_Detail("--Thành công/Lỗi: " + zCountSuccess.ToString() + "/" + zCountErr + "--");

                            ExportTableError(zDate);
                        }
                        catch (Exception Ex)
                        {
                            _txtLog += "Lỗi.Vui lòng liên hệ IT." + Environment.NewLine;
                            AddLog_Detail("Lỗi.Vui lòng liên hệ IT > Chi tiết:" + Ex.ToString());
                        }
                    }
                }
                else
                {
                    _txtLog += "Ngày [ " + day + " ]không có trong tháng tải lên!" + Environment.NewLine;
                    AddLog_Detail("Ngày [ " + day + " ]không có trong tháng tải lên!");
                }
            }
            else
            {
                _txtLog += "Sheet " + SheetNo + "> 2 kí tự không tải lên " + Environment.NewLine;
                AddLog_Detail("Sheet " + SheetNo + " > 2 kí tự không tải lên ");
            }
        }

        string InsertToSQL(DataRow zRow, DateTime SheetDate)
        {
            float zDonGiaComCty = 0;
            zDonGiaComCty = RiceTeamCompany_Data.DonGiaComCty(SheetDate);

            string zTeamID = zRow[1].ToString().Trim();
            RiceTeamCompany_Info zKeeping = new RiceTeamCompany_Info(zTeamID, SheetDate);
            if (zKeeping.Key == 0)
            {
                AddLog_Detail("Dữ liệu thêm mới");
            }
            else
            {
                AddLog_Detail("Dữ liệu cập nhật");
            }

            zKeeping.DateWrite = SheetDate;
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            zKeeping.TeamKey = zTeam.Key;
            zKeeping.TeamID = zTeam.TeamID;
            zKeeping.TeamName = zTeam.TeamName;
            zKeeping.BranchKey = zTeam.BranchKey;
            zKeeping.DepartmentKey = zTeam.DepartmentKey;

            zKeeping.Rice1 = zRow[3].ToInt();
            zKeeping.Price1 = zDonGiaComCty;
            double zMoney1 = Math.Round(zRow[3].ToInt() * zDonGiaComCty, 0);
            zKeeping.Money1 = float.Parse(zMoney1.ToString());

            zKeeping.Rice2 = zRow[4].ToInt();
            zKeeping.Price2 = zDonGiaComCty;
            double zMoney2 = Math.Round(zRow[4].ToInt() * zDonGiaComCty, 0);
            zKeeping.Money2 = float.Parse(zMoney2.ToString());

            float zComDonViNau = 0;
            if (float.TryParse(zRow[5].ToString().Trim(), out zComDonViNau))
            {

            }
            zKeeping.Money3 = zComDonViNau;

            float zTangCaNgoai = 0;
            if (float.TryParse(zRow[6].ToString().Trim(), out zTangCaNgoai))
            {

            }
            zKeeping.Money4 = zTangCaNgoai;

            if (zRow[7].ToString().Trim() != null)
                zKeeping.Description = zRow[7].ToString().Trim();

            zKeeping.CreatedBy = SessionUser.UserLogin.Key;
            zKeeping.CreatedName = SessionUser.UserLogin.EmployeeName;
            zKeeping.ModifiedBy = SessionUser.UserLogin.Key;
            zKeeping.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zKeeping.Save();

            string text = "--Dữ liệu chấm công chuẩn bị--" + Environment.NewLine;
            text += "Key                      :" + zKeeping.Key + Environment.NewLine;
            text += "Ngày                     :" + SheetDate + Environment.NewLine;
            text += "Key nhóm                 :" + zKeeping.TeamKey + Environment.NewLine;
            text += "Mã nhóm                  :" + zKeeping.TeamID + Environment.NewLine;
            text += "Tên nhóm                 :" + zKeeping.TeamName + Environment.NewLine;
            text += "Số phần Cơm trưa         :" + zKeeping.Rice1 + Environment.NewLine;
            text += "Đơn giá cơm trưa         :" + zKeeping.Price1 + Environment.NewLine;
            text += "Số tiền cơm trưa         :" + zKeeping.Money1 + Environment.NewLine;
            text += "Số phần Tăng ca cty      :" + zKeeping.Rice2 + Environment.NewLine;
            text += "Đơn giá Tăng ca cty      :" + zKeeping.Price2 + Environment.NewLine;
            text += "Số tiền Tăng ca cty      :" + zKeeping.Money2 + Environment.NewLine;
            text += "Số tiền hỗ trợ đơn vị nấu:" + zKeeping.Money3 + Environment.NewLine;
            text += "Số tiền Tăng ca ngoài    :" + zKeeping.Money4 + Environment.NewLine;
            text += "Key người tạo            :" + zKeeping.CreatedBy + Environment.NewLine;
            text += "Tên người tạo            :" + zKeeping.CreatedName + Environment.NewLine;
            text += "Key người sửa            :" + zKeeping.ModifiedBy + Environment.NewLine;
            text += "Tên người sửa            :" + zKeeping.ModifiedName;

            AddLog_Detail(text);

            string zError = "";
            if (zKeeping.Message == "")
            {
                AddLog_Detail("--Lưu bảng cơm công ty thành công--");
                AddLog_Detail("--//---------------------------//--");
                _Log_Detail += Environment.NewLine;
                return zError;
            }
            else
            {
                AddLog_Detail("--Lưu bảng cơm công ty Lỗi:" + zKeeping.Message);
                AddLog_Detail("--//---------------------------//--");
                _Log_Detail += Environment.NewLine;
                return zError += zKeeping.Message;
            }

        }
        void ExportTableError(DateTime SheetDate)
        {
            string Path = @"C:\Loi_Excel_Com_Cong_Ty_Chua_Import\";
            string FileName = SheetDate.ToString("yyyy_MM_dd") + "_Chưa_Import.xlsx";
            if (_TableErr.Rows.Count > 0)
            {

                if (!Directory.Exists(Path))
                {
                    Directory.CreateDirectory(Path);
                }
                Path += FileName;
                ExportTableToExcel(_TableErr, Path);
                _txtLog += "Xuất file Excel ngày [ " + SheetDate.ToString("dd") + " ] - " + _TableErr.Rows.Count + " lỗi" + Environment.NewLine;
            }
            else
            {
                //if (Directory.Exists(Path))
                //{
                //Directory.Delete(Path);
                //}
                var newFile = new FileInfo(Path + FileName);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        _txtLog += "Tệp tin " + FileName + " đang sử dụng không thể xóa! " + Environment.NewLine;
                    }
                }

            }
        }

        //-------------------------------------Main process SQL data

        void AddLog_Detail(string Text)
        {
            _Log_Detail += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss.fff") + " " + Text + Environment.NewLine;
            txt_Log_Detail.ScrollToCaret();
        }
        void Save_LogCaculator_Import(string ActionName)
        {
            Application_Info zinfo = new Application_Info();
            zinfo.ActionName = ActionName;
            zinfo.DateLog = _DateLog;
            zinfo.ActionType = 3;
            zinfo.SubDescription = txtLog.Text;
            zinfo.Description = _Log_Detail;
            zinfo.Form = 6;//Dành cơm công ty
            zinfo.FormName = "Form " + HeaderControl.Text;
            zinfo.UserKey = SessionUser.UserLogin.Key;
            zinfo.EmployeeKey = SessionUser.UserLogin.EmployeeKey;
            zinfo.CreatedBy = SessionUser.UserLogin.Key;
            zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zinfo.ModifiedBy = SessionUser.UserLogin.Key;
            zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zinfo.Create();
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        Access_Role_Info Role;
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Import.Enabled = false;
            }

        }
        #endregion
    }
}
