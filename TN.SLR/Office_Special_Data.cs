﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TN.SLR
{
    public class Office_Special_Data
    {        //Load danh sách báo cáo lương bộ phận sản xuất
        //Load danh sách báo cáo lương bộ phận sản xuất
        public static DataTable ListCodeReportOffice()
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.AutoKey, A.ID,A.Name FROM [dbo].[SLR_CodeReportOffice] A 
WHERE A.RecordStatus <>99
ORDER BY A.Rank";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable DanhSachKetThucThuViec(DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            //---------- String SQL Access Database ---------------
            string zSQL = @"
--Nhân viên thử việc
--declare @FromDate datetime='2020-06-01 00:00:00'
--declare @ToDate datetime='2020-06-30 23:59:59'

CREATE TABLE #Tam
(
EmployeeKey nvarchar(50),
EmployeeID nvarchar(50),
EmployeeName nvarchar(500),
BatDau datetime,
HetThuViec datetime,
NghiViec datetime
)
INSERT INTO #Tam (EmployeeKey,EmployeeID,EmployeeName,BatDau,HetThuViec,NghiViec)
SELECT *  FROM 
		(
			SELECT EmployeeKey,EmployeeID,[dbo].[Fn_GetFullName](EmployeeKey) AS EmployeeName,
			StartingDate, LeavingTryDate,
			CASE
				WHEN LeavingDate IS NULL THEN GETDATE()
				ELSE LeavingDate
			END LeavingDate
			FROM  [dbo].[HRM_Employee] WHERE [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=2
			
		) X
		WHERE( (StartingDate <=@FromDate AND @FromDate <=  LeavingDate  )
		OR	  (StartingDate <=@ToDate   AND @ToDate   <=  LeavingDate ) )
		AND  LeavingTryDate Between @FromDate AND @ToDate

SELECT* FROM #Tam 
DROP TABLE #Tam";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }

        public static DataTable DanhSachTangLuong(DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            //---------- String SQL Access Database ---------------
            string zSQL = @"
--Nhân viên thử việc
--declare @FromDate datetime='2020-06-01 00:00:00'
--declare @ToDate datetime='2020-06-30 23:59:59'

CREATE TABLE #Tam
(
EmployeeKey nvarchar(50),
MaDanhMuc Nvarchar(50),
BatDau datetime,
HetHan datetime,
SoTien money
)

INSERT INTO #Tam (EmployeeKey,MaDanhMuc,BatDau,HetHan,SoTien)
SELECT *  FROM 
		(
			SELECT EmployeeKey,CategoryID,
			FromDate AS StartDate,
			CASE
				WHEN ToDate IS NULL THEN GETDATE()
				ELSE ToDate
			END LeavingDate,
			[Value] AS SoTien
			FROM  [dbo].[HRM_SalaryDefault] WHERE [dbo].[Fn_BranchKeyWorkingHistory](EmployeeKey,@FromDate,@ToDate)=2
			AND (CategoryID='MLC' OR CategoryID='LHQ' OR CategoryID='HTX')
			
		) X
		WHERE (StartDate <=@FromDate AND @FromDate <=  LeavingDate  )
		OR	  (StartDate <=@ToDate   AND @ToDate   <=  LeavingDate ) 

SELECT EmployeeKey,MaDanhMuc,HetHan,SoTien FROM #Tam WHERE  HetHan Between @FromDate AND @ToDate
GROUP BY EmployeeKey,MaDanhMuc,HetHan,SoTien
DROP TABLE #Tam";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }


    }
}
