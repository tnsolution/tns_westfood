﻿using C1.Win.C1FlexGrid;
using OfficeOpenXml;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
using ReportAll = TNS.CORE.Report;

namespace TNS.WinApp
{
    public partial class Frm_Report_1 : Form
    {
        string _KQ = "";
        int _TeamKey = 0;
        public Frm_Report_1()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            btn_Export.Click += btn_Export_Click;
            btn_Search.Click += btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;


            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"
SELECT A.TeamKey, A.TeamID +'-'+ A.TeamName AS TeamName  
FROM SYS_Team A
LEFT JOIN [dbo].[SYS_Department] B ON B.DepartmentKey=A.DepartmentKey
LEFT JOIN [dbo].[SYS_Branch] C ON C.BranchKey=B.BranchKey
WHERE A.RecordStatus <> 99 
AND   A.BranchKey = 4 
AND   A.DepartmentKey != 26
AND   A.TeamKey !=98 
ORDER BY C.RANK,B.RANK,A.RANK";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "--- Tất cả---");
        }

        void Frm_Report_1_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void InitGV_Layout(DataTable Table)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int ToTalCol = Table.Columns.Count - 2;
            GVData.Cols.Add(ToTalCol+1);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            int nCol = 3;
            for (int i = 5; i < Table.Columns.Count; i++)
            {
                GVData.Rows[0][nCol] = "Ngày " + Table.Columns[i].ColumnName;
                nCol++;
            }
            GVData.Rows[0][ToTalCol] = "TỔNG CỘNG";

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Cols[0].Width = 40;
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].Width = 200;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].Width = 130;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 3; i <= ToTalCol; i++)
            {
               
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            GVData.Cols[ToTalCol].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol].Width = 120;
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số công nhân " + Table.Select("TeamKey='" + TeamKey+"'").Length;
            int nCol = 3;
            double zTotal = 0;
            for (int i = 5; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "TeamKey='" + TeamKey+"'").Toe0String();
                double zTemp = 0;
                if (double.TryParse(RowView[nCol].ToString(), out zTemp))
                {

                }
                zTotal += zTemp;
                nCol++;
            }
            zTotal = Math.Round(zTotal, 0);
            RowView[nCol] = zTotal.Toe0String();
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[1] = rDetail["EmployeeName"].ToString().Trim();
            RowView[2] = rDetail["EmployeeID"].ToString().Trim();
            double zTotalRow = 0;
            int nCol = 3;
            for (int i = 5; i < rDetail.ItemArray.Length; i++)
            {
                RowView[nCol]= rDetail[i].Toe0String();
                double zTemp = 0;
                if (double.TryParse(rDetail[i].ToString(), out zTemp))
                {

                }
                zTotalRow += zTemp;
                nCol++;
            }
            //double zTotalRow = 0;
            //for (int i = 5; i < rDetail.ItemArray.Length; i++)
            //{
            //    zTotalRow += rDetail[i].ToDouble();
            //}
            zTotalRow = Math.Round(zTotalRow,0);
            RowView[nCol] = zTotalRow.Toe0String();
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số công nhân " + Table.Rows.Count;
            double zTotal = 0;
            int nCol = 3;
            for (int i = 5; i < Table.Columns.Count; i++)
            {
                RowView[nCol] = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "").Toe0String();
                double zTemp = 0;
                if (double.TryParse(RowView[nCol].ToString(), out zTemp))
                {

                }
                zTotal += zTemp;
                nCol++;
            }
            zTotal = Math.Round(zTotal,0);
            RowView[nCol] = zTotal.Toe0String(); ;
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private int _STT = 1;
        private void DisplayData()
        {
            DateTime FromDate = dte_FromDate.Value;
            DateTime ToDate = dte_ToDate.Value;
            DataTable _Intable = ReportAll.No1_V4(FromDate, ToDate, _TeamKey,txt_Search.Text.Trim());
            if (_Intable.Rows.Count > 0)
            {
                //Sắp xếp
                _Intable.Columns.Add("EmployeeIDRank");
                for (int i = 0; i < _Intable.Rows.Count; i++)
                {
                    _Intable.Rows[i]["EmployeeIDRank"] = ConvertIDRank(_Intable.Rows[i][3].ToString());

                }
                DataView dv = _Intable.DefaultView;
                dv.Sort = "BranchRank ASC,DepartmentRank ASC, TeamRank ASC , EmployeeIDRank ASC";
                _Intable = dv.ToTable();
                _Intable.Columns.Remove("BranchRank");
                _Intable.Columns.Remove("DepartmentRank");
                _Intable.Columns.Remove("TeamRank");
                _Intable.Columns.Remove("EmployeeIDRank");
                //--sắp xếp xong xóa cột sắp xếp đi

                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);
                    _STT = 1;
                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);
                    
                }));
            }
        }


        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            //chèn số thấp tới cao--> tới chữ
            string zResult = "";
            string s = "";
            string temp = "";
            if (ID.Substring(0, 1) == "A")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "9";
                }
                zResult = s + temp;
            }
            else if (ID.Substring(0, 1) == "H" || ID.Substring(0, 1) == "L")
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "B";
                }
                zResult = s + temp;
            }
            else
            {
                temp = ID;
                for (int i = 0; i < 8 - ID.Trim().Length; i++)
                {
                    s += "0";
                }
                zResult = s + temp;
            }
            return zResult;
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK( "Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }
            _TeamKey = cboTeam.SelectedValue.ToInt();

            string Status = "Tìm DL form "+HeaderControl.Text+" > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy")  + " > Nhóm:" + cboTeam.Text + " > Từ khóa:" + txt_Search.Text;
             Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
               Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cboTeam.SelectedValue.ToInt() != 0)
            {
                string[] s = cboTeam.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Lương_Khoán_Năng_Suất_Ngày" + zTeamName + "_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 30;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 15;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 15;
                    worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        for (int j = 4; j <= zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}