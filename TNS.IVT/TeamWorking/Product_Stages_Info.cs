﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.WinApp
{
    public class Product_Stages_Info
    {

        #region [ Field Name ]
        private int _IsWorkTime = 0;
        private int _GroupKey = 0;
        private string _GroupID = "";
        private int _StageKey = 0;
        private string _StageName = "";
        private string _StagesID = "";
        private int _BasicUnit = 0;
        private string _UnitName = "";
        private float _Price;
        private int _TeamKey = 0;
        private int _Slug = 0;       
        private int _RecordStatus = 0;
        private string _Description = "";
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private DateTime _ModifiedOn;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        //public bool IsCommandOK
        //{
        //    get
        //    {
        //        int zNumber = 0;
        //        int.TryParse(_Message.Substring(0, 1), out zNumber);
        //        if (zNumber <= 3) return true; else return false;
        //    }
        //}
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _StageKey; }
            set { _StageKey = value; }
        }
        public string StageName
        {
            get { return _StageName; }
            set { _StageName = value; }
        }
        public string StagesID
        {
            get { return _StagesID; }
            set { _StagesID = value; }
        }
        public int BasicUnit
        {
            get { return _BasicUnit; }
            set { _BasicUnit = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public float Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public int Slug
        {
            get { return _Slug; }
            set { _Slug = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int IsWorkTime
        {
            get
            {
                return _IsWorkTime;
            }

            set
            {
                _IsWorkTime = value;
            }
        }

        public int GroupKey
        {
            get
            {
                return _GroupKey;
            }

            set
            {
                _GroupKey = value;
            }
        }

        public string GroupID
        {
            get
            {
                return _GroupID;
            }

            set
            {
                _GroupID = value;
            }
        }

        public string Description
        {
            get
            {
                return _Description;
            }

            set
            {
                _Description = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Product_Stages_Info()
        {
        }
        public Product_Stages_Info(int StageKey)
        {
            string zSQL = "SELECT A.*, B.GroupID FROM IVT_Product_Stages A LEFT JOIN IVT_Product_Stages_Group B ON A.GroupKey = B.GroupKey WHERE StageKey = @StageKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = StageKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["GroupKey"] != DBNull.Value)
                        _GroupKey = int.Parse(zReader["GroupKey"].ToString());
                    _GroupID = zReader["GroupID"].ToString().Trim();
                    if (zReader["StageKey"] != DBNull.Value)
                        _StageKey = int.Parse(zReader["StageKey"].ToString());
                    _StageName = zReader["StageName"].ToString().Trim();
                    _StagesID = zReader["StagesID"].ToString().Trim();
                    if (zReader["BasicUnit"] != DBNull.Value)
                        _BasicUnit = int.Parse(zReader["BasicUnit"].ToString());
                    _UnitName = zReader["UnitName"].ToString().Trim();
                    if (zReader["Price"] != DBNull.Value)
                        _Price = float.Parse(zReader["Price"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Product_Stages_Info(string StageName)
        {
            string zSQL = "SELECT A.*, B.GroupID FROM IVT_Product_Stages A LEFT JOIN IVT_Product_Stages_Group B ON A.GroupKey = B.GroupKey WHERE StageName = @StageName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@StageName", SqlDbType.NVarChar).Value = StageName;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["GroupKey"] != DBNull.Value)
                        _GroupKey = int.Parse(zReader["GroupKey"].ToString());
                    _GroupID = zReader["GroupID"].ToString().Trim();
                    if (zReader["StageKey"] != DBNull.Value)
                        _StageKey = int.Parse(zReader["StageKey"].ToString());
                    _StageName = zReader["StageName"].ToString().Trim();
                    _StagesID = zReader["StagesID"].ToString().Trim();
                    if (zReader["BasicUnit"] != DBNull.Value)
                        _BasicUnit = int.Parse(zReader["BasicUnit"].ToString());
                    _UnitName = zReader["UnitName"].ToString().Trim();
                    if (zReader["Price"] != DBNull.Value)
                        _Price = float.Parse(zReader["Price"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public void GetStagesID_Info(string StagesID)
        {
            string zSQL = "SELECT A.*, B.GroupID FROM IVT_Product_Stages A LEFT JOIN IVT_Product_Stages_Group B ON A.GroupKey = B.GroupKey WHERE StagesID = @StagesID AND A.RecordStatus <99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@StagesID", SqlDbType.NVarChar).Value = StagesID;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["GroupKey"] != DBNull.Value)
                        _GroupKey = int.Parse(zReader["GroupKey"].ToString());
                    _GroupID = zReader["GroupID"].ToString().Trim();
                    if (zReader["StageKey"] != DBNull.Value)
                        _StageKey = int.Parse(zReader["StageKey"].ToString());
                    _StageName = zReader["StageName"].ToString().Trim();
                    _StagesID = zReader["StagesID"].ToString().Trim();
                    if (zReader["BasicUnit"] != DBNull.Value)
                        _BasicUnit = int.Parse(zReader["BasicUnit"].ToString());
                    _UnitName = zReader["UnitName"].ToString().Trim();
                    if (zReader["Price"] != DBNull.Value)
                        _Price = float.Parse(zReader["Price"].ToString());
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    _Description = zReader["Description"].ToString().Trim();
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_Product_Stages ("
        + " StageName ,StagesID ,BasicUnit ,UnitName ,Price ,TeamKey ,Slug, IsWorkTime, GroupKey ,Description ,RecordStatus ,CreatedBy ,CreatedName ,CreatedOn ,ModifiedBy ,ModifiedName ,ModifiedOn ) "
         + " VALUES ( "
         + "@StageName ,@StagesID ,@BasicUnit ,@UnitName ,@Price ,@TeamKey ,@Slug, @IsWorkTime, @GroupKey ,@Description ,0 ,@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ,GETDATE()) "
         + " SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@StageName", SqlDbType.NVarChar).Value = _StageName.Trim();
                zCommand.Parameters.Add("@StagesID", SqlDbType.NVarChar).Value = _StagesID.Trim();
                zCommand.Parameters.Add("@BasicUnit", SqlDbType.Int).Value = _BasicUnit;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName.Trim();
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = _Price;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@IsWorkTime", SqlDbType.Int).Value = IsWorkTime;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = GroupKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE IVT_Product_Stages SET "
                        + " StageName = @StageName,"
                        + " StagesID = @StagesID,"
                        + " BasicUnit = @BasicUnit,"
                        + " UnitName = @UnitName,"
                        + " Price = @Price,"
                        + " TeamKey = @TeamKey,"
                        + " Slug = @Slug,"
                        + " GroupKey =@GroupKey,"
                        + " IsWorkTime = @IsWorkTime,"
                        + " Description = @Description,"
                        + " RecordStatus = 1,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ModifiedOn = GETDATE()"
                       + " WHERE StageKey = @StageKey"
                       + " SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@StageName", SqlDbType.NVarChar).Value = _StageName.Trim();
                zCommand.Parameters.Add("@StagesID", SqlDbType.NVarChar).Value = _StagesID.Trim();
                zCommand.Parameters.Add("@BasicUnit", SqlDbType.Int).Value = _BasicUnit;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName.Trim();
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = _Price;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@IsWorkTime", SqlDbType.Int).Value = IsWorkTime;
                zCommand.Parameters.Add("@GroupKey", SqlDbType.Int).Value = GroupKey;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Product_Stages SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE StageKey = @StageKey";
            zSQL += " SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@StageKey", SqlDbType.Int).Value = _StageKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Delete(int TeamKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_Product_Stages SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE TeamKey = @TeamKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                _Message = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_StageKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
