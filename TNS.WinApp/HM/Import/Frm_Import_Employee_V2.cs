﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TN.Library.System;
using TNS.HRM;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Import_Employee_V2 : Form
    {
        bool _IsSaved = false;//kiem tra đã lưu hay chưa
        FileInfo FileInfo;
        DataTable _GVEmployee;
        private Employee_Object _Emp_Obj;
        int zError = 0;
        int zSuccess = 0;
        int _IndexGV = 0;
        int _TotalGV = 0;
        public Frm_Import_Employee_V2()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Open.Click += Btn_Open_Click;
            btn_SaveAll.Click += Btn_SaveAll_Click;
            GV_Employee.CellEndEdit += GV_Employee_CellEndEdit;
            GV_Employee.KeyDown += GV_Employee_KeyDown;
            btnCreate.Click += BtnCreate_Click;

            timer1.Tick += Timer1_Tick;
            timer2.Tick += Timer2_Tick;
            timer3.Tick += Timer3_Tick;
            timer4.Tick += Timer4_Tick;
            timer5.Tick += Timer5_Tick;
        }



        private void Frm_Import_Employee_V2_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);


            PicLoading.Visible = false;
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Employee, true);
            Utils.DrawGVStyle(ref GV_Employee);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

        }
        private void Btn_SaveAll_Click(object sender, EventArgs e)
        {
            if (GV_Employee.Rows.Count <= 0)
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu!",1);
                return;
            }
            PicLoading.Visible = true;
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            _TotalGV = GV_Employee.Rows.Count;
            txt_Sql.Text = "0";
            timer1.Start();

        }
        private void BtnCreate_Click(object sender, EventArgs e)
        {
            if (GV_Employee.Rows.Count <= 0)
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu!",1);
                return;
            }
            PicLoading.Visible = true;
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            _TotalGV = GV_Employee.Rows.Count;
            txt_Sql.Text = "0";
            timer4.Start();
        }
        private void Btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                DefaultExt = "xlsx",
                Title = "Chọn tập tin Excel",
                Filter = "All Files|*.*",
                FilterIndex = 2,
                ReadOnlyChecked = true,
                CheckFileExists = true,
                CheckPathExists = true,
                RestoreDirectory = true,
                ShowReadOnly = true
            };

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                GV_Employee.Rows.Clear();

                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!", 2);
                }
                else
                {
                    try
                    {
                        lblFileName.Text = FileInfo.Name;
                        //Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);
                        GV_Employee_Layout();
                        _GVEmployee = new DataTable();
                        _GVEmployee = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        GV_Employee_LoadData();
                        _IsSaved = false;
                        //  Cursor.Current = Cursors.Default;

                    }
                    catch (Exception ex)
                    {
                        Utils.TNMessageBoxOK(ex.ToString(),4);
                    }
                }
            }
        }


        #region [GV_Employee]
        private void GV_Employee_Layout()
        {
            // Setup Column 
            GV_Employee.Columns.Add("No", "STT");
            GV_Employee.Columns.Add("EmployeeID", "Mã NV");
            GV_Employee.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV_Employee.Columns.Add("IssueID", "CMND");
            GV_Employee.Columns.Add("BirthDay", "Ngày sinh");
            GV_Employee.Columns.Add("Gender", "Giới tính");
            GV_Employee.Columns.Add("IssueDate", "Ngày cấp");
            GV_Employee.Columns.Add("IssuePlace", "Nơi cấp");
            GV_Employee.Columns.Add("HomeTown", "Nguyên quán");
            GV_Employee.Columns.Add("Branch", "Mã khối");
            GV_Employee.Columns.Add("Department", "Mã Bộ phận");
            GV_Employee.Columns.Add("Team", "Mã nhóm");
            GV_Employee.Columns.Add("Position", "Mã chức vụ");

            GV_Employee.Columns.Add("Email", "Email");
            GV_Employee.Columns.Add("Phone", "Số điện thoại");
            GV_Employee.Columns.Add("ReportToID", "Mã người quản lý");
            GV_Employee.Columns.Add("ReportToName", "Tên người quản lý");
            GV_Employee.Columns.Add("WorkingStatus", "Tình trạng");

            GV_Employee.Columns.Add("BankCode", "ATM");
            GV_Employee.Columns.Add("AddressBank", "Ngân hàng");
            GV_Employee.Columns.Add("Taxcode", "Mã số thuế");
            //GV_Employee.Columns.Add("SalaryBasic", "Lương cơ bản");
            //GV_Employee.Columns.Add("SalaryGood", "Lương hiệu quả");
            GV_Employee.Columns.Add("AccountCode", "Tài khoản kế toán 1");
            GV_Employee.Columns.Add("AccountCode2", "Tài khoản kế toán 2");
            GV_Employee.Columns.Add("Overtime", "Có tính lương ngoài giờ");

            GV_Employee.Columns.Add("StartingDate", "Ngày bắt đầu làm");
            GV_Employee.Columns.Add("LeavingDate", "Ngày nghỉ việc");
            GV_Employee.Columns.Add("Description", "Ghi chú");

            GV_Employee.Columns.Add("Message", "Thông báo");

            GV_Employee.Columns["No"].Width = 40;
            GV_Employee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Employee.Columns["No"].ReadOnly = true;
            GV_Employee.Columns["No"].Frozen = true;

            GV_Employee.Columns["EmployeeName"].Width = 160;
            GV_Employee.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeName"].ReadOnly = true;
            GV_Employee.Columns["EmployeeName"].Frozen = true;

            GV_Employee.Columns["EmployeeID"].Width = 100;
            GV_Employee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Employee.Columns["EmployeeID"].Frozen = true;

            GV_Employee.Columns["HomeTown"].Width = 400;
            GV_Employee.Columns["HomeTown"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV_Employee.Columns["ReportToName"].Width = 160;
            GV_Employee.Columns["ReportToName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["AccountCode"].Width = 100;
            GV_Employee.Columns["AccountCode"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            GV_Employee.Columns["AccountCode2"].Width = 100;
            GV_Employee.Columns["AccountCode2"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV_Employee.Columns["Overtime"].Width = 100;
            GV_Employee.Columns["Overtime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Employee.Columns["AddressBank"].Width = 160;
            GV_Employee.Columns["AddressBank"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;


            GV_Employee.ColumnHeadersHeight = 50;
            //GV_Employee.AllowUserToResizeRows = false;
            //GV_Employee.AllowUserToResizeColumns = true;

            //GV_Employee.AllowUserToDeleteRows = false;
            GV_Employee.Rows.Clear();

        }
        private void GV_Employee_LoadData()
        {
            PicLoading.Visible = true;
            GV_Employee.Rows.Clear();
            _IndexGV = 0;
            _TotalGV = _GVEmployee.Rows.Count;
            timer3.Start();
        }

        private void GV_Employee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //CheckRow(GV_Employee.Rows[e.RowIndex]);
            CheckRowCreate(GV_Employee.Rows[e.RowIndex]);
        }

        private void GV_Employee_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if(Utils.TNMessageBox("Bạn có xóa thông tin này ?",2)=="Y")
                {
                    GV_Employee.CurrentRow.Visible = false;
                }
            }
        }
        #endregion

        #region[Code chỉ tạo mới]
        private string CheckRowCreate(DataGridViewRow zRow)
        {
            string Message = "";
            if (zRow.Cells["EmployeeName"].Value == null || zRow.Cells["EmployeeName"].Value.ToString() == "")
            {
                Message += "Tên nhân viên không bỏ trống /";
            }
            if (zRow.Cells["EmployeeID"].Value == null || zRow.Cells["EmployeeID"].Value.ToString() == "")
            {
                Message += "Mã nhân viên không bỏ trống /";
            }
            if (zRow.Cells["EmployeeID"].Value.ToString() != "")
            {
                int Count = Employee_Data.Check_CardID(zRow.Cells["EmployeeID"].Value.ToString());
                if (Count > 0)
                {
                    Message += "Mã nhân viên này đã có trong danh sách /";
                }
            }
            if (zRow.Cells["Gender"].Value == null || zRow.Cells["Gender"].Value.ToString() == "")
            {
                Message += "Giói tính không hợp lệ /";
            }
            else
            {
                if (zRow.Cells["Gender"].Value.ToString() != "Nam" && zRow.Cells["Gender"].Value.ToString() != "Nữ")
                {
                    Message += "Giói tính không hợp lệ /";
                }
            }
            if (zRow.Cells["BirthDay"].Value == null || zRow.Cells["BirthDay"].Value.ToString() == "")
            {
                Message += "Ngày sinh không hợp lệ /";
            }
            else if (zRow.Cells["BirthDay"].Value != null || zRow.Cells["BirthDay"].Value.ToString() != "")
            {
                DateTime zBirthDay = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["BirthDay"].Value.ToString(), out zBirthDay))
                {
                    Message += "Ngày sinh không hợp lệ /";
                }
            }
            if (zRow.Cells["IssueID"].Value == null || zRow.Cells["IssueID"].Value.ToString() == "" || zRow.Cells["IssueID"].Value.ToString().Trim().Length < 9)
            {
                Message += "CMND không hợp lệ /";
            }
            if (zRow.Cells["IssueDate"].Value == null || zRow.Cells["IssueDate"].Value.ToString() == "")
            {
                Message += "Ngày cấp không hợp lệ /";
            }
            else if (zRow.Cells["IssueDate"].Value.ToString() != "")
            {
                DateTime zIsueDate = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["IssueDate"].Value.ToString(), out zIsueDate))
                {
                    Message += "Ngày cấp không hợp lệ /";
                }
            }
            if (zRow.Cells["IssuePlace"].Value == null)
            {
                zRow.Cells["IssuePlace"].Value = "";
            }
            if (zRow.Cells["HomeTown"].Value == null)
            {
                zRow.Cells["HomeTown"].Value = "";
            }
            if (zRow.Cells["Branch"].Value == null || zRow.Cells["Branch"].Value.ToString() == "")
            {
                Message += "Khối không hợp lệ /";
            }
            else
            {
                Branch_Info zinfo = new Branch_Info();
                zinfo.Get_Branch_ID((zRow.Cells["Branch"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Khối không hợp lệ /";
                }
            }
            if (zRow.Cells["Department"].Value == null || zRow.Cells["Department"].Value.ToString() == "")
            {
                Message += "Phòng ban không hợp lệ /";
            }
            else
            {
                Department_Info zinfo = new Department_Info();
                zinfo.Get_Department_ID((zRow.Cells["Department"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Phòng ban không hợp lệ /";
                }
            }
            if (zRow.Cells["Team"].Value == null)
            {
                zRow.Cells["Team"].Value = "";
            }
            else if (zRow.Cells["Team"].Value.ToString() != "")
            {
                Team_Info zinfo = new Team_Info();
                zinfo.Get_Team_ID((zRow.Cells["Team"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Tổ nhóm không hợp lệ /";
                }
            }
            if (zRow.Cells["Position"].Value == null || zRow.Cells["Position"].Value.ToString() == "")
            {
                Message += "Chức vụ không hợp lệ /";
            }
            else
            {
                Position_Info zinfo = new Position_Info();
                zinfo.Get_Position_ID((zRow.Cells["Position"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Chức vụ không hợp lệ /";
                }
            }
            //
            if (zRow.Cells["Email"].Value == null)
            {
                zRow.Cells["Email"].Value = "";
            }
            if (zRow.Cells["Phone"].Value == null)
            {
                zRow.Cells["Phone"].Value = "";
            }
            if (zRow.Cells["ReportToID"].Value == null)
            {
                zRow.Cells["ReportToID"].Value = "";
            }
            else if (zRow.Cells["ReportToID"].Value.ToString() != "")
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployeeID((zRow.Cells["ReportToID"].Value.ToString().Trim()));
                if (zinfo.Key == "")
                {
                    Message += "Mã người quản lý không hợp lệ /";
                }
            }

            if (zRow.Cells["WorkingStatus"].Value == null || zRow.Cells["WorkingStatus"].Value.ToString() == "")
            {
                Message += "Tình trạng không hợp lệ /";
            }
            else
            {
                if (zRow.Cells["WorkingStatus"].Value.ToString() != "Đang làm việc" && zRow.Cells["WorkingStatus"].Value.ToString() != "Nghỉ việc")
                {
                    Message += "Tình trạng không hợp lệ /";
                }
            }
            if (zRow.Cells["BankCode"].Value == null)
            {
                zRow.Cells["BankCode"].Value = "";
            }
            if (zRow.Cells["AddressBank"].Value == null)
            {
                zRow.Cells["AddressBank"].Value = "";
            }
            if (zRow.Cells["Taxcode"].Value == null)
            {
                zRow.Cells["Taxcode"].Value = "";
            }
            //if (zRow.Cells["SalaryBasic"].Value == null || zRow.Cells["SalaryBasic"].Value.ToString() == "")
            //{
            //    zRow.Cells["SalaryBasic"].Value = "0";
            //}
            //if (zRow.Cells["SalaryGood"].Value == null || zRow.Cells["SalaryGood"].Value.ToString() == "")
            //{
            //    zRow.Cells["SalaryGood"].Value = "0";
            //}
            if (zRow.Cells["AccountCode"].Value == null)
            {
                zRow.Cells["AccountCode"].Value = "";
            }
            if (zRow.Cells["AccountCode2"].Value == null)
            {
                zRow.Cells["AccountCode2"].Value = "";
            }
            if (zRow.Cells["Overtime"].Value == null || zRow.Cells["Overtime"].Value.ToString() == "")
            {
                zRow.Cells["Overtime"].Value = "";
            }
            else
            {
                if (zRow.Cells["Overtime"].Value.ToString() != "CO")
                {
                    Message += "Có tính lương ngoài giờ không hợp lệ /";
                }
            }

            if (zRow.Cells["StartingDate"].Value == null || zRow.Cells["StartingDate"].Value.ToString() == "")
            {
                Message += "Ngày bắt đầu làm không hợp lệ /";
            }
            else if (zRow.Cells["StartingDate"].Value.ToString() != "")
            {
                DateTime zStartingDate = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["StartingDate"].Value.ToString(), out zStartingDate))
                {
                    Message += "Ngày bắt đầu làm không hợp lệ /";
                }
            }
            if (zRow.Cells["LeavingDate"].Value == null)
            {
                zRow.Cells["LeavingDate"].Value = "";
            }
            else if (zRow.Cells["LeavingDate"].Value.ToString() != "")
            {
                DateTime zLeavingDate = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["LeavingDate"].Value.ToString(), out zLeavingDate))
                {
                    Message += "Ngày nghỉ việc không hợp lệ /";
                }
            }
            if (zRow.Cells["Description"].Value == null)
            {
                zRow.Cells["Description"].Value = "";
            }
            if (Message != "")
            {
                zRow.Cells["Message"].Value = Message;
                zRow.DefaultCellStyle.BackColor = Color.Tomato;
                zRow.Tag = null;
            }
            else
            {
                zRow.Cells["Message"].Value = "";
                zRow.DefaultCellStyle.BackColor = Color.White;
                zRow.Tag = 0;
            }
            return Message;
        }
        private void SaveCreate()
        {
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            txt_Sql.Text = zSuccess.ToString();
            txt_Error.Text = zError.ToString();

            _TotalGV = GV_Employee.Rows.Count;
            timer5.Start();
        }
        private string SaveRowCreate(DataGridViewRow zRow)
        {
            string zMessage = "";
            string zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
            Employee_Info zinfo = new Employee_Info();
            zinfo.GetEmployeeID(zEmployeeID);
            if (zinfo.Key == "")
            {
                Branch_Info zbranch = new Branch_Info();
                zbranch.Get_Branch_ID((zRow.Cells["Branch"].Value.ToString().Trim()));
                Department_Info zDepartment = new Department_Info();
                zDepartment.Get_Department_ID((zRow.Cells["Department"].Value.ToString().Trim()));
                Team_Info zTeam = new Team_Info();
                zTeam.Get_Team_ID((zRow.Cells["Team"].Value.ToString().Trim()));
                Position_Info zPosition = new Position_Info();
                zPosition.Get_Position_ID((zRow.Cells["Position"].Value.ToString().Trim()));


                //Employee_Info zinfo = new Employee_Info();
                //zinfo.GetEmployeeID(zEmployeeID);

                _Emp_Obj = new Employee_Object("");
               // _Emp_Obj = new Employee_Object();

                _Emp_Obj.EmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
                _Emp_Obj.BranchKey = zbranch.Key;
                _Emp_Obj.DepartmentKey = zDepartment.Key;
                _Emp_Obj.TeamKey = zTeam.Key;
                _Emp_Obj.PositionKey = zPosition.Key;
                Employee_Info zReport = new Employee_Info();
                zReport.GetEmployeeID(zRow.Cells["ReportToID"].Value.ToString().Trim());
                _Emp_Obj.ReportTo = zReport.Key;
                //_Emp_Obj.SalaryBasic = float.Parse(zRow.Cells["SalaryBasic"].Value.ToString().Trim().Replace(",", ""));
                //_Emp_Obj.SalaryGood = float.Parse(zRow.Cells["SalaryGood"].Value.ToString().Trim().Replace(",", ""));
                _Emp_Obj.AccountCode = zRow.Cells["AccountCode"].Value.ToString().Trim();
                _Emp_Obj.AccountCode2 = zRow.Cells["AccountCode2"].Value.ToString().Trim();
                string zOverTime = zRow.Cells["OverTime"].Value.ToString().Trim();
                if (zOverTime == "CO")
                    _Emp_Obj.OverTime = 1;
                else
                    _Emp_Obj.OverTime = 0;

                string zWorkingStatus = zRow.Cells["WorkingStatus"].Value.ToString().Trim();
                if (zWorkingStatus == "Đang làm việc")
                    _Emp_Obj.WorkingStatus = 1;
                else if (zWorkingStatus == "Nghỉ việc")
                    _Emp_Obj.WorkingStatus = 2;
                _Emp_Obj.CompanyPhone = zRow.Cells["Phone"].Value.ToString().Trim();
                _Emp_Obj.CompanyEmail = zRow.Cells["Email"].Value.ToString().Trim();

                DateTime zStar = DateTime.MinValue;
                if (DateTime.TryParse(zRow.Cells["StartingDate"].Value.ToString().Trim(), out zStar))
                {

                }
                DateTime zLea = DateTime.MinValue;
                if (DateTime.TryParse(zRow.Cells["LeavingDate"].Value.ToString().Trim(), out zLea))
                {

                }
                if (zStar != DateTime.MinValue)
                {
                    zStar = new DateTime(zStar.Year, zStar.Month, zStar.Day, 23, 59, 59);
                }
                if (zLea != DateTime.MinValue)
                {
                    zLea = new DateTime(zLea.Year, zLea.Month, zLea.Day, 23, 59, 59);
                }

                _Emp_Obj.StartingDate = zStar;
                _Emp_Obj.LeavingDate = zLea;

                //_Emp_Obj.StartingDate = DateTime.Parse();
                //if (zRow.Cells["LeavingDate"].Value.ToString().Trim() == "")
                //{
                //    _Emp_Obj.LeavingDate = DateTime.MinValue;
                //}
                //else
                //{
                //    _Emp_Obj.LeavingDate = DateTime.Parse(zRow.Cells["LeavingDate"].Value.ToString().Trim());
                //}
                _Emp_Obj.Description = zRow.Cells["Description"].Value.ToString().Trim();
                _Emp_Obj.CreatedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.CreatedName = SessionUser.UserLogin.EmployeeName;
                _Emp_Obj.ModifiedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.ModifiedName = SessionUser.UserLogin.EmployeeName;
                //Personal
                _Emp_Obj.PersoObject.FullName = zRow.Cells["EmployeeName"].Value.ToString().Trim();
                _Emp_Obj.PersoObject.Name = zRow.Cells["EmployeeName"].Value.ToString().Trim();
                _Emp_Obj.PersoObject.CardID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
                string zGender = zRow.Cells["Gender"].Value.ToString().Trim();
                if (zGender == "Nam")
                    _Emp_Obj.PersoObject.Gender = 1;
                else if (zGender == "Nữ")
                    _Emp_Obj.PersoObject.Gender = 0;
                _Emp_Obj.PersoObject.BirthDay = DateTime.Parse(zRow.Cells["BirthDay"].Value.ToString().Trim());
                _Emp_Obj.PersoObject.IssueID = zRow.Cells["IssueID"].Value.ToString().Trim();
                _Emp_Obj.PersoObject.IssuePlace = zRow.Cells["IssuePlace"].Value.ToString().Trim();
                _Emp_Obj.PersoObject.IssueDate = DateTime.Parse(zRow.Cells["IssueDate"].Value.ToString().Trim());
                _Emp_Obj.PersoObject.HomeTown = zRow.Cells["HomeTown"].Value.ToString().Trim();
                _Emp_Obj.PersoObject.BankCode = zRow.Cells["BankCode"].Value.ToString().Trim();
                _Emp_Obj.PersoObject.AddressBank = zRow.Cells["AddressBank"].Value.ToString().Trim();
                _Emp_Obj.PersoObject.TaxCode = zRow.Cells["TaxCode"].Value.ToString().Trim();


                _Emp_Obj.PersoObject.CreatedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.PersoObject.CreatedName = SessionUser.UserLogin.EmployeeName;
                _Emp_Obj.PersoObject.ModifiedBy = SessionUser.UserLogin.Key;
                _Emp_Obj.PersoObject.ModifiedName = SessionUser.UserLogin.EmployeeName;
                _Emp_Obj.SaveObject();
                if (_Emp_Obj.Message.Substring(0,2) == "11")
                {//Khởi tạo lưu thông tin log
                    CreareWorkingHistory(_Emp_Obj.Key);
                }
                zMessage = TN_Message.Show(_Emp_Obj.Message.Substring(0,2));
                if (zMessage != "")
                {
                    zRow.Cells["Message"].Value = zMessage;
                    zRow.DefaultCellStyle.BackColor = Color.Tomato;
                    zRow.Tag = null;
                }
                else
                {
                    zRow.Tag = 1;
                    zRow.Cells["Message"].Value = "Thành công";
                }
            }
            
            return zMessage;
        }
        #endregion
        private void CreareWorkingHistory(string EmployeeKey)
        {
            WorkingHistory_Detail zinfo = new WorkingHistory_Detail();
            zinfo.EmployeeKey = _Emp_Obj.Key;
            zinfo.EmployeeID = _Emp_Obj.EmployeeID.ToUpper();
            zinfo.EmployeeName = _Emp_Obj.FullName.Trim().ToString();
            zinfo.BranchKey = _Emp_Obj.BranchKey;
            Branch_Info zBranch = new Branch_Info(_Emp_Obj.BranchKey);
            zinfo.BranchName = zBranch.BranchName;
            zinfo.DepartmentKey = _Emp_Obj.DepartmentKey;
            Department_Info zDepartment = new Department_Info(_Emp_Obj.DepartmentKey);
            zinfo.DepartmentName = zDepartment.DepartmentName;
            zinfo.TeamKey = _Emp_Obj.TeamKey;
            Team_Info zTeam = new Team_Info(_Emp_Obj.TeamKey);
            zinfo.TeamName = zTeam.TeamName;
            Position_Info zPos = new Position_Info(_Emp_Obj.PositionKey);
            zinfo.PositionKey = _Emp_Obj.PositionKey;
            zinfo.PositionName = zPos.PositionName;
            zinfo.Description = "";
            //DateTime zFromDate = new DateTime(dte_StartingDate.Value.Year, dte_StartingDate.Value.Month, dte_StartingDate.Value.Day, 0, 0, 0);
            //DateTime zToDate = DateTime.MinValue;
            //if (dte_LeavingDate.Value != DateTime.MinValue)
            //{
            //    zToDate = new DateTime(dte_LeavingDate.Value.Year, dte_LeavingDate.Value.Month, dte_LeavingDate.Value.Day, 23, 59, 59);
            //}
            zinfo.StartingDate = _Emp_Obj.StartingDate;
            zinfo.LeavingDate = DateTime.MinValue;

            zinfo.CreatedBy = SessionUser.UserLogin.Key;
            zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
            zinfo.ModifiedBy = SessionUser.UserLogin.Key;
            zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
            zinfo.Create();
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (_IsSaved == true)
            {
                this.Close();
            }
            else
            {
                if (Utils.TNMessageBox("Dữ liệu bạn chưa được cập nhật !.",2) == "Y")
                {
                    this.Close();
                }
            }
        }
        #endregion

        #region[Timer]

        //
        private void Timer1_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer1.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;

                if (GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag == null)
                {
                    string zMesage = CheckRow(GV_Employee.Rows[_IndexGV]);
                    if (zMesage != "")
                    {
                        zError++;
                        txt_Error.Text = zError.ToString();
                    }
                    else
                    {
                        zSuccess++;
                        //txt_Sql.Text = zSuccess.ToString();
                    }
                }
                _IndexGV++;
                timer1.Start();
            }
            else
            {
                timer1.Stop();
                if (zError > 0)
                {
                    timer2.Stop();
                    PicLoading.Visible = false;
                    Utils.TNMessageBoxOK("Còn " + zError + " dòng không hợp lệ vui lòng kiểm tra lại!",2);
                }
                else

                {
                    _IndexGV = 0;
                    zError = 0;
                    zSuccess = 0;
                    lbl_Sql.Text = "Số lượng đã lưu";
                    txt_Sql.Text = "0";
                    timer2.Start();
                }
            }
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            //Save
            timer2.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;
                if (GV_Employee.Rows[_IndexGV].Cells["EmployeeID"].Value != null && GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag.ToString() == "0")
                {
                    string zMessage = SaveRow(GV_Employee.Rows[_IndexGV]);
                    if (zMessage != "")
                    {
                        zError++;
                    }
                    else
                    {
                        zSuccess++;
                    }

                }
                txt_Sql.Text = zSuccess.ToString();
                txt_Error.Text = zError.ToString();
                _IndexGV++;
                timer2.Start();
            }
            else
            {
                timer2.Stop();
                PicLoading.Visible = false;
                _IsSaved = true;
                if (zError > 0)
                    Utils.TNMessageBoxOK("Còn " + zError + " dòng chưa lưu !", 2);
                else
                    Utils.TNMessageBoxOK("Đã lưu thành công!", 3);
            }
        }
        //Load data
        private void Timer3_Tick(object sender, EventArgs e)
        {
            timer3.Stop();
            if (_IndexGV < _TotalGV)
            {
                int i = _IndexGV;
                DataRow zRow = _GVEmployee.Rows[i];
                GV_Employee.Rows.Add();
                GV_Employee.Rows[i].Cells["No"].Value = (i + 1).ToString();
                GV_Employee.Rows[i].Cells["EmployeeID"].Value = zRow[1].ToString().Trim();
                GV_Employee.Rows[i].Cells["EmployeeName"].Value = zRow[2].ToString().Trim();
                GV_Employee.Rows[i].Cells["IssueID"].Value = zRow[3].ToString().Trim();
                GV_Employee.Rows[i].Cells["BirthDay"].Value = zRow[4].ToString();
                GV_Employee.Rows[i].Cells["Gender"].Value = zRow[5].ToString().Trim();
                GV_Employee.Rows[i].Cells["IssueDate"].Value = zRow[6].ToString().Trim();
                GV_Employee.Rows[i].Cells["IssuePlace"].Value = zRow[7].ToString().Trim();
                GV_Employee.Rows[i].Cells["HomeTown"].Value = zRow[8].ToString().Trim();
                GV_Employee.Rows[i].Cells["Branch"].Value = zRow[9].ToString().Trim();
                GV_Employee.Rows[i].Cells["Department"].Value = zRow[10].ToString().Trim();
                GV_Employee.Rows[i].Cells["Team"].Value = zRow[11].ToString().Trim();
                GV_Employee.Rows[i].Cells["Position"].Value = zRow[12].ToString().Trim();
                GV_Employee.Rows[i].Cells["Email"].Value = zRow[13].ToString().Trim();
                GV_Employee.Rows[i].Cells["Phone"].Value = zRow[14].ToString().Trim();
                GV_Employee.Rows[i].Cells["ReportToID"].Value = zRow[15].ToString().Trim();
                GV_Employee.Rows[i].Cells["ReportToName"].Value = zRow[16].ToString().Trim();
                GV_Employee.Rows[i].Cells["WorkingStatus"].Value = zRow[17].ToString().Trim();
                GV_Employee.Rows[i].Cells["BankCode"].Value = zRow[18].ToString().Trim();
                GV_Employee.Rows[i].Cells["AddressBank"].Value = zRow[19].ToString().Trim();
                GV_Employee.Rows[i].Cells["Taxcode"].Value = zRow[20].ToString().Trim();
                //float zSalaryBasic = 0;
                //if (float.TryParse(zRow[21].ToString().Trim(), out zSalaryBasic))
                //{
                //    GV_Employee.Rows[i].Cells["SalaryBasic"].Value = zSalaryBasic.ToString("###,###,##0");
                //}
                //float zSalaryGood = 0;
                //if (float.TryParse(zRow[22].ToString().Trim(), out zSalaryGood))
                //{
                //    GV_Employee.Rows[i].Cells["SalaryGood"].Value = zSalaryGood.ToString("###,###,##0");
                //}

                GV_Employee.Rows[i].Cells["AccountCode"].Value = zRow[21].ToString().Trim();
                GV_Employee.Rows[i].Cells["AccountCode2"].Value = zRow[22].ToString().Trim();
                GV_Employee.Rows[i].Cells["Overtime"].Value = zRow[23].ToString().Trim();
                GV_Employee.Rows[i].Cells["StartingDate"].Value = zRow[24].ToString().Trim();
                GV_Employee.Rows[i].Cells["LeavingDate"].Value = zRow[25].ToString().Trim();
                GV_Employee.Rows[i].Cells["Description"].Value = zRow[26].ToString().Trim();

                txt_Amount_Excel.Text = (i + 1).ToString();
                _IndexGV++;
                timer3.Start();
            }
            else
            {
                PicLoading.Visible = false;
                timer3.Stop();
            }
        }

        private void Timer4_Tick(object sender, EventArgs e)
        {
            // check befor save
            timer4.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;

                if (GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag == null)
                {
                    string zMesage = CheckRowCreate(GV_Employee.Rows[_IndexGV]);
                    if (zMesage != "")
                    {
                        zError++;
                        txt_Error.Text = zError.ToString();
                    }
                    else
                    {
                        zSuccess++;
                        //txt_Sql.Text = zSuccess.ToString();
                    }
                }
                _IndexGV++;
                timer4.Start();
            }
            else
            {
                timer4.Stop();
                if (zError > 0)
                {
                    timer5.Stop();
                    PicLoading.Visible = false;
                    Utils.TNMessageBoxOK("Còn " + zError + " dòng không hợp lệ vui lòng kiểm tra lại!", 2);
                }
                else

                {
                    _IndexGV = 0;
                    zError = 0;
                    zSuccess = 0;
                    lbl_Sql.Text = "Số lượng đã lưu";
                    txt_Sql.Text = "0";
                    timer5.Start();
                }
            }
        }
        private void Timer5_Tick(object sender, EventArgs e)
        {
            //Save
            timer5.Stop();
            if (_IndexGV < _TotalGV)
            {
                GV_Employee.Rows[_IndexGV].DefaultCellStyle.BackColor = Color.White;
                if (GV_Employee.Rows[_IndexGV].Cells["EmployeeID"].Value != null && GV_Employee.Rows[_IndexGV].Visible == true && GV_Employee.Rows[_IndexGV].Tag.ToString() == "0")
                {
                    string zMessage = SaveRowCreate(GV_Employee.Rows[_IndexGV]);
                    if (zMessage != "")
                    {
                        zError++;
                    }
                    else
                    {
                        zSuccess++;
                    }

                }
                txt_Sql.Text = zSuccess.ToString();
                txt_Error.Text = zError.ToString();
                _IndexGV++;
                timer5.Start();
            }
            else
            {
                timer5.Stop();
                PicLoading.Visible = false;
                _IsSaved = true;
                if (zError > 0)
                {
                    string Status = "Import Excel form "+ HeaderControl.Text + " > lỗi > SL Excel:" + txt_Amount_Excel.Text + " > SL Lỗi:" + txt_Error.Text + " > SL SQL:" + txt_Sql.Text;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    Utils.TNMessageBoxOK("Còn " + zError + " dòng chưa lưu !", 2);
                }
                else
                {
                    string Status = "Import Excel form "+ HeaderControl.Text + " > thành công > SL Excel:" + txt_Amount_Excel.Text + " > SL Lỗi:" + txt_Error.Text + " > SL SQL:" + txt_Sql.Text;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    Utils.TNMessageBoxOK("Đã lưu thành công!", 3);
                }
            }
        }

        #endregion



        #region[Code vừa tạo mới vừa cập]
        private string CheckRow(DataGridViewRow zRow)
        {
            string Message = "";
            if (zRow.Cells["EmployeeName"].Value == null || zRow.Cells["EmployeeName"].Value.ToString() == "")
            {
                Message += "Tên nhân viên không bỏ trống /";
            }
            if (zRow.Cells["EmployeeID"].Value == null || zRow.Cells["EmployeeID"].Value.ToString() == "")
            {
                Message += "Mã nhân viên không bỏ trống /";
            }
            if (zRow.Cells["Gender"].Value == null || zRow.Cells["Gender"].Value.ToString() == "")
            {
                Message += "Giói tính không hợp lệ /";
            }
            else
            {
                if (zRow.Cells["Gender"].Value.ToString() != "Nam" && zRow.Cells["Gender"].Value.ToString() != "Nữ")
                {
                    Message += "Giói tính không hợp lệ /";
                }
            }
            if (zRow.Cells["BirthDay"].Value == null || zRow.Cells["BirthDay"].Value.ToString() == "")
            {
                Message += "Ngày sinh không hợp lệ /";
            }
            else if (zRow.Cells["BirthDay"].Value != null || zRow.Cells["BirthDay"].Value.ToString() != "")
            {
                DateTime zBirthDay = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["BirthDay"].Value.ToString(), out zBirthDay))
                {
                    Message += "Ngày sinh không hợp lệ /";
                }
            }
            if (zRow.Cells["IssueID"].Value == null || zRow.Cells["IssueID"].Value.ToString() == "" || zRow.Cells["IssueID"].Value.ToString().Trim().Length < 9)
            {
                Message += "CMND không hợp lệ /";
            }
            if (zRow.Cells["IssueDate"].Value == null || zRow.Cells["IssueDate"].Value.ToString() == "")
            {
                Message += "Ngày cấp không hợp lệ /";
            }
            else if (zRow.Cells["IssueDate"].Value.ToString() != "")
            {
                DateTime zIsueDate = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["IssueDate"].Value.ToString(), out zIsueDate))
                {
                    Message += "Ngày cấp không hợp lệ /";
                }
            }
            if (zRow.Cells["IssuePlace"].Value == null)
            {
                zRow.Cells["IssuePlace"].Value = "";
            }
            if (zRow.Cells["HomeTown"].Value == null)
            {
                zRow.Cells["HomeTown"].Value = "";
            }
            if (zRow.Cells["Branch"].Value == null || zRow.Cells["Branch"].Value.ToString() == "")
            {
                Message += "Khối không hợp lệ /";
            }
            else
            {
                Branch_Info zinfo = new Branch_Info();
                zinfo.Get_Branch_ID((zRow.Cells["Branch"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Khối không hợp lệ /";
                }
            }
            if (zRow.Cells["Department"].Value == null || zRow.Cells["Department"].Value.ToString() == "")
            {
                Message += "Phòng ban không hợp lệ /";
            }
            else
            {
                Department_Info zinfo = new Department_Info();
                zinfo.Get_Department_ID((zRow.Cells["Department"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Phòng ban không hợp lệ /";
                }
            }
            if (zRow.Cells["Team"].Value == null)
            {
                zRow.Cells["Team"].Value = "";
            }
            else if (zRow.Cells["Team"].Value.ToString() != "")
            {
                Team_Info zinfo = new Team_Info();
                zinfo.Get_Team_ID((zRow.Cells["Team"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Tổ nhóm không hợp lệ /";
                }
            }
            if (zRow.Cells["Position"].Value == null || zRow.Cells["Position"].Value.ToString() == "")
            {
                Message += "Chức vụ không hợp lệ /";
            }
            else
            {
                Position_Info zinfo = new Position_Info();
                zinfo.Get_Position_ID((zRow.Cells["Position"].Value.ToString().Trim()));
                if (zinfo.Key == 0)
                {
                    Message += "Chức vụ không hợp lệ /";
                }
            }
            //
            if (zRow.Cells["Email"].Value == null)
            {
                zRow.Cells["Email"].Value = "";
            }
            if (zRow.Cells["Phone"].Value == null)
            {
                zRow.Cells["Phone"].Value = "";
            }
            if (zRow.Cells["ReportToID"].Value == null)
            {
                zRow.Cells["ReportToID"].Value = "";
            }
            else if (zRow.Cells["ReportToID"].Value.ToString() != "")
            {
                Employee_Info zinfo = new Employee_Info();
                zinfo.GetEmployeeID((zRow.Cells["ReportToID"].Value.ToString().Trim()));
                if (zinfo.Key == "")
                {
                    Message += "Mã người quản lý không hợp lệ /";
                }
            }

            if (zRow.Cells["WorkingStatus"].Value == null || zRow.Cells["WorkingStatus"].Value.ToString() == "")
            {
                Message += "Tình trạng không hợp lệ /";
            }
            else
            {
                if (zRow.Cells["WorkingStatus"].Value.ToString() != "Đang làm việc" && zRow.Cells["WorkingStatus"].Value.ToString() != "Nghỉ việc")
                {
                    Message += "Tình trạng không hợp lệ /";
                }
            }
            if (zRow.Cells["BankCode"].Value == null)
            {
                zRow.Cells["BankCode"].Value = "";
            }
            if (zRow.Cells["AddressBank"].Value == null)
            {
                zRow.Cells["AddressBank"].Value = "";
            }
            if (zRow.Cells["Taxcode"].Value == null)
            {
                zRow.Cells["Taxcode"].Value = "";
            }
            //if (zRow.Cells["SalaryBasic"].Value == null || zRow.Cells["SalaryBasic"].Value.ToString() == "")
            //{
            //    zRow.Cells["SalaryBasic"].Value = "0";
            //}
            //if (zRow.Cells["SalaryGood"].Value == null || zRow.Cells["SalaryGood"].Value.ToString() == "")
            //{
            //    zRow.Cells["SalaryGood"].Value = "0";
            //}
            if (zRow.Cells["AccountCode"].Value == null)
            {
                zRow.Cells["AccountCode"].Value = "";
            }
            if (zRow.Cells["AccountCode2"].Value == null)
            {
                zRow.Cells["AccountCode2"].Value = "";
            }
            if (zRow.Cells["Overtime"].Value == null || zRow.Cells["Overtime"].Value.ToString() == "")
            {
                zRow.Cells["Overtime"].Value = "";
            }
            else
            {
                if (zRow.Cells["Overtime"].Value.ToString() != "CO")
                {
                    Message += "Có tính lương ngoài giờ không hợp lệ /";
                }
            }

            if (zRow.Cells["StartingDate"].Value == null || zRow.Cells["StartingDate"].Value.ToString() == "")
            {
                Message += "Ngày bắt đầu làm không hợp lệ /";
            }
            else if (zRow.Cells["StartingDate"].Value.ToString() != "")
            {
                DateTime zStartingDate = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["StartingDate"].Value.ToString(), out zStartingDate))
                {
                    Message += "Ngày bắt đầu làm không hợp lệ /";
                }
            }
            if (zRow.Cells["LeavingDate"].Value == null)
            {
                zRow.Cells["LeavingDate"].Value = "";
            }
            else if (zRow.Cells["LeavingDate"].Value.ToString() != "")
            {
                DateTime zLeavingDate = DateTime.MinValue;
                if (!DateTime.TryParse(zRow.Cells["LeavingDate"].Value.ToString(), out zLeavingDate))
                {
                    Message += "Ngày nghỉ việc không hợp lệ /";
                }
            }
            if (zRow.Cells["Description"].Value == null)
            {
                zRow.Cells["Description"].Value = "";
            }
            if (Message != "")
            {
                zRow.Cells["Message"].Value = Message;
                zRow.DefaultCellStyle.BackColor = Color.Tomato;
                zRow.Tag = null;
            }
            else
            {
                zRow.Cells["Message"].Value = "";
                zRow.DefaultCellStyle.BackColor = Color.White;
                zRow.Tag = 0;
            }
            return Message;
        }
        private void Save()
        {
            zError = 0;
            zSuccess = 0;
            _IndexGV = 0;
            txt_Sql.Text = zSuccess.ToString();
            txt_Error.Text = zError.ToString();

            _TotalGV = GV_Employee.Rows.Count;
            timer2.Start();
        }
        private string SaveRow(DataGridViewRow zRow)
        {
            string zMessage = "";

            string zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
            Branch_Info zbranch = new Branch_Info();
            zbranch.Get_Branch_ID((zRow.Cells["Branch"].Value.ToString().Trim()));
            Department_Info zDepartment = new Department_Info();
            zDepartment.Get_Department_ID((zRow.Cells["Department"].Value.ToString().Trim()));
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID((zRow.Cells["Team"].Value.ToString().Trim()));
            Position_Info zPosition = new Position_Info();
            zPosition.Get_Position_ID((zRow.Cells["Position"].Value.ToString().Trim()));


            Employee_Info zinfo = new Employee_Info();
            zinfo.GetEmployeeID(zEmployeeID);

            _Emp_Obj = new Employee_Object(zinfo.Key);

            _Emp_Obj.EmployeeID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
            _Emp_Obj.BranchKey = zbranch.Key;
            _Emp_Obj.DepartmentKey = zDepartment.Key;
            _Emp_Obj.TeamKey = zTeam.Key;
            _Emp_Obj.PositionKey = zPosition.Key;
            Employee_Info zReport = new Employee_Info();
            zReport.GetEmployeeID(zRow.Cells["ReportToID"].Value.ToString().Trim());
            _Emp_Obj.ReportTo = zReport.Key;
            //_Emp_Obj.SalaryBasic = float.Parse(zRow.Cells["SalaryBasic"].Value.ToString().Trim().Replace(",", ""));
            //_Emp_Obj.SalaryGood = float.Parse(zRow.Cells["SalaryGood"].Value.ToString().Trim().Replace(",", ""));
            _Emp_Obj.AccountCode = zRow.Cells["AccountCode"].Value.ToString().Trim();
            _Emp_Obj.AccountCode2 = zRow.Cells["AccountCode2"].Value.ToString().Trim();
            string zOverTime = zRow.Cells["OverTime"].Value.ToString().Trim();
            if (zOverTime == "CO")
                _Emp_Obj.OverTime = 1;
            else
                _Emp_Obj.OverTime = 0;

            string zWorkingStatus = zRow.Cells["WorkingStatus"].Value.ToString().Trim();
            if (zWorkingStatus == "Đang làm việc")
                _Emp_Obj.WorkingStatus = 1;
            else if (zWorkingStatus == "Nghỉ việc")
                _Emp_Obj.WorkingStatus = 2;
            _Emp_Obj.CompanyPhone = zRow.Cells["Phone"].Value.ToString().Trim();
            _Emp_Obj.CompanyEmail = zRow.Cells["Email"].Value.ToString().Trim();
            _Emp_Obj.StartingDate = DateTime.Parse(zRow.Cells["StartingDate"].Value.ToString().Trim());
            if (zRow.Cells["LeavingDate"].Value.ToString().Trim() == "")
            {
                _Emp_Obj.LeavingDate = DateTime.MinValue;
            }
            else
            {
                _Emp_Obj.LeavingDate = DateTime.Parse(zRow.Cells["LeavingDate"].Value.ToString().Trim());
            }
            _Emp_Obj.Description = zRow.Cells["Description"].Value.ToString().Trim();
            _Emp_Obj.CreatedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.CreatedName = SessionUser.UserLogin.EmployeeName;
            _Emp_Obj.ModifiedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.ModifiedName = SessionUser.UserLogin.EmployeeName;
            //Personal
            _Emp_Obj.PersoObject.FullName = zRow.Cells["EmployeeName"].Value.ToString().Trim();
            _Emp_Obj.PersoObject.Name = zRow.Cells["EmployeeName"].Value.ToString().Trim();
            _Emp_Obj.PersoObject.CardID = zRow.Cells["EmployeeID"].Value.ToString().Trim();
            string zGender = zRow.Cells["Gender"].Value.ToString().Trim();
            if (zGender == "Nam")
                _Emp_Obj.PersoObject.Gender = 1;
            else if (zGender == "Nữ")
                _Emp_Obj.PersoObject.Gender = 0;
            _Emp_Obj.PersoObject.BirthDay = DateTime.Parse(zRow.Cells["BirthDay"].Value.ToString().Trim());
            _Emp_Obj.PersoObject.IssueID = zRow.Cells["IssueID"].Value.ToString().Trim();
            _Emp_Obj.PersoObject.IssuePlace = zRow.Cells["IssuePlace"].Value.ToString().Trim();
            _Emp_Obj.PersoObject.IssueDate = DateTime.Parse(zRow.Cells["IssueDate"].Value.ToString().Trim());
            _Emp_Obj.PersoObject.HomeTown = zRow.Cells["HomeTown"].Value.ToString().Trim();
            _Emp_Obj.PersoObject.BankCode = zRow.Cells["BankCode"].Value.ToString().Trim();
            _Emp_Obj.PersoObject.AddressBank = zRow.Cells["AddressBank"].Value.ToString().Trim();
            _Emp_Obj.PersoObject.TaxCode = zRow.Cells["TaxCode"].Value.ToString().Trim();


            _Emp_Obj.PersoObject.CreatedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.PersoObject.CreatedName = SessionUser.UserLogin.EmployeeName;
            _Emp_Obj.PersoObject.ModifiedBy = SessionUser.UserLogin.Key;
            _Emp_Obj.PersoObject.ModifiedName = SessionUser.UserLogin.EmployeeName;
            _Emp_Obj.SaveObject();
            zMessage = TN_Message.Show(_Emp_Obj.Message);
            if (zMessage != "")
            {
                zRow.Cells["Message"].Value = zMessage;
                zRow.DefaultCellStyle.BackColor = Color.Tomato;
                zRow.Tag = null;
            }
            else
            {
                zRow.Tag = 1;
                zRow.Cells["Message"].Value = "Thành công";
            }
            return zMessage;
        }
        #endregion
    }
}
