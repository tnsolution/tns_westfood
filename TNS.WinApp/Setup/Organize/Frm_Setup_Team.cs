﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    
    public partial class Frm_Setup_Team : Form
    {
        private int _Key = 0;
        public Frm_Setup_Team()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            GVData.Click += GVData_List_Click;
            btn_Export.Click += Btn_Export_Click;

            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            cbo_Branch_Search.SelectedIndexChanged += Cbo_Branch_Search_SelectedIndexChanged;
            cbo_BranchKey.SelectedIndexChanged += Cbo_BranchKey_SelectedIndexChanged;
            btn_SoDo.Click += Btn_SoDo_Click;
        }

        private void Frm_Setup_Team_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            LoadDataToToolbox.KryptonComboBox(cbo_BranchKey, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE RecordStatus != 99 ORDER BY [RANK]", "--Chọn--");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch_Search, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE RecordStatus != 99 ORDER BY [RANK]", "--Chọn--");
            //DisplayData();
            SetDefault();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Btn_SoDo_Click(object sender, EventArgs e)
        {
            Frm_SoDoToChuc frm = new Frm_SoDoToChuc();
            frm.Show();
        }

        private void Cbo_BranchKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_DeparmentKey, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus != 99 AND BranchKey = " + cbo_BranchKey.SelectedValue.ToInt() + "  ORDER BY [Rank]", "--Chọn--");
        }

        private void Cbo_Branch_Search_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department_Search, "SELECT DepartmentKey,DepartmentName FROM SYS_Department WHERE RecordStatus != 99 AND BranchKey = " + cbo_Branch_Search.SelectedValue.ToInt() + "  ORDER BY [Rank]", "--Chọn--");
        }
        #region[Progess]

        private void LoadData()
        {

            Team_Info zinfo = new Team_Info(_Key);
            txt_TeamID.Text = zinfo.TeamID;
            txt_TeamName.Text = zinfo.TeamName;
            cbo_DeparmentKey.SelectedValue = zinfo.DepartmentKey;
            cbo_BranchKey.SelectedValue = zinfo.BranchKey;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            txt_AccountCode.Text = zinfo.AccountCode;
            txt_AccountCode2.Text = zinfo.AccountCode2;
            lbl_Created.Text = "Tạo bởi:[" + zinfo.CreatedName + "][" + zinfo.CreatedOn + "]";
            lbl_Modified.Text = "Chỉnh sửa:[" + zinfo.ModifiedName + "][" + zinfo.ModifiedOn + "]";
        }

        private void SetDefault()
        {
            _Key = 0;
            txt_TeamID.Text = "";
            txt_TeamName.Text = "";
            txt_Rank.Text = "0";
            txt_Description.Text = "";
            cbo_DeparmentKey.SelectedIndex = 0;
            cbo_DeparmentKey.SelectedIndex = 0;
            txt_AccountCode.Text = "";
            txt_AccountCode2.Text = "";
            lbl_Created.Text = "Tạo bởi:";
            lbl_Modified.Text = "Chỉnh sửa:";
            txt_TeamID.Focus();
        }

        #endregion

        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (txt_TeamID.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập mã!",1);
                return;
            }
            if (txt_TeamName.Text.Trim().Length == 0)
            {
                Utils.TNMessageBoxOK("Chưa nhập tên nhóm!",1);
                return;
            }
            if (Team_Data.CountID(txt_TeamID.Text.Trim().ToUpper()) > 0 && _Key == 0)
            {
                Utils.TNMessageBoxOK("Mã nhóm đã tồn tại !.", 1);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                Utils.TNMessageBoxOK("Vui lòng nhập đúng định dạng số!",1);
                return;
            }
            else
            {
                Team_Info zinfo = new Team_Info(_Key);
                zinfo.TeamID = txt_TeamID.Text.Trim().ToUpper();
                zinfo.TeamName = txt_TeamName.Text.Trim();
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.BranchKey = cbo_BranchKey.SelectedValue.ToInt();
                zinfo.DepartmentKey = cbo_DeparmentKey.SelectedValue.ToInt();
                zinfo.AccountCode = txt_AccountCode.Text.Trim();
                zinfo.AccountCode2 = txt_AccountCode2.Text.Trim();

                zinfo.CreatedBy = SessionUser.UserLogin.Key;
                zinfo.CreatedName = SessionUser.UserLogin.EmployeeName;
                zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                zinfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                zinfo.Save();
                if (zinfo.Message.Substring(0, 2) == "20" || zinfo.Message.Substring(0, 2) == "11")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!",3);
                    DisplayData();
                    SetDefault();
                }
                else
                {
                    Utils.TNMessageBoxOK(zinfo.Message,4);
                }
            }


        }

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (_Key == 0)
            {
                Utils.TNMessageBoxOK("Chưa chọn thông tin!",1);
            }
            else
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?.",2) == "Y")
                {
                    Team_Info zinfo = new Team_Info(_Key);
                    zinfo.ModifiedBy = SessionUser.UserLogin.Key;
                    zinfo.ModifiedBy = SessionUser.UserLogin.EmployeeName;
                    zinfo.Delete();
                    if (zinfo.Message.Substring(0, 2) == "30")
                    {
                        Utils.TNMessageBoxOK("Đã xóa thành công !",3);
                        DisplayData();
                        SetDefault();
                    }
                    else
                    {
                        Utils.TNMessageBoxOK(zinfo.Message,4);
                    }

                }
            }

        }


        private void Btn_New_Click(object sender, EventArgs e)
        {
            SetDefault();
        }

        private void Btn_Search_Click(object sender, EventArgs e)
        {

            DisplayData();
        }
        #endregion

        #region[ListView]
        private void DisplayData()
        {
            int zBranchSearch = 0;
            int zDepartmentSearch = 0;
            if (cbo_Branch_Search.SelectedValue != null)
            {
                zBranchSearch = cbo_Branch_Search.SelectedValue.ToInt();
            }
            if (cbo_Department_Search.SelectedValue != null)
            {
                zDepartmentSearch = cbo_Department_Search.SelectedValue.ToInt();
            }
            DataTable ztb = Team_Data.Search(zBranchSearch, zDepartmentSearch, txt_Search.Text.Trim());
            InitGV_Layout(ztb);
        }
        private void GVData_List_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                string zKey = GVData.Rows[GVData.RowSel][9].ToString();
                if (zKey != null || zKey != "")
                {
                    _Key = int.Parse(zKey);
                    LoadData();
                }
                else
                {
                    _Key = 0;
                    LoadData();
                }
            }

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách tổ nhóm.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 10;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Mã";
            GVData.Rows[0][2] = "Tên tổ/nhóm";
            GVData.Rows[0][3] = "Phòng ban";
            GVData.Rows[0][4] = "Khối";
            GVData.Rows[0][5] = "Sắp xếp";
            GVData.Rows[0][6] = "Mã kế toán 1";
            GVData.Rows[0][7] = "Mã kế toán 2";
            GVData.Rows[0][8] = "Diễn giải";
            GVData.Rows[0][9] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5].Toe0String();
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                GVData.Rows[rIndex + 1][8] = rData[8];
                GVData.Rows[rIndex + 1][9] = rData[0];
            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 50;

            GVData.Cols[0].Width = 30;
            GVData.Cols[1].Width = 60;
            GVData.Cols[2].Width = 250;
            GVData.Cols[3].Width = 200;
            GVData.Cols[4].Width = 180;
            GVData.Cols[5].Width = 50;
            GVData.Cols[6].Width = 50;
            GVData.Cols[7].Width = 50;
            GVData.Cols[8].Width = 100;
            GVData.Cols[9].Visible = false;

            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[6].TextAlign = TextAlignEnum.RightCenter;
            GVData.Cols[7].TextAlign = TextAlignEnum.RightCenter;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;

                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Size = new Size(800, 600);
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_SoDo.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_New.Enabled = false;
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Enabled = false;
            }
        }
        #endregion
    }
}
