﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Adjust_NCPN_Data
    {
        public static string TramaID(string AdjustID)
        {
            string zResult = "";

            //---------- String SQL Access Database ---------------
            string zSQL = @"DECLARE @AssetIDNew nvarchar(50)
	SELECT  @AssetIDNew = ISNULL(MAX(RIGHT(AdjustID,4)) + 1 ,1)  FROM HRM_Adjust_NCPN 
	WHERE LEN(AdjustID) = 11 AND ISNUMERIC(RIGHT(AdjustID,4)) = 1 and Left(AdjustID,6)= @AdjustID AND RecordStatus <> 99 
	SET @AssetIDNew =   @AdjustID +'-'+ RIGHT('000' + @AssetIDNew,4)
	select @AssetIDNew";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AdjustID", SqlDbType.NVarChar).Value = AdjustID.Trim();
                zResult = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                //Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public static DataTable Search_List(DateTime FromDate, DateTime ToDate, string ID)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @" SELECT A.AdjustKey,A.DateWrite,A.EmployeeName,EmployeeID,B.TeamID,Number, A.AdjustName ,A.Description FROM  [dbo].[HRM_Adjust_NCPN] A 
 LEFT JOIN [dbo].SYS_Team B ON B.TeamKey =A.TeamKey 
WHERE DateWrite between @FromDate and @ToDate 
AND A.RecordStatus <> 99 ";

            if (ID.Trim().Length > 0)
            {
                zSQL += " AND A.AdjustName LIKE @ID";
            }
           
            zSQL += " Order by A.AdjustName ";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                if (FromDate != DateTime.MinValue && ToDate != DateTime.MinValue)
                {
                    zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                    zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                }
                if (ID.Trim().Length > 0)
                {
                    zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = "%" + ID + "%";
                }

                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
