﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;
using TNS_Report;

namespace TNS.WinApp
{
    public partial class Frm_BaoCaoDanhSachNhanSu : Form
    {
        private DateTime _DateClose;
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Search = "";
        private int _Gender = 0;
        private int _OrverTime=0;
        private int _Scrore= 0;
        private int _Status = 0;
        private int _Position = 0;
        public Frm_BaoCaoDanhSachNhanSu()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

             btn_Export.Click += Btn_Export_Click;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            dteDate.Value = SessionUser.Date_Work;
            GVData.DoubleClick += GVData_DoubleClick;

            //LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Tất cả ----");
            //LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Tất cả ----");
            LoadDataToToolbox.KryptonComboBox(cbo_Position, "SELECT PositionKey,PositionName FROM SYS_Position WHERE RecordStatus < 99", "---- Tất cả ----");
        }

        private void GVData_DoubleClick(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0 && GVData.Rows[GVData.RowSel][24].ToString() != "")
            {
                Frm_HumanInfo frm = new Frm_HumanInfo();
                frm.ReportDate = dteDate.Value;
                frm.EmployeeKey = GVData.Rows[GVData.RowSel][24].ToString();
                frm.Show();
            }
        }

        private void Frm_BaoCaoDanhSachNhanSu_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            cbo_Gender.SelectedIndex = 0;
            cbo_Overtime.SelectedIndex = 0;
            cbo_Score.SelectedIndex = 0;
            cbo_Status.SelectedIndex = 0;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (cbo_Branch.SelectedIndex > 0)
            {
                _BranchKey = cbo_Branch.SelectedValue.ToInt();
            }
            else
                _BranchKey = 0;
            if (cbo_Department.SelectedIndex > 0)
                _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            else
                _DepartmentKey = 0;
            if (cbo_Team.SelectedIndex > 0)
                _TeamKey = cbo_Team.SelectedValue.ToInt();
            else
                _TeamKey = 0;
            if (cbo_Position.SelectedIndex > 0)
                _Position = cbo_Position.SelectedValue.ToInt();
            else
                _Position = 0;
            
            _DateClose = dteDate.Value;
            _Search = txt_Search.Text.Trim();

            //Giới tính
            if (cbo_Gender.SelectedIndex == 0)
                _Gender = 0;
            if (cbo_Gender.SelectedIndex == 1)
                _Gender = 1;
            if (cbo_Gender.SelectedIndex == 2)
                _Gender = 2;
            //Overtime
            if (cbo_Overtime.SelectedIndex == 0)
                _OrverTime = 0;
            if (cbo_Overtime.SelectedIndex == 1)
                _OrverTime = 1;
            if (cbo_Overtime.SelectedIndex == 2)
                _OrverTime = 2;
            //Overtime
            if (cbo_Score.SelectedIndex == 0)
                _Scrore = 0;
            if (cbo_Score.SelectedIndex == 1)
                _Scrore = 1;
            if (cbo_Score.SelectedIndex == 2)
                _Scrore = 2;
            //Overtime
            if (cbo_Status.SelectedIndex == 0)
                _Status = 0;
            if (cbo_Status.SelectedIndex == 1)
                _Status = 1;
            if (cbo_Status.SelectedIndex == 2)
                _Status = 2;

            //string Status = "Tìm DL form " + HeaderControl.Text + " > tháng: " + dteDate.Value.ToString("MM/yyyy") + " > Nhóm:" + cboTeam.Text + " > Từ khóa:" + txt_Search.Text;
            //Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }

        private void DisplayData()
        {
            this.Invoke(new MethodInvoker(delegate ()
            {
                DataTable ztb = Report_HumanResource.DANHSACHNHANSU(_DateClose,_BranchKey,_DepartmentKey,_TeamKey,_Position,_Search, _Gender, _OrverTime, _Scrore, _Status);
                InitGV_Layout(ztb);
            }));
        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 25;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Họ tên";
            GVData.Rows[0][2] = "Số thẻ";
            GVData.Rows[0][3] = "Chức vụ";
            GVData.Rows[0][4] = "Tổ nhóm";
            GVData.Rows[0][5] = "Bộ phận";
            GVData.Rows[0][6] = "Khối";
            GVData.Rows[0][7] = "Ngày sinh";
            GVData.Rows[0][8] = "Giới tính";
            GVData.Rows[0][9] = "CMND";
            GVData.Rows[0][10] = "Ngày cấp";
            GVData.Rows[0][11] = "Nơi cấp";
            GVData.Rows[0][12] = "Địa chỉ";
            GVData.Rows[0][13] = "Tình trạng";
            GVData.Rows[0][14] = "Ngày bắt đầu làm việc";
            GVData.Rows[0][15] = "Ngày hết thử việc";
            GVData.Rows[0][16] = "Ngày nghỉ việc";
            GVData.Rows[0][17] = "Điện thoại";
            GVData.Rows[0][18] = "Email";
            GVData.Rows[0][19] = "ATM";
            GVData.Rows[0][20] = "Tài khoản kế toán cấp 1";
            GVData.Rows[0][21] = "Tài khoản kế toán cấp 2";
            GVData.Rows[0][22] = "Được tính tăng ca";
            GVData.Rows[0][23] = "Loại tính lương";
            GVData.Rows[0][24] = ""; // ẩn cột này

            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[1];
                GVData.Rows[rIndex + 1][2] = rData[2];
                GVData.Rows[rIndex + 1][3] = rData[3];
                GVData.Rows[rIndex + 1][4] = rData[4];
                GVData.Rows[rIndex + 1][5] = rData[5];
                GVData.Rows[rIndex + 1][6] = rData[6];
                GVData.Rows[rIndex + 1][7] = rData[7];
                if (rData[8].ToInt() == 1)
                    GVData.Rows[rIndex + 1][8] = "Nam";
                else
                    GVData.Rows[rIndex + 1][8] = "Nữ";
                GVData.Rows[rIndex + 1][9] = rData[9];
                GVData.Rows[rIndex + 1][10] = rData[10];
                GVData.Rows[rIndex + 1][11] = rData[11];
                GVData.Rows[rIndex + 1][12] = rData[12];
                if(rData[13].ToInt()==1)
                GVData.Rows[rIndex + 1][13] = "Đang làm việc";
                else
                    GVData.Rows[rIndex + 1][13] = "Đã nghỉ việc";
                GVData.Rows[rIndex + 1][14] = rData[14];
                if (rData[14].ToString() != "")
                    GVData.Rows[rIndex + 1][14] = DateTime.Parse(rData[14].ToString()).ToString("dd/MM/yyyy");
                else
                    GVData.Rows[rIndex + 1][14] = rData[14];
                if (rData[15].ToString() != "")
                    GVData.Rows[rIndex + 1][15] = DateTime.Parse(rData[15].ToString()).ToString("dd/MM/yyyy");
                else
                    GVData.Rows[rIndex + 1][15]= rData[15];

                //Ngày kết thúc
                if (rData[16].ToString() != "")
                {
                    DateTime zDate = DateTime.Parse(rData[16].ToString());
                    if (zDate >=new DateTime(dteDate.Value.Year, dteDate.Value.Month, dteDate.Value.Day, 23, 59, 59))
                    {
                        GVData.Rows[rIndex + 1][16] = "";
                        GVData.Rows[rIndex + 1][13] = "Đang làm việc";
                    }
                    else
                    {
                        GVData.Rows[rIndex + 1][16] = rData[16];
                        GVData.Rows[rIndex + 1].StyleNew.BackColor = Color.LightPink;
                    }
  
                }
                else
                    GVData.Rows[rIndex + 1][16] = rData[16];


                GVData.Rows[rIndex + 1][17] = rData[17];
                GVData.Rows[rIndex + 1][18] = rData[18];
                GVData.Rows[rIndex + 1][19] = rData[19];
                GVData.Rows[rIndex + 1][20] = rData[20];
                GVData.Rows[rIndex + 1][21] = rData[21];
                if(rData[22].ToInt()==1)
                GVData.Rows[rIndex + 1][22] = "Có";
                else
                    GVData.Rows[rIndex + 1][22] = "Không";
                if (rData[23].ToInt() == 1)
                    GVData.Rows[rIndex + 1][23] = "Bù công";
                else
                    GVData.Rows[rIndex + 1][23] = "Thời gian thực tế";
                GVData.Rows[rIndex + 1][24] = rData[0];
            }



            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 80;

            GVData.AutoSizeCols();
            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 100;
            GVData.Cols[0].StyleNew.BackColor = Color.White;
            GVData.Cols[1].StyleNew.BackColor = Color.White;
            GVData.Cols[2].StyleNew.BackColor = Color.White;

            //GVData.Cols[4].Width = 180;
            //GVData.Cols[5].Width = 50;
            //GVData.Cols[6].Width = 50;
            //GVData.Cols[7].Width = 50;
            //GVData.Cols[8].Width = 100;
            GVData.Cols[24].Visible = false;

        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Danh_Sach_Nhan_Su_Đến_Ngày" + dteDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
