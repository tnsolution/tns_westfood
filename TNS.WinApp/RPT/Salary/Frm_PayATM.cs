﻿using C1.Win.C1FlexGrid;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_PayATM : Form
    {
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Search = "";
        public Frm_PayATM()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            btn_Export.Click += Btn_Export_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            btn_Print.Click += Btn_Print_Click;
            btnClose_Panel_Message.Click += BtnClose_Panel_Message_Click;
            btn_Done.Click += Btn_Done_Click;
        }

        private void Frm_PayATM_Load(object sender, EventArgs e)
        {
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            dteDate.Value = SessionUser.Date_Work;
            //LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Chọn----");
            //LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Chọn ----");
            if (Role.AccessBranch == "2,4")
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "--Chọn tất cả--");
            else
            {
                LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey IN (" + Role.AccessBranch + ")  AND RecordStatus < 99", "");
                Cbo_Branch_SelectedIndexChanged(null, null);
            }

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            // Khởi tạo thông tin báo cáo
            LoadInfo_Report();
            Panel_Done.Visible = false;

        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Chọn tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _Search = txt_Search.Text.Trim();

            string Status = "Tìm DL form " + HeaderControl.Text + " > Tháng: " + dteDate.Value.ToString("MM/yyyy") + " > Khối:" + cbo_Branch.Text + " > BP:" + cbo_Department.Text + " > Nhóm:" + cbo_Team.Text +  " > Từ khóa:" + txt_Search.Text;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

            DisplayData();

        }
        private void DisplayData()
        {
            DataTable In_Table = new DataTable();
            In_Table = Report.List_PayATM(dteDate.Value, _BranchKey,_DepartmentKey,_TeamKey,_Search);
            InitGV_Layout(In_Table);
        }
        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            int TotalRow = TableView.Rows.Count + 2;
            int ToTalCol = 7;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            //Row Header
            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "TỔ NHÓM";
            GVData.Rows[0][3] = "SỐ THẺ";
            GVData.Rows[0][4] = "TÀI KHOẢN ATM";
            GVData.Rows[0][5] = "THỰC LÃNH";
            GVData.Rows[0][6] = "GHI CHÚ";


            //Style         
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            double Total = 0;
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                GVData.Rows[rIndex + 1][0] = rIndex + 1;
                GVData.Rows[rIndex + 1][1] = rData[0];
                GVData.Rows[rIndex + 1][2] = rData[1];
                GVData.Rows[rIndex + 1][3] = rData[2];
                GVData.Rows[rIndex + 1][4] = rData[3];
                GVData.Rows[rIndex + 1][5] = rData[4].Toe0String();
                GVData.Rows[rIndex + 1][6] = rData[5];
                Total += double.Parse(rData[4].ToString());
            }
            GVData.Rows[GVData.Rows.Count-1][1] = "TỔNG CỘNG";
            GVData.Rows[GVData.Rows.Count-1][5]= Total.Toe0String();
            GVData.Rows[GVData.Rows.Count-1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 250;
            GVData.Cols[2].Width = 100;
            GVData.Cols[3].Width = 100;
            GVData.Cols[4].Width = 120;
            GVData.Cols[5].Width = 120;
            GVData.Cols[6].Width = 60;

            GVData.Cols[5].TextAlign = TextAlignEnum.RightCenter;
        }
        #region[In]
        private void LoadInfo_Report()
        {
            //Ngày lập
            dte_RpDate.Value = SessionUser.Date_Work;
            //Tên báo cáo
            txt_RpTitle.Text = "Danh sách Chuyển khoản ngân hàng";
            //Người lập
            txt_RpNguoiLap.Text = SessionUser.UserLogin.EmployeeName;
            //Kế toán trưởng
            txt_RpKeToan.Text = "Trần Tấn Long Thạch";
            //HCNS
            txt_RpHCNS.Text = "Lê Văn Hòa";
            //Tổng giám đốc
            txt_RpGiamDoc.Text = "Nguyễn Vũ Lộc";
        }
        private void Btn_Print_Click(object sender, EventArgs e)
        {
            if (GVData.Rows.Count > 0)
            {
                Panel_Done.Visible = true;
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu", 1);
            }
        }
        private void Btn_Done_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Chuyển_Khoản_Ngân_Hàng.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    XuatExcelMau(newFile);
                    Process.Start(Path);

                    string Status = "Xuất Excel In form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);
                }

            }
        }
        private void BtnClose_Panel_Message_Click(object sender, EventArgs e)
        {
            Panel_Done.Visible = false;
        }
        private void XuatExcelMau(FileInfo newFile)
        {
            string TieuDe = txt_RpTitle.Text;
            string Font = "Time New Roman";
            string LapBang = txt_RpNguoiLap.Text;
            string KeToan = txt_RpKeToan.Text;
            string NhanSu = txt_RpHCNS.Text;
            string GiamDoc = txt_RpGiamDoc.Text;

            var fileMau = new FileInfo(Application.StartupPath + @"\FileTemplate\MauBaoCao_So1.xlsx");
            using (ExcelPackage xlPackage = new ExcelPackage(fileMau))
            {
                ExcelWorksheet workSheet = xlPackage.Workbook.Worksheets.FirstOrDefault();

                //tham số chỉ định tùy chọn
                int ColumnGV = GVData.Cols.Count;  //tổng cột cần hiển thị của Gidview
                int RowGV = GVData.Rows.Count; //số dòng dữ liệu Gidview
                int RowExcel = 6;  //dòng bắt đầu cần ghi dữ liệu trong excel               

                #region 2. DATA DỮ LIỆU                
                for (int i = 1; i <= RowGV; i++)
                {
                    int rowGV = i - 1;
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;

                        object val = GVData.Rows[rowGV][colGV];
                        if (val != null)
                        {
                            workSheet.Cells[RowExcel + i, c].Value = val.ToString();
                            //Dòng tổng thì in đậm
                            if (GVData.Rows[rowGV][0]==null || GVData.Rows[rowGV][0].ToString() == "")
                            {
                                workSheet.Cells[RowExcel + i, c].Style.Font.Color.SetColor(Color.Navy);
                                workSheet.Cells[RowExcel + i, c].Style.Font.Bold = true;
                            }
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Value = string.Empty;
                        }
                        if (c == 2 || c == 3||c == 4||c==5)
                        { // cot ten nhan vien
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        }
                        else
                        {
                            workSheet.Cells[RowExcel + i, c].Style.Numberformat.Format = "0";
                            workSheet.Cells[RowExcel + i, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }

                    }
                }

                #endregion

                #region 3. ĐỊNH DẠNG  DATA EXCEL
                //DÒNG THỨ 7 TRONG EXCEL ĐẾN DÒNG 8 TRONG EXCEL LÀ TIÊU ĐỀ BẢNG
                for (int r = 7; r < 8; r++)
                {
                    for (int c = 1; c <= ColumnGV; c++)
                    {
                        int colGV = c - 1;
                        workSheet.Cells[r, c].Style.Font.Name = Font;
                        workSheet.Cells[r, c].Style.Font.Size = 12;
                        workSheet.Cells[r, c].Style.Font.Bold = true;
                        workSheet.Cells[r, c].Style.Font.Color.SetColor(Color.Navy);
                        workSheet.Cells[r, c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        workSheet.Cells[r, c].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        workSheet.Cells[r, c].Style.WrapText = true;
                        //workSheet.Column(c).Width = 15;
                        workSheet.Column(c).AutoFit();

                    }
                }
                workSheet.Column(ColumnGV).Style.Font.Bold = true;

                #endregion

                #region[4.Trộn ngang > dọc tự động tiêu đề column báo cáo]       
                //MergeAutoExcel(workSheet, 7, 8, 1, ColumnGV);
                #endregion

                #region 1. TÊN BÁO CÁO EXCEL
                //Thêm các dường line excel
                workSheet.Cells.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                workSheet.Cells.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                // Dòng Tên báo cáo
                workSheet.Cells["A4:G4"].Merge = true;
                workSheet.Cells["A4"].Value = TieuDe.ToUpper();
                workSheet.Cells["A4"].Style.Font.Size = 16;
                workSheet.Cells["A4"].Style.Font.Name = Font;
                workSheet.Cells["A4"].Style.Font.Bold = true;
                workSheet.Cells["A4"].Style.Font.Color.SetColor(Color.Navy);
                workSheet.Cells["A4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells["A4"].Style.WrapText = true;
                workSheet.Row(4).Height = 40;
                workSheet.Cells["A4"].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                // Xóa các đường line Từ dòng 1 đến dòng tiêu đề
                workSheet.Cells["1:6"].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:6"].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells["1:5"].Style.Border.Bottom.Style = ExcelBorderStyle.None;
                #endregion

                #region[6.Khung chữ ký]
                // Ngày ký
                workSheet.Cells[RowExcel + RowGV + 2, 6].Value = "Ngày " + dte_RpDate.Value.Day + " tháng " + dte_RpDate.Value.Month + " năm " + dte_RpDate.Value.Year;
                workSheet.Cells[RowExcel + RowGV + 2, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 2, 6].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 2, 6].Style.Font.Color.SetColor(Color.Navy);

                //Tiêu đề Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 3, 2].Value = "Lập bảng";
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 2].Style.Font.Color.SetColor(Color.Black);
                // Tên Người lập bảng
                workSheet.Cells[RowExcel + RowGV + 9, 2].Value = LapBang;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 2].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 3, 3].Value = "Kế toán trưởng";
                workSheet.Cells[RowExcel + RowGV + 3, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 3].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 3].Style.Font.Color.SetColor(Color.Black);
                //Tên kế toán trưởng
                workSheet.Cells[RowExcel + RowGV + 9, 3].Value = KeToan;
                workSheet.Cells[RowExcel + RowGV + 9, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 3].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 3].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề BP HCNS
                workSheet.Cells[RowExcel + RowGV + 3, 5].Value = "BP HCNS";
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 5].Style.Font.Color.SetColor(Color.Black);
                //Tên Trưởng phòng HCNS
                workSheet.Cells[RowExcel + RowGV + 9, 5].Value = NhanSu;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 5].Style.Font.Color.SetColor(Color.Black);

                //Tiêu đề Tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 3, 6].Value = "Tổng giám đốc";
                workSheet.Cells[RowExcel + RowGV + 3, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 3, 6].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 3, 6].Style.Font.Color.SetColor(Color.Black);
                //Tên tổng giám đốc
                workSheet.Cells[RowExcel + RowGV + 9, 6].Value = GiamDoc;
                workSheet.Cells[RowExcel + RowGV + 9, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[RowExcel + RowGV + 9, 6].Style.Font.Bold = true;
                workSheet.Cells[RowExcel + RowGV + 9, 6].Style.Font.Color.SetColor(Color.Black);

                //Xóa đường line các dòng ký tên
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Left.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Right.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Top.Style = ExcelBorderStyle.None;
                workSheet.Cells[(RowExcel + RowGV + 1) + ":" + (RowExcel + RowGV + 9)].Style.Border.Bottom.Style = ExcelBorderStyle.None;

                #endregion

                #region[5.Định dạng màu,độ rộng các cột fit]
                workSheet.Column(1).Width = 7;
                workSheet.Row(7).Height = 40; //Độ cao tiêu đề
                workSheet.Column(1).Width = 7;
                workSheet.Column(2).Width = 30;
                workSheet.Column(3).Width = 20;
                workSheet.Column(4).Width = 20;
                workSheet.Column(5).Width = 20;
                workSheet.Column(6).Width = 20;
                workSheet.Column(7).Width = 20;
                #endregion

                #region[7.Tiện ích Cài đặt in]
                workSheet.PrinterSettings.PaperSize = ePaperSize.A4; //Khổ giấy
                workSheet.PrinterSettings.PrintArea = workSheet.Cells[1, 1, RowExcel + RowGV + 9, ColumnGV];//Khu vực in
                workSheet.PrinterSettings.PageOrder = ePageOrder.OverThenDown;//Kiểu in chữ Z
                workSheet.PrinterSettings.RepeatRows = workSheet.Cells["7:8"];// Tiêu đề lặp lại khi qua trang khác
                //workSheet.PrinterSettings.RepeatColumns = workSheet.Cells["A:A"];// Colunm lặp lại khi qua trang khác
                workSheet.PrinterSettings.Orientation = eOrientation.Portrait;//Trang giấy đứng
                workSheet.View.PageBreakView = true;//Đường phân cách trang in
                workSheet.PrinterSettings.FitToPage = true;//In độ cao rộng vào 1 trang duy nhất
                workSheet.PrinterSettings.FitToWidth = 1;//Chiều rộng cột đúng 1 trang
                workSheet.PrinterSettings.FitToHeight = 0;//Tự động cắt trang
                #endregion

                xlPackage.SaveAs(newFile);
            }
        }
        //cấp 1 trộn dòng cố định
        private void MergeDongExcel(ExcelWorksheet workSheet, int currentRow, int fromCol, int ToCol)
        {
            try
            {
                workSheet.Cells[currentRow, fromCol, currentRow, ToCol].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[currentRow, fromCol + 1, currentRow, ToCol].Merge = true;
            }
        }
        //cấp 2 trộn cột cố định
        private void MergeCotExcel(ExcelWorksheet workSheet, int currentColumn, int fromRow, int toRow)
        {
            try
            {
                workSheet.Cells[fromRow, currentColumn, toRow, currentColumn].Merge = true;
            }
            catch (Exception)
            {
                workSheet.Cells[fromRow + 1, currentColumn, toRow, currentColumn].Merge = true;
            }
        }
        //cấp 3 trộn kết hợp
        private void MergeAutoExcel(ExcelWorksheet workSheet, int fromRow, int toRow, int fromCol, int toCol)
        {
            string Val = workSheet.Cells[fromRow, fromCol].Value.ToString();
            int TronCot = 0;

            #region [Trộn cột :(dòng trên dòng dưới) ]

            for (int r = fromRow; r < toRow; r++)
            {
                for (int c = fromCol + 1; c < toCol; c++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();

                    if (Val == Dta)
                    {
                        TronCot++;
                    }
                    else
                    {
                        if (TronCot >= 1)
                        {
                            try
                            {
                                workSheet.Cells[r, c - TronCot - 1, r, c - 1].Merge = true;
                                TronCot = 0;
                            }
                            catch (Exception)
                            {

                            }
                        }
                        Val = Dta;
                    }
                }

                //trộn cuối cùng 
                if (Val == workSheet.Cells[r, toCol, r, toCol].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else if (TronCot >= 1)
                {
                    try
                    {
                        workSheet.Cells[r, toCol - TronCot - 1, r, toCol - 1].Merge = true;
                        TronCot = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {

                }
            }
            #endregion

            #region [Trộn dòng :(cột trái cột phải)]

            for (int c = fromCol; c <= toCol; c++)
            {
                int TronDong = 0;
                Val = workSheet.Cells[fromRow, c].Value.ToString();
                for (int r = fromRow + 1; r <= toRow; r++)
                {
                    string Dta = workSheet.Cells[r, c].Value.ToString();
                    if (Val == Dta)
                    {
                        TronDong++;
                    }
                    else
                    {
                        Val = Dta;
                    }
                }

                if (TronDong > 0)
                {
                    try
                    {
                        workSheet.Cells[fromRow, c, fromRow + TronDong, c].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }
                if (c == toCol && Val == workSheet.Cells[fromRow, c].Value.ToString())
                {
                    try
                    {
                        workSheet.Cells[toRow - TronDong, toCol, toRow, toCol].Merge = true;
                        TronDong = 0;
                    }
                    catch (Exception)
                    {

                    }
                }

            }
            #endregion
        }
        #endregion


        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách lương chuyển khoản ngân hàng.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        Access_Role_Info Role;
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
           

        }
        #endregion
    }
}
