﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.HRM
{
    public class Salary_Report_Productivity
    {
        public static DataTable SalaryProduct(DateTime FromDate, DateTime ToDate,int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"[dbo].[PivotSoLuongThanhPham] @FromDate,@ToDate,@TeamKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable SalaryMoney(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"[dbo].[PivotSoTien] @FromDate,@ToDate,@TeamKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable SalaryProduct_Ver2(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT EmployeeName, EmployeeID, StageName,
SUM(Kg + Kg_Borrow + Kg_Eat+ KgPrivate) AS [Kg],
SUM(Money+ Money_Borrow+Money_Eat+MoneyPrivate) AS [Money]		
FROM [dbo].[FTR_Order_Money] A 
LEFT JOIN IVT_Product_Stages B ON A.StageKey = B.StageKey
WHERE A.TeamKey = @TeamKey AND OrderDate BETWEEN @FromDate AND @ToDate
GROUP BY EmployeeName, EmployeeID, StageName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable SalaryProduct_V3(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();

            DataTable zTablStage = StageName(FromDate, ToDate, TeamKey); //Danh sách tên công đoạn
            string BienKieuDL = "EmployeeName NVARCHAR(255),EmployeeID NVARCHAR(255),"; //Khai báo column bảng tạm
            string BienCot = "EmployeeName,EmployeeID,"; //Column để truyền giá trị insert vào
            string NameSoLuong = "EmployeeName,EmployeeID,"; // Đặt tên cho column số lượng sản phẩm khi union vào wish as
            string NameLuongKhoan = "EmployeeName,EmployeeID,"; // Đặt tên cho column lương khoán khi union vào wish as
            string SumColumnName = "EmployeeName,EmployeeID,"; //Sum để group by bảng #TEAM trong Wish as lại

            for (int i = 0; i < zTablStage.Rows.Count; i++)
            {
                string zStageName = zTablStage.Rows[i]["StageName"].ToString();
                BienKieuDL += "[" + zStageName + "] FLOAT,";
                BienCot += "[" + zStageName + "],";
                NameSoLuong += "[" + zStageName + "] AS [" + zStageName + "_SL] ,'' [" + zStageName + "_LK],";
                NameLuongKhoan += "'' " + "[" + zStageName + "_LK],[" + zStageName + "] AS [" + zStageName + "_SL],";
                SumColumnName += "SUM([" + zStageName + "_SL]) AS [" + zStageName + "_SL],";
                SumColumnName += "SUM([" + zStageName + "_LK]) AS [" + zStageName + "_LK],";
            }
            //Cắt bỏ dấu , cuối cùng
            if (BienKieuDL.Contains(","))
            {
                BienKieuDL = BienKieuDL.Remove(BienKieuDL.LastIndexOf(","));
            }
            if (BienCot.Contains(","))
            {
                BienCot = BienCot.Remove(BienCot.LastIndexOf(","));
            }
            if (NameSoLuong.Contains(","))
            {
                NameSoLuong = NameSoLuong.Remove(NameSoLuong.LastIndexOf(","));
            }
            if (NameLuongKhoan.Contains(","))
            {
                NameLuongKhoan = NameLuongKhoan.Remove(NameLuongKhoan.LastIndexOf(","));
            }
            if (SumColumnName.Contains(","))
            {
                SumColumnName = SumColumnName.Remove(SumColumnName.LastIndexOf(","));
            }
            //Tạo bảng tạm 1 cho store PivotSoLuongThanhPham
            string zSQL = @"
CREATE TABLE #temp1 (
" + BienKieuDL + ") INSERT INTO #temp1 (" + BienCot + ") " +
"EXECUTE [PivotSoLuongThanhPham] @FromDate,@ToDate,@TeamKey ";
            //Tạo bảng tạm 2 cho store PivotSoTien
            zSQL += @"
CREATE TABLE #temp2 (
" + BienKieuDL + ") INSERT INTO #temp2 (" + BienCot + ") " +
"EXECUTE [PivotSoTien]@FromDate,@ToDate,@TeamKey; ";
            // Union 2 bảng tạm 1 và tạm 2 thành 1 bảng
            zSQL += @"WITH  #TEMP AS (
SELECT " + NameSoLuong + " FROM #temp1 UNION SELECT " + NameLuongKhoan + " FROM #temp2)  ";
            //Group by  và hiển thị bảng tạm lại
            zSQL += @" SELECT " + SumColumnName + " FROM  #TEMP  GROUP BY  EmployeeName, EmployeeID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        /// <summary>
        /// dong
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="TeamKey"></param>
        /// <returns></returns>
        public static DataTable StageName(DateTime FromDate, DateTime ToDate, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 0, 0);
            DataTable zTable = new DataTable();
            string zSQL = @"	
SELECT DISTINCT B.StageName
FROM FTR_Order_Money A 
LEFT JOIN IVT_Product_Stages B ON A.StageKey = B.StageKey	
WHERE A.TeamKey = @TeamKey AND A.RecordStatus != 99 AND B.RecordStatus != 99 
AND A.OrderDate BETWEEN @FromDate AND @ToDate	
ORDER BY B.StageName";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
