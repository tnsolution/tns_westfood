﻿namespace TNS.WinApp
{
    partial class Frm_TheKho
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_TheKho));
            this.HeaderControl = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.btnMini = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnMax = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.btnClose = new ComponentFactory.Krypton.Toolkit.ButtonSpecAny();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txt_ProductName = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Unit = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.txt_Search = new ComponentFactory.Krypton.Toolkit.KryptonTextBox();
            this.cbo_Warehouse_Search = new ComponentFactory.Krypton.Toolkit.KryptonComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dte_FromDate = new TN_Tools.TNDateTime();
            this.dte_ToDate = new TN_Tools.TNDateTime();
            this.btn_Search = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_Export = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonHeader3 = new ComponentFactory.Krypton.Toolkit.KryptonHeader();
            this.GVData = new C1.Win.C1FlexGrid.C1FlexGrid();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Warehouse_Search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).BeginInit();
            this.SuspendLayout();
            // 
            // HeaderControl
            // 
            this.HeaderControl.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecAny[] {
            this.btnMini,
            this.btnMax,
            this.btnClose});
            this.HeaderControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeaderControl.Location = new System.Drawing.Point(0, 0);
            this.HeaderControl.Name = "HeaderControl";
            this.HeaderControl.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.HeaderControl.Size = new System.Drawing.Size(1350, 42);
            this.HeaderControl.TabIndex = 220;
            this.HeaderControl.Values.Description = "";
            this.HeaderControl.Values.Heading = "THẺ KHO";
            this.HeaderControl.Values.Image = ((System.Drawing.Image)(resources.GetObject("HeaderControl.Values.Image")));
            // 
            // btnMini
            // 
            this.btnMini.Image = ((System.Drawing.Image)(resources.GetObject("btnMini.Image")));
            this.btnMini.UniqueName = "F5F06E8241504E72ABB5BEA3F6A9B753";
            // 
            // btnMax
            // 
            this.btnMax.Image = ((System.Drawing.Image)(resources.GetObject("btnMax.Image")));
            this.btnMax.UniqueName = "035D1A4881E44F58A084C31DE7352A94";
            // 
            // btnClose
            // 
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.UniqueName = "11B07C6F4E1C4F9D8B91BD924CB0EBE6";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(239)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txt_ProductName);
            this.panel1.Controls.Add(this.txt_Unit);
            this.panel1.Controls.Add(this.txt_Search);
            this.panel1.Controls.Add(this.cbo_Warehouse_Search);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.dte_FromDate);
            this.panel1.Controls.Add(this.dte_ToDate);
            this.panel1.Controls.Add(this.btn_Search);
            this.panel1.Controls.Add(this.btn_Export);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1350, 80);
            this.panel1.TabIndex = 221;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label3.Location = new System.Drawing.Point(182, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 322;
            this.label3.Text = "Tên hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label4.Location = new System.Drawing.Point(624, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 15);
            this.label4.TabIndex = 322;
            this.label4.Text = "ĐVT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label15.Location = new System.Drawing.Point(5, 43);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 15);
            this.label15.TabIndex = 322;
            this.label15.Text = "Mã hàng";
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.Location = new System.Drawing.Point(247, 34);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_ProductName.ReadOnly = true;
            this.txt_ProductName.Size = new System.Drawing.Size(371, 26);
            this.txt_ProductName.StateCommon.Border.ColorAngle = 1F;
            this.txt_ProductName.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_ProductName.StateCommon.Border.Rounding = 4;
            this.txt_ProductName.StateCommon.Border.Width = 1;
            this.txt_ProductName.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_ProductName.TabIndex = 321;
            // 
            // txt_Unit
            // 
            this.txt_Unit.Location = new System.Drawing.Point(661, 33);
            this.txt_Unit.Name = "txt_Unit";
            this.txt_Unit.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Unit.ReadOnly = true;
            this.txt_Unit.Size = new System.Drawing.Size(55, 26);
            this.txt_Unit.StateCommon.Border.ColorAngle = 1F;
            this.txt_Unit.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Unit.StateCommon.Border.Rounding = 4;
            this.txt_Unit.StateCommon.Border.Width = 1;
            this.txt_Unit.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Unit.TabIndex = 321;
            // 
            // txt_Search
            // 
            this.txt_Search.Location = new System.Drawing.Point(66, 37);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.txt_Search.Size = new System.Drawing.Size(113, 26);
            this.txt_Search.StateCommon.Border.ColorAngle = 1F;
            this.txt_Search.StateCommon.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.txt_Search.StateCommon.Border.Rounding = 4;
            this.txt_Search.StateCommon.Border.Width = 1;
            this.txt_Search.StateCommon.Content.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Search.TabIndex = 321;
            // 
            // cbo_Warehouse_Search
            // 
            this.cbo_Warehouse_Search.DropDownWidth = 119;
            this.cbo_Warehouse_Search.Location = new System.Drawing.Point(395, 6);
            this.cbo_Warehouse_Search.Name = "cbo_Warehouse_Search";
            this.cbo_Warehouse_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.cbo_Warehouse_Search.Size = new System.Drawing.Size(225, 22);
            this.cbo_Warehouse_Search.StateCommon.ComboBox.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Warehouse_Search.StateCommon.ComboBox.Border.Rounding = 4;
            this.cbo_Warehouse_Search.StateCommon.ComboBox.Border.Width = 1;
            this.cbo_Warehouse_Search.StateCommon.ComboBox.Content.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cbo_Warehouse_Search.StateCommon.Item.Border.DrawBorders = ((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders)((((ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Top | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Bottom) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Left) 
            | ComponentFactory.Krypton.Toolkit.PaletteDrawBorders.Right)));
            this.cbo_Warehouse_Search.StateCommon.Item.Border.Rounding = 4;
            this.cbo_Warehouse_Search.StateCommon.Item.Border.Width = 1;
            this.cbo_Warehouse_Search.StateCommon.Item.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 10F);
            this.cbo_Warehouse_Search.TabIndex = 320;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label1.Location = new System.Drawing.Point(205, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 15);
            this.label1.TabIndex = 319;
            this.label1.Text = "Đến";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label2.Location = new System.Drawing.Point(33, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 15);
            this.label2.TabIndex = 319;
            this.label2.Text = "Từ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(358, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 15);
            this.label6.TabIndex = 319;
            this.label6.Text = "Kho";
            // 
            // dte_FromDate
            // 
            this.dte_FromDate.CustomFormat = "dd/MM/yyyy";
            this.dte_FromDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_FromDate.Location = new System.Drawing.Point(65, 6);
            this.dte_FromDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_FromDate.Name = "dte_FromDate";
            this.dte_FromDate.Size = new System.Drawing.Size(114, 27);
            this.dte_FromDate.TabIndex = 317;
            this.dte_FromDate.Value = new System.DateTime(((long)(0)));
            // 
            // dte_ToDate
            // 
            this.dte_ToDate.CustomFormat = "dd/MM/yyyy";
            this.dte_ToDate.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.dte_ToDate.Location = new System.Drawing.Point(247, 4);
            this.dte_ToDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dte_ToDate.Name = "dte_ToDate";
            this.dte_ToDate.Size = new System.Drawing.Size(106, 27);
            this.dte_ToDate.TabIndex = 318;
            this.dte_ToDate.Value = new System.DateTime(((long)(0)));
            // 
            // btn_Search
            // 
            this.btn_Search.Location = new System.Drawing.Point(725, 13);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Search.Size = new System.Drawing.Size(106, 40);
            this.btn_Search.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Search.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Search.TabIndex = 281;
            this.btn_Search.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Search.Values.Image")));
            this.btn_Search.Values.Text = "Tìm kiếm";
            // 
            // btn_Export
            // 
            this.btn_Export.Location = new System.Drawing.Point(833, 14);
            this.btn_Export.Name = "btn_Export";
            this.btn_Export.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.btn_Export.Size = new System.Drawing.Size(100, 40);
            this.btn_Export.StateCommon.Content.Image.ImageH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.Image.ImageV = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.StateCommon.Content.LongText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Near;
            this.btn_Export.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Export.StateCommon.Content.ShortText.TextH = ComponentFactory.Krypton.Toolkit.PaletteRelativeAlign.Center;
            this.btn_Export.TabIndex = 280;
            this.btn_Export.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_Export.Values.Image")));
            this.btn_Export.Values.Text = "Xuất Excel";
            // 
            // kryptonHeader3
            // 
            this.kryptonHeader3.AutoSize = false;
            this.kryptonHeader3.Dock = System.Windows.Forms.DockStyle.Top;
            this.kryptonHeader3.Location = new System.Drawing.Point(0, 122);
            this.kryptonHeader3.Name = "kryptonHeader3";
            this.kryptonHeader3.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            this.kryptonHeader3.Size = new System.Drawing.Size(1350, 30);
            this.kryptonHeader3.StateCommon.Content.ShortText.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.kryptonHeader3.TabIndex = 289;
            this.kryptonHeader3.Values.Description = "";
            this.kryptonHeader3.Values.Heading = "DANH SÁCH";
            // 
            // GVData
            // 
            this.GVData.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None;
            this.GVData.AllowEditing = false;
            this.GVData.AllowResizing = C1.Win.C1FlexGrid.AllowResizingEnum.None;
            this.GVData.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None;
            this.GVData.AutoResize = true;
            this.GVData.ColumnInfo = "10,1,0,0,0,95,Columns:";
            this.GVData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GVData.ExtendLastCol = true;
            this.GVData.Location = new System.Drawing.Point(0, 152);
            this.GVData.Name = "GVData";
            this.GVData.Rows.DefaultSize = 19;
            this.GVData.Size = new System.Drawing.Size(1350, 409);
            this.GVData.TabIndex = 290;
            // 
            // Frm_TheKho
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1350, 561);
            this.Controls.Add(this.GVData);
            this.Controls.Add(this.kryptonHeader3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.HeaderControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Frm_TheKho";
            this.Text = "THẺ KHO";
            this.Load += new System.EventHandler(this.Frm_TheKho_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Warehouse_Search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GVData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeader HeaderControl;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMini;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnMax;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecAny btnClose;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label15;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonComboBox cbo_Warehouse_Search;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private TN_Tools.TNDateTime dte_FromDate;
        private TN_Tools.TNDateTime dte_ToDate;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Search;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_Export;
        private ComponentFactory.Krypton.Toolkit.KryptonHeader kryptonHeader3;
        private C1.Win.C1FlexGrid.C1FlexGrid GVData;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_ProductName;
        private ComponentFactory.Krypton.Toolkit.KryptonTextBox txt_Unit;
    }
}