﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.SYS
{
    public class Default_Parameter_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *FROM HRM_Default_Parameter WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(string Name)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT AutoKey,ID,Name,ApplyDate,EndDate,Parameter,Ext,Description FROM HRM_Default_Parameter WHERE ID LIKE @Name  AND   RecordStatus <> 99 ORDER BY LEN(ID), ID ASC, ApplyDate Desc,EndDate ASC    ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable Search(DateTime Date ,string Name)
        {
           DateTime zDate = new DateTime(Date.Year, Date.Month, Date.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"SELECT AutoKey,ID,Name,ApplyDate,EndDate,Parameter,Ext,Description 
                            FROM HRM_Default_Parameter 
                            WHERE ID LIKE @Name  AND   RecordStatus <> 99  AND ApplyDate <= @Date AND ( EndDate IS NULL OR @Date <=  EndDate )
                            ORDER BY LEN(ID), ID ASC, ApplyDate Desc,EndDate ASC    ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = "%" + Name + "%";
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = zDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        //public static float Get_Money_OverTime()
        //{
        //    float zResult = 0;
        //    string zSQL = @"SELECT ISNULL(Parameter,0) FROM [dbo].[HRM_Default_Parameter] WHERE ID = 'E' AND RecordStatus <> 99";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    try
        //    {
        //        SqlConnection zConnect = new SqlConnection(zConnectionString);
        //        zConnect.Open();
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
        //        zResult = float.Parse(zCommand.ExecuteScalar().ToString());
        //        zCommand.Dispose();
        //        zConnect.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        string zMessage = ex.ToString();
        //    }
        //    return zResult;
        //}

        public static int CountID(string Name)
        {
            int zResult = 0;
            string zSQL = "SELECT COUNT(ID) FROM HRM_Default_Parameter WHERE ID = @Name and  RecordStatus <> 99 ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = Name.Trim();
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        
        public static int GetKeyHangSo( DateTime Date, string ID)
        {
            int zResult = 0;
            string zSQL = "SELECT [dbo].[HRM_LayKeyHangSo] (@Date,@ID) ";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ID", SqlDbType.NVarChar).Value = ID.Trim();
                zCommand.Parameters.Add("@Date", SqlDbType.DateTime).Value = Date;
                zResult = int.Parse(zCommand.ExecuteScalar().ToString());
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zResult;
        }
        
    }
}
