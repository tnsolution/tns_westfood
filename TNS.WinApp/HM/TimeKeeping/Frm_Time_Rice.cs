﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TN.Library.System;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Time_Rice : Form
    {
        public Frm_Time_Rice()
        {
            InitializeComponent();
            btn_Save.Click += Btn_Save_Click;
            btn_New.Click += Btn_New_Click;
            btn_Del.Click += Btn_Del_Click;
            btn_Search.Click += Btn_Search_Click;
            txt_Money.KeyPress += txt_Money_KeyPress;
            txt_Rank.KeyPress += txt_Rank_KeyPress;
            GV_List.Click += GV_List_Click;
            GV_List.KeyDown += GV_List_KeyDown;
            GV_List_Layout();
        }

       

        #region[Private]
        private int _Key = 0;
        #endregion
        private void Frm_Time_Rice_Load(object sender, EventArgs e)
        {
            GV_List_LoadData();
            LoadData();
        }

        #region[Progess]

       
        private void LoadData()
        {

            Time_Rice_Info zinfo = new Time_Rice_Info(_Key);
            txt_RiceName.Text = zinfo.RiceName;
            txt_Rank.Text = zinfo.Rank.ToString();
            txt_Description.Text = zinfo.Description;
            if (zinfo.Money != 0)
                txt_Money.Text = zinfo.Money.ToString("###,###,###");
            else
                txt_Money.Text = "0";
        }

        #endregion
        private void GV_List_KeyDown(object sender, KeyEventArgs e)
        {
            string zMessage = "";
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    for (int i = 0; i < GV_List.Rows.Count; i++)
                    {
                        if (GV_List.Rows[i].Selected)
                        {
                            if (GV_List.Rows[i].Tag != null)
                            {
                                Time_Rice_Info zInfo = new Time_Rice_Info();
                                zInfo.Key = int.Parse(GV_List.Rows[i].Tag.ToString());
                                zInfo.Delete();
                                zMessage += TN_Message.Show(zInfo.Message);
                            }
                        }
                    }
                    if (zMessage == "")
                    {
                        MessageBox.Show("Đã xóa !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GV_List_LoadData();
                        _Key = 0;
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        #region[Event]
        private void Btn_Save_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (txt_RiceName.Text.Trim().Length == 0)
            {
                MessageBox.Show("Chưa nhập mã!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            float zMoney;
            if (!float.TryParse(txt_Rank.Text, out zMoney))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (Time_Rice_Data.CountID(txt_RiceName.Text.Trim().ToUpper()) > 0 && _Key== 0)
            {
                MessageBox.Show("Mã chấm cơm đã tồn tại !.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            int zRank;
            if (!int.TryParse(txt_Rank.Text, out zRank))
            {
                MessageBox.Show("Vui lòng nhập đúng định dạng số!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                Time_Rice_Info zinfo = new Time_Rice_Info(_Key);
                zinfo.RiceName = txt_RiceName.Text.Trim().ToUpper();
                zinfo.Money = float.Parse(txt_Money.Text);
                zinfo.Rank = int.Parse(txt_Rank.Text.Trim());
                zinfo.Description = txt_Description.Text.Trim();
                zinfo.Save();
                zMessage = TN_Message.Show(zinfo.Message);
                if (zMessage == "")
                {
                    MessageBox.Show("Cập nhật thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GV_List_LoadData();
                    _Key = zinfo.Key;
                    LoadData();
                }
                else
                {
                    MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            string zMessage = "";
            if (_Key == 0)
            {
                MessageBox.Show("Chưa chọn thông tin!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (MessageBox.Show("Bạn có chắc xóa thông tin này ?.", "Thông báo", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    Time_Rice_Info zinfo = new Time_Rice_Info(_Key);
                    zinfo.Delete();
                    zMessage = TN_Message.Show(zinfo.Message);
                    if (zMessage == "")
                    {
                        MessageBox.Show("Xóa thành công!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GV_List_LoadData();
                        _Key = 0;
                        LoadData();
                    }
                    else
                    {
                        MessageBox.Show(zMessage, "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            _Key = 0;
            LoadData();
            txt_RiceName.Focus();
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {

            GV_List_LoadData();
        }
        #endregion

        #region[ListView]

        private void GV_List_Layout()
        {
            // Setup Column 
            GV_List.Columns.Add("No", "STT");
            GV_List.Columns.Add("RiceName", "Mã");
            GV_List.Columns.Add("Description", "Nội dung");

            GV_List.Columns["No"].Width = 40;
            GV_List.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_List.Columns["No"].ReadOnly = true;

            GV_List.Columns["RiceName"].Width = 68;
            GV_List.Columns["RiceName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_List.Columns["Description"].Width = 195;
            GV_List.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_List.Columns["Description"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;




            GV_List.BackgroundColor = Color.White;
            GV_List.GridColor = Color.FromArgb(227, 239, 255);
            GV_List.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_List.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_List.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            GV_List.ReadOnly = true;

            GV_List.AllowUserToResizeRows = false;
            GV_List.AllowUserToResizeColumns = true;

            GV_List.RowHeadersVisible = false;
            GV_List.AllowUserToAddRows = true;
            GV_List.AllowUserToDeleteRows = false;
            GV_List.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            GV_List.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_List.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_List.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_List.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
            for (int i = 1; i <= 8; i++)
            {
                GV_List.Rows.Add();
            }

        }
        private void GV_List_LoadData()
        {

            GV_List.Rows.Clear();
            DataTable In_Table = new DataTable();
            In_Table = Time_Rice_Data.Search(txt_Search.Text);
            int i = 0;
            foreach (DataRow nRow in In_Table.Rows)
            {
                GV_List.Rows.Add();
                DataGridViewRow nRowView = GV_List.Rows[i];
                nRowView.Tag = nRow["RiceKey"].ToString().Trim();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["riceName"].Value = nRow["RiceName"].ToString().Trim();
                nRowView.Cells["Description"].Value = nRow["Description"].ToString().Trim();
                i++;
            }

        }

        #endregion

        private void GV_List_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < GV_List.Rows.Count; i++)
            {
                if (GV_List.CurrentRow.Selected == true)
                {
                    GV_List.Rows[i].DefaultCellStyle.BackColor = SystemColors.Window; // normal item

                }
                else
                {
                    GV_List.Rows[i].DefaultCellStyle.BackColor = Color.LightBlue; // highlighted item
                }
            }

            if (GV_List.CurrentRow.Tag != null)
            {
                _Key = int.Parse(GV_List.CurrentRow.Tag.ToString());
                LoadData();
            }
            else
            {
                _Key = 0;
                LoadData();
            }

        }
        private void txt_Rank_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;

            }
            else
            {
                e.Handled = true;
            }

        }
        private void txt_Money_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (Keys)e.KeyChar == Keys.Back || e.KeyChar == 46)
            {

                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }

        }

    }
}
