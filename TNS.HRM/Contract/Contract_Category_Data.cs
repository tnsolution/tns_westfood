﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.HRM
{
    public class Contract_Category_Data
    {

        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT CategoryKey, CategoryName FROM HRM_Contract_Category";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }








    }
}
