﻿using C1.Win.C1FlexGrid;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Report_20 : Form
    {
        private int _TeamKey = 0;
        public Frm_Report_20()
        {
            InitializeComponent();
            Load += Frm_Report_20_Load;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;

            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
        }

        private void Frm_Report_20_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);


            //Lấy tất cả nhóm trực tiếp sx  và khác bộ phận hỗ trợ sx
            string zSQL = @"
SELECT TeamKey, TeamID +'-'+ TeamName AS TeamName  
FROM SYS_Team 
WHERE RecordStatus <> 99 
AND BranchKey = 4 
AND  DepartmentKey != 26 AND TeamKey !=98
ORDER BY Rank";
            LoadDataToToolbox.KryptonComboBox(cboTeam, zSQL, "");

            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            this.DoubleBuffered = true;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;

            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string zTeamName = "";
            if (cboTeam.SelectedValue.ToInt() != 0)
            {
                string[] s = cboTeam.Text.Trim().Split('-');
                zTeamName = "_Nhóm_" + s[0];
            }
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Bảng_thông_tin_công_nhân_theo_công_đoạn_số_lượng_lương khoán_tại_tổ_chính" + zTeamName + "_Ngày_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                     Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            txt_TitleGrid.Text = "Thông tin chi tiết lương, năng suất công nhân tổ " + cboTeam.Text + " thực hiện các công việc/ đoạn trong ngày " + dte_FromDate.Value.ToString("dd/MM/yyyy");
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _TeamKey = cboTeam.SelectedValue.ToInt();

            string Status = "Tìm DL form " + HeaderControl.Text + " > ngày: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > Nhóm:" + cboTeam.Text ;
             Application_Data.Save_LogApplication(SessionUser.UserLogin.Key,SessionUser.UserLogin.EmployeeKey, Status, 3);

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DateTime FromDate = new DateTime(dte_FromDate.Value.Year, dte_FromDate.Value.Month, dte_FromDate.Value.Day, 0, 0, 0);
            DateTime ToDate = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);

            DataTable zTable = Report.TRANPHAM1V3(FromDate, ToDate, _TeamKey.ToString());

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable TableView = zPvt.Generate_NoSort("NOIDUNG", "CONGNHAN", new string[] { "TG", "SL", "LK" }, "CÔNG VIỆC", "TỔNG", "TỔNG", new string[] { "TG", "SL", "LK" });
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(TableView);
                }));
            }

        }
        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = TableView.Rows.Count + 3;
            int ToTalCol = TableView.Columns.Count + 3;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);

            CellRange zCellRange = GVData.GetCellRange(0, 0, 3, 0);
            zCellRange.Data = "STT";

            zCellRange = GVData.GetCellRange(0, 1, 3, 1);
            zCellRange.Data = "HỌ TÊN";

            zCellRange = GVData.GetCellRange(0, 2, 3, 2);
            zCellRange.Data = "SỐ THẺ";

            zCellRange = GVData.GetCellRange(0, 3, 3, 3);
            zCellRange.Data = "TỔ NHÓM";

            //Row Header
            int ColStart = 4;
            for (int i = 1; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (strCaption.Length > 2)
                {
                    GVData.Rows[0][ColStart] = strCaption[0];
                    GVData.Rows[1][ColStart] = strCaption[1].Ton0String();
                    GVData.Rows[2][ColStart] = strCaption[2];
                }
                else
                {
                    CellRange CellRange = GVData.GetCellRange(2, ColStart, 3, ColStart);
                    CellRange.Data = strCaption[1];
                }
                ColStart++;
            }

            // Thêm 1 cột để sắp xếp
            //TableView.Columns.Add("Rank");
            //TableView.Columns.Add("Type");
            //for (int i = 0; i < TableView.Rows.Count; i++)
            //{
            //    if (i < TableView.Rows.Count - 1)
            //    {
            //        DataRow row = TableView.Rows[i];
            //        string rColumn0 = row[0].ToString();
            //        string[] temp = rColumn0.Split('|');
            //        string EmployeeID = temp[2];

            //        TableView.Rows[i]["Rank"] = ConvertIDRank(EmployeeID);
            //        string Type= temp[0];

            //        TableView.Rows[i]["Type"] = Type ;
            //    }
            //    else
            //    {
            //        TableView.Rows[i]["Rank"] = "Z00000000"; //dòng tổng để cuối cùng
            //        TableView.Rows[i]["Type"] = "3";
            //    }
            //}
            //DataView dv = TableView.DefaultView;
            //dv.Sort = "Type ASC, Rank ASC";
            //TableView = dv.ToTable();
            //TableView.Columns.Remove("Rank");
            //TableView.Columns.Remove("Type");
            //--sắp xếp xong xóa cột sắp xếp đi

            ColStart = 3;// add data vào cột thứ 2 trở đi
            int RowStart = 3;//add dòng từ số thứ tự 4 trở đi
            for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            {
                DataRow rData = TableView.Rows[rIndex];

                if (rData[0].ToString().Contains("|"))
                {
                    string[] strTemp = rData[0].ToString().Split('|');
                    GVData.Rows[rIndex + RowStart][0] = rIndex + 1;
                    GVData.Rows[rIndex + RowStart][1] = strTemp[1]; // ten cong nhan
                    GVData.Rows[rIndex + RowStart][2] = strTemp[2]; // ma cong nhan
                    GVData.Rows[rIndex + RowStart][3] = strTemp[3]; // ten nhóm

                    if (strTemp[0] == "1")
                    {
                        GVData.Rows[rIndex + RowStart].StyleNew.BackColor = Color.Plum;
                    }
                    else
                    {
                        GVData.Rows[rIndex + RowStart].StyleNew.BackColor = Color.White;
                    }
                }

                for (int cIndex = 1; cIndex < TableView.Columns.Count; cIndex++)
                {
                    GVData.Rows[RowStart + rIndex][ColStart + cIndex] = FormatMoney(rData[cIndex]);
                }
            }

            //TỔNG DÒNG
            zCellRange = GVData.GetCellRange(GVData.Rows.Count - 1, 0, GVData.Rows.Count - 1, 2);
            zCellRange.Data = "TỔNG CỘNG";

            //TỔNG CỘT
            zCellRange = GVData.GetCellRange(0, ToTalCol - 3, 0, ToTalCol - 1);
            zCellRange.Data = "TỔNG CỘNG";

            //Style

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;


            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 60;
            //Trộn dòng 1
            GVData.Rows[1].AllowMerging = true;
            GVData.Rows[1].Height = 30;
            //Trộn dòng 2
            GVData.Rows[2].AllowMerging = true;
            //Trộn dòng 3
            GVData.Rows[3].AllowMerging = true;

            //Trộn dòng cuối
            GVData.Rows[GVData.Rows.Count - 1].AllowMerging = true;

            //Trộn cột đầu
            GVData.Cols[0].AllowMerging = true;
            GVData.Cols[0].Width = 35;
            GVData.Cols[1].AllowMerging = true;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].AllowMerging = true;
            GVData.Cols[2].Width = 100;
            GVData.Cols[3].AllowMerging = true;
            GVData.Cols[3].Width = 100;
            //Trộn 2 cột cuối
            GVData.Cols[ToTalCol - 1].AllowMerging = true;
            GVData.Cols[ToTalCol - 2].AllowMerging = true;
            GVData.Cols[ToTalCol - 3].AllowMerging = true;
            //Freeze Row and Column
            GVData.Rows.Fixed = 3;
            GVData.Cols.Frozen = 4;
            //GVData.Cols.Frozen = 4;

            //GVData.AutoSizeCols(1, GVData.Cols.Count - 1, 10);
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            for (int i = 4; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            //In đậm các dòng tổng
            GVData.Rows[TotalRow - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        //Chuyển số thành dãy chuỗi
        string ConvertIDRank(string ID)
        {
            string zResult = "";
            string temp = ID.Replace("A", "");
            string s = "";
            //Thiếu bao nhieu số kí tự thì thêm vào bấy nhiêu cho đủ chuỗi VD: 11--->0011, A11--->1000011
            for (int i = 0; i < 4 - temp.Trim().Length; i++)
            {
                s += "0";
            }
            if (ID.Substring(0, 1) == "A")
            {
                zResult = "100" + s + temp;
            }
            else
            {
                zResult = s + temp;
            }

            return zResult;
        }
        string FormatMoney(object input)
        {
            try
            {
                if (input.ToString() != "0")
                {
                    double zResult = double.Parse(input.ToString());
                    return zResult.ToString("n", CultureInfo.GetCultureInfo("vi-VN"));
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
