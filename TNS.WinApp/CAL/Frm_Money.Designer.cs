﻿namespace TN_WinApp
{
    partial class Frm_Money
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Money));
            this.lbl_Teams = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.GV_Team = new System.Windows.Forms.DataGridView();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtMoneyDifGeneral = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTimeDifGeneral = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTimeDifBorrowEat = new System.Windows.Forms.TextBox();
            this.btn_Count_All = new System.Windows.Forms.Button();
            this.btn_Save_All = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.LV_Teams = new System.Windows.Forms.ListView();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.txt_TotalTimeTeam = new System.Windows.Forms.TextBox();
            this.txt_ToTalMoneyTeam = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.dte_Teams = new TNLibrary.SYS.Forms.TNDateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.GV_Employee_Month = new System.Windows.Forms.DataGridView();
            this.Panel_Hide = new System.Windows.Forms.Panel();
            this.btn_Hide_Log = new System.Windows.Forms.Label();
            this.btn_Show_Log = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.LB_Log = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn_Count_Month = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_Money_Month = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dte_Month = new TNLibrary.SYS.Forms.TNDateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.LV_Teams_Month = new System.Windows.Forms.ListView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Team)).BeginInit();
            this.groupBox17.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee_Month)).BeginInit();
            this.Panel_Hide.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_Teams
            // 
            this.lbl_Teams.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(199)))), ((int)(((byte)(231)))));
            this.lbl_Teams.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_Teams.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Teams.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.lbl_Teams.Location = new System.Drawing.Point(0, 0);
            this.lbl_Teams.Name = "lbl_Teams";
            this.lbl_Teams.Size = new System.Drawing.Size(1028, 39);
            this.lbl_Teams.TabIndex = 136;
            this.lbl_Teams.Text = "TỔNG HỢP LƯƠNG NĂNG SUẤT";
            this.lbl_Teams.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 39);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1028, 466);
            this.tabControl1.TabIndex = 137;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage1.Controls.Add(this.groupBox18);
            this.tabPage1.Controls.Add(this.groupBox17);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1020, 440);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "TỔNG HỢP LƯƠNG NĂNG SUẤT NGÀY";
            // 
            // groupBox18
            // 
            this.groupBox18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox18.Controls.Add(this.GV_Team);
            this.groupBox18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox18.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox18.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox18.Location = new System.Drawing.Point(292, 3);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(725, 346);
            this.groupBox18.TabIndex = 204;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Thông tin công nhân";
            // 
            // GV_Team
            // 
            this.GV_Team.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Team.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Team.Location = new System.Drawing.Point(3, 17);
            this.GV_Team.Name = "GV_Team";
            this.GV_Team.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Team.Size = new System.Drawing.Size(719, 326);
            this.GV_Team.TabIndex = 0;
            // 
            // groupBox17
            // 
            this.groupBox17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox17.Controls.Add(this.label6);
            this.groupBox17.Controls.Add(this.txtMoneyDifGeneral);
            this.groupBox17.Controls.Add(this.label5);
            this.groupBox17.Controls.Add(this.txtTimeDifGeneral);
            this.groupBox17.Controls.Add(this.label2);
            this.groupBox17.Controls.Add(this.txtTimeDifBorrowEat);
            this.groupBox17.Controls.Add(this.btn_Count_All);
            this.groupBox17.Controls.Add(this.btn_Save_All);
            this.groupBox17.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox17.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox17.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox17.Location = new System.Drawing.Point(292, 349);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(725, 88);
            this.groupBox17.TabIndex = 0;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Thao tác";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(6, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(181, 18);
            this.label6.TabIndex = 151;
            this.label6.Text = "Tổng Tiền Làm Việc Khác";
            // 
            // txtMoneyDifGeneral
            // 
            this.txtMoneyDifGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtMoneyDifGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtMoneyDifGeneral.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtMoneyDifGeneral.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtMoneyDifGeneral.Location = new System.Drawing.Point(193, 64);
            this.txtMoneyDifGeneral.Name = "txtMoneyDifGeneral";
            this.txtMoneyDifGeneral.Size = new System.Drawing.Size(123, 21);
            this.txtMoneyDifGeneral.TabIndex = 150;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label5.Location = new System.Drawing.Point(6, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 18);
            this.label5.TabIndex = 149;
            this.label5.Text = "TG Trung Bình Làm Việc Khác";
            // 
            // txtTimeDifGeneral
            // 
            this.txtTimeDifGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTimeDifGeneral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtTimeDifGeneral.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtTimeDifGeneral.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtTimeDifGeneral.Location = new System.Drawing.Point(193, 39);
            this.txtTimeDifGeneral.Name = "txtTimeDifGeneral";
            this.txtTimeDifGeneral.Size = new System.Drawing.Size(123, 21);
            this.txtTimeDifGeneral.TabIndex = 148;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(6, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 18);
            this.label2.TabIndex = 147;
            this.label2.Text = "Tổng  TG Việc Khác (Ăn Chung)";
            // 
            // txtTimeDifBorrowEat
            // 
            this.txtTimeDifBorrowEat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtTimeDifBorrowEat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txtTimeDifBorrowEat.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txtTimeDifBorrowEat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtTimeDifBorrowEat.Location = new System.Drawing.Point(193, 16);
            this.txtTimeDifBorrowEat.Name = "txtTimeDifBorrowEat";
            this.txtTimeDifBorrowEat.Size = new System.Drawing.Size(123, 21);
            this.txtTimeDifBorrowEat.TabIndex = 146;
            // 
            // btn_Count_All
            // 
            this.btn_Count_All.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count_All.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Count_All.BackgroundImage")));
            this.btn_Count_All.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Count_All.FlatAppearance.BorderSize = 0;
            this.btn_Count_All.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Count_All.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Count_All.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Count_All.Location = new System.Drawing.Point(672, 40);
            this.btn_Count_All.Name = "btn_Count_All";
            this.btn_Count_All.Size = new System.Drawing.Size(48, 49);
            this.btn_Count_All.TabIndex = 0;
            this.btn_Count_All.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Count_All.UseVisualStyleBackColor = true;
            // 
            // btn_Save_All
            // 
            this.btn_Save_All.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save_All.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Save_All.BackgroundImage")));
            this.btn_Save_All.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Save_All.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_Save_All.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.btn_Save_All.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.btn_Save_All.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.btn_Save_All.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Save_All.Font = new System.Drawing.Font("Arial", 7F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Save_All.ForeColor = System.Drawing.Color.Black;
            this.btn_Save_All.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Save_All.Location = new System.Drawing.Point(653, 53);
            this.btn_Save_All.Name = "btn_Save_All";
            this.btn_Save_All.Size = new System.Drawing.Size(67, 25);
            this.btn_Save_All.TabIndex = 1;
            this.btn_Save_All.Text = "CHỐT";
            this.btn_Save_All.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.groupBox16);
            this.panel6.Controls.Add(this.groupBox14);
            this.panel6.Controls.Add(this.groupBox13);
            this.panel6.Controls.Add(this.label47);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(289, 434);
            this.panel6.TabIndex = 138;
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox16.Controls.Add(this.LV_Teams);
            this.groupBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox16.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox16.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox16.Location = new System.Drawing.Point(0, 59);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(284, 306);
            this.groupBox16.TabIndex = 1;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Thông tin nhóm";
            // 
            // LV_Teams
            // 
            this.LV_Teams.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Teams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Teams.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Teams.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Teams.FullRowSelect = true;
            this.LV_Teams.GridLines = true;
            this.LV_Teams.Location = new System.Drawing.Point(3, 17);
            this.LV_Teams.Name = "LV_Teams";
            this.LV_Teams.Size = new System.Drawing.Size(278, 286);
            this.LV_Teams.TabIndex = 0;
            this.LV_Teams.UseCompatibleStateImageBehavior = false;
            this.LV_Teams.View = System.Windows.Forms.View.Details;
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.White;
            this.groupBox14.Controls.Add(this.label39);
            this.groupBox14.Controls.Add(this.label41);
            this.groupBox14.Controls.Add(this.txt_TotalTimeTeam);
            this.groupBox14.Controls.Add(this.txt_ToTalMoneyTeam);
            this.groupBox14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox14.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.groupBox14.Location = new System.Drawing.Point(0, 365);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(284, 69);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Thông tin chi tiết";
            // 
            // label39
            // 
            this.label39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label39.Location = new System.Drawing.Point(6, 23);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 15);
            this.label39.TabIndex = 144;
            this.label39.Text = "Tổng Số Tiền";
            // 
            // label41
            // 
            this.label41.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label41.Location = new System.Drawing.Point(6, 50);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(110, 15);
            this.label41.TabIndex = 145;
            this.label41.Text = "Tổng Số Thời Gian";
            // 
            // txt_TotalTimeTeam
            // 
            this.txt_TotalTimeTeam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_TotalTimeTeam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_TotalTimeTeam.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_TotalTimeTeam.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_TotalTimeTeam.Location = new System.Drawing.Point(136, 44);
            this.txt_TotalTimeTeam.Name = "txt_TotalTimeTeam";
            this.txt_TotalTimeTeam.Size = new System.Drawing.Size(123, 21);
            this.txt_TotalTimeTeam.TabIndex = 1;
            // 
            // txt_ToTalMoneyTeam
            // 
            this.txt_ToTalMoneyTeam.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_ToTalMoneyTeam.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_ToTalMoneyTeam.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_ToTalMoneyTeam.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_ToTalMoneyTeam.Location = new System.Drawing.Point(136, 17);
            this.txt_ToTalMoneyTeam.Name = "txt_ToTalMoneyTeam";
            this.txt_ToTalMoneyTeam.Size = new System.Drawing.Size(123, 21);
            this.txt_ToTalMoneyTeam.TabIndex = 0;
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox13.Controls.Add(this.dte_Teams);
            this.groupBox13.Controls.Add(this.label35);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox13.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox13.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox13.Location = new System.Drawing.Point(0, 0);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(284, 59);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Chọn Ngày";
            // 
            // dte_Teams
            // 
            this.dte_Teams.CustomFormat = "dd/MM/yyyy";
            this.dte_Teams.Location = new System.Drawing.Point(85, 20);
            this.dte_Teams.Name = "dte_Teams";
            this.dte_Teams.Size = new System.Drawing.Size(121, 27);
            this.dte_Teams.TabIndex = 0;
            this.dte_Teams.Value = new System.DateTime(((long)(0)));
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label35.Location = new System.Drawing.Point(32, 25);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 15);
            this.label35.TabIndex = 149;
            this.label35.Text = "Ngày";
            // 
            // label47
            // 
            this.label47.BackColor = System.Drawing.Color.DodgerBlue;
            this.label47.Dock = System.Windows.Forms.DockStyle.Right;
            this.label47.Location = new System.Drawing.Point(284, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(5, 434);
            this.label47.TabIndex = 3;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(178)))), ((int)(((byte)(229)))));
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1020, 440);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "TỔNG HỢP LƯƠNG NĂNG SUẤT THÁNG";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox4.Controls.Add(this.GV_Employee_Month);
            this.groupBox4.Controls.Add(this.Panel_Hide);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox4.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox4.Location = new System.Drawing.Point(292, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(385, 359);
            this.groupBox4.TabIndex = 206;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Thông tin công nhân";
            // 
            // GV_Employee_Month
            // 
            this.GV_Employee_Month.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GV_Employee_Month.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GV_Employee_Month.Location = new System.Drawing.Point(3, 54);
            this.GV_Employee_Month.Name = "GV_Employee_Month";
            this.GV_Employee_Month.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GV_Employee_Month.Size = new System.Drawing.Size(379, 302);
            this.GV_Employee_Month.TabIndex = 149;
            // 
            // Panel_Hide
            // 
            this.Panel_Hide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(159)))), ((int)(((byte)(203)))));
            this.Panel_Hide.Controls.Add(this.btn_Hide_Log);
            this.Panel_Hide.Controls.Add(this.btn_Show_Log);
            this.Panel_Hide.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Hide.Location = new System.Drawing.Point(3, 17);
            this.Panel_Hide.Name = "Panel_Hide";
            this.Panel_Hide.Size = new System.Drawing.Size(379, 37);
            this.Panel_Hide.TabIndex = 150;
            // 
            // btn_Hide_Log
            // 
            this.btn_Hide_Log.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Hide_Log.Image = ((System.Drawing.Image)(resources.GetObject("btn_Hide_Log.Image")));
            this.btn_Hide_Log.Location = new System.Drawing.Point(339, 1);
            this.btn_Hide_Log.Name = "btn_Hide_Log";
            this.btn_Hide_Log.Size = new System.Drawing.Size(37, 33);
            this.btn_Hide_Log.TabIndex = 4;
            // 
            // btn_Show_Log
            // 
            this.btn_Show_Log.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Show_Log.Image = ((System.Drawing.Image)(resources.GetObject("btn_Show_Log.Image")));
            this.btn_Show_Log.Location = new System.Drawing.Point(339, 1);
            this.btn_Show_Log.Name = "btn_Show_Log";
            this.btn_Show_Log.Size = new System.Drawing.Size(37, 33);
            this.btn_Show_Log.TabIndex = 3;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox6.Controls.Add(this.LB_Log);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox6.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox6.Location = new System.Drawing.Point(677, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(340, 359);
            this.groupBox6.TabIndex = 207;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Thông tin thực hiện";
            // 
            // LB_Log
            // 
            this.LB_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LB_Log.FormattingEnabled = true;
            this.LB_Log.ItemHeight = 15;
            this.LB_Log.Location = new System.Drawing.Point(3, 17);
            this.LB_Log.Name = "LB_Log";
            this.LB_Log.Size = new System.Drawing.Size(334, 339);
            this.LB_Log.TabIndex = 208;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox5.Controls.Add(this.btn_Count_Month);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox5.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox5.Location = new System.Drawing.Point(292, 362);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(725, 75);
            this.groupBox5.TabIndex = 205;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Thao tác";
            // 
            // btn_Count_Month
            // 
            this.btn_Count_Month.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Count_Month.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_Count_Month.BackgroundImage")));
            this.btn_Count_Month.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Count_Month.FlatAppearance.BorderSize = 0;
            this.btn_Count_Month.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Count_Month.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.btn_Count_Month.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Count_Month.Location = new System.Drawing.Point(671, 20);
            this.btn_Count_Month.Name = "btn_Count_Month";
            this.btn_Count_Month.Size = new System.Drawing.Size(48, 49);
            this.btn_Count_Month.TabIndex = 18;
            this.btn_Count_Month.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_Count_Month.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(289, 434);
            this.panel1.TabIndex = 139;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txt_Money_Month);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(89)))), ((int)(((byte)(136)))));
            this.groupBox1.Location = new System.Drawing.Point(0, 365);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(284, 69);
            this.groupBox1.TabIndex = 174;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin chi tiết";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(24, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 15);
            this.label1.TabIndex = 144;
            this.label1.Text = "Tổng Số Tiền";
            // 
            // txt_Money_Month
            // 
            this.txt_Money_Month.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txt_Money_Month.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(225)))));
            this.txt_Money_Month.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.txt_Money_Month.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_Money_Month.Location = new System.Drawing.Point(110, 20);
            this.txt_Money_Month.Name = "txt_Money_Month";
            this.txt_Money_Month.Size = new System.Drawing.Size(156, 21);
            this.txt_Money_Month.TabIndex = 153;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox2.Controls.Add(this.dte_Month);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox2.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(281, 59);
            this.groupBox2.TabIndex = 151;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Chọn Ngày";
            // 
            // dte_Month
            // 
            this.dte_Month.CustomFormat = "dd/MM/yyyy";
            this.dte_Month.Location = new System.Drawing.Point(73, 20);
            this.dte_Month.Name = "dte_Month";
            this.dte_Month.Size = new System.Drawing.Size(141, 31);
            this.dte_Month.TabIndex = 157;
            this.dte_Month.Value = new System.DateTime(((long)(0)));
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(32, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 15);
            this.label3.TabIndex = 149;
            this.label3.Text = "Ngày";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.DodgerBlue;
            this.label4.Dock = System.Windows.Forms.DockStyle.Right;
            this.label4.Location = new System.Drawing.Point(284, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(5, 434);
            this.label4.TabIndex = 173;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(220)))), ((int)(((byte)(240)))));
            this.groupBox3.Controls.Add(this.LV_Teams_Month);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox3.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox3.Location = new System.Drawing.Point(0, 68);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(289, 291);
            this.groupBox3.TabIndex = 150;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Thông tin nhóm";
            // 
            // LV_Teams_Month
            // 
            this.LV_Teams_Month.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LV_Teams_Month.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LV_Teams_Month.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LV_Teams_Month.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LV_Teams_Month.FullRowSelect = true;
            this.LV_Teams_Month.GridLines = true;
            this.LV_Teams_Month.Location = new System.Drawing.Point(3, 17);
            this.LV_Teams_Month.Name = "LV_Teams_Month";
            this.LV_Teams_Month.Size = new System.Drawing.Size(283, 271);
            this.LV_Teams_Month.TabIndex = 149;
            this.LV_Teams_Month.UseCompatibleStateImageBehavior = false;
            this.LV_Teams_Month.View = System.Windows.Forms.View.Details;
            // 
            // Frm_Money
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 505);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lbl_Teams);
            this.Name = "Frm_Money";
            this.Text = "TỔNG HỢP LƯƠNG NĂNG SUẤT";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Frm_Money_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Team)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.groupBox16.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GV_Employee_Month)).EndInit();
            this.Panel_Hide.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbl_Teams;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox txt_TotalTimeTeam;
        private System.Windows.Forms.TextBox txt_ToTalMoneyTeam;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.ListView LV_Teams;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.DataGridView GV_Team;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button btn_Count_All;
        private System.Windows.Forms.Button btn_Save_All;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView GV_Employee_Month;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btn_Count_Month;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_Money_Month;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ListView LV_Teams_Month;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ListBox LB_Log;
        private System.Windows.Forms.Panel Panel_Hide;
        private System.Windows.Forms.Label btn_Hide_Log;
        private System.Windows.Forms.Label btn_Show_Log;
        private TNLibrary.SYS.Forms.TNDateTimePicker dte_Teams;
        private TNLibrary.SYS.Forms.TNDateTimePicker dte_Month;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTimeDifBorrowEat;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTimeDifGeneral;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMoneyDifGeneral;
    }
}