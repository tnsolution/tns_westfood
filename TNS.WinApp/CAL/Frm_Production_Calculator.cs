﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.CORE;
using TNS.SYS;
/*
 
    1 chọn ngày
    2 chọn nhóm
    3 chọn công việc
    4 tính....
     
     */
namespace TNS.WinApp
{
    public partial class Frm_Production_Calculator : Form
    {
        #region[Khai báo hằng số]
        const int _Hour = 8;                    //thời gian làm việc hành chính
        const int _DayOfWeek = 2;            //hệ số chủ nhật
        const float _MMoney = 116000;      //tiền danh cho công nhân nam
        const float _FMoney = 104000;       //tiền danh cho công nhân nữ
        const float _OMoney = 7000;        //tiền dư giờ
        const int _TeamSCCD = 88;       //
        const int _TeamSCDH = 89;       //
        #endregion

        //Biến tính tại tổ
        string _OrderKey = "";
        int _TeamKey = 0;
        int _WorkKey = 0;
        //Biến tính công việc khác
        string _OrderDifKey = "";
        int _TeamDifKey = 0;
        int _WorkDifKey = 0;

        //Biến tự động chạy
        private int _Count;

        public Frm_Production_Calculator()
        {
            InitializeComponent();
            this.DoubleBuffered = true;

            InitTab1();
            InitTab2();
        }
        private void Frm_Calculation_Productivity_Load(object sender, EventArgs e)
        {
            dte_Date1.Value = SessionUser.Date_Work;
            dte_Date2.Value = SessionUser.Date_Work;

            btn_Save.Visible = false;
            btn_Save_Dif.Visible = false;

            LoadDataToToolbox.ComboBoxData(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Choose Teams ----");
            LoadDataToToolbox.ComboBoxData(cbo_Team_Dif, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey = 98 AND RecordStatus < 99", "---- Choose Teams ----");

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region[HÀM LAYOUT SƯ DUNG CHUNG]
        //Lay out Listview List Đơn hang
        private void InitLayout_LV(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Đơn Hàng";
            colHead.Width = 150;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tình Trạng";
            colHead.Width = 100;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitData_LV(ListView LV, DataTable Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;
            LV.Items.Clear();
            int n = Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = Table.Rows[i];
                lvi.Tag = nRow["OrderKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderID"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                if (nRow["WorkStatus"].ToInt() == 1)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Đã thực hiện";
                    lvi.SubItems.Add(lvsi);
                }
                if (nRow["WorkStatus"].ToInt() == 0)
                {
                    lvsi = new ListViewItem.ListViewSubItem();
                    lvsi.Text = "Đang thực hiện";
                    lvi.SubItems.Add(lvsi);
                }

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["OrderDate"].ToDateString();
                lvi.SubItems.Add(lvsi);

                float zQuantity = nRow["Quantity"].ToFloat();
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = zQuantity.ToString();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["Price"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TotalBasket"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TotalKg"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TotalTime"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["RateOrder"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["RateDay"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }
        //Layout Gidview chi tiết hệ số
        private void InitLayout_GVRate(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("CategoryName", "Tên Hệ Số");
            GV.Columns.Add("Rate", "Hệ Số");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["CategoryName"].Width = 250;
            GV.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Rate"].Width = 80;
            GV.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Rate"].ReadOnly = true;
        }
        private void InitData_GVRate(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();

            for (int i = 0; i < Table.Rows.Count; i++)
            {
                DataRow r = Table.Rows[i];
                string RateName = r["RateName"].ToString();
                float RateValue = r["Rate"].ToFloat();

                GV.Rows.Add();
                DataGridViewRow GvRow = GV.Rows[i];
                GvRow.Cells["No"].Value = (i + 1).ToString();
                GvRow.Cells["CategoryName"].Value = RateName;
                GvRow.Cells["Rate"].Value = RateValue.ToString("0.00");
            }

        }
        //Layout Gidview chi tiết nhân viên
        private void InitLayout_GVData(DataGridView GV)
        {
            DataGridViewCheckBoxColumn zColumn;
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeID", "Mã NV");
            GV.Columns.Add("EmployeeName", "Tên Nhân Viên");
            GV.Columns.Add("Basket", "Số Rổ");
            GV.Columns.Add("Kilogram", "SL nguyên Liệu");
            GV.Columns.Add("Time", "Thời Gian");
            GV.Columns.Add("KgProduct", "SL thành Phẩm");
            GV.Columns.Add("Money", "Tiền");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Borrow";
            zColumn.HeaderText = "Mượn Người";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Private";
            zColumn.HeaderText = "Ăn Riêng";
            GV.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Share";
            zColumn.HeaderText = "Ăn Chung";
            GV.Columns.Add(zColumn);

            GV.Columns.Add("Gender", "Nam/Nữ");
            GV.Columns.Add("Message", "Thông Báo");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV.Columns["EmployeeID"].Width = 70;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 170;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;
            GV.Columns["EmployeeName"].ReadOnly = true;

            GV.Columns["Basket"].Width = 80;
            GV.Columns["Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Basket"].ReadOnly = true;

            GV.Columns["Kilogram"].Width = 170;
            GV.Columns["Kilogram"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Kilogram"].ReadOnly = true;

            GV.Columns["KgProduct"].Width = 170;
            GV.Columns["KgProduct"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["KgProduct"].ReadOnly = true;


            GV.Columns["Time"].Width = 80;
            GV.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Time"].ReadOnly = true;

            GV.Columns["Money"].Width = 120;
            GV.Columns["Money"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV.Columns["Money"].ReadOnly = true;

            GV.Columns["Borrow"].Width = 90;
            GV.Columns["Borrow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Borrow"].ReadOnly = true;

            GV.Columns["Private"].Width = 80;
            GV.Columns["Private"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Private"].ReadOnly = true;

            GV.Columns["Share"].Width = 80;
            GV.Columns["Share"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Share"].ReadOnly = true;

            GV.Columns["Gender"].Visible = false;
            GV.Columns["Gender"].Width = 0;
            GV.Columns["Gender"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Gender"].ReadOnly = true;

            GV.Columns["Message"].Width = 80;
            GV.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["Message"].ReadOnly = true;

        }
        private void InitData_GVData(DataGridView GV, DataTable Table)
        {
            GV.Rows.Clear();
            for (int i = 0; i < Table.Rows.Count; i++)
            {
                GV.Rows.Add();
                DataGridViewRow nRowView = GV.Rows[i];

                DataRow r = Table.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["EmployeeID"].Tag = r["EmployeeKey"].ToString().Trim();
                nRowView.Cells["EmployeeID"].Value = r["EmployeeID"].ToString().Trim();
                nRowView.Cells["EmployeeName"].Value = r["EmployeeName"].ToString().Trim();
                nRowView.Cells["Basket"].Value = r["Basket"].Ton1String();
                nRowView.Cells["Kilogram"].Value = r["Kilogram"].Ton1String();
                nRowView.Cells["KgProduct"].Value = r["KgProduct"].Ton1String();
                nRowView.Cells["Time"].Value = r["Time"].Ton1String();
                nRowView.Cells["Money"].Value = r["Money"].Ton1String();
                if (r["Borrow"].ToInt() == 1)
                {
                    nRowView.DefaultCellStyle.BackColor = Color.LightBlue;
                    nRowView.Cells["Borrow"].Value = true;
                }
                if (r["Private"].ToInt() == 1)
                {
                    nRowView.Cells["Private"].Value = true;
                }
                if (r["Share"].ToInt() == 1)
                {
                    nRowView.Cells["Share"].Value = true;
                }
                nRowView.Cells["Gender"].Value = r["Gender"].ToString();
            }

        }
        private void InitData_GVSum(DataGridView GV)
        {
            float totalBasket = 0;
            float totalKilogram = 0;
            float totaltime = 0;
            float totalkgProduct = 0;
            float totalmoney = 0;

            int no = GV.Rows.Count - 1;
            GV.Rows[no].Cells["No"].Value = "";
            GV.Rows[no].Cells["EmployeeID"].Value = "";
            GV.Rows[no].Cells["EmployeeName"].Value = "TỔNG";

            for (int k = 0; k < GV.Rows.Count - 1; k++)
            {
                totalBasket += GV.Rows[k].Cells["Basket"].Value.ToFloat();
                totalKilogram += GV.Rows[k].Cells["Kilogram"].Value.ToFloat();
                totaltime += GV.Rows[k].Cells["Time"].Value.ToFloat();
                totalkgProduct += GV.Rows[k].Cells["KgProduct"].Value.ToFloat();
                totalmoney += GV.Rows[k].Cells["Money"].Value.ToFloat();
            }

            GV.Rows[no].Cells["Basket"].Value = totalBasket.Ton1String();
            GV.Rows[no].Cells["Kilogram"].Value = totalKilogram.Ton1String();
            GV.Rows[no].Cells["Time"].Value = totaltime.Ton1String();
            GV.Rows[no].Cells["KgProduct"].Value = totalkgProduct.Ton1String();
            GV.Rows[no].Cells["Money"].Value = totalmoney.Ton1String();
            GV.Rows[no].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
        }
        #endregion

        //-------------------------------------------------------------------------------------------------Tab 1

        #region TAB [Tính năng suất tại tổ]
        private void InitTab1()
        {
            dte_Date1.CustomFormat = "dd/MM/yyyy";

            InitLayout_LV(LVData1);
            InitLayout_GVData(GVData1);
            InitLayout_GVRate(GVRate1);

            LVData1.ItemSelectionChanged += LVData1_Click;

            cbo_Team.SelectedIndexChanged += Cbo_Team_SelectedIndexChanged;
            cbo_Stage.SelectedIndexChanged += Cbo_Stage_SelectedIndexChanged;

            btn_Count.Click += btn_Count_Click;
            btn_Save.Click += btn_Save_Click;

            Utils.DoubleBuffered(GVData1, true);
            Utils.DoubleBuffered(GVRate1, true);
            Utils.DrawGVStyle(ref GVData1);
            Utils.DrawGVStyle(ref GVRate1);
            Utils.DrawLVStyle(ref LVData1);
            Utils.SizeLastColumn_LV(LVData1);
        }
        private void InitDataWork1_Combobox()
        {
            int TeamKey = cbo_Team.SelectedValue.ToInt();
            DateTime zFromDate = new DateTime(dte_Date1.Value.Year, dte_Date1.Value.Month, dte_Date1.Value.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(dte_Date1.Value.Year, dte_Date1.Value.Month, dte_Date1.Value.Day, 23, 0, 0);

            DataTable zTableWork = HoTroSanXuat.CongViecNhom(TeamKey, zFromDate, zToDate);
            LoadDataToToolbox.ComboBoxData(cbo_Stage, zTableWork, true, 0, 1);
        }
        //Event
        private void Cbo_Team_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_Team.SelectedIndex > 0)
            {
                InitDataWork1_Combobox();
            }
        }
        private void Cbo_Stage_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TeamKey = cbo_Team.SelectedValue.ToInt();
            int WorkKey = cbo_Stage.SelectedValue.ToInt();

            DateTime zFromDate = new DateTime(dte_Date1.Value.Year, dte_Date1.Value.Month, dte_Date1.Value.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(dte_Date1.Value.Year, dte_Date1.Value.Month, dte_Date1.Value.Day, 23, 0, 0);

            DataTable zTable = HoTroSanXuat.DonHangThucHien(TeamKey, WorkKey, string.Empty, zFromDate, zToDate);
            InitData_LV(LVData1, zTable);
        }
        private void LVData1_Click(object sender, EventArgs e)
        {
            if (LVData1.SelectedItems.Count > 0)
            {
                LoadInfo_Order();

                btn_Count.Visible = true;
                btn_Save.Visible = false;
            }
            else
            {
                ClearInfo_Order();
            }
        }
        private void btn_Count_Click(object sender, EventArgs e)
        {
            if (LVData1.SelectedItems.Count > 0)
            {
                Calculator_Order();
                btn_Save.Visible = true;
                btn_Count.Visible = false;
            }
            else
            {
                MessageBox.Show("Vui lòng chọn đơn hàng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            SaveResult_Order();
        }
        //Progess
        private void ClearInfo_Order()
        {
            GVData1.Rows.Clear();
            txt_Quantity.Clear();
            txt_Basket.Clear();
            txt_Time.Clear();
            txt_Kilogram.Clear();
            txt_Stage.Clear();
            txt_Price.Clear();
        }
        //Load chi tiết đơn hàng
        private void LoadInfo_Order()
        {
            string OrderKey = LVData1.SelectedItems[0].Tag.ToString();
            string OrderID = LVData1.SelectedItems[0].SubItems[1].Text;
            DateTime OrderDate = Convert.ToDateTime(LVData1.SelectedItems[0].SubItems[3].Text);
            float zQuantity = LVData1.SelectedItems[0].SubItems[4].Text.ToFloat();
            float zPrice = LVData1.SelectedItems[0].SubItems[5].Text.ToFloat();
            float zTotalBasket = LVData1.SelectedItems[0].SubItems[6].Text.ToFloat();
            float zTotalKg = LVData1.SelectedItems[0].SubItems[7].Text.ToFloat();
            float zTotalTime = LVData1.SelectedItems[0].SubItems[8].Text.ToFloat();
            float zRateOrder = LVData1.SelectedItems[0].SubItems[9].Text.ToFloat();
            float zRateDay = LVData1.SelectedItems[0].SubItems[10].Text.ToFloat();

            _OrderKey = LVData1.SelectedItems[0].Tag.ToString();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _WorkKey = cbo_Stage.SelectedValue.ToInt();

            txt_Price.Text = zPrice.ToString("n1");
            txt_Quantity.Text = zQuantity.ToString("n1");
            txt_Basket.Text = zTotalBasket.ToString("n1");
            txt_Kilogram.Text = zTotalKg.ToString("n1");
            txt_Time.Text = zTotalTime.ToString("n1");
            txt_Stage.Text = cbo_Stage.Text;

            DataTable ztblRate = Order_Rate_Data.List(OrderKey);
            DataTable ztblEmployee = HoTroSanXuat.CacNhanVienThucHien(OrderKey);
            InitData_GVRate(GVRate1, ztblRate);
            InitData_GVData(GVData1, ztblEmployee);
            InitData_GVSum(GVData1);
        }
        //tính toán số liệu đơn hàng, công nhân
        private void Calculator_Order()
        {
            string OrderKey = LVData1.SelectedItems[0].Tag.ToString();
            string OrderID = LVData1.SelectedItems[0].SubItems[1].Text;
            DateTime OrderDate = Convert.ToDateTime(LVData1.SelectedItems[0].SubItems[3].Text);
            float zQuantity = LVData1.SelectedItems[0].SubItems[4].Text.ToFloat();
            float zPrice = LVData1.SelectedItems[0].SubItems[5].Text.ToFloat();
            float zTotalBasket = LVData1.SelectedItems[0].SubItems[6].Text.ToFloat();
            float zTotalKg = LVData1.SelectedItems[0].SubItems[7].Text.ToFloat();
            float zTotalTime = LVData1.SelectedItems[0].SubItems[8].Text.ToFloat();
            float zRateOrder = LVData1.SelectedItems[0].SubItems[9].Text.ToFloat();
            float zRateDay = LVData1.SelectedItems[0].SubItems[10].Text.ToFloat();

            #region [---------   Chuẩn bị các tham số   -----------] 

            float zTempMoney = 0;   //code không rõ có sử dụng hay không
            float zTempBasket = 0;
            float zTempTime = 0;
            float zTempKg = 0;

            #region  //	Trường hợp 1: Chỉ có tổng số rổ, tổng số kí=0, tổng thời gian=0
            if (zTotalBasket > 0 &&
                zTotalKg == 0 &&
                zTotalTime == 0)
            {
                //TH 1.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.3:Nếu Ngày hiện tại là ngày chủ nhật
                if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
                    zTempBasket = zQuantity / zTotalBasket;
                }
            }
            #endregion
            #region  //	Trường hợp 2: Chỉ tổng thời gian, tổng số kí=0, tổng số rổ =0
            if (zTotalBasket == 0 &&
                zTotalKg == 0 &&
                zTotalTime > 0)
            {
                //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                    zTempTime = zQuantity / zTotalTime;
                }
                //TH 2.2:Nếu Ngày hiện tại là ngày thường và không phải là ngày chủ nhật
                if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                    zTempTime = zQuantity / zTotalTime;

                }
                if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
                    zTempTime = zQuantity / zTotalTime;

                }
            }
            #endregion
            #region  //	Trường hợp 3: Chỉ tổng số kí, tổng thời gian = 0 ,tổng số rổ = 0
            if (zTotalBasket == 0 &&
               zTotalKg > 0 &&
               zTotalTime == 0)
            {
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                    zTempKg = zQuantity / zTotalTime;

                }
                if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                    zTempKg = zQuantity / zTotalTime;

                }
                if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
                    zTempKg = zQuantity / zTotalTime;

                }
            }
            #endregion
            #region  //	Trường hợp 4: tổng số kí >0, tổng thời gian >0
            if (zTotalKg > 0 && zTotalTime > 0)
            {
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                    zTempKg = zQuantity / zTotalTime;

                }
                if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                    zTempKg = zQuantity / zTotalTime;

                }
                if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * 2;
                    zTempKg = zQuantity / zTotalTime;

                }
            }
            #endregion
            #region //	Trường hợp 5:  tổng số rổ >0, tổng thời gian >0 
            if (zTotalBasket > 0 && zTotalTime > 0)
            {
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * 2;
                    zTempBasket = zQuantity / zTotalBasket;
                }
            }
            #endregion
            #endregion

            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVData1.Rows[i];
                int Gender = r.Cells["Gender"].Value.ToInt(); //Nam nu
                float Time = r.Cells["Time"].Value.ToFloat();       //Số giờ 
                float Kilogram = r.Cells["Kilogram"].Value.ToFloat();   //Số kí 
                float Basket = r.Cells["Basket"].Value.ToFloat();       //Số rỗ 

                float ResultKg = 0;     //ket quả số kg
                float ResultMoney = 0;// kết quả số tiền

                if (zPrice > 0)
                {
                    //TH 1.1 Số giờ > 0 và số rổ = 0 và số kí = 0
                    if (Time > 0 && Basket == 0 && Kilogram == 0)
                    {
                        //TH 1.1.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = zTempTime * Time * zRateOrder;
                            ResultMoney = ResultKg * zPrice * zRateDay;
                        }
                        //TH 1.1.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = zTempTime * Time * zRateOrder;
                            ResultMoney = ResultKg * zPrice;
                        }
                        //TH 1.1.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = zTempTime * Time * zRateOrder;
                            ResultMoney = ResultKg * zPrice * _DayOfWeek;
                        }
                    }
                    //TH 1.2(Số giờ > 0 và số rổ = 0) hoặc(Số giờ == 0 và số kí > 0)
                    if (Time > 0 && Basket > 0 || Time == 0 && Basket > 0)
                    {
                        //TH 1.2.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = zTempBasket * Basket * zRateOrder;
                            ResultMoney = ResultKg * zPrice * zRateDay;
                        }
                        //TH 1.2.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = zTempBasket * Basket * zRateOrder;
                            ResultMoney = ResultKg * zPrice;
                        }
                        //TH 1.2.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = zTempBasket * Basket * zRateOrder;
                            ResultMoney = ResultKg * zPrice * _DayOfWeek;
                        }
                    }
                    //TH 1.3(Số giờ > 0 và Số kí > 0)
                    if (Time > 0 && Kilogram > 0)
                    {
                        //TH 1.3.1  Ngày hiện tại đơn hàng là ngày lễ
                        if (zRateDay > 0)
                        {
                            ResultKg = zTempKg * Kilogram * zRateOrder;
                            ResultMoney = ResultKg * zPrice * zRateDay;
                        }
                        //TH 1.3.2  Ngày hiện tại đơn hàng không là ngày lễ và không phải là ngày chủ nhật
                        if (OrderDate.DayOfWeek != DayOfWeek.Sunday && zRateDay == 0)
                        {
                            ResultKg = zTempKg * Kilogram * zRateOrder;
                            ResultMoney = ResultKg * zPrice;
                        }
                        //TH 1.3.3  Ngày hiện tại đơn hàng không là ngày lễ và là ngày chủ nhật
                        if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                        {
                            ResultKg = zTempTime * Kilogram * zRateOrder;
                            ResultMoney = ResultKg * zPrice * _DayOfWeek;
                        }
                    }
                }
                else
                {
                    float MoneyTime = 0;
                    float MoneyOver = 0;
                    float OverTime = 0;
                    if (Gender == 0)
                    {
                        MoneyTime = (Time / _Hour) * _MMoney;
                        if (Time > 8)
                        {
                            OverTime = Time - _Hour;
                            MoneyOver = OverTime * _OMoney;
                        }
                        MoneyTime = MoneyTime + MoneyOver;
                    }
                    else
                    {
                        MoneyTime = (Time / _Hour) * _FMoney;
                        if (Time > 8)
                        {
                            OverTime = Time - _Hour;
                            MoneyOver = OverTime * _OMoney;
                        }
                        MoneyTime = MoneyTime + MoneyOver;
                    }

                    ResultMoney = MoneyTime;
                }

                GVData1.Rows[i].Cells["KgProduct"].Value = ResultKg.ToString("n1");
                GVData1.Rows[i].Cells["Money"].Value = ResultMoney.ToString("n1");
            }
        }
        //lưu thông tin đã tính toán
        private void SaveResult_Order()
        {
            DateTime OrderDate = Convert.ToDateTime(LVData1.SelectedItems[0].SubItems[3].Text);

            for (int i = 0; i < GVData1.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVData1.Rows[i];
                int Borrow = 0;   //mượn
                int Private = 0;  //ăn riêng
                int Share = 0; //ăn chung
                if (r.Cells["Borrow"].Value != null)
                    Borrow = 1;
                if (r.Cells["Private"].Value != null)
                    Private = 1;
                if (r.Cells["Share"].Value != null)
                    Share = 1;
                float Time = r.Cells["Time"].Value.ToFloat();       //Số giờ 
                float Basket = r.Cells["Basket"].Value.ToFloat();       //Số rỗ 
                float ResultKg = r.Cells["KgProduct"].Value.ToFloat();     //ket quả số kg
                float ResultMoney = r.Cells["Money"].Value.ToFloat();// kết quả số tiền
                string EmployeeKey = r.Cells["EmployeeID"].Tag.ToString();
                string EmployeeID = r.Cells["EmployeeID"].Value.ToString();
                string EmployeeName = r.Cells["EmployeeName"].Value.ToString();


                Order_Money_Model _OrderMoney = new Order_Money_Model();
                _OrderMoney.OrderDate = OrderDate;
                _OrderMoney.EmployeeKey = EmployeeKey;      //**KEY UDPATE**
                _OrderMoney.EmployeeID = EmployeeID;
                _OrderMoney.EmployeeName = EmployeeName;
                _OrderMoney.StageKey = _WorkKey;
                _OrderMoney.TeamKey = _TeamKey;
                _OrderMoney.OrderKey = _OrderKey;                                           //**KEY UDPATE**

                //-----KHÔNG MƯỢN
                if (Borrow == 0 && Private == 0 && Share == 0)
                {
                    _OrderMoney.Time = Time;
                    _OrderMoney.Kg = ResultKg;
                    _OrderMoney.Money = ResultMoney;
                    _OrderMoney.Basket = Basket;

                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;
                    _OrderMoney.Basket_Private = 0;

                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;
                    _OrderMoney.Basket_Borrow = 0;

                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 1;//**Lưu ý**
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 0;
                }
                //-----KHÔNG MƯỢN MÀ ĂN RIÊNG
                if (Borrow == 0 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = Basket;
                    _OrderMoney.Time_Private = Time;
                    _OrderMoney.Kg_Private = ResultKg;
                    _OrderMoney.Money_Private = ResultMoney;

                    _OrderMoney.Basket_Borrow = 0;
                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;

                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 1;//**Lưu ý**
                }
                //-----MƯỢN MÀ ĂN RIÊNG
                if (Borrow == 1 && Private == 1 && Share == 0)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = 0;
                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;

                    _OrderMoney.Basket_Borrow = Basket;
                    _OrderMoney.Time_Borrow = Time;
                    _OrderMoney.Kg_Borrow = ResultKg;
                    _OrderMoney.Money_Borrow = ResultMoney;

                    _OrderMoney.Money_Eat = 0;
                    _OrderMoney.Time_Eat = 0;
                    _OrderMoney.Kg_Eat = 0;
                    _OrderMoney.Basket_Eat = 0;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 1;//**Lưu ý**
                    _OrderMoney.Category_Eat = 0;
                    _OrderMoney.Category_Private = 0;
                }
                //-----MƯỢN MÀ ĂN CHUNG
                if (Borrow == 1 && Private == 0 && Share == 1)
                {
                    _OrderMoney.Time = 0;
                    _OrderMoney.Money = 0;
                    _OrderMoney.Kg = 0;
                    _OrderMoney.Basket = 0;

                    _OrderMoney.Basket_Private = 0;
                    _OrderMoney.Time_Private = 0;
                    _OrderMoney.Kg_Private = 0;
                    _OrderMoney.Money_Private = 0;

                    _OrderMoney.Basket_Borrow = 0;
                    _OrderMoney.Time_Borrow = 0;
                    _OrderMoney.Kg_Borrow = 0;
                    _OrderMoney.Money_Borrow = 0;

                    _OrderMoney.Money_Eat = ResultMoney;
                    _OrderMoney.Time_Eat = Time;
                    _OrderMoney.Kg_Eat = ResultKg;
                    _OrderMoney.Basket_Eat = Basket;

                    _OrderMoney.Category = 0;
                    _OrderMoney.Category_Borrow = 0;
                    _OrderMoney.Category_Eat = 1; //**Lưu ý**
                    _OrderMoney.Category_Private = 0;
                }

                //----- LƯU DB
                Order_Exe OrderExe = new Order_Exe();
                OrderExe.OrderMoney = _OrderMoney;
                OrderExe.CreatedBy = SessionUser.UserLogin.Key;
                OrderExe.ModifiedBy = SessionUser.UserLogin.Key;
                OrderExe.CreatedName = SessionUser.Personal_Info.FullName;
                OrderExe.ModifiedName = SessionUser.Personal_Info.FullName;
                OrderExe.Save();

                if (OrderExe.Message != "11" && OrderExe.Message != "20")
                    r.Cells["Message"].Value = OrderExe.Message;

                //----- 
                Order_Employee_Info zEmployee = new Order_Employee_Info();
                zEmployee.EmployeeKey = EmployeeKey;
                zEmployee.OrderKey = _OrderKey;
                zEmployee.Money = ResultMoney;
                zEmployee.UpdateMoney();

                //SCCĐ và SCĐH
                if (_TeamKey == _TeamSCCD ||
                    _TeamKey == _TeamSCDH)
                {
                    int zCount = OrderExe.TimeExits();
                    Money_TimeKeep_Info zInfo = new Money_TimeKeep_Info();

                    zInfo.OrderKey = _OrderKey;
                    zInfo.DateTimeKeep = _OrderMoney.OrderDate;
                    zInfo.EmployeeKey = _OrderMoney.EmployeeKey;
                    zInfo.EmployeeID = _OrderMoney.EmployeeID;
                    zInfo.EmployeeName = _OrderMoney.EmployeeName;
                    zInfo.MoneyTimeKeep = ResultMoney;

                    if (zCount == 0)
                    {
                        zInfo.CreatedBy = SessionUser.UserLogin.Key;
                        zInfo.CreatedName = SessionUser.Personal_Info.FullName;
                        zInfo.Create();
                    }
                    else
                    {
                        zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                        zInfo.ModifiedName = SessionUser.Personal_Info.FullName;
                        zInfo.Update();
                    }
                }
                //----- 
            }
        }
        #endregion

        //-----------------DIF----------------------------------------------------------------------------Tab 2

        #region TAB [Tính năng suất công việc khác]
        private void InitTab2()
        {
            dte_Date2.CustomFormat = "dd/MM/yyyy";

            InitLayout_LV(LVData2);
            InitLayout_GVData(GVData2);
            InitLayout_GVRate(GVRate2);

            LVData2.ItemSelectionChanged += LVData2_Click;

            cbo_Team_Dif.SelectedIndexChanged += Cbo_Team_Dif_SelectedIndexChanged;
            cbo_Stage_Dif.SelectedIndexChanged += Cbo_Stage_Dif_SelectedIndexChanged;

            btn_Count_Dif.Click += btn_Count_Dif_Click;
            btn_Save_Dif.Click += btn_Save_Dif_Click;

            Utils.DoubleBuffered(GVData2, true);
            Utils.DoubleBuffered(GVRate2, true);
            Utils.DrawGVStyle(ref GVData2);
            Utils.DrawGVStyle(ref GVRate2);
            Utils.DrawLVStyle(ref LVData2);
            Utils.SizeLastColumn_LV(LVData2);
        }
        private void InitDataWork2_Combobox()
        {
            int TeamKey = cbo_Team_Dif.SelectedValue.ToInt();
            DateTime zFromDate = new DateTime(dte_Date2.Value.Year, dte_Date2.Value.Month, dte_Date2.Value.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(dte_Date2.Value.Year, dte_Date2.Value.Month, dte_Date2.Value.Day, 23, 0, 0);

            DataTable zTableWork = HoTroSanXuat.CongViecNhom(TeamKey, zFromDate, zToDate);
            LoadDataToToolbox.ComboBoxData(cbo_Stage_Dif, zTableWork, true, 0, 1);
        }
        //Event
        private void Cbo_Team_Dif_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_Team_Dif.SelectedIndex > 0)
            {
                InitDataWork2_Combobox();
            }
        }
        private void Cbo_Stage_Dif_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TeamKey = cbo_Team_Dif.SelectedValue.ToInt();
            int WorkKey = cbo_Stage_Dif.SelectedValue.ToInt();

            DateTime zFromDate = new DateTime(dte_Date2.Value.Year, dte_Date2.Value.Month, dte_Date2.Value.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(dte_Date2.Value.Year, dte_Date2.Value.Month, dte_Date2.Value.Day, 23, 0, 0);

            DataTable zTable = HoTroSanXuat.DonHangThucHien(TeamKey, WorkKey, string.Empty, zFromDate, zToDate);
            InitData_LV(LVData2, zTable);
        }
        private void LVData2_Click(object sender, EventArgs e)
        {
            if (LVData2.SelectedItems.Count > 0)
            {
                LoadInfo_Order_Dif();

                btn_Count_Dif.Visible = true;
                btn_Save_Dif.Visible = false;
            }
            else
            {
                ClearInfo_Order_Dif();
            }
        }
        private void btn_Count_Dif_Click(object sender, EventArgs e)
        {
            if (_OrderDifKey.Length > 0)
            {
                Calculator_Order_Dif();
                btn_Save_Dif.Visible = true;
                btn_Count_Dif.Visible = false;
            }
            else
            {
                MessageBox.Show("Vui lòng chọn đơn hàng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btn_Save_Dif_Click(object sender, EventArgs e)
        {
            SaveResult_Order_Dif();
        }
        //Progess
        private void ClearInfo_Order_Dif()
        {
            GVData2.Rows.Clear();
            txt_Quantity_Dif.Clear();
            txt_Basket_Dif.Clear();
            txt_Time_Dif.Clear();
            txt_Kilogram_Dif.Clear();
            txt_Stage_Dif.Clear();
            txt_Price_Dif.Clear();
        }
        //Load chi tiết đơn hàng khác
        private void LoadInfo_Order_Dif()
        {
            string OrderKey = LVData2.SelectedItems[0].Tag.ToString();
            string OrderID = LVData2.SelectedItems[0].SubItems[1].Text;
            DateTime OrderDate = Convert.ToDateTime(LVData2.SelectedItems[0].SubItems[3].Text);
            float zQuantity = LVData2.SelectedItems[0].SubItems[4].Text.ToFloat();
            float zPrice = LVData2.SelectedItems[0].SubItems[5].Text.ToFloat();
            float zTotalBasket = LVData2.SelectedItems[0].SubItems[6].Text.ToFloat();
            float zTotalKg = LVData2.SelectedItems[0].SubItems[7].Text.ToFloat();
            float zTotalTime = LVData2.SelectedItems[0].SubItems[8].Text.ToFloat();
            float zRateOrder = LVData2.SelectedItems[0].SubItems[9].Text.ToFloat();
            float zRateDay = LVData2.SelectedItems[0].SubItems[10].Text.ToFloat();

            _OrderDifKey = LVData2.SelectedItems[0].Tag.ToString();
            _TeamDifKey = cbo_Team_Dif.SelectedValue.ToInt();
            _WorkDifKey = cbo_Stage_Dif.SelectedValue.ToInt();

            txt_Quantity_Dif.Text = zQuantity.ToString("n1");
            txt_Price_Dif.Text = zPrice.ToString("n1");
            txt_Basket_Dif.Text = zTotalBasket.ToString("n1");
            txt_Kilogram_Dif.Text = zTotalKg.ToString("n1");
            txt_Time_Dif.Text = zTotalTime.ToString("n1");
            txt_Stage_Dif.Text = cbo_Stage_Dif.Text;

            DataTable ztblRate = Order_Rate_Data.List(OrderKey);
            DataTable ztblEmployee = HoTroSanXuat.CacNhanVienThucHien(OrderKey); //Caculater_Productivity_Data.List_Employee(int.Parse(cbo_Team_Dif.SelectedValue.ToString()), OrderKey);
            InitData_GVRate(GVRate2, ztblRate);
            InitData_GVData(GVData2, ztblEmployee);
            InitData_GVSum(GVData2);
        }
        private void Calculator_Order_Dif()
        {
            string OrderKey = LVData2.SelectedItems[0].Tag.ToString();
            string OrderID = LVData2.SelectedItems[0].SubItems[1].Text;
            DateTime OrderDate = Convert.ToDateTime(LVData2.SelectedItems[0].SubItems[3].Text);
            float zQuantity = LVData2.SelectedItems[0].SubItems[4].Text.ToFloat();
            float zPrice = LVData2.SelectedItems[0].SubItems[5].Text.ToFloat();
            float zTotalBasket = LVData2.SelectedItems[0].SubItems[6].Text.ToFloat();
            float zTotalKg = LVData2.SelectedItems[0].SubItems[7].Text.ToFloat();
            float zTotalTime = LVData2.SelectedItems[0].SubItems[8].Text.ToFloat();
            float zRateOrder = LVData2.SelectedItems[0].SubItems[9].Text.ToFloat();
            float zRateDay = LVData2.SelectedItems[0].SubItems[10].Text.ToFloat();

            // [---------   Chuẩn bị các tham số   -----------] 
            // tính toán phải làm tròn 2 chữ số thập phân n2
            float zTempMoney = 0;   // số tiền tb 1 rỗ
            float zTempBasket = 0;// sô kí tb 1 rỗ
            float zTempTime = 0; //số kg tb trong 1 giờ

            if (zTotalBasket > 0)
            {
                //TH 1.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * zRateDay;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.2 Nếu Ngày hiện tại là ngày thường
                if (zRateDay == 0 && OrderDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder;
                    zTempBasket = zQuantity / zTotalBasket;
                }
                //TH 1.3 Nếu Ngày hiện tại là ngày chủ nhật
                if (dte_Date2.Value.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalBasket) * zPrice * zRateOrder * _DayOfWeek;
                    zTempBasket = zQuantity / zTotalBasket;
                }
            }
            if (zTotalTime > 0)
            { //TH 2.1 Nếu Ngày hiện tại là ngày lễ
                if (zRateDay > 0)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * zRateDay;
                    zTempTime = zQuantity / zTotalTime;
                }
                //TH 2.2 Nếu Ngày hiện tại là ngày thường
                if (zRateDay == 0 && OrderDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder;
                    zTempTime = zQuantity / zTotalTime;
                }
                //TH 2.3 Nếu Ngày hiện tại là ngày chủ nhật
                if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    zTempMoney = (zQuantity / zTotalTime) * zPrice * zRateOrder * _DayOfWeek;
                    zTempTime = zQuantity / zTotalTime;
                }
            }
            //Làm tròn 2 chữ số thập phân
            zTempMoney = Math.Round(zTempMoney, 2).ToFloat();
            zTempTime = Math.Round(zTempTime, 2).ToFloat();
            zTempBasket = Math.Round(zTempBasket, 2).ToFloat();

            for (int i = 0; i < GVData2.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVData2.Rows[i];
                float Time = r.Cells["Time"].Value.ToFloat();       //Số giờ 
                float Kilogram = r.Cells["Kilogram"].Value.ToFloat();   //Số kí 
                float Basket = r.Cells["Basket"].Value.ToFloat();       //Số rỗ 

                float ResultKg = 0;     //ket quả số kg
                float ResultMoney = 0;// kết quả số tiền

                if (Kilogram > 0)
                {
                    ResultMoney = Kilogram * zTempMoney;
                }
                if (Basket > 0)
                {
                    ResultKg = Basket * zTempBasket;
                    ResultMoney = Basket * zTempMoney;
                }
                if (Time > 0)
                {
                    ResultMoney = Time * zTempMoney;

                }
                GVData2.Rows[i].Cells["Money"].Value = ResultMoney.ToString("n1");
                GVData2.Rows[i].Cells["Kilogram"].Value = (Basket * ResultKg).ToString("n1");
            }

        }
        private void SaveResult_Order_Dif()
        {
            DateTime OrderDate = Convert.ToDateTime(LVData2.SelectedItems[0].SubItems[3].Text);
            float zRateDay = LVData2.SelectedItems[0].SubItems[10].Text.ToFloat();

            for (int i = 0; i < GVData2.Rows.Count - 1; i++)
            {
                DataGridViewRow r = GVData2.Rows[i];
                int Borrow = 0;   //mượn
                int Private = 0;  //ăn riêng
                int Share = 0; //ăn chung

                if (r.Cells["Borrow"].Value != null)
                    Borrow = 1;
                if (r.Cells["Private"].Value != null)
                    Private = 1;
                if (r.Cells["Share"].Value != null)
                    Share = 1;
                float ResultMoney = r.Cells["Money"].Value.ToFloat();// kết quả số tiền
                string EmployeeKey = r.Cells["EmployeeID"].Tag.ToString();
                string EmployeeID = r.Cells["EmployeeID"].Value.ToString();
                string EmployeeName = r.Cells["EmployeeName"].Value.ToString();


                Order_Money_Model _OrderMoney = new Order_Money_Model();
                _OrderMoney.OrderDate = OrderDate;
                _OrderMoney.EmployeeKey = EmployeeKey;      //**KEY UDPATE**
                _OrderMoney.EmployeeID = EmployeeID;
                _OrderMoney.EmployeeName = EmployeeName;
                _OrderMoney.StageKey = _WorkDifKey;
                _OrderMoney.TeamKey = _TeamDifKey;
                _OrderMoney.OrderKey = _OrderDifKey;

                if (Borrow == 1 && Private == 1 && Share == 0)  //Trường hợp 1 :MƯỢN VÀ ĂN RIÊNG
                {
                    //TH 1.1 ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        _OrderMoney.Money_Dif_Holiday = ResultMoney;
                    }
                    //TH 1.1 ngày hiện tại là ngày thường
                    if (zRateDay == 0 && OrderDate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif = ResultMoney;
                    }
                    //TH 1.3 ngày hiện tại là ngày chủ nhật
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_Sunday = ResultMoney;
                    }
                }
                if (Borrow == 1 && Private == 0 && Share == 1)  //Trường hợp 1 :MƯỢN VÀ ĂN CHUNG
                {
                    //TH 2.1 ngày hiện tại là ngày lễ
                    if (zRateDay > 0)
                    {
                        _OrderMoney.Money_Dif_Holiday_General = ResultMoney;
                    }
                    //TH 2.1 ngày hiện tại là ngày thường
                    if (zRateDay == 0 && OrderDate.DayOfWeek != DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_General = ResultMoney;
                    }
                    //TH 2.3 ngày hiện tại là ngày chủ nhật
                    if (OrderDate.DayOfWeek == DayOfWeek.Sunday)
                    {
                        _OrderMoney.Money_Dif_Sunday_General = ResultMoney;
                    }
                }
                Order_Exe OrderExe = new Order_Exe();
                OrderExe.OrderMoney = _OrderMoney;
                OrderExe.CreatedBy = SessionUser.UserLogin.Key;
                OrderExe.ModifiedBy = SessionUser.UserLogin.Key;
                OrderExe.CreatedName = SessionUser.Personal_Info.FullName;
                OrderExe.ModifiedName = SessionUser.Personal_Info.FullName;
                OrderExe.Save();

                if (OrderExe.Message != "11" &&
                    OrderExe.Message != "20")
                    r.Cells["Message"].Value = OrderExe.Message;
            }
        }
        #endregion

        //-------------------------------------------------------------------------------------------------

        #region [Timer Team Default]
       
        private int _indexlistview = 0;
        private int _CountList = 0;
        private int _CountComboBox = 0;
        private int _CountComboBoxTeam = 1;
        private void btnRun1_Click(object sender, EventArgs e)
        {        


            timerWork.Start();
        }
        private void timerTeam_Tick(object sender, EventArgs e)
        {
            int zCountListView = LVData1.Items.Count;
            timerTeam.Stop();
            _indexlistview++;
            _CountList++;
            if (_CountList > zCountListView)
            {
                timerTeam.Stop();
                _CountList = 0;
                timerWork.Start();
            }
            else
            {

                _OrderKey = LVData1.Items[_indexlistview].Tag.ToString();
                int _FindOrderID = Order_Money_Data.Find_OrderID(_OrderKey);
                if (_FindOrderID > 0)
                {
                    Order_Money_Info zInfo = new Order_Money_Info();
                    zInfo.OrderKey = _OrderKey;
                    zInfo.Delete_Order();
                }
                string zOrderID = LVData1.Items[_indexlistview].SubItems[1].Text;
                txt_OrderID.Text = zOrderID;
                txt_Quantity.Text = LVData1.Items[_indexlistview].SubItems[3].Text;
                txt_Price.Text = LVData1.Items[_indexlistview].SubItems[4].Text;
                txt_Basket.Text = LVData1.Items[_indexlistview].SubItems[5].Text;
                txt_Kilogram.Text = LVData1.Items[_indexlistview].SubItems[6].Text;
                txt_Time.Text = LVData1.Items[_indexlistview].SubItems[7].Text;

                if (_OrderKey.Length > 0)
                {
                    //CountListView();
                    //InitData_GVData1();
                    //InitGVRate_Data();
                    if (GVData1.Rows.Count > 0)
                    {
                        // Count_Productivity();
                        // Lưu tiền nhân viên                       
                        for (int i = 0; i < GVData1.Rows.Count - 1; i++)
                        {
                            int _Borrow = 0;
                            int _EatPrivate = 0;
                            int _EatGeneral = 0;
                            #region
                            if (GVData1.Rows[i].Cells["Borrow"].Value != null)
                            {
                                _Borrow = 1;
                            }
                            if (GVData1.Rows[i].Cells["Private"].Value != null)
                            {
                                _EatPrivate = 1;
                            }
                            if (GVData1.Rows[i].Cells["General"].Value != null)
                            {
                                _EatGeneral = 1;
                            }
                            #endregion

                            float _Quantity = float.Parse(GVData1.Rows[i].Cells["Kg"].Value.ToString());
                            float _QuantityTime = float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                            //-----KHÔNG MƯỢN
                            if (_Borrow == 0 && _EatPrivate == 0 && _EatGeneral == 0)
                            {
                                Order_Money_Info zOrder = new Order_Money_Info();
                                zOrder.EmployeeKey = GVData1.Rows[i].Cells["EmployeeID"].Tag.ToString();
                                zOrder.EmployeeName = GVData1.Rows[i].Cells["Name"].Value.ToString();
                                zOrder.EmployeeID = GVData1.Rows[i].Cells["EmployeeID"].Value.ToString();
                                zOrder.StageKey = int.Parse(cbo_Stage.SelectedValue.ToString());
                                int zTeamKey = Employee_Data.FindTeamKey(zOrder.EmployeeKey);
                                zOrder.TeamKey = zTeamKey;
                                zOrder.OrderKey = _OrderKey;
                                zOrder.Time = float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                                if (GVData1.Rows[i].Cells["Money"].Value != null)
                                {
                                    zOrder.Money = float.Parse(GVData1.Rows[i].Cells["Money"].Value.ToString());
                                }
                                else
                                {
                                    zOrder.Money = 0;
                                }
                                zOrder.Kg = float.Parse(GVData1.Rows[i].Cells["KgProduct"].Value.ToString());
                                zOrder.Basket = float.Parse(GVData1.Rows[i].Cells["Basket"].Value.ToString());

                                zOrder.Basket_Borrow = 0;
                                zOrder.Basket_Eat = 0;
                                zOrder.BasketPrivate = 0;

                                zOrder.TimePrivate = 0;
                                zOrder.KgPrivate = 0;
                                zOrder.MoneyPrivate = 0;

                                zOrder.Time_Borrow = 0;
                                zOrder.Kg_Borrow = 0;
                                zOrder.Money_Borrow = 0;

                                zOrder.Money_Eat = 0;
                                zOrder.Time_Eat = 0;
                                zOrder.Kg_Eat = 0;

                                zOrder.Category = 1;
                                zOrder.Category_Borrow = 0;
                                zOrder.Category_Eat = 0;
                                zOrder.CategoryPrivate = 0;
                                zOrder.OrderDate = dte_Date1.Value;

                                _Count = Order_Money_Data.Find_Employee(_OrderKey, zOrder.EmployeeKey);
                                if (_Count == 0)
                                {
                                    zOrder.Create();
                                    if (zOrder.Message == "11")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }
                                else
                                {
                                    zOrder.Update();
                                    if (zOrder.Message == "20")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }
                                //------ Tiền Chấm Công
                                if (cbo_Team.SelectedValue.ToInt() == 88 || cbo_Team.SelectedValue.ToInt() == 89)
                                {
                                    Money_TimeKeep_Info zMoney = new Money_TimeKeep_Info();
                                    zMoney.OrderKey = _OrderKey;
                                    zMoney.DateTimeKeep = dte_Date1.Value;
                                    zMoney.EmployeeKey = zOrder.EmployeeKey;
                                    zMoney.EmployeeID = zOrder.EmployeeID;
                                    zMoney.EmployeeName = zOrder.EmployeeName;
                                    zMoney.MoneyTimeKeep = zOrder.Money;
                                    int zCount = Money_TimeKeep_Data.Find_Employee(zMoney.OrderKey, zMoney.EmployeeKey);
                                    if (zCount == 0)
                                    {
                                        zMoney.CreatedBy = SessionUser.UserLogin.Key;
                                        zMoney.CreatedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Create();
                                    }
                                    else
                                    {
                                        zMoney.ModifiedBy = SessionUser.UserLogin.Key;
                                        zMoney.ModifiedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Update();
                                    }
                                }
                            }
                            //-----KHÔNG MƯỢN MÀ ĂN RIÊNG
                            if (_Borrow == 0 && _EatPrivate == 1 && _EatGeneral == 0)
                            {
                                Order_Money_Info zOrder = new Order_Money_Info();
                                zOrder.EmployeeKey = GVData1.Rows[i].Cells["EmployeeID"].Tag.ToString();
                                zOrder.EmployeeName = GVData1.Rows[i].Cells["Name"].Value.ToString();
                                zOrder.EmployeeID = GVData1.Rows[i].Cells["EmployeeID"].Value.ToString();
                                zOrder.StageKey = int.Parse(cbo_Stage.SelectedValue.ToString());
                                int zTeamKey = Employee_Data.FindTeamKey(zOrder.EmployeeKey);
                                zOrder.TeamKey = zTeamKey;
                                zOrder.OrderKey = _OrderKey;

                                zOrder.Time = 0;
                                zOrder.Money = 0;
                                zOrder.Kg = 0;

                                zOrder.TimePrivate = float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                                if (GVData1.Rows[i].Cells["Kg"].Value != null)
                                {
                                    zOrder.KgPrivate = float.Parse(GVData1.Rows[i].Cells["KgProduct"].Value.ToString());
                                }
                                else
                                {
                                    zOrder.KgPrivate = 0;
                                }

                                if (GVData1.Rows[i].Cells["Money"].Value != null)
                                {
                                    zOrder.MoneyPrivate = float.Parse(GVData1.Rows[i].Cells["Money"].Value.ToString());
                                }
                                else
                                {
                                    zOrder.MoneyPrivate = 0;

                                }

                                if (GVData1.Rows[i].Cells["Basket"].Value != null)
                                {
                                    zOrder.BasketPrivate = float.Parse(GVData1.Rows[i].Cells["Basket"].Value.ToString());
                                }
                                else
                                {
                                    zOrder.BasketPrivate = 0;
                                }

                                zOrder.Time_Borrow = 0;
                                zOrder.Kg_Borrow = 0;
                                zOrder.Money_Borrow = 0;

                                zOrder.Money_Eat = 0;
                                zOrder.Time_Eat = 0;
                                zOrder.Kg_Eat = 0;

                                zOrder.Category = 0;
                                zOrder.Category_Borrow = 0;
                                zOrder.Category_Eat = 0;
                                if (GVData1.Rows[i].Cells["Money"].Value != null)
                                {
                                    zOrder.CategoryPrivate = 1;
                                }
                                zOrder.OrderDate = dte_Date1.Value;

                                _Count = Order_Money_Data.Find_Employee(_OrderKey, zOrder.EmployeeKey);
                                if (_Count == 0)
                                {
                                    zOrder.Create();
                                    if (zOrder.Message == "11")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }
                                else
                                {
                                    zOrder.Update();
                                    if (zOrder.Message == "20")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }
                                //------ Tiền Chấm Công
                                if (cbo_Team.SelectedValue.ToInt() == 88 || cbo_Team.SelectedValue.ToInt() == 89)
                                {
                                    Money_TimeKeep_Info zMoney = new Money_TimeKeep_Info();
                                    zMoney.OrderKey = _OrderKey;
                                    zMoney.DateTimeKeep = dte_Date1.Value;
                                    zMoney.EmployeeKey = zOrder.EmployeeKey;
                                    zMoney.EmployeeID = zOrder.EmployeeID;
                                    zMoney.EmployeeName = zOrder.EmployeeName;
                                    zMoney.MoneyTimeKeep = zOrder.MoneyPrivate;
                                    int zCount = Money_TimeKeep_Data.Find_Employee(zMoney.OrderKey, zMoney.EmployeeKey);
                                    if (zCount == 0)
                                    {
                                        zMoney.CreatedBy = SessionUser.UserLogin.Key;
                                        zMoney.CreatedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Create();
                                    }
                                    else
                                    {
                                        zMoney.ModifiedBy = SessionUser.UserLogin.Key;
                                        zMoney.ModifiedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Update();
                                    }
                                }

                            }
                            //-----MƯỢN MÀ ĂN RIÊNG
                            if (_Borrow == 1 && _EatPrivate == 1 && _EatGeneral == 0)
                            {
                                Order_Money_Info zOrder = new Order_Money_Info();
                                zOrder.EmployeeKey = GVData1.Rows[i].Cells["EmployeeID"].Tag.ToString();
                                zOrder.EmployeeName = GVData1.Rows[i].Cells["Name"].Value.ToString();
                                zOrder.EmployeeID = GVData1.Rows[i].Cells["EmployeeID"].Value.ToString();
                                zOrder.StageKey = int.Parse(cbo_Stage.SelectedValue.ToString());
                                int zTeamKey = Employee_Data.FindTeamKey(zOrder.EmployeeKey);
                                zOrder.TeamKey = zTeamKey;
                                zOrder.OrderKey = _OrderKey;

                                zOrder.Time = 0;
                                zOrder.Money = 0;
                                zOrder.Kg = 0;

                                zOrder.TimePrivate = 0;
                                zOrder.KgPrivate = 0;
                                zOrder.MoneyPrivate = 0;

                                zOrder.Time_Borrow = float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                                zOrder.Kg_Borrow = float.Parse(GVData1.Rows[i].Cells["KgProduct"].Value.ToString());
                                zOrder.Basket_Borrow = float.Parse(GVData1.Rows[i].Cells["Basket"].Value.ToString());

                                if (GVData1.Rows[i].Cells["Money"].Value != null)
                                {
                                    zOrder.Money_Borrow = float.Parse(GVData1.Rows[i].Cells["Money"].Value.ToString());
                                }
                                else
                                {
                                    zOrder.Money_Borrow = 0;
                                }

                                zOrder.Basket = 0;
                                zOrder.Basket_Eat = 0;
                                zOrder.BasketPrivate = 0;

                                zOrder.Money_Eat = 0;
                                zOrder.Time_Eat = 0;
                                zOrder.Kg_Eat = 0;

                                zOrder.Category = 0;
                                zOrder.Category_Borrow = 1;
                                zOrder.Category_Eat = 0;
                                zOrder.CategoryPrivate = 0;

                                zOrder.OrderDate = dte_Date1.Value;
                                _Count = Order_Money_Data.Find_Employee(_OrderKey, zOrder.EmployeeKey);
                                if (_Count == 0)
                                {
                                    zOrder.Create();
                                    if (zOrder.Message == "11")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }
                                else
                                {
                                    zOrder.Update();
                                    if (zOrder.Message == "20")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }

                                //------ Tiền Chấm Công
                                if (cbo_Team.SelectedValue.ToInt() == 88 || cbo_Team.SelectedValue.ToInt() == 89)
                                {
                                    Money_TimeKeep_Info zMoney = new Money_TimeKeep_Info();
                                    zMoney.OrderKey = _OrderKey;
                                    zMoney.DateTimeKeep = dte_Date1.Value;
                                    zMoney.EmployeeKey = zOrder.EmployeeKey;
                                    zMoney.EmployeeID = zOrder.EmployeeID;
                                    zMoney.EmployeeName = zOrder.EmployeeName;
                                    zMoney.MoneyTimeKeep = zOrder.Money_Borrow;
                                    int zCount = Money_TimeKeep_Data.Find_Employee(zMoney.OrderKey, zMoney.EmployeeKey);
                                    if (zCount == 0)
                                    {
                                        zMoney.CreatedBy = SessionUser.UserLogin.Key;
                                        zMoney.CreatedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Create();
                                    }
                                    else
                                    {
                                        zMoney.ModifiedBy = SessionUser.UserLogin.Key;
                                        zMoney.ModifiedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Update();
                                    }
                                }
                            }
                            //-----MƯỢN MÀ ĂN CHUNG
                            if (_Borrow == 1 && _EatPrivate == 0 && _EatGeneral == 1)
                            {
                                Order_Money_Info zOrder = new Order_Money_Info();
                                zOrder.EmployeeKey = GVData1.Rows[i].Cells["EmployeeID"].Tag.ToString();
                                zOrder.EmployeeName = GVData1.Rows[i].Cells["Name"].Value.ToString();
                                zOrder.EmployeeID = GVData1.Rows[i].Cells["EmployeeID"].Value.ToString();
                                zOrder.StageKey = int.Parse(cbo_Stage.SelectedValue.ToString());
                                int zTeamKey = Employee_Data.FindTeamKey(zOrder.EmployeeKey);
                                zOrder.TeamKey = zTeamKey;
                                zOrder.OrderKey = _OrderKey;

                                zOrder.Time = 0;
                                zOrder.Money = 0;
                                zOrder.Kg = 0;

                                zOrder.TimePrivate = 0;
                                zOrder.KgPrivate = 0;
                                zOrder.MoneyPrivate = 0;

                                zOrder.Time_Borrow = 0;
                                zOrder.Kg_Borrow = 0;
                                zOrder.Money_Borrow = 0;

                                zOrder.Time_Eat = float.Parse(GVData1.Rows[i].Cells["Time"].Value.ToString());
                                zOrder.Kg_Eat = float.Parse(GVData1.Rows[i].Cells["KgProduct"].Value.ToString());
                                zOrder.Basket_Eat = float.Parse(GVData1.Rows[i].Cells["Basket"].Value.ToString());

                                if (GVData1.Rows[i].Cells["Money"].Value != null)
                                {
                                    zOrder.Money_Eat = float.Parse(GVData1.Rows[i].Cells["Money"].Value.ToString());
                                }
                                else
                                {
                                    zOrder.Money_Eat = 0;
                                }

                                zOrder.Basket_Borrow = 0;
                                zOrder.Basket = 0;
                                zOrder.BasketPrivate = 0;

                                zOrder.Category = 0;
                                zOrder.Category_Borrow = 0;
                                zOrder.Category_Eat = 1;
                                zOrder.CategoryPrivate = 0;
                                zOrder.OrderDate = dte_Date1.Value;

                                _Count = Order_Money_Data.Find_Employee(_OrderKey, zOrder.EmployeeKey);
                                if (_Count == 0)
                                {
                                    zOrder.Create();
                                    if (zOrder.Message == "11")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }
                                else
                                {
                                    zOrder.Update();
                                    if (zOrder.Message == "20")
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Thành Công";
                                    }
                                    else
                                    {
                                        GVData1.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                    }
                                }

                                //------ Tiền Chấm Công
                                if (cbo_Team.SelectedValue.ToInt() == 88 || cbo_Team.SelectedValue.ToInt() == 89)
                                {
                                    Money_TimeKeep_Info zMoney = new Money_TimeKeep_Info();
                                    zMoney.OrderKey = _OrderKey;
                                    zMoney.DateTimeKeep = dte_Date1.Value;
                                    zMoney.EmployeeKey = zOrder.EmployeeKey;
                                    zMoney.EmployeeID = zOrder.EmployeeID;
                                    zMoney.EmployeeName = zOrder.EmployeeName;
                                    zMoney.MoneyTimeKeep = zOrder.Money_Eat;
                                    int zCount = Money_TimeKeep_Data.Find_Employee(zMoney.OrderKey, zMoney.EmployeeKey);
                                    if (zCount == 0)
                                    {
                                        zMoney.CreatedBy = SessionUser.UserLogin.Key;
                                        zMoney.CreatedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Create();
                                    }
                                    else
                                    {
                                        zMoney.ModifiedBy = SessionUser.UserLogin.Key;
                                        zMoney.ModifiedName = SessionUser.Personal_Info.FullName;
                                        zMoney.Update();
                                    }
                                }
                            }
                        }
                        for (int a = 0; a < GVData1.Rows.Count - 1; a++)
                        {
                            Order_Employee_Info zEmployee = new Order_Employee_Info();
                            zEmployee.EmployeeKey = GVData1.Rows[a].Cells["EmployeeID"].Tag.ToString();
                            zEmployee.OrderKey = _OrderKey;
                            if (GVData1.Rows[a].Cells["Money"].Value != null)
                                zEmployee.Money = double.Parse(GVData1.Rows[a].Cells["Money"].Value.ToString());
                            else
                                zEmployee.Money = 0;
                            zEmployee.UpdateMoney();
                        }

                        timerTeam.Start();
                    }
                }
            }
        }
        private void timerWork_Tick(object sender, EventArgs e)
        {
            cbo_Team.SelectedIndex = _CountComboBoxTeam;
            int zCountComboBox = cbo_Stage.Items.Count - 1;
            int zCountComboBoxTeam = cbo_Team.Items.Count - 1;
            timerWork.Stop();
            _CountComboBox++;
            if (_CountComboBox > zCountComboBox)
            {
                timerWork.Stop();
                _CountComboBox = 0;
                if (_CountComboBoxTeam < zCountComboBoxTeam)
                {
                    _CountComboBoxTeam++;
                    timerWork.Start();
                }
                else
                {                  
                    timerWork.Stop();
                    MessageBox.Show("Đã hoàn thành");
                }
            }
            else
            {
                cbo_Stage.SelectedIndex = _CountComboBox;
                timerTeam.Start();
                _indexlistview = -1;
            }
        }
        #endregion

        #region [Timer Team Dif]
        private int _indexlistview_Dif = 0;
        private int _CountList_Dif = 0;
        private int _CountComboBox_Dif = 0;
        private void btn_Timer_Dif_Click(object sender, EventArgs e)
        {
            Timer_Combobox_Dif.Start();
        }
        private void Timer_Combobox_Dif_Tick(object sender, EventArgs e)
        {
            int zCountComboBox = cbo_Stage_Dif.Items.Count - 1;
            Timer_Combobox_Dif.Stop();
            _CountComboBox_Dif++;

            if (_CountComboBox_Dif > zCountComboBox)
            {
                Timer_Combobox_Dif.Stop();
                _CountComboBox_Dif = 0;

                MessageBox.Show("Đã hoàn thành");
            }
            else
            {
                cbo_Stage_Dif.SelectedIndex = _CountComboBox_Dif;
                Timer_Dif.Start();
                _indexlistview_Dif = -1;
            }
        }
        private void Timer_Dif_Tick(object sender, EventArgs e)
        {
            int zCountListView = LVData2.Items.Count;
            Timer_Dif.Stop();
            _indexlistview_Dif++;
            _CountList_Dif++;
            if (_CountList_Dif > zCountListView)
            {
                Timer_Dif.Stop();
                _CountList_Dif = 0;
                Timer_Combobox_Dif.Start();
            }
            else
            {
                _OrderKey = LVData2.Items[_indexlistview_Dif].Tag.ToString();
                int _FindOrderID = Order_Money_Data.Find_OrderID(_OrderKey);
                if (_FindOrderID > 0)
                {
                    Order_Money_Info zInfo = new Order_Money_Info();
                    zInfo.OrderKey = _OrderKey;
                    zInfo.Delete_Order();
                }
                string zOrderID = LVData2.Items[_indexlistview_Dif].SubItems[1].Text;
                txt_Money_TB_Dif.Text = zOrderID;
                txt_Quantity_Dif.Text = LVData2.Items[_indexlistview_Dif].SubItems[3].Text;
                txt_Price_Dif.Text = LVData2.Items[_indexlistview_Dif].SubItems[4].Text;
                txt_Basket_Dif.Text = LVData2.Items[_indexlistview_Dif].SubItems[5].Text;
                txt_Kilogram_Dif.Text = LVData2.Items[_indexlistview_Dif].SubItems[6].Text;
                txt_Time_Dif.Text = LVData2.Items[_indexlistview_Dif].SubItems[7].Text;
                if (_OrderKey.Length > 0)
                {
                    //LoadData_GVRate2();
                    // Count_Dif();
                    //GV_LoadData2();
                    if (GVData2.Rows.Count > 0)
                    {
                        // Count_Productivity_Dif();
                        // Lưu tiền nhân viên     
                        for (int i = 0; i < GVData2.Rows.Count - 1; i++)
                        {
                            Order_Money_Info zOrder = new Order_Money_Info();
                            zOrder.EmployeeKey = GVData2.Rows[i].Cells["EmployeeID"].Tag.ToString();
                            zOrder.EmployeeName = GVData2.Rows[i].Cells["Name"].Value.ToString();
                            zOrder.EmployeeID = GVData2.Rows[i].Cells["EmployeeID"].Value.ToString();
                            zOrder.StageKey = int.Parse(cbo_Stage_Dif.SelectedValue.ToString());
                            int zTeamKey = Employee_Data.FindTeamKey(zOrder.EmployeeKey);
                            zOrder.TeamKey = zTeamKey;
                            zOrder.OrderKey = _OrderKey;

                            int zFind_Date = Caculater_Productivity_Data.Find_Date(dte_Date2.Value);
                            if (GVData2.Rows[i].Cells["Borrow"].Value != null && GVData2.Rows[i].Cells["Private"].Value != null)
                            {
                                if (dte_Date2.Value.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    if (GVData2.Rows[i].Cells["Money"].Value != null)
                                    {
                                        zOrder.Money_Dif_Sunday = float.Parse(GVData2.Rows[i].Cells["Money"].Value.ToString());
                                    }
                                    else
                                    {
                                        zOrder.Money_Dif_Sunday = 0;
                                    }
                                }

                                if (zFind_Date > 1)
                                {
                                    if (GVData2.Rows[i].Cells["Money"].Value != null)
                                    {
                                        zOrder.Money_Dif_Holiday = float.Parse(GVData2.Rows[i].Cells["Money"].Value.ToString());
                                    }
                                    else
                                    {
                                        zOrder.Money_Dif_Holiday = 0;
                                    }
                                }

                                if (zFind_Date == 0 && dte_Date2.Value.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    if (GVData2.Rows[i].Cells["Money"].Value != null)
                                    {
                                        zOrder.Money_Dif = float.Parse(GVData2.Rows[i].Cells["Money"].Value.ToString());
                                    }
                                    else
                                    {
                                        zOrder.Money_Dif = 0;
                                    }
                                }
                            }
                            if (GVData2.Rows[i].Cells["Borrow"].Value != null && GVData2.Rows[i].Cells["General"].Value != null)
                            {
                                if (dte_Date2.Value.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    if (GVData2.Rows[i].Cells["Money"].Value != null)
                                    {
                                        zOrder.Money_Dif_Sunday = 0;
                                        zOrder.Money_Dif_Sunday_General = float.Parse(GVData2.Rows[i].Cells["Money"].Value.ToString());
                                    }
                                    else
                                    {
                                        zOrder.Money_Dif_Sunday_General = 0;
                                        zOrder.Money_Dif_Sunday = 0;
                                    }
                                }

                                if (zFind_Date > 1)
                                {
                                    if (GVData2.Rows[i].Cells["Money"].Value != null)
                                    {
                                        zOrder.Money_Dif_Holiday_General = float.Parse(GVData2.Rows[i].Cells["Money"].Value.ToString());
                                        zOrder.Money_Dif_Holiday = 0;
                                    }
                                    else
                                    {
                                        zOrder.Money_Dif_Holiday_General = 0;
                                        zOrder.Money_Dif_Holiday = 0;
                                    }
                                }

                                if (zFind_Date == 0 && dte_Date2.Value.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    if (GVData2.Rows[i].Cells["Money"].Value != null)
                                    {
                                        zOrder.Money_Dif_General = float.Parse(GVData2.Rows[i].Cells["Money"].Value.ToString());
                                        zOrder.Money_Dif = 0;
                                    }
                                    else
                                    {
                                        zOrder.Money_Dif_General = 0;
                                        zOrder.Money_Dif = 0;
                                    }
                                }
                            }
                            zOrder.OrderDate = dte_Date2.Value;

                            _Count = Order_Money_Data.Find_Employee(_OrderKey, zOrder.EmployeeKey);
                            if (_Count == 0)
                            {
                                zOrder.Create();
                                if (zOrder.Message == "11")
                                {
                                    GVData2.Rows[i].Cells["Message"].Value = "Thành Công";
                                }
                                else
                                {
                                    GVData2.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                }
                            }
                            else
                            {
                                zOrder.Update();
                                if (zOrder.Message == "20")
                                {
                                    GVData2.Rows[i].Cells["Message"].Value = "Thành Công";
                                }
                                else
                                {
                                    GVData2.Rows[i].Cells["Message"].Value = "Kiểm tra lại";
                                }
                            }
                        }
                        for (int a = 0; a < GVData2.Rows.Count - 1; a++)
                        {
                            Order_Employee_Info zEmployee = new Order_Employee_Info();
                            zEmployee.EmployeeKey = GVData2.Rows[a].Cells["EmployeeID"].Tag.ToString();
                            zEmployee.OrderKey = _OrderKey;
                            if (GVData2.Rows[a].Cells["Money"].Value != null)
                                zEmployee.Money = double.Parse(GVData2.Rows[a].Cells["Money"].Value.ToString());
                            else
                                zEmployee.Money = 0;
                            zEmployee.UpdateMoney();
                        }

                        Timer_Dif.Start();
                    }
                }
            }
        }

        #endregion



        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            txtTitle.Text = "Tính năng xuất [" + tabControl1.SelectedTab.Text + "]";
        }

        #region [Hide-UnHide]
        private void btnHide1_Click(object sender, EventArgs e)
        {
            Panel_Temp1.Visible = true;
            btnHide1.Visible = false;
            btnUnhide1.Visible = true;
        }

        private void btnUnhide1_Click(object sender, EventArgs e)
        {
            Panel_Temp1.Visible = false;
            btnHide1.Visible = true;
            btnUnhide1.Visible = false;
        }

        private void btnHide2_Click(object sender, EventArgs e)
        {
            Panel_Temp2.Visible = true;
            btnHide2.Visible = false;
            btnUnhide2.Visible = true;
        }

        private void btnUnhide2_Click(object sender, EventArgs e)
        {
            Panel_Temp2.Visible = false;
            btnHide2.Visible = true;
            btnUnhide2.Visible = false;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion                
    }
}