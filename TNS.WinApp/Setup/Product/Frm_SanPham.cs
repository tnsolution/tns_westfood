﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_SanPham : Form
    {
        List<Product_Info> _List;
        List<Product_Info> _ListSave;
        public Frm_SanPham()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            GVData.KeyDown += GVData_KeyDown;
            GVData.CellEndEdit += GVData_CellEndEdit;
            //GVData.DataError += GVData_DataError;

            btn_Save.Click += Btn_Save_Click;
            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_Import.Click += Btn_Import_Click;
            btn_Del.Click += Btn_Del_Click;

            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
            this.DoubleBuffered = true;
            Utils.DrawGVStyle(ref GVData);
            InitLayout_GV(GVData);
        }



        private void Frm_SanPham_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            LoadDataToToolbox.KryptonComboBox(cbo_Search, "SELECT CAST(ProductKey AS NVARCHAR(50)), ProductName FROM IVT_Product WHERE RecordStatus < 99 AND Parent = '0' ORDER BY Rank", "--Tất cả--");
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void GVData_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // Don't throw an exception when we're done.
            e.ThrowException = false;

            // Display an error message.
            string txt = "Error with " +
                GVData.Columns[e.ColumnIndex].HeaderText +
                "\n\n" + e.Exception;
            Utils.TNMessageBoxOK(txt, 2);

            // If this is true, then the user is trapped in this cell.
            e.Cancel = false;
        }

        private void GVData_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            switch (GVData.CurrentCell.ColumnIndex)
            {
                case 1: // autocomplete for product
                    if (GVData.CurrentCell.Value != null && GVData.CurrentCell.Value.ToString().Trim() != string.Empty)
                    {
                        string ID = GVData.CurrentCell.Value.ToString().Trim();
                        Product_Info zInfo = new Product_Info();
                        zInfo.Get_Product_Info_ID(ID);
                        if (zInfo.ProductKey.Length > 0)
                        {
                            Utils.TNMessageBoxOK("Mã sản phẩm này đã có rồi !.", 1);
                            GVData.CurrentCell.Value = "";
                        }
                    }
                    break;
            }
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            GVData.Rows.Clear();

            string Key = cbo_Search.SelectedValue.ToString();
            DataTable zTable = Product_Data.List_ThanhPham(Key, txt_Search.Text.Trim());
            InitData_GV(GVData, zTable);
            this.Cursor = Cursors.Default;
        }
        private void InitLayout_GV(DataGridView GV)
        {
            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("ProductID", "Mã");
            GV.Columns.Add("ProductName", "Tên");

            DataGridViewComboBoxColumn cboUnit = new DataGridViewComboBoxColumn();
            cboUnit.HeaderText = "Đơn vị";
            cboUnit.Name = "cboUnit";
            LoadDataToToolbox.ComboBoxData(cboUnit, "SELECT UnitKey, UnitName FROM IVT_Product_Unit WHERE RecordStatus < 99 ORDER BY UnitName", true);
            cboUnit.Width = 150;
            cboUnit.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboUnit);

            DataGridViewComboBoxColumn cboCategory = new DataGridViewComboBoxColumn();
            cboCategory.HeaderText = "Loại";
            cboCategory.Name = "cboCategory";
            LoadDataToToolbox.ComboBoxData(cboCategory, " SELECT CategoryKey, CategoryName FROM IVT_Product_Category WHERE  RecordStatus< 99 ORDER BY [RANK]", true);
            cboCategory.Width = 150;
            cboCategory.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboCategory);

            DataGridViewComboBoxColumn cboNguyenLieu = new DataGridViewComboBoxColumn();
            cboNguyenLieu.HeaderText = "Nhóm sản phẩm";
            cboNguyenLieu.Name = "cboNguyenLieu";
            LoadDataToToolbox.ComboBoxData(cboNguyenLieu, "SELECT UPPER(CAST(ProductKey AS NVARCHAR(50))), ProductID FROM IVT_Product WHERE RecordStatus <> 99 AND Parent ='0' AND ProductID IS NOT NULL AND ProductID !=''", false);
            cboNguyenLieu.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
            GVData.Columns.Add(cboNguyenLieu);

            GV.Columns.Add("Rank", "Sắp xếp");
            GV.Columns.Add("Description", "Ghi chú");

            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["ProductID"].Width = 100;
            GV.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["ProductName"].Width = 450;
            GV.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GV.Columns["Rank"].Width = 70;
            GV.Columns["Rank"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GV.Columns["cboNguyenLieu"].Width = 120;

            GV.Columns["Description"].Width = 250;
            GV.Columns["Description"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        private void InitData_GV(DataGridView GV, DataTable TableView)
        {
            GV.Rows.Clear();
            _List = new List<Product_Info>();
            if (TableView.Rows.Count > 0)
            {
                foreach (DataRow r in TableView.Rows)
                {
                    _List.Add(new Product_Info(r["ProductKey"].ToString())
                    {

                    });
                }
            }
            if (_List.Count > 0)
            {
                for (int i = 0; i < _List.Count; i++)
                {
                    Product_Info zProduct = _List[i];

                    GV.Rows.Add();
                    GV.Rows[i].Tag = zProduct;
                    GV.Rows[i].Cells["No"].Value = (i + 1).ToString();
                    GV.Rows[i].Cells["ProductID"].Tag = zProduct.ProductKey;
                    GV.Rows[i].Cells["ProductID"].Value = zProduct.ProductID;
                    GV.Rows[i].Cells["ProductName"].Value = zProduct.ProductName;
                    GV.Rows[i].Cells["cboUnit"].Value = zProduct.BasicUnit;
                    GV.Rows[i].Cells["cboCategory"].Value = zProduct.CategoryKey;
                    GV.Rows[i].Cells["cboNguyenLieu"].Value = zProduct.Parent.ToString().Trim().ToUpper();
                    GV.Rows[i].Cells["Description"].Value = zProduct.Description;
                    GV.Rows[i].Cells["Rank"].Value = zProduct.Rank;
                }
            }
        }
        private void Btn_Del_Click(object sender, EventArgs e)
        {
            if (Role.RoleDel == 1)
            {
                if (Utils.TNMessageBox("Bạn có chắc xóa thông tin này ?", 2) == "Y")
                {
                    for (int i = 0; i < GVData.Rows.Count; i++)
                    {
                        if (GVData.Rows[i].Selected)
                        {
                            if (GVData.Rows[i].Tag != null)
                            {
                                string Key = GVData.Rows[i].Cells["ProductID"].Tag.ToString();
                                Product_Info zInfo = new Product_Info(Key);
                                zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                                zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                                zInfo.Delete();

                                GVData.Rows.Remove(GVData.Rows[i]);
                            }
                            else
                            {
                                GVData.Rows.RemoveAt(i);
                            }

                            GVData.CommitEdit(DataGridViewDataErrorContexts.Commit);
                        }
                    }
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Bạn chưa được phân quyền xóa thông tin", 2);
            }
        }
        private void GVData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (Role.RoleDel == 1)
                {
                    if (Utils.TNMessageBox("Bạn có xóa thông tin này ?", 2) == "Y")
                    {
                        for (int i = 0; i < GVData.Rows.Count; i++)
                        {
                            if (GVData.Rows[i].Selected)
                            {
                                if (GVData.Rows[i].Tag != null)
                                {
                                    string Key = GVData.Rows[i].Cells["ProductID"].Tag.ToString();
                                    Product_Info zInfo = new Product_Info(Key);
                                    zInfo.ModifiedBy = SessionUser.UserLogin.Key;
                                    zInfo.ModifiedName = SessionUser.UserLogin.EmployeeName;
                                    zInfo.Delete();

                                    GVData.Rows.Remove(GVData.Rows[i]);
                                }
                                else
                                {
                                    if (GVData.Rows.Count - 1 == i)
                                        Utils.TNMessageBoxOK("Không thể xóa dòng cuối", 1);
                                    else
                                        GVData.Rows.RemoveAt(i);
                                }

                                GVData.CommitEdit(DataGridViewDataErrorContexts.Commit);
                            }
                        }
                    }
                }
                else
                {
                    Utils.TNMessageBoxOK("Bạn chưa được phân quyền xóa thông tin", 2);
                }
            }

        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            SaveProduct();

        }
        private void SaveProduct()
        {
            if (GVData.Rows.Count > 0)
            {
                _ListSave = new List<Product_Info>();
                for (int i = 0; i < GVData.Rows.Count - 1; i++)
                {
                    DataGridViewRow GvRow = GVData.Rows[i];
                    Product_Info zNewChild = (Product_Info)GvRow.Tag;
                    if (zNewChild != null && GvRow.Cells["ProductID"].Tag != null)
                    {
                        if (GvRow.Cells["ProductID"].Tag.ToInt() == 99)
                        {
                            zNewChild.RecordStatus = 99;
                        }
                        else
                        {
                            if (GvRow.Cells["ProductID"].Tag != null)
                                zNewChild.ProductKey = GvRow.Cells["ProductID"].Tag.ToString();
                            if (GvRow.Cells["ProductID"].Value != null)
                                zNewChild.ProductID = GvRow.Cells["ProductID"].Value.ToString();
                            if (GvRow.Cells["ProductName"].Value != null)
                                zNewChild.ProductName = GvRow.Cells["ProductName"].Value.ToString();
                            if (GvRow.Cells["cboUnit"].Value != null)
                                zNewChild.BasicUnit = GvRow.Cells["cboUnit"].Value.ToInt();
                            if (GvRow.Cells["cboCategory"].Value != null)
                                zNewChild.CategoryKey = GvRow.Cells["cboCategory"].Value.ToInt();
                            if (GvRow.Cells["cboNguyenLieu"].Value != null)
                                zNewChild.Parent = GvRow.Cells["cboNguyenLieu"].Value.ToString();
                            if (GvRow.Cells["Description"].Value != null)
                                zNewChild.Description = GvRow.Cells["Description"].Value.ToString();
                            if (GvRow.Cells["Rank"].Value != null)
                                zNewChild.Rank = GvRow.Cells["Rank"].Value.ToInt();

                            Product_Info zOldChild = new Product_Info(zNewChild.ProductKey);
                            if (zOldChild != null)
                            {
                                //kiem tra 2 đối tượng có thay đổi giá trị hay không có => update 
                                bool Exist = DeepCompare(zOldChild, zNewChild);
                                if (!Exist)
                                {
                                    zNewChild.RecordStatus = 2;
                                }
                            }
                        }

                        _ListSave.Add(zNewChild);
                    }
                    else
                    {
                        if (GvRow.Cells["ProductID"].Value != null && GvRow.Cells["ProductID"].Value.ToString() != "")
                        {
                            zNewChild = new Product_Info();
                            zNewChild.RecordStatus = 1;
                            zNewChild.ProductKey = "";
                            if (GvRow.Cells["ProductID"].Value != null)
                                zNewChild.ProductID = GvRow.Cells["ProductID"].Value.ToString();
                            if (GvRow.Cells["ProductName"].Value != null)
                                zNewChild.ProductName = GvRow.Cells["ProductName"].Value.ToString();
                            if (GvRow.Cells["cboUnit"].Value != null)
                                zNewChild.BasicUnit = GvRow.Cells["cboUnit"].Value.ToInt();
                            if (GvRow.Cells["cboCategory"].Value != null)
                                zNewChild.CategoryKey = GvRow.Cells["cboCategory"].Value.ToInt();
                            if (GvRow.Cells["cboNguyenLieu"].Value != null)
                                zNewChild.Parent = GvRow.Cells["cboNguyenLieu"].Value.ToString();
                            if (GvRow.Cells["Description"].Value != null)
                                zNewChild.Description = GvRow.Cells["Description"].Value.ToString();
                            if (GvRow.Cells["Rank"].Value != null)
                                zNewChild.Rank = GvRow.Cells["Rank"].Value.ToInt();
                            _ListSave.Add(zNewChild);
                        }
                    }
                }

                string zResult = "";
                for (int i = 0; i < _ListSave.Count; i++)
                {
                    Product_Info zProduct = _ListSave[i];
                    zProduct.CreatedBy = SessionUser.UserLogin.ModifiedBy;
                    zProduct.CreatedName = SessionUser.UserLogin.ModifiedName;
                    zProduct.ModifiedBy = SessionUser.UserLogin.ModifiedBy;
                    zProduct.ModifiedName = SessionUser.UserLogin.ModifiedName;
                    switch (zProduct.RecordStatus)
                    {
                        case 99:
                            zProduct.Delete();
                            break;
                        case 1:
                            zProduct.Create();
                            break;
                        case 2:
                            zProduct.Update();
                            break;
                    }
                    if (zProduct.RecordStatus != 0)
                    {
                        if (zProduct.Message.Substring(0, 2) != "11" && zProduct.Message != "20" && zProduct.Message != "30")
                            zResult += zProduct.Message;
                    }

                }

                if (zResult == "")
                {
                    Utils.TNMessageBoxOK("Cập nhật thành công!", 3);
                    Btn_Search_Click(null,null);
                }
                else
                    Utils.TNMessageBoxOK("Lỗi.Vui lòng liên hệ IT! " + zResult, 4);
            }
        }
        #region[So sánh 2 đối tượng]
        public static bool Compare<T>(T e1, T e2)
        {
            bool flag = true;
            bool match = false;
            int countFirst, countSecond;
            foreach (PropertyInfo propObj1 in e1.GetType().GetProperties())
            {
                var propObj2 = e2.GetType().GetProperty(propObj1.Name);
                if (propObj1.PropertyType.Name.Equals("List`1"))
                {
                    dynamic objList1 = propObj1.GetValue(e1, null);
                    dynamic objList2 = propObj2.GetValue(e2, null);
                    countFirst = objList1.Count;
                    countSecond = objList2.Count;
                    if (countFirst == countSecond)
                    {
                        countFirst = objList1.Count - 1;
                        while (countFirst > -1)
                        {
                            match = false;
                            countSecond = objList2.Count - 1;
                            while (countSecond > -1)
                            {
                                match = Compare(objList1[countFirst], objList2[countSecond]);
                                if (match)
                                {
                                    objList2.Remove(objList2[countSecond]);
                                    countSecond = -1;
                                    match = true;
                                }
                                if (match == false && countSecond == 0)
                                {
                                    return false;
                                }
                                countSecond--;
                            }
                            countFirst--;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else if (!(propObj1.GetValue(e1, null).Equals(propObj2.GetValue(e2, null))))
                {
                    flag = false;
                    return flag;
                }
            }
            return flag;
        }

        public static bool DeepCompare(object obj, object another)
        {
            if (ReferenceEquals(obj, another)) return true;
            if ((obj == null) || (another == null)) return false;
            //Compare two object's class, return false if they are difference
            if (obj.GetType() != another.GetType()) return false;

            var result = true;
            //Get all properties of obj
            //And compare each other
            foreach (var property in obj.GetType().GetProperties())
            {
                var objValue = property.GetValue(obj, null);
                var anotherValue = property.GetValue(another, null);
                if (!objValue.Equals(anotherValue))
                {
                    result = false;
                    break;
                }
            }

            return result;
        }
        #endregion
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Danh sách thành phẩm.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                Path = FDialog.FileName;
                DataTable dt = new DataTable();
                foreach (DataGridViewColumn col in GVData.Columns)
                {
                    dt.Columns.Add(col.HeaderText);
                }

                for (int i = 0; i < GVData.RowCount - 1; i++)
                {
                    DataGridViewRow row = GVData.Rows[i];
                    DataRow dRow = dt.NewRow();

                    int k = 0;
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        if (k == 3)
                        {
                            int Key = cell.Value.ToInt();
                            Product_Unit_Info zgroup = new Product_Unit_Info(Key);
                            dRow[cell.ColumnIndex] = zgroup.Unitname;
                        }
                        else if (k == 4)
                        {
                            int Key = cell.Value.ToInt();
                            Product_Category_Info zgroup = new Product_Category_Info(Key);
                            dRow[cell.ColumnIndex] = zgroup.CategoryName;
                        }
                        else if (k == 5)
                        {
                            string Key = cell.Value.ToString();
                            Product_Info zgroup = new Product_Info(Key);
                            dRow[cell.ColumnIndex] = zgroup.ProductID;
                        }
                        else
                            dRow[cell.ColumnIndex] = cell.Value;
                        k++;
                    }
                    dt.Rows.Add(dRow);
                }
                string Message = ExportTableToExcel(dt, Path);
                if (Message != "OK")
                    Utils.TNMessageBoxOK(Message, 4);
                else
                {
                    Message = "Đã tạo tập tin thành công !." + Environment.NewLine + "Bạn có muốn mở thư mục chứa ?.";
                    if (Utils.TNMessageBox(Message, 1) == "Y")
                    {
                        Process.Start(Path);
                    }
                }
            }
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    if (!IsFileLocked(newFile))
                    {
                        newFile.Delete();
                    }
                    else
                    {
                        return "Tập tin đang sử dụng !.";
                    }
                }

                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(2, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 60;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 20;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 20;
                    worksheet.Column(3).Width = 40;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng


                    worksheet.Column(7).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //Canh phải số
                    //for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    //{
                    //for (int j = 4; j < zTable.Columns.Count; j++)
                    //{
                    //worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //}
                    // }
                    //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }
        private void Btn_Import_Click(object sender, EventArgs e)
        {
            Frm_Import_SanPham frm = new Frm_Import_SanPham();
            frm.ShowDialog();
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Get Auth-Action-Chg_Lang ]
        Access_Role_Info Role;
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 0)
            {
                btn_Save.Enabled = false;
            }
            if (Role.RoleAdd == 0 && Role.RoleEdit == 1)
            {
                btn_Save.Enabled = true;
            }
            if (Role.RoleDel == 0)
            {
                btn_Del.Visible = false;
            }
        }
        #endregion
    }
}
