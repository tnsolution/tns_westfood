﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_GioChamCong : Form
    {
        DateTime _FromDate;
        DateTime _ToDate;
        private int _BranchKey = 0;
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Search = "";
        public Frm_GioChamCong()
        {
            InitializeComponent();
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Export.Click += Btn_Export_Click;
            btn_Search.Click += Btn_Search_Click;
            cbo_Branch.SelectedIndexChanged += Cbo_Branch_SelectedIndexChanged;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
        }

        private void Frm_GioChamCong_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Tất cả ----");
            LoadDataToToolbox.KryptonComboBox(cbo_Branch, "SELECT BranchKey,BranchName FROM SYS_Branch WHERE BranchKey != 98  AND RecordStatus < 99", "---- Tất cả ----");
            DateTime DateView = SessionUser.Date_Work;
            dte_FromDate.Value = new DateTime(DateView.Year, DateView.Month, 1, 0, 0, 0);
            dte_ToDate.Value = dte_FromDate.Value.AddMonths(1).AddDays(-1);
            dte_ToDate.Value = new DateTime(dte_ToDate.Value.Year, dte_ToDate.Value.Month, dte_ToDate.Value.Day, 23, 59, 59);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Cbo_Branch_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE BranchKey = " + cbo_Branch.SelectedValue.ToInt() + "  AND RecordStatus< 99", "---- Tất cả----");

        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Tất cả ----");
        }
        private int _STT = 1;
        DataTable _Table;
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _FromDate = dte_FromDate.Value;
            _ToDate = dte_ToDate.Value;
            _BranchKey = cbo_Branch.SelectedValue.ToInt();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _Search = txt_Search.Text.Trim();

            try
            {
                _STT = 1;
                _Table = new DataTable();

                if (rdoTongGio.Checked)
                {
                    using (Frm_Loading frm = new Frm_Loading(TongGio))
                    {
                        frm.ShowDialog(this);
                    }
                }
                if (rdoGiodu.Checked)
                {
                    using (Frm_Loading frm = new Frm_Loading(GioDu))
                    {
                        frm.ShowDialog(this);
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK( ex.ToString(), 4);
            }
        }

        #region TONGGIO
        void TongGio()
        {
            _Table = Report.BaoCaoThoiGian(_FromDate, _ToDate, _BranchKey, _DepartmentKey, _TeamKey, _Search);
            if (_Table.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    GV_TongGio(_Table);

                    Row zGvRow;

                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    string MAPHONGBAN = _Table.Rows[0]["MAPHONGBAN"].ToString();
                    string TENPHONGBAN = _Table.Rows[0]["TENPHONGBAN"].ToString();
                    DataRow[] Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow_TongGio(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, 0);
                    }
                    for (int i = 0; i < _Table.Rows.Count; i++)
                    {
                        DataRow r = _Table.Rows[i];
                        if (MAPHONGBAN != r["MAPHONGBAN"].ToString())
                        {
                            #region [GROUP]
                            MAPHONGBAN = r["MAPHONGBAN"].ToString();
                            TENPHONGBAN = r["TENPHONGBAN"].ToString();
                            Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_TongGio(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_TongGio(zGvRow, r, i + NoGroup - 1);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_TongGio(zGvRow, _Table, RowTam + 1);
                }));
            }
        }
        void GV_TongGio(DataTable Table)
        {
            int ToTalCol = Table.Columns.Count;
            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                string zDayName = Table.Columns[i].ColumnName;
                GVData.Rows[0][nCol] = "Ngày " + zDayName;
                GVData.Cols[nCol].Width = 70;
                DateTime Date = new DateTime(_FromDate.Year, _ToDate.Month, zDayName.ToInt(), 0, 0, 0);
                if (Date.DayOfWeek.ToString() == "Sunday")
                    GVData.Cols[nCol].StyleNew.BackColor = Color.LightPink;
                nCol++;
            }
            GVData.Rows[0][ToTalCol - 1] = "TỔNG CỘNG";

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Cols[0].Width = 40;
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].Width = 200;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].Width = 120;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 3; i < ToTalCol; i++)
            {

                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;

            }
            GVData.Cols[ToTalCol - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void HeaderRow_TongGio(Row RowView, DataTable Table, string MaPhongBan, string TenPhongBan, int No)
        {
            RowView[0] = "";
            RowView[1] = TenPhongBan;
            RowView[2] = "Số nhân sự " + Table.Select("MAPHONGBAN='" + MaPhongBan + "'").Length;

            int nCol = 3;
            double TotalMinus = 0;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double TongSoPhut = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        TongSoPhut += phut + gio * 60;
                    }
                }

                RowView[nCol] = SoGio(TongSoPhut);
                TotalMinus += TongSoPhut;
                nCol++;
            }

            RowView[nCol] = SoGio(TotalMinus);
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void DetailRow_TongGio(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[2] = rDetail["MANHANVIEN"].ToString().Trim();
            RowView[1] = rDetail["HOTEN"].ToString().Trim();
            double tongthuong = 0;
            int nCol = 3;
            for (int i = 4; i < rDetail.ItemArray.Length; i++)
            {
                if (rDetail[i].ToString().Contains(":"))
                {
                    string[] temp = rDetail[i].ToString().Split(':');
                    int gio = temp[0].ToInt();
                    int phut = temp[1].ToInt();
                    if (gio > 0)
                    {
                        RowView[nCol] = (gio).ToString().PadLeft(2, '0') + ":" + phut.ToString().PadLeft(2, '0');
                        tongthuong += phut + gio * 60;
                    }
                }
                nCol++;
            }
            RowView[nCol] = SoGio(tongthuong);
        }
        private void TotalRow_TongGio(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;

            int nCol = 3;
            double TotalMinus = 0;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double TongSoPhut = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        if (gio > 0)
                        {
                            TongSoPhut += phut + gio * 60;
                        }
                    }
                }

                RowView[nCol] = SoGio(TongSoPhut);
                TotalMinus += TongSoPhut;
                nCol++;
            }
            RowView[nCol] = SoGio(TotalMinus);
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }

        #endregion

        #region GIODU
        void GioDu()
        {
            _Table = Report.BaoCaoThoiGian(_FromDate, _ToDate, _BranchKey, _DepartmentKey, _TeamKey, _Search);
            if (_Table.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    GV_GioDu(_Table);

                    Row zGvRow;

                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    string MAPHONGBAN = _Table.Rows[0]["MAPHONGBAN"].ToString();
                    string TENPHONGBAN = _Table.Rows[0]["TENPHONGBAN"].ToString();
                    DataRow[] Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow_GioDu(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, 0);
                    }
                    for (int i = 0; i < _Table.Rows.Count; i++)
                    {
                        DataRow r = _Table.Rows[i];
                        if (MAPHONGBAN != r["MAPHONGBAN"].ToString())
                        {
                            #region [GROUP]
                            MAPHONGBAN = r["MAPHONGBAN"].ToString();
                            TENPHONGBAN = r["TENPHONGBAN"].ToString();
                            Array = _Table.Select("MAPHONGBAN='" + MAPHONGBAN + "'");
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow_GioDu(zGvRow, zGroup, MAPHONGBAN, TENPHONGBAN, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow_GioDu(zGvRow, r, i + NoGroup - 1);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow_GioDu(zGvRow, _Table, RowTam + 1);
                }));
            }
        }
        void GV_GioDu(DataTable Table)
        {
            int ToTalCol = Table.Columns.Count;
            GVData.Cols.Add(ToTalCol + 2);
            GVData.Rows.Add(1);

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "HỌ VÀ TÊN";
            GVData.Rows[0][2] = "SỐ THẺ";
            int nCol = 3;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                string zDayName = Table.Columns[i].ColumnName;
                GVData.Rows[0][nCol] = "Ngày" + zDayName;
                GVData.Cols[nCol].Width = 70;
                DateTime Date = new DateTime(_FromDate.Year, _ToDate.Month, zDayName.ToInt(), 0, 0, 0);
                if (Date.DayOfWeek.ToString() == "Sunday")
                    GVData.Cols[nCol].StyleNew.BackColor = Color.LightPink;
                nCol++;
            }
            GVData.Rows[0][GVData.Cols.Count - 3] = "TỔNG CỘNG NGÀY THƯỜNG";
            GVData.Rows[0][GVData.Cols.Count - 2] = "TỔNG CỘNG NGÀY CN";
            GVData.Rows[0][GVData.Cols.Count - 1] = "TỔNG CỘNG";

            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.Custom;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;

            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;
            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Cols[0].Width = 40;
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].Width = 200;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].Width = 120;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Rows[0].Height = 60;
            GVData.Rows[0].StyleNew.WordWrap = true;
            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;
            for (int i = 3; i <= ToTalCol+1; i++)
            {

                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            GVData.Cols[GVData.Cols.Count - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[GVData.Cols.Count - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[GVData.Cols.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void HeaderRow_GioDu(Row RowView, DataTable Table, string MaPhongBan, string TenPhongBan, int No)
        {
            RowView[0] = "";
            RowView[1] = TenPhongBan;
            RowView[2] = "Số nhân sự " + Table.Select("MAPHONGBAN='" + MaPhongBan + "'").Length;

            int nCol = 3;
            double TotalMinus_Thuong  = 0;
            double TotalMinus_CN = 0;
            double TotalMinus_Tong = 0;
            int zDay = 1;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double tongthuong = 0;
                double tongCN = 0;
                double tong = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        if (gio - 8 >= 0 && phut >= 0)
                        {
                            //TongSoPhut += phut + (gio - 8) * 60;

                            DateTime zD = new DateTime(_ToDate.Year, _ToDate.Month, zDay);
                            if (zD.DayOfWeek == DayOfWeek.Sunday)
                            {
                                tongCN += phut + (gio - 8) * 60;
                            }
                            else
                            {
                                tongthuong += phut + (gio - 8) * 60;
                            }

                            tong = tongCN + tongthuong;
                        }
                    }
                }

                RowView[nCol] = SoGio(tongthuong);
                RowView[nCol+1] = SoGio(tongCN);
                RowView[nCol+2] = SoGio(tong);


                TotalMinus_Thuong += tongthuong;
                TotalMinus_CN += tongCN;
                TotalMinus_Tong += tong;
                nCol++;
                zDay++;
            }
            RowView[nCol] = SoGio(TotalMinus_Thuong);
            RowView[nCol+1] = SoGio(TotalMinus_CN);
            RowView[nCol+2] = SoGio(TotalMinus_Tong);

            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }
        private void DetailRow_GioDu(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[2] = rDetail["MANHANVIEN"].ToString().Trim();
            RowView[1] = rDetail["HOTEN"].ToString().Trim();

            double tongthuong = 0;
            double tongCN = 0;
            double tong = 0;

            int nCol = 3;
            int zDay = 1;
            for (int i = 4; i < rDetail.ItemArray.Length; i++)
            {
                if (rDetail[i].ToString().Contains(":"))
                {
                    string[] temp = rDetail[i].ToString().Split(':');
                    int gio = temp[0].ToInt();
                    int phut = temp[1].ToInt();
                    if (gio - 8 >= 0 && phut >= 0)
                    {
                        RowView[nCol] = (gio - 8).ToString().PadLeft(2, '0') + ":" + phut.ToString().PadLeft(2, '0');

                        DateTime zD = new DateTime(_ToDate.Year, _ToDate.Month, zDay);
                        if (zD.DayOfWeek == DayOfWeek.Sunday)
                        {
                            tongCN += phut + (gio - 8) * 60;
                        }
                        else
                        {
                            tongthuong += phut + (gio - 8) * 60;
                        }

                        tong = tongCN + tongthuong;
                    }
                }
                nCol++;
                zDay++;

            }

            RowView[nCol] = SoGio(tongthuong);
            RowView[nCol + 1] = SoGio(tongCN);
            RowView[nCol + 2] = SoGio(tong);
        }
        private void TotalRow_GioDu(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;

            int nCol = 3;
            double TotalMinus_Thuong = 0;
            double TotalMinus_CN = 0;
            double TotalMinus_Tong = 0;
            int zDay = 1;
            for (int i = 4; i < Table.Columns.Count; i++)
            {
                double tongthuong = 0;
                double tongCN = 0;
                double tong = 0;
                foreach (DataRow r in Table.Rows)
                {
                    if (r[i].ToString().Contains(":"))
                    {
                        string[] temp = r[i].ToString().Split(':');
                        int gio = temp[0].ToInt();
                        int phut = temp[1].ToInt();
                        if (gio - 8 >= 0 && phut >= 0)
                        {
                            DateTime zD = new DateTime(_ToDate.Year, _ToDate.Month, zDay);
                            if (zD.DayOfWeek == DayOfWeek.Sunday)
                            {
                                tongCN += phut + (gio - 8) * 60;
                            }
                            else
                            {
                                tongthuong += phut + (gio - 8) * 60;
                            }

                            tong = tongCN + tongthuong;
                        }
                    }
                }

                RowView[nCol] = SoGio(tongthuong);
                RowView[nCol + 1] = SoGio(tongCN);
                RowView[nCol + 2] = SoGio(tong);


                TotalMinus_Thuong += tongthuong;
                TotalMinus_CN += tongCN;
                TotalMinus_Tong += tong;
                nCol++;
                zDay++;
            }
            RowView[nCol] = SoGio(TotalMinus_Thuong);
            RowView[nCol + 1] = SoGio(TotalMinus_CN);
            RowView[nCol + 2] = SoGio(TotalMinus_Tong);
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        #endregion
        private void Btn_Export_Click(object sender, EventArgs e)
        {

            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            string zName = "";
            if (rdoTongGio.Checked)
            {
                zName = rdoTongGio.Text;
            }
            if (rdoGiodu.Checked)
            {
                zName = rdoGiodu.Text;
            }
            FDialog.FileName = "Báo_Cáo_Chấm_Công-" + zName + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }
        }
        string SoGio(double TongSoPhut)
        {
            if (TongSoPhut > 0)
            {
                int zHour = TongSoPhut.ToInt() / 60;
                int zMinus = TongSoPhut.ToInt() % 60;
                return zHour.ToString().PadLeft(2, '0') + ":" + zMinus.ToString().PadLeft(2, '0');
            }
            return string.Empty;
        }
        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
