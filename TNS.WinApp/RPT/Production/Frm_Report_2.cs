﻿using OfficeOpenXml;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;
using Excel = Microsoft.Office.Interop.Excel;
using ReportAll = TNS.CORE.Report;

namespace TNS.WinApp
{
    public partial class Frm_Report_2 : Form
    {
        const int _HeaderHeight = 80;
        int _TeamKey = 0;
        int _TotalCol = 0;
        int _CountTable = 0;
        string[] TitleMerge;
        string[] TitleColumn;

        public Frm_Report_2()
        {
            InitializeComponent();
            LVData.SelectedIndexChanged += LVData_Click;
        }
        private void Frm_Report_2_Load(object sender, EventArgs e)
        {
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVEmployee, true);
            Utils.DrawLVStyle(ref LVData);
            Utils.SizeLastColumn_LV(LVData);

            dte_FromDate.Value = SessionUser.Date_Work;
            dte_ToDate.Value = SessionUser.Date_Work;

            InitLV_Layout(LVData);
            InitLV_Data(Teams_Data.List());


            GVEmployee.ColumnHeadersHeight = _HeaderHeight;
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }


        private void InitLV_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 40;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 350;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }
        private void InitLV_Data(DataTable Table)
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LVData;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            LV.Items.Clear();
            int n = Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = Table.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }
            this.Cursor = Cursors.Default;
        }
        private void InitGV_Paint(DataGridView GV, DataTable Table)
        {
            /*
             * Dùng tạo column header name cho 2 dòng column trộn và không trộn
             */

            int nRow0 = 0;
            int nRow1 = 3;

            _TotalCol = GV.ColumnCount - 1;

            TitleMerge = new string[_TotalCol - 3];         //column merge
            TitleColumn = new string[GV.ColumnCount];   //column bình thường

            TitleColumn[0] = "";
            TitleColumn[1] = "Tên";
            TitleColumn[2] = "Mã";
            TitleColumn[_TotalCol] = "Tổng";

            string ColumnName = "";
            for (int i = 2; i < Table.Columns.Count; i++)
            {
                string ColName = Table.Columns[i].ColumnName;
                ColName = ColName.Substring(0, ColName.Length - 3);
                if (ColumnName != ColName)
                {
                    ColumnName = ColName;
                    TitleMerge[nRow0] = ColumnName;
                    nRow0++;

                    TitleColumn[nRow1] = "Số lượng";
                    TitleColumn[nRow1 + 1] = "Lương khoán";
                    nRow1 += 2;
                }
            }

            GV.Scroll += GridView_Scroll;
            GV.Paint += GridView_Paint;
            GV.Click += GridView_Click;

        }
        private void InitGV_Layout(DataGridView GV, DataTable zTable)
        {
            GV.Columns.Clear();
            GV.Rows.Clear();

            if (zTable.Rows.Count > 0)
            {
                GV.Columns.Add("No", "#");
                GV.Columns.Add("EmployeeName", "");
                GV.Columns.Add("EmployeeID", "");
                for (int j = 2; j < zTable.Columns.Count; j++)
                {
                    string Name = zTable.Columns[j].ColumnName;
                    GV.Columns.Add(Name, "");
                    GV.Columns[Name].Width = 100;
                    GV.Columns[Name].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                    GV.Columns[Name].ReadOnly = true;
                }
                GV.Columns.Add("ToTal", "");

                GV.Columns["No"].Width = 40;
                GV.Columns["No"].Frozen = true;
                GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                GV.Columns["No"].ReadOnly = true;

                GV.Columns["EmployeeName"].Width = 180;
                GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                GV.Columns["EmployeeName"].ReadOnly = true;

                GV.Columns["EmployeeID"].Width = 80;
                GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                GV.Columns["EmployeeID"].ReadOnly = true;
                GV.Columns["EmployeeID"].Frozen = true;

                GV.Columns["ToTal"].Width = 120;
                GV.Columns["ToTal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                GV.Columns["ToTal"].DefaultCellStyle.ForeColor = Color.Navy;
                GV.Columns["ToTal"].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
                GV.Columns["ToTal"].ReadOnly = true;
            }
        }
        private void InitGV_Data(DataGridView GV, DataTable zTable)
        {
            GV.Rows.Clear();
            DataGridViewRow nRowView;
            int rows = 0;
            if (zTable.Rows.Count > 0)
            {
                double[] Total = new double[zTable.Columns.Count];
                double _TOTAL = 0;

                foreach (DataRow nRow in zTable.Rows)
                {
                    double zTotalRow = 0;
                    GV.Rows.Add();
                    nRowView = GV.Rows[rows];
                    nRowView.Cells["No"].Value = (rows + 1).ToString();
                    nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
                    nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();

                    for (int j = 2; j < zTable.Columns.Count; j++)
                    {
                        string Name = zTable.Columns[j].ColumnName;
                        double zValueCell = 0;
                        if (nRow[Name].ToString().Trim().Length == 0)
                        {
                            nRowView.Cells[Name].Value = "0";
                        }
                        else
                        {
                            zValueCell = nRow[Name].ToDouble();
                            nRowView.Cells[Name].Value = zValueCell.ToString("n1");
                            if (j % 2 != 0)
                                zTotalRow += zValueCell;
                        }

                        Total[j] += zValueCell;
                    }
                    nRowView.Cells["ToTal"].Value = zTotalRow.ToString("n1");
                    _TOTAL += zTotalRow;
                    rows++;
                }

                GV.Rows.Add();
                nRowView = GV.Rows[rows];
                nRowView.Cells["EmployeeName"].Value = "TỔNG";
                nRowView.DefaultCellStyle.ForeColor = Color.Navy;
                nRowView.DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

                int col = 3;
                for (int j = 2; j < Total.Length; j++)
                {
                    nRowView.Cells[col].Value = Total[j].ToString("n1");
                    nRowView.Cells["ToTal"].Value = _TOTAL.ToString("n1");
                    col++;
                }
                GV.Visible = true;
            }
            else
            {
                GV.Visible = false;
            }
        }

        //Event
        private void LVData_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
                {
                    Utils.TNMessageBox("Thông báo", "Bạn chỉ được chọn trong 1 tháng", 2);
                    return;
                }
                _TeamKey = LVData.SelectedItems[0].Tag.ToInt();
                GVEmployee.Rows.Clear();
                try
                {
                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
                catch (Exception ex)
                {
                    Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
                }
            }
        }
        private void DisplayData()
        {
            DataTable zTable = ReportAll.No2_V2(dte_FromDate.Value, dte_ToDate.Value, _TeamKey);
            
            if (zTable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(GVEmployee, zTable);
                    InitGV_Data(GVEmployee, zTable);
                    InitGV_Paint(GVEmployee, zTable);
                }));
            }

        }

        private void GridView_Click(object sender, EventArgs e)
        {
            GVEmployee.Refresh();
        }
        private void GridView_Paint(object sender, PaintEventArgs e)
        {
            Image image = imageList1.Images[0];
            Bitmap drawing = null;
            Graphics g;
            Color nColor = Color.FromArgb(101, 147, 207);
            Brush nBrush = new SolidBrush(Color.Navy);
            Font nFont = new Font("Tahoma", 9, FontStyle.Bold);
            Pen nLinePen = new Pen(nColor, 1);
            drawing = new Bitmap(GVEmployee.Width, GVEmployee.Height, e.Graphics);

            g = Graphics.FromImage(drawing);
            g.SmoothingMode = SmoothingMode.HighQuality;

            for (int i = 0; i <= _TotalCol; i++)
            {
                Rectangle r1 = GVEmployee.GetCellDisplayRectangle(i, -1, true);

                r1.X += 1;
                r1.Y += 1;

                if (i > 2 && i < _TotalCol)
                {
                    // Chia
                    r1.Height = _HeaderHeight / 3 - 2;
                    r1.Width = r1.Width - 2;
                    r1.Y = _HeaderHeight - r1.Height;

                    g.DrawImage(image, r1);
                }
                else
                {
                    // Trộn
                    r1.Width = r1.Width - 2;
                    r1.Height = r1.Height - 2;

                    g.DrawImage(image, r1);
                }
                //e.Graphics.FillRectangle(new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.BackColor), r1);
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                g.DrawString(TitleColumn[i], nFont, nBrush, r1, format);
                //e.Graphics.DrawString(TitleColumn[i],
                //  new Font("Tahoma", 9, FontStyle.Bold),
                //   new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.ForeColor),
                //   r1,
                //   format);
            }

            int k = 0;
            for (int j = 3; j < _TotalCol; j += 2)
            {
                Rectangle r1 = GVEmployee.GetCellDisplayRectangle(j, -1, true);
                int w2 = GVEmployee.GetCellDisplayRectangle(j + 1, -1, true).Width;
                r1.X += 1;
                r1.Y += 1;
                r1.Width = r1.Width + w2 - 2;
                r1.Height = r1.Height - _HeaderHeight / 3 - 2;
                g.DrawImage(image, r1);

                //e.Graphics.FillRectangle(new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.BackColor), r1);
                StringFormat format = new StringFormat();
                format.Alignment = StringAlignment.Center;
                format.LineAlignment = StringAlignment.Center;
                g.DrawString(TitleMerge[k], nFont, nBrush, r1, format);

                //e.Graphics.DrawString(TitleMerge[k],
                //   new Font("Tahoma", 9, FontStyle.Bold),
                //    new SolidBrush(GVData3.ColumnHeadersDefaultCellStyle.ForeColor),
                //    r1,
                //    format);

                k++;
            }

            e.Graphics.DrawImageUnscaled(drawing, 0, 0);
            nBrush.Dispose();
            g.Dispose();
        }
        private void GridView_Scroll(object sender, ScrollEventArgs e)
        {
            Rectangle rtHeader = GVEmployee.DisplayRectangle;
            rtHeader.Height = _HeaderHeight;
            GVEmployee.Invalidate(rtHeader);
            if (e.Type == ScrollEventType.EndScroll)
            {
                GVEmployee.Refresh();
            }
        }
        private void GridView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            Rectangle rtHeader = GVEmployee.DisplayRectangle;
            rtHeader.Height = GVEmployee.ColumnHeadersHeight / 2;
            GVEmployee.Invalidate(rtHeader);
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            if (LVData.SelectedItems.Count > 0)
            {
                if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
                {
                    Utils.TNMessageBox("Thông báo", "Bạn chỉ được chọn trong 1 tháng", 2);
                    return;
                }
                _TeamKey = LVData.SelectedItems[0].Tag.ToInt();
                GVEmployee.Rows.Clear();
                try
                {
                    using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
                }
                catch (Exception ex)
                {
                    Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
                }
            }
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            FolderBrowserDialog FDialog = new FolderBrowserDialog();
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                Path = FDialog.SelectedPath + @"\NangSuat_LuongKhoan" + dte_ToDate.Value.ToString("MM-yyyy") + ".xlsx";

                DataTable dt = new DataTable();
                foreach (DataGridViewColumn col in GVEmployee.Columns)
                {
                    dt.Columns.Add(col.HeaderText);

                }
                DataRow dRow = dt.NewRow();
                dt.Rows.Add(dRow);
                foreach (DataGridViewRow row in GVEmployee.Rows)
                {
                    dRow = dt.NewRow();
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        dRow[cell.ColumnIndex] = cell.Value;
                    }
                    dt.Rows.Add(dRow);
                }
                string Message = ExportTableToExcel(dt, Path);
                if (Message != "OK")
                    MessageBox.Show(Message);
                else
                {
                    Message = "Đã tạo tập tin thành công !." + Environment.NewLine + "Bạn có muốn mở thư mục chứa ?.";
                    if (MessageBox.Show(Message, "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Process.Start(Path);
                    }
                }
            }
        }
        private string ExportTableToExcel(DataTable zTable, string Folder)
        {
            try
            {
                var newFile = new FileInfo(Folder);
                if (newFile.Exists)
                {
                    newFile.Delete();
                }
                using (var package = new ExcelPackage(newFile))
                {
                    //Tạo Sheet 1 mới với tên Sheet là DonHang
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("PhanBo");
                    worksheet.Cells["A1"].LoadFromDataTable(zTable, true);
                    worksheet.Cells.Style.Font.Name = "Times New Roman";
                    worksheet.Cells.Style.Font.Size = 12;
                    worksheet.Cells.AutoFilter = true;
                    worksheet.Cells.AutoFitColumns();
                    worksheet.View.FreezePanes(3, 4);

                    //Rowheader và row tổng
                    worksheet.Row(1).Height = 60;
                    worksheet.Row(1).Style.WrapText = true;
                    worksheet.Row(1).Style.Font.Bold = true;
                    worksheet.Row(1).Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Row(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Row(zTable.Rows.Count + 1).Style.Font.Bold = true;//dòng tổng
                    //Chỉnh fix độ rộng cột
                    for (int j = 4; j <= zTable.Columns.Count; j++)
                    {
                        worksheet.Column(j).Width = 15;
                    }
                    worksheet.Column(1).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Column(1).Width = 5;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 15;
                    worksheet.Column(3).Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Column(zTable.Columns.Count).Style.Font.Bold = true;//côt tổng
                    //Canh phải số
                    for (int i = 2; i <= zTable.Rows.Count + 1; i++)
                    {
                        for (int j = 4; j <= zTable.Columns.Count; j++)
                        {
                            worksheet.Cells[i, j].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        }
                    }
                    //Merge header
                    //DataTable zTablStage = Salary_Report_Productivity.StageName(dte_FromDate.Value, dte_ToDate.Value, _TeamKey); //Danh sách tên công đoạn
                    worksheet.Cells[1, 1].Value = "STT";
                    worksheet.Cells[1, 2].Value = "Tên nhân viên";
                    worksheet.Cells[1, 3].Value = " Số thẻ ";
                    worksheet.Cells[1, 1, 2, 1].Merge = true;
                    worksheet.Cells[1, 2, 2, 2].Merge = true;
                    worksheet.Cells[1, 3, 2, 3].Merge = true;
                    int k = 0;
                    for (int i = 4; i < TitleColumn.Length; i = i + 2)
                    {
                        worksheet.Cells[1, i, 1, i + 1].Merge = true;
                        worksheet.Cells[1, i, 1, i + 1].Value = TitleMerge[k]; //zTablStage.Rows[k]["StageName"].ToString();
                        worksheet.Cells[2, i].Value = "Số lượng";
                        worksheet.Cells[2, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[2, i + 1].Value = "Lương khoán";
                        worksheet.Cells[2, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        k++;
                    }

                    //for (int i = 4; i < zTable.Columns.Count; i = i + 2)
                    //{
                    //    worksheet.Cells[1, i, 1, i + 1].Merge = true;
                    //    worksheet.Cells[1, i, 1, i + 1].Value = zTablStage.Rows[k]["StageName"].ToString();
                    //    worksheet.Cells[2, i].Value = "Số lượng";
                    //    worksheet.Cells[2, i].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    worksheet.Cells[2, i + 1].Value = "Lương khoán";
                    //    worksheet.Cells[2, i + 1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //    k++;
                    //}
                    worksheet.Cells[1, zTable.Columns.Count].Value = " Tổng cộng ";
                    worksheet.Cells[1, zTable.Columns.Count, 2, zTable.Columns.Count].Merge = true;                   //Lưu file Excel
                    package.SaveAs(newFile);
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
            return "OK";
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}