﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.LOC
{
    public class Ward_Info
    {
        #region [ Field Name ]
        private int _WardKey = 0;
        private string _WardName = "";
        private string _WardID = "";
        private int _DistrictKey = 0;
        private int _ProvinceKey = 0;
        private int _CountryKey = 0;
        private string _Ext = "";
        private string _Latlong = "";
        private int _RecordStatus = 0;
        private int _Rank = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private int _ID_Ward = 0;
        private int _ID_Dis = 0;
        private string _RoleID = "";
        private string _Message = "";
        #endregion
        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _WardKey; }
            set { _WardKey = value; }
        }
        public string WardName
        {
            get { return _WardName; }
            set { _WardName = value; }
        }
        public string WardID
        {
            get { return _WardID; }
            set { _WardID = value; }
        }
        public int DistrictKey
        {
            get { return _DistrictKey; }
            set { _DistrictKey = value; }
        }
        public int ProvinceKey
        {
            get { return _ProvinceKey; }
            set { _ProvinceKey = value; }
        }
        public int CountryKey
        {
            get { return _CountryKey; }
            set { _CountryKey = value; }
        }
        public string Ext
        {
            get { return _Ext; }
            set { _Ext = value; }
        }
        public string Latlong
        {
            get { return _Latlong; }
            set { _Latlong = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public int ID_Ward
        {
            get { return _ID_Ward; }
            set { _ID_Ward = value; }
        }
        public int ID_Dis
        {
            get { return _ID_Dis; }
            set { _ID_Dis = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Ward_Info()
        {
        }
        public Ward_Info(int WardKey)
        {
            string zSQL = "SELECT * FROM LOC_Ward WHERE WardKey = @WardKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = WardKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["WardKey"] != DBNull.Value)
                        _WardKey = int.Parse(zReader["WardKey"].ToString());
                    _WardName = zReader["WardName"].ToString().Trim();
                    _WardID = zReader["WardID"].ToString().Trim();
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    if (zReader["CountryKey"] != DBNull.Value)
                        _CountryKey = int.Parse(zReader["CountryKey"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    _Latlong = zReader["Latlong"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ID_Ward"] != DBNull.Value)
                        _ID_Ward = int.Parse(zReader["ID_Ward"].ToString());
                    if (zReader["ID_Dis"] != DBNull.Value)
                        _ID_Dis = int.Parse(zReader["ID_Dis"].ToString());
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }

        public void Get_Ward_Info(string WardName,int DistrictKey)
        {
            string zSQL = "SELECT * FROM LOC_Ward WHERE WardName = @WardName AND DistrictKey =@DistrictKey  AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = WardName;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = DistrictKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["WardKey"] != DBNull.Value)
                        _WardKey = int.Parse(zReader["WardKey"].ToString());
                    _WardName = zReader["WardName"].ToString().Trim();
                    _WardID = zReader["WardID"].ToString().Trim();
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    if (zReader["CountryKey"] != DBNull.Value)
                        _CountryKey = int.Parse(zReader["CountryKey"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    _Latlong = zReader["Latlong"].ToString().Trim();
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    if (zReader["ID_Ward"] != DBNull.Value)
                        _ID_Ward = int.Parse(zReader["ID_Ward"].ToString());
                    if (zReader["ID_Dis"] != DBNull.Value)
                        _ID_Dis = int.Parse(zReader["ID_Dis"].ToString());
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO LOC_Ward ("
        + " WardName ,WardID ,DistrictKey ,ProvinceKey ,CountryKey ,Ext ,Latlong ,Rank ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ,ID_Ward ,ID_Dis ) "
         + " VALUES ( "
         + "@WardName ,@WardID ,@DistrictKey ,@ProvinceKey ,@CountryKey ,@Ext ,@Latlong ,@Rank ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ,@ID_Ward ,@ID_Dis ) ";
            zSQL += " SELECT WardKey FROM LOC_Ward WHERE WardKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = _WardName.Trim();
                zCommand.Parameters.Add("@WardID", SqlDbType.NVarChar).Value = _WardID.Trim();
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Latlong", SqlDbType.NVarChar).Value = _Latlong.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@ID_Ward", SqlDbType.Int).Value = _ID_Ward;
                zCommand.Parameters.Add("@ID_Dis", SqlDbType.Int).Value = _ID_Dis;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE LOC_Ward SET "
                        + " WardName = @WardName,"
                        + " WardID = @WardID,"
                        + " DistrictKey = @DistrictKey,"
                        + " ProvinceKey = @ProvinceKey,"
                        + " CountryKey = @CountryKey,"
                        + " Ext = @Ext,"
                        + " Latlong = @Latlong,"
                        + " Rank = @Rank,"
                        + " CreatedOn = @CreatedOn,"
                        + " CreatedBy = @CreatedBy,"
                        + " CreatedName = @CreatedName,"
                        + " ModifiedOn = @ModifiedOn,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName,"
                        + " ID_Ward = @ID_Ward,"
                        + " ID_Dis = @ID_Dis"
                       + " WHERE WardKey = @WardKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@WardName", SqlDbType.NVarChar).Value = _WardName.Trim();
                zCommand.Parameters.Add("@WardID", SqlDbType.NVarChar).Value = _WardID.Trim();
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Latlong", SqlDbType.NVarChar).Value = _Latlong.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@ID_Ward", SqlDbType.Int).Value = _ID_Ward;
                zCommand.Parameters.Add("@ID_Dis", SqlDbType.Int).Value = _ID_Dis;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOC_Ward SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE WardKey = @WardKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_WardKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
