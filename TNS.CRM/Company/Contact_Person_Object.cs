﻿using System.Data;
using TNS.SYS;

namespace TNS.CRM
{
    public class Contact_Person_Object : Contact_Info
    {      
        private Personal_Info _Person;
        public Personal_Info Person
        {
            get
            {
                return _Person;
            }

            set
            {
                _Person = value;
            }
        }

        public Contact_Person_Object() : base()
        {
            _Person = new Personal_Info();
        }

        public Contact_Person_Object(DataRow Row) : base(Row)
        {
            _Person = new Personal_Info(Row);
        }

        public Contact_Person_Object(string ContactKey) : base(ContactKey)
        {
            _Person = new Personal_Info(ContactKey);
        }

        public void Save_Object()
        {
            base.Create();
            _Person.FullName = base.FullName;
            _Person.ParentKey = base.Key;
            _Person.ParentTable = "CRM_Company_Contact";
            _Person.Phone = base.CompanyPhone;
            _Person.Email = base.CompanyEmail;
            _Person.CreatedBy = base.CreatedBy;
            _Person.ModifiedName = base.ModifiedName;
            _Person.ModifiedBy = base.ModifiedBy;
            _Person.ModifiedName = base.ModifiedName;
            _Person.Create();
        }
        public void Update_Object()
        {
            base.Update();
            _Person.FullName = base.FullName;
            _Person.ParentKey = base.Key;
            _Person.ParentTable = "CRM_Company_Contact";
            _Person.Phone = base.CompanyPhone;
            _Person.Email = base.CompanyEmail;
            _Person.CreatedBy = base.CreatedBy;
            _Person.ModifiedName = base.ModifiedName;
            _Person.ModifiedBy = base.ModifiedBy;
            _Person.ModifiedName = base.ModifiedName;
            _Person.Update();
        }

        public void Delete_Object()
        {
            base.Delete();
            _Person.ModifiedBy = base.ModifiedBy;
            _Person.ModifiedName = base.ModifiedName;
            _Person.ParentKey = base.Key;
            _Person.Delete();                
        }
        public void Delete_Detail_Object()
        {
            base.DeleteDetail();
            _Person.ModifiedBy = base.ModifiedBy;
            _Person.ModifiedName = base.ModifiedName;
            _Person.ParentKey = base.Key;
            _Person.Delete();
        }
    }
}
