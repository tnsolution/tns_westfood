﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TN.SLR
{
    public class Salary_Business_Data
    {
        //tính thưởng kinh doanh-- nhóm mặc đinh
        public static DataTable TinhThuongKinhDoanh_NhomMacDinh(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, double PhanTram, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
                SoThang++;
            }
            PivotDate += "[Tổng cộng],[Bình quân thu nhập],[Số tiền]";
            //if (PivotDate.Contains(","))
            //{
            //    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            //}

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-08-31 23:59:59'
--declare @SoThang int =12
--declare @Phantram float=0.35
--declare @DateOff datetime ='2020-08-10 23:59:59'

CREATE TABLE RPT
(	
	Months NVARCHAR(50),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng từ tháng 1--12
		--công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
--//Tổng cộng
		--công nhân
		INSERT INTO RPT
		SELECT N'Tổng cộng', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--văn phòng
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
--//Tổng bình quân thu nhập
		--công nhân
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--văn phòng
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
--//Tiền ứng
		--công nhân
		INSERT INTO RPT
		SELECT N'Số tiền', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang*@Phantram,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey

;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Months] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@PhanTram", SqlDbType.Decimal).Value = PhanTram;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        //tính thưởng kinh doanh-- nhóm phân bổ
        public static DataTable TinhThuongKinhDoanh_NhomPhanBo(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey, double PhanTram, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
                SoThang++;
            }
            PivotDate += "[Tổng cộng],[Bình quân thu nhập],[Số tiền]";
            //if (PivotDate.Contains(","))
            //{
            //    PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            //}

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-08-31 23:59:59'
--declare @SoThang int =12
--declare @Phantram float=0.35
--declare @DateOff datetime ='2020-08-10 23:59:59'

CREATE TABLE RPT
(	
	Months NVARCHAR(50),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng từ tháng 1--12
		--công nhân
		INSERT INTO RPT
		SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--văn phòng
		INSERT INTO RPT
		SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tổng cộng
		--công nhân
		INSERT INTO RPT
		SELECT N'Tổng cộng', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--văn phòng
		INSERT INTO RPT
		SELECT N'Tổng cộng',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount),0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tổng bình quân thu nhập
		--công nhân
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--văn phòng
		INSERT INTO RPT
		SELECT N'Bình quân thu nhập',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount)/@SoThang,0) AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tiền ứng
		--công nhân
		INSERT INTO RPT
		SELECT N'Số tiền', EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang*@Phantram,0) AS Amount
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--hỗ trợ
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
		--Văn phòng
		INSERT INTO RPT
		SELECT N'Số tiền',EmployeeKey,EmployeeID,EmployeeName,
		[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)

;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Months] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT";

            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.Parameters.Add("@PhanTram", SqlDbType.Decimal).Value = PhanTram;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }


        //Save  thường kinh doanh
        public static string LuuThuongKinhDoanh(DateTime FromDate, DateTime ToDate, double PhanTram, int SoLan, DateTime DateOff)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            DateTime zDateOff = new DateTime(DateOff.Year, DateOff.Month, DateOff.Day, 23, 59, 59);
            int SoThang = 0;
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                SoThang++;
            }
            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-10-31 23:59:59'
--declare @SoThang int =0
--declare @Phantram float=0
--declare @SoLan int =0
--declare @DateOff datetime ='2020-08-10 23:59:59'

declare @StringTB nvarchar(500)= CONCAT(N'BÌNH QUÂN THU NHẬP TÍNH THƯỞNG SXKD ĐỢT ',@SoLan,N' (Chốt tháng ',MONTH(@ToDate),'/',YEAR(@ToDate),')')
declare @StringTU nvarchar(500)= CONCAT(N'THÀNH TIỀN THƯỞNG SXKD ĐỢT ',@SoLan,N' (Mức chi: ',REPLACE(Round(@Phantram*100,0), '.00', ''),N'% - Ngày chi: ',DAY(@DateOff),'/',MONTH(@DateOff),'/',YEAR(@DateOff),')')
declare @Parent nvarchar(50) = newid()
--Bảng cha
INSERT INTO [dbo].[SLR_SalaryBusiness] (AutoKey,FromDate,ToDate,Persent,Type,Description,Class,DateOff)
VALUES (@Parent,@FromDate,@ToDate,@Phantram,@SoLan,CONCAT(N'SỐ TIỀN THƯỞNG SXKD ĐỢT ',@SoLan),N'TU',@DateOff)

--Lọc dữ liệu vào bảng tạm
CREATE TABLE RPT
(	
	ID NVARCHAR(50),
	Name NVARCHAR(550),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	TeamID NVARCHAR(50),
	TeamName NVARCHAR(MAX),
	Amount FLOAT,
	SoLan Int,
	Class CHAR(2)
);

--//Tổng bình quân thu nhập
		--công nhân
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan), @StringTB, EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TB'
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--hỗ trợ
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan),@StringTB,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TB'
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--văn phòng
		INSERT INTO RPT
		SELECT CONCAT('TB',@SoLan),@StringTB,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang,0) AS Amount,@SoLan,'TB'
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
--//Tiền ứng
		--công nhân
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU, EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang*@Phantram,0) AS Amount,@SoLan,'TU'
		FROM [dbo].[SLR_ReportWorker_Close]
		WHERE CodeID ='TCSX' 
		AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--hỗ trợ
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount,@SoLan,'TU'
		FROM [dbo].[SLR_ReportSupport_Close]
		WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName
		--văn phòng
		INSERT INTO RPT
		SELECT CONCAT('TU',@SoLan),@StringTU,EmployeeKey,EmployeeID,EmployeeName,
		TeamKey,TeamID,TeamName,
		Round(SUM(Amount)/@SoThang *@Phantram,0)  AS Amount,@SoLan,'TU'
		FROM [dbo].[SLR_ReportOffice_Close]
		WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
		AND [dbo].[SLR_KiemTraNgayNghiViec_Luong13_ThuongSXKD](EmployeeKey,@DateOff)=1
		GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName

--Lưu từ bảng tạm vào bảng con
		INSERT INTO [dbo].[SLR_SalaryBusiness_Detail](AutoKey,Parent,ID,Name,DateWrite,
		EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,Amount,Type,Class)
		SELECT newid(),@Parent,ID,Name,@ToDate,
		EmployeeKey,EmployeeID,EmployeeName,TeamKey,TeamID,TeamName,Amount,SoLan,Class
		FROM RPT

DROP Table RPT

";

            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@PhanTram", SqlDbType.Decimal).Value = PhanTram;
                zCommand.Parameters.Add("@SoThang", SqlDbType.Int).Value = SoThang;
                zCommand.Parameters.Add("@SoLan", SqlDbType.Int).Value = SoLan;
                zCommand.Parameters.Add("@DateOff", SqlDbType.DateTime).Value = zDateOff;
                zCommand.CommandTimeout = 350;
                string a = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                zResult = "08" + ex.ToString();
            }
            return zResult;
        }
      
        
        
        
        // báo cáo thưởng kinh doanh - nhóm mặc định
        public static DataTable BaoCaoThuongKinhDoanh_NhomMacDinh(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
            }
            PivotDate += "[TỔNG CỘNG],";
            DataTable ztb = Salary_Business_Data.GetName(zFromDate, zToDate);
            if (ztb.Rows.Count > 0)
            {
                for (int i = 0; i < ztb.Rows.Count; i++)
                {
                    PivotDate += "[" + ztb.Rows[i]["Name"] + "],";
                }
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @SoThang int =12

CREATE TABLE RPT
(	
	[Name] NVARCHAR(500),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng
        --công nhân
        INSERT INTO RPT
        SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --hỗ trợ
        INSERT INTO RPT
        SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --văn phòng
        INSERT INTO RPT
        SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
--//Tổng cộng fix 13
        --công nhân
        INSERT INTO RPT
        SELECT N'TỔNG CỘNG', EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --hỗ trợ
        INSERT INTO RPT
        SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --văn phòng
        INSERT INTO RPT
        SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
--Kết quả chi thưởng
        INSERT INTO RPT
        SELECT [Name],EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        SUM(Amount)
        FROM [dbo].[SLR_SalaryBusiness_Detail]
        WHERE DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY [Name], EmployeeKey,EmployeeID,EmployeeName,TeamKey

;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Name] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT
";
            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        // báo cáo thưởng kinh doanh - nhóm phân bổ chi phí
        public static DataTable BaoCaoThuongKinhDoanh_NhomPhanBo(DateTime FromDate, DateTime ToDate, int BranchKey, int DepartmentKey, int TeamKey)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            string PivotDate = "";
            for (int i = zFromDate.Month; i <= zToDate.Month; i++)
            {
                PivotDate += "[" + i + "],";
            }
            PivotDate += "[TỔNG CỘNG],";
            DataTable ztb = Salary_Business_Data.GetName(zFromDate, zToDate);
            if (ztb.Rows.Count > 0)
            {
                for (int i = 0; i < ztb.Rows.Count; i++)
                {
                    PivotDate += "[" + ztb.Rows[i]["Name"] + "],";
                }
            }
            if (PivotDate.Contains(","))
            {
                PivotDate = PivotDate.Remove(PivotDate.LastIndexOf(","));
            }

            string Fillter = "";

            if (BranchKey > 0)
            {
                Fillter += " AND [dbo].[Fn_GetBranchKey_FromTeamKey](A.TeamKey) = @BranchKey";
            }
            if (DepartmentKey > 0)
                Fillter += " AND [dbo].[Fn_GetDepartmentKey_FromTeamKey](A.TeamKey) =@DepartmentKey ";
            if (TeamKey > 0)
                Fillter += " AND A.TeamKey=@TeamKey ";

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'
--declare @SoThang int =12

CREATE TABLE RPT
(	
	[Name] NVARCHAR(500),
	EmployeeKey NVARCHAR(50),
	EmployeeID NVARCHAR(100),
	EmployeeName NVARCHAR(MAX),
	TeamKey int,
	Amount FLOAT
);
--Tổng từng tháng
        --công nhân
        INSERT INTO RPT
        SELECT MONTH(DateWrite), EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --hỗ trợ
        INSERT INTO RPT
        SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --văn phòng
        INSERT INTO RPT
        SELECT MONTH(DateWrite),EmployeeKey,EmployeeID,EmployeeName,
        [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY DateWrite,EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--//Tổng cộng fix 13
        --công nhân
        INSERT INTO RPT
        SELECT N'TỔNG CỘNG', EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportWorker_Close]
        WHERE CodeID ='TCSX' 
        AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --hỗ trợ
        INSERT INTO RPT
        SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
        TeamKey,
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportSupport_Close]
        WHERE CodeID ='TCSX' AND  DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY EmployeeKey,EmployeeID,EmployeeName,TeamKey
        --văn phòng
        INSERT INTO RPT
        SELECT  N'TỔNG CỘNG',EmployeeKey,EmployeeID,EmployeeName,
        [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate),
        Round(SUM(Amount),0) AS Amount
        FROM [dbo].[SLR_ReportOffice_Close]
        WHERE CodeID ='TCSX'AND DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY EmployeeKey,EmployeeID,EmployeeName,[dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
--Kết quả chi thưởng
        INSERT INTO RPT
        SELECT [Name],EmployeeKey,EmployeeID,EmployeeName,
        CASE WHEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate) !=0 THEN [dbo].[Fn_GetTeamKeyViewOffice](EmployeeKey,@ToDate)
		ELSE TeamKey
        END TeamKey,
        SUM(Amount)
        FROM [dbo].[SLR_SalaryBusiness_Detail]
        WHERE DateWrite BETWEEN CONVERT(VARCHAR,@FromDate,120) AND CONVERT(VARCHAR,@ToDate,120)
        GROUP BY [Name], EmployeeKey,EmployeeID,EmployeeName,TeamKey
 ;WITH  N AS  (
		SELECT * FROM(
			SELECT *,[dbo].[Fn_GetTeamID](TeamKey) AS TeamID,[dbo].[Fn_GetTeamName](TeamKey) AS TeamName 
			FROM RPT )
			X PIVOT(
			SUM(Amount)
			FOR [Name] IN ( @Pivot)
			) P  
	)		
	SELECT A.* FROM N A 
	LEFT JOIN [dbo].[SYS_Team] B ON B.TeamKey=A.TeamKey
	LEFT JOIN [dbo].[SYS_Department] C ON C.DepartmentKey=B.DepartmentKey
	LEFT JOIN [dbo].[SYS_Branch] D ON D.BranchKey=C.BranchKey
    WHERE 1=1 @Parameter
	ORDER BY D.Rank,C.Rank,B.Rank,LEN(EmployeeID),EmployeeID 

DROP TABLE RPT";

            zSQL = zSQL.Replace("@Parameter", Fillter);
            zSQL = zSQL.Replace("@Pivot", PivotDate);
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@BranchKey", SqlDbType.Int).Value = BranchKey;
                zCommand.Parameters.Add("@DepartmentKey", SqlDbType.Int).Value = DepartmentKey;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        // báo cáo thưởng kinh doanh
        public static DataTable GetName(DateTime FromDate, DateTime ToDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, 1, 0, 0, 0);
            DateTime zToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);

            DataTable zTable = new DataTable();
            string zSQL = @"
--declare @FromDate datetime ='2020-01-01 00:00:00'
--declare @ToDate datetime ='2020-12-31 23:59:59'

SELECT Name,ID,Type FROM [dbo].[SLR_SalaryBusiness_Detail]
WHERE RecordStatus <>99
AND DateWrite BETWEEN @FromDate AND @ToDate
GROUP BY Name,ID,Type
ORDER BY Type,ID";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.CommandTimeout = 350;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
    }
}
