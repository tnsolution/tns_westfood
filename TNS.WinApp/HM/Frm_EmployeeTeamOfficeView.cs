﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.HRM;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_EmployeeTeamOfficeView : Form
    {
        DataTable ztbType = new DataTable();
        private int _DepartmentKey = 0;
        private int _TeamKey = 0;
        private string _Search = "";
        private int _Type = 0;
        public Frm_EmployeeTeamOfficeView()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;

            btn_Search.Click += Btn_Search_Click;
            btn_Export.Click += Btn_Export_Click;
            btn_New.Click += Btn_New_Click;
            cbo_Department.SelectedIndexChanged += Cbo_Department_SelectedIndexChanged;
            GVData.DoubleClick += GVData_DoubleClick;
        }

        private void Frm_EmployeeTeamOfficeView_Load(object sender, EventArgs e)
        {
            LoadTableType();
            LoadDataToToolbox.KryptonComboBox(cbo_Department, "SELECT DepartmentKey, DepartmentName FROM[dbo].[SYS_Department] WHERE DepartmentKey != 98  AND RecordStatus< 99", "---- Chọn tất cả----");
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98  AND RecordStatus < 99", "---- Chọn tất cả ----");
            LoadDataToToolbox.ComboBoxData(cbo_Type.ComboBox, ztbType, false, 0, 1);
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }
        private void Cbo_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDataToToolbox.KryptonComboBox(cbo_Team, "SELECT TeamKey,TeamName FROM SYS_Team WHERE TeamKey != 98 AND DepartmentKey = " + cbo_Department.SelectedValue.ToInt() + "  AND RecordStatus < 99", "---- Chọn tất cả ----");
        }
        void LoadTableType()
        {
            ztbType = new DataTable();
            ztbType.Columns.Add("Value");
            ztbType.Columns.Add("Name");
            ztbType.Rows.Add("0", "--Tất cả--");
            ztbType.Rows.Add("1", "Đang áp dụng");
            ztbType.Rows.Add("2", "Đã hết thời hạn");
        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            _DepartmentKey = cbo_Department.SelectedValue.ToInt();
            _TeamKey = cbo_Team.SelectedValue.ToInt();
            _Search = txt_Search.Text.Trim();
            _Type = cbo_Type.SelectedValue.ToInt();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }

            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private void GVData_DoubleClick(object sender, EventArgs e)
        {
            if (GVData.Rows[GVData.RowSel][4].ToString() != "")
            {
                Frm_EmployeeTeamOfficeDetail frm = new Frm_EmployeeTeamOfficeDetail();
                frm.Key = GVData.Rows[GVData.RowSel][4].ToString().ToInt(); ;
                frm.Show();
            }
        }
        private void Btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Hiển_Thị_Tổ_Nhóm_Nhân_Sự.xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }
                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }
        private void Btn_New_Click(object sender, EventArgs e)
        {
            Frm_EmployeeTeamOfficeDetail frm = new Frm_EmployeeTeamOfficeDetail();
            frm.Key = 0;
            frm.Show();
        }

        private int _STT = 1;
        private void DisplayData()
        {
            DataTable _Intable = Employee_TeamOfficeView_Data.List(_DepartmentKey, _TeamKey, _Search);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(_Intable);
                    _STT = 1;
                    int NoGroup = 2;
                    int RowTam = NoGroup;
                    Row zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[1];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            _STT = 1;
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, _STT);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, _STT);
                        RowTam = i + NoGroup;
                        _STT++;
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[RowTam + 1];
                    TotalRow(zGvRow, _Intable, RowTam + 1);

                }));
            }
        }

        void InitGV_Layout(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            //int TotalRow = TableView.Rows.Count + 1;
            int ToTalCol = 7;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(1);

            //Row Header
            GVData.Rows[0][0] = "Stt";
            GVData.Rows[0][1] = "Họ và tên";
            GVData.Rows[0][2] = "Số thẻ";
            GVData.Rows[0][3] = "Ghi chú";
            GVData.Rows[0][4] = ""; // ẩn cột này

            //for (int rIndex = 0; rIndex < TableView.Rows.Count; rIndex++)
            //{
            //    DataRow rData = TableView.Rows[rIndex];

            //    GVData.Rows[rIndex + 1][0] = rIndex + 1;
            //    GVData.Rows[rIndex + 1][1] = rData[1];
            //    GVData.Rows[rIndex + 1][2] = rData[2];
            //    GVData.Rows[rIndex + 1][3] = rData[3];
            //    GVData.Rows[rIndex + 1][4] = rData[4].Ton0String();
            //    GVData.Rows[rIndex + 1][5] = rData[5];
            //    GVData.Rows[rIndex + 1][6] = rData[6];
            //    GVData.Rows[rIndex + 1][7] = rData[7];
            //    GVData.Rows[rIndex + 1][8] = rData[0];
            //}

            //Style         
            GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.LeftCenter;
            GVData.Styles.Normal.WordWrap = true;

            //Freeze Row and Column                              
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;

            GVData.Rows[0].TextAlign = TextAlignEnum.CenterCenter;

            GVData.Rows[0].Height = 40;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 200;
            GVData.Cols[2].Width = 120;
            GVData.Cols[3].Width = 120;
            GVData.Cols[4].Visible = false;
        }
        private void HeaderRow(Row RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView[0] = "";
            RowView[1] = TeamName;
            RowView[2] = "Số nhân sự " + Table.Select("TeamKey=" + TeamKey).Length;
            RowView[3] = "";
            RowView[4] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
        }
        private void DetailRow(Row RowView, DataRow rDetail, int No)
        {
            RowView[0] = (No).ToString();
            RowView[1] = rDetail["EmployeeName"].ToString().Trim();
            RowView[2] = rDetail["EmployeeID"].ToString().Trim();
            RowView[3] = rDetail["Description"].ToString();
            
            RowView[4] = rDetail[0];//autokey
        }
        private void TotalRow(Row RowView, DataTable Table, int No)
        {
            RowView[0] = "";
            RowView[1] = "TỔNG CỘNG ";
            RowView[2] = "Số nhân sự " + Table.Rows.Count;
            RowView[3] = "";
            RowView[4] = "";
            RowView.StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);

        }




        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.StartPosition = FormStartPosition.CenterScreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

    }
}
