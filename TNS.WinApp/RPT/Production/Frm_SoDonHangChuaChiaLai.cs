﻿using C1.Win.C1FlexGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.CORE;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_SoDonHangChuaChiaLai : Form
    {
        string _rdoText = "";
        public Frm_SoDonHangChuaChiaLai()
        {
            InitializeComponent();
            btn_Search.Click += Btn_Search_Click;
            btnClose.Click += btnClose_Click;
            btnMax.Click += btnMax_Click;
            btnMini.Click += btnMini_Click;
            btn_Export.Click += btn_Export_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            DateTime ViewDate = SessionUser.Date_Work;
            DateTime FromDate = new DateTime(ViewDate.Year, ViewDate.Month, 1, 0, 0, 0);
            DateTime ToDate = FromDate.AddMonths(1).AddDays(-1);
            ToDate = new DateTime(ToDate.Year, ToDate.Month, ToDate.Day, 23, 59, 59);
            dte_FromDate.Value = FromDate;
            dte_ToDate.Value = ToDate;
        }

        private void Frm_SoDonHangChuaChiaLai_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);
            Check_RoleForm();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

        }
        private void Btn_Search_Click(object sender, EventArgs e)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();
            if (dte_ToDate.Value.Month != dte_FromDate.Value.Month || dte_ToDate.Value.Year != dte_FromDate.Value.Year)
            {
                Utils.TNMessageBoxOK("Bạn chỉ được chọn trong 1 tháng", 1);
                return;
            }

            #region[Log]

            if (rdo_One.Checked)
                _rdoText = rdo_One.Text;
            else
                _rdoText = rdo_Two.Text;
            string Status = "Tìm DL form " + HeaderControl.Text + " > từ: " + dte_FromDate.Value.ToString("dd/MM/yyyy") + " > đến:" + dte_ToDate.Value.ToString("dd/MM/yyyy") + " > Tùy chọn:" + _rdoText;
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);
            #endregion

            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBoxOK(ex.ToString(), 4);
            }
        }
        private void DisplayData()
        {
            DataTable zTable  = new DataTable();
            if (rdo_NS.Checked == true)
                zTable = Order_Adjusted_Data.SoLuongDongHangChiaNangSuat(dte_FromDate.Value, dte_ToDate.Value);
            if (rdo_One.Checked == true)
                zTable = Order_Adjusted_Data.SoLuongDongHangChiaLaiConThieu_Lan1(dte_FromDate.Value, dte_ToDate.Value);
            if (rdo_Two.Checked == true)
                zTable = Order_Adjusted_Data.SoLuongDongHangChiaLaiConThieu_Lan2(dte_FromDate.Value, dte_ToDate.Value);

            if (zTable.Rows.Count > 0)
            {
                PivotTable zPvt = new PivotTable(zTable);
                DataTable zTablePivot = zPvt.Generate_ASC("HeaderColumn", "LeftColumn", new string[] { "Amount" }, "CÔNG VIỆC", "TỔNG CỘNG", "TỔNG CỘNG", new string[] { "Sô tiền" }, 0);
                //Tách cột Leftcolumn
                zTablePivot.Columns.Add("TeamKey").SetOrdinal(1);
                zTablePivot.Columns.Add("TeamID").SetOrdinal(2);
                zTablePivot.Columns.Add("TeamName").SetOrdinal(3);

                for (int i = 0; i < zTablePivot.Rows.Count - 1; i++)
                {
                    string[] temp = zTablePivot.Rows[i][0].ToString().Split('|');
                    zTablePivot.Rows[i]["TeamKey"] = temp[0];
                    zTablePivot.Rows[i]["TeamID"] = temp[1];
                    zTablePivot.Rows[i]["TeamName"] = temp[2];
                }
                //Xóa cột đầu đã cắt chuỗi và xóa dòng tổng cuối
                zTablePivot.Columns.Remove("CÔNG VIỆC");
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGVProduct(zTablePivot);
                }));
            }
            else
            {
                GVData.Rows.Count = 0;
                GVData.Cols.Count = 0;
            }
        }
        void InitGVProduct(DataTable TableView)
        {
            GVData.Rows.Count = 0;
            GVData.Cols.Count = 0;
            GVData.Clear();

            int TotalRow = 1;
            int ToTalCol = TableView.Columns.Count;

            GVData.Cols.Add(ToTalCol);
            GVData.Rows.Add(TotalRow);
            //Row Header

            GVData.Rows[0][0] = "STT";
            GVData.Rows[0][1] = "MÃ TỔ NHÓM";
            GVData.Rows[0][2] = "TÊN TỔ NHÓM";
            int ColStart = 3;
            for (int i = 3; i < TableView.Columns.Count; i++)
            {
                DataColumn Col = TableView.Columns[i];
                string[] strCaption = Col.ColumnName.Split('|');
                if (i != TableView.Columns.Count - 1)
                    GVData.Rows[0][ColStart] = "NGÀY " + strCaption[0];
                else
                    GVData.Rows[0][ColStart] = strCaption[0];
                GVData.Cols[ColStart].Width = 100;
                ColStart++;
            }
            int RowStar = 1;
            for (int i = 0; i < TableView.Rows.Count; i++)
            {
                GVData.Rows.Add();
                GVData.Rows[RowStar][0] = i + 1;
                for (int j = 1; j < TableView.Columns.Count; j++)
                {
                    GVData.Rows[RowStar][j] = TableView.Rows[i][j];

                }
                RowStar++;
            }
            GVData.Rows[GVData.Rows.Count - 1][0] = "";
            GVData.Rows[GVData.Rows.Count - 1][1] = "TỔNG CỘNG";
            ////Style
            //GVData.AllowFreezing = AllowFreezingEnum.Both;
            GVData.AllowResizing = AllowResizingEnum.Both;
            GVData.AllowMerging = AllowMergingEnum.FixedOnly;
            GVData.SelectionMode = SelectionModeEnum.Row;
            GVData.VisualStyle = VisualStyle.Office2010Blue;
            GVData.Styles.Normal.Font = new Font("Tahoma", 9, FontStyle.Regular);
            GVData.Styles.Normal.TextAlign = TextAlignEnum.CenterCenter;
            GVData.Styles.Normal.WordWrap = true;



            //Trộn cột 
            //for (int i = 0; i < GVData.Cols.Count; i++)
            //{
            //    GVData.Cols[i].AllowMerging = true;
            //}
            //Trộn dòng 0
            GVData.Rows[0].AllowMerging = true;
            GVData.Rows[0].Height = 50;
            //GVData.Rows[1].Height = 60;
            //Trộn dòng 1
            //Freeze Row and Column
            GVData.Rows.Fixed = 1;
            GVData.Cols.Frozen = 3;

            GVData.Cols[0].Width = 40;
            GVData.Cols[1].Width = 120;
            GVData.Cols[2].Width = 300;
            //Canh phải các cột từ số 1 trở đi
            GVData.Cols[0].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[1].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[2].TextAlign = TextAlignEnum.LeftCenter;
            GVData.Cols[3].TextAlign = TextAlignEnum.LeftCenter;

            GVData.Cols[0].StyleNew.BackColor = Color.Empty;
            GVData.Cols[1].StyleNew.BackColor = Color.Empty;
            GVData.Cols[2].StyleNew.BackColor = Color.Empty;
            GVData.Cols[3].StyleNew.BackColor = Color.Empty;

            for (int i = 3; i < GVData.Cols.Count; i++)
            {
                GVData.Cols[i].TextAlign = TextAlignEnum.RightCenter;
            }

            //In đậm các dòng, cột tổng
            GVData.Rows[GVData.Rows.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            GVData.Cols[GVData.Cols.Count - 1].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 2].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            ////GVData.Cols[ToTalCol - 3].StyleNew.Font = new Font("Tahoma", 9, FontStyle.Bold);
            //GVData.Cols[ToTalCol - 1].StyleNew.BackColor = Color.LightGreen;
        }
        private void btn_Export_Click(object sender, EventArgs e)
        {
            string Path = "";
            SaveFileDialog FDialog = new SaveFileDialog();
            FDialog.Filter = "Excel Files|*.xlsx;*";
            FDialog.FileName = "Báo_Cáo_Số_Lượng_Đơn_Hàng_Chưa_Chia_Lại_Từ_" + dte_FromDate.Value.ToString("dd_MM_yyyy") + "_Đến_" + dte_ToDate.Value.ToString("dd_MM_yyyy") + ".xlsx";
            if (FDialog.ShowDialog() == DialogResult.OK)
            {
                bool checkfile = true;
                Path = FDialog.FileName;
                var newFile = new FileInfo(Path);
                if (newFile.Exists) //nếu có file mới check file được
                {
                    bool zcheck = true;
                    zcheck = Data_Access.CheckFileStatus(newFile);
                    if (zcheck == true)
                    {
                        Utils.TNMessageBoxOK("File đang được sử dụng ở chương trình khác.Vui lòng đóng file!", 2);
                        checkfile = false;
                    }
                    else
                    {
                        checkfile = true;
                    }
                }
                if (checkfile)
                {
                    if (newFile.Exists)
                    {
                        newFile.Delete();
                    }

                    string Status = "Xuất Excel form " + HeaderControl.Text + " > đường dẫn và tên file xuất:" + FDialog.FileName;
                    Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.EmployeeKey, Status, 3);

                    GVData.SaveExcel(Path, FileFlags.IncludeMergedRanges | FileFlags.AsDisplayed | FileFlags.IncludeFixedCells | FileFlags.LoadMergedRanges);
                    Process.Start(Path);
                }

            }

        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
        #region [ Get Auth-Action-Chg_Lang ]
        private void Check_RoleForm()
        {
            string UserKey = SessionUser.UserLogin.Key;
            string RoleID = "";
            if (this.Tag != null)
            {
                RoleID = this.Tag.ToString();
            }

            Access_Role_Info Role = new Access_Role_Info();
            Role.CheckRoleForm(UserKey, RoleID);
            if (Role.RoleRead == 0)
            {
                btn_Search.Enabled = false;
                btn_Export.Enabled = false;
            }
        }
        #endregion
    }
}
