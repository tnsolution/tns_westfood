﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TN.Library.Inventory;
using TN.Library.Miscellaneous;
using TN.Library.System;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Setup_Category : Form
    {
        OpenFileDialog of = new OpenFileDialog();
        private string _TeamName = "";
        public int _CategoryParent = 0;
        private Product_Stages_Info Product_Stage;
        private DataRow zTable;
        public Frm_Setup_Category()
        {
            InitializeComponent();
            GV_Excel_Layout();
        }

        private void Frm_Setup_Category_Load(object sender, EventArgs e)
        {
            LV_List_Layout(LV_ListGroup);
        }

        #region [Process Data]
        private void SaveGV()
        {
            string zMessage = "";
            int n = GV_Excel.Rows.Count;

            Product_Stage = new Product_Stages_Info();

            for (int i = 0; i < n - 1; i++)
            {
                Product_Stage.StageName = GV_Excel.Rows[i].Cells["StageName"].Value.ToString();
                Product_Stage.TeamKey = _CategoryParent;
                Product_Stage.UnitName = GV_Excel.Rows[i].Cells["UnitName"].Value.ToString();
                Product_Stage.StagesID = GV_Excel.Rows[i].Cells["StagesID"].Value.ToString();
                Product_Stage.Price = float.Parse(GV_Excel.Rows[i].Cells["Price"].Value.ToString());
                zTable = Product_Stages_Data.List(_CategoryParent, GV_Excel.Rows[i].Cells["StageName"].Value.ToString(), float.Parse(GV_Excel.Rows[i].Cells["Price"].Value.ToString())).Rows[0];
                if (Convert.ToInt32(zTable[0]) == 0)
                {
                    Product_Stage.Create();
                    zMessage += Product_Stage.Message;
                }
            }
            if (zMessage.Length > 0)
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        #endregion


        #region [Design Layout]
        private void LV_List_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 50;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Sheet";
            colHead.Width = 270;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void LV_TeamWorking_LoadSheet()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_ListGroup;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = Data_Access.GetSheet(of.FileName);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                //lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["sheetname"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }

        private void GV_Excel_Layout()
        {
            // Setup Column 
            GV_Excel.Columns.Add("No", "STT");
            GV_Excel.Columns.Add("StageName", "Tên Công Đoạn");
            GV_Excel.Columns.Add("StageID", "Mã");
            GV_Excel.Columns.Add("UnitName", "Đơn Vị");
            GV_Excel.Columns.Add("Price", "Đơn Giá");


            GV_Excel.Columns["No"].Width = 40;
            GV_Excel.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Excel.Columns["No"].ReadOnly = true;

            GV_Excel.Columns["StageName"].Width = 320;
            GV_Excel.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Excel.Columns["StageName"].ReadOnly = true;

            GV_Excel.Columns["StageID"].Width = 150;
            GV_Excel.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Excel.Columns["StageID"].ReadOnly = true;

            GV_Excel.Columns["UnitName"].Width = 150;
            GV_Excel.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Excel.Columns["UnitName"].ReadOnly = true;


            GV_Excel.Columns["Price"].Width = 150;
            GV_Excel.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Excel.Columns["Price"].ReadOnly = true;


            // setup style view

            GV_Excel.BackgroundColor = Color.White;
            GV_Excel.GridColor = Color.FromArgb(227, 239, 255);
            GV_Excel.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Excel.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Excel.DefaultCellStyle.Font = new Font("Arial", 9);

            GV_Excel.AllowUserToResizeRows = false;
            GV_Excel.AllowUserToResizeColumns = true;

            GV_Excel.RowHeadersVisible = false;

            //// setup Height Header
            // GV_Employees.ColumnHeadersHeight = GV_Employees.ColumnHeadersHeight * 3 / 2;
            GV_Excel.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Excel.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_Excel.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Excel.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            for (int i = 1; i <= 8; i++)
            {
                GV_Excel.Rows.Add();
            }
        }

        private void GV_Excel_LoadData(DataTable InTable)
        {
            GV_Excel.Rows.Clear();
            int i = 0;
            foreach (DataRow nRow in InTable.Rows)
            {
                GV_Excel.Rows.Add();
                DataGridViewRow nRowView = GV_Excel.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StageName"].Value = nRow["StageName"].ToString().Trim();
                nRowView.Cells["StageID"].Value = nRow["StageID"].ToString().Trim();
                nRowView.Cells["UnitName"].Value = nRow["UnitName"].ToString().Trim();
                nRowView.Cells["Price"].Value = float.Parse(nRow["Price"].ToString().Trim());
                i++;
            }

        }
        #endregion

        #region [Process Event]

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            SaveGV();
        }

        private void Btn_Import_Click(object sender, EventArgs e)
        {
            of.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            of.Multiselect = false;
            if (DialogResult.OK == of.ShowDialog())
            {
                LV_TeamWorking_LoadSheet();
            }
        }

        private void LV_ListGroup_Click(object sender, EventArgs e)
        {
            _TeamName = LV_ListGroup.SelectedItems[0].SubItems[1].Text;
            DataTable zTable = Data_Access.GetTable(of.FileName, _TeamName);

            DataTable zGvTable = new DataTable();
            zGvTable.Columns.Add("StageName");
            zGvTable.Columns.Add("StageID");
            zGvTable.Columns.Add("UnitName");
            zGvTable.Columns.Add("Price");

            foreach (DataRow r in zTable.Rows)
            {
                DataRow rN = zGvTable.NewRow();

                rN["StageName"] = r[1];
                rN["StageID"] = r[2];
                rN["UnitName"] = r[3];
                rN["Price"] = r[4].ToDouble();
                zGvTable.Rows.Add(rN);
            }

            GV_Excel_LoadData(zGvTable);

            label2.Text = "CHI TIẾT CÔNG ĐOẠN \n "
                           + "Import từ Sheet - " + _TeamName;
        }
        #endregion


    }
}
