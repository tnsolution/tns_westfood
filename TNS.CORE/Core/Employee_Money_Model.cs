﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TNS.CORE
{
    public class Employee_Money_Model
    {
        public float PercentOrder { get; set; } = 0;//tỷ lệ hoàn thanh của đơn hàng
        public string EmployeeKey { get; set; } = "";
        public string EmployeeName { get; set; } = "";
        public string EmployeeID { get; set; } = "";
        public int TeamKey { get; set; } = 0;
        public float TimeTeam { get; set; } = 0; //TG Làm Tại Tổ
        public float TimeTeamPrivate { get; set; } = 0; //TG Làm Tại Tổ (Ăn Riêng)
        public float TimeTeamBorrowPrivate { get; set; } = 0;//TG Tổ Khác (Ăn Riêng)
        public float TimeTeamBorrowShare { get; set; } = 0;//TG Tổ Khác (Ăn Chung)
        public float TimeTeamDifBorrowPrivate { get; set; } = 0;//TG Việc Khác (Ăn Riêng)
        public float TimeTeamDifBorrowShare { get; set; } = 0;//TG Việc Khác (Ăn Chung)
        public double MoneyPersonal { get; set; } = 0;   //Lương Tại Tổ
        public double MoneyPrivate { get; set; } = 0;    //Lương Tại Tổ (Ăn Riêng)
        /// <summary>
        /// Lương Tổ Khác (Ăn Riêng)
        /// </summary>
        public double MoneyBorrowPrivate { get; set; } = 0;
        public double MoneyBorrowShare { get; set; } = 0;//Lương Tổ Khác (Ăn Chung)
        /// <summary>
        /// //Lương TG(Ăn Riêng)
        /// </summary>
        public double MoneyTimePrivate { get; set; } = 0;
        /// <summary>
        /// //Lương TG (Ăn Chung)
        /// </summary>
        public double MoneyTimeShare { get; set; } = 0;
        public double MoneyTotal { get; set; } = 0;
        public int Slug { get; set; } = 0;
        public string Befor15days { get; set; } = "";   //15 ngày đầu
        public string After15days { get; set; } = "";   //15 ngày sau
        public DateTime OrderDate { get; set; } = new DateTime();
        public float RateDay { get; set; } = 0; //hệt số ngày làm việc của đơn hàng
    }
}
