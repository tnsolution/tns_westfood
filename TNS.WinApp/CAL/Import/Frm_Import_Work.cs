﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TNS.IVT;
using TNS.LOG;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Import_Work : Form
    {
        OpenFileDialog of = new OpenFileDialog();
        private string _TeamName = "";
        public int _CategoryParent = 0;
        private Product_Stages_Info Product_Stage;

        public Frm_Import_Work()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GV_Excel, true);
            Utils.DrawGVStyle(ref GV_Excel);
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;

            btn_Import.Click += Btn_Import_Click;
            btn_Save.Click += Btn_Save_Click;
            LV_ListGroup.Click += LV_ListGroup_Click;
            GV_Excel_Layout();
        }

        private void Frm_Setup_Category_Load(object sender, EventArgs e)
        {
            Application_Data.Save_LogApplication(SessionUser.UserLogin.Key, SessionUser.UserLogin.Key, "Truy cập form " + HeaderControl.Text, 2);

            LV_List_Layout(LV_ListGroup);
            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region [Process Data]
        private void SaveGV()
        {
            string zMessage = "";
            int n = GV_Excel.Rows.Count;

            Product_Stage = new Product_Stages_Info();

            for (int i = 0; i < n - 1; i++)
            {
                string zStagesID = GV_Excel.Rows[i].Cells["StagesID"].Value.ToString();
                Product_Stage.GetStagesID_Info(zStagesID);
                Product_Stage.StageName = GV_Excel.Rows[i].Cells["StageName"].Value.ToString();
                Product_Stage.TeamKey = _CategoryParent;
                Product_Stage.UnitName = GV_Excel.Rows[i].Cells["UnitName"].Value.ToString();
                Product_Stage.StagesID = zStagesID;
                Product_Stage.Price = float.Parse(GV_Excel.Rows[i].Cells["Price"].Value.ToString());
                Product_Stage.CreatedBy = SessionUser.UserLogin.Key;
                Product_Stage.CreatedName = SessionUser.UserLogin.EmployeeName;
                Product_Stage.ModifiedBy = SessionUser.UserLogin.Key;
                Product_Stage.ModifiedName = SessionUser.UserLogin.EmployeeName;
                Product_Stage.Save();
                if (Product_Stage.Message == "11")
                {
                    GV_Excel.Rows[i].Cells["Message"].Value = "Thêm mới thành công";
                }
                else if (Product_Stage.Message == "20")
                {
                    GV_Excel.Rows[i].Cells["Message"].Value = "Cập nhật thành công";
                }
                else
                {
                    GV_Excel.Rows[i].Cells["Message"].Value = Product_Stage.Message;
                    GV_Excel.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    zMessage += Product_Stage.Message;
                }
            }
            if (zMessage.Length > 0)
            {
                Utils.TNMessageBoxOK(zMessage, 4);
            }
            else
            {
                Utils.TNMessageBoxOK("Cập nhật thành công", 3);
            }

        }

        #endregion


        #region [Design Layout]
        private void LV_List_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "No";
            colHead.Width = 30;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Sheet";
            colHead.Width = 270;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        private void LV_TeamWorking_LoadSheet()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_ListGroup;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = Data_Access.GetSheet(of.FileName);

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                //lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White;

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["sheetname"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);
            }

            this.Cursor = Cursors.Default;
        }

        private void GV_Excel_Layout()
        {
            // Setup Column 
            GV_Excel.Columns.Add("No", "STT");
            GV_Excel.Columns.Add("StageName", "Tên Công Đoạn");
            GV_Excel.Columns.Add("StagesID", "Mã");
            GV_Excel.Columns.Add("UnitName", "Đơn Vị");
            GV_Excel.Columns.Add("Price", "Đơn Giá");
            GV_Excel.Columns.Add("Message", "Thông báo");

            GV_Excel.Columns["No"].Width = 40;
            GV_Excel.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV_Excel.Columns["No"].ReadOnly = true;

            GV_Excel.Columns["StageName"].Width = 320;
            GV_Excel.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Excel.Columns["StageName"].ReadOnly = true;

            GV_Excel.Columns["StagesID"].Width = 150;
            GV_Excel.Columns["StagesID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Excel.Columns["StagesID"].ReadOnly = true;

            GV_Excel.Columns["UnitName"].Width = 150;
            GV_Excel.Columns["UnitName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Excel.Columns["UnitName"].ReadOnly = true;


            GV_Excel.Columns["Price"].Width = 150;
            GV_Excel.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GV_Excel.Columns["Price"].ReadOnly = true;

            GV_Excel.Columns["Message"].Width = 320;
            GV_Excel.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Excel.Columns["Message"].ReadOnly = true;


            // setup style view

            GV_Excel.BackgroundColor = Color.White;
            GV_Excel.GridColor = Color.FromArgb(227, 239, 255);
            GV_Excel.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Excel.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Excel.DefaultCellStyle.Font = new Font("Arial", 9);

            GV_Excel.AllowUserToResizeRows = false;
            GV_Excel.AllowUserToResizeColumns = true;

            GV_Excel.RowHeadersVisible = false;

            //// setup Height Header
            // GV_Employees.ColumnHeadersHeight = GV_Employees.ColumnHeadersHeight * 3 / 2;
            GV_Excel.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Excel.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_Excel.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Excel.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);
        }
        private string CheckBeforSave()
        {
            string zResult = "";
            int n = 0;
            for (int i = 0; i < GV_Excel.Rows.Count - 1; i++)
            {
                DataGridViewRow nRowView = GV_Excel.Rows[i];
                nRowView.DefaultCellStyle.BackColor = Color.White;
                if (nRowView.Cells["StagesID"].Value.ToString() == "" || nRowView.Cells["StagesID"] == null)
                {
                    nRowView.Cells["Message"].Value = "Lỗi,Chưa nhập mã !";
                    nRowView.DefaultCellStyle.BackColor = Color.Red;
                    n++;
                }
                if (nRowView.Cells["StageName"].Value.ToString() == "" || nRowView.Cells["StageName"] == null)
                {
                    nRowView.Cells["Message"].Value = "Lỗi,Chưa nhập tên công đoạn !";
                    nRowView.DefaultCellStyle.BackColor = Color.Red;
                    n++;
                }
                if (nRowView.Cells["Price"].Value.ToString() == "" || nRowView.Cells["Price"] == null)
                {
                    nRowView.Cells["Price"].Value = "0";
                }
                if (nRowView.Cells["UnitName"].Value.ToString() == "" || nRowView.Cells["UnitName"] == null)
                {
                    nRowView.Cells["Message"].Value = "Lỗi,Chưa nhập đơn vị tính !";
                    nRowView.DefaultCellStyle.BackColor = Color.Red;
                    n++;
                }
                else
                {
                    Product_Unit_Info zUnit = new Product_Unit_Info(nRowView.Cells["UnitName"].Value.ToString());
                    if (zUnit.Key == 0)
                    {
                        nRowView.Cells["Message"].Value = "Lỗi,Đơn vị tính chưa khai báo !";
                        nRowView.DefaultCellStyle.BackColor = Color.Red;
                        n++;
                    }
                }
            }
            if (n > 0)
            {
                zResult = "Lỗi vui lòng kiểm tra lại!";
            }
            return zResult;
        }
        private void GV_Excel_LoadData(DataTable InTable)
        {
            GV_Excel.Rows.Clear();
            int i = 0;
            foreach (DataRow nRow in InTable.Rows)
            {
                GV_Excel.Rows.Add();
                DataGridViewRow nRowView = GV_Excel.Rows[i];
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StageName"].Value = nRow["StageName"].ToString().Trim();
                nRowView.Cells["StagesID"].Value = nRow["StagesID"].ToString().Trim();
                nRowView.Cells["UnitName"].Value = nRow["UnitName"].ToString().Trim();
                nRowView.Cells["Price"].Value = float.Parse(nRow["Price"].ToString().Trim());
                i++;
            }

        }
        #endregion

        #region [Process Event]

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            if (GV_Excel.Rows.Count > 1)
            {
                string zResult = CheckBeforSave();
                if (zResult == "")
                {
                    SaveGV();
                }
                else
                {
                    Utils.TNMessageBoxOK(zResult,4);
                }
            }
            else
            {
                Utils.TNMessageBoxOK("Không tìm thấy dữ liệu",1);
            }
        }

        private void Btn_Import_Click(object sender, EventArgs e)
        {
            of.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            of.Multiselect = false;
            if (DialogResult.OK == of.ShowDialog())
            {
                LV_TeamWorking_LoadSheet();
            }
        }

        private void LV_ListGroup_Click(object sender, EventArgs e)
        {
            _TeamName = LV_ListGroup.SelectedItems[0].SubItems[1].Text;
            DataTable zTable = Data_Access.GetTable(of.FileName, _TeamName);

            DataTable zGvTable = new DataTable();
            zGvTable.Columns.Add("StageName");
            zGvTable.Columns.Add("StagesID");
            zGvTable.Columns.Add("UnitName");
            zGvTable.Columns.Add("Price");

            foreach (DataRow r in zTable.Rows)
            {
                DataRow rN = zGvTable.NewRow();

                rN["StageName"] = r[1];
                rN["StagesID"] = r[2];
                rN["UnitName"] = r[3];
                double zPrice = 0;
                if (double.TryParse(r[4].ToString(), out zPrice))
                {

                }
                rN["Price"] = zPrice;
                zGvTable.Rows.Add(rN);
            }

            GV_Excel_LoadData(zGvTable);

            kryptonHeader1.Text = "Công đoạn \n "
                           + "Import từ Sheet - " + _TeamName;
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void btn_Import_Click_1(object sender, EventArgs e)
        {

        }
    }
}
