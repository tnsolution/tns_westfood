﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
using TNS.Misc;


namespace TNS.CORE
{
    public class Caculater_Productivity_Data
    {
        public static DataTable List_Employee(int TeamKey, string OrderKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = @"SELECT A.EmployeeKey,A.EmployeeID,C.FullName,
ROUND(A.Basket,1) AS Basket,
ROUND(A.Kilogram,1) AS Kilogram,[dbo].[FTR_SoLuongThanhPham](A.EmployeeKey,A.OrderKey) AS KgProduct,
ROUND(A.Time,1) AS Time,A.Borrow,A.Private,A.Share,Money
FROM [dbo].[FTR_Order_Employee] A
LEFT JOIN [dbo].[FTR_Order] B ON A.OrderKey = B.OrderKey
LEFT JOIN [dbo].[SYS_Personal] C ON A.EmployeeKey = C.ParentKey
WHERE B.TeamKey = @TeamKey AND A.OrderKey = @OrderKey
AND A.RecordStatus < 99 AND B.RecordStatus < 99
ORDER BY LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static int Find_Date(DateTime FromDate)
        {
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
            int result = 0;
            string zSQL = @"SELECT COUNT(*) FROM HRM_Time_Holiday  WHERE Holiday BETWEEN @FromDate AND @ToDate";

            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                result = zCommand.ExecuteScalar().ToInt();
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return result;
        }
      
        public static DataTable ListEmployeeGeneral(string OrderKey, int TeamKey, DateTime FromDate, int Status)
        {
            string zSQL = "";
            DateTime zFromDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(FromDate.Year, FromDate.Month, FromDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            if (Status == 0)
            {
                zSQL = @"
SELECT EmployeeKey,B.FullName,A.EmployeeID ,
[dbo].[SumTimeTeam](@FromDate,@ToDate,@TeamKey,EmployeeKey) AS Time,
[dbo].[LaySoLuongAnChung](EmployeeKey,@OrderKey) AS KgEat,
[dbo].[LaySoTienAnChung](EmployeeKey,@OrderKey) AS MoneyEat
FROM [dbo].[HRM_Employee] A
LEFT JOIN [dbo].[SYS_Personal] B ON A.EmployeeKey = B.ParentKey
WHERE A.TeamKey = @TeamKey AND A.RecordStatus < 99 AND B.RecordStatus < 99
ORDER BY LEN(EmployeeID), EmployeeID";
            }
            else if (Status == 1)
            {
                zSQL = @"
SELECT EmployeeKey,EmployeeID,EmployeeName AS FullName,TimeEat AS Time,KgEat,MoneyEat, Kg_Eat, Money_Eat
FROM [dbo].[FTR_Order_Money]  
WHERE OrderKey = @OrderKey AND  KgEat IS NOT NULL ORDER BY LEN(EmployeeID), EmployeeID";
            }
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@OrderKey", SqlDbType.NVarChar).Value = OrderKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
