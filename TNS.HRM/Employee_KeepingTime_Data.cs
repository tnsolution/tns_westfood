﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.HRM
{
    public class Employee_KeepingTime_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT *, AutoKey AS [Key] FROM HRM_Employee_Time WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }
        public static DataTable ListDetail(int TeamKey, DateTime Datetime)
        {
            DateTime zFromDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(Datetime.Year, Datetime.Month, Datetime.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT * 
FROM HRM_Employee_KeepingTime 
WHERE TeamKey = @TeamKey 
AND DateWrite BETWEEN @FromDate AND @ToDate 
AND RecordStatus <> 99
ORDER BY  LEN(EmployeeID), EmployeeID";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = TeamKey;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
        public static DataTable TimeKeepingDetail(string EmployeeID, DateTime Datetime)
        {
            DateTime zFromDate = new DateTime(Datetime.Year, Datetime.Month, 1, 0, 0, 0);
            DateTime zToDate = zFromDate.AddMonths(1).AddDays(-1);
            zToDate = new DateTime(zToDate.Year, zToDate.Month, zToDate.Day, 23, 59, 59);
            DataTable zTable = new DataTable();
            string zSQL = @"
SELECT DateWrite,BeginTime,EndTime,OffTime, TotalTime,TimeID,RicePlus,RiceMinus FROM [dbo].[HRM_Employee_KeepingTime]
WHERE EmployeeID= @EmployeeID AND DateWrite BETWEEN @FromDate AND @ToDate
ORDER BY DateWrite";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID.Trim();
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = ex.ToString();
            }
            return zTable;
        }
    }
}
