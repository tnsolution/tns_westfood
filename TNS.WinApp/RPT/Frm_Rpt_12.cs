﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.Misc;
using TNS.SYS;
using ReportAll = TNS.CORE.Report;
namespace TNS.WinApp
{
    public partial class Frm_Rpt_12 : Form
    {
        private int _TeamKeyClose = 0;
        private DateTime _DateClose;
        public Frm_Rpt_12()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVData, true);
            Utils.DrawGVStyle(ref GVData);
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            btnClose.Click += btnClose_Click;
            btn_Search.Click += Btn_Search_Click;
            GVData.DoubleClick += GVData_DoubleClick;

            dteDate.Value = SessionUser.Date_Work;

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
            LoadDataToToolbox.KryptonComboBox(cbo_TeamID_Search, "SELECT TeamKey, TeamID + ' - ' + TeamName FROM SYS_Team WHERE RecordStatus <> 99 ORDER BY TeamID", "---Tất cả---");
        }

        private void Frm_Rpt_12_Load(object sender, EventArgs e)
        {

        }
        private void DisplayData()
        {

            DataTable _Intable = ReportAll.No12(dteDate.Value, _TeamKeyClose);
            if (_Intable.Rows.Count > 0)
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    InitGV_Layout(GVData, _Intable);

                    int NoGroup = 1;
                    DataGridViewRow zGvRow;
                    int TeamKey = _Intable.Rows[0]["TeamKey"].ToInt();
                    string TeamName = _Intable.Rows[0]["TeamName"].ToString();
                    DataRow[] Array = _Intable.Select("TeamKey=" + TeamKey);
                    if (Array.Length > 0)
                    {
                        DataTable zGroup = Array.CopyToDataTable();
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[0];
                        HeaderRow(zGvRow, zGroup, TeamKey, TeamName, 0);
                    }
                    for (int i = 0; i < _Intable.Rows.Count; i++)
                    {
                        DataRow r = _Intable.Rows[i];
                        if (TeamKey != r["TeamKey"].ToInt())
                        {
                            #region [GROUP]
                            TeamKey = r["TeamKey"].ToInt();
                            TeamName = r["TeamName"].ToString();
                            Array = _Intable.Select("TeamKey=" + TeamKey);
                            if (Array.Length > 0)
                            {
                                DataTable zGroup = Array.CopyToDataTable();
                                GVData.Rows.Add();
                                zGvRow = GVData.Rows[i + NoGroup];
                                HeaderRow(zGvRow, zGroup, TeamKey, TeamName, i + NoGroup);
                            }
                            #endregion
                            NoGroup++;
                        }
                        GVData.Rows.Add();
                        zGvRow = GVData.Rows[i + NoGroup];
                        DetailRow(zGvRow, r, i + NoGroup);
                    }
                    GVData.Rows.Add();
                    zGvRow = GVData.Rows[GVData.Rows.Count - 1];
                    TotalRow(zGvRow, _Intable, GVData.Rows.Count - 1);
                }));
            }

        }

        private void HeaderRow(DataGridViewRow RowView, DataTable Table, int TeamKey, string TeamName, int No)
        {
            RowView.Cells["No"].Value = No.ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = TeamName;
            RowView.Cells["EmployeeID"].Value = "Số công nhân " + Table.Select("TeamKey=" + TeamKey).Length;
            int nCol = 3;
            float zTotal = 0;
            for (int i = 5; i < Table.Columns.Count; i++)
            {
                RowView.Cells[nCol].Value = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "TeamKey=" + TeamKey).Ton1String();
                zTotal += RowView.Cells[nCol].Value.ToFloat();
                nCol++;
            }
            //RowView.Cells["Total"].Value = zTotal.Ton0String();
            RowView.DefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LemonChiffon;
        }
        private void DetailRow(DataGridViewRow RowView, DataRow rDetail, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = rDetail["EmployeeKey"].ToString().Trim();
            RowView.Cells["EmployeeName"].Value = rDetail["EmployeeName"].ToString().Trim();
            RowView.Cells["EmployeeID"].Value = rDetail["EmployeeID"].ToString().Trim();
            int nCol = 5;
            for (int i = 3; i < RowView.Cells.Count - 1; i++)
            {
                RowView.Cells[i].Value = rDetail[nCol].Ton1String();
                nCol++;
            }
            double zTotalRow = 0;
            for (int i = 5; i < rDetail.ItemArray.Length; i++)
            {
                zTotalRow += rDetail[i].ToDouble();
            }

            //RowView.Cells["Total"].Value = zTotalRow.Ton0String();
        }
        private void TotalRow(DataGridViewRow RowView, DataTable Table, int No)
        {
            RowView.Cells["No"].Value = (No).ToString();
            RowView.Cells["EmployeeName"].Tag = "";
            RowView.Cells["EmployeeName"].Value = "Tổng ";
            RowView.Cells["EmployeeID"].Value = "Số công nhân " + Table.Rows.Count;
            float zTotal = 0;
            for (int i = 5; i < Table.Columns.Count; i++)
            {
                RowView.Cells[i.ToString()].Value = Table.Compute("SUM([" + Table.Columns[i].ColumnName + "])", "").Ton1String();
                zTotal += RowView.Cells[i.ToString()].Value.ToFloat();
            }
            //RowView.Cells["Total"].Value = zTotal.Ton0String(); ;
            RowView.DefaultCellStyle.Font = new Font("Tahoma", 10, FontStyle.Bold | FontStyle.Italic);
            RowView.DefaultCellStyle.BackColor = Color.LightSkyBlue;
        }



        private void Btn_Search_Click(object sender, EventArgs e)
        {
            _TeamKeyClose = cbo_TeamID_Search.SelectedValue.ToInt();
            _DateClose = dteDate.Value;
            GVData.Rows.Clear();
            try
            {
                using (Frm_Loading frm = new Frm_Loading(DisplayData)) { frm.ShowDialog(this); }
            }
            catch (Exception ex)
            {
                Utils.TNMessageBox("Thông báo", ex.ToString(), 4);
            }
        }
        private void GVData_DoubleClick(object sender, EventArgs e)
        {
            if (GVData.CurrentRow.Cells["EmployeeID"].Value != null)
            {
                DataGridViewRow zRowEdit = GVData.CurrentRow;
                Frm_TimeKeeping_Detail frm = new Frm_TimeKeeping_Detail();
                frm._EmployeeID = GVData.CurrentRow.Cells["EmployeeID"].Value.ToString();
                frm._EmployeeName = GVData.CurrentRow.Cells["EmployeeName"].Value.ToString();
                frm._DateWrite = dteDate.Value;
                frm.ShowDialog();
            }
        }
        void InitGV_Layout(DataGridView GV, DataTable Table)
        {
            GVData.Rows.Clear();
            GVData.Columns.Clear();

            // Setup Column 
            GV.Columns.Add("No", "STT");
            GV.Columns.Add("EmployeeName", "HỌ VÀ TÊN");
            GV.Columns.Add("EmployeeID", "SỐ THẺ");

            for (int i = 5; i < Table.Columns.Count; i++)
            {
                GV.Columns.Add(i.ToString(), Table.Columns[i].ColumnName);
                GV.Columns[i.ToString()].Width = 100;
                GV.Columns[i.ToString()].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            }
            //  GV.Columns.Add("Total", "Tổng");

            #region
            GV.Columns["No"].Width = 40;
            GV.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GV.Columns["No"].Frozen = true;
            GV.Columns["No"].ReadOnly = true;

            GV.Columns["EmployeeName"].Width = 200;
            GV.Columns["EmployeeName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeName"].Frozen = true;

            GV.Columns["EmployeeID"].Width = 150;
            GV.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV.Columns["EmployeeID"].Frozen = true;

            //GV.Columns["Total"].Width = 120;
            //GV.Columns["Total"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //GV.Columns["Total"].DefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            GV.ColumnHeadersHeight = 45;
            #endregion
        }
        //void InitGV_Data(DataGridView GV, DataTable zTable)
        //{
        //    int no = 0;
        //    GVData.Rows.Clear();
        //    foreach (DataRow nRow in zTable.Rows)
        //    {
        //        GV.Rows.Add();
        //        DataGridViewRow nRowView = GV.Rows[no];
        //        nRowView.Cells["No"].Value = (no + 1).ToString();
        //        nRowView.Cells["EmployeeName"].Value = nRow["EmployeeName"].ToString().Trim();
        //        nRowView.Cells["EmployeeID"].Value = nRow["EmployeeID"].ToString().Trim();


        //        double zTotal = 0;
        //        for (int i = 2; i < zTable.Columns.Count; i++)
        //        {
        //            double zrow = nRow[i].ToDouble();
        //            nRowView.Cells[i + 1].Value = zrow.Ton0String();
        //            if (nRow[i].ToString() != "")
        //            {
        //                zTotal += nRow[i].ToDouble();
        //            }
        //        }
        //        //  nRowView.Cells["Total"].Value = zTotal.Ton0String();
        //        no++;
        //    }
        //    GV.Rows.Add();
        //    // InitSum_GVData(GV.Rows[zTable.Rows.Count], zTable);
        //}


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
