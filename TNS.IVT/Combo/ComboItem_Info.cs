﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace TNS.IVT
{
    public class ComboItem_Info
    {

        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ItemKey = "";
        private string _ItemID = "";
        private string _ItemName = "";
        private string _ComboKey = "";
        private int _UnitKey = 0;
        private string _UnitName = "";
        private float _Quantity;
        private float _Price;
        private float _RecoveryRate;
        private float _ConvertRate;
        private float _SubTotalPrice;
        private bool _Active;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private int _Rank = 0;
        private float _LonRate = 0;
        private float _KgRate = 0;
        private float _GroupStage = 0;
        private string _GroupName = "";
        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ItemKey
        {
            get { return _ItemKey; }
            set { _ItemKey = value; }
        }
        public string ItemID
        {
            get { return _ItemID; }
            set { _ItemID = value; }
        }
        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        }
        public string ComboKey
        {
            get { return _ComboKey; }
            set { _ComboKey = value; }
        }
        public int UnitKey
        {
            get { return _UnitKey; }
            set { _UnitKey = value; }
        }
        public string UnitName
        {
            get { return _UnitName; }
            set { _UnitName = value; }
        }
        public float Quantity
        {
            get { return _Quantity; }
            set { _Quantity = value; }
        }
        public float Price
        {
            get { return _Price; }
            set { _Price = value; }
        }
        public float RecoveryRate
        {
            get { return _RecoveryRate; }
            set { _RecoveryRate = value; }
        }
        public float ConvertRate
        {
            get { return _ConvertRate; }
            set { _ConvertRate = value; }
        }
        public float SubTotalPrice
        {
            get { return _SubTotalPrice; }
            set { _SubTotalPrice = value; }
        }
        public bool Active
        {
            get { return _Active; }
            set { _Active = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int Rank
        {
            get
            {
                return _Rank;
            }

            set
            {
                _Rank = value;
            }
        }

        public float LonRate
        {
            get
            {
                return _LonRate;
            }

            set
            {
                _LonRate = value;
            }
        }

        public float KgRate
        {
            get
            {
                return _KgRate;
            }

            set
            {
                _KgRate = value;
            }
        }

        public float GroupStage
        {
            get
            {
                return _GroupStage;
            }

            set
            {
                _GroupStage = value;
            }
        }

        public string GroupName
        {
            get
            {
                return _GroupName;
            }

            set
            {
                _GroupName = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public ComboItem_Info()
        {
        }
        public ComboItem_Info(string AutoKey)
        {
            string zSQL = "SELECT * FROM IVT_ComboItem WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    if (zReader["ItemKey"] != DBNull.Value)
                        _ItemKey = zReader["ItemKey"].ToString();
                    _ItemID = zReader["ItemID"].ToString().Trim();
                    _ItemName = zReader["ItemName"].ToString().Trim();
                    _ComboKey = zReader["ComboKey"].ToString();
                    if (zReader["UnitKey"] != DBNull.Value)
                        _UnitKey = int.Parse(zReader["UnitKey"].ToString());
                    _UnitName = zReader["UnitName"].ToString().Trim();
                    if (zReader["GroupStage"] != DBNull.Value)
                        _GroupStage = int.Parse(zReader["GroupStage"].ToString());
                    if (zReader["Quantity"] != DBNull.Value)
                        _Quantity = float.Parse(zReader["Quantity"].ToString());
                    if (zReader["Price"] != DBNull.Value)
                        _Price = float.Parse(zReader["Price"].ToString());
                    if (zReader["LonRate"] != DBNull.Value)
                        _LonRate = float.Parse(zReader["LonRate"].ToString());
                    if (zReader["KgRate"] != DBNull.Value)
                        _KgRate = float.Parse(zReader["KgRate"].ToString());
                    if (zReader["RecoveryRate"] != DBNull.Value)
                        _RecoveryRate = float.Parse(zReader["RecoveryRate"].ToString());
                    if (zReader["ConvertRate"] != DBNull.Value)
                        _ConvertRate = float.Parse(zReader["ConvertRate"].ToString());
                    if (zReader["SubTotalPrice"] != DBNull.Value)
                        _SubTotalPrice = float.Parse(zReader["SubTotalPrice"].ToString());
                    if (zReader["Active"] != DBNull.Value)
                        _Active = (bool)zReader["Active"];
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    _GroupName = zReader["GroupName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public ComboItem_Info(DataRow Row)
        {
            if (Row["AutoKey"] != DBNull.Value)
                _AutoKey = int.Parse(Row["AutoKey"].ToString());
            if (Row["ItemKey"] != DBNull.Value)
                _ItemKey = Row["ItemKey"].ToString();
            _ItemID = Row["ItemID"].ToString().Trim();
            _ItemName = Row["ItemName"].ToString().Trim();
            _ComboKey = Row["ComboKey"].ToString();
            if (Row["UnitKey"] != DBNull.Value)
                _UnitKey = int.Parse(Row["UnitKey"].ToString());
            _UnitName = Row["UnitName"].ToString().Trim();
            if (Row["GroupStage"] != DBNull.Value)
                _GroupStage = int.Parse(Row["GroupStage"].ToString());
            if (Row["Quantity"] != DBNull.Value)
                _Quantity = float.Parse(Row["Quantity"].ToString());
            if (Row["Price"] != DBNull.Value)
                _Price = float.Parse(Row["Price"].ToString());
            if (Row["LonRate"] != DBNull.Value)
                _LonRate = float.Parse(Row["LonRate"].ToString());
            if (Row["KgRate"] != DBNull.Value)
                _KgRate = float.Parse(Row["KgRate"].ToString());
            if (Row["RecoveryRate"] != DBNull.Value)
                _RecoveryRate = float.Parse(Row["RecoveryRate"].ToString());
            if (Row["ConvertRate"] != DBNull.Value)
                _ConvertRate = float.Parse(Row["ConvertRate"].ToString());
            if (Row["SubTotalPrice"] != DBNull.Value)
                _SubTotalPrice = float.Parse(Row["SubTotalPrice"].ToString());
            if (Row["Active"] != DBNull.Value)
                _Active = (bool)Row["Active"];
            _Description = Row["Description"].ToString().Trim();
            if (Row["Rank"] != DBNull.Value)
                _Rank = int.Parse(Row["Rank"].ToString());
            if (Row["RecordStatus"] != DBNull.Value)
                _RecordStatus = int.Parse(Row["RecordStatus"].ToString());
            if (Row["CreatedOn"] != DBNull.Value)
                _CreatedOn = (DateTime)Row["CreatedOn"];
            _CreatedBy = Row["CreatedBy"].ToString().Trim();
            _CreatedName = Row["CreatedName"].ToString().Trim();
            if (Row["ModifiedOn"] != DBNull.Value)
                _ModifiedOn = (DateTime)Row["ModifiedOn"];
            _ModifiedBy = Row["ModifiedBy"].ToString().Trim();
            _ModifiedName = Row["ModifiedName"].ToString().Trim();
            _GroupName = Row["GroupName"].ToString().Trim();
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO IVT_ComboItem ("
        + " ItemKey ,ItemID ,ItemName ,ComboKey ,UnitKey ,UnitName ,GroupStage , GroupName ,Quantity ,Price ,KgRate ,LonRate ,RecoveryRate ,ConvertRate ,SubTotalPrice ,Active ,Description  ,Rank ,RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@ItemKey ,@ItemID ,@ItemName ,@ComboKey ,@UnitKey ,@UnitName ,@GroupStage , @GroupName ,@Quantity ,@Price ,@KgRate ,@LonRate ,@RecoveryRate ,@ConvertRate ,@SubTotalPrice ,@Active ,@Description ,@Rank ,0 ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = _ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID.Trim();
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName.Trim();
                if (_ComboKey.Length == 36)
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ComboKey);
                else
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName.Trim();
                zCommand.Parameters.Add("@GroupStage", SqlDbType.Int).Value = _GroupStage;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = _GroupName.Trim();
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = _Price;
                zCommand.Parameters.Add("@LonRate", SqlDbType.Float).Value = _LonRate;
                zCommand.Parameters.Add("@KgRate", SqlDbType.Float).Value = _KgRate;
                zCommand.Parameters.Add("@RecoveryRate", SqlDbType.Float).Value = _RecoveryRate;
                zCommand.Parameters.Add("@ConvertRate", SqlDbType.Float).Value = _ConvertRate;
                zCommand.Parameters.Add("@SubTotalPrice", SqlDbType.Float).Value = _SubTotalPrice;
                zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE IVT_ComboItem SET "
                        + " ItemKey = @ItemKey,"
                        + " ItemID = @ItemID,"
                        + " ItemName = @ItemName,"
                        + " ComboKey = @ComboKey,"
                        + " UnitKey = @UnitKey,"
                        + " UnitName = @UnitName,"
                        + " GroupStage = @GroupStage,"
                        + " GroupName = @GroupName,"
                        + " Quantity = @Quantity,"
                        + " Price = @Price,"
                        + " KgRate = @KgRate,"
                        + " LonRate = @LonRate,"
                        + " RecoveryRate = @RecoveryRate,"
                        + " ConvertRate = @ConvertRate,"
                        + " SubTotalPrice = @SubTotalPrice,"
                        + " Active = @Active,"
                        + " Rank = @Rank,"
                        + " Description = @Description,"
                        + " RecordStatus = 0,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ItemKey", SqlDbType.Int).Value = _ItemKey;
                zCommand.Parameters.Add("@ItemID", SqlDbType.NVarChar).Value = _ItemID.Trim();
                zCommand.Parameters.Add("@ItemName", SqlDbType.NVarChar).Value = _ItemName.Trim();
                if (_ComboKey.Length == 36)
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ComboKey);
                else
                    zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@UnitKey", SqlDbType.Int).Value = _UnitKey;
                zCommand.Parameters.Add("@UnitName", SqlDbType.NVarChar).Value = _UnitName.Trim();
                zCommand.Parameters.Add("@GroupStage", SqlDbType.Int).Value = _GroupStage;
                zCommand.Parameters.Add("@GroupName", SqlDbType.NVarChar).Value = _GroupName.Trim();
                zCommand.Parameters.Add("@Quantity", SqlDbType.Float).Value = _Quantity;
                zCommand.Parameters.Add("@Price", SqlDbType.Float).Value = _Price;
                zCommand.Parameters.Add("@KgRate", SqlDbType.Float).Value = _KgRate;
                zCommand.Parameters.Add("@LonRate", SqlDbType.Float).Value = _LonRate;
                zCommand.Parameters.Add("@RecoveryRate", SqlDbType.Float).Value = _RecoveryRate;
                zCommand.Parameters.Add("@ConvertRate", SqlDbType.Float).Value = _ConvertRate;
                zCommand.Parameters.Add("@SubTotalPrice", SqlDbType.Float).Value = _SubTotalPrice;
                zCommand.Parameters.Add("@Active", SqlDbType.Bit).Value = _Active;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteItem()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_ComboItem SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteAll()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE IVT_ComboItem SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ComboKey = @ComboKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ComboKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ComboKey);
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Emty()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "DELETE FROM IVT_ComboItem  WHERE AutoKey = @AutoKey AND ItemKey =@ItemKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_ComboKey.Trim().Length == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
