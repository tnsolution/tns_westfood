﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using TNS.CORE;
using TNS.HRM;
using TNS.IVT;
using TNS.Misc;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_Import_Record : Form
    {
        List<Order_Rate_Info> _List_Rate;
        List<Order_Employee_Info> _List_Employee;
        List<Order_Object> _List_OrderObj;
        Order_Object _OrderObj;

        int MaxData = 0;
        string _OrderID = "";
        string _OrderIDCompare = "";

        //zRow.Cells["Message"].Tag = null; là dòng đang lỗi//hoặc chưa kiểm tra
        //zRow.Cells["Message"].Tag =0; dòng đã check xong lỗi
        FileInfo FileInfo;
        public Frm_Import_Record()
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            Utils.DoubleBuffered(GVRate, true);
            Utils.DoubleBuffered(GVOrder, true);
            Utils.DoubleBuffered(GVEmployee, true);

            Utils.DrawGVStyle(ref GVRate);
            Utils.DrawGVStyle(ref GVOrder);
            Utils.DrawGVStyle(ref GVEmployee);

            GVOrder_Layout();
            GVEmployee_Layout();
            GVRate_Layout();
            GVOrder.CellEndEdit += GVOrder_CellEndEdit;
            //GVOrder.KeyDown += GVOrder_KeyDown;

            GVEmployee.CellEndEdit += GVEmpoyee_CellEndEdit;
            GVRate.CellEndEdit += GVRate_CellEndEdit;
        }

        private int _CheckErr = 0;

        private void FrmImExProductivity_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            timer1.Stop();

            timer2.Enabled = true;
            timer2.Stop();

            this.Bounds = Screen.PrimaryScreen.WorkingArea;
        }

        #region [Design Layout DataGridView]
        private void GVOrder_Layout()
        {
            // Setup Column 
            GVOrder.Columns.Add("No", "STT");
            GVOrder.Columns.Add("OrderID_Compare", "Mã Đơn Hàng đối chiếu");
            GVOrder.Columns.Add("OrderID", "Mã Đơn Hàng");
            GVOrder.Columns.Add("OrderDate", "Ngày Đơn Hàng");
            GVOrder.Columns.Add("TeamID", "Mã Nhóm");
            GVOrder.Columns.Add("TeamName", "Tên Nhóm");
            GVOrder.Columns.Add("ProductID", "Mã Sản Phẩm");
            GVOrder.Columns.Add("ProductName", "Tên Sản Phẩm");
            GVOrder.Columns.Add("StageID", "Mã công Đoạn");
            GVOrder.Columns.Add("StageName", "Tên công Đoạn");
            GVOrder.Columns.Add("PriceStage", "Đơn Giá");
            GVOrder.Columns.Add("OrderIDFollow", "Số Chứng Từ");
            GVOrder.Columns.Add("QuantityDocument", "SL Chứng Từ");
            GVOrder.Columns.Add("QuantityReality", "SL Thực Tế");
            GVOrder.Columns.Add("QuantityLose", "SL Hao Hụt");
            GVOrder.Columns.Add("Unit", "Đơn Vị");
            GVOrder.Columns.Add("WorkStatus", "Tình Trạng");
            GVOrder.Columns.Add("Percent", "Tỷ lệ HT");
            GVOrder.Columns.Add("Note", "Ghi Chú");
            GVOrder.Columns.Add("Message", "Thông Báo");

            GVOrder.Columns["No"].Width = 40;
            GVOrder.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Message"].Width = 120;
            GVOrder.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["OrderID_Compare"].Width = 150;
            GVOrder.Columns["OrderID_Compare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderID"].Width = 150;
            GVOrder.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["OrderDate"].Width = 120;
            GVOrder.Columns["OrderDate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["TeamID"].Width = 120;
            GVOrder.Columns["TeamID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["TeamName"].Width = 250;
            GVOrder.Columns["TeamName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["TeamName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["ProductID"].Width = 120;
            GVOrder.Columns["ProductID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["ProductName"].Width = 200;
            GVOrder.Columns["ProductName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVOrder.Columns["StageID"].Width = 120;
            GVOrder.Columns["StageID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageID"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["StageName"].Width = 200;
            GVOrder.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVOrder.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GVOrder.Columns["PriceStage"].Width = 80;
            GVOrder.Columns["PriceStage"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVOrder.Columns["PriceStage"].ReadOnly = true;

            GVOrder.Columns["OrderIDFollow"].Width = 120;
            GVOrder.Columns["OrderIDFollow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["QuantityDocument"].Width = 120;
            GVOrder.Columns["QuantityDocument"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["QuantityReality"].Width = 120;
            GVOrder.Columns["QuantityReality"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["QuantityLose"].Width = 120;
            GVOrder.Columns["QuantityLose"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Unit"].Width = 90;
            GVOrder.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["WorkStatus"].Width = 90;
            GVOrder.Columns["WorkStatus"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Percent"].Width = 90;
            GVOrder.Columns["Percent"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Note"].Width = 200; //AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            GVOrder.Columns["Note"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVOrder.Columns["Message"].Width = 300; //AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;   
        }
        private void GVEmployee_Layout()
        {
            DataGridViewCheckBoxColumn zColumn;

            // Setup Column 
            GVEmployee.Columns.Add("No", "STT");
            GVEmployee.Columns.Add("EmployeeID", "Mã NV");
            GVEmployee.Columns.Add("Name", "Tên Nhân Viên");
            GVEmployee.Columns.Add("Basket", "Số Rổ");
            GVEmployee.Columns.Add("Kg", "Số Lượng");
            GVEmployee.Columns.Add("Time", "Thời Gian");
            GVEmployee.Columns.Add("Borrow", "Mượn Người");

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "Private";
            zColumn.HeaderText = "Ăn Riêng";
            GVEmployee.Columns.Add(zColumn);

            zColumn = new DataGridViewCheckBoxColumn();
            zColumn.Name = "General";
            zColumn.HeaderText = "Ăn Chung";
            GVEmployee.Columns.Add(zColumn);

            GVEmployee.Columns.Add("OrderID", "Mã Đơn Hàng");
            GVEmployee.Columns.Add("OrderID_Compare", "Mã Đơn Hàng đối chiếu");

            GVEmployee.Columns.Add("Message", "Thông Báo");

            GVEmployee.Columns["No"].Width = 40;
            GVEmployee.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVEmployee.Columns["EmployeeID"].Width = 70;
            GVEmployee.Columns["EmployeeID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVEmployee.Columns["Name"].Width = 315;
            GVEmployee.Columns["Name"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVEmployee.Columns["Name"].Frozen = true;

            GVEmployee.Columns["Basket"].Width = 120;
            GVEmployee.Columns["Basket"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVEmployee.Columns["Kg"].Width = 80;
            GVEmployee.Columns["Kg"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVEmployee.Columns["Time"].Width = 80;
            GVEmployee.Columns["Time"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            GVEmployee.Columns["Borrow"].Width = 100;
            GVEmployee.Columns["Borrow"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            GVEmployee.Columns["Borrow"].ReadOnly = true;

            GVEmployee.Columns["Private"].Width = 100;
            GVEmployee.Columns["Private"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVEmployee.Columns["General"].Width = 100;
            GVEmployee.Columns["General"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVEmployee.Columns["OrderID"].Width = 200;
            GVEmployee.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVEmployee.Columns["OrderID_Compare"].Width = 200;
            GVEmployee.Columns["OrderID_Compare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVEmployee.Columns["Message"].Width = 300;
            GVEmployee.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVEmployee.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

        }
        private void GVRate_Layout()
        {
            // Setup Column 
            GVRate.Columns.Add("No", "STT");
            GVRate.Columns.Add("CategoryName", "Tên Hệ Số");
            GVRate.Columns.Add("RateID", "Mã Hệ Số");
            GVRate.Columns.Add("Rate", "Giá Trị");
            GVRate.Columns.Add("OrderID", "Mã Đơn Hàng");
            GVRate.Columns.Add("OrderID_Compare", "Mã Đơn Hàng đối chiếu");
            GVRate.Columns.Add("Message", "Thông Báo");

            GVRate.Columns["No"].Width = 40;
            GVRate.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVRate.Columns["CategoryName"].Width = 250;
            GVRate.Columns["CategoryName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVRate.Columns["RateID"].Width = 80;
            GVRate.Columns["RateID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            GVRate.Columns["Rate"].Width = 80;
            GVRate.Columns["Rate"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            GVRate.Columns["Rate"].ReadOnly = true;

            GVRate.Columns["OrderID"].Width = 200;
            GVRate.Columns["OrderID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVRate.Columns["OrderID_Compare"].Width = 200;
            GVRate.Columns["OrderID_Compare"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GVRate.Columns["Message"].Width = 120;
            GVRate.Columns["Message"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GVRate.Columns["Message"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

        }
        #endregion

        #region [Process Data]
        private void GVOrder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (GVOrder.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                CheckRowOrder(GVOrder.Rows[e.RowIndex], e.RowIndex);
        }
        private void GVEmpoyee_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (GVEmployee.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                CheckRowEmployee(GVEmployee.Rows[e.RowIndex], e.RowIndex);
        }
        private void GVRate_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (GVRate.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                CheckRowRate(GVRate.Rows[e.RowIndex], e.RowIndex);
        }

        private string CheckRowOrder(DataGridViewRow zRow, int stt)
        {
            string zMessage = "";

            zRow.DefaultCellStyle.ForeColor = Color.Navy;
            string zProductID = zRow.Cells["ProductID"].Value.ToString();
            string zTeamID = zRow.Cells["TeamID"].Value.ToString();
            string zStageID = zRow.Cells["StageID"].Value.ToString();
            string zOrder = zRow.Cells["OrderID"].Value.ToString();
            int zFindOrder = Order_Data.FindOrderID(zRow.Cells["OrderID"].Value.ToString());
            //kiểm tra order đã tồn tại chưa
            if (zFindOrder != 0 || zOrder == "")
            {
                zMessage += "\n Đơn hàng;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zOrder + " đã nhập rồi";
            }
            string zOrderID_Compare = zRow.Cells["OrderID_Compare"].Value.ToString();
            int zFindOrderID_Compare = Order_Data.FindOrderID_Compare(zRow.Cells["OrderID_Compare"].Value.ToString());
            //kiểm tra order đôi chieu đã tồn tại chưa
            if (zOrderID_Compare == "")
            {
                zMessage += "\n Đơn hàng;Stt:" + (stt) + ";Lỗi!Mã ĐH đối chiếu:" + zOrder + " không nhập mã đối chiếu";
            }
            else if (zFindOrderID_Compare != 0)
            {
                zMessage += "\n Đơn hàng;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zOrder + " đã nhập rồi";
            }
            // kiểm tra product đã có cài đặt chưa
            Product_Info zProduct = new Product_Info();
            zProduct.Get_Product_Info_ID(zProductID);
            if (zProduct.ProductKey == "")
            {
                zMessage += "\n Đơn hàng;Stt:" + (stt) + ";Lỗi!.Sản phẩm:" + zProductID + " không tồn tại";
            }
            // kiểm tra công đoạn đã có cài đặt chưa
            Product_Stages_Info zStage = new Product_Stages_Info();
            zStage.GetStagesID_Info(zStageID);
            if (zStage.Key == 0 || zStageID == "")
            {
                zMessage += "\n Đơn hàng;Stt:" + (stt) + ";Lỗi!.Công đoạn:" + zStageID + " không tồn tại";
            }


            // kiểm tra nhóm đã có cài đặt chưa
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            if (zTeam.Key == 0 || zTeamID == "")
            {
                zMessage += "\n Đơn hàng;Stt:" + (stt) + ";Lỗi!.Tổ nhóm:" + zTeamID + " không tồn tại";
            }
            if (zMessage == "")
            {
                zRow.Cells["Message"].Tag = 0;
                zRow.Cells["Message"].Value = "";
            }
            else
            {
                zRow.Cells["Message"].Tag = null;
                zRow.Cells["Message"].Value = zMessage;
                zRow.DefaultCellStyle.ForeColor = Color.Maroon;
            }
            return zMessage;
        }
        private string CheckRowEmployee(DataGridViewRow zRow, int stt)
        {
            string zMessage = "";

            zRow.DefaultCellStyle.ForeColor = Color.Navy;
            string zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString();
            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            if (zEmployee.Key == "")
            {
                zMessage += "\n Công nhân;Stt:" + (stt) + ";Lỗi!.Số thẻ:" + zEmployeeID + " không tồn tại";
            }
            else
            {
                if (zEmployee.TeamKey == 0)
                {
                    zMessage += "\n Công nhân;Stt:" + (stt) + ";Lỗi!.Số thẻ:" + zEmployeeID + " chưa chọn tổ nhóm";
                }
            }

            string zOrderID = zRow.Cells["OrderID"].Value.ToString();
            int zFindOrderID = Order_Data.FindOrderID(zRow.Cells["OrderID"].Value.ToString());
            //kiểm tra order đã tồn tại chưa
            if (zOrderID == "")
            {
                zMessage += "\n Công nhân;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zEmployeeID + " không nhập mã đơn hàng";
            }
            else if (zFindOrderID != 0)
            {
                zMessage += "\n Công nhân;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zOrderID + " đã nhập rồi";
            }


            string zOrderID_Compare = zRow.Cells["OrderID_Compare"].Value.ToString();
            int zFindOrderID_Compare = Order_Data.FindOrderID_Compare(zRow.Cells["OrderID_Compare"].Value.ToString());
            //kiểm tra order đôi chieu đã tồn tại chưa
            if (zOrderID_Compare == "")
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!Mã ĐH đối chiếu:" + zEmployeeID + " không nhập mã đối chiếu";
            }
            else if (zFindOrderID_Compare != 0)
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!Mã ĐH đối chiếu:" + zOrderID_Compare + " đã nhập rồi";
            }
            if (zMessage == "")
            {
                zRow.Cells["Message"].Tag = 0;
                zRow.Cells["Message"].Value = "";
            }
            else
            {
                zRow.Cells["Message"].Tag = null;
                zRow.DefaultCellStyle.ForeColor = Color.Red;
            }
            zRow.Cells["Message"].Value = zMessage;
            return zMessage;
        }
        private string CheckRowRate(DataGridViewRow zRow, int stt)
        {
            string zMessage = "";
            zRow.DefaultCellStyle.ForeColor = Color.Navy;
            string zRateID = zRow.Cells["RateID"].Value.ToString();
            Category_Info zInfo = new Category_Info();
            zInfo.GetID(zRateID);
            if (zInfo.AutoKey == 0 || zRateID == "")
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!.Mã hế số:" + zRateID + " không tồn tại";
            }
            string zOrder = zRow.Cells["OrderID"].Value.ToString();
            int zFindOrder = Order_Data.FindOrderID(zRow.Cells["OrderID"].Value.ToString());
            //kiểm tra order đã tồn tại chưa
            if (zOrder == "")
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zOrder + " không nhập mã";
            }
            else if (zFindOrder != 0)
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zOrder + " đã nhập rồi";
            }

            string zOrderID_Compare = zRow.Cells["OrderID_Compare"].Value.ToString();
            int zFindOrderID_Compare = Order_Data.FindOrderID_Compare(zRow.Cells["OrderID_Compare"].Value.ToString());
            if (zOrderID_Compare == "")
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zOrder + "mã đối chiếu chưa nhập";
            }
            else if (zOrderID_Compare == "")
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!Mã ĐH:" + zOrder + " không nhập mã đối chiếu";
            }
            //kiểm tra order đôi chieu đã tồn tại chưa
            else if (zFindOrderID_Compare != 0)
            {
                zMessage += "\n Hệ số;Stt:" + (stt) + ";Lỗi!Mã ĐH đối chiếu:" + zOrderID_Compare + " đã nhập rồi";
            }
            if (zMessage == "")
            {
                zRow.Cells["Message"].Tag = 0;
                zRow.Cells["Message"].Value = "";
            }
            else
            {
                zRow.Cells["Message"].Tag = null;
                zRow.DefaultCellStyle.ForeColor = Color.Red;
                zRow.Cells["Message"].Value = zMessage;
            }
            return zMessage;
        }

        private void SaveObject(DataGridViewRow zRow)
        {
            string zOrderKey = "";
            string zOrderID = zRow.Cells["OrderID"].Value.ToString();
            DateTime zOrderDate = DateTime.Parse(zRow.Cells["OrderDate"].Value.ToString());
            //Lưu order
            zOrderKey = AddObjOrder(zRow);
            //Lưu Nhân sự
            _List_Employee = new List<Order_Employee_Info>();
            for (int y = 0; y < GVEmployee.Rows.Count; y++)
            {

                if (GVEmployee.Rows[y].Cells["OrderID"].Value.ToString() == zOrderID &&
                    GVEmployee.Rows[y].Cells["Message"].Value == null)
                {
                    AddObjEmployee(GVEmployee.Rows[y], zOrderDate);
                }
            }
            _OrderObj.List_Employee = _List_Employee;
            //Lưu hệ số
            _List_Rate = new List<Order_Rate_Info>();
            for (int z = 0; z < GVRate.Rows.Count; z++)
            {
                if (GVRate.Rows[z].Cells["OrderID"].Value.ToString() == zOrderID &&
                    GVRate.Rows[z].Cells["Message"].Value == null)
                {
                    AddObjRate(GVRate.Rows[z], zOrderDate);
                }
            }
            _OrderObj.List_Rate = _List_Rate;
        }
        private string AddObjOrder(DataGridViewRow zRow)
        {
            string zOrderKey = "";
            string zProductID = zRow.Cells["ProductID"].Value.ToString();
            _OrderObj.OrderID_Compare = zRow.Cells["OrderID_Compare"].Value.ToString();
            _OrderObj.OrderID = zRow.Cells["OrderID"].Value.ToString();
            _OrderObj.OrderDate = DateTime.Parse(zRow.Cells["OrderDate"].Value.ToString());

            string zTeamID = zRow.Cells["TeamID"].Value.ToString();
            Team_Info zTeam = new Team_Info();
            zTeam.Get_Team_ID(zTeamID);
            _OrderObj.TeamKey = zTeam.Key;

            if (zProductID.Length > 0)
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(zProductID);
                _OrderObj.ProductKey = zProduct.ProductKey;
                _OrderObj.ProductID = zProduct.ProductID;
                _OrderObj.ProductName = zProduct.ProductName;
            }
            string zStageID = zRow.Cells["StageID"].Value.ToString();
            Product_Stages_Info zStage = new Product_Stages_Info();
            zStage.GetStagesID_Info(zStageID);
            if (zStage.Key > 0)
            {
                _OrderObj.Category_Stage = zStage.Key;
                _OrderObj.OrderIDFollow = zRow.Cells["OrderIDFollow"].Value.ToString();
                _OrderObj.QuantityDocument = float.Parse(zRow.Cells["QuantityDocument"].Value.ToString());
                _OrderObj.QuantityReality = float.Parse(zRow.Cells["QuantityReality"].Value.ToString());
                _OrderObj.QuantityLoss = float.Parse(zRow.Cells["QuantityLose"].Value.ToString());
                _OrderObj.PercentFinish = float.Parse(zRow.Cells["Percent"].Value.ToString());
                string UnitName = zRow.Cells["Unit"].Value.ToString();
                Product_Unit_Info zUnit = new Product_Unit_Info(UnitName);
                _OrderObj.UnitKey = zUnit.Key;
                _OrderObj.UnitName = zUnit.Unitname;
                if (zRow.Cells["WorkStatus"].Value.ToString().ToUpper() == "ĐÃ THỰC HIỆN")
                {
                    _OrderObj.WorkStatus = 1;
                }
                else
                {
                    _OrderObj.WorkStatus = 0;
                }
                _OrderObj.Note = zRow.Cells["Note"].Value.ToString();
                _OrderObj.CreatedBy = SessionUser.UserLogin.Key;
                _OrderObj.CreatedName = SessionUser.Personal_Info.FullName;
                _OrderObj.ModifiedBy = SessionUser.UserLogin.Key;
                _OrderObj.ModifiedName = SessionUser.Personal_Info.FullName;
            }
            return zOrderKey;
        }
        private void AddObjEmployee(DataGridViewRow zRow, DateTime OrderDate)
        {
            Order_Employee_Info zOrder_Employee = new Order_Employee_Info();
            zOrder_Employee.OrderDate = OrderDate;
            string zEmployeeID = zRow.Cells["EmployeeID"].Value.ToString();
            Employee_Info zEmployee = new Employee_Info();
            zEmployee.GetEmployeeID(zEmployeeID);
            zOrder_Employee.EmployeeID = zEmployeeID;
            zOrder_Employee.EmployeeKey = zEmployee.Key;
            zOrder_Employee.FullName = zEmployee.FullName;
            float zBasket = 0;
            float.TryParse(zRow.Cells["Basket"].Value.ToString(), out zBasket);
            zOrder_Employee.Basket = zBasket;
            float zKilogram = 0;
            float.TryParse(zRow.Cells["Kg"].Value.ToString(), out zKilogram);
            zOrder_Employee.Kilogram = zKilogram;
            float zTime = 0;
            float.TryParse(zRow.Cells["TIme"].Value.ToString(), out zTime);
            zOrder_Employee.Time = zTime;
            if (zRow.Cells["Borrow"].Value.ToString() != "")
            {
                zOrder_Employee.Borrow = 1;
            }
            if (zRow.Cells["Borrow"].Value.ToString() == "")
            {
                zOrder_Employee.Borrow = 0;
            }
            if (zRow.Cells["General"].Value != null)
            {
                if (Convert.ToBoolean(zRow.Cells["General"].Value) == true)
                {
                    zOrder_Employee.Share = 1;
                }
                else
                {
                    zOrder_Employee.Share = 0;
                }
            }
            if (zRow.Cells["Private"].Value != null)
            {
                if (Convert.ToBoolean(zRow.Cells["Private"].Value) == true)
                {
                    zOrder_Employee.Private = 1;
                }
                else
                {
                    zOrder_Employee.Private = 0;
                }
            }
            zOrder_Employee.RecordStatus = 1;
            _List_Employee.Add(zOrder_Employee);
        }
        private void AddObjRate(DataGridViewRow zRow, DateTime OrderDate)
        {
            Order_Rate_Info zOrder_Rate = new Order_Rate_Info();
            string zRateID = zRow.Cells["RateID"].Value.ToString();
            Category_Info zCategory_Info = new Category_Info();
            zCategory_Info.GetID(zRateID);
            zOrder_Rate.CategoryKey = zCategory_Info.AutoKey;
            zOrder_Rate.OrderDate = OrderDate;
            zOrder_Rate.RecordStatus = 1;
            _List_Rate.Add(zOrder_Rate);
        }

        #endregion

        #region [Process Event]
        private void btn_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog zOpf = new OpenFileDialog();

            zOpf.InitialDirectory = @"C:\";
            zOpf.Title = "Browse Excel Files";

            zOpf.CheckFileExists = true;
            zOpf.CheckPathExists = true;

            zOpf.DefaultExt = "txt";
            zOpf.Filter = "All Files|*.*";
            zOpf.FilterIndex = 2;
            zOpf.RestoreDirectory = true;

            zOpf.ReadOnlyChecked = true;
            zOpf.ShowReadOnly = true;

            if (zOpf.ShowDialog() == DialogResult.OK)
            {
                FileInfo = new FileInfo(zOpf.FileName);
                bool zcheck = true;
                zcheck = Data_Access.CheckFileStatus(FileInfo);
                if (zcheck == true)
                {
                    MessageBox.Show("File đang được sử dụng ở chương trình khác.Vui lòng đóng file để tải lên!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        ExcelPackage package = new ExcelPackage(FileInfo);
                        _TableOrder = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[1].Name);
                        _TableEmployee = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[2].Name);
                        _TableRate = Data_Access.GetTable(FileInfo.FullName, package.Workbook.Worksheets[3].Name);
                        package.Dispose();
                        Cursor.Current = Cursors.Default;

                        MaxData = _TableOrder.Rows.Count + _TableEmployee.Rows.Count + _TableRate.Rows.Count;
                        PBar.Maximum = MaxData;

                        _ThreadExcelOrder = new Thread(new ThreadStart(ImportExcelToGVOrder));
                        _ThreadExcelOrder.Start();

                        timer1.Start();
                        btn_Save.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        btn_Save.Enabled = false;
                    }
                }
            }
        }
        private void btn_Save_Click(object sender, EventArgs e)
        {
            int zCountMessage = 0;
            if (GVOrder.Rows.Count > 0 &&
                 GVEmployee.Rows.Count > 0 &&
                 GVRate.Rows.Count > 0)
            {
                GVOrder.CommitEdit(DataGridViewDataErrorContexts.Commit);
                GVEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit);
                GVRate.CommitEdit(DataGridViewDataErrorContexts.Commit);

                this.Cursor = Cursors.WaitCursor;

                _List_OrderObj = new List<Order_Object>();
                for (int i = 0; i < GVOrder.Rows.Count; i++)
                {
                    DataGridViewRow zRow = GVOrder.Rows[i];
                    if (zRow.Cells["Message"].Value == null ||
                        zRow.Cells["Message"].Value.ToString().Length == 0)
                    {
                        _OrderObj = new Order_Object();
                        SaveObject(zRow);
                        _List_OrderObj.Add(_OrderObj);
                    }
                    else
                    {
                        zCountMessage++;
                    }
                }

                string Message = Order_Exe.GetValue(_List_OrderObj);
                if (zCountMessage > 0 || Message != string.Empty)
                {
                    MessageBox.Show("Còn [" + zCountMessage + "] đơn hàng chưa cập nhật !. \r\n " + Message, "Cần kiểm tra lại thông tin !.");
                }
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region [ Excel to GVOrder ]
        DataTable _TableOrder;

        private Thread _ThreadExcelOrder;
        private int _IndexExcelOrder = 0;
        private int _IndexGVOrder = 0;

        delegate void SetItemExcelOrderCallback(DataRow RowExcel);
        delegate void DelItemExcelOrderCallback(int IndexExcel);
        delegate void UpdateItemExcelOrderCallback(int IndexExcel, string MessageError);

        void ImportExcelToGVOrder()
        {
            for (int i = 0; i < _TableOrder.Rows.Count; i++)
            {
                ImportExcelRowToGVOrder(_TableOrder.Rows[i]);
            }
            _ThreadExcelOrder.Abort();
        }
        void ImportExcelRowToGVOrder(DataRow RowExcel)
        {
            if (GVOrder.InvokeRequired)
            {
                SetItemExcelOrderCallback d = new SetItemExcelOrderCallback(ImportExcelRowToGVOrder);
                this.Invoke(d, new object[] { RowExcel });
            }
            else
            {
                PBar.Value++;
                ShowViewToGVOrder(RowExcel);
            }
        }
        void ShowViewToGVOrder(DataRow zRow)
        {
            _IndexExcelOrder++;

            GVOrder.Rows.Add();
            GVOrder.Rows[_IndexGVOrder].Cells["No"].Value = (_IndexExcelOrder + 1).ToString();
            GVOrder.Rows[_IndexGVOrder].Cells["OrderID_Compare"].Value = zRow[1].ToString().Trim();
            GVOrder.Rows[_IndexGVOrder].Cells["OrderID"].Value = zRow[2].ToString().Trim();
            DateTime zOrderDate = DateTime.Parse(zRow[3].ToString().Trim());
            GVOrder.Rows[_IndexGVOrder].Cells["OrderDate"].Value = zOrderDate.ToString("dd/MM/yyyy");
            GVOrder.Rows[_IndexGVOrder].Cells["TeamID"].Value = zRow[4].ToString().Trim();
            GVOrder.Rows[_IndexGVOrder].Cells["TeamName"].Value = zRow[5].ToString().Trim();
            GVOrder.Rows[_IndexGVOrder].Cells["ProductID"].Value = zRow[6].ToString().Trim();
            GVOrder.Rows[_IndexGVOrder].Cells["ProductName"].Value = zRow[7].ToString().Trim();
            GVOrder.Rows[_IndexGVOrder].Cells["StageID"].Value = zRow[8].ToString().Trim();
            GVOrder.Rows[_IndexGVOrder].Cells["StageName"].Value = zRow[9].ToString().Trim();
            double zAmountOrderMain;
            if (!double.TryParse(zRow[10].ToString().Trim(), out zAmountOrderMain))
            {
                GVOrder.Rows[_IndexGVOrder].Cells["PriceStage"].Value = zRow[10].ToString().Trim();
            }
            else
            {
                GVOrder.Rows[_IndexGVOrder].Cells["PriceStage"].Value = zAmountOrderMain.ToString(); ;
            }

            GVOrder.Rows[_IndexGVOrder].Cells["OrderIDFollow"].Value = zRow[11].ToString().Trim();

            float zQuantityDocument;
            if (!float.TryParse(zRow[12].ToString().Trim(), out zQuantityDocument))
            {
                GVOrder.Rows[_IndexGVOrder].Cells["QuantityDocument"].Value = zRow[12].ToString().Trim();
            }
            else
            {
                GVOrder.Rows[_IndexGVOrder].Cells["QuantityDocument"].Value = zQuantityDocument.ToString();
            }

            float zQuantittyReality;
            if (!float.TryParse(zRow[11].ToString().Trim(), out zQuantittyReality))
            {
                GVOrder.Rows[_IndexGVOrder].Cells["QuantityReality"].Value = zRow[13].ToString().Trim();
            }
            else
            {
                GVOrder.Rows[_IndexGVOrder].Cells["QuantityReality"].Value = zQuantittyReality.ToString();
            }

            float zQuantityLose;
            if (!float.TryParse(zRow[12].ToString().Trim(), out zQuantityLose))
            {
                GVOrder.Rows[_IndexGVOrder].Cells["QuantityLose"].Value = zRow[14].ToString().Trim();
            }
            else
            {
                GVOrder.Rows[_IndexGVOrder].Cells["QuantityLose"].Value = zQuantityLose.ToString();
            }

            GVOrder.Rows[_IndexGVOrder].Cells["Unit"].Value = zRow[15].ToString().Trim();
            GVOrder.Rows[_IndexGVOrder].Cells["WorkStatus"].Value = zRow[16].ToString().Trim();
            float zPercent;
            if (!float.TryParse(zRow[15].ToString().Trim(), out zPercent))
            {
                GVOrder.Rows[_IndexGVOrder].Cells["Percent"].Value = zRow[17].ToString().Trim();
            }
            else
            {
                GVOrder.Rows[_IndexGVOrder].Cells["Percent"].Value = zPercent.ToString();
            }
            GVOrder.Rows[_IndexGVOrder].Cells["Note"].Value = zRow[18].ToString().Trim();

            string zMessage = CheckOrder(zRow);
            if (zMessage.Trim().Length > 0)
            {
                ShowError(_IndexGVOrder, zMessage);
            }

            _IndexGVOrder++;
        }
        void ShowError(int IndexExcel, string MessageError)
        {
            if (GVOrder.InvokeRequired)
            {
                UpdateItemExcelOrderCallback d = new UpdateItemExcelOrderCallback(ShowError);
                this.Invoke(d, new object[] { IndexExcel, MessageError });
            }
            else
            {
                if (IndexExcel < GVOrder.Rows.Count)
                {
                    GVOrder.Rows[_IndexGVOrder].Cells["Message"].Value = MessageError;
                    GVOrder.Rows[_IndexGVOrder].DefaultCellStyle.ForeColor = Color.Maroon;
                }
            }
        }
        #endregion

        #region [ Excel to GVEmployee]
        DataTable _TableEmployee;

        private Thread _ThreadExcelEmployee;
        private int _IndexExcelEmployee = 0;
        private int _IndexGVEmployee = 0;

        delegate void SetItemExcelEmployeeCallback(DataRow RowExcel);
        delegate void DelItemExcelEmployeeCallback(int IndexExcel);
        delegate void UpdateItemExcelEmployeeCallback(int IndexExcel, string MessageError);

        void ImportExcelToGVEmployee()//read row excel
        {
            for (int i = 0; i < _TableEmployee.Rows.Count; i++)
            {
                ImportExcelRowToGVEmployee(_TableEmployee.Rows[i]);
            }
            _OrderID = "";
            _OrderIDCompare = "";
            _ThreadExcelEmployee.Abort();
        }
        void ImportExcelRowToGVEmployee(DataRow RowExcel)//set row excel to row gridview
        {
            if (GVEmployee.InvokeRequired)
            {
                SetItemExcelEmployeeCallback d = new SetItemExcelEmployeeCallback(ImportExcelRowToGVEmployee);
                this.Invoke(d, new object[] { RowExcel });
            }
            else
            {
                PBar.Value++;
                ShowViewToGVEmployee(RowExcel);
            }
        }
        void ShowViewToGVEmployee(DataRow zRow)//show data to UI form
        {
            _IndexExcelEmployee++;

            GVEmployee.Rows.Add();
            GVEmployee.Rows[_IndexGVEmployee].Cells["No"].Value = (_IndexExcelEmployee + 1).ToString();
            GVEmployee.Rows[_IndexGVEmployee].Cells["EmployeeID"].Value = zRow[1].ToString();
            GVEmployee.Rows[_IndexGVEmployee].Cells["Name"].Value = zRow[2].ToString();

            float zBasket;
            if (!float.TryParse(zRow[3].ToString().Trim(), out zBasket))
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["Basket"].Value = zRow[3].ToString();
            }
            else
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["Basket"].Value = zBasket.ToString();
            }

            float zKg;
            if (!float.TryParse(zRow[4].ToString().Trim(), out zKg))
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["Kg"].Value = zRow[4].ToString();
            }
            else
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["Kg"].Value = zKg.ToString();
            }

            float zTime;
            if (!float.TryParse(zRow[5].ToString().Trim(), out zTime))
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["TIme"].Value = zRow[5].ToString();
            }
            else
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["TIme"].Value = zTime.ToString();
            }

            if (zRow[6].ToString() == "1")
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["Borrow"].Value = "X";
            }
            else
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["Borrow"].Value = "";
            }

            if (zRow[7].ToString() == "1")
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["Private"].Value = true;
            }

            if (zRow[8].ToString() == "1")
            {
                GVEmployee.Rows[_IndexGVEmployee].Cells["General"].Value = true;
            }

            GVEmployee.Rows[_IndexGVEmployee].Cells["OrderID"].Value = zRow[9].ToString();
            GVEmployee.Rows[_IndexGVEmployee].Cells["OrderID_Compare"].Value = zRow[10].ToString();

            if (_OrderID != zRow[9].ToString() &&
                _OrderIDCompare != zRow[10].ToString())
            {
                _OrderID = zRow[9].ToString();
                _OrderIDCompare = zRow[10].ToString();

                string zMessage = CheckEmployee(zRow);
                if (zMessage.Trim().Length > 0)
                {
                    ShowErrorGVEmployee(_IndexGVEmployee, zMessage);
                }
            }

            _IndexGVEmployee++;

            //GVEmployee.FirstDisplayedScrollingRowIndex = GVEmployee.RowCount - 1;
        }
        void ShowErrorGVEmployee(int IndexExcel, string MessageError)
        {
            if (GVEmployee.InvokeRequired)
            {
                UpdateItemExcelEmployeeCallback d = new UpdateItemExcelEmployeeCallback(ShowErrorGVEmployee);
                this.Invoke(d, new object[] { IndexExcel, MessageError });
            }
            else
            {
                if (IndexExcel < GVEmployee.Rows.Count)
                {
                    GVEmployee.Rows[_IndexGVEmployee].Cells["Message"].Value = MessageError;
                    GVEmployee.Rows[_IndexGVEmployee].DefaultCellStyle.ForeColor = Color.Maroon;
                }
            }
        }
        #endregion

        #region [ Excel to GVRate]
        DataTable _TableRate;

        private Thread _ThreadExcelRate;
        private int _IndexExcelRate = 0;
        private int _IndexGVRate = 0;

        delegate void SetItemExcelRateCallback(DataRow RowExcel);
        delegate void DelItemExcelRateCallback(int IndexExcel);
        delegate void UpdateItemExcelRateCallback(int IndexExcel, string MessageError);

        void ImportExcelToGVRate()//read row excel
        {
            for (int i = 0; i < _TableRate.Rows.Count; i++)
            {
                ImportExcelRowToGVRate(_TableRate.Rows[i]);
            }
            _OrderID = "";
            _OrderIDCompare = "";
            _ThreadExcelRate.Abort();
        }
        void ImportExcelRowToGVRate(DataRow RowExcel)//set row excel to row gridview
        {
            if (GVRate.InvokeRequired)
            {
                SetItemExcelRateCallback d = new SetItemExcelRateCallback(ImportExcelRowToGVRate);
                this.Invoke(d, new object[] { RowExcel });
            }
            else
            {
                PBar.Value++;
                ShowViewToGVRate(RowExcel);
            }
        }
        void ShowViewToGVRate(DataRow zRow)//show data to UI form
        {
            _IndexExcelRate++;

            string zRateID = zRow[2].ToString();
            Category_Info zInfo = new Category_Info();
            zInfo.GetID(zRateID);

            GVRate.Rows.Add();

            GVRate.Rows[_IndexGVRate].Cells["No"].Value = (_IndexExcelRate + 1).ToString();
            GVRate.Rows[_IndexGVRate].Cells["CategoryName"].Value = zInfo.RateName;
            GVRate.Rows[_IndexGVRate].Cells["RateID"].Value = zRow[2].ToString();
            GVRate.Rows[_IndexGVRate].Cells["Rate"].Value = zInfo.Rate.ToString();
            GVRate.Rows[_IndexGVRate].Cells["OrderID"].Value = zRow[3].ToString();
            GVRate.Rows[_IndexGVRate].Cells["OrderID_Compare"].Value = zRow[4].ToString();

            if (_OrderID != zRow[3].ToString() &&
                _OrderIDCompare != zRow[4].ToString())
            {
                _OrderID = zRow[3].ToString();
                _OrderIDCompare = zRow[4].ToString();

                string zMessage = CheckRate(zRow);
                if (zMessage.Trim().Length > 0)
                {
                    ShowErrorGVRate(_IndexGVRate, zMessage);
                }
            }

            _IndexGVRate++;

            //GVRate.FirstDisplayedScrollingRowIndex = GVRate.RowCount - 1;
        }
        void ShowErrorGVRate(int IndexExcel, string MessageError)
        {
            if (GVRate.InvokeRequired)
            {
                UpdateItemExcelRateCallback d = new UpdateItemExcelRateCallback(ShowErrorGVRate);
                this.Invoke(d, new object[] { IndexExcel, MessageError });
            }
            else
            {
                if (IndexExcel < GVRate.Rows.Count)
                {
                    GVRate.Rows[_IndexGVRate].Cells["Message"].Value = MessageError;
                }
            }
        }
        #endregion

        string CheckOrder(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra mã đơn hàng tham chiếu gốc-----------------------------------
            //kiểm tra order id gốc có giá trị
            string zOrderID = RowExcel[1].ToString();
            if (zOrderID.Trim() == "")
            {
                zMessage += "Mã đơn hàng tham chiếu bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key gốc đã tồn tại chưa
                int zOrderKey = Order_Data.FindOrderID_Compare(zOrderID);
                if (zOrderKey != 0)
                {
                    zMessage += "Mã đơn hàng tham chiếu [" + zOrderID + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }


            //---kiểm tra mã đơn hàng của pm----------------------------------------------
            //kiểm tra order id có giá trị
            string zOrderIDPM = RowExcel[2].ToString();
            if (zOrderIDPM.Trim() == "")
            {
                zMessage += "Mã đơn hàng bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key đã tồn tại chưa
                int zOrderKeyPM = Order_Data.FindOrderID(zOrderID);
                if (zOrderKeyPM != 0)
                {
                    zMessage += "Mã đơn hàng [" + zOrderID + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //--- kiểm tra product đã có cài đặt chưa -----------------------------------------------------
            string zProductID = RowExcel[6].ToString();
            if (zProductID == "")
            {
                zMessage += "Mã sản phẩm bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Product_Info zProduct = new Product_Info();
                zProduct.Get_Product_Info_ID(zProductID);
                if (zProduct.ProductKey == "")
                {
                    zMessage += "Sản phẩm: [" + zProductID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //--- kiểm tra công đoạn đã có cài đặt chưa -----------------------------------------------------
            string zStageID = RowExcel[8].ToString();
            string zStagePrice = RowExcel[10].ToString();
            if (zStageID == "")
            {
                zMessage += "Mã công việc bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Product_Stages_Info zStage = new Product_Stages_Info();
                zStage.GetStagesID_Info(zStageID);
                float Price = zStagePrice.ToFloat();
                if (Price != zStage.Price)
                {
                    zMessage += "Giá công việc: [" + zStageID + "] trong excel khác với dữ liệu phần mềm" + Environment.NewLine;
                    isError = true;
                }

                if (zStage.Key == 0)
                {
                    zMessage += "Công việc: [" + zStageID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //--- kiểm tra nhóm đã có cài đặt chưa -----------------------------------------------------
            string zTeamID = RowExcel[4].ToString();
            if (zTeamID == "")
            {
                zMessage += "Mã nhóm bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Team_Info zTeam = new Team_Info();
                zTeam.Get_Team_ID(zTeamID);
                if (zTeam.Key == 0)
                {
                    zMessage += "Tổ nhóm: [" + zTeamID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            if (isError)
                return zMessage;
            else
                return string.Empty;
        }

        string CheckEmployee(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra mã đơn hàng tham chiếu gốc-----------------------------------
            //kiểm tra order id gốc có giá trị
            string zOrderID = RowExcel[10].ToString();
            if (zOrderID.Trim() == "")
            {
                zMessage += "Mã đơn hàng tham chiếu bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key gốc đã tồn tại chưa
                int zOrderKey = Order_Data.FindOrderID_Compare(zOrderID);
                if (zOrderKey != 0)
                {
                    zMessage += "Mã đơn hàng tham chiếu [" + zOrderID + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }


            //---kiểm tra mã đơn hàng của pm----------------------------------------------
            //kiểm tra order id có giá trị
            string zOrderIDPM = RowExcel[9].ToString();
            if (zOrderIDPM.Trim() == "")
            {
                zMessage += "Mã đơn hàng bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key đã tồn tại chưa
                int zOrderKeyPM = Order_Data.FindOrderID(zOrderIDPM);
                if (zOrderKeyPM != 0)
                {
                    zMessage += "Mã đơn hàng [" + zOrderIDPM + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //---kiểm tra mã thẻ nhân viên----------------------------------------------
            string zEmployeeID = RowExcel[1].ToString();
            if (zEmployeeID.Trim() == "")
            {
                zMessage += "Mã thẻ công nhân bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Employee_Info zEmployee = new Employee_Info();
                zEmployee.GetEmployeeID(zEmployeeID);

                if (zEmployee.Key == "")
                {
                    zMessage += "Số thẻ:" + zEmployeeID + " không tồn tại" + Environment.NewLine;
                    isError = true;
                }
                else
                {
                    if (zEmployee.TeamKey == 0)
                    {
                        zMessage += "Số thẻ:" + zEmployeeID + " chưa chọn tổ nhóm" + Environment.NewLine;
                        isError = true;
                    }
                }
            }

            if (isError)
                return zMessage;
            else
                return string.Empty;
        }

        string CheckRate(DataRow RowExcel)
        {
            bool isError = false;
            string zMessage = "Vui lòng kiểm tra: " + Environment.NewLine;

            //---kiểm tra mã đơn hàng tham chiếu gốc-----------------------------------
            //kiểm tra order id gốc có giá trị
            string zOrderID = RowExcel[4].ToString();
            if (zOrderID.Trim() == "")
            {
                zMessage += "Mã đơn hàng tham chiếu bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key gốc đã tồn tại chưa
                int zOrderKey = Order_Data.FindOrderID_Compare(zOrderID);
                if (zOrderKey != 0)
                {
                    zMessage += "Mã đơn hàng tham chiếu [" + zOrderID + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }


            //---kiểm tra mã đơn hàng của pm----------------------------------------------
            //kiểm tra order id có giá trị
            string zOrderIDPM = RowExcel[3].ToString();
            if (zOrderIDPM.Trim() == "")
            {
                zMessage += "Mã đơn hàng bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                //kiểm tra order key đã tồn tại chưa
                int zOrderKeyPM = Order_Data.FindOrderID(zOrderID);
                if (zOrderKeyPM != 0)
                {
                    zMessage += "Mã đơn hàng [" + zOrderID + "] đã tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }

            //---kiểm tra hệ số đơn hàng của pm----------------------------------------------
            string zRateID = RowExcel[2].ToString();
            if (zRateID.Trim() == "")
            {
                zMessage += "Mã hệ số bị rỗng" + Environment.NewLine;
                isError = true;
            }
            else
            {
                Category_Info zInfo = new Category_Info();
                zInfo.GetID(zRateID);
                if (zInfo.AutoKey == 0)
                {
                    zMessage += "Mã hế số: [" + zRateID + "] không tồn tại" + Environment.NewLine;
                    isError = true;
                }
            }
            if (isError)
                return zMessage;
            else
                return string.Empty;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            if (!_ThreadExcelOrder.IsAlive)
            {
                _ThreadExcelEmployee = new Thread(new ThreadStart(ImportExcelToGVEmployee));
                _ThreadExcelEmployee.Start();

                timer2.Start();
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Stop();
            if (!_ThreadExcelEmployee.IsAlive)
            {
                _ThreadExcelRate = new Thread(new ThreadStart(ImportExcelToGVRate));
                _ThreadExcelRate.Start();
            }
            else
            {
                timer2.Start();
            }
        }
    }
}