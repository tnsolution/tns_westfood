﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace TNS.Misc
{
    public partial class TNMessageBox : Form
    {
        public int ActionResult;
        public TNMessageBox()
        {
            InitializeComponent();
            //kéo form
            this.MouseDown += Frm_Main_MouseDown;
            this.MouseMove += Frm_Main_MouseMove;
            this.MouseUp += Frm_Main_MouseUp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="type"></param>
        public TNMessageBox( string description, int type)
        {
            InitializeComponent();

            if (type == 1)
            {
                Panel_Error.Visible = false;
                Panel_Success.Visible = false;
                Panel_Warning.Visible = false;

                Panel_Info.Visible = true;
               // txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[0];
                return;
            }
            if (type == 2)
            {
                Panel_Error.Visible = false;
                Panel_Success.Visible = false;
                Panel_Info.Visible = false;

                Panel_Warning.Visible = true;
                //txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[1];
                return;
            }
            if (type == 3)
            {
                Panel_Error.Visible = false;
                Panel_Warning.Visible = false;
                Panel_Info.Visible = false;

                Panel_Success.Visible = true;
                //txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[2];
                return;
            }
            if (type == 4)
            {
                Panel_Success.Visible = false;
                Panel_Warning.Visible = false;
                Panel_Info.Visible = false;

                Panel_Error.Visible = true;
                //txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[3];
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="type"></param>
        public TNMessageBox(string title, string description, int type, int width, int height)
        {
            InitializeComponent();
            this.Width = width;
            this.Height = height;
            if (type == 1)
            {
                Panel_Error.Visible = false;
                Panel_Success.Visible = false;
                Panel_Warning.Visible = false;

                Panel_Info.Visible = true;
                //txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[0];
                return;
            }
            if (type == 2)
            {
                Panel_Error.Visible = false;
                Panel_Success.Visible = false;
                Panel_Info.Visible = false;

                Panel_Warning.Visible = true;
                //txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[1];
                return;
            }
            if (type == 3)
            {
                Panel_Error.Visible = false;
                Panel_Warning.Visible = false;
                Panel_Info.Visible = false;

                Panel_Success.Visible = true;
                //txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[2];
                return;
            }
            if (type == 4)
            {
                Panel_Success.Visible = false;
                Panel_Warning.Visible = false;
                Panel_Info.Visible = false;

                Panel_Error.Visible = true;
                //txt_Title.Text = title;
                txt_Description.Text = description;
                FormTitle.Values.Image = imageList1.Images[3];
                return;
            }


        }

        private void TNMessageBox_Load(object sender, EventArgs e)
        {

        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            ActionResult = 1;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ActionResult = 0;
            this.Close();
        }

        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = true;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;

                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                //this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion
    }
}
