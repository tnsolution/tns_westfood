﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using TN.Connect;

namespace TNS.HRM.BasicSalary
{
    public class BasicSalary_Info
    {
        private int _AutoKey = 0;
        private string _EmployeeKey = "";
        private double _Amount = 0;
        private DateTime _FromDate;
        private DateTime _ToDate;
        private int _RecordStatus = 0;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _CreatedOn;
        private DateTime _ModifiedOn;
        private string _ModifiedName = "";
        private string _ModifiedBy = "";
        private string _Message = "";

        #region
        public int AutoKey
        {
            get
            {
                return _AutoKey;
            }

            set
            {
                _AutoKey = value;
            }
        }
        public string EmployeeKey
        {
            get
            {
                return _EmployeeKey;
            }

            set
            {
                _EmployeeKey = value;
            }
        }
        public double Amount
        {
            get
            {
                return _Amount;
            }

            set
            {
                _Amount = value;
            }
        }
        public DateTime FromDate
        {
            get
            {
                return _FromDate;
            }

            set
            {
                _FromDate = value;
            }
        }
        public DateTime ToDate
        {
            get
            {
                return _ToDate;
            }

            set
            {
                _ToDate = value;
            }
        }
        public int RecordStatus
        {
            get
            {
                return _RecordStatus;
            }

            set
            {
                _RecordStatus = value;
            }
        }
        public string CreatedBy
        {
            get
            {
                return _CreatedBy;
            }

            set
            {
                _CreatedBy = value;
            }
        }
        public string Createdname
        {
            get
            {
                return _CreatedName;
            }

            set
            {
                _CreatedName = value;
            }
        }
        public DateTime CreatedOn
        {
            get
            {
                return _CreatedOn;
            }

            set
            {
                _CreatedOn = value;
            }
        }
        public DateTime ModifiedOn
        {
            get
            {
                return _ModifiedOn;
            }

            set
            {
                _ModifiedOn = value;
            }
        }
        public string ModifiedName
        {
            get
            {
                return _ModifiedName;
            }

            set
            {
                _ModifiedName = value;
            }
        }
        public string ModifiedBy
        {
            get
            {
                return _ModifiedBy;
            }

            set
            {
                _ModifiedBy = value;
            }
        }
        public string Message
        {
            get
            {
                return _Message;
            }

            set
            {
                _Message = value;
            }
        }
        #endregion

        public BasicSalary_Info()
        {

        }
        
        public BasicSalary_Info(int AutoKey)
        {
            string zSQL = "SELECT * FROM HRM_Basic_Salary WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.UniqueIdentifier).Value = AutoKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AutoKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AutoKey"].ToString());
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _Amount = double.Parse(zReader["Amount"].ToString().Trim());
                    if (zReader["FromDate"] != DBNull.Value)
                        _FromDate = (DateTime)zReader["FromDate"];
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
        }

        public string Create()
        {
            string zSQL = "INSERT INTO HRM_Basic_Salary ("
                + " EmployeeKey, Amount, FromDate, RecordStatus )"
                + " VALUES ("
                + " @EmployeeKey, @Amount, @FromDate, @RecordStatus )";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("FromDate", SqlDbType.DateTime).Value = _FromDate;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                Message = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }

        public string Update()
        {
            string zSQL = " UPDATE HRM_Basic_Salary SET "
                        + " EmployeeKey = @EmployeeKey, "
                        + " Amount = @Amount, "
                        + " FromDate = @FromDate, "
                        + " RecordStatus = @RecordStatus "
                        + "WHERE AutoKey = @AutoKey ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey;
                zCommand.Parameters.Add("Amount", SqlDbType.Money).Value = _Amount;
                zCommand.Parameters.Add("FromDate", SqlDbType.DateTime).Value = _FromDate;
                zCommand.Parameters.Add("RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                Message = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }


        public string Delete()
        {
            string zResult = "";
            string zSQL = "DELETE FROM HRM_Basic_Salary WHERE AutoKey = @AutoKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                Message = Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;

        }

        public string Save()
        {
            string zResult = "";
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
    }  
}
