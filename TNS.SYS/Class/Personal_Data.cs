﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TN.Connect;

namespace TNS.SYS
{
    public class Personal_Data
    {
        public static DataTable List()
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Personal WHERE RecordStatus != 99 ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        public static DataTable List_Person(string ParentKey)
        {
            DataTable zTable = new DataTable();
            string zSQL = "SELECT * FROM SYS_Personal WHERE ParentKey = @ParentKey ORDER BY [RANK]";
            string zConnectionString = ConnectDataBase.ConnectionString;
            try
            {
                SqlConnection zConnect = new SqlConnection(zConnectionString);
                zConnect.Open();
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                SqlDataAdapter zAdapter = new SqlDataAdapter(zCommand);
                zAdapter.Fill(zTable);
                zCommand.Dispose();
                zConnect.Close();
            }
            catch (Exception ex)
            {
                string zstrMessage = "08" + ex.ToString();
            }
            return zTable;
        }

        //public static string Delete_Person(string Parent)
        //{
        //    string _Message = "";
        //    string zResult = "";
        //    //---------- String SQL Access Database ---------------
        //    string zSQL = "DELETE FROM SYS_Personal WHERE ParentKey = @Parent ";
        //    string zConnectionString = ConnectDataBase.ConnectionString;
        //    SqlConnection zConnect = new SqlConnection(zConnectionString);
        //    zConnect.Open();
        //    try
        //    {
        //        SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
        //        zCommand.CommandType = CommandType.Text;
        //        zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
        //        zResult = zCommand.ExecuteNonQuery().ToString();
        //        zCommand.Dispose();
        //    }
        //    catch (Exception Err)
        //    {
        //        _Message = "08" + Err.ToString();
        //    }
        //    finally
        //    {
        //        zConnect.Close();
        //    }
        //    return zResult;
        //}

        public static string Delete_Person(string Parent)
        {
            string _Message = "";
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = @"DELETE A FROM SYS_Personal A
JOIN CRM_Company_Contact B ON A.ParentKey = B.ContactKey
WHERE B.CompanyKey = @Parent";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Parent", SqlDbType.NVarChar).Value = Parent;
                zResult = zCommand.ExecuteNonQuery().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
    }
}
