﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TNS.SYS;

namespace TNS.WinApp
{
    public partial class Frm_SoDoToChuc : Form
    {
        public Frm_SoDoToChuc()
        {
            InitializeComponent();
            btnClose.Click += btnClose_Click;
            btnMini.Click += btnMini_Click;
            btnMax.Click += btnMax_Click;
            //kéo form
            HeaderControl.MouseDown += Frm_Main_MouseDown;
            HeaderControl.MouseMove += Frm_Main_MouseMove;
            HeaderControl.MouseUp += Frm_Main_MouseUp;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            DataTable zbranch = Branch_Data.List();
            DataTable zDepartment = Department_Data.List();
            DataTable zTeam = Team_Data.List();
            TreeNode root = new TreeNode("Tổng Công Ty WESTFOOD", 0, 0);
            root.Tag = 0;
            root.ImageIndex = 0;
            root.ExpandAll();
            foreach (DataRow r in zbranch.Rows)
            {
                TreeNode child = new TreeNode(r["BranchName"].ToString(), 1, 1);
                child.Tag = r["BranchKey"].ToString();
                root.Nodes.Add(child);
                DataRow[] Array1 = zDepartment.Select("BranchKey='" + r["BranchKey"].ToString() + "'");
                if (Array1.Length > 0)
                {
                    DataTable zGroup1 = Array1.CopyToDataTable();
                    foreach (DataRow r1 in zGroup1.Rows)
                    {
                        TreeNode child1 = new TreeNode(r1["DepartmentName"].ToString(), 2, 2);
                        child1.Tag = r1["DepartmentKey"].ToString();
                        child.Nodes.Add(child1);

                        DataRow[] Array2 = zTeam.Select("DepartmentKey='" + r1["DepartmentKey"].ToString() + "'");
                        if (Array2.Length > 0)
                        {
                            DataTable zGroup = Array2.CopyToDataTable();
                            foreach (DataRow r2 in zGroup.Rows)
                            {
                                TreeNode child2 = new TreeNode(r2["TeamName"].ToString(), 3, 3);
                                child2.Tag = r2["TeamKey"].ToString();
                                child1.Nodes.Add(child2);
                            }
                        }
                    }
                }
            }
            treeView1.Nodes.Add(root);
        }


        #region [Custom Control Box Form]
        #region [Dùng kéo rê form]

        private bool dragging = false;
        private Point dragCursorPoint;
        private Point dragFormPoint;
        private bool isMax = false;

        private void Frm_Main_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            dragCursorPoint = Cursor.Position;
            dragFormPoint = this.Location;
        }

        private void Frm_Main_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
                this.Location = Point.Add(dragFormPoint, new Size(dif));
            }
        }

        private void Frm_Main_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
        #endregion

        private void btnMini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            isMax = false;
        }

        private void btnMax_Click(object sender, EventArgs e)
        {
            if (!isMax)
            {
                this.Bounds = Screen.PrimaryScreen.WorkingArea;
                this.FormBorderStyle = FormBorderStyle.None;
                isMax = true;
                return;
            }
            if (isMax)
            {
                this.Height = 600;
                this.Width = 800;
                this.FormBorderStyle = FormBorderStyle.Sizable;
                this.ControlBox = false;
                this.Location = new Point(
                    (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2,
                    (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2);
                isMax = false;
                return;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
