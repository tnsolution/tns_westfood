﻿using System;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.LOC
{
    public class Address_Info
    {
        #region [ Field Name ]
        private int _AutoKey = 0;
        private string _ParentKey = "";
        private string _ParentTable = "";
        private string _Address = "";
        private int _StreetKey = 0;
        private int _WardKey = 0;
        private int _DistrictKey = 0;
        private int _ProvinceKey = 0;
        private int _CountryKey = 0;
        private string _Latlong = "";
        private int _CategoryKey = 0;
        private int _RecordStatus = 0;
        private int _Rank = 0;
        private string _Ext = "";
        private string _Description = "";
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private string _CountryName = "";
        private string _ProvinceName = "";
        private string _DistrictName = "";
        private string _WardName = "";
        #endregion

        #region [ Properties ]
        public bool IsCommandOK
        {
            get
            {
                int zNumber = 0;
                int.TryParse(_Message.Substring(0, 1), out zNumber);
                if (zNumber <= 3) return true; else return false;
            }
        }
        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _AutoKey; }
            set { _AutoKey = value; }
        }
        public string ParentKey
        {
            get { return _ParentKey; }
            set { _ParentKey = value; }
        }
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        public int StreetKey
        {
            get { return _StreetKey; }
            set { _StreetKey = value; }
        }
        public int WardKey
        {
            get { return _WardKey; }
            set { _WardKey = value; }
        }
        public int DistrictKey
        {
            get { return _DistrictKey; }
            set { _DistrictKey = value; }
        }
        public int ProvinceKey
        {
            get { return _ProvinceKey; }
            set { _ProvinceKey = value; }
        }
        public int CountryKey
        {
            get { return _CountryKey; }
            set { _CountryKey = value; }
        }
        public string Latlong
        {
            get { return _Latlong; }
            set { _Latlong = value; }
        }
        public int CategoryKey
        {
            get { return _CategoryKey; }
            set { _CategoryKey = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public int Rank
        {
            get { return _Rank; }
            set { _Rank = value; }
        }
        public string Ext
        {
            get { return _Ext; }
            set { _Ext = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public string ParentTable
        {
            get
            {
                return _ParentTable;
            }

            set
            {
                _ParentTable = value;
            }
        }

        public string CountryName
        {
            get
            {
                return _CountryName;
            }

            set
            {
                _CountryName = value;
            }
        }

        public string ProvinceName
        {
            get
            {
                return _ProvinceName;
            }

            set
            {
                _ProvinceName = value;
            }
        }

        public string DistrictName
        {
            get
            {
                return _DistrictName;
            }

            set
            {
                _DistrictName = value;
            }
        }

        public string WardName
        {
            get
            {
                return _WardName;
            }

            set
            {
                _WardName = value;
            }
        }
        #endregion

        #region [ Constructor Get Information ]
        public Address_Info()
        {
        }
        public Address_Info(DataRow InRow)
        {
            if (InRow["AuToKey"] != DBNull.Value)
                _AutoKey = int.Parse(InRow["AuToKey"].ToString());
            _ParentKey = InRow["ParentKey"].ToString();
            _ParentTable = InRow["ParentTable"].ToString();
            _Address = InRow["Address"].ToString().Trim();
            if (InRow["StreetKey"] != DBNull.Value)
                _StreetKey = int.Parse(InRow["StreetKey"].ToString());
            if (InRow["WardKey"] != DBNull.Value)
                _WardKey = int.Parse(InRow["WardKey"].ToString());
            if (InRow["DistrictKey"] != DBNull.Value)
                _DistrictKey = int.Parse(InRow["DistrictKey"].ToString());
            if (InRow["ProvinceKey"] != DBNull.Value)
                _ProvinceKey = int.Parse(InRow["ProvinceKey"].ToString());
            if (InRow["CountryKey"] != DBNull.Value)
                _CountryKey = int.Parse(InRow["CountryKey"].ToString());
            _Latlong = InRow["Latlong"].ToString().Trim();
            if (InRow["CategoryKey"] != DBNull.Value)
                _CategoryKey = int.Parse(InRow["CategoryKey"].ToString());
            if (InRow["RecordStatus"] != DBNull.Value)
                _RecordStatus = int.Parse(InRow["RecordStatus"].ToString());
            if (InRow["Rank"] != DBNull.Value)
                _Rank = int.Parse(InRow["Rank"].ToString());
            _Ext = InRow["Ext"].ToString().Trim();
            _Description = InRow["Description"].ToString().Trim();
            if (InRow["CreatedOn"] != DBNull.Value)
                _CreatedOn = (DateTime)InRow["CreatedOn"];
            _CreatedBy = InRow["CreatedBy"].ToString().Trim();
            _CreatedName = InRow["CreatedName"].ToString().Trim();
            if (InRow["ModifiedOn"] != DBNull.Value)
                _ModifiedOn = (DateTime)InRow["ModifiedOn"];
            _ModifiedBy = InRow["ModifiedBy"].ToString().Trim();
            _ModifiedName = InRow["ModifiedName"].ToString().Trim();
            _CountryName = InRow["CountryName"].ToString().Trim();
            _ProvinceName = InRow["ProvinceName"].ToString().Trim();
            _DistrictName = InRow["DistrictName"].ToString().Trim();
            _WardName = InRow["WardName"].ToString().Trim();
        }

        public Address_Info(int AuToKey)
        {
            string zSQL = "SELECT * FROM LOC_Address WHERE AuToKey = @AuToKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AuToKey", SqlDbType.Int).Value = AuToKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["AuToKey"] != DBNull.Value)
                        _AutoKey = int.Parse(zReader["AuToKey"].ToString());
                    _ParentKey = zReader["ParentKey"].ToString();
                    _ParentTable = zReader["ParentTable"].ToString();
                    _Address = zReader["Address"].ToString().Trim();
                    if (zReader["StreetKey"] != DBNull.Value)
                        _StreetKey = int.Parse(zReader["StreetKey"].ToString());
                    if (zReader["WardKey"] != DBNull.Value)
                        _WardKey = int.Parse(zReader["WardKey"].ToString());
                    if (zReader["DistrictKey"] != DBNull.Value)
                        _DistrictKey = int.Parse(zReader["DistrictKey"].ToString());
                    if (zReader["ProvinceKey"] != DBNull.Value)
                        _ProvinceKey = int.Parse(zReader["ProvinceKey"].ToString());
                    if (zReader["CountryKey"] != DBNull.Value)
                        _CountryKey = int.Parse(zReader["CountryKey"].ToString());
                    _Latlong = zReader["Latlong"].ToString().Trim();
                    if (zReader["CategoryKey"] != DBNull.Value)
                        _CategoryKey = int.Parse(zReader["CategoryKey"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["Rank"] != DBNull.Value)
                        _Rank = int.Parse(zReader["Rank"].ToString());
                    _Ext = zReader["Ext"].ToString().Trim();
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion

        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO LOC_Address ("
        + " ParentKey,ParentTable ,Address ,StreetKey ,WardKey ,DistrictKey ,ProvinceKey ,CountryKey ,Latlong ,CategoryKey ,Rank ,Ext ,Description ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@ParentKey,@ParentTable ,@Address ,@StreetKey ,@WardKey ,@DistrictKey ,@ProvinceKey ,@CountryKey ,@Latlong ,@CategoryKey  ,@Rank ,@Ext ,@Description ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            //zSQL += " SELECT AutoKey FROM LOC_Address WHERE AutoKey = SCOPE_IDENTITY() ";
            zSQL += " SELECT 11";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_ParentKey.Length > 0)
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = Guid.Parse(_ParentKey);
                else
                    zCommand.Parameters.Add("@ParentKey", SqlDbType.UniqueIdentifier).Value = DBNull.Value;
                zCommand.Parameters.Add("@ParentTable", SqlDbType.NVarChar).Value = _ParentTable.Trim();
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address.Trim();
                zCommand.Parameters.Add("@StreetKey", SqlDbType.Int).Value = _StreetKey;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;
                zCommand.Parameters.Add("@Latlong", SqlDbType.NVarChar).Value = _Latlong.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE LOC_Address SET "
                        + " Address = @Address,"
                        + " StreetKey = @StreetKey,"
                        + " WardKey = @WardKey,"
                        + " DistrictKey = @DistrictKey,"
                        + " ProvinceKey = @ProvinceKey,"
                        + " CountryKey = @CountryKey,"
                        + " Latlong = @Latlong,"
                        + " CategoryKey = @CategoryKey,"
                        + " Rank = @Rank,"
                        + " Ext = @Ext,"
                        + " Description = @Description,"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE AutoKey = @AutoKey";
            zSQL += " SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@Address", SqlDbType.NVarChar).Value = _Address.Trim();
                zCommand.Parameters.Add("@StreetKey", SqlDbType.Int).Value = _StreetKey;
                zCommand.Parameters.Add("@WardKey", SqlDbType.Int).Value = _WardKey;
                zCommand.Parameters.Add("@DistrictKey", SqlDbType.Int).Value = _DistrictKey;
                zCommand.Parameters.Add("@ProvinceKey", SqlDbType.Int).Value = _ProvinceKey;
                zCommand.Parameters.Add("@CountryKey", SqlDbType.Int).Value = _CountryKey;
                zCommand.Parameters.Add("@Latlong", SqlDbType.NVarChar).Value = _Latlong.Trim();
                zCommand.Parameters.Add("@CategoryKey", SqlDbType.Int).Value = _CategoryKey;
                zCommand.Parameters.Add("@Rank", SqlDbType.Int).Value = _Rank;
                zCommand.Parameters.Add("@Ext", SqlDbType.NVarChar).Value = _Ext.Trim();
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOC_Address SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ParentKey = @ParentKey";
            zSQL += " SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = _ParentKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete(string ParentKey)
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOC_Address SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE ParentKey = @ParentKey";
            zSQL += " SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@ParentKey", SqlDbType.NVarChar).Value = ParentKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteDetail()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE LOC_Address SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE AutoKey = @AutoKey";
            zSQL += " SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@AutoKey", SqlDbType.Int).Value = _AutoKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_AutoKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
