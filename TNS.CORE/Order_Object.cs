﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;

namespace TNS.CORE
{
    public class Order_Object : Order_Info
    {
        private List<Order_Rate_Info> _List_Rate;
        public List<Order_Rate_Info> List_Rate
        {
            get
            {
                return _List_Rate;
            }

            set
            {
                _List_Rate = value;
            }
        }

        private List<Order_Employee_Info> _List_Employee;
        public List<Order_Employee_Info> List_Employee
        {
            get
            {
                return _List_Employee;
            }

            set
            {
                _List_Employee = value;
            }
        }

        public Order_Object() : base()
        {
            _List_Rate = new List<Order_Rate_Info>();
            _List_Employee = new List<Order_Employee_Info>();
        }


        public Order_Object(string OrderKey) : base(OrderKey)
        {
            //--Order
            DataTable zTable = Order_Rate_Data.List(OrderKey);
            _List_Rate = new List<Order_Rate_Info>();
            foreach (DataRow nRow in zTable.Rows)
            {
                Order_Rate_Info zOrder = new Order_Rate_Info(nRow);
                _List_Rate.Add(zOrder);
            }
            //--Order_Employee
            zTable = Order_Employee_Data.List(OrderKey);
            _List_Employee = new List<Order_Employee_Info>();
            foreach (DataRow nRow in zTable.Rows)
            {
                Order_Employee_Info Order_Employee = new Order_Employee_Info(nRow);
                _List_Employee.Add(Order_Employee);
            }
        }
        public void Save_Object()
        {
            string zResult = "";
            base.Save();

            if (base.HasInfo && base.IsCommandOK)
            {
                for (int i = 0; i < _List_Rate.Count; i++)
                {
                    Order_Rate_Info zOrder_Rate = _List_Rate[i];
                    zOrder_Rate.OrderDate = base.OrderDate;
                    zOrder_Rate.OrderKey = base.Key;
                    zOrder_Rate.ModifiedBy = base.ModifiedBy;
                    zOrder_Rate.ModifiedName = base.ModifiedName;
                    switch (zOrder_Rate.RecordStatus)
                    {
                        case 99:
                            zOrder_Rate.Delete();
                            break;
                        case 1:
                            zOrder_Rate.Create();
                            break;
                        case 2:
                            zOrder_Rate.Update();
                            break;
                    }
                    zResult += zOrder_Rate.Message;
                }

                for (int i = 0; i < _List_Employee.Count; i++)
                {
                    Order_Employee_Info zOrder_Employee = _List_Employee[i];
                    zOrder_Employee.OrderDate = base.OrderDate;
                    zOrder_Employee.OrderKey = base.Key;
                    zOrder_Employee.ModifiedBy = base.ModifiedBy;
                    zOrder_Employee.ModifiedName = base.ModifiedName;
                    switch (zOrder_Employee.RecordStatus)
                    {
                        case 99:
                            zOrder_Employee.Delete();
                            break;
                        case 1:
                            zOrder_Employee.Create();
                            break;
                        case 2:
                            zOrder_Employee.Update();
                            break;
                    }
                    zResult += zOrder_Employee.Message;
                }
            }
        }

        public void Delete_Object()
        {
            string zResult = "";
            base.Delete();

            if (base.Message == "30")
            {
                Order_Rate_Info zOrder_Rate = new Order_Rate_Info();
                zOrder_Rate.ModifiedBy = base.ModifiedBy;
                zOrder_Rate.ModifiedName = base.ModifiedName;
                zOrder_Rate.OrderKey = base.Key;
                zOrder_Rate.Delete_All();
                zResult = zOrder_Rate.Message;

                Order_Employee_Info zOrder_Employee = new Order_Employee_Info();
                zOrder_Employee.ModifiedBy = base.ModifiedBy;
                zOrder_Employee.ModifiedName = base.ModifiedName;
                zOrder_Employee.OrderKey = base.Key;
                zOrder_Employee.Delete_All();
                zResult = zOrder_Employee.Message;

                //* Đông bổ sung xóa  đã chia tiền năng suất, chia lại cho nhóm lại lần 1, lần 2 ngày 31/01/2021
                Order_Money_Info zInfo = new Order_Money_Info();
                zInfo.OrderKey = base.Key;
                zInfo.Delete_Order();
                Order_Adjusted_Info zAd = new Order_Adjusted_Info();
                zAd.DeleteAll(base.Key);


                base.Message = zResult;
            }
        }
    }
}
