﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using TN.Connect;
namespace HHT.HRM
{
    public class Employee_KeepingTime_Info
    {

        #region [ Field Name ]
        private int _KeepingKey = 0;
        private DateTime _DateWrite;
        private int _TeamKey = 0;
        private string _TeamName = "";
        private string _EmployeeKey = "";
        private string _TeamID = "";
        private string _EmployeeName = "";
        private string _EmployeeID = "";
        private string _BeginTime = "";
        private int _BeginHour =0;
        private int _BeginMinute = 0;
        private string _EndTime = "";
        private int _EndHour = 0;
        private int _EndMinute = 0;
        private string _OffTime = "";
        private string _OverTime = "";
        private string _TotalTime = "";
        private int _TimeKey = 0;
        private string _TimeID = "";
        private string _TimeName = "";
        private string _RicePlus = "";
        private string _RiceMinus = "";
        private double _Money = 0;
        private DateTime _OverTimeBegin;
        private DateTime _OverTimeEnd;
        private string _Description = "";
        private int _RecordStatus = 0;
        private DateTime _CreatedOn;
        private string _CreatedBy = "";
        private string _CreatedName = "";
        private DateTime _ModifiedOn;
        private string _ModifiedBy = "";
        private string _ModifiedName = "";
        private string _RoleID = "";
        private string _Message = "";
        private int _Slug = 0;
        private string _Class = "TH"; //mặc định là ngày thường

        #endregion
        #region [ Properties ]

        public string RoleID
        {
            get { return _RoleID; }
            set { _RoleID = value; }
        }
        public int Key
        {
            get { return _KeepingKey; }
            set { _KeepingKey = value; }
        }
        public DateTime DateWrite
        {
            get { return _DateWrite; }
            set { _DateWrite = value; }
        }
        public int TeamKey
        {
            get { return _TeamKey; }
            set { _TeamKey = value; }
        }
        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }
        public string EmployeeKey
        {
            get { return _EmployeeKey; }
            set { _EmployeeKey = value; }
        }
        public string EmployeeName
        {
            get { return _EmployeeName; }
            set { _EmployeeName = value; }
        }
        public string EmployeeID
        {
            get { return _EmployeeID; }
            set { _EmployeeID = value; }
        }
        public string BeginTime
        {
            get { return _BeginTime; }
            set { _BeginTime = value; }
        }
        public string EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; }
        }
        public string OffTime
        {
            get { return _OffTime; }
            set { _OffTime = value; }
        }
        public string OverTime
        {
            get { return _OverTime; }
            set { _OverTime = value; }
        }
        public string TotalTime
        {
            get { return _TotalTime; }
            set { _TotalTime = value; }
        }
        public int TimeKey
        {
            get { return _TimeKey; }
            set { _TimeKey = value; }
        }
        public string TimeName
        {
            get { return _TimeName; }
            set { _TimeName = value; }
        }
        public string RicePlus
        {
            get { return _RicePlus; }
            set { _RicePlus = value; }
        }
        public string RiceMinus
        {
            get { return _RiceMinus; }
            set { _RiceMinus = value; }
        }
        public double Money
        {
            get { return _Money; }
            set { _Money = value; }
        }
        public DateTime OverTimeBegin
        {
            get { return _OverTimeBegin; }
            set { _OverTimeBegin = value; }
        }
        public DateTime OverTimeEnd
        {
            get { return _OverTimeEnd; }
            set { _OverTimeEnd = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public int RecordStatus
        {
            get { return _RecordStatus; }
            set { _RecordStatus = value; }
        }
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }
        public string CreatedName
        {
            get { return _CreatedName; }
            set { _CreatedName = value; }
        }
        public DateTime ModifiedOn
        {
            get { return _ModifiedOn; }
            set { _ModifiedOn = value; }
        }
        public string ModifiedBy
        {
            get { return _ModifiedBy; }
            set { _ModifiedBy = value; }
        }
        public string ModifiedName
        {
            get { return _ModifiedName; }
            set { _ModifiedName = value; }
        }
        public string Message
        {
            get { return _Message; }
            set { _Message = value; }
        }

        public int Slug
        {
            get
            {
                return _Slug;
            }

            set
            {
                _Slug = value;
            }
        }

        public string TeamID
        {
            get
            {
                return _TeamID;
            }

            set
            {
                _TeamID = value;
            }
        }

        public int EndHour
        {
            get
            {
                return _EndHour;
            }

            set
            {
                _EndHour = value;
            }
        }

        public int EndMinute
        {
            get
            {
                return _EndMinute;
            }

            set
            {
                _EndMinute = value;
            }
        }

        public int BeginHour
        {
            get
            {
                return _BeginHour;
            }

            set
            {
                _BeginHour = value;
            }
        }

        public int BeginMinute
        {
            get
            {
                return _BeginMinute;
            }

            set
            {
                _BeginMinute = value;
            }
        }

        public string TimeID
        {
            get
            {
                return _TimeID;
            }

            set
            {
                _TimeID = value;
            }
        }

        public string Class
        {
            get
            {
                return _Class;
            }

            set
            {
                _Class = value;
            }
        }
        #endregion
        #region [ Constructor Get Information ]
        public Employee_KeepingTime_Info()
        {
        }
        public Employee_KeepingTime_Info(int KeepingKey)
        {
            string zSQL = "SELECT * FROM HRM_Employee_KeepingTime WHERE KeepingKey = @KeepingKey";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@KeepingKey", SqlDbType.Int).Value = KeepingKey;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["KeepingKey"] != DBNull.Value)
                        _KeepingKey = int.Parse(zReader["KeepingKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _BeginTime = zReader["BeginTime"].ToString().Trim();
                    _EndTime = zReader["EndTime"].ToString().Trim();
                    _OffTime = zReader["OffTime"].ToString().Trim();
                    _OverTime = zReader["OverTime"].ToString().Trim();
                    _TotalTime = zReader["TotalTime"].ToString().Trim();
                    if (zReader["TimeKey"] != DBNull.Value)
                        _TimeKey = int.Parse(zReader["TimeKey"].ToString());
                    _TimeID = zReader["TimeID"].ToString().Trim();
                    _TimeName = zReader["TimeName"].ToString().Trim();
                    _RicePlus = zReader["RicePlus"].ToString().Trim();
                    _RiceMinus = zReader["RiceMinus"].ToString().Trim();
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["OverTimeBegin"] != DBNull.Value)
                        _OverTimeBegin = (DateTime)zReader["OverTimeBegin"];
                    if (zReader["OverTimeEnd"] != DBNull.Value)
                        _OverTimeEnd = (DateTime)zReader["OverTimeEnd"];
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    _Class = zReader["Class"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        public Employee_KeepingTime_Info(string EmployeeID, DateTime DateTime)
        {
            DateTime zFromDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(DateTime.Year, DateTime.Month, DateTime.Day, 23, 59, 59);
            string zSQL = "SELECT * FROM HRM_Employee_KeepingTime WHERE EmployeeID = @Name AND DateWrite BETWEEN @FromDate AND @ToDate AND RecordStatus <> 99";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@Name", SqlDbType.NVarChar).Value = EmployeeID.Trim();
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                SqlDataReader zReader = zCommand.ExecuteReader();
                if (zReader.HasRows)
                {
                    zReader.Read();
                    if (zReader["KeepingKey"] != DBNull.Value)
                        _KeepingKey = int.Parse(zReader["KeepingKey"].ToString());
                    if (zReader["DateWrite"] != DBNull.Value)
                        _DateWrite = (DateTime)zReader["DateWrite"];
                    if (zReader["TeamKey"] != DBNull.Value)
                        _TeamKey = int.Parse(zReader["TeamKey"].ToString());
                    _TeamName = zReader["TeamName"].ToString().Trim();
                    _EmployeeKey = zReader["EmployeeKey"].ToString().Trim();
                    _TeamID = zReader["TeamID"].ToString().Trim();
                    _EmployeeName = zReader["EmployeeName"].ToString().Trim();
                    _EmployeeID = zReader["EmployeeID"].ToString().Trim();
                    _BeginTime = zReader["BeginTime"].ToString().Trim();
                    _EndTime = zReader["EndTime"].ToString().Trim();

                    if (zReader["BeginHour"] != DBNull.Value)
                        _BeginHour = int.Parse(zReader["BeginHour"].ToString());
                    if (zReader["BeginMinute"] != DBNull.Value)
                        _BeginMinute = int.Parse(zReader["BeginMinute"].ToString());
                    if (zReader["EndHour"] != DBNull.Value)
                        _EndHour = int.Parse(zReader["EndHour"].ToString());
                    if (zReader["EndMinute"] != DBNull.Value)
                        _EndMinute = int.Parse(zReader["EndMinute"].ToString());

                    _OffTime = zReader["OffTime"].ToString().Trim();
                    _OverTime = zReader["OverTime"].ToString().Trim();
                    _TotalTime = zReader["TotalTime"].ToString().Trim();
                    if (zReader["TimeKey"] != DBNull.Value)
                        _TimeKey = int.Parse(zReader["TimeKey"].ToString());
                    _TimeID = zReader["TimeID"].ToString().Trim();
                    _TimeName = zReader["TimeName"].ToString().Trim();
                    _RicePlus = zReader["RicePlus"].ToString().Trim();
                    _RiceMinus = zReader["RiceMinus"].ToString().Trim();
                    if (zReader["Money"] != DBNull.Value)
                        _Money = double.Parse(zReader["Money"].ToString());
                    if (zReader["OverTimeBegin"] != DBNull.Value)
                        _OverTimeBegin = (DateTime)zReader["OverTimeBegin"];
                    if (zReader["OverTimeEnd"] != DBNull.Value)
                        _OverTimeEnd = (DateTime)zReader["OverTimeEnd"];
                    _Description = zReader["Description"].ToString().Trim();
                    if (zReader["Slug"] != DBNull.Value)
                        _Slug = int.Parse(zReader["Slug"].ToString());
                    if (zReader["RecordStatus"] != DBNull.Value)
                        _RecordStatus = int.Parse(zReader["RecordStatus"].ToString());
                    if (zReader["CreatedOn"] != DBNull.Value)
                        _CreatedOn = (DateTime)zReader["CreatedOn"];
                    _CreatedBy = zReader["CreatedBy"].ToString().Trim();
                    _CreatedName = zReader["CreatedName"].ToString().Trim();
                    if (zReader["ModifiedOn"] != DBNull.Value)
                        _ModifiedOn = (DateTime)zReader["ModifiedOn"];
                    _ModifiedBy = zReader["ModifiedBy"].ToString().Trim();
                    _ModifiedName = zReader["ModifiedName"].ToString().Trim();
                    _Class = zReader["Class"].ToString().Trim();
                }
                zReader.Close();
                zCommand.Dispose();
            }
            catch (Exception Err) { _Message = "08" + Err.ToString(); }
            finally { zConnect.Close(); }
        }
        #endregion
        #region [ Constructor Update Information ]
        public string Create()
        {
            //---------- String SQL Access Database ---------------
            string zSQL = "INSERT INTO HRM_Employee_KeepingTime ("
        + " DateWrite ,TeamKey ,TeamID,TeamName ,EmployeeKey ,EmployeeName ,EmployeeID ,BeginTime ,EndTime , BeginHour,BeginMinute,EndHour,EndMinute,OffTime ,OverTime ,TotalTime ,TimeKey,TimeID ,TimeName ,RicePlus ,RiceMinus ,Money ,OverTimeBegin ,OverTimeEnd ,Description ,Slug, Class, RecordStatus ,CreatedOn ,CreatedBy ,CreatedName ,ModifiedOn ,ModifiedBy ,ModifiedName ) "
         + " VALUES ( "
         + "@DateWrite ,@TeamKey ,@TeamID,@TeamName ,@EmployeeKey ,@EmployeeName ,@EmployeeID ,@BeginTime ,@EndTime ,@BeginHour,@BeginMinute,@EndHour,@EndMinute,@OffTime ,@OverTime ,@TotalTime ,@TimeKey ,@TimeID,@TimeName ,@RicePlus ,@RiceMinus ,@Money ,@OverTimeBegin ,@OverTimeEnd ,@Description ,@Slug, @Class,@RecordStatus ,GETDATE(),@CreatedBy ,@CreatedName ,GETDATE(),@ModifiedBy ,@ModifiedName ) ";
            zSQL += " SELECT '11' +CAST(KeepingKey AS nvarchar(50)) FROM HRM_Employee_KeepingTime WHERE KeepingKey = SCOPE_IDENTITY() ";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.NVarChar).Value = _BeginTime.Trim();
                zCommand.Parameters.Add("@EndTime", SqlDbType.NVarChar).Value = _EndTime.Trim();

                zCommand.Parameters.Add("@BeginHour", SqlDbType.Int).Value = _BeginHour;
                zCommand.Parameters.Add("@BeginMinute", SqlDbType.Int).Value = _BeginMinute;
                zCommand.Parameters.Add("@EndHour", SqlDbType.Int).Value = _EndHour;
                zCommand.Parameters.Add("@EndMinute", SqlDbType.Int).Value = _EndMinute;

                zCommand.Parameters.Add("@OffTime", SqlDbType.NVarChar).Value = _OffTime.Trim();
                zCommand.Parameters.Add("@OverTime", SqlDbType.NVarChar).Value = _OverTime.Trim();
                zCommand.Parameters.Add("@TotalTime", SqlDbType.NVarChar).Value = _TotalTime.Trim();
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zCommand.Parameters.Add("@TimeID", SqlDbType.NVarChar).Value = _TimeID.Trim();
                zCommand.Parameters.Add("@TimeName", SqlDbType.NVarChar).Value = _TimeName.Trim();
                zCommand.Parameters.Add("@RicePlus", SqlDbType.NVarChar).Value = _RicePlus.Trim();
                zCommand.Parameters.Add("@RiceMinus", SqlDbType.NVarChar).Value = _RiceMinus.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                if (_OverTimeBegin == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = _OverTimeBegin;
                if (_OverTimeEnd == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = _OverTimeEnd;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@CreatedBy", SqlDbType.NVarChar).Value = _CreatedBy.Trim();
                zCommand.Parameters.Add("@CreatedName", SqlDbType.NVarChar).Value = _CreatedName.Trim();
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@Class", SqlDbType.Char).Value = _Class.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zResult = zCommand.ExecuteScalar().ToString();
                if (zResult.Substring(0, 2) == "11")
                {
                    _Message = zResult.Substring(0,2);
                    _KeepingKey = int.Parse(zResult.Substring(2));
                }
                else
                    _KeepingKey = 0;
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Update()
        {
            string zSQL = "UPDATE HRM_Employee_KeepingTime SET "
                        + " DateWrite = @DateWrite,"
                        + " TeamKey = @TeamKey,"
                        + " TeamID = @TeamID,"
                        + " TeamName = @TeamName,"
                        + " EmployeeKey = @EmployeeKey,"
                        + " EmployeeName = @EmployeeName,"
                        + " EmployeeID = @EmployeeID,"
                        + " BeginTime = @BeginTime,"
                        + " EndTime = @EndTime,"
                        + " BeginHour = @BeginHour,"
                        + " BeginMinute = @BeginMinute,"
                        + " EndHour = @EndHour,"
                        + " EndMinute = @EndMinute,"
                        + " OffTime = @OffTime,"
                        + " OverTime = @OverTime,"
                        + " TotalTime = @TotalTime,"
                        + " TimeKey = @TimeKey,"
                        + " TimeID = @TimeID,"
                        + " TimeName = @TimeName,"
                        + " RicePlus = @RicePlus,"
                        + " RiceMinus = @RiceMinus,"
                        + " Money = @Money,"
                        + " OverTimeBegin = @OverTimeBegin,"
                        + " OverTimeEnd = @OverTimeEnd,"
                        + " Slug = @Slug,"
                        + " Description = @Description,"
                        + " Class = @Class,"
                        + " RecordStatus = @RecordStatus,"
                        + " ModifiedOn = GETDATE(),"
                        + " ModifiedBy = @ModifiedBy,"
                        + " ModifiedName = @ModifiedName"
                       + " WHERE KeepingKey = @KeepingKey SELECT 20";
            string zResult = "";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@KeepingKey", SqlDbType.Int).Value = _KeepingKey;
                if (_DateWrite == DateTime.MinValue)
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@DateWrite", SqlDbType.DateTime).Value = _DateWrite;
                zCommand.Parameters.Add("@TeamKey", SqlDbType.Int).Value = _TeamKey;
                zCommand.Parameters.Add("@TeamID", SqlDbType.NVarChar).Value = _TeamID.Trim();
                zCommand.Parameters.Add("@TeamName", SqlDbType.NVarChar).Value = _TeamName.Trim();
                zCommand.Parameters.Add("@EmployeeKey", SqlDbType.NVarChar).Value = _EmployeeKey.Trim();
                zCommand.Parameters.Add("@EmployeeName", SqlDbType.NVarChar).Value = _EmployeeName.Trim();
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = _EmployeeID.Trim();
                zCommand.Parameters.Add("@BeginTime", SqlDbType.NVarChar).Value = _BeginTime.Trim();
                zCommand.Parameters.Add("@EndTime", SqlDbType.NVarChar).Value = _EndTime.Trim();

                zCommand.Parameters.Add("@BeginHour", SqlDbType.Int).Value = _BeginHour;
                zCommand.Parameters.Add("@BeginMinute", SqlDbType.Int).Value = _BeginMinute;
                zCommand.Parameters.Add("@EndHour", SqlDbType.Int).Value = _EndHour;
                zCommand.Parameters.Add("@EndMinute", SqlDbType.Int).Value = _EndMinute;

                zCommand.Parameters.Add("@OffTime", SqlDbType.NVarChar).Value = _OffTime.Trim();
                zCommand.Parameters.Add("@OverTime", SqlDbType.NVarChar).Value = _OverTime.Trim();
                zCommand.Parameters.Add("@TotalTime", SqlDbType.NVarChar).Value = _TotalTime.Trim();
                zCommand.Parameters.Add("@TimeKey", SqlDbType.Int).Value = _TimeKey;
                zCommand.Parameters.Add("@TimeID", SqlDbType.NVarChar).Value = _TimeID.Trim();
                zCommand.Parameters.Add("@TimeName", SqlDbType.NVarChar).Value = _TimeName.Trim();
                zCommand.Parameters.Add("@RicePlus", SqlDbType.NVarChar).Value = _RicePlus.Trim();
                zCommand.Parameters.Add("@RiceMinus", SqlDbType.NVarChar).Value = _RiceMinus.Trim();
                zCommand.Parameters.Add("@Slug", SqlDbType.Int).Value = _Slug;
                zCommand.Parameters.Add("@Money", SqlDbType.Money).Value = _Money;
                if (_OverTimeBegin == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeBegin", SqlDbType.DateTime).Value = _OverTimeBegin;
                if (_OverTimeEnd == DateTime.MinValue)
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = DBNull.Value;
                else
                    zCommand.Parameters.Add("@OverTimeEnd", SqlDbType.DateTime).Value = _OverTimeEnd;
                zCommand.Parameters.Add("@Description", SqlDbType.NVarChar).Value = _Description.Trim();
                zCommand.Parameters.Add("@RecordStatus", SqlDbType.Int).Value = _RecordStatus;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy.Trim();
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName.Trim();
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                zCommand.Parameters.Add("@Class", SqlDbType.Char).Value = _Class.Trim();
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Delete()
        {
            string zResult = "";
            //---------- String SQL Access Database ---------------
            string zSQL = "UPDATE HRM_Employee_KeepingTime SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName WHERE KeepingKey = @KeepingKey SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@KeepingKey", SqlDbType.Int).Value = _KeepingKey;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string DeleteAll(string EmployeeID, DateTime DateWrite)
        {
            DateTime zFromDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 0, 0, 0);
            DateTime zToDate = new DateTime(DateWrite.Year, DateWrite.Month, DateWrite.Day, 23, 0, 0);
            string zResult = ""; 
            //---------- String SQL Access Database ---------------
            string zSQL = @"UPDATE HRM_Employee_KeepingTime SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName 
                            WHERE EmployeeID = @EmployeeID AND DateWrite BETWEEN @FromDate AND @ToDate 
                            UPDATE HRM_Employee_Rice SET [RecordStatus] = 99, [ModifiedOn] = GETDATE(), ModifiedBy = @ModifiedBy, ModifiedName = @ModifiedName 
                            WHERE  EmployeeID = @EmployeeID AND DateWrite BETWEEN @FromDate AND @ToDate 
                            SELECT 30";
            string zConnectionString = ConnectDataBase.ConnectionString;
            SqlConnection zConnect = new SqlConnection(zConnectionString);
            zConnect.Open();
            try
            {
                SqlCommand zCommand = new SqlCommand(zSQL, zConnect);
                zCommand.CommandType = CommandType.Text;
                zCommand.Parameters.Add("@EmployeeID", SqlDbType.NVarChar).Value = EmployeeID;
                zCommand.Parameters.Add("@ModifiedBy", SqlDbType.NVarChar).Value = _ModifiedBy;
                zCommand.Parameters.Add("@ModifiedName", SqlDbType.NVarChar).Value = _ModifiedName;
                zCommand.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = zFromDate;
                zCommand.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = zToDate;
                zCommand.Parameters.Add("@RoleID", SqlDbType.NVarChar).Value = _RoleID;
                _Message = zCommand.ExecuteScalar().ToString();
                zCommand.Dispose();
            }
            catch (Exception Err)
            {
                _Message = "08" + Err.ToString();
            }
            finally
            {
                zConnect.Close();
            }
            return zResult;
        }
        public string Save()
        {
            string zResult;
            if (_KeepingKey == 0)
                zResult = Create();
            else
                zResult = Update();
            return zResult;
        }
        #endregion
    }
}
