﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TN.Library.Inventory;
using TN.Library.SystemManagement;

namespace TN_WinApp
{
    public partial class Frm_Stages_Work : Form
    {
        private DataTable zTable;
        private DataRow Count;
        private Product_Stages_Info  Product_Stages;
        private DataRow zCount;
        private int _AutoKey = 0;
        private int _Catergory = 0;
        public Frm_Stages_Work()
        {
            InitializeComponent();
            GV_Product_Layout();
        }

        private void Frm_Stages_Work_Load(object sender, EventArgs e)
        {
            LV_Category_Layout(LV_ListGroup);
            LV_CategoryLoadData();

        }

        #region [Process Data]


        private void SaveGV()
        {

            string zMessage = "";
            int n = GV_Excel.Rows.Count;

            Product_Stages = new Product_Stages_Info();
            for (int i = 0; i < n - 1; i++)
            {
                if (GV_Excel.Rows[i].Tag == null)
                {
                    _AutoKey = 0;
                }
                else
                {
                    _AutoKey = int.Parse(GV_Excel.Rows[i].Tag.ToString());
                }
                Product_Stages.Key = _AutoKey;
                Product_Stages.StageName = GV_Excel.Rows[i].Cells["StageName"].Value.ToString();
                Product_Stages.StagesID = GV_Excel.Rows[i].Cells["StagesID"].Value.ToString();
                Product_Stages.UnitName = GV_Excel.Rows[i].Cells["Unit"].Value.ToString();
                Product_Stages.Price = float.Parse(GV_Excel.Rows[i].Cells["Price"].Value.ToString());
                Product_Stages.TeamKey = _Catergory;
                Product_Stages.Save();
            }
            if (zMessage.Length > 0)
            {
                MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GV_Employee_LoadData();
            }


        }
        #endregion

        #region [Design Layout]
        private void LV_Category_Layout(ListView LV)
        {
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.Text = "STT";
            colHead.Width = 70;
            colHead.TextAlign = HorizontalAlignment.Center;
            LV.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.Text = "Tên Nhóm";
            colHead.Width = 250;
            colHead.TextAlign = HorizontalAlignment.Left;
            LV.Columns.Add(colHead);
        }

        public void LV_CategoryLoadData()
        {
            this.Cursor = Cursors.WaitCursor;
            ListView LV = LV_ListGroup;
            ListViewItem lvi;
            ListViewItem.ListViewSubItem lvsi;

            DataTable In_Table = new DataTable();

            In_Table = Team_Data.List();

            LV.Items.Clear();
            int n = In_Table.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                lvi = new ListViewItem();
                lvi.Text = (i + 1).ToString();
                lvi.ForeColor = Color.Navy;
                DataRow nRow = In_Table.Rows[i];
                lvi.Tag = nRow["TeamKey"].ToString();
                lvi.BackColor = Color.White; ;

                lvi.ImageIndex = 0;
                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = nRow["TeamName"].ToString().Trim();
                lvi.SubItems.Add(lvsi);

                LV.Items.Add(lvi);

            }

            this.Cursor = Cursors.Default;

        }

        private void GV_Product_Layout()
        {
            // Setup Column 
            GV_Excel.Columns.Add("No", "STT");
            GV_Excel.Columns.Add("StageName", "Tên Công Việc");
            GV_Excel.Columns.Add("StagesID", "Mã");
            GV_Excel.Columns.Add("Unit", "Đơn vị tính");
            GV_Excel.Columns.Add("Price", "Đơn Giá");


            GV_Excel.Columns["No"].Width = 40;
            GV_Excel.Columns["No"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            GV_Excel.Columns["StageName"].Width = 420;
            GV_Excel.Columns["StageName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            GV_Excel.Columns["StageName"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            GV_Excel.Columns["StagesID"].Width = 100;
            GV_Excel.Columns["StagesID"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Excel.Columns["Unit"].Width = 100;
            GV_Excel.Columns["Unit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            GV_Excel.Columns["Price"].Width = 130;
            GV_Excel.Columns["Price"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


            // setup style view
            GV_Excel.BackgroundColor = Color.White;
            GV_Excel.GridColor = Color.FromArgb(227, 239, 255);
            GV_Excel.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            GV_Excel.DefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Excel.DefaultCellStyle.Font = new Font("Arial", 9);

            GV_Excel.AllowUserToResizeRows = false;
            GV_Excel.AllowUserToResizeColumns = true;

            GV_Excel.RowHeadersVisible = false;
            GV_Excel.AllowUserToAddRows = true;
            GV_Excel.AllowUserToDeleteRows = true;


            GV_Excel.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            GV_Excel.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomCenter;
            GV_Excel.ColumnHeadersDefaultCellStyle.ForeColor = Color.FromArgb(43, 43, 43);
            GV_Excel.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 9, FontStyle.Bold);

            for (int i = 1; i <= 500; i++)
            {
                GV_Excel.Rows.Add();
            }

            //this.GV_Employee.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.GV_Employee_CellEndEdit);
            //this.GV_Employee.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.GV_Employee_EditingControlShowing);
        }

        private void GV_Employee_LoadData()
        {
            GV_Excel.Rows.Clear();

            zTable = Product_Stages_Data.List_Product(int.Parse(LV_ListGroup.SelectedItems[0].Tag.ToString()));

            int i = 0;
            foreach (DataRow nRow in zTable.Rows)
            {
                GV_Excel.Rows.Add();
                DataGridViewRow nRowView = GV_Excel.Rows[i];
                nRowView.Tag = nRow["StageKey"].ToString();
                nRowView.Cells["No"].Value = (i + 1).ToString();
                nRowView.Cells["StageName"].Value = nRow["StageName"].ToString().Trim();
                nRowView.Cells["StagesID"].Value = nRow["StagesID"].ToString().Trim();
                nRowView.Cells["Unit"].Value = nRow["UnitName"].ToString().Trim();
                nRowView.Cells["Price"].Value = double.Parse(nRow["Price"].ToString().Trim());
                i++;
            }
        }
        #endregion



        private void btn_Import_Click(object sender, EventArgs e)
        {
            if (LV_ListGroup.SelectedItems.Count == 0)
            {
                MessageBox.Show("Vui lòng chọn công đoạn", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                _Catergory = int.Parse(LV_ListGroup.SelectedItems[0].Tag.ToString());
                Frm_Setup_Category frm = new Frm_Setup_Category();
                frm._CategoryParent = _Catergory;
                frm.ShowDialog();
                GV_Employee_LoadData();
            }
        }
        private void LV_ListGroup_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < LV_ListGroup.Items.Count; i++)
            {
                if (LV_ListGroup.Items[i].Selected == true)
                {
                    LV_ListGroup.Items[i].BackColor = Color.LightBlue; // highlighted item
                }
                else
                {
                    LV_ListGroup.Items[i].BackColor = SystemColors.Window; // normal item
                }
            }
            _Catergory = int.Parse(LV_ListGroup.SelectedItems[0].Tag.ToString());
            lbl_Name.Text = "CÁC CÔNG VIỆC CỦA CÔNG ĐOẠN " + ": " + LV_ListGroup.SelectedItems[0].SubItems[1].Text;
            GV_Employee_LoadData();
        }

        private void LV_ListGroup_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                Product_Stages_Info Product_Stages = new Product_Stages_Info(_Catergory);
                Product_Stages.Delete();
                if (Product_Stages.Message.Length > 0)
                {
                    MessageBox.Show("Vui lòng kiểm tra lại", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Cập nhật thành công", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LV_CategoryLoadData();
                }
            }
        }


        private void btn_Add_Click(object sender, EventArgs e)
        {
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            SaveGV();
        }

        private void GV_Excel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    foreach (DataGridViewRow nRow in GV_Excel.SelectedRows)
                    {
                        if (GV_Excel.CurrentRow.Tag != null)
                        {
                            int CategoryKey = int.Parse(GV_Excel.CurrentRow.Tag.ToString());
                            Product_Stages = new  Product_Stages_Info();
                            Product_Stages.Key = CategoryKey;
                            Product_Stages.Delete();
                            GV_Employee_LoadData();
                        }
                        else
                        {
                            GV_Excel.Rows.RemoveAt(GV_Excel.CurrentRow.Index);
                            GV_Employee_LoadData();
                        }
                    }
                }
            }

            if (e.KeyCode == Keys.Delete && e.Control)
            {
                DialogResult dlr = MessageBox.Show("Bạn có xóa thông tin này ?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dlr == DialogResult.Yes)
                {
                    foreach (DataGridViewRow nRow in GV_Excel.SelectedRows)
                    {
                        if (GV_Excel.CurrentRow.Tag != null)
                        {
                            int TeamKey = int.Parse(GV_Excel.CurrentRow.Tag.ToString());
                            Product_Stages = new  Product_Stages_Info();
                            Product_Stages.Delete(_Catergory);
                            GV_Employee_LoadData();
                        }
                        else
                        {
                            GV_Excel.Rows.RemoveAt(GV_Excel.CurrentRow.Index);
                            GV_Employee_LoadData();
                        }
                    }
                }
            }
        }

        private void GV_Excel_Click(object sender, EventArgs e)
        {
            if (_Catergory == 0)
            {
                MessageBox.Show("Vui lòng chọn đơn hàng", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                LV_ListGroup.SelectedItems[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#4876FF");
                LV_ListGroup.SelectedItems[0].ForeColor = Color.White;
            }
        }
    }
}
